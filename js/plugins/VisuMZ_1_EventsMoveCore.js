//=============================================================================
// VisuStella MZ - Events & Movement Core
// VisuMZ_1_EventsMoveCore.js
//=============================================================================

var Imported = Imported || {};
Imported.VisuMZ_1_EventsMoveCore = true;

var VisuMZ = VisuMZ || {};
VisuMZ.EventsMoveCore = VisuMZ.EventsMoveCore || {};
VisuMZ.EventsMoveCore.version = 1.19;

//=============================================================================
 /*:
 * @target MZ
 * @plugindesc [RPG Maker MZ] [Tier 1] [Version 1.19] [EventsMoveCore]
 * @author VisuStella
 * @url http://www.yanfly.moe/wiki/Events_and_Movement_Core_VisuStella_MZ
 * @orderAfter VisuMZ_0_CoreEngine
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 *
 * The Events & Movement Core plugin adds a lot of new functionality in terms
 * of event flexibility and movement options to RPG Maker MZ. These range from
 * adding in old capabilities from previous iterations of RPG Maker to more
 * mainstream techniques found in other game engines. Movement options are also
 * expanded to support 8-directional movement as well as sprite sheets provided
 * that the VisuStella 8 format is used.
 *
 * Features include all (but not limited to) the following:
 * 
 * * Event commands expanded upon to include old and new functions.
 * * Event templates for Copying Events, Morphing Events, and Spawning Events.
 * * 8-directional movement option available and sprite sheet support.
 * * Aesthetics for tilting the sprite when dashing and having shadows below.
 * * Pathfinding support for event movement through custom Move Route commands.
 * * Advanced switches and variable support to run code automatically.
 * * Turn regular Switches and Variables into Self Switches and Self Variables.
 * * Put labels and icons over events.
 * * Allow numerous ways to trigger events, through clicking, proximity, or by
 *   usage of Regions.
 * * Change the hitbox sizes of events to larger in any direction.
 * * Synchronize event movement options to move when player/other events move.
 * * The ability for the player to turn in place.
 *
 * ============================================================================
 * Requirements
 * ============================================================================
 *
 * This plugin is made for RPG Maker MZ. This will not work in other iterations
 * of RPG Maker.
 *
 * ------ Tier 1 ------
 *
 * This plugin is a Tier 1 plugin. Place it under other plugins of lower tier
 * value on your Plugin Manager list (ie: 0, 1, 2, 3, 4, 5). This is to ensure
 * that your plugins will have the best compatibility with the rest of the
 * VisuStella MZ library.
 *
 * ============================================================================
 * Features: Advanced Switches and Variables
 * ============================================================================
 *
 * Switches and variables can now run JavaScript code and return values
 * instantly. While at first glance, this may seem no different from using
 * the Control Variables event command's Script option, this can be used to
 * instantly set up Switch and/or Variable conditions for Parallel Common
 * Events, Event Page Conditions, Enemy Skill Conditions, and Troop Page
 * Conditions instantly without needing to make an event command to do so.
 *
 * ---
 *
 * <JS> code </JS>
 * - Used for: Switch and Variable names
 * - Replace 'code' with JavaScript code on what value to return.
 *
 * ---
 *
 * NOTE: Tagged Switches/Variables are mutually exclusive from one another.
 * You cannot tag them with <JS>, <Self>, or <Global> simultaneously.
 *
 * ============================================================================
 * Features: Self Switches and Variables
 * ============================================================================
 *
 * RPG Maker MZ by default has 4 Self Switches: A, B, C, D. For some types of
 * games, this isn't enough. This plugin gives you the ability convert regular
 * Switches into Self Switches so you could have more.
 *
 * Self Variables also do not exist in RPG Maker MZ by default. Just like with
 * Switches, you can turn regular Variables into Self Variables.
 *
 * ---
 *
 * <Self>
 * - Used for: Switch and Variable names
 * - Converts the Switch/Variable into a Self Switch/Variable.
 *
 * ---
 *
 * After, just use them like you would for normal Switches and Variables in an
 * event's page conditions. If the <Self> tag is present inside the Switch or
 * Variable's name, then it will use data unique to only that event.
 *
 * NOTE: Tagged Switches/Variables are mutually exclusive from one another.
 * You cannot tag them with <JS>, <Self>, or <Global> simultaneously.
 * 
 * ---
 * 
 * If you need to use a script call to get a Self Switch or Self Variable's
 * value, you can use the following script calls.
 * 
 *   ---
 * 
 *   Get Self Switch Values:
 * 
 *   getSelfSwitchValue(mapID, eventID, switchID)
 *   - Replace 'mapID' with the map ID the target event is located on.
 *   - Replace 'eventID' with the ID of the target event.
 *   - Replace 'switchID' with the ID number if it is a Self Switch made with
 *     <Self> or a capital letter surrounded by quotes if it's A, B, C, or D.
 *   - This will return the true/false value of the Self Switch.
 *   - Example: getSelfSwitchValue(12, 34, 56)
 *   - Example: getSelfSwitchValue(12, 34, 'B')
 * 
 *   ---
 * 
 *   Get Self Variable Values:
 * 
 *   getSelfVariableValue(mapID, eventID, variableID)
 *   - Replace 'mapID' with the map ID the target event is located on.
 *   - Replace 'eventID' with the ID of the target event.
 *   - Replace 'variableID' with the ID number of the Self Variable.
 *   - This will return whatever stored value is found in the Self Variable.
 *   - Example: getSelfVariableValue(12, 34, 56)
 * 
 *   ---
 * 
 *   Set Self Switch Values:
 * 
 *   setSelfSwitchValue(mapID, eventID, switchID, value)
 *   - Replace 'mapID' with the map ID the target event is located on.
 *   - Replace 'eventID' with the ID of the target event.
 *   - Replace 'switchID' with the ID number if it is a Self Switch made with
 *     <Self> or a capital letter surrounded by quotes if it's A, B, C, or D.
 *   - Replace 'value' with either 'true' or 'false' for ON/OFF respectively.
 *     Do not use quotes.
 *   - This will change the Self Switch's value to true/false.
 *     - Example: setSelfSwitchValue(12, 34, 56, false)
 *     - Example: setSelfSwitchValue(12, 34, 'B', true)
 * 
 *   ---
 * 
 *   Set Self Variable Values:
 * 
 *   setSelfVariableValue(mapID, eventID, variableID, value)
 *   - Replace 'mapID' with the map ID the target event is located on.
 *   - Replace 'eventID' with the ID of the target event.
 *   - Replace 'variableID' with the ID number of the Self Variable.
 *   - Replace 'value' with the value you want to set the Self Variable to.
 *   - Example: setSelfVariableValue(12, 34, 56, 88888)
 * 
 *   ---
 * 
 * ---
 *
 * ============================================================================
 * Features: VisuStella-Style 8-Directional Sprite Sheets
 * ============================================================================
 *
 * This plugin provides support for the VisuStella-Style 8-Directional Sprite
 * Sheets, also know as VS8. VS8 sprite sheets offer support for walking
 * frames, dashing frames, carrying frames, and emotes.
 *
 * ---
 *
 * To designate a sprite sheet as VS8, simply add [VS8] to the filename.
 * Something like Actor1.png would become Actor1_[VS8].png.
 *
 * ---
 *
 * VS8 sprites are formatted as such. Each block below is a set of 3 frames.
 *
 * Walk Down    Walk DL     Dash Down   Dash DL
 * Walk Left    Walk DR     Dash Left   Dash DR
 * Walk Right   Walk UL     Dash Right  Dash UL
 * Walk Up      Walk UR     Dash Up     Dash UR
 *
 * Carry Down   Carry DL    Ladder      Emotes 3
 * Carry Left   Carry DR    Rope        Emotes 4
 * Carry Right  Carry UL    Emotes 1    Emotes 5
 * Carry Up     Carry UR    Emotes 2    Emotes 6
 *
 * ---
 *
 * Here are how each of the emote sets are grouped from left to right.
 *
 * Emotes 1: Item, Hmph, Victory
 * Emotes 2: Hurt, Kneel, Collapse
 * Emotes 3: !, ?, Music Note
 * Emotes 4: Heart, Anger, Sweat
 * Emotes 5: Cobweb, ..., Light Bulb
 * Emotes 6: Sleep0, Sleep1, Sleep2
 *
 * ---
 *
 * ============================================================================
 * Features: Weighted Random Movement
 * ============================================================================
 * 
 * When creating events to place on the map, you can determine what type of
 * autonomous movement the event will have. When selecting "Random", the event
 * will move randomly across the map.
 * 
 * However, with the way "Random" movement works with the RPG Maker MZ default
 * code, the event is more likely to hit a wall and then hug the said wall as
 * it maps laps around the map's outer borders making it feel very unnatural
 * for any player who's been on the map long enough.
 * 
 * This is where "Weighted Random Movement" comes in. It changes up the random
 * movement behavior to function where the farther the event is, the more
 * likely the event is to step back towards its "home" position (aka where it
 * spawned upon loading the map). This is so that a housewife NPC doesn't
 * suddenly wander off into the middle of an army's training grounds on the
 * same town map.
 * 
 * The event will stay closer to its home value depending on how high the
 * weight's value is. There are a number of ways to adjust the weighted value.
 * 
 * ---
 * 
 * Plugin Parameters > Movement > Event Movement > Random Move Weight
 * 
 * This Plugin Parameter setting allows you to set the default weight for all
 * events with "Random" autonomous movement. It is set at a default value of
 * 0.10 to give the event an understandable degree of freedom.
 * 
 * Lower numbers give events more freedom to move. Larger numbers will make the
 * events stick closer to home.
 * 
 * Change this value to 0 to disable it.
 * 
 * ---
 * 
 * You can customize this individually per event by using Notetags and/or
 * Comment Tags for the events.
 * 
 * <Random Move Weight: x>
 * 
 * - Used for: Event Notetags and Event Page Comment Tags
 * - If this tag is used on an event with random-type autonomous movement, then
 *   the event will stick closer to their home location (where they are located
 *   upon spawning on the map). How close they stick to their home location
 *   will depend on the weighted 'x' value.
 * - Replace 'x' with a number between 0 and 1. Numbers closer to 0 give the
 *   event more freedom when moving randomly while numbers closer to 1 cause
 *   the event to stick closer to their home position.
 * 
 * <True Random Move>
 * 
 * - Used for: Event Notetags and Event Page Comment Tags
 * - If this tag is used on an event with random-type autonomous movement, then
 *   that event will ignore the effects of weighted randomized movement.
 * 
 * ---
 *
 * ============================================================================
 * Notetags and Comment Tags
 * ============================================================================
 *
 * The following are notetags that have been added through this plugin. These
 * notetags will not work with your game if this plugin is OFF or not present.
 *
 * Some of these are comment tags. Comment tags are used for events to mark and
 * affect individual event pages rather than the whole event.
 *
 * === Map Notetags ===
 *
 * The following notetags are used for maps only. While some of these options
 * are also available in the Plugin Parameters, some of these notetags extend
 * usage to specific maps marked by these notetags as well.
 *
 * ---
 *
 * <Diagonal Movement: On>
 * <Diagonal Movement: Off>
 *
 * - Used for: Map Notetags
 * - Turns on/off diagonal movement for those maps.
 * - If notetag isn't present, use Plugin Parameter setting.
 *
 * ---
 *
 * <type Allow Region: x>
 * <type Allow Region: x, x, x>
 *
 * <type Forbid Region: x>
 * <type Forbid Region: x, x, x>
 *
 * <type Dock Region: x>
 * <type Dock Region: x, x, x>
 *
 * - Used for: Map Notetags
 * - Replace 'type' with 'All', 'Walk', 'Player', 'Event', 'Vehicle', 'Boat',
 *   'Ship', or 'Airship'.
 * - 'Allow' notetag variants allow that type to pass through them no matter
 *   what other passability settings are in place.
 * - 'Forbid' notetag variants forbid that type from passing through at all.
 * - 'Dock' notetag variants allow vehicles to dock there. Boats and ships must
 *   face the region direction while airships must land directly on top.
 *
 * ---
 *
 * <Save Event Locations>
 *
 * - Used for: Maps Notetags
 * - Saves the locations of all events on the map so that when you return to
 *   that map at a later point, the events will be in the position they were
 *   last in.
 *
 * ---
 * 
 * === Page Comment Tags ===
 * 
 * The following comment tags are to be put inside of the pages of events,
 * troops, and common events for them to work!
 * 
 * ---
 * 
 * <Page Conditions>
 *   conditions
 *   conditions
 *   conditions
 * </Page Conditions>
 * 
 * - Used for: Map Event Page, Troop Page, and Common Event Page Comment Tags
 * - This allows you to create custom page conditions that utilize the
 *   Conditional Branch event command to see if the additional page conditions
 *   are met.
 * 
 * ---
 * 
 * <Conditions Met>
 * - Used for: Map Event Page, Troop Page, and Common Event Page Comment Tags
 * - If used between the <Page Conditions> and </Page Conditions> comment tag,
 *   upon reaching this part of event command list, the custom page conditions
 *   will be considered met.
 * 
 * ---
 * 
 * Example:
 * 
 * ◆Comment：<Page Conditions>
 * ◆If：Reid has equipped Potion Sword
 *   ◆Comment：If Reid has equipped the Potion Sword
 * ：       ：<Condition Met>
 *   ◆
 * ：End
 * ◆Comment：</Page Conditions>
 * 
 * If Reid has the "Potion Sword" weapon equipped, then the additional custom
 * page conditions are met and the event page will be present/active.
 * 
 * If this is a troop condition, the troop page event will activate.
 * 
 * If this is a common event, there will be a parallel common event active.
 * 
 * ---
 *
 * === Event and Event Page Notetags ===
 *
 * The following notetags have comment tag variants (with a few exceptions).
 * If a notetag is used for an event, it will affect the event constantly.
 * If a comment tag is used, it will only affect the page the comment tag is
 * on and only that page.
 *
 * ---
 *
 * <Activation Region: x>
 * <Activation Regions: x,x,x>
 *
 * - Used for: Event Notetags and Event Page Comment Tags
 * - Allows this event to be remotely activated as long as the player is
 *   standing within a tile marked by a designated region.
 * - Replace 'x' with the regions you wish to remotely activate this event in.
 *   - Action Button: Player must press OK while being in the region.
 *   - Player/Event Touch: Player must step onto the region.
 *   - Autorun/Parallel: Player be in the region.
 * - If this is placed in a notetag, the effect will be present across
 *   all event pages used.
 * - If this is placed inside a page's comment, the effect will only occur
 *   if that event page is currently active.
 * - NOTE: This cannot be used with any other activation tags.
 *
 * ---
 *
 * <Activation Square: x>
 * <Activation Radius: x>
 * <Activation Row: x>
 * <Activation Column: x>
 *
 * - Used for: Event Notetags and Event Page Comment Tags
 * - Allows this event to be remotely activated as long as the player is
 *   within range of its activation type.
 * - Replace 'x' with a number stating the range in tiles.
 *   - Square: A square-shaped range with the event at the center.
 *   - Radius: A diamond-shaped range with the event at the center.
 *   - Row: Spans horizontally across the map. 'x' expands up and down.
 *   - Column: Spans vertically across the map. 'x' expands left and right.
 * - If this is placed in a notetag, the effect will be present across
 *   all event pages used.
 * - If this is placed inside a page's comment, the effect will only occur
 *   if that event page is currently active.
 * - NOTE: This cannot be used with any other activation tags.
 *
 * ---
 *
 * <Always Update Movement>
 *
 * - Used for: Event Notetags and Event Page Comment Tags
 * - Events normally have to be within screen range for them to update their
 *   self movement. If this tag is present, the event is always updating.
 * - If this is placed in a notetag, the effect will be present across
 *   all event pages used.
 * - If this is placed inside a page's comment, the effect will only occur
 *   if that event page is currently active.
 *
 * ---
 *
 * <Click Trigger>
 *
 * - Used for: Event Notetags and Event Page Comment Tags
 * - Allows this event to activate upon being clicked on with the mouse.
 * - If this is placed in a notetag, the effect will be present across
 *   all event pages used.
 * - If this is placed inside a page's comment, the effect will only occur
 *   if that event page is currently active.
 *
 * ---
 *
 * <Copy Event: Map x, Event y>
 * <Copy Event: x, y>
 *
 * <Copy Event: template>
 *
 * - Used for: Event Notetags ONLY
 * - Makes this event copy all of the event settings from a different event
 *   that can be found on a different map (as long as that map is registered
 *   inside of Plugin Parameters => Event Template Settings => Preloaded Maps).
 * - Replace 'x' with a number representing the copied event's Map ID.
 * - Replace 'y' with a number representing the copied event's Event ID.
 * - For the 'template' variant, replace 'template' with the name of the
 *   template made in Plugin Parameters => Event Template Settings =>
 *   Event Template List.
 * - If this is placed in a notetag, the effect will be present across
 *   all event pages used.
 * - If this is placed inside a page's comment, the effect will only occur
 *   if that event page is currently active.
 *
 * ---
 *
 * <Hitbox Left: x>
 * <Hitbox Right: x>
 * <Hitbox Up: x>
 * <Hitbox Down: x>
 *
 * - Used for: Event Notetags and Event Page Comment Tags
 * - Replace 'x' with a number to extend the hitbox of the event by that many
 *   tiles towards the listed direction.
 * - Use multiples of this notetag to extend them to different directions.
 * - If this is placed in a notetag, the effect will be present across
 *   all event pages used.
 * - If this is placed inside a page's comment, the effect will only occur
 *   if that event page is currently active.
 *
 * ---
 *
 * <Icon: x>
 *
 * - Used for: Event Notetags and Event Page Comment Tags
 * - Replace 'x' with the Icon ID you wish to put above this event.
 * - This will not override any Icons designated to the ID through a
 *   Plugin Command.
 * - If this is placed in a notetag, the effect will be present across
 *   all event pages used.
 * - If this is placed inside a page's comment, the effect will only occur
 *   if that event page is currently active.
 *
 * ---
 *
 * <Icon Buffer X: +x>
 * <Icon Buffer X: -x>
 *
 * <Icon Buffer Y: +x>
 * <Icon Buffer Y: -x>
 *
 * <Icon Buffer: +x, +y>
 * <Icon Buffer: -x, -y>
 *
 * - Used for: Event Notetags and Event Page Comment Tags
 * - Allows you to adjust the positions of the icon on the envent by buffers.
 * - Replace 'x' and 'y' with the values to adjust the position buffers by.
 * - If this is placed in a notetag, the effect will be present across
 *   all event pages used.
 * - If this is placed inside a page's comment, the effect will only occur
 *   if that event page is currently active.
 *
 * ---
 *
 * <Icon Blend Mode: Normal>
 * <Icon Blend Mode: Additive>
 * <Icon Blend Mode: Multiply>
 * <Icon Blend Mode: Screen>
 *
 * - Used for: Event Notetags and Event Page Comment Tags
 * - Sets the blend mode for the icon on the event.
 * - If this is placed in a notetag, the effect will be present across
 *   all event pages used.
 * - If this is placed inside a page's comment, the effect will only occur
 *   if that event page is currently active.
 *
 * ---
 *
 * <Label: text>
 *
 * - Used for: Event Notetags and Event Page Comment Tags
 * - Puts a label over the event's head displaying 'text'.
 * - Text codes can be used.
 * - If this is placed in a notetag, the effect will be present across
 *   all event pages used.
 * - If this is placed inside a page's comment, the effect will only occur
 *   if that event page is currently active.
 *
 * ---
 *
 * <Label>
 * text
 * text
 * </Label>
 *
 * - Used for: Event Notetags and Event Page Comment Tags
 * - Puts a label over the event's head displaying 'text'.
 * - This can display multiple lines.
 * - Text codes can be used.
 * - If this is placed in a notetag, the effect will be present across
 *   all event pages used.
 * - If this is placed inside a page's comment, the effect will only occur
 *   if that event page is currently active.
 *
 * ---
 *
 * <Label Range: x>
 *
 * - Used for: Event Notetags and Event Page Comment Tags
 * - Sets a range requirement for the player to be in order for the event's
 *   label to appear.
 * - Replace 'x' with a number value depicting the range in tiles.
 * - If this is placed in a notetag, the effect will be present across
 *   all event pages used.
 * - If this is placed inside a page's comment, the effect will only occur
 *   if that event page is currently active.
 *
 * ---
 *
 * <Label Offset X: +x>
 * <Label Offset X: -x>
 *
 * <Label Offset Y: +x>
 * <Label Offset Y: -x>
 *
 * <Label Offset: +x, +y>
 * <Label Offset: -x, -y>
 *
 * - Used for: Event Notetags and Event Page Comment Tags
 * - Allows you to adjust the positions of the label on the envent by offsets.
 * - Replace 'x' and 'y' with the values to adjust the position offsets by.
 * - If this is placed in a notetag, the effect will be present across
 *   all event pages used.
 * - If this is placed inside a page's comment, the effect will only occur
 *   if that event page is currently active.
 *
 * ---
 * 
 * <Move Only Region: x>
 * <Move Only Regions: x,x,x>
 * 
 * - Used for: Event Notetags and Event Page Comment Tags
 * - Sets the move range of this event to only the region(s) marked by the
 *   notetag(s) or comment tag(s).
 * - This will bypass terrain passability.
 * - This will not bypass event collision.
 * - If this is placed in a notetag, the effect will be present across
 *   all event pages used.
 * - If this is placed inside a page's comment, the effect will only occur
 *   if that event page is currently active.
 * 
 * ---
 *
 * <Move Synch Target: Player>
 *
 * <Move Synch Target: Event x>
 *
 * - Used for: Event Notetags and Event Page Comment Tags
 * - Synchronizes the movement of this event with a target (either the player
 *   or another event). This event will only move whenever the synchronized
 *   target moves.
 * - For 'Event x' variant, replace 'x' with the ID of the event to synch to.
 * - If this is placed in a notetag, the effect will be present across
 *   all event pages used.
 * - If this is placed inside a page's comment, the effect will only occur
 *   if that event page is currently active.
 *
 * ---
 *
 * <Move Synch Type: Random>
 * <Move Synch Type: Approach>
 * <Move Synch Type: Away>
 * <Move Synch Type: Custom>
 *
 * <Move Synch Type: Mimic>
 * <Move Synch Type: Reverse Mimic>
 *
 * <Move Synch Type: Mirror Horizontal>
 * <Move Synch Type: Mirror Vertical>
 *
 * - Used for: Event Notetags and Event Page Comment Tags
 * - Choose the type of movement the event will have if it is synchronized to
 *   a target.
 *   - Random: Move to a random position.
 *   - Approach: Approaches target.
 *   - Away: Flees from target.
 *   - Custom: Follows a custom move route.
 *   - Mimic: Imitates the target's movement style.
 *   - Reverse Mimic: Does the opposite of the target's movement.
 *   - Mirror Horizontal: Moves as if a mirror is placed horizontally.
 *   - Mirror Vertical: Moves as if a mirror is placed vertically.
 * - If this is placed in a notetag, the effect will be present across
 *   all event pages used.
 * - If this is placed inside a page's comment, the effect will only occur
 *   if that event page is currently active.
 *
 * ---
 *
 * <Move Synch Delay: x>
 *
 * - Used for: Event Notetags and Event Page Comment Tags
 * - If this tag is present, the event will wait a bit after each move before
 *   moving again.
 * - Replace 'x' with the number of movement instances in between.
 * - If this is placed in a notetag, the effect will be present across
 *   all event pages used.
 * - If this is placed inside a page's comment, the effect will only occur
 *   if that event page is currently active.
 *
 * ---
 * 
 * <Random Move Weight: x>
 * 
 * - Used for: Event Notetags and Event Page Comment Tags
 * - If this tag is used on an event with random-type autonomous movement, then
 *   the event will stick closer to their home location (where they are located
 *   upon spawning on the map). How close they stick to their home location
 *   will depend on the weighted 'x' value.
 * - Replace 'x' with a number between 0 and 1. Numbers closer to 0 give the
 *   event more freedom when moving randomly while numbers closer to 1 cause
 *   the event to stick closer to their home position.
 * 
 * ---
 * 
 * <True Random Move>
 * 
 * - Used for: Event Notetags and Event Page Comment Tags
 * - If this tag is used on an event with random-type autonomous movement, then
 *   that event will ignore the effects of weighted randomized movement.
 * 
 * ---
 *
 * <Save Event Location>
 *
 * - Used for: Event Notetags ONLY
 * - Saves the locations of the event on the map so that when you return to
 *   that map at a later point, the event will be in the position it was
 *   last in.
 *
 * ---
 *
 * <Hide Shadow>
 * - Used for: Event Notetags and Event Page Comment Tags
 * - Hides the shadow for the event.
 * - If this is placed in a notetag, the effect will be present across
 *   all event pages used.
 * - If this is placed inside a page's comment, the effect will only occur
 *   if that event page is currently active.
 *
 * ---
 *
 * <Shadow Filename: filename>
 *
 * - Used for: Event Notetags and Event Page Comment Tags
 * - Replaces the shadow graphic used with 'filename' found in the
 *   img/system/ project folder.
 * - If this is placed in a notetag, the effect will be present across
 *   all event pages used.
 * - If this is placed inside a page's comment, the effect will only occur
 *   if that event page is currently active.
 *
 * ---
 *
 * <Sprite Offset X: +x>
 * <Sprite Offset X: -x>
 *
 * <Sprite Offset Y: +x>
 * <Sprite Offset Y: -x>
 *
 * <Sprite Offset: +x, +y>
 * <Sprite Offset: -x, -y>
 *
 * - Used for: Event Notetags and Event Page Comment Tags
 * - Changes how much the event's sprite is visibly offset by.
 * - Replace 'x' and 'y' with numbers indicating the offset in pixels.
 * - If this is placed in a notetag, the effect will be present across
 *   all event pages used.
 * - If this is placed inside a page's comment, the effect will only occur
 *   if that event page is currently active.
 *
 * ---
 *
 * <Step Pattern: Left to Right>
 * <Step Pattern: Right to Left>
 *
 * <Step Pattern: Spin Clockwise>
 * <Step Pattern: Spin CW>
 *
 * <Step Pattern: Spin CounterClockwise>
 * <Step Pattern: Spin CCW>
 * <Step Pattern: Spin AntiClockwise>
 * <Step Pattern: Spin ACW>
 *
 * - Used for: Event Notetags and Event Page Comment Tags
 * - Changes the way the event animates if a tag is present.
 *   - Left to Right: Makes the event sprite's step behavior go from frame 0 to
 *     1 to 2, then back to 0 instead of looping backward.
 *   - Right to Left: Makes the event sprite's step behavior go from frame 2 to
 *     1 to 0, then back to 2 instead of looping forward.
 *   - Spin Clockwise: Makes the event sprite's step behavior spin CW.
 *   - Spin CounterClockwise: Makes the event sprite's step behavior spin CCW.
 * - If this is placed in a notetag, the effect will be present across
 *   all event pages used.
 * - If this is placed inside a page's comment, the effect will only occur
 *   if that event page is currently active.
 *
 * ---
 *
 * ============================================================================
 * Plugin Commands
 * ============================================================================
 *
 * The following are Plugin Commands that come with this plugin. They can be
 * accessed through the Plugin Command event command.
 *
 * ---
 * 
 * === Auto Movement Plugin Commands ===
 * 
 * ---
 *
 * Auto Movement: Events
 * - Allow/stop events from auto movement.
 *
 *   Value:
 *   - Allow events to move automatically?
 *
 * ---
 * 
 * === Call Event Plugin Commands ===
 * 
 * ---
 *
 * Call Event: Remote Activation
 * - Runs the page of a different event remotely.
 *
 *   Map ID:
 *   - Target event's map. Use 0 for current map.
 *   - You may use JavaScript code.
 *
 *   Event ID:
 *   - The ID of the event to remotely run.
 *   - Use 0 for current event.
 *   - You may use JavaScript code.
 *
 *   Page ID:
 *   - The page of the remote event to run.
 *   - You may use JavaScript code.
 *
 * ---
 * 
 * === Dash Plugin Commands ===
 * 
 * ---
 *
 * Dash Enable: Toggle
 * - Enable/Disable Dashing on maps.
 *
 *   Value:
 *   - What do you wish to change dashing to?
 *
 * ---
 * 
 * === Event Icon Plugin Commands ===
 * 
 * ---
 *
 * Event Icon: Change
 * - Change the icon that appears on an event.
 *
 *   Map ID:
 *   - The map the target map. Use 0 for current map.
 *   - You may use JavaScript code.
 *
 *   Event ID:
 *   - The ID of the target event.
 *   - Use 0 for current event.
 *   - You may use JavaScript code.
 *
 *   Icon Index:
 *   - Icon index used for the icon.
 *   - You may use JavaScript code.
 *
 *   Buffer X:
 *   - How much to shift the X position by?
 *   - You may use JavaScript code.
 *
 *   Buffer Y:
 *   - How much to shift the Y position by?
 *   - You may use JavaScript code.
 *
 *   Blend Mode:
 *   - What kind of blend mode do you wish to apply to the icon sprite?
 *
 * ---
 *
 * Event Icon: Delete
 * - Delete the icon that appears on an event.
 *
 *   Map ID:
 *   - The map the target map. Use 0 for current map.
 *   - You may use JavaScript code.
 *
 *   Event ID:
 *   - The ID of the target event.
 *   - Use 0 for current event.
 *   - You may use JavaScript code.
 *
 * ---
 * 
 * === Event Label Plugin Commands ===
 * 
 * ---
 *
 * Event Label: Refresh
 * - Refresh all Event Labels on screen.
 * - This is used to refresh page conditions for map changes that don't
 *   force a refresh.
 *
 * ---
 *
 * Event Label: Visible
 * - Change the visibility of Event Labels.
 *
 *   Visibility:
 *   - What do you wish to change visibility to?
 *
 * ---
 * 
 * === Event Location Plugin Commands ===
 * 
 * ---
 *
 * Event Location: Save
 * - Memorize an event's map location so it reappears there the next time the
 *   map is loaded.
 *
 *   Event ID:
 *   - The ID of the target event.
 *   - Use 0 for current event.
 *   - You may use JavaScript code.
 *
 * ---
 *
 * Event Location: Delete
 * - Deletes an event's saved map location.
 * - The event will reappear at its default location.
 *
 *   Map ID:
 *   - The map the target map. Use 0 for current map.
 *   - You may use JavaScript code.
 *   
 *   Event ID:
 *   - The ID of the target event.
 *   - Use 0 for current event.
 *   - You may use JavaScript code.
 *
 * ---
 *
 * Event Location: Create
 * - Creates a custom spawn location for a specific map's event so it appears
 *   there the next time the map is loaded.
 *
 *   Map ID:
 *   - The map the target map. Use 0 for current map.
 *   - You may use JavaScript code.
 *
 *   Event ID:
 *   - The ID of the target event.
 *   - Use 0 for current event.
 *   - You may use JavaScript code.
 *
 *   X Coordinate:
 *   - The X coordinate of the event.
 *   - You may use JavaScript code.
 *
 *   Y Coordinate:
 *   - The Y coordinate of the event.
 *   - You may use JavaScript code.
 *
 *   Direction:
 *   - The direction the event will be facing.
 *
 *   Optional:
 *
 *     Page ID:
 *     - The page of the event to set the move route to.
 *     - You may use JavaScript code.
 *
 *     Move Route Index:
 *     - The point in the move route for this event to be at if the page ID
 *       matches the rest of the page conditions.
 *
 * ---
 * 
 * === Event Timer Plugin Commands ===
 * 
 * ---
 *
 * Event Timer: Change Speed
 * - Changes the timer frame decrease (or increase) speed.
 *
 *   Speed:
 *   - How many 1/60ths of a second does each frame increase or decrease by?
 *   - Negative decreases.
 *   - Positive increases.
 *   - JavaScript allowed.
 *
 * ---
 *
 * Event Timer: Expire Event Assign
 * - Sets a Common Event to run upon expiration.
 * - Bypasses the default code if one is set.
 *
 *   Common Event ID:
 *   - Select the Common Event to run upon the timer's expiration.
 *
 * ---
 *
 * Event Timer: Expire Event Clear
 * - Clears any set to expire Common Event and instead, run the default
 *   Game_Timer expiration code.
 *
 * ---
 *
 * Event Timer: Frames Gain
 * - Chooses how many frames, seconds, minutes, or hours are gained or lost for
 *   the event timer.
 *
 *   Frames:
 *   - How many 1/60ths of a second are gained/lost?
 *   - Positive for gain.
 *   - Negative for lost.
 *   - JavaScript allowed.
 *
 *   Seconds:
 *   - How many seconds are gained/lost?
 *   - Positive for gain.
 *   - Negative for lost.
 *   - JavaScript allowed.
 *
 *   Minutes:
 *   - How many minutes are gained/lost?
 *   - Positive for gain.
 *   - Negative for lost.
 *   - JavaScript allowed.
 *
 *   Hours:
 *   - How many hours are gained/lost?
 *   - Positive for gain.
 *   - Negative for lost.
 *   - JavaScript allowed.
 *
 * ---
 *
 * Event Timer: Frames Set
 * - Chooses how many frames, seconds, minutes, or hours are set for the event
 *   timer.
 *
 *   Frames:
 *   - Set frame count to this value.
 *   - Each frame is 1/60th of a second.
 *   - JavaScript allowed.
 *
 *   Seconds:
 *   - Set seconds to this value.
 *   - JavaScript allowed.
 *
 *   Minutes:
 *   - Set minutes to this value.
 *   - Each minute is 60 seconds.
 *   - JavaScript allowed.
 *
 *   Hours:
 *   - Set hours to this value.
 *   - Each hour is 60 minutes.
 *   - JavaScript allowed.
 *
 * ---
 *
 * Event Timer: Pause
 * - Pauses the current event timer, but does not stop it.
 *
 * ---
 *
 * Event Timer: Resume
 * - Resumes the current event timer from the paused state.
 *
 * ---
 * 
 * === Follower Control Plugin Commands ===
 * 
 * ---
 *
 * Follower: Set Global Chase
 * - Disables all followers from chasing the player or reenables it.
 *
 *   Chase:
 *   - Sets all followers to chase the player or not.
 *
 * ---
 *
 * Follower: Set Target Chase
 * - Disables target follower from chasing the player or reenables it.
 *
 *   Follower ID:
 *   - Select which follower ID to disable/reenable chasing for.
 *
 *   Chase:
 *   - Sets target follower to chase its target or not.
 *
 * ---
 *
 * Follower: Set Control
 * - Sets the event commands to target a follower when "Player" is selected as
 *   the target.
 *
 *   Follower ID:
 *   - Select which follower ID to control.
 *   - 0 is the player.
 *
 * ---
 *
 * Follower: Reset
 * - Resets all follower controls. Event Commands that target the "Player"
 *   return to normal and followers chase again.
 *
 * ---
 * 
 * === Global Switch Plugin Commands ===
 * 
 * ---
 * 
 * Global Switch: Get Self Switch A B C D
 * - Gets the current ON/OFF value from a Self Switch and stores it onto a
 *   Global Switch.
 * 
 *   Map ID:
 *   - The map the source map. Use 0 for current map.
 *   - You may use JavaScript code.
 * 
 *   Event ID:
 *   - The ID of the source event.
 *   - Use 0 for current event.
 *   - You may use JavaScript code.
 * 
 *   Letter:
 *   - Letter of the target event's Self Switch to obtain data from.
 * 
 *   -
 * 
 *   Target Switch ID:
 *   - The ID of the target switch.
 * 
 * ---
 * 
 * Global Switch: Get Self Switch ID
 * - Gets the current ON/OFF value from a Self Switch and stores it onto a
 *   Global Switch.
 * 
 *   Map ID:
 *   - The map the source map. Use 0 for current map.
 *   - You may use JavaScript code.
 * 
 *   Event ID:
 *   - The ID of the source event.
 *   - Use 0 for current event.
 *   - You may use JavaScript code.
 * 
 *   Switch ID:
 *   - The ID of the source switch.
 * 
 *   -
 * 
 *   Target Switch ID:
 *   - The ID of the target switch.
 * 
 * ---
 * 
 * === Global Variable Plugin Commands ===
 * 
 * ---
 * 
 * Global Variable: Get Self Variable ID
 * - Gets the current stored value from a Self Variable and stores it onto a
 *   Global Variable.
 * 
 *   Map ID:
 *   - The map the source map. Use 0 for current map.
 *   - You may use JavaScript code.
 * 
 *   Event ID:
 *   - The ID of the source event.
 *   - Use 0 for current event.
 *   - You may use JavaScript code.
 * 
 *   Variable ID:
 *   - The ID of the source variable.
 * 
 *   -
 * 
 *   Target Variable ID:
 *   - The ID of the target variable.
 * 
 * ---
 * 
 * === Morph Event Plugin Commands ===
 * 
 * ---
 *
 * Morph Event: Change
 * - Runs the page of a different event remotely.
 *
 *   Step 1:
 *
 *     Map ID:
 *     - Target event's map. Use 0 for current map.
 *     - You may use JavaScript code.
 *
 *     Event ID:
 *     - The ID of the target event.
 *     - Use 0 for current event.
 *     - You may use JavaScript code.
 *
 *   Step 2:
 *
 *     Template Name:
 *     - Name of the target event template to morph into.
 *     - Ignored if this is called "Untitled".
 *
 *     Map ID:
 *     - Target event's map. Use 0 for current map.
 *     - You may use JavaScript code.
 *
 *     Event ID:
 *     - The ID of the target event.
 *     - Use 0 for current event.
 *     - You may use JavaScript code.
 *
 *     Preserve Morph:
 *     - Is the morph effect preserved?
 *     - Or does it expire upon leaving the map?
 *
 * ---
 *
 * Morph Event: Remove
 * - Remove the morph status of an event.
 *
 *   Map ID:
 *   - Target event's map. Use 0 for current map.
 *   - You may use JavaScript code.
 *
 *   Event ID:
 *   - The ID of the event to remotely run.
 *   - Use 0 for current event.
 *   - You may use JavaScript code.
 *
 *   Remove Preservation:
 *   - Also remove the preservation effect?
 *
 * ---
 * 
 * === Player Icon Plugin Commands ===
 * 
 * ---
 *
 * Player Icon: Change
 * - Change the icon that appears on on the player.
 *
 *   Icon Index:
 *   - Icon index used for the icon.
 *   - You may use JavaScript code.
 *
 *   Buffer X:
 *   - How much to shift the X position by?
 *   - You may use JavaScript code.
 *
 *   Buffer Y:
 *   - How much to shift the Y position by?
 *   - You may use JavaScript code.
 *
 *   Blend Mode:
 *   - What kind of blend mode do you wish to apply to the icon sprite?
 *
 * ---
 *
 * Player Icon: Delete
 * - Delete the icon that appears on the player.
 *
 * ---
 * 
 * === Player Movement Plugin Commands ===
 * 
 * ---
 * 
 * Player Movement: Control
 * - Enable or disable player control over the player character's movement.
 * 
 *   Enable?:
 *   - Let the player control where the player character moves?
 * 
 * ---
 * 
 * Player Movement: Diagonal
 * - Override settings to for player diagonal movement.
 * 
 *   Setting:
 *   - How do you want to change diagonal movement?
 *   - Default: Whatever the Map Uses
 *   - Forcefully Disable Diagonal Movement
 *   - Forcefully Enable Diagonal Movement
 * 
 * ---
 * 
 * === Self Switch Plugin Commands ===
 * 
 * ---
 *
 * Self Switch: A B C D
 * - Change the Self Switch of a different event.
 *
 *   Map ID:
 *   - The map the target map. Use 0 for current map.
 *   - You may use JavaScript code.
 *
 *   Event ID:
 *   - The ID of the target event.
 *   - Use 0 for current event.
 *   - You may use JavaScript code.
 *
 *   Letter:
 *   - Letter of the target event's Self Switch to change.
 *
 *   Value:
 *   - What value do you want to set the Self Switch to?
 *
 * ---
 *
 * Self Switch: Switch ID
 * - Change the Self Switch of a different event.
 *
 *   Map ID:
 *   - The map the target map. Use 0 for current map.
 *   - You may use JavaScript code.
 *
 *   Event ID:
 *   - The ID of the target event.
 *   - Use 0 for current event.
 *   - You may use JavaScript code.
 *
 *   Switch ID:
 *   - The ID of the target switch.
 *
 *   Value:
 *   - What value do you want to set the Self Switch to?
 *
 * ---
 * 
 * === Self Variable Plugin Commands ===
 * 
 * ---
 *
 * Self Variable: Variable ID
 * - Change the Self Variable of a different event.
 *
 *   Map ID:
 *   - The map the target map. Use 0 for current map.
 *   - You may use JavaScript code.
 *
 *   Event ID:
 *   - The ID of the target event.
 *   - Use 0 for current event.
 *   - You may use JavaScript code.
 *
 *   Variable ID:
 *   - The ID of the target variable.
 *
 *   Value:
 *   - What value do you want to set the Self Switch to?
 *
 * ---
 * 
 * === Spawn Event Plugin Commands ===
 * 
 * ---
 *
 * Spawn Event: Spawn At X, Y
 * - Spawns desired event at X, Y location on the current map.
 *
 *   Step 1:
 *
 *     Template Name:
 *     - Name of the target event template to spawn as.
 *     - Ignored if this is called "Untitled".
 *
 *     Map ID:
 *     - Target event's map to be used as reference.
 *     - You may use JavaScript code.
 *
 *     Event ID:
 *     - The ID of the target event to be used as reference.
 *     - You may use JavaScript code.
 *
 *   Step 2:
 *
 *     X Coordinate:
 *     - Target Location to spawn at.
 *     - You may use JavaScript code.
 *
 *     Y Coordinate:
 *     - Target Location to spawn at.
 *     - You may use JavaScript code.
 *
 *     Check Event Collision:
 *     - Check collision with any other events and player?
 *
 *     Check Passability:
 *     - Check passability of the target location.
 *
 *     Preserve Spawn:
 *     - Is the spawned event preserved?
 *     - Or does it expire upon leaving the map?
 *
 *   Step 3:
 *
 *     Success Switch ID:
 *     - Target switch ID to record spawning success.
 *     - Ignore if ID is 0. OFF means failed. ON means success.
 *
 * ---
 *
 * Spawn Event: Spawn At Region
 * - Spawns desired event at a random region-marked location on the
 *   current map.
 *
 *   Step 1:
 *
 *     Template Name:
 *     - Name of the target event template to spawn as.
 *     - Ignored if this is called "Untitled".
 *
 *     Map ID:
 *     - Target event's map to be used as reference.
 *     - You may use JavaScript code.
 *
 *     Event ID:
 *     - The ID of the target event to be used as reference.
 *     - You may use JavaScript code.
 *
 *   Step 2:
 *
 *     Region ID(s):
 *     - Pick region(s) to spawn this event at.
 *
 *     Check Event Collision:
 *     - Check collision with any other events and player?
 *
 *     Check Passability:
 *     - Check passability of the target location.
 *
 *     Preserve Spawn:
 *     - Is the spawned event preserved?
 *     - Or does it expire upon leaving the map?
 *
 *   Step 3:
 *
 *     Success Switch ID:
 *     - Target switch ID to record spawning success.
 *     - Ignore if ID is 0. OFF means failed. ON means success.
 *
 * ---
 *
 * Spawn Event: Spawn At Terrain Tag
 * - Spawns desired event at a random terrain tag-marked location on the
 *   current map.
 *
 *   Step 1:
 *
 *     Template Name:
 *     - Name of the target event template to spawn as.
 *     - Ignored if this is called "Untitled".
 *
 *     Map ID:
 *     - Target event's map to be used as reference.
 *     - You may use JavaScript code.
 *
 *     Event ID:
 *     - The ID of the target event to be used as reference.
 *     - You may use JavaScript code.
 *
 *   Step 2:
 *
 *     Terrain Tag(s):
 *     - Pick terrain tag(s) to spawn this event at.
 *     - Insert numbers between 0 and 7.
 *
 *     Check Event Collision:
 *     - Check collision with any other events and player?
 *
 *     Check Passability:
 *     - Check passability of the target location.
 *
 *     Preserve Spawn:
 *     - Is the spawned event preserved?
 *     - Or does it expire upon leaving the map?
 *
 *   Step 3:
 *
 *     Success Switch ID:
 *     - Target switch ID to record spawning success.
 *     - Ignore if ID is 0. OFF means failed. ON means success.
 *
 * ---
 *
 * Spawn Event: Despawn Event ID
 * - Despawns the selected Event ID on the current map.
 *
 *   Event ID
 *   - The ID of the target event.
 *   - You may use JavaScript code.
 *
 * ---
 *
 * Spawn Event: Despawn At X, Y
 * - Despawns any spawned event(s) at X, Y location on the current map.
 *
 *   X Coordinate:
 *   - Target Location to despawn at.
 *   - You may use JavaScript code.
 *
 *   Y Coordinate:
 *   - Target Location to despawn at.
 *   - You may use JavaScript code.
 *
 * ---
 *
 * Spawn Event: Despawn Region(s)
 * - Despawns the selected Region(s) on the current map.
 *
 *   Region ID(s):
 *   - Pick region(s) and despawn everything inside it.
 *
 * ---
 *
 * Spawn Event: Despawn Terrain Tag(s)
 * - Despawns the selected Terrain Tags(s) on the current map.
 *
 *   Terrain Tag(s):
 *   - Pick terrain tag(s) and despawn everything inside it.
 *   - Insert numbers between 0 and 7.
 *
 * ---
 *
 * Spawn Event: Despawn Everything
 * - Despawns all spawned events on the current map.
 *
 * ---
 *
 * ============================================================================
 * Move Route Custom Commands
 * ============================================================================
 *
 * Some custom commands have been added to the "Set Movement Route" event
 * command. These can be accessed by pressing the "Script..." command and
 * typing in the following, which don't need to be in code form.
 *
 * Keep in mind that since these are custom additions and RPG Maker MZ does not
 * allow plugins to modify the editor, the "Preview" button will not factor in
 * the effects of these commands.
 * 
 * If you wish to use a value from a variable, insert $gameVariables.value(x)
 * or \V[x] in place of the x in any of the below.
 * 
 * If you wish to use a value from a self variable, insert \SelfVar[x] in place
 * of the x in any of the below. This will only draw from the current event. If
 * you wish to draw data from outside event self variables, we recommend you
 * use the \V[x] variant after using the Plugin Commands to draw data from them
 * for the best accuracy.
 *
 * ---
 * 
 * Animation: x
 * - Replace 'x' with the ID of the animation to play on moving unit.
 *
 * ---
 * 
 * Balloon: name
 * - Replace 'name' with any of the following to play a balloon on that the
 *   target moving unit.
 * - '!', '?', 'Music Note', 'Heart', 'Anger', 'Sweat', 'Cobweb', 'Silence',
 *   'Light Bulb', 'Sleep', 'User-Defined 1', 'User-Defined 2',
 *   'User-Defined 3', 'User-Defined 4', 'User-Defined 5'
 *    - Do NOT insert quotes.
 * - Examples:
 *   - Balloon: !
 *   - Balloon: Sleep
 *   - Balloon: Heart
 *
 * ---
 * 
 * Fade In: x
 * Fade Out: x
 * - Fades in/out the sprite's opacity.
 * - Fade In will continuously raise the opacity level until it reaches 255.
 * - Fade Out will continuously lower the opacity level until it reaches 0.
 * - Replace 'x' with the speed to fade in/out the sprite.
 * 
 * ---
 * 
 * Force Carry: On
 * Force Carry: Off
 * - For usage with the VS8 sprite sheet.
 * - Use ON to turn force carrying on.
 * - Use OFF to turn force carrying off.
 * - Sprites using the VS8 sprite sheet will also show the VS8 Carry frames.
 * 
 * ---
 * 
 * Force Dash: On
 * Force Dash: Off
 * - Use ON to turn force dashing on.
 * - Use OFF to turn force dashing off.
 * - Forces dashing will prompt the player or event to be in the dashing state.
 * - Sprites using the VS8 sprite sheet will also show the VS8 Dashing frames.
 * 
 * ---
 * 
 * Hug: Left
 * Hug: Right
 * - Causes the moving unit to hug the left/right side of the wall.
 *
 * ---
 * 
 * Index: x
 * - Replace 'x' with a number depicting the character index to change the
 *   moving unit's sprite to.
 *
 * ---
 * 
 * Index: +x
 * Index: -x
 * - Replace 'x' with the value to change the character index of the moving
 *   unit's sprite by.
 *
 * ---
 * 
 * Jump Forward: x
 * - Replace 'x' with the number of tiles for the unit to jump forward by.
 *
 * ---
 * 
 * Jump To: x, y
 * - Replace 'x' and 'y' with the coordinates for the unit to jump to.
 *
 * ---
 * 
 * Jump to Event: x
 * - Replace 'x' with the ID of the event for the unit to jump to.
 *
 * ---
 * 
 * Jump to Player
 * - Causes the moving unit to jump to the player.
 *
 * ---
 * 
 * Move Lower Left Until Stop
 * Move Down Until Stop
 * Move Lower Right Until Stop
 * Move Left Until Stop
 * Move Right Until Stop
 * Move Upper Left Until Stop
 * Move Up Until Stop
 * Move Upper Right Until Stop
 * - Causes the moving unit to move that direction until it hits a stop.
 *
 * ---
 * 
 * Move To: x, y
 * - Replace 'x' and 'y' with the map coordinates to move the unit to through
 *   pathfinding.
 * - This uses RPG Maker MZ's pathfinding algorithm. It is not perfect so do
 *   not expect the most optimal results.
 *
 * ---
 * 
 * Move to Event: x
 * - Replace 'x' with the ID of the event to move the unit to.
 * - This uses RPG Maker MZ's pathfinding algorithm. It is not perfect so do
 *   not expect the most optimal results.
 *
 * ---
 * 
 * Move to Player
 * - Moves the unit to the player.
 * - This uses RPG Maker MZ's pathfinding algorithm. It is not perfect so do
 *   not expect the most optimal results.
 *
 * ---
 * 
 * Move Lower Left: x
 * Move Down: x
 * Move Lower Right: x
 * Move Left: x
 * Move Right: x
 * Move Upper Left: x
 * Move Up: x
 * Move Upper Right: x
 * - Replace 'x' with the number of times to move the unit by in the designated
 *   direction on the map.
 *
 * ---
 * 
 * Opacity: x%
 * - Replace 'x' with the percentage to change the unit's sprite opacity to.
 *
 * ---
 * 
 * Opacity: +x
 * Opacity: -x
 * - Replace 'x' with the increment to change the unit's sprite opacity by.
 *
 * ---
 *
 * Pattern Lock: x
 * - Replace 'x' with the step pattern to lock the unit's sprite to.
 *
 * ---
 *
 * Pattern Unlock
 * - Removes pattern lock effect.
 *
 * ---
 * 
 * Pose: name
 * - If using a VS8 sprite, this will cause the unit to strike a pose.
 * - Replace 'name' with any the following:
 * - 'Item', 'Hmph', 'Victory', 'Hurt', 'Kneel', 'Collapse',
 *   '!', '?', 'Music Note', 'Heart', 'Anger', 'Sweat', 'Cobweb', 'Silence',
 *   'Light Bulb', 'Sleep'
 *    - Do NOT insert quotes.
 * - Examples:
 *   - Balloon: Item
 *   - Balloon: Victory
 *   - Balloon: ?
 *
 * ---
 * 
 * Step Toward: x, y
 * - Replace 'x' and 'y' for the desired coordinates for the unit to take one
 *   step towards.
 * - This uses RPG Maker MZ's pathfinding algorithm. It is not perfect so do
 *   not expect the most optimal results.
 *
 * ---
 * 
 * Step Toward Event: x
 * - Replace 'x' with the ID of the event for the unit to take one step to.
 * - This uses RPG Maker MZ's pathfinding algorithm. It is not perfect so do
 *   not expect the most optimal results.
 *
 * ---
 * 
 * Step Toward Player
 * - Causes event to take one step towards the player.
 * - This uses RPG Maker MZ's pathfinding algorithm. It is not perfect so do
 *   not expect the most optimal results.
 *
 * ---
 * 
 * Step Away From: x, y
 * - Replace 'x' and 'y' for the desired coordinates for the unit to take one
 *   step away from.
 * - This uses RPG Maker MZ's pathfinding algorithm. It is not perfect so do
 *   not expect the most optimal results.
 *
 * ---
 * 
 * Step Away From Event: x
 * - Replace 'x' with the ID of the event for the unit to take one step from.
 * - This uses RPG Maker MZ's pathfinding algorithm. It is not perfect so do
 *   not expect the most optimal results.
 *
 * ---
 * 
 * Step Away From Player
 * - Causes event to take one step away from the player.
 * - This uses RPG Maker MZ's pathfinding algorithm. It is not perfect so do
 *   not expect the most optimal results.
 *
 * ---
 * 
 * Turn To: x, y
 * - Replace 'x' and 'y' for the coordinates to make the unit face towards.
 * - This supports 8 directional turning.
 *
 * ---
 * 
 * Turn to Event: x
 * - Replace 'x' with the ID of the event to turn the unit towards.
 * - This supports 8 directional turning.
 *
 * ---
 * 
 * Turn to Player
 * - Causes the unit to turn towards the player.
 * - This supports 8 directional turning.
 *
 * ---
 * 
 * Turn Away From: x, y
 * - Replace 'x' and 'y' for the coordinates to make the unit face away from.
 * - This supports 8 directional turning.
 *
 * ---
 * 
 * Turn Away From Event: x
 * - Replace 'x' with the ID of the event to turn the unit away from.
 * - This supports 8 directional turning.
 *
 * ---
 * 
 * Turn Away From Player
 * - Causes the unit to turn away from the player.
 * - This supports 8 directional turning.
 *
 * ---
 * 
 * Turn Lower Left
 * Turn Lower Right
 * Turn Upper Left
 * Turn Upper Right
 * - Causes the unit to turn to one of the diagonal directions.
 *
 * ---
 * 
 * Self Switch x: On
 * Self Switch x: Off
 * Self Switch x: Toggle
 * - Replace 'x' with 'A', 'B', 'C', 'D', or a <Self> Switch ID to adjust the
 *   unit's Self Switch.
 *
 * ---
 * 
 * Self Variable x: y
 * - Replace 'x' with a <Self> Variable ID to adjust the unit's Self Variable.
 * - Replace 'y' with a number value to set the Self Variable to.
 *
 * ---
 * 
 * Teleport To: x, y
 * - Replace 'x' and 'y' with the coordinates to instantly move the unit to.
 *
 * ---
 * 
 * Teleport to Event: x
 * - Replace 'x' with the ID of the event to instantly move the unit to.
 *
 * ---
 * 
 * Teleport to Player
 * - Instantly moves the unit to the player's location.
 *
 * ---
 * 
 * If none of the commands are detected above, then a script call will be ran.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Event Label Settings
 * ============================================================================
 *
 * Event Labels are small windows created to display text over an event's head.
 * They're set up using the <Label> notetags and/or comment tags. Event Labels
 * are a great way to instantly relay information about the event's role to
 * the player.
 *
 * ---
 *
 * Event Labels
 * 
 *   Font Size:
 *   - The font size used for the Event Labels.
 * 
 *   Icon Size:
 *   - The size of the icons used in the Event Labels.
 * 
 *   Line Height:
 *   - The line height used for the Event Labels.
 * 
 *   Offset X:
 *   - Globally offset all labels horizontally by this amount.
 * 
 *   Offset Y:
 *   - Globally offset all labels vertically by this amount.
 * 
 *   Fade Speed:
 *   - Fade speed for labels.
 * 
 *   Visible Range:
 *   - Range the player has to be within the event to make its label visible.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Event Icon Settings
 * ============================================================================
 *
 * Icons can be displayed over an event's head through the <Icon> notetags
 * and/or comment tags. These can be used for a variety of things such as
 * making them look like they're carrying an item or to indicate they have a
 * specific role.
 *
 * ---
 *
 * Event Icon
 * 
 *   Buffer X:
 *   - Default X position buffer for event icons.
 * 
 *   Buffer Y:
 *   - Default Y position buffer for event icons.
 * 
 *   Blend Mode:
 *   - Default blend mode for even icons.
 *     - 0 - Normal
 *     - 1 - Additive
 *     - 2 - Multiply
 *     - 3 - Screen
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Event Template Settings
 * ============================================================================
 *
 * Event Templates allow you to store specific maps and/or event data to bring
 * out on need while having a premade set base. They're similar to prefabs but
 * aren't things that can be altered individually as one setting for an event
 * template will serve as a blueprint for all of them that use them.
 *
 * Event Templates are used for the <Copy Event> notetags, the Morph Event and
 * Spawn Event Plugin Commands.
 *
 * ---
 *
 * Settings
 * 
 *   Preloaded Maps:
 *   - A list of all the ID's of the maps that will be preloaded to serve as
 *     template maps for this plugin.
 *
 * ---
 *
 * Templates
 * - A list of all the Event Templates used by this project. Used for notetags
 *   and Plugin Commands.
 * 
 *     Name:
 *     - Name of the template. It'll be used as anchor points for notetags and
 *       Plugin Commands.
 * 
 *     Map ID:
 *     - ID of the map the template event is stored on.
 *     - This will automatically add this ID to preloaded list.
 * 
 *     Event ID:
 *     - ID of the event the template event is based on.
 * 
 *     JavaScript:
 *       JS: Pre-Copy:
 *       JS: Post-Copy:
 *       JS: Pre-Morph:
 *       JS: Post-Morph:
 *       JS: Pre-Spawn:
 *       JS: Post-Spawn:
 *       - Code that's ran during certain circumstances.
 *       - The code will occur at the same time as the ones listed in the main
 *         Event Template Settings Plugin Parameters. However, the ones listed
 *         in these individual entries will only occur for these specific
 *         templates and only if the templates are used.
 *
 * ---
 *
 * JavaScript
 * 
 *   JS: Pre-Copy:
 *   JS: Post-Copy:
 *   JS: Pre-Morph:
 *   JS: Post-Morph:
 *   JS: Pre-Spawn:
 *   JS: Post-Spawn:
 *   - Code that's ran during certain circumstances.
 *   - These are global and are ran for all copies, morphs, and/or spawns.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Movement Settings
 * ============================================================================
 *
 * These plugin parameters allow you to control how movement works in your
 * game, toggling it from 4-directional to 8-directional, setting up rules to
 * stop self-movement from events while an event or message is present, and
 * other aesthetics such as tilting the sprite while dashing, setting shadows
 * beneath the sprites, and allow for turning in place.
 *
 * ---
 *
 * 8 Directional Movement
 * 
 *   Enable:
 *   - Allow 8-directional movement by default? Players can move diagonally.
 * 
 *   Strict Collision:
 *   - Enforce strict collission rules where the player must be able to pass
 *     both cardinal directions?
 * 
 *   Favor Horizontal:
 *   - Favor horizontal if cannot pass diagonally but can pass both
 *     horizontally and vertically?
 * 
 *   Slower Diagonals?
 *   - Enforce a slower movement speed when moving diagonally?
 * 
 *     Speed Multiplier
 *     - What's the multiplier to adjust movement speed when moving diagonally?
 *
 * ---
 *
 * Automatic Movement
 * 
 *   Stop During Events:
 *   - Stop automatic event movement while events are running.
 * 
 *   Stop During Messages:
 *   - Stop automatic event movement while a message is running.
 *
 * ---
 * 
 * Bitmap
 * 
 *   Smoothing:
 *   - Do you want to smooth or pixelate the map sprites?
 *   - Pixelating them is better for zooming and tilting.
 * 
 * ---
 *
 * Dash
 * 
 *   Dash Modifier:
 *   - Alters the dash speed modifier.
 * 
 *   Enable Dash Tilt?:
 *   - Tilt any sprites that are currently dashing?
 * 
 *     Tilt Left Amount:
 *     - Amount in radians when moving left (upper left, left, lower left).
 * 
 *     Tilt Right Amount:
 *     - Amount in radians when moving right (upper right, right, lower right).
 * 
 *     Tilt Vertical Amount:
 *     - Amount in radians when moving vertical (up, down).
 *
 * ---
 * 
 * Event Movement
 * 
 *   Random Move Weight:
 *   - Use numbers between 0 and 1.
 *   - Numbers closer to 1 stay closer to their home position.
 *   - 0 to disable it.
 * 
 * ---
 *
 * Shadows
 * 
 *   Show:
 *   - Show shadows on all events and player-related sprites.
 * 
 *   Default Filename:
 *   - Default filename used for shadows found in img/system/ folder.
 *
 * ---
 *
 * Turn in Place
 * 
 *   Enable:
 *   - When not dashing, player will turn in place before moving.
 *   - This only applies with keyboard inputs.
 * 
 *   Delay in Frames:
 *   - The number of frames to wait before moving.
 *
 * ---
 * 
 * Vehicle Speeds
 * 
 *   Boat Speed:
 *   - Allows you to adjust the base speed of the boat vehicle.
 * 
 *   Ship Speed:
 *   - Allows you to adjust the base speed of the ship vehicle.
 * 
 *   Airship Speed:
 *   - Allows you to adjust the base speed of the airship vehicle.
 * 
 * ---
 *
 * ============================================================================
 * Plugin Parameters: VisuStella 8-Dir Settings
 * ============================================================================
 *
 * These are settings for sprite sheets using the VS8 format.
 * For more information on the VS8 format, look in the help section above.
 *
 * ---
 *
 * Balloon Icon Settings
 * 
 *   Auto-Balloon Poses:
 *   - Automatically pose VS8 sprites when using balloon icons.
 * 
 *   Balloon Offset X:
 *   - Offset balloon icons on VS8 sprites by x pixels.
 * 
 *   Balloon Offset Y:
 *   - Offset balloon icons on VS8 sprites by y pixels.
 *
 * ---
 *
 * Icons
 * 
 *   Auto Buffer:
 *   - Automatically buffer the X and Y coordinates of VS8 sprites?
 * 
 *   Use Carry Pose:
 *   - Use the carry pose when moving with an icon overhead.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Region Rulings
 * ============================================================================
 *
 * These settings allow you to decide the passability of the player, events,
 * and various vehicles through the usage of Regions.
 *
 * ---
 *
 * Allow Regions
 * 
 *   All Allow:
 *   Walk Allow:
 *   Player Allow:
 *   Event Allow:
 *   Vehicle Allow:
 *   Boat Allow:
 *   Ship Allow:
 *   Airship Allow:
 *   - Insert Region ID's where the affected unit type can enter.
 *   - Region ID's range from 0 to 255.
 *
 * ---
 *
 * Forbid Regions
 * 
 *   All Forbid:
 *   Walk Forbid:
 *   Player Forbid:
 *   Event Forbid:
 *   Vehicle Forbid:
 *   Boat Forbid:
 *   Ship Forbid:
 *   Airship Forbid:
 *   - Insert Region ID's where the affected unit type cannot enter.
 *   - Region ID's range from 0 to 255.
 *
 * ---
 *
 * Dock Regions
 * 
 *   Vehicle Dock:
 *   Boat Dock:
 *   Ship Dock:
 *   Airship Dock:
 *   - Insert Region ID's where the affected vehicle can dock
 *   - Region ID's range from 0 to 255.
 * 
 *   Only Region Dockable:
 *   - Vehicles are only able to dock at designated regions.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Common Event on OK Button
 * ============================================================================
 *
 * These Plugin Parameters allow you to setup Common Events that activate using
 * Regions when pressing the OK button while standing on top of them or in
 * front of them. These let you create near universally interactable objects
 * using Regions, such as rivers to start up fishing events or locations to
 * places items on.
 *
 * ---
 *
 * Regions
 * 
 *   Regions 1 - 255:
 *   - Which Common Event does this region activate?
 *   - Use 0 to not activate any Common Events.
 *
 * ---
 *
 * Target Tile
 * 
 *   Target Tile:
 *   - Which tile should be checked for Common Event on OK Button?
 *     - Tile in front of player.
 *     - Tile player is standing on top of.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Common Event on Touch
 * ============================================================================
 *
 * These Plugin Parameters allow you to setup Common Events that trigger when
 * stepping onto Region-marked tiles. These let you create custom effects that
 * will occur such as customized damage floors, traps, and/or events.
 *
 * ---
 *
 * Regions
 * 
 *   Regions 1 - 255:
 *   - Which Common Event does this region activate?
 *   - Use 0 to not activate any Common Events.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Terrain Tag Settings
 * ============================================================================
 *
 * Terrain Tags are used in Database => Tilesets to mark certain tiles and
 * give them unique properties through terrain tags.
 *
 * ---
 *
 * Terrain Tag ID's
 * 
 *   Rope:
 *   - Which terrain tag number to use for ropes?
 *
 * ---
 *
 * ============================================================================
 * Terms of Use
 * ============================================================================
 *
 * 1. These plugins may be used in free or commercial games provided that they
 * have been acquired through legitimate means at VisuStella.com and/or any
 * other official approved VisuStella sources. Exceptions and special
 * circumstances that may prohibit usage will be listed on VisuStella.com.
 * 
 * 2. All of the listed coders found in the Credits section of this plugin must
 * be given credit in your games or credited as a collective under the name:
 * "VisuStella".
 * 
 * 3. You may edit the source code to suit your needs, so long as you do not
 * claim the source code belongs to you. VisuStella also does not take
 * responsibility for the plugin if any changes have been made to the plugin's
 * code, nor does VisuStella take responsibility for user-provided custom code
 * used for custom control effects including advanced JavaScript notetags
 * and/or plugin parameters that allow custom JavaScript code.
 * 
 * 4. You may NOT redistribute these plugins nor take code from this plugin to
 * use as your own. These plugins and their code are only to be downloaded from
 * VisuStella.com and other official/approved VisuStella sources. A list of
 * official/approved sources can also be found on VisuStella.com.
 *
 * 5. VisuStella is not responsible for problems found in your game due to
 * unintended usage, incompatibility problems with plugins outside of the
 * VisuStella MZ library, plugin versions that aren't up to date, nor
 * responsible for the proper working of compatibility patches made by any
 * third parties. VisuStella is not responsible for errors caused by any
 * user-provided custom code used for custom control effects including advanced
 * JavaScript notetags and/or plugin parameters that allow JavaScript code.
 *
 * 6. If a compatibility patch needs to be made through a third party that is
 * unaffiliated with VisuStella that involves using code from the VisuStella MZ
 * library, contact must be made with a member from VisuStella and have it
 * approved. The patch would be placed on VisuStella.com as a free download
 * to the public. Such patches cannot be sold for monetary gain, including
 * commissions, crowdfunding, and/or donations.
 *
 * ============================================================================
 * Credits
 * ============================================================================
 * 
 * If you are using this plugin, credit the following people in your game:
 * 
 * Team VisuStella
 * * Yanfly
 * * Arisu
 * * Olivia
 * * Irina
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 * 
 * Version 1.19: February 12, 2021
 * * Bug Fixes!
 * ** "Self Variable: Variable ID" plugin command's Map ID should now be able
 *    to use "0" to self reference the current map. Fix made by Olivia.
 * 
 * Version 1.18: February 5, 2021
 * * Bug Fixes!
 * ** Event icon plugin commands should now work properly. Fix made by Arisu.
 * * Documentation Update!
 * ** Added new "Features: Weighted Random Movement" section.
 * ** Help file updated for new features.
 * * New Features!
 * ** New Notetags added by Arisu:
 * *** <Random Move Weight: x>
 * **** If this tag is used on an event with random-type autonomous movement,
 *      then the event will stick closer to their home location (where they are
 *      located upon spawning on the map). How close they stick to their home
 *      location will depend on the weighted 'x' value.
 * *** <True Random Move>
 * **** If this tag is used on an event with random-type autonomous movement,
 *      then that event will ignore the effects of weighted randomized
 *      movement.
 * ** New Plugin Commands added by Arisu and sponsored by AndyL:
 * *** Event Timer: Change Speed
 * *** Event Timer: Expire Event Assign
 * *** Event Timer: Expire Event Clear
 * *** Event Timer: Frames Gain
 * *** Event Timer: Frames Set
 * *** Event Timer: Pause
 * *** Event Timer: Resume
 * **** The above Plugin Commands allow you to control the game timer better.
 * ** New Plugin Parameters added by Arisu:
 * *** Plugin Parameters > Movement > Event Movement > Random Move Weight
 * **** Use numbers between 0 and 1. Numbers closer to 1 stay closer to their
 *      home position.
 * 
 * Version 1.17: January 29, 2021
 * * Documentation Update!
 * ** Added "Do NOT insert quotes" to "Balloon: name" and "Pose: name".
 * ** Added Examples for extra clarification.
 * * Optimization Update!
 * ** When touch clicking an event on a map with multiple events, pathfinding
 *    will utilize the non-diagonal function for less resource consumption to
 *    prevent FPS frame drops. Fix made by Arisu.
 * 
 * Version 1.16: January 22, 2021
 * * Optimization Update!
 * ** When touch clicking multiple times on an impassable tile, pathfinding
 *    will utilize the non-diagonal function for less resource consumption to
 *    prevent FPS frame drops. Fix made by Arisu.
 * 
 * Version 1.15: January 1, 2021
 * * Bug Fixes!
 * ** Spawned events should now resume their automated self movement after
 *    being interacted with. Fix made by Yanfly.
 * * Documentation Update!
 * ** Help file updated for new features.
 * ** Help file updated for updated features.
 * * Feature Updates!
 * ** Collission checks for the Spawn Event Plugin Commands now account for
 *    the spawning event's Hitbox, too. Update made by Yanfly.
 * ** Spawn Event Plugin Commands adds a new parameter "Success Switch ID" to
 *    check if the spawning has been successful or not.
 * * New Features!
 * ** New Plugin Commands added by Yanfly!
 * *** Spawn Event: Spawn At Terrain Tag
 * *** Spawn Event: Despawn Terrain Tag(s)
 * **** These function similar to their region counterparts except they target
 *      terrain tags instead.
 * 
 * Version 1.14: December 18, 2020
 * * Bug Fixes!
 * ** Caching for event label positions now account for page index.
 *    Fix made by Yanfly.
 * * Documentation Update!
 * ** Added documentation for the new features!
 * * New Features!
 * ** New Plugin Commands added by Irina.
 * *** Follower: Set Global Chase
 * *** Follower: Set Target Chase
 * *** Follower: Set Control
 * *** Follower: Reset
 * **** These plugin commands allow you to change whether or not the followers
 *      will chase their intended targets and/or shift control over their
 *      movement route from the "Player" to the target follower.
 * 
 * Version 1.13: December 4, 2020
 * * Bug Fixes!
 * ** Caching for event label positions now account for one-screen maps.
 *    Fix made by Arisu.
 * 
 * Version 1.12: November 29, 2020
 * * Bug Fixes!
 * ** Click Triggers no longer work on erased events. Fix made by Arisu.
 * ** Erased events no longer have icons appear above their heads.
 *    Fix made by Arisu.
 * * Feature Update!
 * ** Initialization of the plugin's effects no only occur if the event's
 *    current page settings have been altered. Change made by Arisu.
 * * Optimization Update!
 * ** Plugin should run more optimized.
 * 
 * Version 1.11: November 15, 2020
 * * Bug Fixes!
 * ** Morph plugin command should no longer cause crashes. Fix made by Yanfly.
 * * Documentation Update!
 * ** Added documentation for the updated features!
 * * Feature Updates!
 * ** Updates to these Plugin Commands made by Yanfly:
 * *** Call Event: Remote Activation
 * *** Event Icon: Change
 * *** Event Icon: Delete
 * *** Event Location: Create
 * *** Event Location: Delete
 * *** Global Switch: Get Self Switch A B C D
 * *** Global Switch: Get Self Switch ID
 * *** Global Variable: Get Self Variable ID
 * *** Morph Event: Change
 * *** Morph Event: Remove
 * *** Self Switch: A B C D
 * *** Self Switch: Switch ID
 * *** Self Variable: Variable ID
 * **** All of the above Plugin Commands can now use 0 for their Event ID's in
 *      order to refer to the running event's ID value.
 * 
 * Version 1.10: November 1, 2020
 * * Bug Fixes!
 * ** Spawned Event preserve function now works properly. Fix made by Arisu.
 * 
 * Version 1.09: October 25, 2020
 * * Documentation Update
 * ** Added clarity on the notetags and comment tags on when their effects
 *    are present.
 * * Feature Update!
 * ** Event icons now have an unsmoothing property to them to make them
 *    look better. Update made by Irina.
 * 
 * Version 1.08: October 11, 2020
 * * Compatibility Update
 * ** Added failsafes for better compatibility.
 * 
 * Version 1.07: October 4, 2020
 * * Documentation Update!
 * ** Updated for the new features!
 * * Feature Update!
 * ** Data from deleted events will now be cleared and removed from maps if the
 *    events do not exist to prevent conflict with plugins from the VisuStella
 *    MZ library and other plugins. Feature added by Irina.
 * ** Move Route Custom Commands now support self variable values! If you wish
 *    to use a value from a self variable, insert \SelfVar[x] in place of the x
 *    in any of the below. This will only draw from the current event. If you 
 *    wish to draw data from outside event self variables, we recommend you
 *    use the \V[x] variant after using the Plugin Commands to draw data from
 *    them for the best accuracy.
 * * New Features!
 * ** New Plugin Parameter added by Yanfly!
 * *** Movement > Bitmap > Smoothing
 * **** Do you want to smooth or pixelate the map sprites? Pixelating them is
 *      better for zooming and tilting.
 * 
 * Version 1.06: September 27, 2020
 * * Bug Fixes!
 * ** Events & Movement Core no longer disables the Core Engine's Smart Event
 *    Collision plugin parameter. Fix made by Yanfly.
 * * Documentation Update!
 * ** Move Route Custom Commands updated with the new feature for inserting
 *    variable values.
 * * Feature Update!
 * ** Move Route Custom Commands now support $gameVariable.value(x) values.
 *    You can also just use \V[x] for variable values, too. Added by Irina.
 * 
 * Version 1.05: September 20, 2020
 * * Bug Fixes!
 * ** If player movement is disabled, mouse movement is disabled, too.
 *    Fix made by Arisu.
 * ** The region restriction notetags should be fixed and work again.
 *    Fix made by Arisu.
 * 
 * Version 1.04: September 13, 2020
 * * Feature Update!
 * * Some Move Route Custom Commands are updated to ignore spaces:
 * ** Jump To: x, y
 * ** Move To: x, y
 * ** Step Toward: x, y
 * ** Step Away From: x, y
 * ** Turn To: x, y
 * ** Turn Away From: x, y
 * ** Teleport To: x, y
 * *** These can now be written as x,y. There still needs to be a space between
 *     the : and x for parsing clarity, however.
 * *** Feature updated by Arisu with help from BlueMoon and Zeriab.
 * * New Features!
 * ** New 'Move Route Custom Commands' added by Arisu.
 * *** Fade In: x
 * *** Fade Out: x
 * *** Force Carry: On
 * *** Force Carry: Off
 * *** Force Dash: On
 * *** Force Dash: Off
 * ** New Plugin Commands added by Arisu.
 * *** Player Movement: Control
 * **** Enable or disable player control over the player character's movement.
 * *** Player Movement: Diagonal
 * **** Override settings to for player diagonal movement.
 * 
 * Version 1.03: September 6, 2020
 * * Bug Fixes!
 * ** Sleeping pose is now fixed and working! Fix made by Yanfly.
 * * Documentation Update!
 * ** Extended "Features: Self Switches and Variables" to explain how to use
 *    script calls to grab self switch information.
 * * New Features!
 * ** New Plugin Commands added by Yanfly:
 * *** Global Switch: Get Self Switch A B C D
 * *** Global Switch: Get Self Switch ID
 * *** Global Variable: Get Self Variable ID
 * **** These plugin commands allow you to transfer data stored in a self
 *      switch or Self Variable into a global switch or global variable.
 * 
 * Version 1.02: August 30, 2020
 * * Bug Fixes!
 * ** <Diagonal Movement: Off> notetag now works properly. Fix made by Yanfly.
 * ** Plugin Command "Event Label: Visible" now works properly. Fix made by
 *    Shaz.
 * ** Custom Move Route commands should now be working properly. Fix made by
 *    Shaz.
 * 
 * Version 1.01: August 23, 2020
 * * Bug Fixes!
 * ** Event Cache issues fixed upon loading a saved game. Fix made by Yanfly.
 *
 * Version 1.00: August 20, 2020
 * * Finished Plugin!
 *
 * ============================================================================
 * End of Helpfile
 * ============================================================================
 *
 * @ --------------------------------------------------------------------------
 *
 * @command AutoMoveEvents
 * @text Auto Movement: Events
 * @desc Allow/stop events from auto movement.
 *
 * @arg Value:str
 * @text Value
 * @type select
 * @option Allow
 * @value Allow
 * @option Stop
 * @value Stop
 * @option Toggle
 * @value Toggle
 * @desc Allow events to move automatically?
 * @default Allow
 *
 * @ --------------------------------------------------------------------------
 *
 * @command CallEvent
 * @text Call Event: Remote Activation
 * @desc Runs the page of a different event remotely.
 *
 * @arg MapId:eval
 * @text Map ID
 * @desc Target event's map. Use 0 for current map.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg EventId:eval
 * @text Event ID
 * @desc The ID of the event to remotely run. Use 0 for current event. You may use JavaScript code.
 * @default 0
 *
 * @arg PageId:eval
 * @text Page ID
 * @desc The page of the remote event to run.
 * You may use JavaScript code.
 * @default 1
 *
 * @ --------------------------------------------------------------------------
 *
 * @command DashEnableToggle
 * @text Dash Enable: Toggle
 * @desc Enable/Disable Dashing on maps.
 *
 * @arg Value:str
 * @text Value
 * @type select
 * @option Enable
 * @value Enable
 * @option Disable
 * @value Disable
 * @option Toggle
 * @value Toggle
 * @desc What do you wish to change dashing to?
 * @default Enable
 *
 * @ --------------------------------------------------------------------------
 *
 * @command EventIconChange
 * @text Event Icon: Change
 * @desc Change the icon that appears on an event.
 *
 * @arg MapId:eval
 * @text Map ID
 * @desc The map the target map. Use 0 for current map.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg EventId:eval
 * @text Event ID
 * @parent MapId:eval
 * @desc The ID of the target event.  Use 0 for current event.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg IconIndex:eval
 * @text Icon Index
 * @desc Icon index used for the icon.
 * You may use JavaScript code.
 * @default 1
 *
 * @arg IconBufferX:eval
 * @text Buffer X
 * @parent IconIndex:eval
 * @desc How much to shift the X position by?
 * You may use JavaScript code.
 * @default 0
 *
 * @arg IconBufferY:eval
 * @text Buffer Y
 * @parent IconIndex:eval
 * @desc How much to shift the Y position by?
 * You may use JavaScript code.
 * @default 12
 *
 * @arg IconBlendMode:num
 * @text Blend Mode
 * @parent IconIndex:eval
 * @type select
 * @option 0 - Normal
 * @value 0
 * @option 1 - Additive
 * @value 1
 * @option 2 - Multiply
 * @value 2
 * @option 3 - Screen
 * @value 3
 * @desc What kind of blend mode do you wish to apply to the icon sprite?
 * @default 0
 *
 * @ --------------------------------------------------------------------------
 *
 * @command EventIconDelete
 * @text Event Icon: Delete
 * @desc Delete the icon that appears on an event.
 *
 * @arg MapId:eval
 * @text Map ID
 * @desc The map the target map. Use 0 for current map.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg EventId:eval
 * @text Event ID
 * @parent MapId:eval
 * @desc The ID of the target event. Use 0 for current event.
 * You may use JavaScript code.
 * @default 0
 *
 * @ --------------------------------------------------------------------------
 *
 * @command EventLabelRefresh
 * @text Event Label: Refresh
 * @desc Refresh all Event Labels on screen.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command EventLabelVisible
 * @text Event Label: Visible
 * @desc Change the visibility of Event Labels.
 *
 * @arg Visibility:str
 * @text Visibility
 * @type select
 * @option Visible
 * @value Visible
 * @option Hidden
 * @value Hidden
 * @option Toggle
 * @value Toggle
 * @desc What do you wish to change visibility to?
 * @default Visible
 *
 * @ --------------------------------------------------------------------------
 *
 * @command EventLocationSave
 * @text Event Location: Save
 * @desc Memorize an event's map location so it reappears there
 * the next time the map is loaded.
 *
 * @arg EventId:eval
 * @text Event ID
 * @desc The ID of the target event.
 * You may use JavaScript code.
 * @default 1
 *
 * @ --------------------------------------------------------------------------
 *
 * @command EventLocationCreate
 * @text Event Location: Create
 * @desc Creates a custom spawn location for a specific map's event
 * so it appears there the next time the map is loaded.
 *
 * @arg MapId:eval
 * @text Map ID
 * @desc The map the target map. Use 0 for current map.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg EventId:eval
 * @text Event ID
 * @parent MapId:eval
 * @desc The ID of the target event. Use 0 for current event.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg PosX:eval
 * @text X Coordinate
 * @parent MapId:eval
 * @desc The X coordinate of the event.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg PosY:eval
 * @text Y Coordinate
 * @parent MapId:eval
 * @desc The Y coordinate of the event.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg Direction:num
 * @text Direction
 * @parent MapId:eval
 * @type select
 * @option 1 - Lower Left
 * @value 1
 * @option 2 - Down
 * @value 2
 * @option 3 - Lower Right
 * @value 3
 * @option 4 - Left
 * @value 4
 * @option 6 - Right
 * @value 6
 * @option 7 - Upper Left
 * @value 7
 * @option 8 - Up
 * @value 8
 * @option 9 - Upper Right
 * @value 9
 * @desc The direction the event will be facing.
 * @default 2
 *
 * @arg Optional
 *
 * @arg PageId:eval
 * @text Page ID
 * @parent Optional
 * @desc The page of the event to set the move route to.
 * You may use JavaScript code.
 * @default 1
 *
 * @arg MoveRouteIndex:eval
 * @text Move Route Index
 * @parent Optional
 * @desc The point in the move route for this event to be at
 * if the page ID matches the rest of the page conditions.
 * @default 0
 *
 * @ --------------------------------------------------------------------------
 *
 * @command EventLocationDelete
 * @text Event Location: Delete
 * @desc Deletes an event's saved map location.
 * The event will reappear at its default location.
 *
 * @arg MapId:eval
 * @text Map ID
 * @desc The map the target map. Use 0 for current map.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg EventId:eval
 * @text Event ID
 * @desc The ID of the target event. Use 0 for current event.
 * You may use JavaScript code.
 * @default 0
 *
 * @ --------------------------------------------------------------------------
 *
 * @command EventTimerExpireEvent
 * @text Event Timer: Expire Event Assign
 * @desc Sets a Common Event to run upon expiration.
 * Bypasses the default code if one is set.
 *
 * @arg CommonEventID:num
 * @text Common Event ID
 * @type common_event
 * @desc Select the Common Event to run upon the timer's expiration.
 * @default 1
 *
 * @ --------------------------------------------------------------------------
 *
 * @command EventTimerSpeed
 * @text Event Timer: Change Speed
 * @desc Changes the timer frame decrease (or increase) speed.
 *
 * @arg Speed:eval
 * @text Speed
 * @desc How many 1/60ths of a second does each frame increase or
 * decrease by? Negative decreases. Positive increases.
 * @default -1
 *
 * @ --------------------------------------------------------------------------
 *
 * @command EventTimerExpireClear
 * @text Event Timer: Expire Event Clear
 * @desc Clears any set to expire Common Event and instead,
 * run the default Game_Timer expiration code.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command EventTimerFramesGain
 * @text Event Timer: Frames Gain
 * @desc Chooses how many frames, seconds, minutes, or hours
 * are gained or lost for the event timer.
 *
 * @arg Frames:eval
 * @text Frames
 * @desc How many 1/60ths of a second are gained/lost?
 * Positive for gain. Negative for lost. JavaScript allowed.
 * @default +0
 *
 * @arg Seconds:eval
 * @text Seconds
 * @desc How many seconds are gained/lost?
 * Positive for gain. Negative for lost. JavaScript allowed.
 * @default +0
 *
 * @arg Minutes:eval
 * @text Minutes
 * @desc How many minutes are gained/lost?
 * Positive for gain. Negative for lost. JavaScript allowed.
 * @default +0
 *
 * @arg Hours:eval
 * @text Hours
 * @desc How many hours are gained/lost?
 * Positive for gain. Negative for lost. JavaScript allowed.
 * @default +0
 *
 * @ --------------------------------------------------------------------------
 *
 * @command EventTimerFramesSet
 * @text Event Timer: Frames Set
 * @desc Chooses how many frames, seconds, minutes, or hours
 * are set for the event timer.
 *
 * @arg Frames:eval
 * @text Frames
 * @desc Set frame count to this value.
 * Each frame is 1/60th of a second. JavaScript allowed.
 * @default 0
 *
 * @arg Seconds:eval
 * @text Seconds
 * @desc Set seconds to this value.
 * JavaScript allowed.
 * @default 0
 *
 * @arg Minutes:eval
 * @text Minutes
 * @desc Set minutes to this value.
 * Each minute is 60 seconds. JavaScript allowed.
 * @default 0
 *
 * @arg Hours:eval
 * @text Hours
 * @desc Set hours to this value.
 * Each hour is 60 minutes. JavaScript allowed.
 * @default 0
 *
 * @ --------------------------------------------------------------------------
 *
 * @command EventTimerPause
 * @text Event Timer: Pause
 * @desc Pauses the current event timer, but does not stop it.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command EventTimerResume
 * @text Event Timer: Resume
 * @desc Resumes the current event timer from the paused state.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command FollowerSetGlobalChase
 * @text Follower: Set Global Chase
 * @desc Disables all followers from chasing the player
 * or reenables it.
 *
 * @arg Chase:eval
 * @text Chase
 * @type boolean
 * @on Chase
 * @off Don't Chase
 * @desc Sets all followers to chase the player or not.
 * @default false
 *
 * @ --------------------------------------------------------------------------
 *
 * @command FollowerSetTargetChase
 * @text Follower: Set Target Chase
 * @desc Disables target follower from chasing the player
 * or reenables it.
 *
 * @arg FollowerID:eval
 * @text Follower ID
 * @desc Select which follower ID to disable/reenable chasing for.
 * @default 1
 *
 * @arg Chase:eval
 * @text Chase
 * @type boolean
 * @on Chase
 * @off Don't Chase
 * @desc Sets target follower to chase its target or not.
 * @default false
 *
 * @ --------------------------------------------------------------------------
 *
 * @command FollowerSetControl
 * @text Follower: Set Control
 * @desc Sets the event commands to target a follower when "Player"
 * is selected as the target.
 *
 * @arg FollowerID:eval
 * @text Follower ID
 * @desc Select which follower ID to control.
 * 0 is the player.
 * @default 1
 *
 * @ --------------------------------------------------------------------------
 *
 * @command FollowerReset
 * @text Follower: Reset
 * @desc Resets all follower controls. Event Commands that target
 * the "Player" return to normal and followers chase again.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SwitchGetSelfSwitchABCD
 * @text Global Switch: Get Self Switch A B C D
 * @desc Gets the current ON/OFF value from a Self Switch and
 * stores it onto a Global Switch.
 *
 * @arg MapId:eval
 * @text Map ID
 * @desc The map the source map. Use 0 for current map.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg EventId:eval
 * @text Event ID
 * @desc The ID of the source event. Use 0 for current event.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg Letter:str
 * @text Letter
 * @type select
 * @option A
 * @value A
 * @option B
 * @value B
 * @option C
 * @value C
 * @option D
 * @value D
 * @desc Letter of the target event's Self Switch to obtain data from.
 * @default A
 *
 * @arg Break
 * @text -
 *
 * @arg TargetSwitchId:num
 * @text Target Switch ID
 * @type switch
 * @desc The ID of the target switch.
 * @default 1
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SwitchGetSelfSwitchID
 * @text Global Switch: Get Self Switch ID
 * @desc Gets the current ON/OFF value from a Self Switch and
 * stores it onto a Global Switch.
 *
 * @arg MapId:eval
 * @text Map ID
 * @desc The map the source map. Use 0 for current map.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg EventId:eval
 * @text Event ID
 * @desc The ID of the source event. Use 0 for current event.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg SwitchId:num
 * @text Switch ID
 * @type switch
 * @desc The ID of the source switch.
 * @default 1
 *
 * @arg Break
 * @text -
 *
 * @arg TargetSwitchId:num
 * @text Target Switch ID
 * @type switch
 * @desc The ID of the target switch.
 * @default 1
 *
 * @ --------------------------------------------------------------------------
 *
 * @command VariableGetSelfVariableID
 * @text Global Variable: Get Self Variable ID
 * @desc Gets the current stored value from a Self Variable and
 * stores it onto a Global Variable.
 *
 * @arg MapId:eval
 * @text Map ID
 * @desc The map the source map. Use 0 for current map.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg EventId:eval
 * @text Event ID
 * @desc The ID of the source event. Use 0 for current event.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg VariableId:num
 * @text Variable ID
 * @type variable
 * @desc The ID of the source variable.
 * @default 1
 *
 * @arg Break
 * @text -
 *
 * @arg TargetVariableId:num
 * @text Target Variable ID
 * @type variable
 * @desc The ID of the target variable.
 * @default 1
 *
 * @ --------------------------------------------------------------------------
 *
 * @command MorphEventTo
 * @text Morph Event: Change
 * @desc Runs the page of a different event remotely.
 *
 * @arg Step1
 * @text Step 1: To Be Changed
 *
 * @arg Step1MapId:eval
 * @text Map ID
 * @parent Step1
 * @desc Target event's map. Use 0 for current map.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg Step1EventId:eval
 * @text Event ID
 * @parent Step1
 * @desc The ID of the target event. Use 0 for current event.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg Step2
 * @text Step 2: Change Into
 *
 * @arg TemplateName:str
 * @text Template Name
 * @parent Step2
 * @desc Name of the target event template to morph into.
 * Ignored if this is called "Untitled".
 * @default Untitled
 *
 * @arg Step2MapId:eval
 * @text Map ID
 * @parent Step2
 * @desc Target event's map. Use 0 for current map.
 * You may use JavaScript code.
 * @default 1
 *
 * @arg Step2EventId:eval
 * @text Event ID
 * @parent Step2
 * @desc The ID of the target event. Use 0 for current event.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg Step2Preserve:eval
 * @text Preserve Morph
 * @parent Step2
 * @type boolean
 * @on Preserve
 * @off Expires
 * @desc Is the morph effect preserved?
 * Or does it expire upon leaving the map?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command MorphEventRemove
 * @text Morph Event: Remove
 * @desc Remove the morph status of an event.
 *
 * @arg MapId:eval
 * @text Map ID
 * @parent Step1
 * @desc Target event's map. Use 0 for current map.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg EventId:eval
 * @text Event ID
 * @parent Step1
 * @desc The ID of the event to remove morph from. Use 0 for current event. You may use JavaScript code.
 * @default 0
 *
 * @arg RemovePreserve:eval
 * @text Remove Preservation
 * @parent Step2
 * @type boolean
 * @on Remove
 * @off Contain
 * @desc Also remove the preservation effect?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command PlayerIconChange
 * @text Player Icon: Change
 * @desc Change the icon that appears on on the player.
 *
 * @arg IconIndex:eval
 * @text Icon Index
 * @desc Icon index used for the icon.
 * You may use JavaScript code.
 * @default 1
 *
 * @arg IconBufferX:eval
 * @text Buffer X
 * @parent IconIndex:eval
 * @desc How much to shift the X position by?
 * You may use JavaScript code.
 * @default 0
 *
 * @arg IconBufferY:eval
 * @text Buffer Y
 * @parent IconIndex:eval
 * @desc How much to shift the Y position by?
 * You may use JavaScript code.
 * @default 12
 *
 * @arg IconBlendMode:num
 * @text Blend Mode
 * @parent IconIndex:eval
 * @type select
 * @option 0 - Normal
 * @value 0
 * @option 1 - Additive
 * @value 1
 * @option 2 - Multiply
 * @value 2
 * @option 3 - Screen
 * @value 3
 * @desc What kind of blend mode do you wish to apply to the icon sprite?
 * @default 0
 *
 * @ --------------------------------------------------------------------------
 *
 * @command PlayerIconDelete
 * @text Player Icon: Delete
 * @desc Delete the icon that appears on the player.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command PlayerMovementChange
 * @text Player Movement: Control
 * @desc Enable or disable player control over the player character's movement.
 *
 * @arg Enable:eval
 * @text Enable?
 * @type boolean
 * @on Enable
 * @off Disable
 * @desc Let the player control where the player character moves?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command PlayerMovementDiagonal
 * @text Player Movement: Diagonal
 * @desc Override settings to for player diagonal movement.
 *
 * @arg Setting:str
 * @text Setting
 * @type select
 * @option Default: Whatever the Map Uses
 * @value default
 * @option Forcefully Disable Diagonal Movement
 * @value disable
 * @option Forcefully Enable Diagonal Movement
 * @value enable
 * @desc How do you want to change diagonal movement?
 * @default default
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SelfSwitchABCD
 * @text Self Switch: A B C D
 * @desc Change the Self Switch of a different event.
 *
 * @arg MapId:eval
 * @text Map ID
 * @desc The map the target map. Use 0 for current map.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg EventId:eval
 * @text Event ID
 * @desc The ID of the target event. Use 0 for current event.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg Letter:str
 * @text Letter
 * @type select
 * @option A
 * @value A
 * @option B
 * @value B
 * @option C
 * @value C
 * @option D
 * @value D
 * @desc Letter of the target event's Self Switch to change.
 * @default A
 *
 * @arg Break
 * @text -
 *
 * @arg Value:str
 * @text Value
 * @type select
 * @option ON
 * @value ON
 * @option OFF
 * @value OFF
 * @option Toggle
 * @value Toggle
 * @desc What value do you want to set the Self Switch to?
 * @default ON
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SelfSwitchID
 * @text Self Switch: Switch ID
 * @desc Change the Self Switch of a different event.
 *
 * @arg MapId:eval
 * @text Map ID
 * @desc The map the target map. Use 0 for current map.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg EventId:eval
 * @text Event ID
 * @desc The ID of the target event. Use 0 for current event.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg SwitchId:num
 * @text Switch ID
 * @type switch
 * @desc The ID of the target switch.
 * @default 1
 *
 * @arg Break
 * @text -
 *
 * @arg Value:str
 * @text Value
 * @type select
 * @option ON
 * @value ON
 * @option OFF
 * @value OFF
 * @option Toggle
 * @value Toggle
 * @desc What value do you want to set the Self Switch to?
 * @default ON
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SelfVariableID
 * @text Self Variable: Variable ID
 * @desc Change the Self Variable of a different event.
 *
 * @arg MapId:eval
 * @text Map ID
 * @desc The map the target map. Use 0 for current map.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg EventId:eval
 * @text Event ID
 * @desc The ID of the target event. Use 0 for current event.
 * You may use JavaScript code.
 * @default 0
 *
 * @arg VariableId:num
 * @text Variable ID
 * @type variable
 * @desc The ID of the target variable.
 * @default 1
 *
 * @arg Break
 * @text -
 *
 * @arg Operation:str
 * @text Operation
 * @type select
 * @option = Set
 * @value =
 * @option + Add
 * @value +
 * @option - Subtract
 * @value -
 * @option * Multiply
 * @value *
 * @option / Divide
 * @value /
 * @option % Modulus
 * @value %
 * @desc Set the operation used.
 * @default =
 *
 * @arg Break2
 * @text -
 *
 * @arg Value:eval
 * @text Value
 * @desc Insert the value to modify the Self Variable by.
 * You may use JavaScript code.
 * @default 0
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SpawnEventAtXY
 * @text Spawn Event: Spawn At X, Y
 * @desc Spawns desired event at X, Y location on the current map.
 *
 * @arg Step1
 * @text Step 1: Spawned Event
 *
 * @arg TemplateName:str
 * @text Template Name
 * @parent Step1
 * @desc Name of the target event template to spawn as.
 * Ignored if this is called "Untitled".
 * @default Untitled
 *
 * @arg MapId:eval
 * @text Map ID
 * @parent Step1
 * @desc Target event's map to be used as reference.
 * You may use JavaScript code.
 * @default 1
 *
 * @arg EventId:eval
 * @text Event ID
 * @parent Step1
 * @desc The ID of the target event to be used as reference.
 * You may use JavaScript code.
 * @default 1
 *
 * @arg Step2
 * @text Step 2: Location
 *
 * @arg PosX:eval
 * @text X Coordinate
 * @parent Step2
 * @type combo
 * @option $gamePlayer.frontX()
 * @option $gamePlayer.backX()
 * @option Math.randomInt($gameMap.width())
 * @option 0
 * @desc Target Location to spawn at.
 * You may use JavaScript code.
 * @default $gamePlayer.frontX()
 *
 * @arg PosY:eval
 * @text Y Coordinate
 * @parent Step2
 * @type combo
 * @option $gamePlayer.frontY()
 * @option $gamePlayer.backY()
 * @option Math.randomInt($gameMap.height())
 * @option 0
 * @desc Target Location to spawn at.
 * You may use JavaScript code.
 * @default $gamePlayer.frontY()
 *
 * @arg Collision:eval
 * @text Check Event Collision
 * @parent Step2
 * @type boolean
 * @on Check
 * @off Ignore
 * @desc Check collision with any other events and player?
 * @default true
 *
 * @arg Passability:eval
 * @text Check Passability
 * @parent Step2
 * @type boolean
 * @on Check
 * @off Ignore
 * @desc Check passability of the target location.
 * @default true
 *
 * @arg Preserve:eval
 * @text Preserve Spawn
 * @parent Step2
 * @type boolean
 * @on Preserve
 * @off Expires
 * @desc Is the spawned event preserved?
 * Or does it expire upon leaving the map?
 * @default true
 *
 * @arg Step3
 * @text Step 3: Success Check
 *
 * @arg SuccessSwitchId:num
 * @text Success Switch ID
 * @parent Step3
 * @type switch
 * @desc Target switch ID to record spawning success.
 * Ignore if ID is 0. OFF means failed. ON means success.
 * @default 0
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SpawnEventAtRegion
 * @text Spawn Event: Spawn At Region
 * @desc Spawns desired event at a random region-marked location on the current map.
 *
 * @arg Step1
 * @text Step 1: Spawned Event
 *
 * @arg TemplateName:str
 * @text Template Name
 * @parent Step1
 * @desc Name of the target event template to spawn as.
 * Ignored if this is called "Untitled".
 * @default Untitled
 *
 * @arg MapId:eval
 * @text Map ID
 * @parent Step1
 * @desc Target event's map.
 * You may use JavaScript code.
 * @default 1
 *
 * @arg EventId:eval
 * @text Event ID
 * @parent Step1
 * @desc The ID of the target event.
 * You may use JavaScript code.
 * @default 1
 *
 * @arg Step2
 * @text Step 2: Location
 *
 * @arg Region:arraynum
 * @text Region ID(s)
 * @parent Step2
 * @type number[]
 * @min 0
 * @max 255
 * @desc Pick region(s) to spawn this event at.
 * @default ["1"]
 *
 * @arg Collision:eval
 * @text Check Event Collision
 * @parent Step2
 * @type boolean
 * @on Check
 * @off Ignore
 * @desc Check collision with any other events and player?
 * @default true
 *
 * @arg Passability:eval
 * @text Check Passability
 * @parent Step2
 * @type boolean
 * @on Check
 * @off Ignore
 * @desc Check passability of the target location.
 * @default true
 *
 * @arg Preserve:eval
 * @text Preserve Spawn
 * @parent Step2
 * @type boolean
 * @on Preserve
 * @off Expires
 * @desc Is the spawned event preserved?
 * Or does it expire upon leaving the map?
 * @default true
 *
 * @arg Step3
 * @text Step 3: Success Check
 *
 * @arg SuccessSwitchId:num
 * @text Success Switch ID
 * @parent Step3
 * @type switch
 * @desc Target switch ID to record spawning success.
 * Ignore if ID is 0. OFF means failed. ON means success.
 * @default 0
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SpawnEventAtTerrainTag
 * @text Spawn Event: Spawn At Terrain Tag
 * @desc Spawns desired event at a random terrain tag-marked location on the current map.
 *
 * @arg Step1
 * @text Step 1: Spawned Event
 *
 * @arg TemplateName:str
 * @text Template Name
 * @parent Step1
 * @desc Name of the target event template to spawn as.
 * Ignored if this is called "Untitled".
 * @default Untitled
 *
 * @arg MapId:eval
 * @text Map ID
 * @parent Step1
 * @desc Target event's map.
 * You may use JavaScript code.
 * @default 1
 *
 * @arg EventId:eval
 * @text Event ID
 * @parent Step1
 * @desc The ID of the target event.
 * You may use JavaScript code.
 * @default 1
 *
 * @arg Step2
 * @text Step 2: Location
 *
 * @arg TerrainTags:arraynum
 * @text Terrain Tag(s)
 * @parent Step2
 * @type number[]
 * @min 0
 * @max 7
 * @desc Pick terrain tag(s) to spawn this event at.
 * Insert numbers between 0 and 7.
 * @default ["1"]
 *
 * @arg Collision:eval
 * @text Check Event Collision
 * @parent Step2
 * @type boolean
 * @on Check
 * @off Ignore
 * @desc Check collision with any other events and player?
 * @default true
 *
 * @arg Passability:eval
 * @text Check Passability
 * @parent Step2
 * @type boolean
 * @on Check
 * @off Ignore
 * @desc Check passability of the target location.
 * @default true
 *
 * @arg Preserve:eval
 * @text Preserve Spawn
 * @parent Step2
 * @type boolean
 * @on Preserve
 * @off Expires
 * @desc Is the spawned event preserved?
 * Or does it expire upon leaving the map?
 * @default true
 *
 * @arg Step3
 * @text Step 3: Success Check
 *
 * @arg SuccessSwitchId:num
 * @text Success Switch ID
 * @parent Step3
 * @type switch
 * @desc Target switch ID to record spawning success.
 * Ignore if ID is 0. OFF means failed. ON means success.
 * @default 0
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SpawnEventDespawnEventID
 * @text Spawn Event: Despawn Event ID
 * @desc Despawns the selected Event ID on the current map.
 *
 * @arg EventID:eval
 * @text Event ID
 * @type combo
 * @option $gameMap.firstSpawnedEventID()
 * @option $gameMap.lastSpawnedEventID()
 * @option 1001
 * @desc The ID of the target event.
 * You may use JavaScript code.
 * @default $gameMap.lastSpawnedEventID()
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SpawnEventDespawnAtXY
 * @text Spawn Event: Despawn At X, Y
 * @desc Despawns any spawned event(s) at X, Y location on the current map.
 *
 * @arg PosX:eval
 * @text X Coordinate
 * @parent Step2
 * @type combo
 * @option $gamePlayer.frontX()
 * @option $gamePlayer.backX()
 * @option Math.randomInt($gameMap.width())
 * @option 0
 * @desc Target Location to despawn at.
 * You may use JavaScript code.
 * @default $gamePlayer.frontX()
 *
 * @arg PosY:eval
 * @text Y Coordinate
 * @parent Step2
 * @type combo
 * @option $gamePlayer.frontY()
 * @option $gamePlayer.backY()
 * @option Math.randomInt($gameMap.height())
 * @option 0
 * @desc Target Location to despawn at.
 * You may use JavaScript code.
 * @default $gamePlayer.frontY()
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SpawnEventDespawnRegions
 * @text Spawn Event: Despawn Region(s)
 * @desc Despawns the selected Region(s) on the current map.
 *
 * @arg Region:arraynum
 * @text Region ID(s)
 * @parent Step2
 * @type number[]
 * @min 0
 * @max 255
 * @desc Pick region(s) and despawn everything inside it.
 * @default ["1"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SpawnEventDespawnTerrainTags
 * @text Spawn Event: Despawn Terrain Tag(s)
 * @desc Despawns the selected Terrain Tags(s) on the current map.
 *
 * @arg TerrainTags:arraynum
 * @text Terrain Tag(s)
 * @parent Step2
 * @type number[]
 * @min 0
 * @max 7
 * @desc Pick terrain tag(s) and despawn everything inside it.
 * Insert numbers between 0 and 7.
 * @default ["1"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SpawnEventDespawnEverything
 * @text Spawn Event: Despawn Everything
 * @desc Despawns all spawned events on the current map.
 *
 * @ --------------------------------------------------------------------------
 *
 * @ ==========================================================================
 * @ Plugin Parameters
 * @ ==========================================================================
 *
 * @param BreakHead
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param EventsMoveCore
 * @default Plugin Parameters
 *
 * @param ATTENTION
 * @default READ THE HELP FILE
 *
 * @param BreakSettings
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param Label:struct
 * @text Event Label Settings
 * @type struct<Label>
 * @desc Choose settings regarding the Event Labels.
 * @default {"FontSize:num":"22","IconSize:num":"26","LineHeight:num":"30","OffsetX:num":"0","OffsetY:num":"12","OpacitySpeed:num":"16","VisibleRange:num":"30"}
 *
 * @param Icon:struct
 * @text Event Icon Settings
 * @type struct<Icon>
 * @desc Choose settings regarding the Event Icons.
 * @default {"BufferX:num":"0","BufferY:num":"12","BlendMode:num":"0"}
 *
 * @param Template:struct
 * @text Event Template Settings
 * @type struct<Template>
 * @desc Choose settings regarding Event Templates.
 * @default {"Settings":"","PreloadMaps:arraynum":"[\"1\"]","Prefabs":"","List:arraystruct":"[]","JavaScript":"","PreCopyJS:func":"\"// Declare Constants\\nconst mapId = arguments[1];\\nconst eventId = arguments[2];\\nconst target = arguments[3];\\nconst player = $gamePlayer;\\n\\n// Perform Actions\\n\"","PostCopyJS:func":"\"// Declare Constants\\nconst mapId = arguments[1];\\nconst eventId = arguments[2];\\nconst target = arguments[3];\\nconst player = $gamePlayer;\\n\\n// Perform Actions\\n\"","PreMorphJS:func":"\"// Declare Constants\\nconst mapId = arguments[1];\\nconst eventId = arguments[2];\\nconst target = arguments[3];\\nconst player = $gamePlayer;\\n\\n// Perform Actions\\n\"","PostMorphJS:func":"\"// Declare Constants\\nconst mapId = arguments[1];\\nconst eventId = arguments[2];\\nconst target = arguments[3];\\nconst player = $gamePlayer;\\n\\n// Perform Actions\\n\"","PreSpawnJS:func":"\"// Declare Constants\\nconst mapId = arguments[1];\\nconst eventId = arguments[2];\\nconst target = arguments[3];\\nconst player = $gamePlayer;\\n\\n// Perform Actions\\n\"","PostSpawnJS:func":"\"// Declare Constants\\nconst mapId = arguments[1];\\nconst eventId = arguments[2];\\nconst target = arguments[3];\\nconst player = $gamePlayer;\\n\\n// Perform Actions\\n\""}
 *
 * @param EventBreak
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param Movement:struct
 * @text Movement Settings
 * @type struct<Movement>
 * @desc Change the rules regarding movement in the game.
 * @default {"Dir8":"","EnableDir8:eval":"true","StrictCollision:eval":"true","FavorHorz:eval":"true","SlowerSpeed:eval":"false","DiagonalSpeedMultiplier:num":"0.85","AutoMove":"","StopAutoMoveEvents:eval":"true","StopAutoMoveMessages:eval":"true","Bitmap":"","BitmapSmoothing:eval":"false","Dash":"","DashModifier:num":"+1.0","EnableDashTilt:eval":"true","TiltLeft:num":"-0.15","TiltRight:num":"0.15","TiltVert:num":"0.05","EventMove":"","RandomMoveWeight:num":"0.10","Shadows":"","ShowShadows:eval":"true","DefaultShadow:str":"Shadow1","TurnInPlace":"","EnableTurnInPlace:eval":"false","TurnInPlaceDelay:num":"10","Vehicle":"","BoatSpeed:num":"4.0","ShipSpeed:num":"5.0","AirshipSpeed:num":"6.0"}
 *
 * @param VS8:struct
 * @text VisuStella 8-Dir Settings
 * @type struct<VS8>
 * @desc Choose settings regarding VisuStella 8-Directional Sprites.
 * @default {"Balloons":"","AutoBalloon:eval":"true","BalloonOffsetX:num":"0","BalloonOffsetY:num":"12","Icons":"","AutoBuffer:eval":"true","CarryPose:eval":"true"}
 *
 * @param MovementBreak
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param Region:struct
 * @text Region Rulings
 * @type struct<Region>
 * @desc Choose settings regarding regions.
 * @default {"Allow":"","AllAllow:arraynum":"[]","WalkAllow:arraynum":"[]","PlayerAllow:arraynum":"[]","EventAllow:arraynum":"[]","VehicleAllow:arraynum":"[]","BoatAllow:arraynum":"[]","ShipAllow:arraynum":"[]","AirshipAllow:arraynum":"[]","Forbid":"","AllForbid:arraynum":"[]","WalkForbid:arraynum":"[]","PlayerForbid:arraynum":"[]","EventForbid:arraynum":"[]","VehicleForbid:arraynum":"[]","BoatForbid:arraynum":"[]","ShipForbid:arraynum":"[]","AirshipForbid:arraynum":"[]","Dock":"","VehicleDock:arraynum":"[]","BoatDock:arraynum":"[]","BoatDockRegionOnly:eval":"false","ShipDock:arraynum":"[]","ShipDockRegionOnly:eval":"false","AirshipDock:arraynum":"[]","AirshipDockRegionOnly:eval":"false"}
 *
 * @param RegionOk:struct
 * @text Common Event on OK Button
 * @parent Region:struct
 * @type struct<RegionCommonEvent>
 * @desc Set Common Events that activate upon pressing the
 * OK button while standing on top of designated regions.
 * @default {"Region1:num":"0","Region2:num":"0","Region3:num":"0","Region4:num":"0","Region5:num":"0","Region6:num":"0","Region7:num":"0","Region8:num":"0","Region9:num":"0","Region10:num":"0","Region11:num":"0","Region12:num":"0","Region13:num":"0","Region14:num":"0","Region15:num":"0","Region16:num":"0","Region17:num":"0","Region18:num":"0","Region19:num":"0","Region20:num":"0","Region21:num":"0","Region22:num":"0","Region23:num":"0","Region24:num":"0","Region25:num":"0","Region26:num":"0","Region27:num":"0","Region28:num":"0","Region29:num":"0","Region30:num":"0","Region31:num":"0","Region32:num":"0","Region33:num":"0","Region34:num":"0","Region35:num":"0","Region36:num":"0","Region37:num":"0","Region38:num":"0","Region39:num":"0","Region40:num":"0","Region41:num":"0","Region42:num":"0","Region43:num":"0","Region44:num":"0","Region45:num":"0","Region46:num":"0","Region47:num":"0","Region48:num":"0","Region49:num":"0","Region50:num":"0","Region51:num":"0","Region52:num":"0","Region53:num":"0","Region54:num":"0","Region55:num":"0","Region56:num":"0","Region57:num":"0","Region58:num":"0","Region59:num":"0","Region60:num":"0","Region61:num":"0","Region62:num":"0","Region63:num":"0","Region64:num":"0","Region65:num":"0","Region66:num":"0","Region67:num":"0","Region68:num":"0","Region69:num":"0","Region70:num":"0","Region71:num":"0","Region72:num":"0","Region73:num":"0","Region74:num":"0","Region75:num":"0","Region76:num":"0","Region77:num":"0","Region78:num":"0","Region79:num":"0","Region80:num":"0","Region81:num":"0","Region82:num":"0","Region83:num":"0","Region84:num":"0","Region85:num":"0","Region86:num":"0","Region87:num":"0","Region88:num":"0","Region89:num":"0","Region90:num":"0","Region91:num":"0","Region92:num":"0","Region93:num":"0","Region94:num":"0","Region95:num":"0","Region96:num":"0","Region97:num":"0","Region98:num":"0","Region99:num":"0","Region100:num":"0","Region101:num":"0","Region102:num":"0","Region103:num":"0","Region104:num":"0","Region105:num":"0","Region106:num":"0","Region107:num":"0","Region108:num":"0","Region109:num":"0","Region110:num":"0","Region111:num":"0","Region112:num":"0","Region113:num":"0","Region114:num":"0","Region115:num":"0","Region116:num":"0","Region117:num":"0","Region118:num":"0","Region119:num":"0","Region120:num":"0","Region121:num":"0","Region122:num":"0","Region123:num":"0","Region124:num":"0","Region125:num":"0","Region126:num":"0","Region127:num":"0","Region128:num":"0","Region129:num":"0","Region130:num":"0","Region131:num":"0","Region132:num":"0","Region133:num":"0","Region134:num":"0","Region135:num":"0","Region136:num":"0","Region137:num":"0","Region138:num":"0","Region139:num":"0","Region140:num":"0","Region141:num":"0","Region142:num":"0","Region143:num":"0","Region144:num":"0","Region145:num":"0","Region146:num":"0","Region147:num":"0","Region148:num":"0","Region149:num":"0","Region150:num":"0","Region151:num":"0","Region152:num":"0","Region153:num":"0","Region154:num":"0","Region155:num":"0","Region156:num":"0","Region157:num":"0","Region158:num":"0","Region159:num":"0","Region160:num":"0","Region161:num":"0","Region162:num":"0","Region163:num":"0","Region164:num":"0","Region165:num":"0","Region166:num":"0","Region167:num":"0","Region168:num":"0","Region169:num":"0","Region170:num":"0","Region171:num":"0","Region172:num":"0","Region173:num":"0","Region174:num":"0","Region175:num":"0","Region176:num":"0","Region177:num":"0","Region178:num":"0","Region179:num":"0","Region180:num":"0","Region181:num":"0","Region182:num":"0","Region183:num":"0","Region184:num":"0","Region185:num":"0","Region186:num":"0","Region187:num":"0","Region188:num":"0","Region189:num":"0","Region190:num":"0","Region191:num":"0","Region192:num":"0","Region193:num":"0","Region194:num":"0","Region195:num":"0","Region196:num":"0","Region197:num":"0","Region198:num":"0","Region199:num":"0","Region200:num":"0","Region201:num":"0","Region202:num":"0","Region203:num":"0","Region204:num":"0","Region205:num":"0","Region206:num":"0","Region207:num":"0","Region208:num":"0","Region209:num":"0","Region210:num":"0","Region211:num":"0","Region212:num":"0","Region213:num":"0","Region214:num":"0","Region215:num":"0","Region216:num":"0","Region217:num":"0","Region218:num":"0","Region219:num":"0","Region220:num":"0","Region221:num":"0","Region222:num":"0","Region223:num":"0","Region224:num":"0","Region225:num":"0","Region226:num":"0","Region227:num":"0","Region228:num":"0","Region229:num":"0","Region230:num":"0","Region231:num":"0","Region232:num":"0","Region233:num":"0","Region234:num":"0","Region235:num":"0","Region236:num":"0","Region237:num":"0","Region238:num":"0","Region239:num":"0","Region240:num":"0","Region241:num":"0","Region242:num":"0","Region243:num":"0","Region244:num":"0","Region245:num":"0","Region246:num":"0","Region247:num":"0","Region248:num":"0","Region249:num":"0","Region250:num":"0","Region251:num":"0","Region252:num":"0","Region253:num":"0","Region254:num":"0","Region255:num":"0"}
 *
 * @param RegionOkTarget:str
 * @text Target Tile
 * @parent RegionOk:struct
 * @type select
 * @option Tile in front of player.
 * @value front
 * @option Tile player is standing on top of.
 * @value standing
 * @desc Which tile should be checked for
 * Common Event on OK Button?
 * @default front
 *
 * @param RegionTouch:struct
 * @text Common Event on Touch
 * @parent Region:struct
 * @type struct<RegionCommonEvent>
 * @desc Set Common Events that activate upon stepping the tiles
 * marked by the designated regions.
 * @default {"Region1:num":"0","Region2:num":"0","Region3:num":"0","Region4:num":"0","Region5:num":"0","Region6:num":"0","Region7:num":"0","Region8:num":"0","Region9:num":"0","Region10:num":"0","Region11:num":"0","Region12:num":"0","Region13:num":"0","Region14:num":"0","Region15:num":"0","Region16:num":"0","Region17:num":"0","Region18:num":"0","Region19:num":"0","Region20:num":"0","Region21:num":"0","Region22:num":"0","Region23:num":"0","Region24:num":"0","Region25:num":"0","Region26:num":"0","Region27:num":"0","Region28:num":"0","Region29:num":"0","Region30:num":"0","Region31:num":"0","Region32:num":"0","Region33:num":"0","Region34:num":"0","Region35:num":"0","Region36:num":"0","Region37:num":"0","Region38:num":"0","Region39:num":"0","Region40:num":"0","Region41:num":"0","Region42:num":"0","Region43:num":"0","Region44:num":"0","Region45:num":"0","Region46:num":"0","Region47:num":"0","Region48:num":"0","Region49:num":"0","Region50:num":"0","Region51:num":"0","Region52:num":"0","Region53:num":"0","Region54:num":"0","Region55:num":"0","Region56:num":"0","Region57:num":"0","Region58:num":"0","Region59:num":"0","Region60:num":"0","Region61:num":"0","Region62:num":"0","Region63:num":"0","Region64:num":"0","Region65:num":"0","Region66:num":"0","Region67:num":"0","Region68:num":"0","Region69:num":"0","Region70:num":"0","Region71:num":"0","Region72:num":"0","Region73:num":"0","Region74:num":"0","Region75:num":"0","Region76:num":"0","Region77:num":"0","Region78:num":"0","Region79:num":"0","Region80:num":"0","Region81:num":"0","Region82:num":"0","Region83:num":"0","Region84:num":"0","Region85:num":"0","Region86:num":"0","Region87:num":"0","Region88:num":"0","Region89:num":"0","Region90:num":"0","Region91:num":"0","Region92:num":"0","Region93:num":"0","Region94:num":"0","Region95:num":"0","Region96:num":"0","Region97:num":"0","Region98:num":"0","Region99:num":"0","Region100:num":"0","Region101:num":"0","Region102:num":"0","Region103:num":"0","Region104:num":"0","Region105:num":"0","Region106:num":"0","Region107:num":"0","Region108:num":"0","Region109:num":"0","Region110:num":"0","Region111:num":"0","Region112:num":"0","Region113:num":"0","Region114:num":"0","Region115:num":"0","Region116:num":"0","Region117:num":"0","Region118:num":"0","Region119:num":"0","Region120:num":"0","Region121:num":"0","Region122:num":"0","Region123:num":"0","Region124:num":"0","Region125:num":"0","Region126:num":"0","Region127:num":"0","Region128:num":"0","Region129:num":"0","Region130:num":"0","Region131:num":"0","Region132:num":"0","Region133:num":"0","Region134:num":"0","Region135:num":"0","Region136:num":"0","Region137:num":"0","Region138:num":"0","Region139:num":"0","Region140:num":"0","Region141:num":"0","Region142:num":"0","Region143:num":"0","Region144:num":"0","Region145:num":"0","Region146:num":"0","Region147:num":"0","Region148:num":"0","Region149:num":"0","Region150:num":"0","Region151:num":"0","Region152:num":"0","Region153:num":"0","Region154:num":"0","Region155:num":"0","Region156:num":"0","Region157:num":"0","Region158:num":"0","Region159:num":"0","Region160:num":"0","Region161:num":"0","Region162:num":"0","Region163:num":"0","Region164:num":"0","Region165:num":"0","Region166:num":"0","Region167:num":"0","Region168:num":"0","Region169:num":"0","Region170:num":"0","Region171:num":"0","Region172:num":"0","Region173:num":"0","Region174:num":"0","Region175:num":"0","Region176:num":"0","Region177:num":"0","Region178:num":"0","Region179:num":"0","Region180:num":"0","Region181:num":"0","Region182:num":"0","Region183:num":"0","Region184:num":"0","Region185:num":"0","Region186:num":"0","Region187:num":"0","Region188:num":"0","Region189:num":"0","Region190:num":"0","Region191:num":"0","Region192:num":"0","Region193:num":"0","Region194:num":"0","Region195:num":"0","Region196:num":"0","Region197:num":"0","Region198:num":"0","Region199:num":"0","Region200:num":"0","Region201:num":"0","Region202:num":"0","Region203:num":"0","Region204:num":"0","Region205:num":"0","Region206:num":"0","Region207:num":"0","Region208:num":"0","Region209:num":"0","Region210:num":"0","Region211:num":"0","Region212:num":"0","Region213:num":"0","Region214:num":"0","Region215:num":"0","Region216:num":"0","Region217:num":"0","Region218:num":"0","Region219:num":"0","Region220:num":"0","Region221:num":"0","Region222:num":"0","Region223:num":"0","Region224:num":"0","Region225:num":"0","Region226:num":"0","Region227:num":"0","Region228:num":"0","Region229:num":"0","Region230:num":"0","Region231:num":"0","Region232:num":"0","Region233:num":"0","Region234:num":"0","Region235:num":"0","Region236:num":"0","Region237:num":"0","Region238:num":"0","Region239:num":"0","Region240:num":"0","Region241:num":"0","Region242:num":"0","Region243:num":"0","Region244:num":"0","Region245:num":"0","Region246:num":"0","Region247:num":"0","Region248:num":"0","Region249:num":"0","Region250:num":"0","Region251:num":"0","Region252:num":"0","Region253:num":"0","Region254:num":"0","Region255:num":"0"}
 *
 * @param TerrainTag:struct
 * @text Terrain Tag Settings
 * @type struct<TerrainTag>
 * @desc Choose settings regarding terrain tags.
 * @default {"TerrainTag":"","Rope:num":"1"}
 *
 * @param BreakEnd1
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param End Of
 * @default Plugin Parameters
 *
 * @param BreakEnd2
 * @text --------------------------
 * @default ----------------------------------
 *
 */
/* ----------------------------------------------------------------------------
 * Label Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Label:
 *
 * @param FontSize:num
 * @text Font Size
 * @type number
 * @min 1
 * @desc The font size used for the Event Labels.
 * @default 22
 *
 * @param IconSize:num
 * @text Icon Size
 * @type number
 * @min 1
 * @desc The size of the icons used in the Event Labels.
 * @default 26
 *
 * @param LineHeight:num
 * @text Line Height
 * @type number
 * @min 1
 * @desc The line height used for the Event Labels.
 * @default 26
 *
 * @param OffsetX:num
 * @text Offset X
 * @type number
 * @min 0
 * @desc Globally offset all labels horizontally by this amount.
 * @default 0
 *
 * @param OffsetY:num
 * @text Offset Y
 * @type number
 * @min 0
 * @desc Globally offset all labels vertically by this amount.
 * @default 12
 *
 * @param OpacitySpeed:num
 * @text Fade Speed
 * @type number
 * @min 1
 * @desc Fade speed for labels.
 * @default 16
 *
 * @param VisibleRange:num
 * @text Visible Range
 * @type number
 * @min 1
 * @desc Range the player has to be within the event to make its label visible.
 * @default 30
 *
 */
/* ----------------------------------------------------------------------------
 * Icon Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Icon:
 *
 * @param BufferX:num
 * @text Buffer X
 * @desc Default X position buffer for event icons.
 * @default 0
 *
 * @param BufferY:num
 * @text Buffer Y
 * @desc Default Y position buffer for event icons.
 * @default 12
 *
 * @param BlendMode:num
 * @text Blend Mode
 * @type select
 * @option 0 - Normal
 * @value 0
 * @option 1 - Additive
 * @value 1
 * @option 2 - Multiply
 * @value 2
 * @option 3 - Screen
 * @value 3
 * @desc Default blend mode for even icons.
 * @default 0
 *
 */
/* ----------------------------------------------------------------------------
 * Template Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Template:
 *
 * @param Settings
 *
 * @param PreloadMaps:arraynum
 * @text Preloaded Maps
 * @parent Settings
 * @type number[]
 * @desc A list of all the ID's of the maps that will be preloaded
 * to serve as template maps for this plugin.
 * @default ["1"]
 *
 * @param Templates
 *
 * @param List:arraystruct
 * @text Event Template List
 * @parent Templates
 * @type struct<EventTemplate>[]
 * @desc A list of all the Event Templates used by this project.
 * Used for notetags and Plugin Commands.
 * @default []
 *
 * @param JavaScript
 *
 * @param PreCopyJS:func
 * @text JS: Pre-Copy
 * @parent JavaScript
 * @type note
 * @desc Code that's ran before an event is copied.
 * This is global and is ran for all copied events.
 * @default "// Declare Constants\nconst mapId = arguments[1];\nconst eventId = arguments[2];\nconst target = arguments[3];\nconst player = $gamePlayer;\n\n// Perform Actions\n"
 *
 * @param PostCopyJS:func
 * @text JS: Post-Copy
 * @parent JavaScript
 * @type note
 * @desc Code that's ran after an event is copied.
 * This is global and is ran for all copied events.
 * @default "// Declare Constants\nconst mapId = arguments[1];\nconst eventId = arguments[2];\nconst target = arguments[3];\nconst player = $gamePlayer;\n\n// Perform Actions\n"
 *
 * @param PreMorphJS:func
 * @text JS: Pre-Morph
 * @parent JavaScript
 * @type note
 * @desc Code that's ran before an event is morphed.
 * This is global and is ran for all morphed events.
 * @default "// Declare Constants\nconst mapId = arguments[1];\nconst eventId = arguments[2];\nconst target = arguments[3];\nconst player = $gamePlayer;\n\n// Perform Actions\n"
 *
 * @param PostMorphJS:func
 * @text JS: Post-Morph
 * @parent JavaScript
 * @type note
 * @desc Code that's ran after an event is morphed.
 * This is global and is ran for all morphed events.
 * @default "// Declare Constants\nconst mapId = arguments[1];\nconst eventId = arguments[2];\nconst target = arguments[3];\nconst player = $gamePlayer;\n\n// Perform Actions\n"
 *
 * @param PreSpawnJS:func
 * @text JS: Pre-Spawn
 * @parent JavaScript
 * @type note
 * @desc Code that's ran before an event is spawned.
 * This is global and is ran for all spawned events.
 * @default "// Declare Constants\nconst mapId = arguments[1];\nconst eventId = arguments[2];\nconst target = arguments[3];\nconst player = $gamePlayer;\n\n// Perform Actions\n"
 *
 * @param PostSpawnJS:func
 * @text JS: Post-Spawn
 * @parent JavaScript
 * @type note
 * @desc Code that's ran after an event is spawned.
 * This is global and is ran for all spawned events.
 * @default "// Declare Constants\nconst mapId = arguments[1];\nconst eventId = arguments[2];\nconst target = arguments[3];\nconst player = $gamePlayer;\n\n// Perform Actions\n"
 *
 */
/* ----------------------------------------------------------------------------
 * Event Template
 * ----------------------------------------------------------------------------
 */
/*~struct~EventTemplate:
 *
 * @param Name:str
 * @text Name
 * @desc Name of the template. It'll be used as anchor points for
 * notetags and Plugin Commands.
 * @default Untitled
 *
 * @param MapID:num
 * @text Map ID
 * @parent Name:str
 * @type number
 * @min 1
 * @max 999
 * @desc ID of the map the template event is stored on.
 * This will automatically add this ID to preloaded list.
 * @default 1
 *
 * @param EventID:num
 * @text Event ID
 * @parent Name:str
 * @type number
 * @min 1
 * @max 999
 * @desc ID of the event the template event is based on.
 * @default 1
 *
 * @param JavaScript
 *
 * @param PreCopyJS:func
 * @text JS: Pre-Copy
 * @parent JavaScript
 * @type note
 * @desc Code that's ran before an event is copied.
 * This is local only for this template.
 * @default "// Declare Constants\nconst mapId = arguments[1];\nconst eventId = arguments[2];\nconst target = arguments[3];\nconst player = $gamePlayer;\n\n// Perform Actions\n"
 *
 * @param PostCopyJS:func
 * @text JS: Post-Copy
 * @parent JavaScript
 * @type note
 * @desc Code that's ran after an event is copied.
 * This is local only for this template.
 * @default "// Declare Constants\nconst mapId = arguments[1];\nconst eventId = arguments[2];\nconst target = arguments[3];\nconst player = $gamePlayer;\n\n// Perform Actions\n"
 *
 * @param PreMorphJS:func
 * @text JS: Pre-Morph
 * @parent JavaScript
 * @type note
 * @desc Code that's ran before an event is morphed.
 * This is local only for this template.
 * @default "// Declare Constants\nconst mapId = arguments[1];\nconst eventId = arguments[2];\nconst target = arguments[3];\nconst player = $gamePlayer;\n\n// Perform Actions\n"
 *
 * @param PostMorphJS:func
 * @text JS: Post-Morph
 * @parent JavaScript
 * @type note
 * @desc Code that's ran after an event is morphed.
 * This is local only for this template.
 * @default "// Declare Constants\nconst mapId = arguments[1];\nconst eventId = arguments[2];\nconst target = arguments[3];\nconst player = $gamePlayer;\n\n// Perform Actions\n"
 *
 * @param PreSpawnJS:func
 * @text JS: Pre-Spawn
 * @parent JavaScript
 * @type note
 * @desc Code that's ran before an event is spawned.
 * This is global and is ran for all spawned events.
 * @default "// Declare Constants\nconst mapId = arguments[1];\nconst eventId = arguments[2];\nconst target = arguments[3];\nconst player = $gamePlayer;\n\n// Perform Actions\n"
 *
 * @param PostSpawnJS:func
 * @text JS: Post-Spawn
 * @parent JavaScript
 * @type note
 * @desc Code that's ran after an event is spawned.
 * This is global and is ran for all spawned events.
 * @default "// Declare Constants\nconst mapId = arguments[1];\nconst eventId = arguments[2];\nconst target = arguments[3];\nconst player = $gamePlayer;\n\n// Perform Actions\n"
 *
 */
/* ----------------------------------------------------------------------------
 * Movement Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Movement:
 *
 * @param Dir8
 * @text 8 Directional Movement
 *
 * @param EnableDir8:eval
 * @text Enable
 * @parent Dir8
 * @type boolean
 * @on Enable
 * @off Disabled
 * @desc Allow 8-directional movement by default? Players can move diagonally.
 * @default true
 *
 * @param StrictCollision:eval
 * @text Strict Collision
 * @parent Dir8
 * @type boolean
 * @on Strict
 * @off Flexible
 * @desc Enforce strict collission rules where the player must be able to pass both cardinal directions?
 * @default true
 *
 * @param FavorHorz:eval
 * @text Favor Horizontal
 * @parent StrictCollision:eval
 * @type boolean
 * @on Horizontal
 * @off Vertical
 * @desc Favor horizontal if cannot pass diagonally but can pass both horizontally and vertically?
 * @default true
 *
 * @param SlowerSpeed:eval
 * @text Slower Diagonals?
 * @parent Dir8
 * @type boolean
 * @on Slower
 * @off Normal
 * @desc Enforce a slower movement speed when moving diagonally?
 * @default false
 *
 * @param DiagonalSpeedMultiplier:num
 * @text Speed Multiplier
 * @parent SlowerSpeed:eval
 * @desc What's the multiplier to adjust movement speed when moving diagonally?
 * @default 0.85
 *
 * @param AutoMove
 * @text Automatic Movement
 *
 * @param StopAutoMoveEvents:eval
 * @text Stop During Events
 * @parent AutoMove
 * @type boolean
 * @on Stop
 * @off Wander
 * @desc Stop automatic event movement while events are running.
 * @default true
 *
 * @param StopAutoMoveMessages:eval
 * @text Stop During Messages
 * @parent AutoMove
 * @type boolean
 * @on Stop
 * @off Wander
 * @desc Stop automatic event movement while a message is running.
 * @default true
 *
 * @param Bitmap
 *
 * @param BitmapSmoothing:eval
 * @text Smoothing
 * @parent Bitmap
 * @type boolean
 * @on Smooth
 * @off Pixelated
 * @desc Do you want to smooth or pixelate the map sprites?
 * Pixelating them is better for zooming and tilting.
 * @default false
 *
 * @param Dash
 * @text Dash
 *
 * @param DashModifier:num
 * @text Dash Modifier
 * @parent Dash
 * @desc Alters the dash speed modifier.
 * @default +1.0
 *
 * @param EnableDashTilt:eval
 * @text Enable Dash Tilt?
 * @parent Dash
 * @type boolean
 * @on Enable
 * @off Disabled
 * @desc Tilt any sprites that are currently dashing?
 * @default true
 *
 * @param TiltLeft:num
 * @text Tilt Left Amount
 * @parent EnableDashTilt:eval
 * @desc Amount in radians when moving left (upper left, left, lower left).
 * @default -0.15
 *
 * @param TiltRight:num
 * @text Tilt Right Amount
 * @parent EnableDashTilt:eval
 * @desc Amount in radians when moving right (upper right, right, lower right).
 * @default 0.15
 *
 * @param TiltVert:num
 * @text Tilt Vertical Amount
 * @parent EnableDashTilt:eval
 * @desc Amount in radians when moving vertical (up, down).
 * @default 0.05
 * 
 * @param EventMove
 * @text Event Movement
 *
 * @param RandomMoveWeight:num
 * @text Random Move Weight
 * @parent EventMove
 * @desc Use numbers between 0 and 1. Numbers closer to 1 stay
 * closer to their home position. 0 to disable it.
 * @default 0.10
 *
 * @param Shadows
 *
 * @param ShowShadows:eval
 * @text Show
 * @parent Shadows
 * @type boolean
 * @on Show
 * @off Don't Show
 * @desc Show shadows on all events and player-related sprites.
 * @default true
 *
 * @param DefaultShadow:str
 * @text Default Filename
 * @parent Shadows
 * @type file
 * @dir img/system/
 * @desc Default filename used for shadows found in img/system/ folder.
 * @default Shadow1
 *
 * @param TurnInPlace
 * @text Turn in Place
 *
 * @param EnableTurnInPlace:eval
 * @text Enable
 * @parent TurnInPlace
 * @type boolean
 * @on Turn in Place
 * @off Skip
 * @desc When not dashing, player will turn in place before moving.
 * This only applies with keyboard inputs.
 * @default false
 *
 * @param TurnInPlaceDelay:num
 * @text Delay in Frames
 * @parent TurnInPlace
 * @type number
 * @min 0
 * @desc The number of frames to wait before moving.
 * @default 10
 *
 * @param Vehicle
 * @text Vehicle Speeds
 *
 * @param BoatSpeed:num
 * @text Boat Speed
 * @parent Vehicle
 * @desc Allows you to adjust the base speed of the boat vehicle.
 * @default 4.0
 *
 * @param ShipSpeed:num
 * @text Ship Speed
 * @parent Vehicle
 * @desc Allows you to adjust the base speed of the ship vehicle.
 * @default 5.0
 *
 * @param AirshipSpeed:num
 * @text Airship Speed
 * @parent Vehicle
 * @desc Allows you to adjust the base speed of the airship vehicle.
 * @default 6.0
 *
 */
/* ----------------------------------------------------------------------------
 * Region Rulings
 * ----------------------------------------------------------------------------
 */
/*~struct~Region:
 *
 * @param Allow
 * @text Allow Regions
 *
 * @param AllAllow:arraynum
 * @text All Allow
 * @parent Allow
 * @type number[]
 * @min 0
 * @max 255
 * @desc Insert Region ID's where the player can enter.
 * Region ID's range from 0 to 255.
 * @default []
 *
 * @param WalkAllow:arraynum
 * @text Walk Allow
 * @parent Allow
 * @type number[]
 * @min 0
 * @max 255
 * @desc Insert Region ID's where walking units can enter.
 * Region ID's range from 0 to 255.
 * @default []
 *
 * @param PlayerAllow:arraynum
 * @text Player Allow
 * @parent WalkAllow:arraynum
 * @type number[]
 * @min 0
 * @max 255
 * @desc Insert Region ID's where the player can enter.
 * Region ID's range from 0 to 255.
 * @default []
 *
 * @param EventAllow:arraynum
 * @text Event Allow
 * @parent WalkAllow:arraynum
 * @type number[]
 * @min 0
 * @max 255
 * @desc Insert Region ID's where events can enter.
 * Region ID's range from 0 to 255.
 * @default []
 *
 * @param VehicleAllow:arraynum
 * @text Vehicle Allow
 * @parent Allow
 * @type number[]
 * @min 0
 * @max 255
 * @desc Insert Region ID's where any vehicle can enter.
 * Region ID's range from 0 to 255.
 * @default []
 *
 * @param BoatAllow:arraynum
 * @text Boat Allow
 * @parent VehicleAllow:arraynum
 * @type number[]
 * @min 0
 * @max 255
 * @desc Insert Region ID's where boats can enter.
 * Region ID's range from 0 to 255.
 * @default []
 *
 * @param ShipAllow:arraynum
 * @text Ship Allow
 * @parent VehicleAllow:arraynum
 * @type number[]
 * @min 0
 * @max 255
 * @desc Insert Region ID's where ships can enter.
 * Region ID's range from 0 to 255.
 * @default []
 *
 * @param AirshipAllow:arraynum
 * @text Airship Allow
 * @parent VehicleAllow:arraynum
 * @type number[]
 * @min 0
 * @max 255
 * @desc Insert Region ID's where airships can enter.
 * Region ID's range from 0 to 255.
 * @default []
 *
 * @param Forbid
 * @text Forbid Regions
 *
 * @param AllForbid:arraynum
 * @text All Forbid
 * @parent Forbid
 * @type number[]
 * @min 0
 * @max 255
 * @desc Insert Region ID's where the player cannot enter.
 * Region ID's range from 0 to 255.
 * @default []
 *
 * @param WalkForbid:arraynum
 * @text Walk Forbid
 * @parent Forbid
 * @type number[]
 * @min 0
 * @max 255
 * @desc Insert Region ID's where walking units cannot enter.
 * Region ID's range from 0 to 255.
 * @default []
 *
 * @param PlayerForbid:arraynum
 * @text Player Forbid
 * @parent WalkForbid:arraynum
 * @type number[]
 * @min 0
 * @max 255
 * @desc Insert Region ID's where the player cannot enter.
 * Region ID's range from 0 to 255.
 * @default []
 *
 * @param EventForbid:arraynum
 * @text Event Forbid
 * @parent WalkForbid:arraynum
 * @type number[]
 * @min 0
 * @max 255
 * @desc Insert Region ID's where events cannot enter.
 * Region ID's range from 0 to 255.
 * @default []
 *
 * @param VehicleForbid:arraynum
 * @text Vehicle Forbid
 * @parent Forbid
 * @type number[]
 * @min 0
 * @max 255
 * @desc Insert Region ID's where vehicles cannot enter.
 * Region ID's range from 0 to 255.
 * @default []
 *
 * @param BoatForbid:arraynum
 * @text Boat Forbid
 * @parent VehicleForbid:arraynum
 * @type number[]
 * @min 0
 * @max 255
 * @desc Insert Region ID's where ships cannot enter.
 * Region ID's range from 0 to 255.
 * @default []
 *
 * @param ShipForbid:arraynum
 * @text Ship Forbid
 * @parent VehicleForbid:arraynum
 * @type number[]
 * @min 0
 * @max 255
 * @desc Insert Region ID's where ships cannot enter.
 * Region ID's range from 0 to 255.
 * @default []
 *
 * @param AirshipForbid:arraynum
 * @text Airship Forbid
 * @parent VehicleForbid:arraynum
 * @type number[]
 * @min 0
 * @max 255
 * @desc Insert Region ID's where airships cannot enter.
 * Region ID's range from 0 to 255.
 * @default []
 *
 * @param Dock
 * @text Dock Regions
 *
 * @param VehicleDock:arraynum
 * @text Vehicle Dock
 * @parent Dock
 * @type number[]
 * @min 0
 * @max 255
 * @desc Insert Region ID's where any vehicle can dock.
 * Region ID's range from 0 to 255.
 * @default []
 *
 * @param BoatDock:arraynum
 * @text Boat Dock
 * @parent Dock
 * @type number[]
 * @min 0
 * @max 255
 * @desc Insert Region ID's where boats can dock.
 * Region ID's range from 0 to 255.
 * @default []
 *
 * @param BoatDockRegionOnly:eval
 * @text Only Region Dockable
 * @parent BoatDock:arraynum
 * @type boolean
 * @on At Regions Only
 * @off Default
 * @desc Boats can only dock at designated regions.
 * @default false
 *
 * @param ShipDock:arraynum
 * @text Ship Dock
 * @parent Dock
 * @type number[]
 * @min 0
 * @max 255
 * @desc Insert Region ID's where ships can dock.
 * Region ID's range from 0 to 255.
 * @default []
 *
 * @param ShipDockRegionOnly:eval
 * @text Only Region Dockable
 * @parent ShipDock:arraynum
 * @type boolean
 * @on At Regions Only
 * @off Default
 * @desc Ships can only dock at designated regions.
 * @default false
 *
 * @param AirshipDock:arraynum
 * @text Airship Dock
 * @parent Dock
 * @type number[]
 * @min 0
 * @max 255
 * @desc Insert Region ID's where airships can dock.
 * Region ID's range from 0 to 255.
 * @default []
 *
 * @param AirshipDockRegionOnly:eval
 * @text Only Region Dockable
 * @parent AirshipDock:arraynum
 * @type boolean
 * @on At Regions Only
 * @off Default
 * @desc Airships can only dock at designated regions.
 * @default false
 *
 */
/* ----------------------------------------------------------------------------
 * Region Common Events
 * ----------------------------------------------------------------------------
 */
/*~struct~RegionCommonEvent:
 *
 * @param Region1:num
 * @text Region 1
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region2:num
 * @text Region 2
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region3:num
 * @text Region 3
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region4:num
 * @text Region 4
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region5:num
 * @text Region 5
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region6:num
 * @text Region 6
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region7:num
 * @text Region 7
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region8:num
 * @text Region 8
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region9:num
 * @text Region 9
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region10:num
 * @text Region 10
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region11:num
 * @text Region 11
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region12:num
 * @text Region 12
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region13:num
 * @text Region 13
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region14:num
 * @text Region 14
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region15:num
 * @text Region 15
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region16:num
 * @text Region 16
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region17:num
 * @text Region 17
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region18:num
 * @text Region 18
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region19:num
 * @text Region 19
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region20:num
 * @text Region 20
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region21:num
 * @text Region 21
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region22:num
 * @text Region 22
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region23:num
 * @text Region 23
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region24:num
 * @text Region 24
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region25:num
 * @text Region 25
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region26:num
 * @text Region 26
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region27:num
 * @text Region 27
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region28:num
 * @text Region 28
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region29:num
 * @text Region 29
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region30:num
 * @text Region 30
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region31:num
 * @text Region 31
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region32:num
 * @text Region 32
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region33:num
 * @text Region 33
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region34:num
 * @text Region 34
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region35:num
 * @text Region 35
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region36:num
 * @text Region 36
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region37:num
 * @text Region 37
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region38:num
 * @text Region 38
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region39:num
 * @text Region 39
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region40:num
 * @text Region 40
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region41:num
 * @text Region 41
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region42:num
 * @text Region 42
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region43:num
 * @text Region 43
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region44:num
 * @text Region 44
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region45:num
 * @text Region 45
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region46:num
 * @text Region 46
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region47:num
 * @text Region 47
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region48:num
 * @text Region 48
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region49:num
 * @text Region 49
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region50:num
 * @text Region 50
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region51:num
 * @text Region 51
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region52:num
 * @text Region 52
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region53:num
 * @text Region 53
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region54:num
 * @text Region 54
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region55:num
 * @text Region 55
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region56:num
 * @text Region 56
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region57:num
 * @text Region 57
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region58:num
 * @text Region 58
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region59:num
 * @text Region 59
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region60:num
 * @text Region 60
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region61:num
 * @text Region 61
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region62:num
 * @text Region 62
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region63:num
 * @text Region 63
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region64:num
 * @text Region 64
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region65:num
 * @text Region 65
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region66:num
 * @text Region 66
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region67:num
 * @text Region 67
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region68:num
 * @text Region 68
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region69:num
 * @text Region 69
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region70:num
 * @text Region 70
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region71:num
 * @text Region 71
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region72:num
 * @text Region 72
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region73:num
 * @text Region 73
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region74:num
 * @text Region 74
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region75:num
 * @text Region 75
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region76:num
 * @text Region 76
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region77:num
 * @text Region 77
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region78:num
 * @text Region 78
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region79:num
 * @text Region 79
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region80:num
 * @text Region 70
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region81:num
 * @text Region 71
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region82:num
 * @text Region 72
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region83:num
 * @text Region 73
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region84:num
 * @text Region 74
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region85:num
 * @text Region 75
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region86:num
 * @text Region 76
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region87:num
 * @text Region 77
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region88:num
 * @text Region 78
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region89:num
 * @text Region 79
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region80:num
 * @text Region 80
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region81:num
 * @text Region 81
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region82:num
 * @text Region 82
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region83:num
 * @text Region 83
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region84:num
 * @text Region 84
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region85:num
 * @text Region 85
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region86:num
 * @text Region 86
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region87:num
 * @text Region 87
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region88:num
 * @text Region 88
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region89:num
 * @text Region 89
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region90:num
 * @text Region 80
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region91:num
 * @text Region 81
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region92:num
 * @text Region 82
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region93:num
 * @text Region 83
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region94:num
 * @text Region 84
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region95:num
 * @text Region 85
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region96:num
 * @text Region 86
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region97:num
 * @text Region 87
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region98:num
 * @text Region 88
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region99:num
 * @text Region 89
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region90:num
 * @text Region 90
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region91:num
 * @text Region 91
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region92:num
 * @text Region 92
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region93:num
 * @text Region 93
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region94:num
 * @text Region 94
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region95:num
 * @text Region 95
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region96:num
 * @text Region 96
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region97:num
 * @text Region 97
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region98:num
 * @text Region 98
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region99:num
 * @text Region 99
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region100:num
 * @text Region 100
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region101:num
 * @text Region 101
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region102:num
 * @text Region 102
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region103:num
 * @text Region 103
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region104:num
 * @text Region 104
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region105:num
 * @text Region 105
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region106:num
 * @text Region 106
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region107:num
 * @text Region 107
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region108:num
 * @text Region 108
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region109:num
 * @text Region 109
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region110:num
 * @text Region 110
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region111:num
 * @text Region 111
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region112:num
 * @text Region 112
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region113:num
 * @text Region 113
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region114:num
 * @text Region 114
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region115:num
 * @text Region 115
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region116:num
 * @text Region 116
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region117:num
 * @text Region 117
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region118:num
 * @text Region 118
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region119:num
 * @text Region 119
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region120:num
 * @text Region 120
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region121:num
 * @text Region 121
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region122:num
 * @text Region 122
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region123:num
 * @text Region 123
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region124:num
 * @text Region 124
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region125:num
 * @text Region 125
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region126:num
 * @text Region 126
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region127:num
 * @text Region 127
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region128:num
 * @text Region 128
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region129:num
 * @text Region 129
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region130:num
 * @text Region 130
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region131:num
 * @text Region 131
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region132:num
 * @text Region 132
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region133:num
 * @text Region 133
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region134:num
 * @text Region 134
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region135:num
 * @text Region 135
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region136:num
 * @text Region 136
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region137:num
 * @text Region 137
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region138:num
 * @text Region 138
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region139:num
 * @text Region 139
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region140:num
 * @text Region 140
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region141:num
 * @text Region 141
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region142:num
 * @text Region 142
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region143:num
 * @text Region 143
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region144:num
 * @text Region 144
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region145:num
 * @text Region 145
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region146:num
 * @text Region 146
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region147:num
 * @text Region 147
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region148:num
 * @text Region 148
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region149:num
 * @text Region 149
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region150:num
 * @text Region 150
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region151:num
 * @text Region 151
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region152:num
 * @text Region 152
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region153:num
 * @text Region 153
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region154:num
 * @text Region 154
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region155:num
 * @text Region 155
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region156:num
 * @text Region 156
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region157:num
 * @text Region 157
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region158:num
 * @text Region 158
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region159:num
 * @text Region 159
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region160:num
 * @text Region 160
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region161:num
 * @text Region 161
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region162:num
 * @text Region 162
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region163:num
 * @text Region 163
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region164:num
 * @text Region 164
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region165:num
 * @text Region 165
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region166:num
 * @text Region 166
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region167:num
 * @text Region 167
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region168:num
 * @text Region 168
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region169:num
 * @text Region 169
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region170:num
 * @text Region 170
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region171:num
 * @text Region 171
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region172:num
 * @text Region 172
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region173:num
 * @text Region 173
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region174:num
 * @text Region 174
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region175:num
 * @text Region 175
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region176:num
 * @text Region 176
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region177:num
 * @text Region 177
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region178:num
 * @text Region 178
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region179:num
 * @text Region 179
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region180:num
 * @text Region 170
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region181:num
 * @text Region 171
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region182:num
 * @text Region 172
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region183:num
 * @text Region 173
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region184:num
 * @text Region 174
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region185:num
 * @text Region 175
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region186:num
 * @text Region 176
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region187:num
 * @text Region 177
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region188:num
 * @text Region 178
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region189:num
 * @text Region 179
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region180:num
 * @text Region 180
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region181:num
 * @text Region 181
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region182:num
 * @text Region 182
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region183:num
 * @text Region 183
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region184:num
 * @text Region 184
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region185:num
 * @text Region 185
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region186:num
 * @text Region 186
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region187:num
 * @text Region 187
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region188:num
 * @text Region 188
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region189:num
 * @text Region 189
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region190:num
 * @text Region 180
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region191:num
 * @text Region 181
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region192:num
 * @text Region 182
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region193:num
 * @text Region 183
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region194:num
 * @text Region 184
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region195:num
 * @text Region 185
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region196:num
 * @text Region 186
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region197:num
 * @text Region 187
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region198:num
 * @text Region 188
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region199:num
 * @text Region 189
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region190:num
 * @text Region 190
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region191:num
 * @text Region 191
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region192:num
 * @text Region 192
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region193:num
 * @text Region 193
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region194:num
 * @text Region 194
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region195:num
 * @text Region 195
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region196:num
 * @text Region 196
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region197:num
 * @text Region 197
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region198:num
 * @text Region 198
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region199:num
 * @text Region 199
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region200:num
 * @text Region 200
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region201:num
 * @text Region 201
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region202:num
 * @text Region 202
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region203:num
 * @text Region 203
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region204:num
 * @text Region 204
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region205:num
 * @text Region 205
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region206:num
 * @text Region 206
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region207:num
 * @text Region 207
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region208:num
 * @text Region 208
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region209:num
 * @text Region 209
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region210:num
 * @text Region 210
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region211:num
 * @text Region 211
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region212:num
 * @text Region 212
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region213:num
 * @text Region 213
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region214:num
 * @text Region 214
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region215:num
 * @text Region 215
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region216:num
 * @text Region 216
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region217:num
 * @text Region 217
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region218:num
 * @text Region 218
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region219:num
 * @text Region 219
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region220:num
 * @text Region 220
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region221:num
 * @text Region 221
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region222:num
 * @text Region 222
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region223:num
 * @text Region 223
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region224:num
 * @text Region 224
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region225:num
 * @text Region 225
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region226:num
 * @text Region 226
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region227:num
 * @text Region 227
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region228:num
 * @text Region 228
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region229:num
 * @text Region 229
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region230:num
 * @text Region 230
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region231:num
 * @text Region 231
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region232:num
 * @text Region 232
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region233:num
 * @text Region 233
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region234:num
 * @text Region 234
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region235:num
 * @text Region 235
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region236:num
 * @text Region 236
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region237:num
 * @text Region 237
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region238:num
 * @text Region 238
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region239:num
 * @text Region 239
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region240:num
 * @text Region 240
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region241:num
 * @text Region 241
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region242:num
 * @text Region 242
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region243:num
 * @text Region 243
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region244:num
 * @text Region 244
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region245:num
 * @text Region 245
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region246:num
 * @text Region 246
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region247:num
 * @text Region 247
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region248:num
 * @text Region 248
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region249:num
 * @text Region 249
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region250:num
 * @text Region 250
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region251:num
 * @text Region 251
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region252:num
 * @text Region 252
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region253:num
 * @text Region 253
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region254:num
 * @text Region 254
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 * @param Region255:num
 * @text Region 255
 * @type common_event
 * @desc Which Common Event does this region activate?
 * Use 0 to not activate any Common Events.
 * @default 0
 *
 */
/* ----------------------------------------------------------------------------
 * Terrain Tag Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~TerrainTag:
 *
 * @param TerrainTag
 * @text Terrain Tag ID's
 *
 * @param Rope:num
 * @text Rope
 * @parent TerrainTag
 * @type number
 * @min 0
 * @max 7
 * @desc Which terrain tag number to use for ropes?
 * @default 1
 *
 */
/* ----------------------------------------------------------------------------
 * VisuStella 8-Dir Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~VS8:
 *
 * @param Balloons
 * @text Balloon Icon Settings
 *
 * @param AutoBalloon:eval
 * @text Auto-Balloon Poses
 * @parent Balloons
 * @type boolean
 * @on Auto
 * @off Manual
 * @desc Automatically pose VS8 sprites when using balloon icons.
 * @default true
 *
 * @param BalloonOffsetX:num
 * @text Balloon Offset X
 * @parent Balloons
 * @desc Offset balloon icons on VS8 sprites by x pixels.
 * @default 0
 *
 * @param BalloonOffsetY:num
 * @text Balloon Offset Y
 * @parent Balloons
 * @desc Offset balloon icons on VS8 sprites by y pixels.
 * @default 10
 *
 * @param Icons
 * 
 * @param AutoBuffer:eval
 * @text Auto Buffer
 * @parent Icons
 * @type boolean
 * @on Auto
 * @off Manual
 * @desc Automatically buffer the X and Y coordinates of
 * VS8 sprites?
 * @default true
 * 
 * @param CarryPose:eval
 * @text Use Carry Pose
 * @parent Icons
 * @type boolean
 * @on Carry Pose
 * @off Normal
 * @desc Use the carry pose when moving with an icon overhead.
 * @default true
 *
 */
//=============================================================================

const _0x5820=['Game_Player_checkEventTriggerThere','height','SelfVariableID','textSizeEx','increaseSteps','startMapCommonEventOnOK','_visibleEventY','updateEventsMoveCoreTagChanges','MUSIC\x20NOTE','VisibleRange','distance','updatePosition','mimic','createLabelWindowForTarget','prototype','TiltLeft','pages','frameCount','Window_EventItem_onOk','Game_Timer_stop','startCallEvent','innerWidth','_commonEvents','USER-DEFINED\x204','EventLabelRefresh','_callEventData','addChild','processOk','getPlayerDiagonalSetting','trim','_visiblePlayerX','PreCopyJS','processMoveRouteAnimation','convertSelfVariableValuesInScriptCall','_MapSpawnedEventData','hasDragonbones','hasCPCs','setMoveRoute','setMoveSpeed','setLastPluginCommandInterpreter','name','Vehicle','FUNC','Allow','visible','isBigCharacter','lineHeight','Operation','isStopFollowerChasing','BufferX','Toggle','drawTextEx','Game_Switches_value','conditions','stop','_waitMode','300871pFVeEY','processMoveRouteSelfVariable','_shadowSprite','isSaveEventLocations','getSavedEventLocation','abs','getControlledFollowerID','isPressed','BoatSpeed','isInVehicle','isTargetEventValidForLabelWindow','Frames','UPPER\x20RIGHT','97171nwIbOz','Game_Map_parallelCommonEvents','processMoveRouteTeleportTo','FALSE','startMapCommonEventOnTouch','setupSpawnTest','TargetSwitchId','_cpc','destinationX','followers','CommonEventID','_spriteOffsetY','Game_Variables_value','terrainTag','updateText','Window_ScrollText_startMessage','Game_Vehicle_initMoveSpeed','row','checkAdvancedSwitchVariablePresent','updateShadow','isRegionForbidPass','spawnPreserved','_visibleEventX','forceMoveRoute','setupDiagonalSupport','parse','Game_Event_locate','away','resume','SelfSwitchID','29kuJuxf','_moveOnlyRegions','left','forceDashing','width','setChaseOff','checkActivationProximity','processMoveRouteMoveTo','_character','EventTimerExpireEvent','Game_CharacterBase_setDirection','PostSpawnJS','Hours','scale','refresh','onOk','_reflection','FastForwardKey','isWorking','EventLocationDelete','_text','setOpacity','setValue','_commonEventId','_forceDashing','initMembersEventsMoveCore','eraseEvent','_pageIndex','roundX','initMembers','_randomHomeY','createCharacterShadow','firstSpawnedEvent','isSelfSwitch','findDiagonalDirectionTo','reverse\x20mimic','isMoveOnlyRegionPassable','_eventMorphData','Game_Event_checkEventTriggerAuto','createLabelWindows','deleteIconsOnEventsDataKey','Game_Variables_setValue','labelWindowText','deltaY','hideShadows','bind','processMoveSynchApproach','lastMovedDirection','Sprite_Character_characterPatternY','$preloadedMap_%1','random','locate','_saveEventLocations','setFrame','list','deltaYFrom','hasStepAnime','TurnInPlaceDelay','Game_Map_refresh','_moveSynch','PlayerMovementDiagonal','events','setStopFollowerChasing','ShipSpeed','PosX','Sprite_Balloon_setup','Game_Map_update','EventLocationCreate','ANGER','loadCPC','_regionRules','toUpperCase','region','isBattleTest','jump','JSON','DiagonalSpeedMultiplier','processMoveRouteMoveUntilStop','morphIntoTemplate','Game_Vehicle_isLandOk','CallEvent','deleteEventLocation','BalloonOffsetX','clearCarrying','canStartLocalEvents','FollowerReset','target','selfValue','DefaultShadow','clearDestination','isPassable','updateWaitMode','_DisablePlayerControl','Game_Timer_onExpire','screenY','Game_Map_setup','VisuMZ_Setup_Preload_Map','moveBackToRandomHome','IconIndex','roundXWithDirection','checkValidEventerMap','Game_Event_start','_eventErased','onChange','filter','setupPageSettings','128ewirLT','Scene_Load_onLoadSuccess','_poseDuration','deleteIconsOnEventsData','GetMoveSynchTarget','Game_Message_add','setupCopyEvent','bufferX','Sprite_Character_update','Button','BULB','log','determineCommonEventsWithCPC','Stop','disable','setupEventsMoveCoreNotetags','SpawnEventAtTerrainTag','player','setDashingEnabled','FollowerSetControl','indexOf','Game_Map_events','isPassableByAnyDirection','SILENCE','_labelWindows','Game_Troop_meetsConditions','Game_Interpreter_executeCommand','Game_Message_setItemChoice','SPIN\x20CCW','variableValid','setPattern','setPose','createSpawnedEventWithData','execute','getPosingCharacterIndex','isAirshipPassable','morphInto','reverseDir','onDatabaseLoaded','PosY','processMoveSynch','%1Dock','processMoveRouteMoveToCharacter','Game_Player_increaseSteps','EventLabelVisible','correctFacingDirection','HEART','ConvertParams','_eventScreenY','setFrames','reserveCommonEvent','COBWEB','setDestination','_spawnData','EVAL','processMoveRouteJumpForward','Game_Message_setNumberInput','EnableDir8','turnAwayFromPoint','_cacheVisibility','VisuMZ_1_MessageCore','Game_Interpreter_character','EventTimerResume','Game_CharacterBase_characterIndex','canMove','apply','setBalloonPose','_counter','_event','CPCsMet','offsetY','_spriteset','moveDiagonally','Value','isAutoBufferIcon','%1\x27s\x20version\x20does\x20not\x20match\x20plugin\x27s.\x20Please\x20update\x20it\x20in\x20the\x20Plugin\x20Manager.','restoreSavedEventPosition','shadowY','Region','moveAwayFromCharacter','Game_Character_processMoveCommand','isSupportDiagonalMovement','processMoveRouteJumpToCharacter','spriteId','meetsSwitchCondition','processMoveRouteHugWall','isEventRunning','Map%1.json','_inputTime','Setting','Game_Event_updateSelfMovement','createContents','description','getEventIconIndex','turnTowardPoint','updateShadowChanges','clearDashing','TemplateName','SpawnEventAtRegion','isNearTheScreen','PlayerAllow','MULTIPLY','remove','Game_CharacterBase_canPass','updateTilt','RegionOkTarget','_alwaysUpdateMove','contents','_randomMoveWeight','pos','_hidden','getPreservedMorphEventData','advancedValue','HURT','SwitchId','LOVE','hasMoveOnlyRegions','setItemChoice','EventAutoMovement','FavorHorz','convertVariableValuesInScriptCall','Game_Event_meetsConditionsCPC','roundYWithDirection','autosaveEventLocation','ARRAYFUNC','processMoveSynchReverseMimic','PostMorphJS','EnableDashTilt','_eventOverload','Sprite_Character_setCharacterBitmap','Letter','%1DockRegionOnly','Map%1-Event%2','95517otKknR','requestBalloon','opacitySpeed','setupRegionRestrictions','isAdvancedVariable','Game_Follower_chaseCharacter','_duration','column','Game_CharacterBase_increaseSteps','isLandOk','makeDeepCopy','createSaveEventLocationData','VICTORY','_vehicleType','chaseCharacter','clearPose','Walk','labelWindowRange','SelfSwitches','DashingEnable','Game_Vehicle_isMapPassable','_eventSpawnData','_eventCopyData','reverse\x20copy','push','Hidden','turnAwayFromCharacter','SuccessSwitchId','startMessage','value','reverse','fittingHeight','updateOpacity','command357','toLowerCase','Spriteset_Map_createLowerLayer','Game_Event_updateParallel','IconSet','PreSpawnJS','needsUpdate','EXCLAMATION','return\x200','LIGHT\x20BULB','processMoveRouteTeleportToCharacter','IconBlendMode','Game_Event_clearPageSettings','isDashDisabled','searchLimit','map','despawnRegions','LEFT\x20TO\x20RIGHT','radius','EventIconChange','findDirectionTo','EventTimerPause','_diagonalSupport','characterName','destinationY','PlayerForbid','getInputDir8','blendMode','metCPC','Airship','eventLabelsVisible','processMoveRouteMoveRepeat','setMovementSuccess','setAllowEventAutoMovement','updatePatternEventsMoveCore','isAdvancedSwitch','front','isTile','setupEventsMoveCoreEffects','_characterIndex','Speed','LOWER\x20RIGHT','SLEEP','deletePreservedMorphEventDataKey','processDrawIcon','IconBufferX','SpawnEventDespawnEventID','checkEventTriggerAuto','floor','isSaveEventLocation','setDirection','anchor','SwitchGetSelfSwitchABCD','EnableTurnInPlace','_erased','pageId','process_VisuMZ_EventsMoveCore_LoadTemplateMaps','_paused','_pattern','splice','despawnEventId','mirror\x20vert','SPIN\x20CW','processMoveSynchMimic','Template','defaultFontSize','Player','_patternLocked','Game_Event_moveTypeRandom','setupMorphEvent','Game_Map_unlockEvent','_moveRouteIndex','advancedFunc','turn180','isPlaytest','EventTimerSpeed','UNTITLED','Game_Timer_initialize','BlendMode','_PreservedEventMorphData','mirror\x20vertical','_eventIcon','_selfTargetItemChoice','_opacity','despawnEverything','MUSICNOTE','BufferY','characterPatternYVS8','_characterName','changeSpeed','registerCommand','processMoveCommandEventsMoveCore','initFollowerController','Direction','checkRegionEventTrigger','_interpreter','Game_CommonEvent_isActive','updatePeriodicRefresh','isShadowShrink','removeMorph','Scene_Boot_onDatabaseLoaded','getLastPluginCommandInterpreter','SpawnEventDespawnAtXY','processMoveRouteStepFrom','activationProximityDistance','Spriteset_Map_createShadow','Game_CharacterBase_moveStraight','right','lastSpawnedEvent','registerSelfTarget','resizeWindow','isMovementSucceeded','setCommonEvent','switches','_moveRoute','EventForbid','_lastPluginCommandInterpreter','Scene_Map_startEncounterEffect','_scene','character','iconWidth','Enable','setSelfValue','Game_Player_executeMove','isEventTest','Game_Event_setupPageSettings','checkEventTriggerEventsMoveCore','Game_Interpreter_updateWaitMode','AutoMoveEvents','_EventsMoveCoreSettings','ANNOYED','COLLAPSE','update','despawnTerrainTags','moveRouteIndex','_clickTrigger','RIGHT','Game_Map_event','pattern','ADDITIVE','Game_Troop_meetsConditionsCPC','type','Game_Follower_initialize','%1%2','setEventLabelsVisible','Game_Event_initialize','frontX','_selfTargetNumberInput','createShadow','USER-DEFINED\x205','EventID','_seconds','updateSelfMovement','template','vert\x20mirror','12473sXbdHx','MorphEventTo','%1:%2','processMoveSynchMirrorVert','setControlledFollowerID','none','status','moveStraight','PlayerIconDelete','setupSaveEventLocations','activationRegionList','_needsRefresh','Game_Enemy_meetsSwitchCondition','isSmartEventCollisionOn','concat','charAt','processMoveSynchCustom','processMoveRoutePatternLock','event','1847aIAxei','prepareSpawnedEventAtRegion','hasEventIcon','bufferY','switch1Valid','_forceCarrying','shadowFilename','Game_CharacterBase_update','getPose','QUESTION','directionOnLadderSpriteVS8dir','KNEEL','Step1MapId','OpacitySpeed','PreloadedMaps','_shadowGraphic','loadDataFile','findProperPageIndex','string','follower','SwitchGetSelfSwitchID','Game_CharacterBase_initMembers','isOnRope','Sprite_Balloon_updatePosition','Game_Player_isDashing','...','drawIcon','setCharacterBitmap','initEventsMoveCoreEffects','Movement','match','_characterSprites','isPosing','isRegionDockable','getInputDirection','isSelfVariable','IconSize','custom','_selfEvent','posEventsMoveCore','down','FollowerSetGlobalChase','updateMoveSynch','General','eventsXyNt','USER-DEFINED\x201','moveTypeRandom','Game_Event_meetsConditions','add','_type','_eventLabelOffsetX','backX','%1\x20is\x20missing\x20a\x20required\x20plugin.\x0aPlease\x20install\x20%2\x20into\x20the\x20Plugin\x20Manager.','executeCommand','Game_Character_setMoveRoute','boxWidth','Seconds','characterIndexVS8','isSpriteVS8dir','_advancedSwitchVariable','moveTowardCharacter','CustomPageConditions','setNumberInput','_saveEventLocation','_screenZoomScale','Game_CharacterBase_moveDiagonally','_tilemap','Game_SelfSwitches_value','turnRight90','isDashingEnabled','DashEnableToggle','moveSynchTarget','_mapId','_spriteOffsetX','return\x20%1','page','processMoveRouteStepToPlayer','_activationProximity','timer','Minutes','EventTimerFramesSet','iconIndex','Settings','opacity','_eventCache','setupEvents','round','processMoveRouteBalloon','meetsConditions','NOTE','switch2Id','isAllowCharacterTilt','LIGHTBULB','Step2MapId','jumpHeight','characterIndex','fontFace','Game_Map_isDashDisabled','backY','forceCarrying','eventId','LEFT','resetFontSettings','AdvancedVariables','isCollidedWithEvents','%1Allow','Sprite_Character_initMembers','addLoadListener','startMapCommonEventOnOKTarget','Window_NumberInput_processOk','Name','_eventIconSprite','setEventIconDataKey','isRegionAllowPass','constructor','StopAutoMoveEvents','TiltVert','SPIN\x20CLOCKWISE','process_VisuMZ_EventsMoveCore_Switches_Variables','setupSpawnedEvents','Game_CharacterBase_updatePattern','meetsCPC','VehicleAllow','ERROR:\x20Map\x20%1\x20has\x20not\x20been\x20preloaded\x20for\x20<Copy\x20Event>\x20usage.','PostCopyJS','gainFrames','isValid','code','adjustDir8MovementSpeed','AllAllow','SpawnEventAtXY','EventLocationSave','updateBitmapSmoothing','outlineColor','dashSpeedModifier','_eventId','checkEventsMoveCoreStringTags','clear','113699TlTPUe','mapId','isSpawnedEvent','Label','Game_Event_findProperPageIndex','createSpawnedEvent','requestRefresh','processMoveSynchAway','Game_Timer_start','SPIN\x20ANTICLOCKWISE','PageId','delay','ZZZ','Self\x20Switch\x20%1','BalloonOffsetY','FollowerSetTargetChase','erase','trigger','initEventsMoveCoreSettings','VariableId','isAnyEventStarting','MessageCore','drawing','setupEventsMoveCoreCommentTags','pluginCommandCallEvent','visibleRange','Game_Interpreter_PluginCommand','AutoBalloon','Passability','getSelfTarget','Collision','_data','square','isDestinationValid','getEventIconData','processMoveRouteStepTo','RandomMoveWeight','Forbid','bitmap','VehicleForbid','slice','posNt','isOnLadder','EventsMoveCore','roundY','isJumping','updateEventIconSprite','onExpire','_eventLabelOffsetY','frontY','RegionOk','switchId','createShadows','isRunning','onCancel','createLowerLayer','Game_Event_event','processMoveCommand','_speed','hasClickTrigger','processMoveSynchRandom','setPlayerControlDisable','_dragonbones','unlockEvent','OFF','_target','some','PreMorphJS','Game_Player_isMapPassable','_frames','Game_Character_forceMoveRoute','STR','clearEventCache','clamp','_isObjectCharacter','Game_Player_getInputDirection','TRUE','getPosingCharacterDirection','setWaitMode','_needsPeriodicRefresh','_stepPattern','isLabelVisible','isBoat','note','removeTemporaryMapSpawnedEvents','deltaXFrom','parameters','autoEventIconBuffer','executeMove','2qnBacR','checkEventTriggerThere','_SavedEventLocations','_working','AdvancedSwitches','canPass','regionList','MapID','VisibleEventLabels','max','CPC','replace','Game_CharacterBase_screenY','isNormalPriority','setImage','onClickTrigger','realMoveSpeed','useCarryPoseForIcons','registerSelfEvent','text','isAllowEventAutoMovement','turnTowardCharacter','meetActivationProximityConditions','getDirectionToPoint','_pose','Game_Map_setupEvents','PlayerIconChange','moveTowardPoint','isShadowVisible','Step2Preserve','clearStepPattern','updateVS8BalloonOffsets','meetActivationRegionConditions','_lastMovedDirection','_EventIcons','updatePose','vehicle','findTargetSprite','create','setupSpawn','clearSelfTarget','_encounterEffectDuration','_eventScreenX','exit','ROUTE_SCRIPT','_spawnedEvents','NUM','_expireCommonEvent','call','OperateValues','createIconSprite','DOWN','FollowerID','parent','format','Game_System_initialize','regionId','processMoveRouteSetIndex','BitmapSmoothing','isEventClickTriggered','fontSize','setEventIconData','USER-DEFINED\x203','initEventsMoveCore','offsetX','contentsOpacity','SWEAT','initialize','RemovePreserve','isDiagonalDirection','copy','windowPadding','MUSIC-NOTE','getMapSpawnedEventData','default','_labelWindow','Step2EventId','boat','TiltRight','processMoveRouteJumpTo','checkEventTriggerHere','VisuMZ_0_CoreEngine','checkExistingEntitiesAt','enable','Ship','_addedHitbox','_filename','rotation','_transparent','_PlayerDiagonalSetting','AirshipSpeed','removeChild','shiftY','initMoveSpeed','Boat','includes','getPosingCharacterPattern','moveForward','shadowX','updateScale','_followerChaseOff','Icon','airship','direction','start','MoveAllSynchTargets','_CPCs','UPPER\x20LEFT','ShowShadows','EventTimerExpireClear','MapId','_comments','moveSynchType','checkSmartEventCollision','canPassDiagonally','iconHeight','deleteSavedEventLocationKey','itemPadding','activationProximityType','spawnEventId','variableId','length','EventTemplates','Game_CharacterBase_realMoveSpeed','updatePattern','isDashing','IconBufferY','_eventPageIndex','clearPageSettings','Window_NumberInput_start','USER-DEFINED\x202','FRUSTRATION','_visiblePlayerY','Region%1','EventIconDelete','_chaseOff','loadSystem','3ZcAetz','Sprite_Character_setTileBitmap','Rope','ITEM','MoveRouteIndex','_periodicRefreshTimer','vertical\x20mirror','Game_CharacterBase_pattern','despawnAtXY','_stopCount','prepareSpawnedEventAtXY','characterPatternY','524683gIWSIi','EventId','Step1EventId','VS8','zoomScale','_randomHomeX','determineEventOverload','_activationProximityAutoTriggerBypass','iconSize','StrictCollision','Game_Event_refresh','moveAwayFromPoint','padZero','saveEventLocation','_eventOverloadThreshold','deleteSavedEventLocation','MUSIC','processMoveRouteStepToCharacter','_moveSpeed','processMoveRouteFadeOut','smooth','SelfVariables','turnLeft90','VisuMZ_2_DragonbonesUnion','TerrainTags','_followerControlID','eventsXy','updateParallel','deltaX','isPlayerControlDisabled','horizontal\x20mirror','variables','isPreventSelfMovement','savePreservedMorphEventDataKey','firstSpawnedEventID','_callEventMap','LOWER\x20LEFT','EventAllow','processMoveRouteSelfSwitch','requestAnimation','isEventOverloaded','isMapPassable','parallelCommonEvents','isTurnInPlace','executeMoveDir8','Self\x20Variable\x20%1','ARRAYSTR','randomInt','_spawnPreserved','switch1Id','getDirectionFromPoint','Game_Switches_setValue','SelfSwitchABCD','standing','DashModifier','pause','isActive','isDashingAndMoving','Window_EventItem_onCancel','isSpawnHitboxCollisionOk','isMoving','Window_Message_startMessage','Game_SelfSwitches_setValue','screenX','_selfTarget','min','unlock','Game_CharacterBase_screenX','setup','HMPH','hasAdvancedSwitchVariable','Preserve','mirror\x20horz','setDiagonalDirection'];const _0x26d7=function(_0x47dc88,_0x314085){_0x47dc88=_0x47dc88-0x113;let _0x5820c7=_0x5820[_0x47dc88];return _0x5820c7;};const _0x5e5f48=_0x26d7;(function(_0x17dffb,_0x727ed9){const _0x58a4a2=_0x26d7;while(!![]){try{const _0x3cd1ee=-parseInt(_0x58a4a2(0x4a7))+-parseInt(_0x58a4a2(0x38d))+parseInt(_0x58a4a2(0x276))*parseInt(_0x58a4a2(0x21d))+parseInt(_0x58a4a2(0x193))*parseInt(_0x58a4a2(0x422))+-parseInt(_0x58a4a2(0x180))*-parseInt(_0x58a4a2(0x3b8))+parseInt(_0x58a4a2(0x39a))*parseInt(_0x58a4a2(0x2ff))+-parseInt(_0x58a4a2(0x30b));if(_0x3cd1ee===_0x727ed9)break;else _0x17dffb['push'](_0x17dffb['shift']());}catch(_0x3f2637){_0x17dffb['push'](_0x17dffb['shift']());}}}(_0x5820,0x2fd85));var label=_0x5e5f48(0x248),tier=tier||0x0,dependencies=[],pluginData=$plugins['filter'](function(_0x34a579){const _0x472b34=_0x5e5f48;return _0x34a579[_0x472b34(0x186)]&&_0x34a579['description']['includes']('['+label+']');})[0x0];VisuMZ[label]['Settings']=VisuMZ[label][_0x5e5f48(0x1e5)]||{},VisuMZ[_0x5e5f48(0x451)]=function(_0x116cef,_0x94873a){const _0x31c4ba=_0x5e5f48;for(const _0x33a96c in _0x94873a){if(_0x33a96c['match'](/(.*):(.*)/i)){const _0x4875e6=String(RegExp['$1']),_0x299787=String(RegExp['$2'])['toUpperCase']()[_0x31c4ba(0x372)]();let _0x5cdc33,_0x40347f,_0x5a6481;switch(_0x299787){case _0x31c4ba(0x2a4):_0x5cdc33=_0x94873a[_0x33a96c]!==''?Number(_0x94873a[_0x33a96c]):0x0;break;case'ARRAYNUM':_0x40347f=_0x94873a[_0x33a96c]!==''?JSON[_0x31c4ba(0x3b3)](_0x94873a[_0x33a96c]):[],_0x5cdc33=_0x40347f['map'](_0x1e8124=>Number(_0x1e8124));break;case _0x31c4ba(0x458):_0x5cdc33=_0x94873a[_0x33a96c]!==''?eval(_0x94873a[_0x33a96c]):null;break;case'ARRAYEVAL':_0x40347f=_0x94873a[_0x33a96c]!==''?JSON[_0x31c4ba(0x3b3)](_0x94873a[_0x33a96c]):[],_0x5cdc33=_0x40347f['map'](_0x4f57ff=>eval(_0x4f57ff));break;case _0x31c4ba(0x403):_0x5cdc33=_0x94873a[_0x33a96c]!==''?JSON[_0x31c4ba(0x3b3)](_0x94873a[_0x33a96c]):'';break;case'ARRAYJSON':_0x40347f=_0x94873a[_0x33a96c]!==''?JSON[_0x31c4ba(0x3b3)](_0x94873a[_0x33a96c]):[],_0x5cdc33=_0x40347f[_0x31c4ba(0x4d7)](_0x4f8303=>JSON[_0x31c4ba(0x3b3)](_0x4f8303));break;case _0x31c4ba(0x37f):_0x5cdc33=_0x94873a[_0x33a96c]!==''?new Function(JSON['parse'](_0x94873a[_0x33a96c])):new Function(_0x31c4ba(0x4d0));break;case _0x31c4ba(0x49e):_0x40347f=_0x94873a[_0x33a96c]!==''?JSON[_0x31c4ba(0x3b3)](_0x94873a[_0x33a96c]):[],_0x5cdc33=_0x40347f['map'](_0x26b434=>new Function(JSON[_0x31c4ba(0x3b3)](_0x26b434)));break;case _0x31c4ba(0x264):_0x5cdc33=_0x94873a[_0x33a96c]!==''?String(_0x94873a[_0x33a96c]):'';break;case _0x31c4ba(0x339):_0x40347f=_0x94873a[_0x33a96c]!==''?JSON[_0x31c4ba(0x3b3)](_0x94873a[_0x33a96c]):[],_0x5cdc33=_0x40347f[_0x31c4ba(0x4d7)](_0x1e10e6=>String(_0x1e10e6));break;case'STRUCT':_0x5a6481=_0x94873a[_0x33a96c]!==''?JSON['parse'](_0x94873a[_0x33a96c]):{},_0x116cef[_0x4875e6]={},VisuMZ[_0x31c4ba(0x451)](_0x116cef[_0x4875e6],_0x5a6481);continue;case'ARRAYSTRUCT':_0x40347f=_0x94873a[_0x33a96c]!==''?JSON[_0x31c4ba(0x3b3)](_0x94873a[_0x33a96c]):[],_0x5cdc33=_0x40347f[_0x31c4ba(0x4d7)](_0x4e4eab=>VisuMZ[_0x31c4ba(0x451)]({},JSON[_0x31c4ba(0x3b3)](_0x4e4eab)));break;default:continue;}_0x116cef[_0x4875e6]=_0x5cdc33;}}return _0x116cef;},(_0x250dce=>{const _0x195164=_0x5e5f48,_0x4725e1=_0x250dce[_0x195164(0x37d)];for(const _0x4773f3 of dependencies){if(!Imported[_0x4773f3]){alert(_0x195164(0x1c7)[_0x195164(0x2ac)](_0x4725e1,_0x4773f3)),SceneManager['exit']();break;}}const _0x26cee3=_0x250dce[_0x195164(0x47e)];if(_0x26cee3[_0x195164(0x1b1)](/\[Version[ ](.*?)\]/i)){const _0x998ea3=Number(RegExp['$1']);_0x998ea3!==VisuMZ[label]['version']&&(alert(_0x195164(0x46d)[_0x195164(0x2ac)](_0x4725e1,_0x998ea3)),SceneManager[_0x195164(0x2a1)]());}if(_0x26cee3[_0x195164(0x1b1)](/\[Tier[ ](\d+)\]/i)){const _0x5ac7e0=Number(RegExp['$1']);_0x5ac7e0<tier?(alert('%1\x20is\x20incorrectly\x20placed\x20on\x20the\x20plugin\x20list.\x0aIt\x20is\x20a\x20Tier\x20%2\x20plugin\x20placed\x20over\x20other\x20Tier\x20%3\x20plugins.\x0aPlease\x20reorder\x20the\x20plugin\x20list\x20from\x20smallest\x20to\x20largest\x20tier\x20numbers.'[_0x195164(0x2ac)](_0x4725e1,_0x5ac7e0,tier)),SceneManager['exit']()):tier=Math[_0x195164(0x27f)](_0x5ac7e0,tier);}VisuMZ[_0x195164(0x451)](VisuMZ[label][_0x195164(0x1e5)],_0x250dce[_0x195164(0x273)]);})(pluginData),VisuMZ[_0x5e5f48(0x2a7)]=function(_0xc7653d,_0x1b165c,_0x5d85e6){switch(_0x5d85e6){case'=':return _0x1b165c;break;case'+':return _0xc7653d+_0x1b165c;break;case'-':return _0xc7653d-_0x1b165c;break;case'*':return _0xc7653d*_0x1b165c;break;case'/':return _0xc7653d/_0x1b165c;break;case'%':return _0xc7653d%_0x1b165c;break;}return _0xc7653d;},PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x165),_0x3b0e15=>{const _0x273bca=_0x5e5f48;VisuMZ[_0x273bca(0x451)](_0x3b0e15,_0x3b0e15);switch(_0x3b0e15[_0x273bca(0x46b)]){case _0x273bca(0x380):$gameSystem[_0x273bca(0x4e9)](!![]);break;case _0x273bca(0x42f):$gameSystem[_0x273bca(0x4e9)](![]);break;case _0x273bca(0x387):$gameSystem[_0x273bca(0x4e9)](!$gameSystem[_0x273bca(0x28a)]());break;}}),PluginManager['registerCommand'](pluginData['name'],_0x5e5f48(0x408),_0x20f8a1=>{const _0x9f14e5=_0x5e5f48;VisuMZ['ConvertParams'](_0x20f8a1,_0x20f8a1);const _0x2b31aa=$gameTemp['getLastPluginCommandInterpreter'](),_0x260dc7={'mapId':_0x20f8a1[_0x9f14e5(0x2e4)],'eventId':_0x20f8a1[_0x9f14e5(0x30c)]||_0x2b31aa[_0x9f14e5(0x1f7)](),'pageId':_0x20f8a1[_0x9f14e5(0x227)]};if(_0x260dc7[_0x9f14e5(0x21e)]<=0x0)_0x260dc7['mapId']=$gameMap?$gameMap[_0x9f14e5(0x21e)]():0x1;$gameTemp[_0x9f14e5(0x14a)]()[_0x9f14e5(0x235)](_0x260dc7);}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x1d9),_0x13ec0a=>{const _0x465507=_0x5e5f48;VisuMZ[_0x465507(0x451)](_0x13ec0a,_0x13ec0a);switch(_0x13ec0a[_0x465507(0x46b)]){case _0x465507(0x15e):$gameSystem[_0x465507(0x434)](!![]);break;case'Disable':$gameSystem[_0x465507(0x434)](![]);break;case _0x465507(0x387):$gameSystem[_0x465507(0x434)](!$gameSystem['isDashingEnabled']());break;}}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x4db),_0x2b3f2d=>{const _0x2cae75=_0x5e5f48;VisuMZ['ConvertParams'](_0x2b3f2d,_0x2b3f2d);const _0x5127e9=$gameTemp[_0x2cae75(0x14a)]();_0x2b3f2d[_0x2cae75(0x2e4)]=_0x2b3f2d[_0x2cae75(0x2e4)]||$gameMap[_0x2cae75(0x21e)](),$gameSystem[_0x2cae75(0x203)](_0x2b3f2d['MapId'],_0x2b3f2d['EventId']||_0x5127e9[_0x2cae75(0x1f7)](),_0x2b3f2d[_0x2cae75(0x41a)],_0x2b3f2d[_0x2cae75(0x4f5)],_0x2b3f2d['IconBufferY'],_0x2b3f2d[_0x2cae75(0x4d3)]);}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x2fc),_0x362a41=>{const _0x299c52=_0x5e5f48;VisuMZ[_0x299c52(0x451)](_0x362a41,_0x362a41);const _0x322cb9=$gameTemp[_0x299c52(0x14a)]();_0x362a41[_0x299c52(0x2e4)]=_0x362a41['MapId']||$gameMap[_0x299c52(0x21e)](),$gameSystem[_0x299c52(0x3e0)](_0x362a41[_0x299c52(0x2e4)],_0x362a41['EventId']||_0x322cb9[_0x299c52(0x1f7)]());}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x36d),_0x11440b=>{if($gameMap)for(const _0x497aa6 of $gameMap['events']()){_0x497aa6['refresh']();}}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x44e),_0x4ceba0=>{const _0x188747=_0x5e5f48;VisuMZ[_0x188747(0x451)](_0x4ceba0,_0x4ceba0);switch(_0x4ceba0['Visibility']){case'Visible':$gameSystem[_0x188747(0x175)](!![]);break;case _0x188747(0x4c0):$gameSystem[_0x188747(0x175)](![]);break;case'Toggle':$gameSystem['setEventLabelsVisible'](!$gameSystem['eventLabelsVisible']());break;}}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x216),_0x496f80=>{const _0x4a8f44=_0x5e5f48;VisuMZ[_0x4a8f44(0x451)](_0x496f80,_0x496f80);const _0x596cc3=$gameTemp[_0x4a8f44(0x14a)]();if(!$gameMap)return;const _0xc04bc8=$gameMap[_0x4a8f44(0x192)](_0x496f80[_0x4a8f44(0x30c)]||_0x596cc3[_0x4a8f44(0x1f7)]());if(_0xc04bc8)_0xc04bc8[_0x4a8f44(0x318)]();}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x3fb),_0x497876=>{const _0x59924f=_0x5e5f48;VisuMZ[_0x59924f(0x451)](_0x497876,_0x497876);const _0x463a44=$gameTemp[_0x59924f(0x14a)](),_0x3f748a=_0x497876[_0x59924f(0x2e4)]||$gameMap[_0x59924f(0x21e)](),_0x555342=_0x497876['EventId']||_0x463a44[_0x59924f(0x1f7)](),_0x2f38c1=_0x497876['PosX']||0x0,_0x1ecc1b=_0x497876['PosY']||0x0,_0xeec0d4=_0x497876[_0x59924f(0x142)]||0x2,_0x4b7ad0=((_0x497876['PageId']||0x1)-0x1)[_0x59924f(0x266)](0x0,0x13),_0x47ec9e=_0x497876[_0x59924f(0x303)]||0x0;$gameSystem[_0x59924f(0x4b2)](_0x3f748a,_0x555342,_0x2f38c1,_0x1ecc1b,_0xeec0d4,_0x4b7ad0,_0x47ec9e);}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x3cb),_0x5e5f2d=>{const _0x160eea=_0x5e5f48;VisuMZ[_0x160eea(0x451)](_0x5e5f2d,_0x5e5f2d);const _0x1b62df=$gameTemp['getLastPluginCommandInterpreter'](),_0x59450d=_0x5e5f2d[_0x160eea(0x2e4)]||$gameMap[_0x160eea(0x21e)](),_0x1c4d3d=_0x5e5f2d['EventId']||_0x1b62df[_0x160eea(0x1f7)]();$gameSystem[_0x160eea(0x2ea)](_0x59450d,_0x1c4d3d);}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x3c1),_0x697507=>{const _0x57d901=_0x5e5f48;VisuMZ[_0x57d901(0x451)](_0x697507,_0x697507);const _0xe1f798=_0x697507[_0x57d901(0x3a4)];$gameTimer['setCommonEvent'](_0xe1f798);}),PluginManager[_0x5e5f48(0x13f)](pluginData['name'],_0x5e5f48(0x2e3),_0x36db9d=>{const _0xcbcf19=_0x5e5f48;$gameTimer[_0xcbcf19(0x155)](0x0);}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],'EventTimerFramesGain',_0x2c297e=>{const _0x3cde75=_0x5e5f48;if(!$gameTimer[_0x3cde75(0x3ca)]())return;VisuMZ[_0x3cde75(0x451)](_0x2c297e,_0x2c297e);let _0x169f58=0x0;_0x169f58+=_0x2c297e[_0x3cde75(0x398)],_0x169f58+=_0x2c297e['Seconds']*0x3c,_0x169f58+=_0x2c297e[_0x3cde75(0x1e2)]*0x3c*0x3c,_0x169f58+=_0x2c297e[_0x3cde75(0x3c4)]*0x3c*0x3c*0x3c,$gameTimer[_0x3cde75(0x210)](_0x169f58);}),PluginManager['registerCommand'](pluginData['name'],_0x5e5f48(0x1e3),_0x28dd34=>{const _0x37614d=_0x5e5f48;if(!$gameTimer['isWorking']())return;VisuMZ[_0x37614d(0x451)](_0x28dd34,_0x28dd34);let _0x203461=0x0;_0x203461+=_0x28dd34['Frames'],_0x203461+=_0x28dd34[_0x37614d(0x1cb)]*0x3c,_0x203461+=_0x28dd34['Minutes']*0x3c*0x3c,_0x203461+=_0x28dd34[_0x37614d(0x3c4)]*0x3c*0x3c*0x3c,$gameTimer[_0x37614d(0x453)](_0x203461);}),PluginManager['registerCommand'](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x4dd),_0x24b5ec=>{const _0x54bd38=_0x5e5f48;if(!$gameTimer['isWorking']())return;$gameTimer[_0x54bd38(0x342)]();}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x460),_0x4f9326=>{const _0x30166a=_0x5e5f48;if(!$gameTimer[_0x30166a(0x3ca)]())return;$gameTimer[_0x30166a(0x3b6)]();}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x130),_0xedbcb4=>{const _0x10a58c=_0x5e5f48;VisuMZ['ConvertParams'](_0xedbcb4,_0xedbcb4);const _0x54cead=_0xedbcb4[_0x10a58c(0x4f0)]||0x0;$gameTimer[_0x10a58c(0x13e)](_0x54cead);}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x1bc),_0x31074d=>{const _0xd46661=_0x5e5f48;VisuMZ['ConvertParams'](_0x31074d,_0x31074d);const _0x20f050=!_0x31074d['Chase'];$gameSystem[_0xd46661(0x3f6)](_0x20f050);}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x22c),_0x2289a0=>{const _0x5f2e91=_0x5e5f48;VisuMZ[_0x5f2e91(0x451)](_0x2289a0,_0x2289a0);const _0x3e66ad=(_0x2289a0[_0x5f2e91(0x2aa)]||0x0)-0x1,_0x51da4b=!_0x2289a0['Chase'],_0x406083=$gamePlayer[_0x5f2e91(0x3a3)]()[_0x5f2e91(0x1a6)](_0x3e66ad);if(_0x406083)_0x406083[_0x5f2e91(0x3bd)](_0x51da4b);}),PluginManager['registerCommand'](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x435),_0x5bdcb5=>{const _0x1434f2=_0x5e5f48;VisuMZ['ConvertParams'](_0x5bdcb5,_0x5bdcb5);const _0xfadbad=_0x5bdcb5[_0x1434f2(0x2aa)];$gameSystem[_0x1434f2(0x184)](_0xfadbad);}),PluginManager[_0x5e5f48(0x13f)](pluginData['name'],_0x5e5f48(0x40d),_0x361f27=>{const _0x37f5e7=_0x5e5f48;VisuMZ[_0x37f5e7(0x451)](_0x361f27,_0x361f27),$gameSystem[_0x37f5e7(0x184)](0x0),$gameSystem[_0x37f5e7(0x3f6)](![]);for(const _0x3cf299 of $gamePlayer['followers']()['_data']){if(_0x3cf299)_0x3cf299[_0x37f5e7(0x3bd)](![]);}}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x119),_0x2a6bde=>{const _0x4460fe=_0x5e5f48;VisuMZ[_0x4460fe(0x451)](_0x2a6bde,_0x2a6bde);const _0x2e47d0=$gameTemp['getLastPluginCommandInterpreter']();_0x2a6bde[_0x4460fe(0x2e4)]=_0x2a6bde[_0x4460fe(0x2e4)]||$gameMap[_0x4460fe(0x21e)]();const _0x58ef99=[_0x2a6bde[_0x4460fe(0x2e4)],_0x2a6bde[_0x4460fe(0x30c)]||_0x2e47d0[_0x4460fe(0x1f7)](),_0x2a6bde[_0x4460fe(0x4a4)]],_0x2fd7f8=_0x2a6bde[_0x4460fe(0x3a0)],_0x2dcaff=$gameSelfSwitches[_0x4460fe(0x4c4)](_0x58ef99)||![];$gameSwitches[_0x4460fe(0x3ce)](_0x2fd7f8,_0x2dcaff);}),PluginManager['registerCommand'](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x1a7),_0x4fd8c4=>{const _0x31a215=_0x5e5f48;VisuMZ[_0x31a215(0x451)](_0x4fd8c4,_0x4fd8c4);const _0x59a59e=$gameTemp[_0x31a215(0x14a)]();_0x4fd8c4['MapId']=_0x4fd8c4[_0x31a215(0x2e4)]||$gameMap['mapId']();const _0x2e4cd2=[_0x4fd8c4['MapId'],_0x4fd8c4[_0x31a215(0x30c)]||_0x59a59e[_0x31a215(0x1f7)](),_0x31a215(0x22a)[_0x31a215(0x2ac)](_0x4fd8c4[_0x31a215(0x494)])],_0xddd9d5=_0x4fd8c4['TargetSwitchId'],_0x22b891=$gameSelfSwitches['value'](_0x2e4cd2)||![];$gameSwitches['setValue'](_0xddd9d5,_0x22b891);}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],'VariableGetSelfVariableID',_0x4c9eec=>{const _0x49d79d=_0x5e5f48;VisuMZ[_0x49d79d(0x451)](_0x4c9eec,_0x4c9eec);const _0x2a782f=$gameTemp[_0x49d79d(0x14a)]();_0x4c9eec['MapId']=_0x4c9eec[_0x49d79d(0x2e4)]||$gameMap['mapId']();const _0x6b7d44=[_0x4c9eec[_0x49d79d(0x2e4)],_0x4c9eec['EventId']||_0x2a782f[_0x49d79d(0x1f7)](),_0x49d79d(0x338)[_0x49d79d(0x2ac)](_0x4c9eec['VariableId'])],_0x31e212=_0x4c9eec['TargetVariableId'],_0x1e8266=$gameSelfSwitches[_0x49d79d(0x4c4)](_0x6b7d44)||![];$gameVariables['setValue'](_0x31e212,_0x1e8266);}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x181),_0x4abe3b=>{const _0x5e6379=_0x5e5f48;VisuMZ['ConvertParams'](_0x4abe3b,_0x4abe3b);if(!$gameMap)return;const _0x1d3c8b=$gameTemp['getLastPluginCommandInterpreter'](),_0x358bbf=_0x4abe3b[_0x5e6379(0x293)];_0x4abe3b[_0x5e6379(0x19f)]=_0x4abe3b[_0x5e6379(0x19f)]||$gameMap[_0x5e6379(0x21e)](),_0x4abe3b['Step2MapId']=_0x4abe3b[_0x5e6379(0x1f0)]||$gameMap['mapId'](),_0x4abe3b[_0x5e6379(0x483)]=_0x4abe3b['TemplateName'][_0x5e6379(0x3ff)]()[_0x5e6379(0x372)]();if(!_0x358bbf&&_0x4abe3b[_0x5e6379(0x19f)]!==$gameMap[_0x5e6379(0x21e)]())return;if($gameMap[_0x5e6379(0x21e)]()===_0x4abe3b[_0x5e6379(0x19f)]){const _0x5e09b6=$gameMap[_0x5e6379(0x192)](_0x4abe3b[_0x5e6379(0x30d)]||_0x1d3c8b[_0x5e6379(0x1f7)]());if(!_0x5e09b6)return;_0x4abe3b[_0x5e6379(0x483)]!=='UNTITLED'?_0x5e09b6[_0x5e6379(0x406)](_0x4abe3b[_0x5e6379(0x483)]):_0x5e09b6['morphInto'](_0x4abe3b[_0x5e6379(0x1f0)],_0x4abe3b[_0x5e6379(0x2c2)]||_0x1d3c8b[_0x5e6379(0x1f7)]());}_0x358bbf&&$gameSystem[_0x5e6379(0x32c)](_0x4abe3b[_0x5e6379(0x19f)],_0x4abe3b['Step1EventId'],_0x4abe3b[_0x5e6379(0x483)],_0x4abe3b[_0x5e6379(0x1f0)],_0x4abe3b[_0x5e6379(0x2c2)]);}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],'MorphEventRemove',_0x2cd9ee=>{const _0x5ccc0a=_0x5e5f48;VisuMZ[_0x5ccc0a(0x451)](_0x2cd9ee,_0x2cd9ee);if(!$gameMap)return;const _0x8cb344=$gameTemp[_0x5ccc0a(0x14a)]();_0x2cd9ee[_0x5ccc0a(0x2e4)]=_0x2cd9ee[_0x5ccc0a(0x2e4)]||$gameMap[_0x5ccc0a(0x21e)]();if($gameMap['mapId']()===_0x2cd9ee[_0x5ccc0a(0x2e4)]){const _0x252f97=$gameMap[_0x5ccc0a(0x192)](_0x2cd9ee['EventId']||_0x8cb344[_0x5ccc0a(0x1f7)]());_0x252f97[_0x5ccc0a(0x148)]();}_0x2cd9ee[_0x5ccc0a(0x2ba)]&&$gameSystem[_0x5ccc0a(0x4f3)](_0x2cd9ee[_0x5ccc0a(0x2e4)],_0x2cd9ee[_0x5ccc0a(0x30c)]||_0x8cb344['eventId']());}),PluginManager['registerCommand'](pluginData[_0x5e5f48(0x37d)],'PlayerMovementChange',_0x50bb91=>{const _0xec5373=_0x5e5f48;VisuMZ[_0xec5373(0x451)](_0x50bb91,_0x50bb91),$gameSystem[_0xec5373(0x25a)](!_0x50bb91[_0xec5373(0x15e)]);}),PluginManager['registerCommand'](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x3f4),_0x319fae=>{const _0x59cea6=_0x5e5f48;VisuMZ[_0x59cea6(0x451)](_0x319fae,_0x319fae),$gameSystem['setPlayerDiagonalSetting'](_0x319fae[_0x59cea6(0x47b)]);}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x290),_0xee6af8=>{const _0x480733=_0x5e5f48;VisuMZ[_0x480733(0x451)](_0xee6af8,_0xee6af8),$gameSystem[_0x480733(0x2b3)]($gamePlayer,_0xee6af8[_0x480733(0x41a)],_0xee6af8[_0x480733(0x4f5)],_0xee6af8[_0x480733(0x2f4)],_0xee6af8[_0x480733(0x4d3)]);}),PluginManager['registerCommand'](pluginData['name'],_0x5e5f48(0x188),_0x452187=>{const _0x123f96=_0x5e5f48;VisuMZ[_0x123f96(0x451)](_0x452187,_0x452187),$gameSystem[_0x123f96(0x425)]($gamePlayer);}),PluginManager[_0x5e5f48(0x13f)](pluginData['name'],_0x5e5f48(0x33f),_0x490e2d=>{const _0x371c3d=_0x5e5f48;VisuMZ[_0x371c3d(0x451)](_0x490e2d,_0x490e2d);const _0x4dfbd7=$gameTemp[_0x371c3d(0x14a)]();_0x490e2d[_0x371c3d(0x2e4)]=_0x490e2d[_0x371c3d(0x2e4)]||$gameMap[_0x371c3d(0x21e)]();const _0x37e25c=[_0x490e2d[_0x371c3d(0x2e4)],_0x490e2d[_0x371c3d(0x30c)]||_0x4dfbd7[_0x371c3d(0x1f7)](),_0x490e2d['Letter']];switch(_0x490e2d['Value']){case'ON':$gameSelfSwitches[_0x371c3d(0x3ce)](_0x37e25c,!![]);break;case _0x371c3d(0x25d):$gameSelfSwitches[_0x371c3d(0x3ce)](_0x37e25c,![]);break;case'Toggle':$gameSelfSwitches[_0x371c3d(0x3ce)](_0x37e25c,!$gameSelfSwitches[_0x371c3d(0x4c4)](_0x37e25c));break;}}),PluginManager['registerCommand'](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x3b7),_0x400a9b=>{const _0x82de50=_0x5e5f48;VisuMZ[_0x82de50(0x451)](_0x400a9b,_0x400a9b);const _0x2e7383=$gameTemp[_0x82de50(0x14a)]();_0x400a9b['MapId']=_0x400a9b[_0x82de50(0x2e4)]||$gameMap['mapId']();const _0xa9bcdc=[_0x400a9b[_0x82de50(0x2e4)],_0x400a9b[_0x82de50(0x30c)]||_0x2e7383[_0x82de50(0x1f7)](),'Self\x20Switch\x20%1'['format'](_0x400a9b['SwitchId'])];switch(_0x400a9b[_0x82de50(0x46b)]){case'ON':$gameSelfSwitches['setValue'](_0xa9bcdc,!![]);break;case'OFF':$gameSelfSwitches[_0x82de50(0x3ce)](_0xa9bcdc,![]);break;case _0x82de50(0x387):$gameSelfSwitches[_0x82de50(0x3ce)](_0xa9bcdc,!$gameSelfSwitches[_0x82de50(0x4c4)](_0xa9bcdc));break;}}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x357),_0x1196c3=>{const _0xc7f9c1=_0x5e5f48;VisuMZ['ConvertParams'](_0x1196c3,_0x1196c3);const _0x51fd12=$gameTemp[_0xc7f9c1(0x14a)]();_0x1196c3[_0xc7f9c1(0x2e4)]=_0x1196c3[_0xc7f9c1(0x2e4)]||$gameMap[_0xc7f9c1(0x21e)]();const _0x595e6b=[_0x1196c3['MapId'],_0x1196c3[_0xc7f9c1(0x30c)]||_0x51fd12[_0xc7f9c1(0x1f7)](),_0xc7f9c1(0x338)[_0xc7f9c1(0x2ac)](_0x1196c3[_0xc7f9c1(0x230)])],_0x40024c=VisuMZ['OperateValues']($gameSelfSwitches[_0xc7f9c1(0x4c4)](_0x595e6b),_0x1196c3[_0xc7f9c1(0x46b)],_0x1196c3[_0xc7f9c1(0x384)]);$gameSelfSwitches[_0xc7f9c1(0x3ce)](_0x595e6b,_0x40024c);}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x215),_0x5c125b=>{const _0x44e530=_0x5e5f48;VisuMZ[_0x44e530(0x451)](_0x5c125b,_0x5c125b);const _0x1aa75f=$gameTemp[_0x44e530(0x14a)](),_0x27bdfe={'template':_0x5c125b['TemplateName'],'mapId':_0x5c125b[_0x44e530(0x2e4)]||$gameMap[_0x44e530(0x21e)](),'eventId':_0x5c125b[_0x44e530(0x30c)]||_0x1aa75f[_0x44e530(0x1f7)](),'x':_0x5c125b[_0x44e530(0x3f8)],'y':_0x5c125b['PosY'],'spawnPreserved':_0x5c125b['Preserve'],'spawnEventId':$gameMap[_0x44e530(0x2a3)]['length']+0x3e8},_0x4d40d=_0x5c125b[_0x44e530(0x4c2)]||0x0,_0xdf020c=$gameMap[_0x44e530(0x309)](_0x27bdfe,_0x5c125b[_0x44e530(0x23b)],_0x5c125b[_0x44e530(0x239)]);_0x4d40d&&$gameSwitches[_0x44e530(0x3ce)](_0x4d40d,!!_0xdf020c);}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x484),_0x38177a=>{const _0xd09c36=_0x5e5f48;VisuMZ[_0xd09c36(0x451)](_0x38177a,_0x38177a);const _0x5557cf=$gameTemp['getLastPluginCommandInterpreter'](),_0x159fde={'template':_0x38177a[_0xd09c36(0x483)],'mapId':_0x38177a['MapId']||$gameMap['mapId'](),'eventId':_0x38177a[_0xd09c36(0x30c)]||_0x5557cf[_0xd09c36(0x1f7)](),'x':-0x1,'y':-0x1,'spawnPreserved':_0x38177a['Preserve'],'spawnEventId':$gameMap[_0xd09c36(0x2a3)][_0xd09c36(0x2ef)]+0x3e8},_0x201f8e=_0x38177a[_0xd09c36(0x4c2)]||0x0,_0x60c519=$gameMap[_0xd09c36(0x194)](_0x159fde,_0x38177a[_0xd09c36(0x470)],_0x38177a[_0xd09c36(0x23b)],_0x38177a['Passability']);_0x201f8e&&$gameSwitches[_0xd09c36(0x3ce)](_0x201f8e,!!_0x60c519);}),PluginManager['registerCommand'](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x432),_0x279c06=>{const _0x353f29=_0x5e5f48;VisuMZ[_0x353f29(0x451)](_0x279c06,_0x279c06);const _0x596bf0=$gameTemp[_0x353f29(0x14a)](),_0x2ed2b7={'template':_0x279c06[_0x353f29(0x483)],'mapId':_0x279c06[_0x353f29(0x2e4)]||$gameMap[_0x353f29(0x21e)](),'eventId':_0x279c06[_0x353f29(0x30c)]||_0x596bf0[_0x353f29(0x1f7)](),'x':-0x1,'y':-0x1,'spawnPreserved':_0x279c06[_0x353f29(0x352)],'spawnEventId':$gameMap[_0x353f29(0x2a3)][_0x353f29(0x2ef)]+0x3e8},_0x32e37b=_0x279c06['SuccessSwitchId']||0x0,_0x59dff7=$gameMap['prepareSpawnedEventAtTerrainTag'](_0x2ed2b7,_0x279c06['TerrainTags'],_0x279c06[_0x353f29(0x23b)],_0x279c06['Passability']);_0x32e37b&&$gameSwitches[_0x353f29(0x3ce)](_0x32e37b,!!_0x59dff7);}),PluginManager[_0x5e5f48(0x13f)](pluginData['name'],_0x5e5f48(0x113),_0xb27bc=>{const _0x4226b2=_0x5e5f48;VisuMZ[_0x4226b2(0x451)](_0xb27bc,_0xb27bc);const _0x410f00=$gameTemp[_0x4226b2(0x14a)]();$gameMap[_0x4226b2(0x121)](_0xb27bc['EventID']||_0x410f00[_0x4226b2(0x1f7)]());}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],_0x5e5f48(0x14b),_0x42941b=>{const _0x13f90d=_0x5e5f48;VisuMZ['ConvertParams'](_0x42941b,_0x42941b);const _0x6be77c=_0x42941b[_0x13f90d(0x3f8)],_0x507d3e=_0x42941b[_0x13f90d(0x449)];$gameMap[_0x13f90d(0x307)](_0x6be77c,_0x507d3e);}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],'SpawnEventDespawnRegions',_0x5cb13e=>{const _0x295b8d=_0x5e5f48;VisuMZ[_0x295b8d(0x451)](_0x5cb13e,_0x5cb13e),$gameMap['despawnRegions'](_0x5cb13e[_0x295b8d(0x470)]);}),PluginManager['registerCommand'](pluginData[_0x5e5f48(0x37d)],'SpawnEventDespawnTerrainTags',_0x3dbe9f=>{const _0x3013db=_0x5e5f48;VisuMZ[_0x3013db(0x451)](_0x3dbe9f,_0x3dbe9f),$gameMap[_0x3013db(0x16a)](_0x3dbe9f[_0x3013db(0x323)]);}),PluginManager[_0x5e5f48(0x13f)](pluginData[_0x5e5f48(0x37d)],'SpawnEventDespawnEverything',_0x3c5a39=>{const _0x523566=_0x5e5f48;VisuMZ[_0x523566(0x451)](_0x3c5a39,_0x3c5a39),$gameMap[_0x523566(0x139)]();}),VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x149)]=Scene_Boot['prototype'][_0x5e5f48(0x448)],Scene_Boot['prototype'][_0x5e5f48(0x448)]=function(){const _0x1fcac4=_0x5e5f48;VisuMZ[_0x1fcac4(0x248)]['Scene_Boot_onDatabaseLoaded']['call'](this),this[_0x1fcac4(0x11d)](),this[_0x1fcac4(0x209)]();if(VisuMZ[_0x1fcac4(0x248)][_0x1fcac4(0x1d0)])VisuMZ[_0x1fcac4(0x248)][_0x1fcac4(0x1d0)][_0x1fcac4(0x2b9)]();},VisuMZ[_0x5e5f48(0x1a1)]=[],VisuMZ[_0x5e5f48(0x2f0)]={},Scene_Boot[_0x5e5f48(0x363)][_0x5e5f48(0x11d)]=function(){const _0x3fc956=_0x5e5f48;if(DataManager[_0x3fc956(0x401)]()||DataManager[_0x3fc956(0x161)]())return;const _0x42ecdb=VisuMZ['EventsMoveCore'][_0x3fc956(0x1e5)][_0x3fc956(0x125)],_0x2644af=_0x42ecdb['PreloadMaps'][_0x3fc956(0x245)](0x0);for(const _0x317260 of _0x42ecdb['List']){_0x317260[_0x3fc956(0x201)]=_0x317260[_0x3fc956(0x201)][_0x3fc956(0x3ff)]()[_0x3fc956(0x372)](),VisuMZ[_0x3fc956(0x2f0)][_0x317260[_0x3fc956(0x201)]]=_0x317260;if(!_0x2644af[_0x3fc956(0x2d5)](_0x317260['MapID']))_0x2644af[_0x3fc956(0x4bf)](_0x317260[_0x3fc956(0x27d)]);}for(const _0x4b69f5 of _0x2644af){if(VisuMZ[_0x3fc956(0x1a1)][_0x4b69f5])continue;const _0x1666b7=_0x3fc956(0x479)[_0x3fc956(0x2ac)](_0x4b69f5[_0x3fc956(0x317)](0x3)),_0x1d3216=_0x3fc956(0x3e9)[_0x3fc956(0x2ac)](_0x4b69f5);DataManager[_0x3fc956(0x1a3)](_0x1d3216,_0x1666b7),setTimeout(this['VisuMZ_Setup_Preload_Map'][_0x3fc956(0x3e5)](this,_0x4b69f5,_0x1d3216),0x64);}},Scene_Boot['prototype'][_0x5e5f48(0x418)]=function(_0x525192,_0xab6176){const _0x16a7a7=_0x5e5f48;window[_0xab6176]?(VisuMZ[_0x16a7a7(0x1a1)][_0x525192]=window[_0xab6176],window[_0xab6176]=undefined):setTimeout(this[_0x16a7a7(0x418)][_0x16a7a7(0x3e5)](this,_0x525192,_0xab6176),0x64);},VisuMZ['AdvancedSwitches']=[],VisuMZ['SelfSwitches']=[],VisuMZ[_0x5e5f48(0x1fa)]=[],VisuMZ[_0x5e5f48(0x320)]=[],Scene_Boot[_0x5e5f48(0x363)][_0x5e5f48(0x209)]=function(){const _0x462890=_0x5e5f48;for(let _0x348bc8=0x1;_0x348bc8<$dataSystem[_0x462890(0x156)][_0x462890(0x2ef)];_0x348bc8++){if($dataSystem['switches'][_0x348bc8][_0x462890(0x1b1)](/<JS>\s*([\s\S]*)\s*<\/JS>/i))VisuMZ['AdvancedSwitches'][_0x462890(0x4bf)](_0x348bc8);if($dataSystem[_0x462890(0x156)][_0x348bc8]['match'](/<SELF>/i))VisuMZ[_0x462890(0x4b9)]['push'](_0x348bc8);}for(let _0xa81700=0x1;_0xa81700<$dataSystem[_0x462890(0x32a)][_0x462890(0x2ef)];_0xa81700++){if($dataSystem[_0x462890(0x32a)][_0xa81700]['match'](/<JS>\s*([\s\S]*)\s*<\/JS>/i))VisuMZ[_0x462890(0x1fa)]['push'](_0xa81700);if($dataSystem[_0x462890(0x32a)][_0xa81700]['match'](/<SELF>/i))VisuMZ['SelfVariables'][_0x462890(0x4bf)](_0xa81700);}},VisuMZ['EventsMoveCore'][_0x5e5f48(0x1d0)]={},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x1d0)][_0x5e5f48(0x2b9)]=function(){const _0x4ae097=_0x5e5f48;this[_0x4ae097(0x144)]=new Game_CPCInterpreter(),this[_0x4ae097(0x42e)]();},VisuMZ[_0x5e5f48(0x248)]['CustomPageConditions'][_0x5e5f48(0x42e)]=function(){const _0x2ff9cb=_0x5e5f48;this[_0x2ff9cb(0x36b)]=[];for(const _0xbc741b of $dataCommonEvents){if(!_0xbc741b)continue;VisuMZ[_0x2ff9cb(0x248)][_0x2ff9cb(0x1d0)]['loadCPC'](_0xbc741b);if(_0xbc741b['CPC'][_0x2ff9cb(0x2ef)]>0x0)this['_commonEvents'][_0x2ff9cb(0x4bf)](_0xbc741b['id']);}},VisuMZ['EventsMoveCore'][_0x5e5f48(0x1d0)][_0x5e5f48(0x4e4)]=function(_0x497b7c,_0x49739b){const _0x4d1116=_0x5e5f48;return this[_0x4d1116(0x144)]['setup'](_0x497b7c,_0x49739b),this[_0x4d1116(0x144)][_0x4d1116(0x443)](),this[_0x4d1116(0x144)][_0x4d1116(0x3a1)];},VisuMZ[_0x5e5f48(0x248)]['CustomPageConditions']['loadCPC']=function(_0xdbfe8e){const _0x49033e=_0x5e5f48;let _0x2d4566=![];_0xdbfe8e[_0x49033e(0x280)]=[];for(const _0x12b178 of _0xdbfe8e[_0x49033e(0x3ee)]){if([0x6c,0x198]['includes'](_0x12b178[_0x49033e(0x212)])){const _0x56f8a5=_0x12b178[_0x49033e(0x273)][0x0];if(_0x56f8a5[_0x49033e(0x1b1)](/<PAGE (?:CONDITION|CONDITIONS)>/i))_0x2d4566=!![];else _0x56f8a5[_0x49033e(0x1b1)](/<\/PAGE (?:CONDITION|CONDITIONS)>/i)&&(_0x2d4566=![]);}_0x2d4566&&_0xdbfe8e[_0x49033e(0x280)][_0x49033e(0x4bf)](_0x12b178);}},getSelfSwitchValue=function(_0x16e9f7,_0x948e7,_0x17846e){const _0x57fc3b=_0x5e5f48;let _0x2cbb00=[_0x16e9f7,_0x948e7,_0x57fc3b(0x22a)[_0x57fc3b(0x2ac)](_0x17846e)];return typeof _0x17846e===_0x57fc3b(0x1a5)&&(_0x2cbb00=[_0x16e9f7,_0x948e7,_0x17846e['toUpperCase']()[_0x57fc3b(0x372)]()]),$gameSelfSwitches['value'](_0x2cbb00);},getSelfVariableValue=function(_0x1fcb3f,_0x30512b,_0x11e4b5){const _0xb836b6=_0x5e5f48,_0x55f0bc=[_0x1fcb3f,_0x30512b,_0xb836b6(0x338)[_0xb836b6(0x2ac)](_0x11e4b5)];return $gameSelfSwitches[_0xb836b6(0x4c4)](_0x55f0bc);},setSelfSwitchValue=function(_0x50e30c,_0x27ee38,_0x279a00,_0x4de9a7){const _0x520add=_0x5e5f48;let _0x2ac49c=[_0x50e30c,_0x27ee38,_0x520add(0x22a)[_0x520add(0x2ac)](_0x279a00)];typeof _0x279a00===_0x520add(0x1a5)&&(_0x2ac49c=[_0x50e30c,_0x27ee38,_0x279a00[_0x520add(0x3ff)]()[_0x520add(0x372)]()]);},setSelfVariableValue=function(_0x494d43,_0x216d03,_0x5b4726,_0x49d879){const _0x3c6d8a=[_0x494d43,_0x216d03,'Self\x20Variable\x20%1'['format'](_0x5b4726)];},DataManager['isAdvancedSwitch']=function(_0xdb56b4){const _0xf8e800=_0x5e5f48;if(SceneManager[_0xf8e800(0x15b)][_0xf8e800(0x205)]===Scene_Debug)return![];return VisuMZ[_0xf8e800(0x27a)][_0xf8e800(0x2d5)](_0xdb56b4);},DataManager[_0x5e5f48(0x4ab)]=function(_0x5ee016){const _0x3c9916=_0x5e5f48;if(SceneManager[_0x3c9916(0x15b)][_0x3c9916(0x205)]===Scene_Debug)return![];return VisuMZ[_0x3c9916(0x1fa)]['includes'](_0x5ee016);},DataManager[_0x5e5f48(0x3d9)]=function(_0x1c6548){const _0x11872c=_0x5e5f48;if(SceneManager['_scene'][_0x11872c(0x205)]===Scene_Debug)return![];return VisuMZ[_0x11872c(0x4b9)]['includes'](_0x1c6548);},DataManager[_0x5e5f48(0x1b6)]=function(_0x3ec974){const _0xc317f1=_0x5e5f48;if(SceneManager['_scene'][_0xc317f1(0x205)]===Scene_Debug)return![];return VisuMZ[_0xc317f1(0x320)][_0xc317f1(0x2d5)](_0x3ec974);},VisuMZ['EventsMoveCore']['Game_Temp_setDestination']=Game_Temp[_0x5e5f48(0x363)][_0x5e5f48(0x456)],Game_Temp[_0x5e5f48(0x363)]['setDestination']=function(_0x439b6d,_0x6b70f2){const _0x19423c=_0x5e5f48;if(this[_0x19423c(0x2b1)](_0x439b6d,_0x6b70f2))return;VisuMZ[_0x19423c(0x248)]['Game_Temp_setDestination'][_0x19423c(0x2a6)](this,_0x439b6d,_0x6b70f2);},Game_Temp[_0x5e5f48(0x363)][_0x5e5f48(0x2b1)]=function(_0x3da90c,_0x1b75bb){const _0x1a4470=_0x5e5f48,_0x333886=$gameMap[_0x1a4470(0x325)](_0x3da90c,_0x1b75bb);for(const _0x15da24 of _0x333886){if(_0x15da24&&_0x15da24[_0x1a4470(0x258)]())return _0x15da24[_0x1a4470(0x285)](),!![];}return![];},Game_Temp['prototype'][_0x5e5f48(0x37c)]=function(_0x28ae95){const _0x7f9847=_0x5e5f48;this[_0x7f9847(0x159)]=_0x28ae95;},Game_Temp[_0x5e5f48(0x363)][_0x5e5f48(0x14a)]=function(){const _0x414653=_0x5e5f48;return this[_0x414653(0x159)];},Game_Temp[_0x5e5f48(0x363)]['registerSelfTarget']=function(_0x1bfff7){this['_selfTarget']=_0x1bfff7;},Game_Temp[_0x5e5f48(0x363)][_0x5e5f48(0x29e)]=function(){const _0x525935=_0x5e5f48;this[_0x525935(0x34b)]=undefined;},Game_Temp[_0x5e5f48(0x363)][_0x5e5f48(0x23a)]=function(){const _0x510519=_0x5e5f48;return this[_0x510519(0x34b)];},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x2ad)]=Game_System[_0x5e5f48(0x363)][_0x5e5f48(0x2b9)],Game_System[_0x5e5f48(0x363)][_0x5e5f48(0x2b9)]=function(){const _0x1087de=_0x5e5f48;VisuMZ['EventsMoveCore'][_0x1087de(0x2ad)]['call'](this),this['initEventsMoveCore'](),this[_0x1087de(0x141)]();},Game_System[_0x5e5f48(0x363)][_0x5e5f48(0x2b5)]=function(){const _0x1121e5=_0x5e5f48;this[_0x1121e5(0x166)]={'DashingEnable':!![],'EventAutoMovement':!![],'VisibleEventLabels':!![]},this[_0x1121e5(0x298)]={},this[_0x1121e5(0x377)]=[],this[_0x1121e5(0x134)]={},this[_0x1121e5(0x278)]={},this['_DisablePlayerControl']=![],this['_PlayerDiagonalSetting']='default';},Game_System['prototype']['isDashingEnabled']=function(){const _0x37ae91=_0x5e5f48;if(this[_0x37ae91(0x166)]===undefined)this['initEventsMoveCore']();if(this[_0x37ae91(0x166)][_0x37ae91(0x4ba)]===undefined)this[_0x37ae91(0x2b5)]();return this['_EventsMoveCoreSettings'][_0x37ae91(0x4ba)];},Game_System[_0x5e5f48(0x363)][_0x5e5f48(0x434)]=function(_0x59aa64){const _0x43a434=_0x5e5f48;if(this[_0x43a434(0x166)]===undefined)this[_0x43a434(0x2b5)]();if(this[_0x43a434(0x166)]['DashingEnable']===undefined)this[_0x43a434(0x2b5)]();this[_0x43a434(0x166)]['DashingEnable']=_0x59aa64;},Game_System[_0x5e5f48(0x363)][_0x5e5f48(0x28a)]=function(){const _0x35c306=_0x5e5f48;if(this[_0x35c306(0x166)]===undefined)this[_0x35c306(0x2b5)]();if(this[_0x35c306(0x166)][_0x35c306(0x498)]===undefined)this[_0x35c306(0x2b5)]();return this[_0x35c306(0x166)][_0x35c306(0x498)];},Game_System[_0x5e5f48(0x363)]['setAllowEventAutoMovement']=function(_0x5c309b){const _0x3786c6=_0x5e5f48;if(this[_0x3786c6(0x166)]===undefined)this[_0x3786c6(0x2b5)]();if(this[_0x3786c6(0x166)]['EventAutoMovement']===undefined)this[_0x3786c6(0x2b5)]();this[_0x3786c6(0x166)][_0x3786c6(0x498)]=_0x5c309b;},Game_System[_0x5e5f48(0x363)][_0x5e5f48(0x4e6)]=function(){const _0x2136d6=_0x5e5f48;if(this[_0x2136d6(0x166)]===undefined)this[_0x2136d6(0x2b5)]();if(this[_0x2136d6(0x166)][_0x2136d6(0x27e)]===undefined)this[_0x2136d6(0x2b5)]();return this['_EventsMoveCoreSettings'][_0x2136d6(0x27e)];},Game_System['prototype'][_0x5e5f48(0x175)]=function(_0x2fbcbf){const _0x13040a=_0x5e5f48;if(this['_EventsMoveCoreSettings']===undefined)this[_0x13040a(0x2b5)]();if(this['_EventsMoveCoreSettings'][_0x13040a(0x27e)]===undefined)this['initEventsMoveCore']();this[_0x13040a(0x166)][_0x13040a(0x27e)]=_0x2fbcbf;},Game_System[_0x5e5f48(0x363)][_0x5e5f48(0x328)]=function(){const _0x3a7858=_0x5e5f48;return this[_0x3a7858(0x414)]===undefined&&(this[_0x3a7858(0x414)]=![]),this[_0x3a7858(0x414)];},Game_System[_0x5e5f48(0x363)][_0x5e5f48(0x25a)]=function(_0x4c7130){const _0x4e8968=_0x5e5f48;this[_0x4e8968(0x414)]=_0x4c7130;},Game_System[_0x5e5f48(0x363)][_0x5e5f48(0x371)]=function(){const _0x169323=_0x5e5f48;return this[_0x169323(0x2cf)];},Game_System[_0x5e5f48(0x363)]['setPlayerDiagonalSetting']=function(_0x2604b9){const _0x430f0a=_0x5e5f48;this['_PlayerDiagonalSetting']=String(_0x2604b9)[_0x430f0a(0x4c9)]()['trim']();},Game_System[_0x5e5f48(0x363)][_0x5e5f48(0x23f)]=function(_0x4ee91e){const _0x4ce041=_0x5e5f48;if(this[_0x4ce041(0x298)]===undefined)this['initEventsMoveCore']();if(!_0x4ee91e)return null;if(_0x4ee91e===$gamePlayer)return this[_0x4ce041(0x298)][_0x4ce041(0x127)];else{const _0x56e32e=VisuMZ['EventsMoveCore'][_0x4ce041(0x1e5)],_0x1b9ae9=_0x4ce041(0x4a6)[_0x4ce041(0x2ac)](_0x4ee91e[_0x4ce041(0x1db)],_0x4ee91e[_0x4ce041(0x21a)]);return this[_0x4ce041(0x298)][_0x1b9ae9]=this[_0x4ce041(0x298)][_0x1b9ae9]||{'iconIndex':0x0,'bufferX':_0x56e32e[_0x4ce041(0x2db)][_0x4ce041(0x386)],'bufferY':_0x56e32e[_0x4ce041(0x2db)][_0x4ce041(0x13b)],'blendMode':_0x56e32e[_0x4ce041(0x2db)][_0x4ce041(0x133)]},this[_0x4ce041(0x298)][_0x1b9ae9];}},Game_System['prototype'][_0x5e5f48(0x2b3)]=function(_0x151b9c,_0x501f87,_0x45fbff,_0x440007,_0x2af91c){const _0x253828=_0x5e5f48;if(this['_EventIcons']===undefined)this['initEventsMoveCore']();const _0x2131f3=_0x151b9c===$gamePlayer?_0x253828(0x127):_0x253828(0x4a6)['format'](_0x151b9c[_0x253828(0x1db)],_0x151b9c[_0x253828(0x21a)]);this[_0x253828(0x298)][_0x2131f3]={'iconIndex':_0x501f87,'bufferX':_0x45fbff,'bufferY':_0x440007,'blendMode':_0x2af91c};},Game_System[_0x5e5f48(0x363)]['setEventIconDataKey']=function(_0x44e3e6,_0x4325dd,_0x57e703,_0x20ac33,_0xfeaf47,_0x44926f){const _0x31603c=_0x5e5f48;if(this[_0x31603c(0x298)]===undefined)this[_0x31603c(0x2b5)]();const _0x1d8cf1=_0x31603c(0x4a6)[_0x31603c(0x2ac)](_0x44e3e6,_0x4325dd);this[_0x31603c(0x298)][_0x1d8cf1]={'iconIndex':_0x57e703,'bufferX':_0x20ac33,'bufferY':_0xfeaf47,'blendMode':_0x44926f};},Game_System[_0x5e5f48(0x363)][_0x5e5f48(0x425)]=function(_0x31629e){const _0xc0b356=_0x5e5f48;if(this[_0xc0b356(0x298)]===undefined)this[_0xc0b356(0x2b5)]();if(!_0x31629e)return null;_0x31629e===$gamePlayer?delete this[_0xc0b356(0x298)][_0xc0b356(0x127)]:this[_0xc0b356(0x3e0)](_0x31629e[_0xc0b356(0x1db)],_0x31629e['_eventId']);},Game_System[_0x5e5f48(0x363)][_0x5e5f48(0x3e0)]=function(_0xe4d3d2,_0x494ad8){const _0x1dcbd2=_0x5e5f48;if(this['_EventIcons']===undefined)this['initEventsMoveCore']();const _0x235a0a=_0x1dcbd2(0x4a6)[_0x1dcbd2(0x2ac)](_0xe4d3d2,_0x494ad8);delete this['_EventIcons'][_0x235a0a];},Game_System[_0x5e5f48(0x363)][_0x5e5f48(0x391)]=function(_0xfb7bcc){const _0x170d9e=_0x5e5f48;if(this[_0x170d9e(0x278)]===undefined)this[_0x170d9e(0x2b5)]();if(!_0xfb7bcc)return null;const _0x429d2e=_0x170d9e(0x4a6)[_0x170d9e(0x2ac)](_0xfb7bcc[_0x170d9e(0x1db)],_0xfb7bcc[_0x170d9e(0x21a)]);return this[_0x170d9e(0x278)][_0x429d2e];},Game_System[_0x5e5f48(0x363)][_0x5e5f48(0x318)]=function(_0x467d39){const _0x4c6825=_0x5e5f48;if(this['_SavedEventLocations']===undefined)this[_0x4c6825(0x2b5)]();if(!_0x467d39)return;const _0x4748b6=_0x4c6825(0x4a6)[_0x4c6825(0x2ac)](_0x467d39['_mapId'],_0x467d39[_0x4c6825(0x21a)]);this['_SavedEventLocations'][_0x4748b6]={'direction':_0x467d39[_0x4c6825(0x2dd)](),'x':Math[_0x4c6825(0x1e9)](_0x467d39['x']),'y':Math[_0x4c6825(0x1e9)](_0x467d39['y']),'pageIndex':_0x467d39[_0x4c6825(0x3d3)],'moveRouteIndex':_0x467d39[_0x4c6825(0x12c)]};},Game_System[_0x5e5f48(0x363)]['deleteSavedEventLocation']=function(_0x54eedf){const _0x777078=_0x5e5f48;if(this['_SavedEventLocations']===undefined)this[_0x777078(0x2b5)]();if(!_0x54eedf)return;this[_0x777078(0x2ea)](_0x54eedf[_0x777078(0x1db)],_0x54eedf[_0x777078(0x21a)]);},Game_System[_0x5e5f48(0x363)]['deleteSavedEventLocationKey']=function(_0x2af0a6,_0x667090){const _0x26add5=_0x5e5f48;if(this[_0x26add5(0x278)]===undefined)this[_0x26add5(0x2b5)]();const _0x5ab06a='Map%1-Event%2'[_0x26add5(0x2ac)](_0x2af0a6,_0x667090);delete this[_0x26add5(0x278)][_0x5ab06a];},Game_System[_0x5e5f48(0x363)][_0x5e5f48(0x4b2)]=function(_0x194f88,_0x250750,_0x176f71,_0x5b7d43,_0x2edba6,_0x3bdc55,_0x4c736d){const _0x30039b=_0x5e5f48;if(this[_0x30039b(0x278)]===undefined)this[_0x30039b(0x2b5)]();const _0x46c07b=_0x30039b(0x4a6)['format'](_0x194f88,_0x250750);this[_0x30039b(0x278)][_0x46c07b]={'direction':_0x2edba6,'x':Math[_0x30039b(0x1e9)](_0x176f71),'y':Math['round'](_0x5b7d43),'pageIndex':_0x3bdc55,'moveRouteIndex':_0x4c736d};},Game_System['prototype'][_0x5e5f48(0x491)]=function(_0x26aaa3){const _0x3693cc=_0x5e5f48;if(this[_0x3693cc(0x134)]===undefined)this[_0x3693cc(0x2b5)]();if(!_0x26aaa3)return;const _0x4e4f25=_0x3693cc(0x4a6)[_0x3693cc(0x2ac)](_0x26aaa3['_mapId'],_0x26aaa3[_0x3693cc(0x21a)]);return this[_0x3693cc(0x134)][_0x4e4f25];},Game_System[_0x5e5f48(0x363)][_0x5e5f48(0x32c)]=function(_0x2f102d,_0xf541f5,_0x46324b,_0x3ef4fb,_0x14f0d3){const _0x384582=_0x5e5f48;if(this[_0x384582(0x134)]===undefined)this[_0x384582(0x2b5)]();const _0x59783f=_0x384582(0x4a6)['format'](_0x2f102d,_0xf541f5);this[_0x384582(0x134)][_0x59783f]={'template':_0x46324b,'mapId':_0x3ef4fb,'eventId':_0x14f0d3};},Game_System[_0x5e5f48(0x363)][_0x5e5f48(0x4f3)]=function(_0x1687c7,_0x1c27d4){const _0x47ea27=_0x5e5f48;if(this[_0x47ea27(0x134)]===undefined)this[_0x47ea27(0x2b5)]();const _0x5d8f60=_0x47ea27(0x4a6)['format'](_0x1687c7,_0x1c27d4);delete this[_0x47ea27(0x134)][_0x5d8f60];},Game_System[_0x5e5f48(0x363)][_0x5e5f48(0x2bf)]=function(_0x1520af){const _0x2fd2fc=_0x5e5f48;if(this[_0x2fd2fc(0x377)]===undefined)this['initEventsMoveCore']();return this['_MapSpawnedEventData'][_0x1520af]=this[_0x2fd2fc(0x377)][_0x1520af]||[],this[_0x2fd2fc(0x377)][_0x1520af];},Game_System[_0x5e5f48(0x363)][_0x5e5f48(0x271)]=function(_0x40fc20){const _0x442980=_0x5e5f48,_0x18cd6a=this['getMapSpawnedEventData'](_0x40fc20);for(const _0x3fef6a of _0x18cd6a){if(!_0x3fef6a)continue;if(_0x3fef6a[_0x442980(0x33b)])continue;const _0x5ded65=_0x18cd6a[_0x442980(0x436)](_0x3fef6a);_0x18cd6a[_0x5ded65]=null;}},Game_System[_0x5e5f48(0x363)][_0x5e5f48(0x141)]=function(){const _0x36c7b0=_0x5e5f48;this['_followerControlID']=0x0,this[_0x36c7b0(0x2da)]=![];},Game_System['prototype'][_0x5e5f48(0x393)]=function(){const _0x58338e=_0x5e5f48;if(this[_0x58338e(0x324)]===undefined)this['initFollowerController']();return this['_followerControlID'];},Game_System[_0x5e5f48(0x363)]['setControlledFollowerID']=function(_0x597c5b){const _0x25b29a=_0x5e5f48;if(this[_0x25b29a(0x324)]===undefined)this[_0x25b29a(0x141)]();this[_0x25b29a(0x324)]=_0x597c5b;;},VisuMZ[_0x5e5f48(0x248)]['Game_Interpreter_character']=Game_Interpreter[_0x5e5f48(0x363)][_0x5e5f48(0x15c)],Game_Interpreter['prototype'][_0x5e5f48(0x15c)]=function(_0x1ffba4){const _0x36f271=_0x5e5f48;if(!$gameParty['inBattle']()&&_0x1ffba4<0x0){let _0x32e791=$gameSystem[_0x36f271(0x393)]();if(_0x32e791>0x0)return $gamePlayer[_0x36f271(0x3a3)]()['follower'](_0x32e791-0x1);}return VisuMZ[_0x36f271(0x248)][_0x36f271(0x45f)][_0x36f271(0x2a6)](this,_0x1ffba4);},Game_System['prototype']['isStopFollowerChasing']=function(){const _0xcf8b4e=_0x5e5f48;if(this[_0xcf8b4e(0x2da)]===undefined)this[_0xcf8b4e(0x141)]();return this['_followerChaseOff'];},Game_System[_0x5e5f48(0x363)][_0x5e5f48(0x3f6)]=function(_0x1508e9){const _0x109fce=_0x5e5f48;if(this['_followerChaseOff']===undefined)this[_0x109fce(0x141)]();this[_0x109fce(0x2da)]=_0x1508e9;;},VisuMZ['EventsMoveCore'][_0x5e5f48(0x132)]=Game_Timer[_0x5e5f48(0x363)][_0x5e5f48(0x2b9)],Game_Timer[_0x5e5f48(0x363)][_0x5e5f48(0x2b9)]=function(){const _0x4d7f86=_0x5e5f48;VisuMZ['EventsMoveCore']['Game_Timer_initialize']['call'](this),this[_0x4d7f86(0x2b5)]();},Game_Timer[_0x5e5f48(0x363)][_0x5e5f48(0x2b5)]=function(){const _0x4944d6=_0x5e5f48;this[_0x4944d6(0x11e)]=![],this[_0x4944d6(0x257)]=-0x1,this[_0x4944d6(0x2a5)]=0x0;},Game_Timer[_0x5e5f48(0x363)][_0x5e5f48(0x169)]=function(_0x4e2be7){const _0x32c331=_0x5e5f48;if(!_0x4e2be7)return;if(!this[_0x32c331(0x279)])return;if(this['_paused'])return;if(this[_0x32c331(0x262)]<=0x0)return;if(this[_0x32c331(0x257)]===undefined)this[_0x32c331(0x2b5)]();this[_0x32c331(0x262)]+=this['_speed'],this[_0x32c331(0x262)]<=0x0&&this[_0x32c331(0x24c)]();},VisuMZ[_0x5e5f48(0x248)]['Game_Timer_start']=Game_Timer[_0x5e5f48(0x363)][_0x5e5f48(0x2de)],Game_Timer[_0x5e5f48(0x363)]['start']=function(_0x36ba6f){const _0x2464ed=_0x5e5f48;VisuMZ[_0x2464ed(0x248)][_0x2464ed(0x225)][_0x2464ed(0x2a6)](this,_0x36ba6f);if(this['_paused']===undefined)this[_0x2464ed(0x2b5)]();this['_paused']=![];},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x368)]=Game_Timer[_0x5e5f48(0x363)][_0x5e5f48(0x38b)],Game_Timer['prototype'][_0x5e5f48(0x38b)]=function(){const _0x22b3fe=_0x5e5f48;VisuMZ[_0x22b3fe(0x248)]['Game_Timer_stop']['call'](this);if(this[_0x22b3fe(0x11e)]===undefined)this['initEventsMoveCore']();this[_0x22b3fe(0x11e)]=![];},Game_Timer[_0x5e5f48(0x363)][_0x5e5f48(0x342)]=function(){const _0x159412=_0x5e5f48;if(this['_frames']<=0x0)return;this[_0x159412(0x11e)]=!![],this[_0x159412(0x279)]=!![];},Game_Timer[_0x5e5f48(0x363)][_0x5e5f48(0x3b6)]=function(){const _0x1d307a=_0x5e5f48;if(this[_0x1d307a(0x262)]<=0x0)return;this[_0x1d307a(0x11e)]=![],this['_working']=!![];},Game_Timer['prototype'][_0x5e5f48(0x210)]=function(_0x1104f2){const _0x50af75=_0x5e5f48;this[_0x50af75(0x262)]=this[_0x50af75(0x262)]||0x0,this[_0x50af75(0x262)]+=_0x1104f2,this['_working']=!![],this['_frames']=Math[_0x50af75(0x27f)](0x1,this[_0x50af75(0x262)]);},Game_Timer[_0x5e5f48(0x363)][_0x5e5f48(0x453)]=function(_0x186761){const _0xe021d3=_0x5e5f48;this[_0xe021d3(0x262)]=this[_0xe021d3(0x262)]||0x0,this['_frames']=_0x186761,this[_0xe021d3(0x279)]=!![],this[_0xe021d3(0x262)]=Math[_0xe021d3(0x27f)](0x1,this[_0xe021d3(0x262)]);},Game_Timer['prototype']['changeSpeed']=function(_0x7c570a){const _0x1e11a7=_0x5e5f48;this['_speed']=_0x7c570a,this[_0x1e11a7(0x279)]=!![],_0x7c570a>0x0&&(this['_frames']=Math[_0x1e11a7(0x27f)](this[_0x1e11a7(0x262)],0x1));},Game_Timer[_0x5e5f48(0x363)]['setCommonEvent']=function(_0x43c296){const _0x583ede=_0x5e5f48;if(this[_0x583ede(0x2a5)]===undefined)this[_0x583ede(0x2b5)]();this[_0x583ede(0x2a5)]=_0x43c296;},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x415)]=Game_Timer['prototype'][_0x5e5f48(0x24c)],Game_Timer[_0x5e5f48(0x363)]['onExpire']=function(){const _0x47936e=_0x5e5f48;if(this[_0x47936e(0x2a5)]===undefined)this[_0x47936e(0x2b5)]();this[_0x47936e(0x2a5)]?$gameTemp['reserveCommonEvent'](this[_0x47936e(0x2a5)]):VisuMZ[_0x47936e(0x248)][_0x47936e(0x415)][_0x47936e(0x2a6)](this);},VisuMZ['EventsMoveCore']['Game_Message_add']=Game_Message[_0x5e5f48(0x363)][_0x5e5f48(0x1c3)],Game_Message[_0x5e5f48(0x363)][_0x5e5f48(0x1c3)]=function(_0x16c310){const _0x27dba2=_0x5e5f48;VisuMZ[_0x27dba2(0x248)][_0x27dba2(0x427)][_0x27dba2(0x2a6)](this,_0x16c310),this[_0x27dba2(0x1b9)]=$gameTemp[_0x27dba2(0x23a)]();},Game_Message[_0x5e5f48(0x363)]['registerSelfEvent']=function(){const _0x23ab0b=_0x5e5f48;$gameTemp[_0x23ab0b(0x152)](this[_0x23ab0b(0x1b9)]);},VisuMZ[_0x5e5f48(0x248)]['Game_Switches_value']=Game_Switches[_0x5e5f48(0x363)]['value'],Game_Switches[_0x5e5f48(0x363)][_0x5e5f48(0x4c4)]=function(_0xf4977d){const _0x315a05=_0x5e5f48;if(DataManager[_0x315a05(0x4eb)](_0xf4977d))return!!this[_0x315a05(0x492)](_0xf4977d);else return DataManager[_0x315a05(0x3d9)](_0xf4977d)?!!this[_0x315a05(0x40f)](_0xf4977d):VisuMZ[_0x315a05(0x248)][_0x315a05(0x389)][_0x315a05(0x2a6)](this,_0xf4977d);},Game_Switches[_0x5e5f48(0x12d)]={},Game_Switches[_0x5e5f48(0x363)][_0x5e5f48(0x492)]=function(_0x2f6357){const _0x3d7c3d=_0x5e5f48;if(!Game_Switches['advancedFunc'][_0x2f6357]){$dataSystem[_0x3d7c3d(0x156)][_0x2f6357][_0x3d7c3d(0x1b1)](/<JS>\s*([\s\S]*)\s*<\/JS>/i);const _0x3028bb='return\x20%1'[_0x3d7c3d(0x2ac)](String(RegExp['$1']));Game_Switches[_0x3d7c3d(0x12d)][_0x2f6357]=new Function(_0x3d7c3d(0x250),_0x3028bb);}const _0x5bfba0=$gameTemp[_0x3d7c3d(0x23a)]()||this;return Game_Switches[_0x3d7c3d(0x12d)][_0x2f6357]['call'](_0x5bfba0,_0x2f6357);},Game_Switches[_0x5e5f48(0x363)][_0x5e5f48(0x40f)]=function(_0x1e633d){const _0x8f2c4f=_0x5e5f48,_0x5003ee=$gameTemp[_0x8f2c4f(0x23a)]()||this;if(_0x5003ee[_0x8f2c4f(0x205)]!==Game_Event)return VisuMZ[_0x8f2c4f(0x248)][_0x8f2c4f(0x389)][_0x8f2c4f(0x2a6)](this,_0x1e633d);else{const _0x2eedfb=[_0x5003ee[_0x8f2c4f(0x1db)],_0x5003ee[_0x8f2c4f(0x21a)],_0x8f2c4f(0x22a)[_0x8f2c4f(0x2ac)](_0x1e633d)];return $gameSelfSwitches[_0x8f2c4f(0x4c4)](_0x2eedfb);}},VisuMZ['EventsMoveCore'][_0x5e5f48(0x33e)]=Game_Switches[_0x5e5f48(0x363)][_0x5e5f48(0x3ce)],Game_Switches['prototype'][_0x5e5f48(0x3ce)]=function(_0x3684a3,_0x445178){const _0x1b4938=_0x5e5f48;DataManager[_0x1b4938(0x3d9)](_0x3684a3)?this['setSelfValue'](_0x3684a3,_0x445178):VisuMZ['EventsMoveCore'][_0x1b4938(0x33e)][_0x1b4938(0x2a6)](this,_0x3684a3,_0x445178);},Game_Switches[_0x5e5f48(0x363)]['setSelfValue']=function(_0x5ec2f4,_0x280984){const _0x45194e=_0x5e5f48,_0x454810=$gameTemp[_0x45194e(0x23a)]()||this;if(_0x454810['constructor']!==Game_Event)VisuMZ[_0x45194e(0x248)][_0x45194e(0x33e)][_0x45194e(0x2a6)](this,_0x5ec2f4,_0x280984);else{const _0x415881=[_0x454810['_mapId'],_0x454810[_0x45194e(0x21a)],_0x45194e(0x22a)['format'](_0x5ec2f4)];$gameSelfSwitches[_0x45194e(0x3ce)](_0x415881,_0x280984);}},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x3a6)]=Game_Variables[_0x5e5f48(0x363)]['value'],Game_Variables['prototype'][_0x5e5f48(0x4c4)]=function(_0x37b540){const _0x251fff=_0x5e5f48;if(DataManager['isAdvancedVariable'](_0x37b540))return this[_0x251fff(0x492)](_0x37b540);else return DataManager['isSelfVariable'](_0x37b540)?this[_0x251fff(0x40f)](_0x37b540):VisuMZ['EventsMoveCore'][_0x251fff(0x3a6)][_0x251fff(0x2a6)](this,_0x37b540);},Game_Variables[_0x5e5f48(0x12d)]={},Game_Variables['prototype']['advancedValue']=function(_0x166ccf){const _0x195e82=_0x5e5f48;if(!Game_Variables[_0x195e82(0x12d)][_0x166ccf]){$dataSystem[_0x195e82(0x32a)][_0x166ccf][_0x195e82(0x1b1)](/<JS>\s*([\s\S]*)\s*<\/JS>/i);const _0x18f3fb=_0x195e82(0x1dd)[_0x195e82(0x2ac)](String(RegExp['$1']));Game_Variables[_0x195e82(0x12d)][_0x166ccf]=new Function(_0x195e82(0x2ee),_0x18f3fb);}const _0x1a4ffc=$gameTemp[_0x195e82(0x23a)]()||this;return Game_Variables[_0x195e82(0x12d)][_0x166ccf][_0x195e82(0x2a6)](_0x1a4ffc,_0x166ccf);},Game_Variables['prototype'][_0x5e5f48(0x40f)]=function(_0x2caef7){const _0x28171c=_0x5e5f48,_0x4c306e=$gameTemp['getSelfTarget']()||this;if(_0x4c306e[_0x28171c(0x205)]!==Game_Event)return VisuMZ[_0x28171c(0x248)]['Game_Variables_value'][_0x28171c(0x2a6)](this,_0x2caef7);else{const _0x342ad8=[_0x4c306e[_0x28171c(0x1db)],_0x4c306e[_0x28171c(0x21a)],'Self\x20Variable\x20%1'['format'](_0x2caef7)];return $gameSelfSwitches[_0x28171c(0x4c4)](_0x342ad8);}},VisuMZ['EventsMoveCore']['Game_Variables_setValue']=Game_Variables[_0x5e5f48(0x363)][_0x5e5f48(0x3ce)],Game_Variables[_0x5e5f48(0x363)][_0x5e5f48(0x3ce)]=function(_0x3a5048,_0x3756dd){const _0x32e3b4=_0x5e5f48;DataManager[_0x32e3b4(0x1b6)](_0x3a5048)?this[_0x32e3b4(0x15f)](_0x3a5048,_0x3756dd):VisuMZ[_0x32e3b4(0x248)][_0x32e3b4(0x3e1)][_0x32e3b4(0x2a6)](this,_0x3a5048,_0x3756dd);},Game_Variables['prototype']['setSelfValue']=function(_0x455767,_0x45ce3c){const _0x25b136=_0x5e5f48,_0x172754=$gameTemp[_0x25b136(0x23a)]()||this;if(_0x172754[_0x25b136(0x205)]!==Game_Event)VisuMZ[_0x25b136(0x248)][_0x25b136(0x3e1)]['call'](this,_0x455767,_0x45ce3c);else{const _0x213110=[_0x172754['_mapId'],_0x172754[_0x25b136(0x21a)],_0x25b136(0x338)[_0x25b136(0x2ac)](_0x455767)];$gameSelfSwitches['setValue'](_0x213110,_0x45ce3c);}},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x1d6)]=Game_SelfSwitches[_0x5e5f48(0x363)][_0x5e5f48(0x4c4)],Game_SelfSwitches[_0x5e5f48(0x363)]['value']=function(_0x53999a){const _0xa25067=_0x5e5f48;if(_0x53999a[0x2][_0xa25067(0x1b1)](/SELF/i))return this[_0xa25067(0x40f)](_0x53999a);else{return VisuMZ[_0xa25067(0x248)][_0xa25067(0x1d6)][_0xa25067(0x2a6)](this,_0x53999a);;}},Game_SelfSwitches[_0x5e5f48(0x363)][_0x5e5f48(0x40f)]=function(_0xd355b9){const _0x1839f5=_0x5e5f48;return _0xd355b9[0x2][_0x1839f5(0x1b1)](/VAR/i)?this[_0x1839f5(0x23c)][_0xd355b9]||0x0:!!this[_0x1839f5(0x23c)][_0xd355b9];},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x349)]=Game_SelfSwitches['prototype'][_0x5e5f48(0x3ce)],Game_SelfSwitches[_0x5e5f48(0x363)][_0x5e5f48(0x3ce)]=function(_0xfdd6d2,_0x432ed7){const _0x4bb775=_0x5e5f48;_0xfdd6d2[0x2][_0x4bb775(0x1b1)](/SELF/i)?this[_0x4bb775(0x15f)](_0xfdd6d2,_0x432ed7):VisuMZ[_0x4bb775(0x248)][_0x4bb775(0x349)][_0x4bb775(0x2a6)](this,_0xfdd6d2,_0x432ed7);},Game_SelfSwitches[_0x5e5f48(0x363)]['setSelfValue']=function(_0x4141dc,_0x45ea6c){const _0xf56fb3=_0x5e5f48;this[_0xf56fb3(0x23c)][_0x4141dc]=_0x4141dc[0x2][_0xf56fb3(0x1b1)](/VAR/i)?_0x45ea6c:!!_0x45ea6c,this[_0xf56fb3(0x41f)]();},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x18c)]=Game_Enemy[_0x5e5f48(0x363)][_0x5e5f48(0x476)],Game_Enemy[_0x5e5f48(0x363)][_0x5e5f48(0x476)]=function(_0x19b233){const _0x2da2e1=_0x5e5f48;$gameTemp[_0x2da2e1(0x152)](this);const _0x1a8ccf=VisuMZ[_0x2da2e1(0x248)][_0x2da2e1(0x18c)][_0x2da2e1(0x2a6)](this,_0x19b233);return $gameTemp[_0x2da2e1(0x29e)](),_0x1a8ccf;},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x43b)]=Game_Troop[_0x5e5f48(0x363)]['meetsConditions'],Game_Troop['prototype'][_0x5e5f48(0x1eb)]=function(_0x5189a4){const _0x24ecac=_0x5e5f48;$gameTemp[_0x24ecac(0x152)](this);const _0x5b0b2d=VisuMZ[_0x24ecac(0x248)]['Game_Troop_meetsConditions'][_0x24ecac(0x2a6)](this,_0x5189a4);return $gameTemp[_0x24ecac(0x29e)](),_0x5b0b2d;},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x417)]=Game_Map['prototype']['setup'],Game_Map[_0x5e5f48(0x363)]['setup']=function(_0x56849f){const _0x2d19c6=_0x5e5f48;this[_0x2d19c6(0x271)](_0x56849f),this[_0x2d19c6(0x265)](),VisuMZ[_0x2d19c6(0x248)][_0x2d19c6(0x417)]['call'](this,_0x56849f),this[_0x2d19c6(0x265)](),this[_0x2d19c6(0x3b2)](),this[_0x2d19c6(0x4aa)](),this['setupSaveEventLocations'](),this[_0x2d19c6(0x20a)](),this[_0x2d19c6(0x265)]();},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x28f)]=Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x1e8)],Game_Map['prototype'][_0x5e5f48(0x1e8)]=function(){const _0x5862fe=_0x5e5f48;VisuMZ[_0x5862fe(0x248)][_0x5862fe(0x28f)][_0x5862fe(0x2a6)](this),this['refreshIfNeeded']();},Game_Map[_0x5e5f48(0x319)]=0xc8,Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x311)]=function(){const _0x37d692=_0x5e5f48,_0x16333c=Game_Map[_0x37d692(0x319)];this[_0x37d692(0x4a2)]=this['events']()[_0x37d692(0x2ef)]>_0x16333c;if(this[_0x37d692(0x4a2)]&&$gameTemp[_0x37d692(0x12f)]()){}},Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x333)]=function(){const _0x5e1c64=_0x5e5f48;return this[_0x5e1c64(0x4a2)];},Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x265)]=function(){const _0x222568=_0x5e5f48;this[_0x222568(0x1e7)]=undefined;},Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x3b2)]=function(){const _0x2450c1=_0x5e5f48;this[_0x2450c1(0x4de)]=VisuMZ['EventsMoveCore'][_0x2450c1(0x1e5)][_0x2450c1(0x1b0)][_0x2450c1(0x45b)];const _0x5be98c=$dataMap[_0x2450c1(0x270)]||'';if(_0x5be98c['match'](/<DIAGONAL MOVEMENT: ON>/i))this[_0x2450c1(0x4de)]=!![];else _0x5be98c['match'](/<DIAGONAL MOVEMENT: OFF>/i)&&(this[_0x2450c1(0x4de)]=![]);},Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x473)]=function(){const _0x58235b=_0x5e5f48,_0x50bf31=$gameSystem[_0x58235b(0x371)]();if(_0x50bf31===_0x58235b(0x2c9))return!![];if(_0x50bf31===_0x58235b(0x430))return![];if(this[_0x58235b(0x4de)]===undefined)this[_0x58235b(0x3b2)]();return this[_0x58235b(0x4de)];},Game_Map['prototype'][_0x5e5f48(0x41b)]=function(_0x30a6ec,_0x5cd886){const _0x144775=_0x5e5f48;if([0x1,0x4,0x7]['includes'](_0x5cd886))_0x30a6ec-=0x1;if([0x3,0x6,0x9][_0x144775(0x2d5)](_0x5cd886))_0x30a6ec+=0x1;return this[_0x144775(0x3d4)](_0x30a6ec);},Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x49c)]=function(_0x4609e1,_0x31a2db){const _0x431f5a=_0x5e5f48;if([0x1,0x2,0x3][_0x431f5a(0x2d5)](_0x31a2db))_0x4609e1+=0x1;if([0x7,0x8,0x9][_0x431f5a(0x2d5)](_0x31a2db))_0x4609e1-=0x1;return this[_0x431f5a(0x249)](_0x4609e1);},Game_Map[_0x5e5f48(0x363)]['absDistance']=function(_0x3c0dde,_0x33c09,_0x21ccc3,_0x97f5ca){const _0x57e0f6=_0x5e5f48;return Math['max'](Math[_0x57e0f6(0x392)](this[_0x57e0f6(0x327)](_0x3c0dde,_0x21ccc3)),Math[_0x57e0f6(0x392)](this['deltaY'](_0x33c09,_0x97f5ca)));},Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x4aa)]=function(){const _0x42a19f=_0x5e5f48,_0x332cb0=VisuMZ[_0x42a19f(0x248)][_0x42a19f(0x1e5)][_0x42a19f(0x470)],_0x2bc5b5={},_0x367ca7=['Allow',_0x42a19f(0x242),'Dock'],_0x4c39a1=['All',_0x42a19f(0x4b7),'Player','Event',_0x42a19f(0x37e),_0x42a19f(0x2d4),_0x42a19f(0x2ca),_0x42a19f(0x4e5)];for(const _0x261503 of _0x367ca7){for(const _0x1653ba of _0x4c39a1){const _0x2741d1=_0x42a19f(0x174)[_0x42a19f(0x2ac)](_0x1653ba,_0x261503);_0x332cb0[_0x2741d1]&&(_0x2bc5b5[_0x2741d1]=_0x332cb0[_0x2741d1][_0x42a19f(0x245)](0x0));}}const _0x209b24=$dataMap['note']||'',_0x266072=_0x209b24[_0x42a19f(0x1b1)](/<(.*) (.*) REGION:[ ]*(\d+(?:\s*,\s*\d+)*)>/i);if(_0x266072)for(const _0x22c524 of _0x266072){_0x22c524[_0x42a19f(0x1b1)](/<(.*) (.*) REGION:[ ]*(\d+(?:\s*,\s*\d+)*)>/i);let _0x122929=String(RegExp['$1'])[_0x42a19f(0x4c9)]()[_0x42a19f(0x372)](),_0x274b7d=String(RegExp['$2'])['toLowerCase']()[_0x42a19f(0x372)]();const _0x3459f2=JSON[_0x42a19f(0x3b3)]('['+RegExp['$3'][_0x42a19f(0x1b1)](/\d+/g)+']');_0x122929=_0x122929['charAt'](0x0)[_0x42a19f(0x3ff)]()+_0x122929[_0x42a19f(0x245)](0x1),_0x274b7d=_0x274b7d[_0x42a19f(0x18f)](0x0)['toUpperCase']()+_0x274b7d[_0x42a19f(0x245)](0x1);const _0x50fa1c='%1%2'[_0x42a19f(0x2ac)](_0x122929,_0x274b7d);if(_0x2bc5b5[_0x50fa1c])_0x2bc5b5[_0x50fa1c]=_0x2bc5b5[_0x50fa1c]['concat'](_0x3459f2);}this[_0x42a19f(0x3fe)]=_0x2bc5b5;},Game_Map[_0x5e5f48(0x363)]['isRegionAllowPass']=function(_0x1929f1,_0x5e54df,_0x475f2a,_0x3b533a){const _0x447199=_0x5e5f48,_0x452456=this[_0x447199(0x41b)](_0x1929f1,_0x475f2a),_0x4f538b=this[_0x447199(0x49c)](_0x5e54df,_0x475f2a),_0x352967=this[_0x447199(0x2ae)](_0x452456,_0x4f538b),_0x2ca0db=this['_regionRules'];if(_0x2ca0db[_0x447199(0x214)][_0x447199(0x2d5)](_0x352967))return!![];else{if(_0x3b533a===_0x447199(0x433))return _0x2ca0db[_0x447199(0x486)][_0x447199(0x2d5)](_0x352967)||_0x2ca0db['WalkAllow'][_0x447199(0x2d5)](_0x352967);else{if(_0x3b533a===_0x447199(0x192))return _0x2ca0db[_0x447199(0x330)][_0x447199(0x2d5)](_0x352967)||_0x2ca0db['WalkAllow']['includes'](_0x352967);else{if(_0x2ca0db[_0x447199(0x20d)]['includes'](_0x352967))return!![];else{const _0x133488=_0x447199(0x1fc)[_0x447199(0x2ac)](_0x3b533a[_0x447199(0x18f)](0x0)[_0x447199(0x3ff)]()+_0x3b533a[_0x447199(0x245)](0x1));if(_0x2ca0db[_0x133488])return _0x2ca0db[_0x133488][_0x447199(0x2d5)](_0x352967);}}}}return![];},Game_Map['prototype']['isRegionForbidPass']=function(_0x382ec6,_0x5465c6,_0x25314f,_0x13c681){const _0x31b42c=_0x5e5f48,_0xbfa19d=this[_0x31b42c(0x41b)](_0x382ec6,_0x25314f),_0x2f5e7c=this[_0x31b42c(0x49c)](_0x5465c6,_0x25314f),_0x3c6bce=this[_0x31b42c(0x2ae)](_0xbfa19d,_0x2f5e7c),_0x1e1535=this[_0x31b42c(0x3fe)];if(_0x1e1535['AllForbid'][_0x31b42c(0x2d5)](_0x3c6bce))return!![];else{if(_0x13c681===_0x31b42c(0x433))return _0x1e1535[_0x31b42c(0x4e1)]['includes'](_0x3c6bce)||_0x1e1535['WalkForbid'][_0x31b42c(0x2d5)](_0x3c6bce);else{if(_0x13c681==='event')return _0x1e1535[_0x31b42c(0x158)][_0x31b42c(0x2d5)](_0x3c6bce)||_0x1e1535['WalkForbid'][_0x31b42c(0x2d5)](_0x3c6bce);else{if(_0x1e1535[_0x31b42c(0x244)][_0x31b42c(0x2d5)](_0x3c6bce))return!![];else{const _0x17df89='%1Forbid'[_0x31b42c(0x2ac)](_0x13c681['charAt'](0x0)['toUpperCase']()+_0x13c681['slice'](0x1));if(_0x1e1535[_0x17df89])return _0x1e1535[_0x17df89][_0x31b42c(0x2d5)](_0x3c6bce);}}}}return![];},Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x1b4)]=function(_0x2b6122,_0x1ce55c,_0x4788a9,_0x50fbe2){const _0x1ff7fe=_0x5e5f48;_0x4788a9=_0x50fbe2===_0x1ff7fe(0x2dc)?0x5:_0x4788a9;const _0x24b9da=this[_0x1ff7fe(0x41b)](_0x2b6122,_0x4788a9),_0x496f84=this[_0x1ff7fe(0x49c)](_0x1ce55c,_0x4788a9),_0x22f106=this[_0x1ff7fe(0x2ae)](_0x24b9da,_0x496f84),_0x5c880f=this[_0x1ff7fe(0x3fe)];if(_0x5c880f['VehicleDock'][_0x1ff7fe(0x2d5)](_0x22f106))return!![];else{const _0x12908b=_0x1ff7fe(0x44b)['format'](_0x50fbe2[_0x1ff7fe(0x18f)](0x0)[_0x1ff7fe(0x3ff)]()+_0x50fbe2[_0x1ff7fe(0x245)](0x1));if(_0x5c880f[_0x12908b])return _0x5c880f[_0x12908b][_0x1ff7fe(0x2d5)](_0x22f106);}return![];},VisuMZ['EventsMoveCore']['Game_Map_refresh']=Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x3c6)],Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x3c6)]=function(){const _0x3e7fb0=_0x5e5f48;VisuMZ[_0x3e7fb0(0x248)][_0x3e7fb0(0x3f2)]['call'](this),this['checkNeedForPeriodicRefresh']();},Game_Map[_0x5e5f48(0x363)]['checkNeedForPeriodicRefresh']=function(){const _0x40453f=_0x5e5f48;this[_0x40453f(0x26c)]=![];if(this[_0x40453f(0x3f5)]()['some'](_0x257a93=>_0x257a93['hasAdvancedSwitchVariable']())){this[_0x40453f(0x26c)]=!![];return;}if(this[_0x40453f(0x3f5)]()[_0x40453f(0x25f)](_0x17f6d1=>_0x17f6d1[_0x40453f(0x379)]())){this[_0x40453f(0x26c)]=!![];return;}if(this[_0x40453f(0x36b)][_0x40453f(0x25f)](_0x3e6113=>_0x3e6113[_0x40453f(0x351)]())){this[_0x40453f(0x26c)]=!![];return;}if(this[_0x40453f(0x36b)][_0x40453f(0x25f)](_0x3be2da=>_0x3be2da[_0x40453f(0x379)]())){this['_needsPeriodicRefresh']=!![];return;}},VisuMZ[_0x5e5f48(0x248)]['Game_Map_update']=Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x169)],Game_Map['prototype'][_0x5e5f48(0x169)]=function(_0x349dc3){const _0x45b7df=_0x5e5f48;this[_0x45b7df(0x146)](),VisuMZ[_0x45b7df(0x248)][_0x45b7df(0x3fa)][_0x45b7df(0x2a6)](this,_0x349dc3);},Game_Map[_0x5e5f48(0x363)]['updatePeriodicRefresh']=function(){const _0x38e50f=_0x5e5f48;if(!this[_0x38e50f(0x26c)])return;this[_0x38e50f(0x304)]=this[_0x38e50f(0x304)]||0x3c,this['_periodicRefreshTimer']--,this[_0x38e50f(0x304)]<=0x0&&(this[_0x38e50f(0x223)](),this['_periodicRefreshTimer']=0x3c);},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x1f4)]=Game_Map['prototype'][_0x5e5f48(0x4d5)],Game_Map['prototype']['isDashDisabled']=function(){const _0x4a0d3e=_0x5e5f48;if(!$gameSystem[_0x4a0d3e(0x1d8)]())return!![];return VisuMZ[_0x4a0d3e(0x248)]['Game_Map_isDashDisabled'][_0x4a0d3e(0x2a6)](this);},Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x189)]=function(){const _0x3b6d71=_0x5e5f48;this[_0x3b6d71(0x3ec)]=![];const _0x450b43=$dataMap[_0x3b6d71(0x270)]||'';_0x450b43['match'](/<SAVE EVENT (?:LOCATION|LOCATIONS)>/i)&&(this['_saveEventLocations']=!![]);},Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x390)]=function(){const _0x5f5302=_0x5e5f48;if(this[_0x5f5302(0x3ec)]===undefined)this[_0x5f5302(0x189)]();return this[_0x5f5302(0x3ec)];},Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x271)]=function(_0x328e1c){const _0x4f5d6c=_0x5e5f48;_0x328e1c!==this['mapId']()&&$gamePlayer&&$gameSystem['removeTemporaryMapSpawnedEvents'](this[_0x4f5d6c(0x21e)]());},Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x20a)]=function(){const _0x58ce8f=_0x5e5f48;this[_0x58ce8f(0x2a3)]=$gameSystem['getMapSpawnedEventData'](this['mapId']()),this[_0x58ce8f(0x18b)]=!![];},VisuMZ[_0x5e5f48(0x248)]['Game_Map_events']=Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x3f5)],Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x3f5)]=function(){const _0x38b5a9=_0x5e5f48;if(this[_0x38b5a9(0x1e7)])return this[_0x38b5a9(0x1e7)];const _0x25705a=VisuMZ[_0x38b5a9(0x248)][_0x38b5a9(0x437)]['call'](this),_0x54a32f=_0x25705a[_0x38b5a9(0x18e)](this[_0x38b5a9(0x2a3)]||[]);return this[_0x38b5a9(0x1e7)]=_0x54a32f[_0x38b5a9(0x420)](_0xc5deab=>!!_0xc5deab),this[_0x38b5a9(0x1e7)];},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x16e)]=Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x192)],Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x192)]=function(_0x5c5fb9){const _0x10f798=_0x5e5f48;return _0x5c5fb9>=0x3e8?(_0x5c5fb9-=0x3e8,this[_0x10f798(0x2a3)][_0x5c5fb9]):VisuMZ[_0x10f798(0x248)][_0x10f798(0x16e)]['call'](this,_0x5c5fb9);},Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x3d2)]=function(_0x3c646f){const _0x2353e1=_0x5e5f48,_0x10d976=this['event'](_0x3c646f);if(_0x10d976)_0x10d976[_0x2353e1(0x22d)]();},Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x39f)]=function(){const _0x1cb126=_0x5e5f48,_0x2f4432={'template':_0x1cb126(0x42b),'mapId':0x1,'eventId':0xc,'x':$gamePlayer['x']+0x1,'y':$gamePlayer['y']+0x1,'spawnPreserved':!![],'spawnEventId':this[_0x1cb126(0x2a3)]['length']+0x3e8};this[_0x1cb126(0x442)](_0x2f4432);},Game_Map['prototype']['checkExistingEntitiesAt']=function(_0x286813,_0x208957){const _0x121ea7=_0x5e5f48;if(this[_0x121ea7(0x325)](_0x286813,_0x208957)[_0x121ea7(0x2ef)]>0x0)return!![];if($gamePlayer['x']===_0x286813&&$gamePlayer['y']===_0x208957)return!![];if(this[_0x121ea7(0x2c3)]()[_0x121ea7(0x246)](_0x286813,_0x208957))return!![];if(this['ship']()[_0x121ea7(0x246)](_0x286813,_0x208957))return!![];return![];},Game_Map['prototype'][_0x5e5f48(0x346)]=function(_0x106f80,_0x492eb3,_0x2d3256){const _0x269f37=_0x5e5f48;$gameTemp[_0x269f37(0x457)]=_0x106f80;const _0x15badb=new Game_Event(_0x106f80[_0x269f37(0x21e)],_0x106f80[_0x269f37(0x1f7)]);$gameTemp[_0x269f37(0x457)]=undefined,_0x15badb['refresh']();let _0x2e47bc=_0x492eb3-_0x15badb[_0x269f37(0x2cb)][_0x269f37(0x3ba)],_0x2b087c=_0x492eb3+_0x15badb['_addedHitbox'][_0x269f37(0x3ba)],_0x499f5c=_0x2d3256-_0x15badb[_0x269f37(0x2cb)]['up'],_0x5bbec5=_0x2d3256+_0x15badb[_0x269f37(0x2cb)][_0x269f37(0x1bb)];for(let _0x2da2f8=_0x2e47bc;_0x2da2f8<=_0x2b087c;_0x2da2f8++){for(let _0x3c1c76=_0x499f5c;_0x3c1c76<=_0x5bbec5;_0x3c1c76++){if(this[_0x269f37(0x2c8)](_0x2da2f8,_0x3c1c76))return![];}}return!![];},Game_Map['prototype']['createSpawnedEventWithData']=function(_0x37b930){const _0x5ac16a=_0x5e5f48;$gameTemp[_0x5ac16a(0x457)]=_0x37b930;const _0x216472=new Game_Event(_0x37b930[_0x5ac16a(0x21e)],_0x37b930[_0x5ac16a(0x1f7)]);$gameTemp[_0x5ac16a(0x457)]=undefined,this[_0x5ac16a(0x2a3)][_0x5ac16a(0x4bf)](_0x216472),_0x216472[_0x5ac16a(0x29d)](_0x37b930),this[_0x5ac16a(0x265)]();},Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x309)]=function(_0x2dafc0,_0x2cb7d6,_0x53062a){const _0x103de6=_0x5e5f48,_0x56199d=_0x2dafc0['x'],_0x2dc9b4=_0x2dafc0['y'];if(!this[_0x103de6(0x211)](_0x56199d,_0x2dc9b4))return![];if(_0x2cb7d6){if(this[_0x103de6(0x2c8)](_0x56199d,_0x2dc9b4))return![];if(!this[_0x103de6(0x346)](_0x2dafc0,_0x56199d,_0x2dc9b4))return![];}if(_0x53062a){if(!this['isPassableByAnyDirection'](_0x56199d,_0x2dc9b4))return![];}return this[_0x103de6(0x442)](_0x2dafc0),!![];},Game_Map['prototype']['prepareSpawnedEventAtRegion']=function(_0x244c58,_0x5d8cdb,_0x5636ca,_0x3db945){const _0x3a7ba5=_0x5e5f48,_0x23314d=[],_0x5520b1=this['width'](),_0x5c4297=this[_0x3a7ba5(0x356)]();for(let _0x4e1824=0x0;_0x4e1824<_0x5520b1;_0x4e1824++){for(let _0x2b2d0d=0x0;_0x2b2d0d<_0x5c4297;_0x2b2d0d++){if(!_0x5d8cdb[_0x3a7ba5(0x2d5)](this[_0x3a7ba5(0x2ae)](_0x4e1824,_0x2b2d0d)))continue;if(!this[_0x3a7ba5(0x211)](_0x4e1824,_0x2b2d0d))continue;if(_0x5636ca){if(this[_0x3a7ba5(0x2c8)](_0x4e1824,_0x2b2d0d))continue;if(!this[_0x3a7ba5(0x346)](_0x244c58,_0x4e1824,_0x2b2d0d))continue;}if(_0x3db945){if(!this['isPassableByAnyDirection'](_0x4e1824,_0x2b2d0d))continue;}_0x23314d['push']([_0x4e1824,_0x2b2d0d]);}}if(_0x23314d[_0x3a7ba5(0x2ef)]>0x0){const _0x59e35a=_0x23314d[Math[_0x3a7ba5(0x33a)](_0x23314d[_0x3a7ba5(0x2ef)])];return _0x244c58['x']=_0x59e35a[0x0],_0x244c58['y']=_0x59e35a[0x1],this[_0x3a7ba5(0x442)](_0x244c58),!![];}return![];},Game_Map[_0x5e5f48(0x363)]['prepareSpawnedEventAtTerrainTag']=function(_0x1c7cfd,_0x4bb352,_0x4c064e,_0x53c9bd){const _0x5124c0=_0x5e5f48,_0x36b3df=[],_0xa8fce=this['width'](),_0x2b370c=this[_0x5124c0(0x356)]();for(let _0x25fc20=0x0;_0x25fc20<_0xa8fce;_0x25fc20++){for(let _0x9595c8=0x0;_0x9595c8<_0x2b370c;_0x9595c8++){if(!_0x4bb352[_0x5124c0(0x2d5)](this[_0x5124c0(0x3a7)](_0x25fc20,_0x9595c8)))continue;if(!this[_0x5124c0(0x211)](_0x25fc20,_0x9595c8))continue;if(_0x4c064e){if(this['checkExistingEntitiesAt'](_0x25fc20,_0x9595c8))continue;if(!this[_0x5124c0(0x346)](_0x1c7cfd,_0x25fc20,_0x9595c8))continue;}if(_0x53c9bd){if(!this[_0x5124c0(0x438)](_0x25fc20,_0x9595c8))continue;}_0x36b3df[_0x5124c0(0x4bf)]([_0x25fc20,_0x9595c8]);}}if(_0x36b3df[_0x5124c0(0x2ef)]>0x0){const _0x1a0fa9=_0x36b3df[Math[_0x5124c0(0x33a)](_0x36b3df['length'])];return _0x1c7cfd['x']=_0x1a0fa9[0x0],_0x1c7cfd['y']=_0x1a0fa9[0x1],this['createSpawnedEventWithData'](_0x1c7cfd),!![];}return![];},Game_Map[_0x5e5f48(0x363)]['isPassableByAnyDirection']=function(_0x5244ff,_0x51cf48){const _0x1b5a5b=_0x5e5f48;if(this[_0x1b5a5b(0x412)](_0x5244ff,_0x51cf48,0x2))return!![];if(this[_0x1b5a5b(0x412)](_0x5244ff,_0x51cf48,0x4))return!![];if(this[_0x1b5a5b(0x412)](_0x5244ff,_0x51cf48,0x6))return!![];if(this[_0x1b5a5b(0x412)](_0x5244ff,_0x51cf48,0x8))return!![];return![];},Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x121)]=function(_0x24b23e){const _0xf57426=_0x5e5f48;if(_0x24b23e<0x3e8)return;if(!this[_0xf57426(0x2a3)])return;const _0x5d01a8=this[_0xf57426(0x192)](_0x24b23e);_0x5d01a8['locate'](-0x1,-0x1),_0x5d01a8[_0xf57426(0x22d)](),this[_0xf57426(0x2a3)][_0x24b23e-0x3e8]=null,this['clearEventCache']();},Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x3d8)]=function(){const _0x40f9ce=_0x5e5f48;for(const _0x2f6b2a of this[_0x40f9ce(0x2a3)]){if(_0x2f6b2a)return _0x2f6b2a;}return null;},Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x32d)]=function(){const _0x5642ef=_0x5e5f48,_0x3bedf9=this[_0x5642ef(0x3d8)]();return _0x3bedf9?_0x3bedf9['_eventId']:0x0;},Game_Map['prototype'][_0x5e5f48(0x151)]=function(){const _0x3dfd4e=_0x5e5f48,_0x35d469=this[_0x3dfd4e(0x2a3)][_0x3dfd4e(0x245)](0x0)[_0x3dfd4e(0x4c5)]();for(const _0x540da2 of _0x35d469){if(_0x540da2)return _0x540da2;}return null;},Game_Map[_0x5e5f48(0x363)]['lastSpawnedEventID']=function(){const _0x26087b=_0x5e5f48,_0xc9e517=this[_0x26087b(0x151)]();return _0xc9e517?_0xc9e517['_eventId']:0x0;},Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x307)]=function(_0x27bc00,_0x5348eb){const _0x58ed96=_0x5e5f48,_0xf0971c=this[_0x58ed96(0x325)](_0x27bc00,_0x5348eb);for(const _0x2bc661 of _0xf0971c){if(!_0x2bc661)continue;if(_0x2bc661[_0x58ed96(0x21f)]())this['despawnEventId'](_0x2bc661[_0x58ed96(0x21a)]);}},Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x4d8)]=function(_0x41a278){const _0x5a55a3=_0x5e5f48;for(const _0x526a37 of this[_0x5a55a3(0x2a3)]){if(!_0x526a37)continue;_0x41a278[_0x5a55a3(0x2d5)](_0x526a37[_0x5a55a3(0x2ae)]())&&this['despawnEventId'](_0x526a37['_eventId']);}},Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x16a)]=function(_0x87c714){const _0x144fe9=_0x5e5f48;for(const _0x576b53 of this[_0x144fe9(0x2a3)]){if(!_0x576b53)continue;_0x87c714['includes'](_0x576b53[_0x144fe9(0x3a7)]())&&this[_0x144fe9(0x121)](_0x576b53[_0x144fe9(0x21a)]);}},Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x139)]=function(){const _0x55cb63=_0x5e5f48;for(const _0x5a5c06 of this['_spawnedEvents']){if(!_0x5a5c06)continue;this[_0x55cb63(0x121)](_0x5a5c06['_eventId']);}},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x12b)]=Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x25c)],Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x25c)]=function(_0x51f7be){const _0xa69b50=_0x5e5f48;VisuMZ['EventsMoveCore'][_0xa69b50(0x12b)][_0xa69b50(0x2a6)](this,_0x51f7be);if(_0x51f7be>=0x3e8){const _0xe98c9f=this['event'](_0x51f7be);if(_0xe98c9f)_0xe98c9f[_0xa69b50(0x34d)]();}},Game_CommonEvent[_0x5e5f48(0x363)][_0x5e5f48(0x351)]=function(){const _0x326b4e=_0x5e5f48,_0x23bc29=this[_0x326b4e(0x192)]();return this[_0x326b4e(0x343)]()&&_0x23bc29[_0x326b4e(0x22e)]>=0x1&&DataManager['isAdvancedSwitch'](_0x23bc29[_0x326b4e(0x250)]);},Game_CommonEvent['prototype'][_0x5e5f48(0x379)]=function(){const _0x23c937=_0x5e5f48;return VisuMZ[_0x23c937(0x248)][_0x23c937(0x1d0)][_0x23c937(0x36b)]['includes'](this[_0x23c937(0x3cf)]);},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x145)]=Game_CommonEvent[_0x5e5f48(0x363)]['isActive'],Game_CommonEvent[_0x5e5f48(0x363)][_0x5e5f48(0x343)]=function(){const _0x5bc147=_0x5e5f48;return VisuMZ[_0x5bc147(0x248)][_0x5bc147(0x145)][_0x5bc147(0x2a6)](this)?!![]:VisuMZ[_0x5bc147(0x248)][_0x5bc147(0x1d0)][_0x5bc147(0x4e4)](this['event']()['CPC'],this[_0x5bc147(0x3cf)]);},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x39b)]=Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x335)],Game_Map[_0x5e5f48(0x363)][_0x5e5f48(0x335)]=function(){const _0x20ee77=_0x5e5f48,_0x5c93e4=VisuMZ['EventsMoveCore'][_0x20ee77(0x39b)][_0x20ee77(0x2a6)](this),_0x5a6513=VisuMZ[_0x20ee77(0x248)][_0x20ee77(0x1d0)][_0x20ee77(0x36b)][_0x20ee77(0x4d7)](_0x4f4b08=>$dataCommonEvents[_0x4f4b08]);return _0x5c93e4[_0x20ee77(0x18e)](_0x5a6513)[_0x20ee77(0x420)]((_0x5723bb,_0x5d4f76,_0x31a5cd)=>_0x31a5cd[_0x20ee77(0x436)](_0x5723bb)===_0x5d4f76);},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x1a8)]=Game_CharacterBase[_0x5e5f48(0x363)]['initMembers'],Game_CharacterBase['prototype'][_0x5e5f48(0x3d5)]=function(){const _0x3150ad=_0x5e5f48;VisuMZ[_0x3150ad(0x248)]['Game_CharacterBase_initMembers']['call'](this),this[_0x3150ad(0x22f)]();},Game_CharacterBase['prototype'][_0x5e5f48(0x22f)]=function(){const _0x465755=_0x5e5f48;this[_0x465755(0x128)]=![],this[_0x465755(0x4b6)](),this[_0x465755(0x482)](),this['clearSpriteOffsets'](),this[_0x465755(0x294)]();},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x1cd)]=function(){const _0x3c2586=_0x5e5f48;if(this[_0x3c2586(0x205)]===Game_Player&&this[_0x3c2586(0x396)]())return this[_0x3c2586(0x29a)]()[_0x3c2586(0x4df)]()[_0x3c2586(0x1b1)](/\[VS8\]/i);else return Imported[_0x3c2586(0x322)]&&this[_0x3c2586(0x378)]()?!![]:this['characterName']()[_0x3c2586(0x1b1)](/\[VS8\]/i);},VisuMZ[_0x5e5f48(0x248)]['Game_CharacterBase_direction']=Game_CharacterBase[_0x5e5f48(0x363)]['direction'],Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x2dd)]=function(){const _0x5d3223=_0x5e5f48;if(this['isOnLadder']()&&!this[_0x5d3223(0x24a)]()&&this['isSpriteVS8dir']())return this[_0x5d3223(0x19d)]();else{if(this[_0x5d3223(0x247)]()&&!this[_0x5d3223(0x24a)]())return 0x8;else return this[_0x5d3223(0x1b3)]()&&this[_0x5d3223(0x1cd)]()?this[_0x5d3223(0x26a)]():VisuMZ[_0x5d3223(0x248)]['Game_CharacterBase_direction'][_0x5d3223(0x2a6)](this);}},VisuMZ['EventsMoveCore'][_0x5e5f48(0x3c2)]=Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x117)],Game_CharacterBase['prototype']['setDirection']=function(_0x1fbffc){const _0x393730=_0x5e5f48;if(!this[_0x393730(0x1cd)]())_0x1fbffc=this[_0x393730(0x44f)](_0x1fbffc);VisuMZ[_0x393730(0x248)][_0x393730(0x3c2)][_0x393730(0x2a6)](this,_0x1fbffc);},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x44f)]=function(_0x3b853e){const _0x22f347=_0x5e5f48;if(_0x3b853e===0x1)return this[_0x22f347(0x27b)](this['_x'],this['_y'],0x4)?0x4:0x2;if(_0x3b853e===0x3)return this['canPass'](this['_x'],this['_y'],0x6)?0x6:0x2;if(_0x3b853e===0x7)return this[_0x22f347(0x27b)](this['_x'],this['_y'],0x4)?0x4:0x8;if(_0x3b853e===0x9)return this[_0x22f347(0x27b)](this['_x'],this['_y'],0x6)?0x6:0x8;return _0x3b853e;},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x2bb)]=function(_0x47bdaa){const _0x5ed8e6=_0x5e5f48;return[0x1,0x3,0x5,0x7,0x9][_0x5ed8e6(0x2d5)](_0x47bdaa);},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x3e7)]=function(){const _0x49456c=_0x5e5f48;return this[_0x49456c(0x297)]||0x0;},VisuMZ['EventsMoveCore']['Game_CharacterBase_moveStraight']=Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x187)],Game_CharacterBase[_0x5e5f48(0x363)]['moveStraight']=function(_0x427d30){const _0x5618f0=_0x5e5f48;this[_0x5618f0(0x297)]=_0x427d30,VisuMZ[_0x5618f0(0x248)][_0x5618f0(0x14f)]['call'](this,_0x427d30);},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x337)]=function(_0x5a0b8a){const _0xba4ca3=_0x5e5f48;if(!this[_0xba4ca3(0x2bb)](_0x5a0b8a))return this[_0xba4ca3(0x187)](_0x5a0b8a);let _0x2e2bbf=0x0,_0x33ebb4=0x0;switch(_0x5a0b8a){case 0x1:_0x2e2bbf=0x4,_0x33ebb4=0x2;break;case 0x3:_0x2e2bbf=0x6,_0x33ebb4=0x2;break;case 0x7:_0x2e2bbf=0x4,_0x33ebb4=0x8;break;case 0x9:_0x2e2bbf=0x6,_0x33ebb4=0x8;break;}if(VisuMZ[_0xba4ca3(0x248)][_0xba4ca3(0x1e5)][_0xba4ca3(0x1b0)][_0xba4ca3(0x314)]){if(!this[_0xba4ca3(0x27b)](this['_x'],this['_y'],_0x2e2bbf))return this[_0xba4ca3(0x187)](_0x33ebb4);if(!this[_0xba4ca3(0x27b)](this['_x'],this['_y'],_0x33ebb4))return this[_0xba4ca3(0x187)](_0x2e2bbf);if(!this['canPassDiagonally'](this['_x'],this['_y'],_0x2e2bbf,_0x33ebb4)){let _0x1c2e32=VisuMZ['EventsMoveCore'][_0xba4ca3(0x1e5)][_0xba4ca3(0x1b0)][_0xba4ca3(0x499)]?_0x2e2bbf:_0x33ebb4;return this[_0xba4ca3(0x187)](_0x1c2e32);}}this['_lastMovedDirection']=_0x5a0b8a,this[_0xba4ca3(0x46a)](_0x2e2bbf,_0x33ebb4);},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x2f1)]=Game_CharacterBase['prototype'][_0x5e5f48(0x286)],Game_CharacterBase[_0x5e5f48(0x363)]['realMoveSpeed']=function(){const _0x1d42f3=_0x5e5f48;let _0x589210=this[_0x1d42f3(0x31d)];return this[_0x1d42f3(0x2f3)]()&&(_0x589210+=this[_0x1d42f3(0x219)]()),this[_0x1d42f3(0x213)](_0x589210);},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x219)]=function(){const _0x540bef=_0x5e5f48,_0x7386a3=VisuMZ['EventsMoveCore'][_0x540bef(0x1e5)][_0x540bef(0x1b0)];return _0x7386a3[_0x540bef(0x341)]!==undefined?_0x7386a3[_0x540bef(0x341)]:VisuMZ['EventsMoveCore'][_0x540bef(0x2f1)][_0x540bef(0x2a6)](this)-this['_moveSpeed'];},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x213)]=function(_0x4590e9){const _0x2751c2=_0x5e5f48,_0x4ca30a=VisuMZ['EventsMoveCore'][_0x2751c2(0x1e5)][_0x2751c2(0x1b0)];if(!_0x4ca30a['SlowerSpeed'])return _0x4590e9;return[0x1,0x3,0x7,0x9][_0x2751c2(0x2d5)](this[_0x2751c2(0x297)])&&(_0x4590e9*=_0x4ca30a[_0x2751c2(0x404)]||0.01),_0x4590e9;},VisuMZ['EventsMoveCore']['Game_CharacterBase_isDashing']=Game_CharacterBase['prototype'][_0x5e5f48(0x2f3)],Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x2f3)]=function(){const _0x521ae5=_0x5e5f48;if(this[_0x521ae5(0x3d0)])return!![];return VisuMZ[_0x521ae5(0x248)]['Game_CharacterBase_isDashing'][_0x521ae5(0x2a6)](this);},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x344)]=function(){const _0x4ea213=_0x5e5f48;return this[_0x4ea213(0x2f3)]();},VisuMZ[_0x5e5f48(0x248)]['Game_CharacterBase_pattern']=Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x16f)],Game_CharacterBase['prototype']['pattern']=function(){const _0x301915=_0x5e5f48;return this[_0x301915(0x1b3)]()?this[_0x301915(0x2d6)]():VisuMZ['EventsMoveCore'][_0x301915(0x306)][_0x301915(0x2a6)](this);},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x4af)]=Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x359)],Game_CharacterBase[_0x5e5f48(0x363)]['increaseSteps']=function(){const _0x48b959=_0x5e5f48;VisuMZ[_0x48b959(0x248)][_0x48b959(0x4af)]['call'](this),this[_0x48b959(0x4b6)]();},VisuMZ[_0x5e5f48(0x248)]['Game_CharacterBase_characterIndex']=Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x1f2)],Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x1f2)]=function(){const _0x1d9c5c=_0x5e5f48;if(this['isSpriteVS8dir']())return this[_0x1d9c5c(0x1cc)]();return VisuMZ[_0x1d9c5c(0x248)][_0x1d9c5c(0x461)][_0x1d9c5c(0x2a6)](this);},Game_CharacterBase[_0x5e5f48(0x363)]['characterIndexVS8']=function(){const _0x44917a=_0x5e5f48,_0x19ca55=this[_0x44917a(0x2dd)]();if(this[_0x44917a(0x24a)]()){if([0x2,0x4,0x6,0x8][_0x44917a(0x2d5)](_0x19ca55))return 0x4;if([0x1,0x3,0x7,0x9][_0x44917a(0x2d5)](_0x19ca55))return 0x5;}else{if(this[_0x44917a(0x247)]())return 0x6;else{if(this[_0x44917a(0x1b3)]())return this[_0x44917a(0x444)]();else{if(this[_0x44917a(0x198)]){if([0x2,0x4,0x6,0x8][_0x44917a(0x2d5)](_0x19ca55))return 0x4;if([0x1,0x3,0x7,0x9][_0x44917a(0x2d5)](_0x19ca55))return 0x5;}else{if(this[_0x44917a(0x195)]()&&this[_0x44917a(0x287)]()){if([0x2,0x4,0x6,0x8][_0x44917a(0x2d5)](_0x19ca55))return 0x4;if([0x1,0x3,0x7,0x9][_0x44917a(0x2d5)](_0x19ca55))return 0x5;}else{if(this[_0x44917a(0x344)]()){if([0x2,0x4,0x6,0x8][_0x44917a(0x2d5)](_0x19ca55))return 0x2;if([0x1,0x3,0x7,0x9][_0x44917a(0x2d5)](_0x19ca55))return 0x3;}else{if([0x2,0x4,0x6,0x8][_0x44917a(0x2d5)](_0x19ca55))return 0x0;if([0x1,0x3,0x7,0x9][_0x44917a(0x2d5)](_0x19ca55))return 0x1;}}}}}}},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x287)]=function(){const _0x151926=_0x5e5f48;return VisuMZ[_0x151926(0x248)][_0x151926(0x1e5)]['VS8']['CarryPose'];},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x1a9)]=function(){const _0x29982d=_0x5e5f48;return this[_0x29982d(0x247)]()&&this[_0x29982d(0x3a7)]()===VisuMZ[_0x29982d(0x248)][_0x29982d(0x1e5)]['TerrainTag'][_0x29982d(0x301)];},Game_CharacterBase['prototype'][_0x5e5f48(0x19d)]=function(){return this['isOnRope']()?0x4:0x2;},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x19a)]=Game_CharacterBase['prototype'][_0x5e5f48(0x169)],Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x169)]=function(){const _0x2cf412=_0x5e5f48;VisuMZ[_0x2cf412(0x248)][_0x2cf412(0x19a)]['call'](this),this[_0x2cf412(0x299)]();},Game_CharacterBase['prototype'][_0x5e5f48(0x299)]=function(){const _0x4b5fa1=_0x5e5f48;this[_0x4b5fa1(0x424)]=this[_0x4b5fa1(0x424)]||0x0;if(this[_0x4b5fa1(0x424)]>0x0){this[_0x4b5fa1(0x424)]--;if(this[_0x4b5fa1(0x424)]<=0x0&&this[_0x4b5fa1(0x28e)]!=='ZZZ')this[_0x4b5fa1(0x4b6)]();}},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x1d4)]=Game_CharacterBase[_0x5e5f48(0x363)]['moveDiagonally'],Game_CharacterBase[_0x5e5f48(0x363)]['moveDiagonally']=function(_0x41adc5,_0xb476b6){const _0x1d9364=_0x5e5f48;VisuMZ[_0x1d9364(0x248)][_0x1d9364(0x1d4)]['call'](this,_0x41adc5,_0xb476b6);if(this[_0x1d9364(0x1cd)]())this[_0x1d9364(0x354)](_0x41adc5,_0xb476b6);},Game_CharacterBase['prototype'][_0x5e5f48(0x354)]=function(_0x11b994,_0xc9c761){const _0x147961=_0x5e5f48;if(_0x11b994===0x4&&_0xc9c761===0x2)this[_0x147961(0x117)](0x1);if(_0x11b994===0x6&&_0xc9c761===0x2)this['setDirection'](0x3);if(_0x11b994===0x4&&_0xc9c761===0x8)this[_0x147961(0x117)](0x7);if(_0x11b994===0x6&&_0xc9c761===0x8)this[_0x147961(0x117)](0x9);},VisuMZ[_0x5e5f48(0x248)]['Game_CharacterBase_hasStepAnime']=Game_CharacterBase['prototype'][_0x5e5f48(0x3f0)],Game_CharacterBase['prototype'][_0x5e5f48(0x3f0)]=function(){const _0x2c5bf2=_0x5e5f48;if(this[_0x2c5bf2(0x1b3)]()&&this[_0x2c5bf2(0x19b)]()==='ZZZ')return!![];return VisuMZ[_0x2c5bf2(0x248)]['Game_CharacterBase_hasStepAnime'][_0x2c5bf2(0x2a6)](this);},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x441)]=function(_0x46fcfe,_0x15a786){const _0x2da874=_0x5e5f48;if(_0x46fcfe[_0x2da874(0x1b1)](/Z/i))_0x46fcfe=_0x2da874(0x229);if(_0x46fcfe[_0x2da874(0x1b1)](/SLEEP/i))_0x46fcfe=_0x2da874(0x229);this['isSpriteVS8dir']()&&(this[_0x2da874(0x28e)]=_0x46fcfe['toUpperCase']()[_0x2da874(0x372)](),this[_0x2da874(0x424)]=_0x15a786||Infinity);},Game_CharacterBase['prototype'][_0x5e5f48(0x19b)]=function(){const _0x59b60c=_0x5e5f48;return this[_0x59b60c(0x1cd)]()?(this['_pose']||'')[_0x59b60c(0x3ff)]()[_0x59b60c(0x372)]():''[_0x59b60c(0x3ff)]()[_0x59b60c(0x372)]();},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x464)]=function(_0x262bbf,_0x56765e){const _0x2bdf8f=_0x5e5f48;if(this[_0x2bdf8f(0x1cd)]()){const _0x5ea9be=['','EXCLAMATION',_0x2bdf8f(0x19c),_0x2bdf8f(0x35d),_0x2bdf8f(0x450),'ANGER',_0x2bdf8f(0x2b8),'COBWEB',_0x2bdf8f(0x439),_0x2bdf8f(0x4d1),_0x2bdf8f(0x229),'','','','',''][_0x262bbf];this[_0x2bdf8f(0x441)](_0x5ea9be,_0x56765e);}},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x4b6)]=function(){const _0x5027d0=_0x5e5f48;this[_0x5027d0(0x28e)]='',this[_0x5027d0(0x424)]=0x0;},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x1b3)]=function(){const _0x5f54b5=_0x5e5f48;return this[_0x5f54b5(0x1cd)]()&&!!this[_0x5f54b5(0x28e)];},Game_CharacterBase['prototype'][_0x5e5f48(0x444)]=function(){const _0x2c710a=_0x5e5f48,_0x3820a9=this[_0x2c710a(0x28e)][_0x2c710a(0x3ff)]();switch(this[_0x2c710a(0x28e)]['toUpperCase']()[_0x2c710a(0x372)]()){case _0x2c710a(0x302):case _0x2c710a(0x350):case _0x2c710a(0x4b3):case'HURT':case _0x2c710a(0x19e):case'COLLAPSE':return 0x6;break;default:return 0x7;break;}},Game_CharacterBase[_0x5e5f48(0x363)]['getPosingCharacterDirection']=function(){const _0x4f25c3=_0x5e5f48;switch(this[_0x4f25c3(0x28e)][_0x4f25c3(0x3ff)]()){case'EXCLAMATION':case'QUESTION':case _0x4f25c3(0x35d):case'!':case'?':return 0x2;break;case _0x4f25c3(0x450):case'ANGER':case _0x4f25c3(0x2b8):return 0x4;break;case _0x4f25c3(0x302):case'HMPH':case _0x4f25c3(0x4b3):case _0x4f25c3(0x455):case _0x4f25c3(0x439):case'LIGHT\x20BULB':return 0x6;break;case _0x4f25c3(0x493):case _0x4f25c3(0x19e):case'COLLAPSE':case _0x4f25c3(0x229):case'SLEEP':return 0x8;break;default:return VisuMZ[_0x4f25c3(0x248)][_0x4f25c3(0x3c2)]['call'](this);break;}},Game_CharacterBase[_0x5e5f48(0x363)]['getPosingCharacterPattern']=function(){const _0x3a1c01=_0x5e5f48;switch(this[_0x3a1c01(0x28e)][_0x3a1c01(0x3ff)]()){case'ITEM':case _0x3a1c01(0x493):case _0x3a1c01(0x4cf):case'!':case _0x3a1c01(0x450):case _0x3a1c01(0x455):return 0x0;break;case _0x3a1c01(0x350):case _0x3a1c01(0x19e):case _0x3a1c01(0x19c):case'?':case _0x3a1c01(0x3fc):case _0x3a1c01(0x439):return 0x1;break;case'VICTORY':case _0x3a1c01(0x168):case'MUSIC\x20NOTE':case _0x3a1c01(0x2b8):case _0x3a1c01(0x4d1):return 0x2;break;default:return VisuMZ['EventsMoveCore'][_0x3a1c01(0x306)][_0x3a1c01(0x2a6)](this);break;}},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x1f6)]=function(){this['_forceCarrying']=!![];},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x40b)]=function(){const _0x2b421e=_0x5e5f48;this[_0x2b421e(0x198)]=![];},Game_CharacterBase['prototype'][_0x5e5f48(0x3bb)]=function(){const _0x843a59=_0x5e5f48;this[_0x843a59(0x3d0)]=!![];},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x482)]=function(){this['_forceDashing']=![];},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x292)]=function(){const _0xf038c5=_0x5e5f48;if(this[_0xf038c5(0x4ed)]())return![];if(this[_0xf038c5(0x267)])return![];if(this[_0xf038c5(0x2ce)])return![];if(this[_0xf038c5(0x13d)]==='')return![];if(this['constructor']===Game_Vehicle)return![];return!![];},Game_CharacterBase['prototype']['isShadowShrink']=function(){const _0x424c55=_0x5e5f48;if(this['isOnLadder']())return!![];if(this[_0x424c55(0x205)]===Game_Player&&this['isInVehicle']())return!![];return![];},Game_CharacterBase['prototype'][_0x5e5f48(0x199)]=function(){const _0x5c6566=_0x5e5f48;return VisuMZ[_0x5c6566(0x248)]['Settings']['Movement'][_0x5c6566(0x410)];},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x2d8)]=function(){const _0x4ab15f=_0x5e5f48;return this[_0x4ab15f(0x34a)]();},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x46f)]=function(){const _0x2f1545=_0x5e5f48;return this[_0x2f1545(0x416)]()+this[_0x2f1545(0x2d2)]()+this[_0x2f1545(0x1f1)]();},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x3da)]=function(_0x515449,_0x138575){const _0x5728d8=_0x5e5f48,_0x7b35bc=this[_0x5728d8(0x4d6)](),_0x64e6cc=$gameMap['width'](),_0x2f5668=[],_0x34776a=[],_0x30d7cc=[],_0x13b086={};let _0x32dd11=_0x13b086;if(this['x']===_0x515449&&this['y']===_0x138575)return 0x0;_0x13b086[_0x5728d8(0x2ab)]=null,_0x13b086['x']=this['x'],_0x13b086['y']=this['y'],_0x13b086['g']=0x0,_0x13b086['f']=$gameMap[_0x5728d8(0x35f)](_0x13b086['x'],_0x13b086['y'],_0x515449,_0x138575),_0x2f5668[_0x5728d8(0x4bf)](_0x13b086),_0x34776a['push'](_0x13b086['y']*_0x64e6cc+_0x13b086['x']);while(_0x2f5668[_0x5728d8(0x2ef)]>0x0){let _0x3e6ffa=0x0;for(let _0x2169e1=0x0;_0x2169e1<_0x2f5668[_0x5728d8(0x2ef)];_0x2169e1++){_0x2f5668[_0x2169e1]['f']<_0x2f5668[_0x3e6ffa]['f']&&(_0x3e6ffa=_0x2169e1);}const _0x1c416a=_0x2f5668[_0x3e6ffa],_0x2f93fb=_0x1c416a['x'],_0x265fb0=_0x1c416a['y'],_0x3a2dfd=_0x265fb0*_0x64e6cc+_0x2f93fb,_0x1d2be8=_0x1c416a['g'];_0x2f5668[_0x5728d8(0x120)](_0x3e6ffa,0x1),_0x34776a[_0x5728d8(0x120)](_0x34776a['indexOf'](_0x3a2dfd),0x1),_0x30d7cc['push'](_0x3a2dfd);if(_0x1c416a['x']===_0x515449&&_0x1c416a['y']===_0x138575){_0x32dd11=_0x1c416a;break;}if(_0x1d2be8>=_0x7b35bc)continue;const _0x55e3d2=[0x0,0x4,0x0,0x6,0x4,0x0,0x6,0x4,0x0,0x6],_0x38a499=[0x0,0x2,0x2,0x2,0x0,0x0,0x0,0x8,0x8,0x8];for(let _0xf31d83=0x1;_0xf31d83<0xa;_0xf31d83++){if(_0xf31d83===0x5)continue;const _0x1377f3=_0xf31d83,_0x4ed46d=_0x55e3d2[_0xf31d83],_0xc05c39=_0x38a499[_0xf31d83],_0x5565de=$gameMap[_0x5728d8(0x41b)](_0x2f93fb,_0x1377f3),_0x1e8589=$gameMap[_0x5728d8(0x49c)](_0x265fb0,_0x1377f3),_0x1b68e6=_0x1e8589*_0x64e6cc+_0x5565de;if(_0x30d7cc['includes'](_0x1b68e6))continue;if(this[_0x5728d8(0x205)]===Game_Player&&VisuMZ[_0x5728d8(0x248)][_0x5728d8(0x1e5)][_0x5728d8(0x1b0)]['StrictCollision']){if(!this[_0x5728d8(0x27b)](_0x2f93fb,_0x265fb0,_0x4ed46d))continue;if(!this[_0x5728d8(0x27b)](_0x2f93fb,_0x265fb0,_0xc05c39))continue;}if(!this[_0x5728d8(0x2e8)](_0x2f93fb,_0x265fb0,_0x4ed46d,_0xc05c39))continue;const _0x204866=_0x1d2be8+0x1,_0x35820b=_0x34776a[_0x5728d8(0x436)](_0x1b68e6);if(_0x35820b<0x0||_0x204866<_0x2f5668[_0x35820b]['g']){let _0x21e97f={};_0x35820b>=0x0?_0x21e97f=_0x2f5668[_0x35820b]:(_0x2f5668['push'](_0x21e97f),_0x34776a[_0x5728d8(0x4bf)](_0x1b68e6)),_0x21e97f[_0x5728d8(0x2ab)]=_0x1c416a,_0x21e97f['x']=_0x5565de,_0x21e97f['y']=_0x1e8589,_0x21e97f['g']=_0x204866,_0x21e97f['f']=_0x204866+$gameMap[_0x5728d8(0x35f)](_0x5565de,_0x1e8589,_0x515449,_0x138575),(!_0x32dd11||_0x21e97f['f']-_0x21e97f['g']<_0x32dd11['f']-_0x32dd11['g'])&&(_0x32dd11=_0x21e97f);}}}let _0x5b7e91=_0x32dd11;while(_0x5b7e91[_0x5728d8(0x2ab)]&&_0x5b7e91[_0x5728d8(0x2ab)]!==_0x13b086){_0x5b7e91=_0x5b7e91[_0x5728d8(0x2ab)];}const _0x53513c=$gameMap[_0x5728d8(0x327)](_0x5b7e91['x'],_0x13b086['x']),_0x298054=$gameMap[_0x5728d8(0x3e3)](_0x5b7e91['y'],_0x13b086['y']);if(_0x53513c<0x0&&_0x298054>0x0)return 0x1;if(_0x53513c>0x0&&_0x298054>0x0)return 0x3;if(_0x53513c<0x0&&_0x298054<0x0)return 0x7;if(_0x53513c>0x0&&_0x298054<0x0)return 0x9;if(_0x298054>0x0)return 0x2;if(_0x53513c<0x0)return 0x4;if(_0x53513c>0x0)return 0x6;if(_0x298054<0x0)return 0x8;const _0x5af0ba=this[_0x5728d8(0x272)](_0x515449),_0x3dd8ba=this[_0x5728d8(0x3ef)](_0x138575);if(Math[_0x5728d8(0x392)](_0x5af0ba)>Math[_0x5728d8(0x392)](_0x3dd8ba))return _0x5af0ba>0x0?0x4:0x6;else{if(_0x3dd8ba!==0x0)return _0x3dd8ba>0x0?0x8:0x2;}return 0x0;},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x489)]=Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x27b)],Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x27b)]=function(_0x28476b,_0x21fb98,_0x3669e5){const _0x1d0b16=_0x5e5f48;return this[_0x1d0b16(0x4b4)]===_0x1d0b16(0x2dc)?this[_0x1d0b16(0x29a)]()['isAirshipPassable'](_0x28476b,_0x21fb98,_0x3669e5):VisuMZ[_0x1d0b16(0x248)][_0x1d0b16(0x489)][_0x1d0b16(0x2a6)](this,_0x28476b,_0x21fb98,_0x3669e5);},Game_CharacterBase[_0x5e5f48(0x363)]['clearSpriteOffsets']=function(){const _0x555e88=_0x5e5f48;this[_0x555e88(0x1dc)]=0x0,this[_0x555e88(0x3a5)]=0x0;},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x34e)]=Game_CharacterBase['prototype']['screenX'],Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x34a)]=function(){const _0x52622b=_0x5e5f48;return VisuMZ[_0x52622b(0x248)]['Game_CharacterBase_screenX']['call'](this)+(this['_spriteOffsetX']||0x0);},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x282)]=Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x416)],Game_CharacterBase[_0x5e5f48(0x363)]['screenY']=function(){const _0x5ebd93=_0x5e5f48;return VisuMZ['EventsMoveCore'][_0x5ebd93(0x282)]['call'](this)+(this[_0x5ebd93(0x3a5)]||0x0);},Game_CharacterBase[_0x5e5f48(0x363)]['clearStepPattern']=function(){const _0x1c886c=_0x5e5f48;this[_0x1c886c(0x26d)]='';},VisuMZ[_0x5e5f48(0x248)]['Game_CharacterBase_updatePattern']=Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x2f2)],Game_CharacterBase[_0x5e5f48(0x363)]['updatePattern']=function(){const _0x2195de=_0x5e5f48;if(this['_patternLocked'])return;if(this[_0x2195de(0x4ea)]())return;VisuMZ[_0x2195de(0x248)][_0x2195de(0x20b)][_0x2195de(0x2a6)](this);},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x4ea)]=function(){const _0x1bee23=_0x5e5f48;if(!this['hasStepAnime']()&&this[_0x1bee23(0x308)]>0x0)return![];switch(String(this['_stepPattern'])[_0x1bee23(0x3ff)]()[_0x1bee23(0x372)]()){case _0x1bee23(0x4d9):this[_0x1bee23(0x11f)]+=0x1;if(this[_0x1bee23(0x11f)]>0x2)this[_0x1bee23(0x440)](0x0);break;case'RIGHT\x20TO\x20LEFT':this['_pattern']-=0x1;if(this[_0x1bee23(0x11f)]<0x0)this[_0x1bee23(0x440)](0x2);break;case _0x1bee23(0x208):case _0x1bee23(0x123):this[_0x1bee23(0x1d7)]();break;case'SPIN\x20COUNTERCLOCKWISE':case _0x1bee23(0x43e):case _0x1bee23(0x226):case'SPIN\x20ACW':this[_0x1bee23(0x321)]();break;default:return![];}return!![];},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x23f)]=function(){const _0x31691b=_0x5e5f48;return $gameSystem[_0x31691b(0x23f)](this);},Game_CharacterBase['prototype'][_0x5e5f48(0x195)]=function(){const _0x1d4004=_0x5e5f48,_0x278681=this['getEventIconData']();if(!_0x278681)return![];return _0x278681[_0x1d4004(0x1e4)]>0x0;},Game_CharacterBase['prototype'][_0x5e5f48(0x177)]=function(){const _0x15e78a=_0x5e5f48,_0x2a637d=this[_0x15e78a(0x2dd)]();return $gameMap[_0x15e78a(0x41b)](this['x'],_0x2a637d);},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x24e)]=function(){const _0x35745a=_0x5e5f48,_0x4f2eca=this['direction']();return $gameMap[_0x35745a(0x49c)](this['y'],_0x4f2eca);},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x1c6)]=function(){const _0x3105d7=_0x5e5f48,_0x7c9994=this[_0x3105d7(0x447)](this['direction']());return $gameMap[_0x3105d7(0x41b)](this['x'],_0x7c9994);},Game_CharacterBase[_0x5e5f48(0x363)][_0x5e5f48(0x1f5)]=function(){const _0x78ab8d=_0x5e5f48,_0x47a305=this[_0x78ab8d(0x447)](this[_0x78ab8d(0x2dd)]());return $gameMap[_0x78ab8d(0x49c)](this['y'],_0x47a305);},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x1c9)]=Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x37a)],Game_Character[_0x5e5f48(0x363)]['setMoveRoute']=function(_0x36efc6){const _0x1a8632=_0x5e5f48;route=JsonEx[_0x1a8632(0x4b1)](_0x36efc6),VisuMZ['EventsMoveCore'][_0x1a8632(0x1c9)]['call'](this,route);},VisuMZ[_0x5e5f48(0x248)]['Game_Character_forceMoveRoute']=Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x3b1)],Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x3b1)]=function(_0x3a0bfb){const _0x17b3f1=_0x5e5f48;route=JsonEx[_0x17b3f1(0x4b1)](_0x3a0bfb),VisuMZ[_0x17b3f1(0x248)][_0x17b3f1(0x263)][_0x17b3f1(0x2a6)](this,route);},VisuMZ[_0x5e5f48(0x248)]['Game_Character_processMoveCommand']=Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x256)],Game_Character['prototype']['processMoveCommand']=function(_0x137d5e){const _0x53f1b0=_0x5e5f48,_0x212a7e=Game_Character,_0x528392=_0x137d5e['parameters'];if(_0x137d5e[_0x53f1b0(0x212)]===_0x212a7e[_0x53f1b0(0x2a2)]){let _0x4f0f77=_0x137d5e[_0x53f1b0(0x273)][0x0];_0x4f0f77=this[_0x53f1b0(0x49a)](_0x4f0f77),_0x4f0f77=this[_0x53f1b0(0x376)](_0x4f0f77),this[_0x53f1b0(0x140)](_0x137d5e,_0x4f0f77);}else VisuMZ[_0x53f1b0(0x248)][_0x53f1b0(0x472)][_0x53f1b0(0x2a6)](this,_0x137d5e);},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x49a)]=function(_0xd894f9){const _0x3ee893=_0x5e5f48,_0x51c205=/\$gameVariables\.value\((\d+)\)/gi,_0x579c60=/\\V\[(\d+)\]/gi;while(_0xd894f9['match'](_0x51c205)){_0xd894f9=_0xd894f9[_0x3ee893(0x281)](_0x51c205,(_0x48f088,_0x95f87b)=>$gameVariables[_0x3ee893(0x4c4)](parseInt(_0x95f87b)));}while(_0xd894f9[_0x3ee893(0x1b1)](_0x579c60)){_0xd894f9=_0xd894f9['replace'](_0x579c60,(_0x276833,_0x5aae76)=>$gameVariables['value'](parseInt(_0x5aae76)));}return _0xd894f9;},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x376)]=function(_0x2e309e){const _0x50c9c9=_0x5e5f48,_0x3aaa24=/\\SELFVAR\[(\d+)\]/gi;while(_0x2e309e[_0x50c9c9(0x1b1)](_0x3aaa24)){_0x2e309e=_0x2e309e[_0x50c9c9(0x281)](_0x3aaa24,(_0x3fe94b,_0x17b962)=>getSelfVariableValue(this[_0x50c9c9(0x1db)],this['_eventId'],parseInt(_0x17b962)));}return _0x2e309e;},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x140)]=function(_0x4d4857,_0x9850e8){const _0x126b6e=_0x5e5f48;if(_0x9850e8['match'](/ANIMATION:[ ](\d+)/i))return this[_0x126b6e(0x375)](Number(RegExp['$1']));if(_0x9850e8[_0x126b6e(0x1b1)](/BALLOON:[ ](.*)/i))return this[_0x126b6e(0x1ea)](String(RegExp['$1']));if(_0x9850e8['match'](/FADE IN:[ ](\d+)/i))return this['processMoveRouteFadeIn'](Number(RegExp['$1']));if(_0x9850e8[_0x126b6e(0x1b1)](/FADE OUT:[ ](\d+)/i))return this[_0x126b6e(0x31e)](Number(RegExp['$1']));if(_0x9850e8[_0x126b6e(0x1b1)](/FORCE (?:CARRY|CARRYING|HOLD|HOLDING):[ ](?:TRUE|ON)/i))return this['forceCarrying']();if(_0x9850e8[_0x126b6e(0x1b1)](/FORCE (?:CARRY|CARRYING|HOLD|HOLDING):[ ](?:FALSE|OFF)/i))return this[_0x126b6e(0x40b)]();if(_0x9850e8['match'](/FORCE (?:DASH|DASHING|RUN|RUNNING):[ ](?:TRUE|ON)/i))return this[_0x126b6e(0x3bb)]();if(_0x9850e8[_0x126b6e(0x1b1)](/FORCE (?:DASH|DASHING|RUN|RUNNING):[ ](?:FALSE|OFF)/i))return this[_0x126b6e(0x482)]();if(_0x9850e8['match'](/HUG:[ ]LEFT/i))return this[_0x126b6e(0x477)](_0x126b6e(0x3ba));if(_0x9850e8[_0x126b6e(0x1b1)](/HUG:[ ]RIGHT/i))return this[_0x126b6e(0x477)](_0x126b6e(0x150));if(_0x9850e8[_0x126b6e(0x1b1)](/INDEX:[ ](\d+)/i))return this[_0x126b6e(0x2af)](Number(RegExp['$1']));if(_0x9850e8[_0x126b6e(0x1b1)](/INDEX:[ ]([\+\-]\d+)/i)){const _0x27ac00=this[_0x126b6e(0x4ef)]+Number(RegExp['$1']);return this['processMoveRouteSetIndex'](_0x27ac00);}if(_0x9850e8['match'](/JUMP FORWARD:[ ](\d+)/i))return this[_0x126b6e(0x459)](Number(RegExp['$1']));if(_0x9850e8[_0x126b6e(0x1b1)](/JUMP TO:\s*(\d+)\s*[, ]\s*(\d+)/i))return this['processMoveRouteJumpTo'](Number(RegExp['$1']),Number(RegExp['$2']));if(_0x9850e8[_0x126b6e(0x1b1)](/JUMP TO EVENT:[ ](\d+)/i)){const _0x123fe4=$gameMap['event'](Number(RegExp['$1']));return this[_0x126b6e(0x474)](_0x123fe4);}if(_0x9850e8[_0x126b6e(0x1b1)](/JUMP TO PLAYER/i))return this[_0x126b6e(0x474)]($gamePlayer);if(_0x9850e8[_0x126b6e(0x1b1)](/MOVE[ ](.*)[ ]UNTIL STOP/i)){const _0x265505=String(RegExp['$1']);return this['processMoveRouteMoveUntilStop'](_0x265505);}if(_0x9850e8[_0x126b6e(0x1b1)](/MOVE TO:\s*(\d+)\s*[, ]\s*(\d+)/i)){const _0x1397eb=Number(RegExp['$1']),_0x4cefab=Number(RegExp['$2']);return this[_0x126b6e(0x3bf)](_0x1397eb,_0x4cefab);}if(_0x9850e8[_0x126b6e(0x1b1)](/MOVE TO EVENT:[ ](\d+)/i)){const _0x1aed7d=$gameMap[_0x126b6e(0x192)](Number(RegExp['$1']));return this[_0x126b6e(0x44c)](_0x1aed7d);}if(_0x9850e8[_0x126b6e(0x1b1)](/MOVE TO PLAYER/i))return this['processMoveRouteMoveToCharacter']($gamePlayer);if(_0x9850e8['match'](/MOVE LOWER LEFT:[ ](\d+)/i))return this['processMoveRouteMoveRepeat'](0x1,Number(RegExp['$1']));if(_0x9850e8[_0x126b6e(0x1b1)](/MOVE DOWN:[ ](\d+)/i))return this[_0x126b6e(0x4e7)](0x2,Number(RegExp['$1']));if(_0x9850e8[_0x126b6e(0x1b1)](/MOVE LOWER RIGHT:[ ](\d+)/i))return this[_0x126b6e(0x4e7)](0x3,Number(RegExp['$1']));if(_0x9850e8[_0x126b6e(0x1b1)](/MOVE LEFT:[ ](\d+)/i))return this['processMoveRouteMoveRepeat'](0x4,Number(RegExp['$1']));if(_0x9850e8[_0x126b6e(0x1b1)](/MOVE RIGHT:[ ](\d+)/i))return this[_0x126b6e(0x4e7)](0x6,Number(RegExp['$1']));if(_0x9850e8[_0x126b6e(0x1b1)](/MOVE UPPER LEFT:[ ](\d+)/i))return this[_0x126b6e(0x4e7)](0x7,Number(RegExp['$1']));if(_0x9850e8[_0x126b6e(0x1b1)](/MOVE UP:[ ](\d+)/i))return this[_0x126b6e(0x4e7)](0x8,Number(RegExp['$1']));if(_0x9850e8[_0x126b6e(0x1b1)](/MOVE UPPER RIGHT:[ ](\d+)/i))return this[_0x126b6e(0x4e7)](0x9,Number(RegExp['$1']));if(_0x9850e8['match'](/OPACITY:[ ](\d+)([%％])/i)){const _0x1b6aa0=Math[_0x126b6e(0x1e9)](Number(RegExp['$1'])/0x64*0xff);return this[_0x126b6e(0x3cd)](_0x1b6aa0[_0x126b6e(0x266)](0x0,0xff));}if(_0x9850e8[_0x126b6e(0x1b1)](/OPACITY:[ ]([\+\-]\d+)([%％])/i)){const _0xda0f80=this[_0x126b6e(0x138)]+Math['round'](Number(RegExp['$1'])/0x64*0xff);return this['setOpacity'](_0xda0f80[_0x126b6e(0x266)](0x0,0xff));}if(_0x9850e8[_0x126b6e(0x1b1)](/OPACITY:[ ]([\+\-]\d+)/i)){const _0x28f99f=this['_opacity']+Number(RegExp['$1']);return this[_0x126b6e(0x3cd)](_0x28f99f[_0x126b6e(0x266)](0x0,0xff));}if(_0x9850e8[_0x126b6e(0x1b1)](/PATTERN LOCK:[ ](\d+)/i))return this[_0x126b6e(0x191)](Number(RegExp['$1']));if(_0x9850e8['match'](/PATTERN UNLOCK/i))return this['_patternLocked']=![];if(_0x9850e8[_0x126b6e(0x1b1)](/POSE:[ ](.*)/i)){const _0x5b6674=String(RegExp['$1'])[_0x126b6e(0x3ff)]()[_0x126b6e(0x372)]();return this[_0x126b6e(0x441)](_0x5b6674);}if(_0x9850e8[_0x126b6e(0x1b1)](/STEP TOWARD:\s*(\d+)\s*[, ]\s*(\d+)/i)){const _0x4ab600=Number(RegExp['$1']),_0xf2e318=Number(RegExp['$2']);return this[_0x126b6e(0x240)](_0x4ab600,_0xf2e318);}if(_0x9850e8[_0x126b6e(0x1b1)](/STEP TOWARD EVENT:[ ](\d+)/i)){const _0x4d1486=$gameMap[_0x126b6e(0x192)](Number(RegExp['$1']));return this[_0x126b6e(0x31c)](_0x4d1486);}if(_0x9850e8[_0x126b6e(0x1b1)](/STEP TOWARD PLAYER/i))return this[_0x126b6e(0x1df)]($gamePlayer);if(_0x9850e8[_0x126b6e(0x1b1)](/STEP AWAY FROM:\s*(\d+)\s*[, ]\s*(\d+)/i))return this[_0x126b6e(0x316)](Number(RegExp['$1']),Number(RegExp['$2']));if(_0x9850e8[_0x126b6e(0x1b1)](/STEP AWAY FROM EVENT:[ ](\d+)/i)){const _0x2941e7=$gameMap['event'](Number(RegExp['$1']));return this[_0x126b6e(0x471)](_0x2941e7);}if(_0x9850e8[_0x126b6e(0x1b1)](/STEP AWAY FROM PLAYER/i))return this[_0x126b6e(0x471)]($gamePlayer);if(_0x9850e8['match'](/TURN TO:\s*(\d+)\s*[, ]\s*(\d+)/i))return this[_0x126b6e(0x291)](Number(RegExp['$1']),Number(RegExp['$2']));if(_0x9850e8[_0x126b6e(0x1b1)](/TURN TO EVENT:[ ](\d+)/i)){const _0x114370=$gameMap[_0x126b6e(0x192)](Number(RegExp['$1']));return this[_0x126b6e(0x28b)](_0x114370);}if(_0x9850e8['match'](/TURN TO PLAYER/i))return this['turnTowardCharacter']($gamePlayer);if(_0x9850e8[_0x126b6e(0x1b1)](/TURN AWAY FROM:\s*(\d+)\s*[, ]\s*(\d+)/i))return this[_0x126b6e(0x45c)](Number(RegExp['$1']),Number(RegExp['$2']));if(_0x9850e8['match'](/TURN AWAY FROM EVENT:[ ](\d+)/i)){const _0x4cee0a=$gameMap[_0x126b6e(0x192)](Number(RegExp['$1']));return this['turnAwayFromCharacter'](_0x4cee0a);}if(_0x9850e8[_0x126b6e(0x1b1)](/TURN AWAY FROM PLAYER/i))return this['turnAwayFromCharacter']($gamePlayer);if(_0x9850e8['match'](/TURN LOWER LEFT/i))return this[_0x126b6e(0x117)](0x1);if(_0x9850e8[_0x126b6e(0x1b1)](/TURN LOWER RIGHT/i))return this[_0x126b6e(0x117)](0x3);if(_0x9850e8['match'](/TURN UPPER LEFT/i))return this['setDirection'](0x7);if(_0x9850e8[_0x126b6e(0x1b1)](/TURN UPPER RIGHT/i))return this['setDirection'](0x9);if(_0x9850e8[_0x126b6e(0x1b1)](/Self Switch[ ](.*):[ ](.*)/i))return this[_0x126b6e(0x331)](RegExp['$1'],RegExp['$2']);if(_0x9850e8[_0x126b6e(0x1b1)](/Self Variable[ ](.*):[ ](.*)/i))return this[_0x126b6e(0x38e)](RegExp['$1'],RegExp['$2']);if(_0x9850e8[_0x126b6e(0x1b1)](/TELEPORT TO:\s*(\d+)\s*[, ]\s*(\d+)/i))return this[_0x126b6e(0x39c)](Number(RegExp['$1']),Number(RegExp['$2']));if(_0x9850e8['match'](/TELEPORT TO EVENT:[ ](\d+)/i)){const _0x4e9c81=$gameMap[_0x126b6e(0x192)](Number(RegExp['$1']));return this['processMoveRouteTeleportToCharacter'](_0x4e9c81);}if(_0x9850e8['match'](/TELEPORT TO PLAYER/i))return this[_0x126b6e(0x4d2)]($gamePlayer);try{VisuMZ[_0x126b6e(0x248)][_0x126b6e(0x472)][_0x126b6e(0x2a6)](this,_0x4d4857);}catch(_0x3e29d9){if($gameTemp[_0x126b6e(0x12f)]())console[_0x126b6e(0x42d)](_0x3e29d9);}},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x375)]=function(_0x5dc80e){const _0x5cfd81=_0x5e5f48;$gameTemp[_0x5cfd81(0x332)]([this],_0x5dc80e);},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x1ea)]=function(_0x565f41){const _0x325794=_0x5e5f48;let _0x5e0314=0x0;switch(_0x565f41[_0x325794(0x3ff)]()[_0x325794(0x372)]()){case'!':case'EXCLAMATION':_0x5e0314=0x1;break;case'?':case _0x325794(0x19c):_0x5e0314=0x2;break;case _0x325794(0x31b):case _0x325794(0x1ec):case'MUSIC\x20NOTE':case _0x325794(0x2be):case _0x325794(0x13a):_0x5e0314=0x3;break;case _0x325794(0x450):case _0x325794(0x495):_0x5e0314=0x4;break;case _0x325794(0x3fc):_0x5e0314=0x5;break;case _0x325794(0x2b8):_0x5e0314=0x6;break;case _0x325794(0x455):case _0x325794(0x167):case _0x325794(0x2f9):_0x5e0314=0x7;break;case _0x325794(0x439):case _0x325794(0x1ac):_0x5e0314=0x8;break;case'LIGHT':case _0x325794(0x42c):case _0x325794(0x4d1):case'LIGHT-BULB':case _0x325794(0x1ef):_0x5e0314=0x9;break;case'Z':case'ZZ':case _0x325794(0x229):case _0x325794(0x4f2):_0x5e0314=0xa;break;case _0x325794(0x1c0):_0x5e0314=0xb;break;case _0x325794(0x2f8):_0x5e0314=0xc;break;case _0x325794(0x2b4):_0x5e0314=0xd;break;case _0x325794(0x36c):_0x5e0314=0xe;break;case _0x325794(0x17a):_0x5e0314=0xf;break;}$gameTemp[_0x325794(0x4a8)](this,_0x5e0314);},Game_Character['prototype']['processMoveRouteFadeIn']=function(_0x573c57){const _0x36daf7=_0x5e5f48;_0x573c57+=this[_0x36daf7(0x138)],this[_0x36daf7(0x3cd)](_0x573c57[_0x36daf7(0x266)](0x0,0xff));if(this[_0x36daf7(0x138)]<0xff)this[_0x36daf7(0x12c)]--;},Game_Character['prototype'][_0x5e5f48(0x31e)]=function(_0x2384aa){const _0x32c6b2=_0x5e5f48;_0x2384aa=this[_0x32c6b2(0x138)]-_0x2384aa,this[_0x32c6b2(0x3cd)](_0x2384aa['clamp'](0x0,0xff));if(this[_0x32c6b2(0x138)]>0x0)this['_moveRouteIndex']--;},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x477)]=function(_0x2d252){const _0x3df18e=_0x5e5f48,_0x57ff15=[0x0,0x3,0x6,0x9,0x2,0x0,0x8,0x1,0x4,0x7],_0x4d4b5f=[0x0,0x7,0x4,0x1,0x8,0x0,0x2,0x9,0x6,0x3],_0x4528e1=this['direction'](),_0x40bc5d=(_0x2d252===_0x3df18e(0x3ba)?_0x57ff15:_0x4d4b5f)[_0x4528e1],_0x336580=(_0x2d252===_0x3df18e(0x3ba)?_0x4d4b5f:_0x57ff15)[_0x4528e1];if(this[_0x3df18e(0x27b)](this['x'],this['y'],_0x40bc5d))_0x2d252==='left'?this['turnLeft90']():this[_0x3df18e(0x1d7)]();else!this[_0x3df18e(0x27b)](this['x'],this['y'],this[_0x3df18e(0x2dd)]())&&(this['canPass'](this['x'],this['y'],_0x336580)?_0x2d252===_0x3df18e(0x3ba)?this[_0x3df18e(0x1d7)]():this['turnLeft90']():this[_0x3df18e(0x12e)]());this[_0x3df18e(0x27b)](this['x'],this['y'],this[_0x3df18e(0x2dd)]())&&this[_0x3df18e(0x2d7)]();},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x2af)]=function(_0x185928){const _0x1b4bb7=_0x5e5f48;if(ImageManager[_0x1b4bb7(0x382)](this[_0x1b4bb7(0x13d)]))return;_0x185928=_0x185928['clamp'](0x0,0x7),this[_0x1b4bb7(0x284)](this[_0x1b4bb7(0x13d)],_0x185928);},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x459)]=function(_0x3a5c0d){const _0x10de26=_0x5e5f48;switch(this[_0x10de26(0x2dd)]()){case 0x1:this[_0x10de26(0x402)](-_0x3a5c0d,_0x3a5c0d);break;case 0x2:this['jump'](0x0,_0x3a5c0d);break;case 0x3:this[_0x10de26(0x402)](_0x3a5c0d,_0x3a5c0d);break;case 0x4:this[_0x10de26(0x402)](-_0x3a5c0d,0x0);break;case 0x6:this[_0x10de26(0x402)](_0x3a5c0d,0x0);break;case 0x7:this[_0x10de26(0x402)](-_0x3a5c0d,-_0x3a5c0d);break;case 0x8:this['jump'](0x0,-_0x3a5c0d);break;case 0x9:this[_0x10de26(0x402)](_0x3a5c0d,-_0x3a5c0d);break;}},Game_Character[_0x5e5f48(0x363)]['processMoveRouteJumpTo']=function(_0x52bb01,_0x234c2c){const _0x54599f=_0x5e5f48,_0x429491=Math[_0x54599f(0x1e9)](_0x52bb01-this['x']),_0x4c891e=Math[_0x54599f(0x1e9)](_0x234c2c-this['y']);this[_0x54599f(0x402)](_0x429491,_0x4c891e);},Game_Character['prototype'][_0x5e5f48(0x474)]=function(_0x260544){const _0x26a98b=_0x5e5f48;if(_0x260544)this[_0x26a98b(0x2c5)](_0x260544['x'],_0x260544['y']);},Game_Character[_0x5e5f48(0x363)]['processMoveRouteStepTo']=function(_0x457f88,_0x482c9f){const _0x4f6150=_0x5e5f48;let _0x23d0bf=0x0;$gameMap[_0x4f6150(0x473)]()?_0x23d0bf=this[_0x4f6150(0x3da)](_0x457f88,_0x482c9f):_0x23d0bf=this[_0x4f6150(0x4dc)](_0x457f88,_0x482c9f),this[_0x4f6150(0x337)](_0x23d0bf),this[_0x4f6150(0x4e8)](!![]);},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x31c)]=function(_0x11bb24){if(_0x11bb24)this['processMoveRouteStepTo'](_0x11bb24['x'],_0x11bb24['y']);},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x14c)]=function(_0x264c90,_0x3db761){const _0x3a1dec=_0x5e5f48,_0x59fc54=this[_0x3a1dec(0x272)](_0x264c90),_0x1cbbda=this[_0x3a1dec(0x3ef)](_0x3db761);},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x405)]=function(_0x5a911b){const _0x5bee73=_0x5e5f48,_0x3c4610=['',_0x5bee73(0x32f),_0x5bee73(0x2a9),_0x5bee73(0x4f1),_0x5bee73(0x1f8),'',_0x5bee73(0x16d),_0x5bee73(0x2e1),'UP',_0x5bee73(0x399)],_0x469170=_0x3c4610[_0x5bee73(0x436)](_0x5a911b[_0x5bee73(0x3ff)]()[_0x5bee73(0x372)]());if(directioin<=0x0)return;this['canPass'](this['x'],this['y'],_0x469170)&&(this[_0x5bee73(0x337)](_0x469170),this[_0x5bee73(0x12c)]-=0x1);},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x3bf)]=function(_0x1a2763,_0x287def){const _0x56f77e=_0x5e5f48;this[_0x56f77e(0x240)](_0x1a2763,_0x287def);if(this['x']!==_0x1a2763||this['y']!==_0x287def)this['_moveRouteIndex']--;},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x44c)]=function(_0x1fd7d2){if(_0x1fd7d2)this['processMoveRouteMoveTo'](_0x1fd7d2['x'],_0x1fd7d2['y']);},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x4e7)]=function(_0x3dd658,_0x485c8b){const _0xe389db=_0x5e5f48;_0x485c8b=_0x485c8b||0x0;const _0x5ca568={'code':0x1,'indent':null,'parameters':[]};_0x5ca568[_0xe389db(0x212)]=[0x0,0x5,0x1,0x6,0x2,0x0,0x3,0x7,0x4,0x8][_0x3dd658],this[_0xe389db(0x157)][_0xe389db(0x3ee)][this[_0xe389db(0x12c)]][_0xe389db(0x273)][0x0]='';while(_0x485c8b--){this[_0xe389db(0x157)][_0xe389db(0x3ee)][_0xe389db(0x120)](this['_moveRouteIndex']+0x1,0x0,_0x5ca568);}},Game_Character[_0x5e5f48(0x363)]['processMoveRoutePatternLock']=function(_0x2af819){const _0x4106a3=_0x5e5f48;this['_patternLocked']=!![],this[_0x4106a3(0x440)](_0x2af819);},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x331)]=function(_0x31dda6,_0x2f50e3){const _0x2c446d=_0x5e5f48;if(this===$gamePlayer)return;const _0x4d15fb=[this[_0x2c446d(0x1db)],this['_eventId'],'A'];_0x31dda6[_0x2c446d(0x1b1)](/\b[ABCD]\b/i)?_0x4d15fb[0x2]=String(_0x31dda6)['charAt'](0x0)[_0x2c446d(0x3ff)]()[_0x2c446d(0x372)]():_0x4d15fb[0x2]=_0x2c446d(0x22a)[_0x2c446d(0x2ac)](_0x31dda6);switch(_0x2f50e3['toUpperCase']()[_0x2c446d(0x372)]()){case'ON':case _0x2c446d(0x269):$gameSelfSwitches[_0x2c446d(0x3ce)](_0x4d15fb,!![]);break;case _0x2c446d(0x25d):case _0x2c446d(0x39d):$gameSelfSwitches[_0x2c446d(0x3ce)](_0x4d15fb,![]);break;case _0x2c446d(0x387):$gameSelfSwitches['setValue'](_0x4d15fb,!$gameSelfSwitches[_0x2c446d(0x4c4)](_0x4d15fb));break;}},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x38e)]=function(_0x7154f,_0x1785c4){const _0x57e243=_0x5e5f48;if(this===$gamePlayer)return;const _0x1cdb50=[this[_0x57e243(0x1db)],this[_0x57e243(0x21a)],'Self\x20Variable\x20%1'[_0x57e243(0x2ac)](switchId)];$gameSelfSwitches[_0x57e243(0x3ce)](_0x1cdb50,Number(_0x1785c4));},Game_Character['prototype']['processMoveRouteTeleportTo']=function(_0x15213c,_0x58e5fa){const _0x2eebaa=_0x5e5f48;this[_0x2eebaa(0x3eb)](_0x15213c,_0x58e5fa);},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x4d2)]=function(_0x3af0ab){if(_0x3af0ab)this['processMoveRouteTeleportTo'](_0x3af0ab['x'],_0x3af0ab['y']);},Game_Character['prototype'][_0x5e5f48(0x1d7)]=function(){const _0x5213cb=_0x5e5f48;switch(this[_0x5213cb(0x2dd)]()){case 0x1:this[_0x5213cb(0x117)](0x7);break;case 0x2:this[_0x5213cb(0x117)](0x4);break;case 0x3:this[_0x5213cb(0x117)](0x1);break;case 0x4:this[_0x5213cb(0x117)](0x8);break;case 0x6:this[_0x5213cb(0x117)](0x2);break;case 0x7:this[_0x5213cb(0x117)](0x9);break;case 0x8:this[_0x5213cb(0x117)](0x6);break;case 0x9:this[_0x5213cb(0x117)](0x3);break;}},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x321)]=function(){const _0x79f26c=_0x5e5f48;switch(this['direction']()){case 0x1:this[_0x79f26c(0x117)](0x3);break;case 0x2:this[_0x79f26c(0x117)](0x6);break;case 0x3:this[_0x79f26c(0x117)](0x9);break;case 0x4:this['setDirection'](0x2);break;case 0x6:this['setDirection'](0x8);break;case 0x7:this['setDirection'](0x1);break;case 0x8:this[_0x79f26c(0x117)](0x4);break;case 0x9:this[_0x79f26c(0x117)](0x7);break;}},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x28d)]=function(_0x1da5b2,_0x86e823,_0x672643){const _0x5ebc7d=_0x5e5f48,_0x1cdad2=this[_0x5ebc7d(0x272)](_0x1da5b2),_0x50217e=this[_0x5ebc7d(0x3ef)](_0x86e823);if($gameMap[_0x5ebc7d(0x473)]()){if(_0x672643||this[_0x5ebc7d(0x1cd)]()){if(_0x1cdad2>0x0&&_0x50217e<0x0)return 0x1;if(_0x1cdad2<0x0&&_0x50217e<0x0)return 0x3;if(_0x1cdad2>0x0&&_0x50217e>0x0)return 0x7;if(_0x1cdad2<0x0&&_0x50217e>0x0)return 0x9;}}if(Math[_0x5ebc7d(0x392)](_0x1cdad2)>Math[_0x5ebc7d(0x392)](_0x50217e))return _0x1cdad2>0x0?0x4:0x6;else{if(_0x50217e!==0x0)return _0x50217e>0x0?0x8:0x2;}return 0x0;},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x33d)]=function(_0x435c4f,_0x9a86f9,_0x587e9c){const _0x58f68b=_0x5e5f48,_0x15be81=this[_0x58f68b(0x272)](_0x435c4f),_0xb0709a=this['deltaYFrom'](_0x9a86f9);if($gameMap['isSupportDiagonalMovement']()){if(_0x587e9c||this[_0x58f68b(0x1cd)]()){if(_0x15be81>0x0&&_0xb0709a<0x0)return 0x9;if(_0x15be81<0x0&&_0xb0709a<0x0)return 0x7;if(_0x15be81>0x0&&_0xb0709a>0x0)return 0x3;if(_0x15be81<0x0&&_0xb0709a>0x0)return 0x1;}}if(Math[_0x58f68b(0x392)](_0x15be81)>Math['abs'](_0xb0709a))return _0x15be81>0x0?0x6:0x4;else{if(_0xb0709a!==0x0)return _0xb0709a>0x0?0x2:0x8;}return 0x0;},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x291)]=function(_0x4ed427,_0x93f0a){const _0x2944a1=_0x5e5f48,_0x429ec=this['getDirectionToPoint'](_0x4ed427,_0x93f0a,!![]);if(_0x429ec)this[_0x2944a1(0x337)](_0x429ec);},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x316)]=function(_0x41eb09,_0x3c92c7){const _0x235228=_0x5e5f48,_0x218f72=this[_0x235228(0x33d)](_0x41eb09,_0x3c92c7,!![]);if(_0x218f72)this[_0x235228(0x337)](_0x218f72);},Game_Character['prototype']['turnTowardPoint']=function(_0x322c97,_0x5edf03){const _0x33a803=_0x5e5f48,_0x2a64fb=this[_0x33a803(0x28d)](_0x322c97,_0x5edf03,![]);if(_0x2a64fb)this['setDirection'](_0x2a64fb);},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x45c)]=function(_0x5652ee,_0x2b81c7){const _0x3c1f9c=_0x5e5f48,_0x42447e=this[_0x3c1f9c(0x33d)](_0x5652ee,_0x2b81c7,![]);if(_0x42447e)this[_0x3c1f9c(0x117)](_0x42447e);},Game_Character[_0x5e5f48(0x363)]['moveTowardCharacter']=function(_0x439237){const _0x3a804e=_0x5e5f48;if(_0x439237)this[_0x3a804e(0x291)](_0x439237['x'],_0x439237['y']);},Game_Character['prototype']['moveAwayFromCharacter']=function(_0x124dd4){const _0x51c3da=_0x5e5f48;if(_0x124dd4)this[_0x51c3da(0x316)](_0x124dd4['x'],_0x124dd4['y']);},Game_Character[_0x5e5f48(0x363)][_0x5e5f48(0x28b)]=function(_0x17d385){const _0x44844f=_0x5e5f48;if(_0x17d385)this[_0x44844f(0x480)](_0x17d385['x'],_0x17d385['y']);},Game_Character['prototype'][_0x5e5f48(0x4c1)]=function(_0x39283a){const _0x4e95f9=_0x5e5f48;if(_0x39283a)this[_0x4e95f9(0x45c)](_0x39283a['x'],_0x39283a['y']);},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x1ab)]=Game_Player['prototype'][_0x5e5f48(0x2f3)],Game_Player[_0x5e5f48(0x363)][_0x5e5f48(0x2f3)]=function(){const _0x2bef36=_0x5e5f48;if(this[_0x2bef36(0x3d0)])return!![];return VisuMZ[_0x2bef36(0x248)][_0x2bef36(0x1ab)]['call'](this);},Game_Player[_0x5e5f48(0x363)]['isDashingAndMoving']=function(){const _0x197682=_0x5e5f48;return this[_0x197682(0x2f3)]()&&(this[_0x197682(0x347)]()||this[_0x197682(0x1b5)]()!==0x0&&this[_0x197682(0x27b)](this['_x'],this['_y'],this['getInputDirection']())||$gameTemp['isDestinationValid']());},VisuMZ['EventsMoveCore'][_0x5e5f48(0x268)]=Game_Player['prototype'][_0x5e5f48(0x1b5)],Game_Player[_0x5e5f48(0x363)][_0x5e5f48(0x1b5)]=function(){const _0x1a3051=_0x5e5f48;return $gameMap[_0x1a3051(0x473)]()?this[_0x1a3051(0x4e2)]():VisuMZ[_0x1a3051(0x248)][_0x1a3051(0x268)][_0x1a3051(0x2a6)](this);},Game_Player[_0x5e5f48(0x363)][_0x5e5f48(0x4e2)]=function(){return Input['dir8'];},Game_Player['prototype']['moveByInput']=function(){const _0x4ade4c=_0x5e5f48;if($gameSystem['isPlayerControlDisabled']())return 0x0;if(!this[_0x4ade4c(0x347)]()&&this[_0x4ade4c(0x462)]()){let _0x535a27=this[_0x4ade4c(0x1b5)]();if(_0x535a27>0x0)$gameTemp[_0x4ade4c(0x411)]();else{if($gameTemp['isDestinationValid']()){const _0x24fd88=$gameTemp[_0x4ade4c(0x3a2)](),_0x44c363=$gameTemp[_0x4ade4c(0x4e0)](),_0x3ecfe5=$gameMap[_0x4ade4c(0x473)](),_0x583848=$gameMap[_0x4ade4c(0x438)](_0x24fd88,_0x44c363),_0x1e6a4c=$gameMap[_0x4ade4c(0x1bf)](_0x24fd88,_0x44c363)['length']<=0x0;_0x3ecfe5&&_0x583848&&_0x1e6a4c?_0x535a27=this[_0x4ade4c(0x3da)](_0x24fd88,_0x44c363):_0x535a27=this[_0x4ade4c(0x4dc)](_0x24fd88,_0x44c363);}}_0x535a27>0x0?(this['_inputTime']=this['_inputTime']||0x0,this['isTurnInPlace']()?this[_0x4ade4c(0x117)](_0x535a27):this[_0x4ade4c(0x275)](_0x535a27),this['_inputTime']++):this[_0x4ade4c(0x47a)]=0x0;}},Game_Player['prototype'][_0x5e5f48(0x336)]=function(){const _0x4e234f=_0x5e5f48,_0x2e28cb=VisuMZ[_0x4e234f(0x248)][_0x4e234f(0x1e5)][_0x4e234f(0x1b0)];if(!_0x2e28cb[_0x4e234f(0x11a)])return![];if($gameTemp[_0x4e234f(0x23e)]())return![];if(this[_0x4e234f(0x2f3)]()||this[_0x4e234f(0x347)]()||this['isOnLadder']())return![];return this[_0x4e234f(0x47a)]<_0x2e28cb[_0x4e234f(0x3f1)];},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x160)]=Game_Player[_0x5e5f48(0x363)][_0x5e5f48(0x275)],Game_Player[_0x5e5f48(0x363)]['executeMove']=function(_0x255b28){const _0x3d14d4=_0x5e5f48;$gameMap['isSupportDiagonalMovement']()?this[_0x3d14d4(0x337)](_0x255b28):VisuMZ[_0x3d14d4(0x248)][_0x3d14d4(0x160)][_0x3d14d4(0x2a6)](this,_0x255b28);},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x261)]=Game_Player[_0x5e5f48(0x363)][_0x5e5f48(0x334)],Game_Player[_0x5e5f48(0x363)]['isMapPassable']=function(_0x3e46b4,_0x3c435e,_0x55f030){const _0x327508=_0x5e5f48;if($gameMap['isRegionAllowPass'](_0x3e46b4,_0x3c435e,_0x55f030,_0x327508(0x433)))return!![];if($gameMap['isRegionForbidPass'](_0x3e46b4,_0x3c435e,_0x55f030,_0x327508(0x433)))return![];return VisuMZ[_0x327508(0x248)][_0x327508(0x261)][_0x327508(0x2a6)](this,_0x3e46b4,_0x3c435e,_0x55f030);},VisuMZ[_0x5e5f48(0x248)]['Game_Player_checkEventTriggerHere']=Game_Player[_0x5e5f48(0x363)][_0x5e5f48(0x2c6)],Game_Player[_0x5e5f48(0x363)]['checkEventTriggerHere']=function(_0x58e222){const _0x1c9d09=_0x5e5f48;VisuMZ[_0x1c9d09(0x248)]['Game_Player_checkEventTriggerHere'][_0x1c9d09(0x2a6)](this,_0x58e222);if(this[_0x1c9d09(0x40c)]()){this['checkEventTriggerEventsMoveCore'](_0x58e222);if(_0x58e222[_0x1c9d09(0x2d5)](0x0)&&this[_0x1c9d09(0x1ff)]()===_0x1c9d09(0x340))this['startMapCommonEventOnOK'](this['x'],this['y']);else(_0x58e222[_0x1c9d09(0x2d5)](0x1)||_0x58e222[_0x1c9d09(0x2d5)](0x2))&&this[_0x1c9d09(0x39e)]();}},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x355)]=Game_Player[_0x5e5f48(0x363)][_0x5e5f48(0x277)],Game_Player[_0x5e5f48(0x363)][_0x5e5f48(0x277)]=function(_0x38c402){const _0x550ce0=_0x5e5f48;VisuMZ[_0x550ce0(0x248)][_0x550ce0(0x355)][_0x550ce0(0x2a6)](this,_0x38c402);if(this[_0x550ce0(0x40c)]()&&_0x38c402['includes'](0x0)&&this[_0x550ce0(0x1ff)]()===_0x550ce0(0x4ec)){const _0x55e7d5=this[_0x550ce0(0x2dd)](),_0x16b726=$gameMap['roundXWithDirection'](this['x'],_0x55e7d5),_0x4491e1=$gameMap[_0x550ce0(0x49c)](this['y'],_0x55e7d5);this[_0x550ce0(0x35a)](_0x16b726,_0x4491e1);}},Game_Player[_0x5e5f48(0x363)][_0x5e5f48(0x163)]=function(_0x36e670){const _0x358af5=_0x5e5f48;if($gameMap[_0x358af5(0x478)]())return;if($gameMap[_0x358af5(0x231)]())return;const _0x30080a=$gameMap[_0x358af5(0x3f5)]();for(const _0x321a27 of _0x30080a){if(!_0x321a27)continue;if(!_0x321a27['isTriggerIn'](_0x36e670))continue;if(this['meetActivationRegionConditions'](_0x321a27))return _0x321a27['start']();if(this[_0x358af5(0x28c)](_0x321a27))return _0x321a27[_0x358af5(0x2de)]();}},Game_Player[_0x5e5f48(0x363)][_0x5e5f48(0x296)]=function(_0x429736){const _0x57d771=_0x5e5f48;if($gameMap[_0x57d771(0x478)]())return![];if($gameMap[_0x57d771(0x231)]())return![];return _0x429736[_0x57d771(0x18a)]()['includes'](this['regionId']());},Game_Player['prototype']['meetActivationProximityConditions']=function(_0x54fac4){const _0x4e8d7c=_0x5e5f48;if($gameMap[_0x4e8d7c(0x478)]())return![];if($gameMap[_0x4e8d7c(0x231)]())return![];if([_0x4e8d7c(0x185),_0x4e8d7c(0x400)]['includes'](_0x54fac4[_0x4e8d7c(0x2ec)]()))return![];const _0x37de97=_0x54fac4[_0x4e8d7c(0x2ec)](),_0x1fcec0=_0x54fac4[_0x4e8d7c(0x14d)]();switch(_0x37de97){case _0x4e8d7c(0x4da):const _0x4e638e=$gameMap[_0x4e8d7c(0x35f)](this['x'],this['y'],_0x54fac4['x'],_0x54fac4['y']);return _0x54fac4[_0x4e8d7c(0x14d)]()>=_0x4e638e;break;case _0x4e8d7c(0x23d):return _0x1fcec0>=Math[_0x4e8d7c(0x392)](_0x54fac4[_0x4e8d7c(0x272)](this['x']))&&_0x1fcec0>=Math[_0x4e8d7c(0x392)](_0x54fac4[_0x4e8d7c(0x3ef)](this['y']));break;case _0x4e8d7c(0x3ab):return _0x1fcec0>=Math[_0x4e8d7c(0x392)](_0x54fac4[_0x4e8d7c(0x3ef)](this['y']));break;case _0x4e8d7c(0x4ae):return _0x1fcec0>=Math[_0x4e8d7c(0x392)](_0x54fac4['deltaXFrom'](this['x']));break;case _0x4e8d7c(0x2c0):return![];break;}},Game_Player[_0x5e5f48(0x363)][_0x5e5f48(0x35a)]=function(_0x5f4d5f,_0x23af57){const _0x54a283=_0x5e5f48;if($gameMap[_0x54a283(0x478)]())return;if($gameMap[_0x54a283(0x231)]())return;let _0x42648b=VisuMZ[_0x54a283(0x248)][_0x54a283(0x1e5)][_0x54a283(0x24f)],_0x29c9f0=$gameMap['regionId'](_0x5f4d5f,_0x23af57);const _0x4c7393=_0x54a283(0x2fb)[_0x54a283(0x2ac)](_0x29c9f0);_0x42648b[_0x4c7393]&&$gameTemp[_0x54a283(0x454)](_0x42648b[_0x4c7393]);},Game_Player['prototype'][_0x5e5f48(0x1ff)]=function(){const _0x32999f=_0x5e5f48;return VisuMZ[_0x32999f(0x248)][_0x32999f(0x1e5)][_0x32999f(0x48b)];},Game_Player[_0x5e5f48(0x363)][_0x5e5f48(0x39e)]=function(){const _0x32d664=_0x5e5f48;if($gameMap[_0x32d664(0x478)]())return;if($gameMap[_0x32d664(0x231)]())return;let _0xb27d1a=VisuMZ['EventsMoveCore'][_0x32d664(0x1e5)]['RegionTouch'];const _0x4529c8=_0x32d664(0x2fb)[_0x32d664(0x2ac)](this[_0x32d664(0x2ae)]());_0xb27d1a[_0x4529c8]&&$gameTemp['reserveCommonEvent'](_0xb27d1a[_0x4529c8]);},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x44d)]=Game_Player[_0x5e5f48(0x363)][_0x5e5f48(0x359)],Game_Player['prototype']['increaseSteps']=function(){const _0x5d6fc8=_0x5e5f48;VisuMZ[_0x5d6fc8(0x248)][_0x5d6fc8(0x44d)][_0x5d6fc8(0x2a6)](this),VisuMZ['MoveAllSynchTargets'](0x0);},VisuMZ[_0x5e5f48(0x248)]['Game_Follower_initialize']=Game_Follower[_0x5e5f48(0x363)][_0x5e5f48(0x2b9)],Game_Follower[_0x5e5f48(0x363)][_0x5e5f48(0x2b9)]=function(_0x735966){const _0x3da6aa=_0x5e5f48;VisuMZ[_0x3da6aa(0x248)][_0x3da6aa(0x173)][_0x3da6aa(0x2a6)](this,_0x735966),this['_chaseOff']=![];},Game_Follower['prototype'][_0x5e5f48(0x2f3)]=function(){return $gamePlayer['isDashing']();},Game_Follower[_0x5e5f48(0x363)][_0x5e5f48(0x344)]=function(){const _0x2ccc5f=_0x5e5f48;return $gamePlayer[_0x2ccc5f(0x344)]();},Game_Follower['prototype'][_0x5e5f48(0x286)]=function(){const _0x545bd8=_0x5e5f48;return $gamePlayer[_0x545bd8(0x286)]();},Game_Follower[_0x5e5f48(0x363)][_0x5e5f48(0x3bd)]=function(_0x109312){const _0x5a60aa=_0x5e5f48;this[_0x5a60aa(0x2fd)]=_0x109312;},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x4ac)]=Game_Follower['prototype'][_0x5e5f48(0x4b5)],Game_Follower[_0x5e5f48(0x363)][_0x5e5f48(0x4b5)]=function(_0x307f61){const _0x4ffc05=_0x5e5f48;if(this[_0x4ffc05(0x2fd)])return;if($gameSystem[_0x4ffc05(0x385)]())return;VisuMZ['EventsMoveCore']['Game_Follower_chaseCharacter'][_0x4ffc05(0x2a6)](this,_0x307f61);},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x4bb)]=Game_Vehicle['prototype']['isMapPassable'],Game_Vehicle[_0x5e5f48(0x363)][_0x5e5f48(0x334)]=function(_0x48093e,_0x12a2cc,_0x158450){const _0x2b6d62=_0x5e5f48;if($gameMap[_0x2b6d62(0x204)](_0x48093e,_0x12a2cc,_0x158450,this['_type']))return!![];if($gameMap[_0x2b6d62(0x3ae)](_0x48093e,_0x12a2cc,_0x158450,this[_0x2b6d62(0x1c4)]))return![];return VisuMZ[_0x2b6d62(0x248)]['Game_Vehicle_isMapPassable'][_0x2b6d62(0x2a6)](this,_0x48093e,_0x12a2cc,_0x158450);},Game_Vehicle[_0x5e5f48(0x363)][_0x5e5f48(0x445)]=function(_0x35146a,_0x4baa53,_0x14b78b){const _0x60a731=_0x5e5f48;if($gameMap[_0x60a731(0x204)](_0x35146a,_0x4baa53,_0x14b78b,this[_0x60a731(0x1c4)]))return!![];if($gameMap[_0x60a731(0x3ae)](_0x35146a,_0x4baa53,_0x14b78b,this[_0x60a731(0x1c4)]))return![];return VisuMZ[_0x60a731(0x248)][_0x60a731(0x489)][_0x60a731(0x2a6)]($gamePlayer,_0x35146a,_0x4baa53,_0x14b78b);},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x407)]=Game_Vehicle['prototype'][_0x5e5f48(0x4b0)],Game_Vehicle[_0x5e5f48(0x363)][_0x5e5f48(0x4b0)]=function(_0x54d8c9,_0x592b00,_0x2574dc){const _0x55ec6a=_0x5e5f48;if($gameMap[_0x55ec6a(0x1b4)](_0x54d8c9,_0x592b00,_0x2574dc,this[_0x55ec6a(0x1c4)]))return!![];const _0xfd9aac=this['_type'][_0x55ec6a(0x18f)](0x0)['toUpperCase']()+this[_0x55ec6a(0x1c4)]['slice'](0x1),_0x38c6e5=_0x55ec6a(0x4a5)[_0x55ec6a(0x2ac)](_0xfd9aac);return VisuMZ['EventsMoveCore'][_0x55ec6a(0x1e5)][_0x55ec6a(0x470)][_0x38c6e5]?![]:VisuMZ[_0x55ec6a(0x248)][_0x55ec6a(0x407)][_0x55ec6a(0x2a6)](this,_0x54d8c9,_0x592b00,_0x2574dc);},VisuMZ['EventsMoveCore'][_0x5e5f48(0x3aa)]=Game_Vehicle['prototype']['initMoveSpeed'],Game_Vehicle[_0x5e5f48(0x363)][_0x5e5f48(0x2d3)]=function(){const _0xb62861=_0x5e5f48;VisuMZ[_0xb62861(0x248)][_0xb62861(0x3aa)][_0xb62861(0x2a6)](this);const _0xf08092=VisuMZ[_0xb62861(0x248)][_0xb62861(0x1e5)][_0xb62861(0x1b0)];if(this[_0xb62861(0x26f)]()){if(_0xf08092[_0xb62861(0x395)])this[_0xb62861(0x37b)](_0xf08092['BoatSpeed']);}else{if(this['isShip']()){if(_0xf08092['ShipSpeed'])this[_0xb62861(0x37b)](_0xf08092[_0xb62861(0x3f7)]);}else{if(this['isAirship']()){if(_0xf08092[_0xb62861(0x2d0)])this[_0xb62861(0x37b)](_0xf08092[_0xb62861(0x2d0)]);}}}},VisuMZ[_0x5e5f48(0x248)]['Game_Event_initialize']=Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x2b9)],Game_Event['prototype'][_0x5e5f48(0x2b9)]=function(_0x3b405f,_0x5d6231){const _0x3f6c11=_0x5e5f48;VisuMZ[_0x3f6c11(0x248)][_0x3f6c11(0x176)][_0x3f6c11(0x2a6)](this,_0x3b405f,_0x5d6231),this[_0x3f6c11(0x428)](),this['setupMorphEvent'](),this[_0x3f6c11(0x46e)]();},VisuMZ['EventsMoveCore'][_0x5e5f48(0x255)]=Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x192)],Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x192)]=function(){const _0x88d4b=_0x5e5f48;if(this[_0x88d4b(0x3dd)]!==undefined){const _0x214d8a=this[_0x88d4b(0x3dd)]['mapId'],_0x3fcbe9=this[_0x88d4b(0x3dd)][_0x88d4b(0x1f7)];return VisuMZ[_0x88d4b(0x1a1)][_0x214d8a][_0x88d4b(0x3f5)][_0x3fcbe9];}if(this['_eventCopyData']!==undefined){const _0x1c5e94=this[_0x88d4b(0x4bd)]['mapId'],_0x239ae3=this[_0x88d4b(0x4bd)]['eventId'];return VisuMZ[_0x88d4b(0x1a1)][_0x1c5e94][_0x88d4b(0x3f5)][_0x239ae3];}if(this['_eventSpawnData']!==undefined){const _0x405721=this[_0x88d4b(0x4bc)][_0x88d4b(0x21e)],_0x169ffb=this[_0x88d4b(0x4bc)][_0x88d4b(0x1f7)];return VisuMZ[_0x88d4b(0x1a1)][_0x405721][_0x88d4b(0x3f5)][_0x169ffb];}if($gameTemp[_0x88d4b(0x457)]!==undefined){const _0x8e060a=$gameTemp[_0x88d4b(0x457)][_0x88d4b(0x21e)],_0x29c010=$gameTemp[_0x88d4b(0x457)]['eventId'];return VisuMZ['PreloadedMaps'][_0x8e060a][_0x88d4b(0x3f5)][_0x29c010];}return VisuMZ[_0x88d4b(0x248)]['Game_Event_event'][_0x88d4b(0x2a6)](this);},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x41c)]=function(_0x301127,_0x4307ee){const _0x399709=_0x5e5f48;if(_0x301127===0x0||_0x4307ee===0x0)return![];if(!VisuMZ[_0x399709(0x1a1)][_0x301127])return $gameTemp['isPlaytest']()&&console[_0x399709(0x42d)](_0x399709(0x20e)[_0x399709(0x2ac)](_0x301127)),![];return!![];},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x41d)]=Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x2de)],Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x2de)]=function(){const _0x3f9d13=_0x5e5f48;VisuMZ[_0x3f9d13(0x248)]['Game_Event_start'][_0x3f9d13(0x2a6)](this),Imported[_0x3f9d13(0x45e)]&&Input[_0x3f9d13(0x394)](VisuMZ[_0x3f9d13(0x232)][_0x3f9d13(0x1e5)][_0x3f9d13(0x1be)][_0x3f9d13(0x3c9)])&&Input['clear']();},Game_Event[_0x5e5f48(0x363)]['setupCopyEvent']=function(){const _0x546b60=_0x5e5f48,_0x3738d5=this[_0x546b60(0x192)]()[_0x546b60(0x270)];if(_0x3738d5==='')return;if(DataManager[_0x546b60(0x401)]()||DataManager['isEventTest']())return;const _0x8bd95b=VisuMZ[_0x546b60(0x248)]['Settings'][_0x546b60(0x125)];let _0x4619e3=null,_0x477f32=0x0,_0x735040=0x0;if(_0x3738d5[_0x546b60(0x1b1)](/<COPY EVENT:[ ]MAP[ ](\d+),[ ]EVENT[ ](\d+)>/i))_0x477f32=Number(RegExp['$1']),_0x735040=Number(RegExp['$2']);else{if(_0x3738d5[_0x546b60(0x1b1)](/<COPY EVENT:[ ](\d+),[ ](\d+)>/i))_0x477f32=Number(RegExp['$1']),_0x735040=Number(RegExp['$2']);else{if(_0x3738d5[_0x546b60(0x1b1)](/<COPY EVENT:[ ](.*?)>/i)){const _0x440c06=String(RegExp['$1'])[_0x546b60(0x3ff)]()[_0x546b60(0x372)]();_0x4619e3=VisuMZ['EventTemplates'][_0x440c06];if(!_0x4619e3)return;_0x477f32=_0x4619e3[_0x546b60(0x27d)],_0x735040=_0x4619e3['EventID'];}}}if(!this[_0x546b60(0x41c)](_0x477f32,_0x735040))return;_0x8bd95b[_0x546b60(0x374)][_0x546b60(0x2a6)](this,_0x477f32,_0x735040,this);if(_0x4619e3)_0x4619e3['PreCopyJS'][_0x546b60(0x2a6)](this,_0x477f32,_0x735040,this);this['_eventCopyData']={'mapId':_0x477f32,'eventId':_0x735040},this[_0x546b60(0x3d3)]=-0x2,this[_0x546b60(0x3c6)](),_0x8bd95b[_0x546b60(0x20f)]['call'](this,_0x477f32,_0x735040,this);if(_0x4619e3)_0x4619e3[_0x546b60(0x20f)][_0x546b60(0x2a6)](this,_0x477f32,_0x735040,this);$gameMap[_0x546b60(0x265)]();},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x12a)]=function(){const _0x1917f7=_0x5e5f48,_0x3857c9=$gameSystem[_0x1917f7(0x491)](this);if(!_0x3857c9)return;const _0x863e9d=_0x3857c9[_0x1917f7(0x17e)][_0x1917f7(0x3ff)]()[_0x1917f7(0x372)]();_0x863e9d!==_0x1917f7(0x131)?this[_0x1917f7(0x406)](_0x863e9d,!![]):this[_0x1917f7(0x446)](_0x3857c9['mapId'],_0x3857c9[_0x1917f7(0x1f7)],!![]);},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x446)]=function(_0x18ed2e,_0x4110d0,_0x41a786){const _0x13dfaf=_0x5e5f48;if(!this['checkValidEventerMap'](_0x18ed2e,_0x4110d0))return;const _0xbb4600=VisuMZ[_0x13dfaf(0x248)][_0x13dfaf(0x1e5)][_0x13dfaf(0x125)];if(!_0x41a786)_0xbb4600[_0x13dfaf(0x260)][_0x13dfaf(0x2a6)](this,_0x18ed2e,_0x4110d0,this);this[_0x13dfaf(0x3dd)]={'mapId':_0x18ed2e,'eventId':_0x4110d0},this['_pageIndex']=-0x2,this[_0x13dfaf(0x3c6)]();if(!_0x41a786)_0xbb4600[_0x13dfaf(0x4a0)]['call'](this,_0x18ed2e,_0x4110d0,this);$gameMap['clearEventCache']();},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x406)]=function(_0x13e673,_0x159b80){const _0x2e238f=_0x5e5f48;_0x13e673=_0x13e673[_0x2e238f(0x3ff)]()[_0x2e238f(0x372)]();const _0x3ac6c9=VisuMZ['EventTemplates'][_0x13e673];if(!_0x3ac6c9)return;const _0x3c07bc=_0x3ac6c9[_0x2e238f(0x27d)],_0x505eeb=_0x3ac6c9[_0x2e238f(0x17b)];if(!this[_0x2e238f(0x41c)](_0x3c07bc,_0x505eeb))return;if(!_0x159b80)_0x3ac6c9[_0x2e238f(0x260)][_0x2e238f(0x2a6)](this,_0x3c07bc,_0x505eeb,this);this[_0x2e238f(0x446)](_0x3c07bc,_0x505eeb,_0x159b80);if(!_0x159b80)_0x3ac6c9['PostMorphJS'][_0x2e238f(0x2a6)](this,_0x3c07bc,_0x505eeb,this);this[_0x2e238f(0x265)]();},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x148)]=function(){const _0x53b6c=_0x5e5f48;this['_eventMorphData']=undefined,this[_0x53b6c(0x3d3)]=-0x2,this[_0x53b6c(0x3c6)]();},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x29d)]=function(_0x1d0389){const _0x9e3a43=_0x5e5f48,_0x140316=VisuMZ[_0x9e3a43(0x248)][_0x9e3a43(0x1e5)][_0x9e3a43(0x125)],_0x4dc2fe=_0x1d0389[_0x9e3a43(0x17e)][_0x9e3a43(0x3ff)]()[_0x9e3a43(0x372)](),_0x1e10d6=!['',_0x9e3a43(0x131)][_0x9e3a43(0x2d5)](_0x4dc2fe);let _0x5ad75c=0x0,_0xc576a2=0x0;if(_0x1e10d6){const _0x1814ef=VisuMZ['EventTemplates'][_0x4dc2fe];if(!_0x1814ef)return;_0x5ad75c=_0x1814ef[_0x9e3a43(0x27d)],_0xc576a2=_0x1814ef['EventID'];}else _0x5ad75c=_0x1d0389[_0x9e3a43(0x21e)],_0xc576a2=_0x1d0389[_0x9e3a43(0x1f7)];if(!this['checkValidEventerMap'](_0x5ad75c,_0xc576a2))return;if(_0x1e10d6){const _0x5934bd=VisuMZ[_0x9e3a43(0x2f0)][_0x4dc2fe];_0x5934bd[_0x9e3a43(0x4cd)][_0x9e3a43(0x2a6)](this,_0x5ad75c,_0xc576a2,this);}_0x140316[_0x9e3a43(0x4cd)][_0x9e3a43(0x2a6)](this,_0x5ad75c,_0xc576a2,this),this['_eventSpawnData']=_0x1d0389,this[_0x9e3a43(0x3d3)]=-0x2,this[_0x9e3a43(0x1db)]=$gameMap['mapId'](),this[_0x9e3a43(0x21a)]=_0x1d0389[_0x9e3a43(0x2ed)],this[_0x9e3a43(0x33b)]=_0x1d0389[_0x9e3a43(0x3af)],this[_0x9e3a43(0x3eb)](_0x1d0389['x'],_0x1d0389['y']),this[_0x9e3a43(0x117)](_0x1d0389[_0x9e3a43(0x2dd)]),this[_0x9e3a43(0x3c6)]();if(_0x1e10d6){const _0x1ad424=VisuMZ[_0x9e3a43(0x2f0)][_0x4dc2fe];if(!_0x1ad424)return;_0x1ad424[_0x9e3a43(0x3c3)]['call'](this,_0x5ad75c,_0xc576a2,this);}_0x140316[_0x9e3a43(0x3c3)]['call'](this,_0x5ad75c,_0xc576a2,this);const _0x15ea43=SceneManager[_0x9e3a43(0x15b)];if(_0x15ea43&&_0x15ea43['_spriteset'])_0x15ea43[_0x9e3a43(0x469)][_0x9e3a43(0x222)](this);},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x21f)]=function(){return!!this['_eventSpawnData'];},VisuMZ['EventsMoveCore']['Game_Event_refresh']=Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x3c6)],Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x3c6)]=function(){const _0x19a3e1=_0x5e5f48,_0x22639c=this[_0x19a3e1(0x3d3)];VisuMZ[_0x19a3e1(0x248)][_0x19a3e1(0x315)]['call'](this),_0x22639c!==this[_0x19a3e1(0x3d3)]&&this[_0x19a3e1(0x4ee)]();},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x4d4)]=Game_Event[_0x5e5f48(0x363)]['clearPageSettings'],Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x2f6)]=function(){const _0x420ead=_0x5e5f48;VisuMZ[_0x420ead(0x248)][_0x420ead(0x4d4)][_0x420ead(0x2a6)](this),this[_0x420ead(0x1af)]();},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x162)]=Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x421)],Game_Event[_0x5e5f48(0x363)]['setupPageSettings']=function(){const _0x5bbb95=_0x5e5f48;this[_0x5bbb95(0x312)]=!![],VisuMZ['EventsMoveCore'][_0x5bbb95(0x162)][_0x5bbb95(0x2a6)](this),this[_0x5bbb95(0x4ee)](),this[_0x5bbb95(0x312)]=![];},Game_Event[_0x5e5f48(0x363)]['setupEventsMoveCoreEffects']=function(){const _0x471194=_0x5e5f48;if(!this[_0x471194(0x192)]())return;this[_0x471194(0x1af)](),this['setupEventsMoveCoreNotetags'](),this[_0x471194(0x234)](),this[_0x471194(0x35c)]();},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x431)]=function(){const _0x994562=_0x5e5f48,_0x27db51=this[_0x994562(0x192)]()[_0x994562(0x270)];if(_0x27db51==='')return;this[_0x994562(0x21b)](_0x27db51);},Game_Event['prototype'][_0x5e5f48(0x234)]=function(){const _0x1d3ca0=_0x5e5f48;if(!this[_0x1d3ca0(0x1de)]())return;const _0x43e345=this[_0x1d3ca0(0x3ee)]();let _0x5e601f='';for(const _0x516fff of _0x43e345){if([0x6c,0x198][_0x1d3ca0(0x2d5)](_0x516fff[_0x1d3ca0(0x212)])){if(_0x5e601f!=='')_0x5e601f+='\x0a';_0x5e601f+=_0x516fff[_0x1d3ca0(0x273)][0x0];}}this[_0x1d3ca0(0x21b)](_0x5e601f);},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x1af)]=function(){const _0x41c7ef=_0x5e5f48,_0x3f23d4=VisuMZ[_0x41c7ef(0x248)][_0x41c7ef(0x1e5)];this[_0x41c7ef(0x1e0)]={'type':_0x41c7ef(0x185),'distance':0x0,'regionList':[]},this[_0x41c7ef(0x48c)]=![],this[_0x41c7ef(0x16c)]=![],this['_addedHitbox']={'up':0x0,'down':0x0,'left':0x0,'right':0x0},this[_0x41c7ef(0x136)]=$gameSystem['getEventIconData'](this),this[_0x41c7ef(0x2c1)]={'text':'','visibleRange':_0x3f23d4[_0x41c7ef(0x220)][_0x41c7ef(0x35e)],'offsetX':_0x3f23d4['Label']['OffsetX'],'offsetY':_0x3f23d4['Label']['OffsetY']},this[_0x41c7ef(0x3b9)]=[],this[_0x41c7ef(0x3f3)]={'target':-0x1,'type':_0x41c7ef(0x3ea),'delay':0x1},this[_0x41c7ef(0x48e)]=_0x3f23d4[_0x41c7ef(0x1b0)][_0x41c7ef(0x241)]??0x0,this[_0x41c7ef(0x1d2)]=![],this[_0x41c7ef(0x1a2)]={'visible':!![],'filename':_0x3f23d4[_0x41c7ef(0x1b0)]['DefaultShadow']},this['clearSpriteOffsets'](),this[_0x41c7ef(0x294)]();},Game_Event[_0x5e5f48(0x363)]['checkEventsMoveCoreStringTags']=function(_0xa667c7){const _0x4ff303=_0x5e5f48;if(_0xa667c7[_0x4ff303(0x1b1)](/<ACTIVATION[ ](?:REGION|REGIONS):[ ]*(\d+(?:\s*,\s*\d+)*)>/i))this[_0x4ff303(0x1e0)][_0x4ff303(0x27c)]=JSON['parse']('['+RegExp['$1'][_0x4ff303(0x1b1)](/\d+/g)+']'),this[_0x4ff303(0x1e0)]['type']='region';else _0xa667c7[_0x4ff303(0x1b1)](/<ACTIVATION[ ](.*?):[ ](\d+)>/i)&&(type=String(RegExp['$1'])['toLowerCase']()[_0x4ff303(0x372)](),this['_activationProximity'][_0x4ff303(0x172)]=type,this[_0x4ff303(0x1e0)]['distance']=Number(RegExp['$2']));_0xa667c7[_0x4ff303(0x1b1)](/<ALWAYS UPDATE MOVEMENT>/i)&&(this[_0x4ff303(0x48c)]=!![]);_0xa667c7[_0x4ff303(0x1b1)](/<CLICK TRIGGER>/i)&&(this[_0x4ff303(0x16c)]=!![]);const _0x2f2e09=_0xa667c7['match'](/<HITBOX[ ](.*?):[ ](\d+)>/gi);if(_0x2f2e09)for(const _0x3a7d1a of _0x2f2e09){if(_0x3a7d1a[_0x4ff303(0x1b1)](/<HITBOX[ ](.*?):[ ](\d+)>/i)){const _0x37c82e=String(RegExp['$1'])[_0x4ff303(0x4c9)]()[_0x4ff303(0x372)](),_0xf86a7=Number(RegExp['$2']);this[_0x4ff303(0x2cb)][_0x37c82e]=_0xf86a7;}}_0xa667c7[_0x4ff303(0x1b1)](/<ICON:[ ](\d+)>/i)&&(this[_0x4ff303(0x136)][_0x4ff303(0x1e4)]=Number(RegExp['$1']));_0xa667c7[_0x4ff303(0x1b1)](/<ICON (?:BUFFER|OFFSET) X:[ ]([\+\-]\d+)>/i)&&(this['_eventIcon']['bufferX']=Number(RegExp['$1']));_0xa667c7[_0x4ff303(0x1b1)](/<ICON (?:BUFFER|OFFSET) Y:[ ]([\+\-]\d+)>/i)&&(this[_0x4ff303(0x136)][_0x4ff303(0x196)]=Number(RegExp['$1']));_0xa667c7['match'](/<ICON (?:BUFFER|OFFSET):[ ]([\+\-]\d+),[ ]([\+\-]\d+)>/i)&&(this[_0x4ff303(0x136)][_0x4ff303(0x429)]=Number(RegExp['$1']),this['_eventIcon'][_0x4ff303(0x196)]=Number(RegExp['$2']));if(_0xa667c7[_0x4ff303(0x1b1)](/<ICON BLEND MODE:[ ](.*?)>/i)){const _0x5c514e=String(RegExp['$1'])[_0x4ff303(0x3ff)]()[_0x4ff303(0x372)](),_0x1cb553=['NORMAL',_0x4ff303(0x170),_0x4ff303(0x487),'SCREEN'];this[_0x4ff303(0x136)]['blendMode']=_0x1cb553['indexOf'](_0x5c514e)[_0x4ff303(0x266)](0x0,0x3);}_0xa667c7[_0x4ff303(0x1b1)](/<LABEL:[ ](.*?)>/i)&&(this['_labelWindow'][_0x4ff303(0x289)]=String(RegExp['$1'])['trim']());_0xa667c7[_0x4ff303(0x1b1)](/<LABEL>\s*([\s\S]*)\s*<\/LABEL>/i)&&(this[_0x4ff303(0x2c1)][_0x4ff303(0x289)]=String(RegExp['$1'])[_0x4ff303(0x372)]());_0xa667c7[_0x4ff303(0x1b1)](/<LABEL (?:BUFFER|OFFSET) X:[ ]([\+\-]\d+)>/i)&&(this['_labelWindow']['offsetX']=Number(RegExp['$1']));_0xa667c7[_0x4ff303(0x1b1)](/<LABEL (?:BUFFER|OFFSET) Y:[ ]([\+\-]\d+)>/i)&&(this[_0x4ff303(0x2c1)][_0x4ff303(0x468)]=Number(RegExp['$1']));_0xa667c7['match'](/<LABEL (?:BUFFER|OFFSET):[ ]([\+\-]\d+),[ ]([\+\-]\d+)>/i)&&(this[_0x4ff303(0x2c1)][_0x4ff303(0x2b6)]=Number(RegExp['$1']),this['_labelWindow'][_0x4ff303(0x468)]=Number(RegExp['$2']));$gameTemp[_0x4ff303(0x152)](this);for(;;){if(this['_labelWindow']['text'][_0x4ff303(0x1b1)](/\\V\[(\d+)\]/gi))this[_0x4ff303(0x2c1)][_0x4ff303(0x289)]=this[_0x4ff303(0x2c1)]['text'][_0x4ff303(0x281)](/\\V\[(\d+)\]/gi,(_0x1fc204,_0x14b0e4)=>$gameVariables[_0x4ff303(0x4c4)](parseInt(_0x14b0e4)));else break;}$gameTemp['clearSelfTarget']();_0xa667c7[_0x4ff303(0x1b1)](/<LABEL RANGE:[ ](\d+)>/i)&&(this[_0x4ff303(0x2c1)][_0x4ff303(0x236)]=Number(RegExp['$1']));if(_0xa667c7[_0x4ff303(0x1b1)](/<MOVE ONLY (?:REGION|REGIONS):[ ](\d+(?:\s*,\s*\d+)*)>/i)){const _0x4707a1=JSON[_0x4ff303(0x3b3)]('['+RegExp['$1'][_0x4ff303(0x1b1)](/\d+/g)+']');this['_moveOnlyRegions']=this[_0x4ff303(0x3b9)]['concat'](_0x4707a1),this[_0x4ff303(0x3b9)][_0x4ff303(0x488)](0x0);}if(_0xa667c7[_0x4ff303(0x1b1)](/<MOVE SYNCH TARGET:[ ](.*?)>/i)){const _0x1ac188=String(RegExp['$1']);if(_0x1ac188['match'](/PLAYER/i))this[_0x4ff303(0x3f3)][_0x4ff303(0x40e)]=0x0;else _0x1ac188[_0x4ff303(0x1b1)](/EVENT[ ](\d+)/i)&&(this[_0x4ff303(0x3f3)][_0x4ff303(0x40e)]=Number(RegExp['$1']));}_0xa667c7['match'](/<MOVE SYNCH TYPE:[ ](.*?)>/i)&&(this[_0x4ff303(0x3f3)][_0x4ff303(0x172)]=String(RegExp['$1'])[_0x4ff303(0x4c9)]()[_0x4ff303(0x372)]());_0xa667c7[_0x4ff303(0x1b1)](/<MOVE SYNCH DELAY:[ ](\d+)>/i)&&(this['_moveSynch'][_0x4ff303(0x228)]=Number(RegExp['$1']));if(_0xa667c7[_0x4ff303(0x1b1)](/<TRUE RANDOM MOVE>/i))this[_0x4ff303(0x48e)]=0x0;else _0xa667c7[_0x4ff303(0x1b1)](/<RANDOM MOVE WEIGHT:[ ](.*?)>/i)&&(this[_0x4ff303(0x48e)]=Number(RegExp['$1'])||0x0);_0xa667c7[_0x4ff303(0x1b1)](/<SAVE EVENT (?:LOCATION|LOCATIONS)>/i)&&(this['_saveEventLocation']=!![]),_0xa667c7[_0x4ff303(0x1b1)](/<HIDE SHADOW>/i)&&(this[_0x4ff303(0x1a2)]['visible']=![]),_0xa667c7[_0x4ff303(0x1b1)](/<SHADOW FILENAME:[ ](.*?)>/i)&&(this['_shadowGraphic']['filename']=String(RegExp['$1'])),_0xa667c7[_0x4ff303(0x1b1)](/<SPRITE OFFSET X:[ ]([\+\-]\d+)>/i)&&(this['_spriteOffsetX']=Number(RegExp['$1'])),_0xa667c7[_0x4ff303(0x1b1)](/<SPRITE OFFSET Y:[ ]([\+\-]\d+)>/i)&&(this[_0x4ff303(0x3a5)]=Number(RegExp['$1'])),_0xa667c7[_0x4ff303(0x1b1)](/<SPRITE OFFSET:[ ]([\+\-]\d+),[ ]([\+\-]\d+)>/i)&&(this[_0x4ff303(0x1dc)]=Number(RegExp['$1']),this[_0x4ff303(0x3a5)]=Number(RegExp['$2'])),_0xa667c7[_0x4ff303(0x1b1)](/<STEP PATTERN:[ ](.*)>/i)&&(this['_stepPattern']=String(RegExp['$1'])['toUpperCase']()[_0x4ff303(0x372)]());},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x35c)]=function(){const _0x2ee127=_0x5e5f48;this[_0x2ee127(0x481)]();},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x485)]=function(){const _0x2b57ec=_0x5e5f48;if(this[_0x2b57ec(0x48c)])return!![];return Game_Character[_0x2b57ec(0x363)][_0x2b57ec(0x485)]['call'](this);},VisuMZ['EventsMoveCore'][_0x5e5f48(0x47c)]=Game_Event['prototype'][_0x5e5f48(0x17d)],Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x17d)]=function(){const _0x4afe19=_0x5e5f48;if(this[_0x4afe19(0x32b)]())return;VisuMZ[_0x4afe19(0x248)][_0x4afe19(0x47c)][_0x4afe19(0x2a6)](this),this[_0x4afe19(0x347)]()&&VisuMZ[_0x4afe19(0x2df)](this['_eventId']);},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x32b)]=function(){const _0x39f032=_0x5e5f48,_0x11f771=VisuMZ[_0x39f032(0x248)][_0x39f032(0x1e5)]['Movement'];if($gameMap[_0x39f032(0x478)]()&&_0x11f771[_0x39f032(0x206)])return!![];if($gameMessage['isBusy']()&&_0x11f771['StopAutoMoveMessages'])return!![];if(!$gameSystem[_0x39f032(0x28a)]())return!![];if(this['moveSynchTarget']()>=0x0)return!![];return![];},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x481)]=function(){const _0x31e605=_0x5e5f48,_0x4a8be2=SceneManager[_0x31e605(0x15b)][_0x31e605(0x469)];if(_0x4a8be2){const _0x3e733b=_0x4a8be2[_0x31e605(0x29b)](this);_0x3e733b&&_0x3e733b['_shadowSprite']&&_0x3e733b['_shadowSprite'][_0x31e605(0x2cc)]!==this['shadowFilename']()&&(_0x3e733b[_0x31e605(0x38f)]['_filename']=this[_0x31e605(0x199)](),_0x3e733b[_0x31e605(0x38f)][_0x31e605(0x243)]=ImageManager[_0x31e605(0x2fe)](_0x3e733b[_0x31e605(0x38f)][_0x31e605(0x2cc)]));}},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x199)]=function(){const _0x561d9a=_0x5e5f48;return this[_0x561d9a(0x1a2)]['filename'];},Game_Event[_0x5e5f48(0x363)]['isShadowVisible']=function(){const _0x53bbad=_0x5e5f48;if(!this[_0x53bbad(0x1a2)][_0x53bbad(0x381)])return![];return Game_CharacterBase[_0x53bbad(0x363)][_0x53bbad(0x292)][_0x53bbad(0x2a6)](this);},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x3e2)]=function(){const _0x249cff=_0x5e5f48;return this['_labelWindow'][_0x249cff(0x289)];},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x4b8)]=function(){const _0x104877=_0x5e5f48;return this['_labelWindow'][_0x104877(0x236)];},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x334)]=function(_0x1d0762,_0x5d3af4,_0x4572e4){const _0x2e8d8a=_0x5e5f48;if(this[_0x2e8d8a(0x496)]())return this['isMoveOnlyRegionPassable'](_0x1d0762,_0x5d3af4,_0x4572e4);if($gameMap[_0x2e8d8a(0x204)](_0x1d0762,_0x5d3af4,_0x4572e4,_0x2e8d8a(0x192)))return!![];if($gameMap['isRegionForbidPass'](_0x1d0762,_0x5d3af4,_0x4572e4,_0x2e8d8a(0x192)))return![];return Game_Character[_0x2e8d8a(0x363)][_0x2e8d8a(0x334)][_0x2e8d8a(0x2a6)](this,_0x1d0762,_0x5d3af4,_0x4572e4);},Game_Event['prototype']['hasMoveOnlyRegions']=function(){const _0x13c665=_0x5e5f48;if(this[_0x13c665(0x3b9)]===undefined)this[_0x13c665(0x1af)]();return this[_0x13c665(0x3b9)][_0x13c665(0x2ef)]>0x0;},Game_Event['prototype'][_0x5e5f48(0x3dc)]=function(_0x1871ea,_0x16879c,_0x21a431){const _0x5df87e=_0x5e5f48,_0x887999=$gameMap[_0x5df87e(0x41b)](_0x1871ea,_0x21a431),_0x4b4a88=$gameMap[_0x5df87e(0x49c)](_0x16879c,_0x21a431),_0x4fdaf3=$gameMap['regionId'](_0x887999,_0x4b4a88);return this['_moveOnlyRegions'][_0x5df87e(0x2d5)](_0x4fdaf3);},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x221)]=Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x1a4)],Game_Event[_0x5e5f48(0x363)]['findProperPageIndex']=function(){const _0x34d9d0=_0x5e5f48;return this[_0x34d9d0(0x1ce)]=![],this[_0x34d9d0(0x2e0)]=![],this[_0x34d9d0(0x192)]()?VisuMZ[_0x34d9d0(0x248)][_0x34d9d0(0x221)]['call'](this):-0x1;},VisuMZ[_0x5e5f48(0x248)]['Game_Event_meetsConditions']=Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x1eb)],Game_Event['prototype'][_0x5e5f48(0x1eb)]=function(_0xdec29f){const _0x530c32=_0x5e5f48;this[_0x530c32(0x3ac)](_0xdec29f),$gameTemp[_0x530c32(0x152)](this);const _0x339258=VisuMZ[_0x530c32(0x248)][_0x530c32(0x1c2)][_0x530c32(0x2a6)](this,_0xdec29f);return $gameTemp[_0x530c32(0x29e)](),_0x339258;},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x351)]=function(){return this['_advancedSwitchVariable'];},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x3ac)]=function(_0x28ee59){const _0x3a9e1e=_0x5e5f48,_0x25b124=_0x28ee59[_0x3a9e1e(0x38a)];if(_0x25b124[_0x3a9e1e(0x197)]&&DataManager[_0x3a9e1e(0x4eb)](_0x25b124[_0x3a9e1e(0x33c)]))this[_0x3a9e1e(0x1ce)]=!![];else{if(_0x25b124['switch2Valid']&&DataManager[_0x3a9e1e(0x4eb)](_0x25b124[_0x3a9e1e(0x1ed)]))this['_advancedSwitchVariable']=!![];else _0x25b124[_0x3a9e1e(0x43f)]&&DataManager[_0x3a9e1e(0x4ab)](_0x25b124['variableId'])&&(this[_0x3a9e1e(0x1ce)]=!![]);}},Game_Event['prototype'][_0x5e5f48(0x258)]=function(){const _0xbc4932=_0x5e5f48;if(this[_0xbc4932(0x11b)])return![];return this[_0xbc4932(0x16c)];},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x285)]=function(){const _0x4902f3=_0x5e5f48;$gameTemp['clearDestination'](),this[_0x4902f3(0x2de)]();},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x48f)]=function(_0x316684,_0x395197){const _0x28cf34=_0x5e5f48;return this['_addedHitbox']?this[_0x28cf34(0x1ba)](_0x316684,_0x395197):Game_Character[_0x28cf34(0x363)][_0x28cf34(0x48f)][_0x28cf34(0x2a6)](this,_0x316684,_0x395197);},Game_Event[_0x5e5f48(0x363)]['posEventsMoveCore']=function(_0x5e915a,_0x5f04dd){const _0x39c341=_0x5e5f48;var _0x5918a2=this['x']-this[_0x39c341(0x2cb)][_0x39c341(0x3ba)],_0xb31c8a=this['x']+this[_0x39c341(0x2cb)][_0x39c341(0x150)],_0x3dd09f=this['y']-this[_0x39c341(0x2cb)]['up'],_0x3b4b07=this['y']+this[_0x39c341(0x2cb)][_0x39c341(0x1bb)];return _0x5918a2<=_0x5e915a&&_0x5e915a<=_0xb31c8a&&_0x3dd09f<=_0x5f04dd&&_0x5f04dd<=_0x3b4b07;},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x27b)]=function(_0x5c9f70,_0x1340d1,_0xaba49f){const _0xaa2e26=_0x5e5f48;for(let _0x1df944=-this['_addedHitbox'][_0xaa2e26(0x3ba)];_0x1df944<=this[_0xaa2e26(0x2cb)]['right'];_0x1df944++){for(let _0xca9d40=-this[_0xaa2e26(0x2cb)]['up'];_0xca9d40<=this['_addedHitbox'][_0xaa2e26(0x1bb)];_0xca9d40++){if(!Game_Character[_0xaa2e26(0x363)]['canPass']['call'](this,_0x5c9f70+_0x1df944,_0x1340d1+_0xca9d40,_0xaba49f))return![];}}return!![];},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x1fb)]=function(_0x3550ec,_0xf8d690){const _0xba1d69=_0x5e5f48;if(Imported[_0xba1d69(0x2c7)]&&this[_0xba1d69(0x18d)]())return this[_0xba1d69(0x2e7)](_0x3550ec,_0xf8d690);else{const _0x1fc504=$gameMap[_0xba1d69(0x1bf)](_0x3550ec,_0xf8d690)[_0xba1d69(0x420)](_0x59f188=>_0x59f188!==this);return _0x1fc504[_0xba1d69(0x2ef)]>0x0;}},Game_Event['prototype'][_0x5e5f48(0x2e7)]=function(_0x2bbb23,_0xc2ffb6){const _0x147918=_0x5e5f48;if(!this[_0x147918(0x283)]())return![];else{const _0x466863=$gameMap[_0x147918(0x1bf)](_0x2bbb23,_0xc2ffb6)[_0x147918(0x420)](_0x58c939=>_0x58c939!==this&&_0x58c939[_0x147918(0x283)]());return _0x466863[_0x147918(0x2ef)]>0x0;}},Game_Event['prototype'][_0x5e5f48(0x2ec)]=function(){const _0x2214fa=_0x5e5f48;return this[_0x2214fa(0x1e0)][_0x2214fa(0x172)]||_0x2214fa(0x185);},Game_Event['prototype'][_0x5e5f48(0x14d)]=function(){const _0x2053b4=_0x5e5f48;return this['_activationProximity'][_0x2053b4(0x35f)]||0x0;},Game_Event['prototype'][_0x5e5f48(0x18a)]=function(){const _0x460e5a=_0x5e5f48;return this[_0x460e5a(0x1e0)][_0x460e5a(0x27c)]||[];},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x359)]=function(){const _0x50133e=_0x5e5f48;Game_Character[_0x50133e(0x363)][_0x50133e(0x359)]['call'](this);if([_0x50133e(0x185),_0x50133e(0x400)][_0x50133e(0x2d5)](this[_0x50133e(0x2ec)]()))return;$gamePlayer[_0x50133e(0x163)]([0x2]);},VisuMZ['EventsMoveCore'][_0x5e5f48(0x3de)]=Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x114)],Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x114)]=function(){const _0x546e6f=_0x5e5f48;if(this['_trigger']!==0x3)return;if(this[_0x546e6f(0x312)])return;if(!this[_0x546e6f(0x143)](![]))return;if(!this[_0x546e6f(0x3be)](![]))return;VisuMZ[_0x546e6f(0x248)][_0x546e6f(0x3de)]['call'](this);},VisuMZ[_0x5e5f48(0x248)]['Game_Event_updateParallel']=Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x326)],Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x326)]=function(){const _0x1799d5=_0x5e5f48;if(!this[_0x1799d5(0x144)])return;if(!this[_0x1799d5(0x143)](!![]))return;if(!this[_0x1799d5(0x3be)](!![]))return;VisuMZ['EventsMoveCore'][_0x1799d5(0x4cb)]['call'](this);},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x143)]=function(_0x2f1fc6){const _0x5e918c=_0x5e5f48;if(!_0x2f1fc6&&$gameMap[_0x5e918c(0x478)]())return![];if(!_0x2f1fc6&&$gameMap['isAnyEventStarting']())return![];if(this[_0x5e918c(0x18a)]()<=0x0)return!![];return $gamePlayer[_0x5e918c(0x296)](this);},Game_Event['prototype'][_0x5e5f48(0x3be)]=function(_0x5ed7b5){const _0x22dbd3=_0x5e5f48;if(!_0x5ed7b5&&$gameMap[_0x22dbd3(0x478)]())return![];if(!_0x5ed7b5&&$gameMap[_0x22dbd3(0x231)]())return![];if([_0x22dbd3(0x185),_0x22dbd3(0x400)][_0x22dbd3(0x2d5)](this[_0x22dbd3(0x2ec)]()))return!![];return $gamePlayer['meetActivationProximityConditions'](this);},VisuMZ[_0x5e5f48(0x2df)]=function(_0x398efa){const _0x3acff2=_0x5e5f48;for(const _0x4a080d of $gameMap[_0x3acff2(0x3f5)]()){if(!_0x4a080d)continue;_0x4a080d['moveSynchTarget']()===_0x398efa&&_0x4a080d[_0x3acff2(0x1bd)]();}},VisuMZ[_0x5e5f48(0x426)]=function(_0x401524){const _0x451336=_0x5e5f48;if(_0x401524===0x0)return $gamePlayer;return $gameMap[_0x451336(0x192)](_0x401524);},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x1da)]=function(){const _0x33d970=_0x5e5f48;return this[_0x33d970(0x3f3)][_0x33d970(0x40e)];},Game_Event['prototype'][_0x5e5f48(0x2e6)]=function(){const _0x4c0440=_0x5e5f48;return this[_0x4c0440(0x3f3)][_0x4c0440(0x172)];},Game_Event['prototype'][_0x5e5f48(0x286)]=function(){const _0x52f975=_0x5e5f48;if(this[_0x52f975(0x1da)]()>=0x0){const _0x59ded8=VisuMZ[_0x52f975(0x426)](this[_0x52f975(0x1da)]());if(_0x59ded8)return _0x59ded8['realMoveSpeed']();}return Game_Character[_0x52f975(0x363)][_0x52f975(0x286)]['call'](this);},Game_Event[_0x5e5f48(0x363)]['updateMoveSynch']=function(){const _0x5654bc=_0x5e5f48;this[_0x5654bc(0x3f3)][_0x5654bc(0x1e1)]=this[_0x5654bc(0x3f3)][_0x5654bc(0x1e1)]||0x0,this['_moveSynch'][_0x5654bc(0x1e1)]--;if(this[_0x5654bc(0x3f3)][_0x5654bc(0x1e1)]>0x0)return;this[_0x5654bc(0x3f3)]['timer']=this[_0x5654bc(0x3f3)][_0x5654bc(0x228)],this[_0x5654bc(0x44a)]();},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x44a)]=function(){const _0x55e6d2=_0x5e5f48;switch(this[_0x55e6d2(0x2e6)]()){case _0x55e6d2(0x3ea):this[_0x55e6d2(0x259)]();break;case'approach':this[_0x55e6d2(0x3e6)]();break;case _0x55e6d2(0x3b5):this[_0x55e6d2(0x224)]();break;case _0x55e6d2(0x1b8):this[_0x55e6d2(0x190)]();break;case _0x55e6d2(0x361):case _0x55e6d2(0x2bc):this['processMoveSynchMimic']();break;case _0x55e6d2(0x3db):case _0x55e6d2(0x4be):this[_0x55e6d2(0x49f)]();break;case'mirror\x20horizontal':case _0x55e6d2(0x329):case _0x55e6d2(0x353):case'horz\x20mirror':this['processMoveSynchMirrorHorz']();break;case _0x55e6d2(0x135):case _0x55e6d2(0x305):case _0x55e6d2(0x122):case _0x55e6d2(0x17f):this[_0x55e6d2(0x183)]();break;default:this[_0x55e6d2(0x259)]();break;}this['update']();},Game_Event[_0x5e5f48(0x363)]['processMoveSynchRandom']=function(){const _0x2135fd=_0x5e5f48,_0x17a723=[0x2,0x4,0x6,0x8];$gameMap['isSupportDiagonalMovement']()&&_0x17a723['push'](0x1,0x3,0x7,0x9);const _0x37164d=[];for(const _0x20e9b5 of _0x17a723){if(this[_0x2135fd(0x27b)](this['x'],this['y'],_0x20e9b5))_0x37164d[_0x2135fd(0x4bf)](_0x20e9b5);}if(_0x37164d[_0x2135fd(0x2ef)]>0x0){const _0x3eb9cd=_0x37164d[Math[_0x2135fd(0x33a)](_0x37164d[_0x2135fd(0x2ef)])];this[_0x2135fd(0x337)](_0x3eb9cd);}},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x3e6)]=function(){const _0xec4d4f=_0x5e5f48,_0x448f44=VisuMZ[_0xec4d4f(0x426)](this[_0xec4d4f(0x1da)]());this[_0xec4d4f(0x1cf)](_0x448f44);},Game_Event['prototype']['processMoveSynchAway']=function(){const _0x15d1df=_0x5e5f48,_0x380dd7=VisuMZ[_0x15d1df(0x426)](this[_0x15d1df(0x1da)]());this[_0x15d1df(0x471)](_0x380dd7);},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x190)]=function(){this['updateRoutineMove']();},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x124)]=function(){const _0x126116=_0x5e5f48,_0x3e9d78=VisuMZ[_0x126116(0x426)](this[_0x126116(0x1da)]());this[_0x126116(0x337)](_0x3e9d78['lastMovedDirection']());},Game_Event[_0x5e5f48(0x363)]['processMoveSynchReverseMimic']=function(){const _0xa23079=_0x5e5f48,_0x394de9=VisuMZ['GetMoveSynchTarget'](this[_0xa23079(0x1da)]()),_0x529d37=this[_0xa23079(0x447)](_0x394de9[_0xa23079(0x3e7)]());this[_0xa23079(0x337)](this[_0xa23079(0x447)](_0x394de9[_0xa23079(0x2dd)]()));},Game_Event['prototype']['processMoveSynchMirrorHorz']=function(){const _0x152f9a=_0x5e5f48,_0x4c198e=VisuMZ[_0x152f9a(0x426)](this[_0x152f9a(0x1da)]()),_0x345499=[0x0,0x7,0x8,0x9,0x4,0x0,0x6,0x1,0x2,0x3][_0x4c198e[_0x152f9a(0x3e7)]()];this['executeMoveDir8'](_0x345499);},Game_Event['prototype'][_0x5e5f48(0x183)]=function(){const _0x4f3cd9=_0x5e5f48,_0xa0c80=VisuMZ[_0x4f3cd9(0x426)](this[_0x4f3cd9(0x1da)]()),_0xa4e750=[0x0,0x3,0x2,0x1,0x6,0x0,0x4,0x9,0x8,0x7][_0xa0c80[_0x4f3cd9(0x3e7)]()];this[_0x4f3cd9(0x337)](_0xa4e750);},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x46e)]=function(){const _0x46cce5=_0x5e5f48,_0x3719a6=$gameSystem['getSavedEventLocation'](this);if(!_0x3719a6)return;this['locate'](_0x3719a6['x'],_0x3719a6['y']),this[_0x46cce5(0x117)](_0x3719a6[_0x46cce5(0x2dd)]),this[_0x46cce5(0x3d3)]===_0x3719a6['pageIndex']&&(this[_0x46cce5(0x12c)]=_0x3719a6[_0x46cce5(0x16b)]);},Game_Event['prototype']['updateMove']=function(){const _0x18bd73=_0x5e5f48;Game_Character[_0x18bd73(0x363)]['updateMove']['call'](this),this[_0x18bd73(0x49d)]();},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x116)]=function(){const _0x129468=_0x5e5f48;if($gameMap[_0x129468(0x390)]())return!![];return this[_0x129468(0x1d2)];},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x49d)]=function(){const _0x1908a8=_0x5e5f48;if(!this[_0x1908a8(0x116)]())return;this['saveEventLocation']();},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x318)]=function(){const _0x1bc469=_0x5e5f48;$gameSystem[_0x1bc469(0x318)](this);},Game_Event['prototype'][_0x5e5f48(0x409)]=function(){const _0x2c1e69=_0x5e5f48;$gameSystem[_0x2c1e69(0x31a)](this);},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x23f)]=function(){const _0x349318=_0x5e5f48;return $gameSystem[_0x349318(0x23f)](this)?Game_Character['prototype'][_0x349318(0x23f)][_0x349318(0x2a6)](this):{'iconIndex':0x0,'bufferX':settings[_0x349318(0x2db)][_0x349318(0x386)],'bufferY':settings[_0x349318(0x2db)]['BufferY'],'blendMode':settings['Icon'][_0x349318(0x133)]};},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x379)]=function(){const _0x4a12da=_0x5e5f48;return this[_0x4a12da(0x2e0)];},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x49b)]=Game_Event['prototype'][_0x5e5f48(0x1eb)],Game_Event['prototype'][_0x5e5f48(0x1eb)]=function(_0x54b5d0){const _0x3af10e=_0x5e5f48,_0x1f12c5=VisuMZ[_0x3af10e(0x248)][_0x3af10e(0x49b)]['call'](this,_0x54b5d0);if(!_0x1f12c5)return![];return this[_0x3af10e(0x20c)](_0x54b5d0);},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x20c)]=function(_0xb64ae9){const _0xce3f0c=_0x5e5f48;VisuMZ['EventsMoveCore'][_0xce3f0c(0x1d0)][_0xce3f0c(0x3fd)](_0xb64ae9),this[_0xce3f0c(0x2e0)]=_0xb64ae9[_0xce3f0c(0x280)][_0xce3f0c(0x2ef)]>0x0;_0xb64ae9[_0xce3f0c(0x280)]===undefined&&VisuMZ[_0xce3f0c(0x248)]['CustomPageConditions'][_0xce3f0c(0x3fd)](_0xb64ae9);if(_0xb64ae9[_0xce3f0c(0x280)][_0xce3f0c(0x2ef)]>0x0)return $gameMap[_0xce3f0c(0x192)](this[_0xce3f0c(0x21a)])&&VisuMZ[_0xce3f0c(0x248)][_0xce3f0c(0x1d0)][_0xce3f0c(0x4e4)](_0xb64ae9[_0xce3f0c(0x280)],this[_0xce3f0c(0x21a)]);return!![];},VisuMZ['EventsMoveCore'][_0x5e5f48(0x171)]=Game_Troop[_0x5e5f48(0x363)][_0x5e5f48(0x1eb)],Game_Troop[_0x5e5f48(0x363)][_0x5e5f48(0x1eb)]=function(_0x58b0c9){const _0x39c1e0=_0x5e5f48;var _0x1409e6=VisuMZ[_0x39c1e0(0x248)][_0x39c1e0(0x171)]['call'](this,_0x58b0c9);return _0x1409e6&&this[_0x39c1e0(0x467)](_0x58b0c9);},Game_Troop[_0x5e5f48(0x363)][_0x5e5f48(0x467)]=function(_0x29083f){const _0x1c1ff7=_0x5e5f48;_0x29083f[_0x1c1ff7(0x280)]===undefined&&VisuMZ[_0x1c1ff7(0x248)][_0x1c1ff7(0x1d0)][_0x1c1ff7(0x3fd)](_0x29083f);if(_0x29083f[_0x1c1ff7(0x280)][_0x1c1ff7(0x2ef)]>0x0)return VisuMZ[_0x1c1ff7(0x248)][_0x1c1ff7(0x1d0)][_0x1c1ff7(0x4e4)](_0x29083f['CPC'],0x0);return!![];},VisuMZ['EventsMoveCore'][_0x5e5f48(0x3b4)]=Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x3eb)],Game_Event['prototype']['locate']=function(_0x26da34,_0x5e4eeb){const _0x15281b=_0x5e5f48;VisuMZ[_0x15281b(0x248)]['Game_Event_locate'][_0x15281b(0x2a6)](this,_0x26da34,_0x5e4eeb),this[_0x15281b(0x310)]=_0x26da34,this['_randomHomeY']=_0x5e4eeb;},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x129)]=Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x1c1)],Game_Event[_0x5e5f48(0x363)]['moveTypeRandom']=function(){const _0xc78682=_0x5e5f48,_0x1774d2=$gameMap[_0xc78682(0x35f)](this['x'],this['y'],this[_0xc78682(0x310)],this[_0xc78682(0x3d6)]),_0x748a26=_0x1774d2*(this[_0xc78682(0x48e)]||0x0);Math['random']()>=_0x748a26?VisuMZ[_0xc78682(0x248)][_0xc78682(0x129)][_0xc78682(0x2a6)](this):this[_0xc78682(0x419)]();},Game_Event[_0x5e5f48(0x363)][_0x5e5f48(0x419)]=function(){const _0x232b68=_0x5e5f48,_0x46b211=this[_0x232b68(0x272)](this[_0x232b68(0x310)]),_0x276aeb=this['deltaYFrom'](this[_0x232b68(0x3d6)]);if(Math[_0x232b68(0x392)](_0x46b211)>Math[_0x232b68(0x392)](_0x276aeb))this[_0x232b68(0x187)](_0x46b211>0x0?0x4:0x6),!this[_0x232b68(0x154)]()&&_0x276aeb!==0x0&&this['moveStraight'](_0x276aeb>0x0?0x8:0x2);else _0x276aeb!==0x0&&(this['moveStraight'](_0x276aeb>0x0?0x8:0x2),!this[_0x232b68(0x154)]()&&_0x46b211!==0x0&&this[_0x232b68(0x187)](_0x46b211>0x0?0x4:0x6));},VisuMZ[_0x5e5f48(0x248)]['Game_Interpreter_updateWaitMode']=Game_Interpreter[_0x5e5f48(0x363)][_0x5e5f48(0x413)],Game_Interpreter['prototype'][_0x5e5f48(0x413)]=function(){const _0x1b1afc=_0x5e5f48;if(this[_0x1b1afc(0x38c)]===_0x1b1afc(0x408)){if(window[this[_0x1b1afc(0x32e)]])this[_0x1b1afc(0x38c)]='',this[_0x1b1afc(0x369)]();else return!![];}else return VisuMZ[_0x1b1afc(0x248)][_0x1b1afc(0x164)][_0x1b1afc(0x2a6)](this);},VisuMZ['EventsMoveCore'][_0x5e5f48(0x43c)]=Game_Interpreter[_0x5e5f48(0x363)][_0x5e5f48(0x1c8)],Game_Interpreter[_0x5e5f48(0x363)]['executeCommand']=function(){const _0x45116f=_0x5e5f48,_0x30899c=$gameMap&&this[_0x45116f(0x21a)]?$gameMap[_0x45116f(0x192)](this[_0x45116f(0x21a)]):null;$gameTemp[_0x45116f(0x152)](_0x30899c);const _0x290dd3=VisuMZ[_0x45116f(0x248)][_0x45116f(0x43c)][_0x45116f(0x2a6)](this);return $gameTemp[_0x45116f(0x29e)](),_0x290dd3;},VisuMZ[_0x5e5f48(0x248)]['Game_Interpreter_PluginCommand']=Game_Interpreter[_0x5e5f48(0x363)][_0x5e5f48(0x4c8)],Game_Interpreter[_0x5e5f48(0x363)][_0x5e5f48(0x4c8)]=function(_0x164aed){const _0x3ea326=_0x5e5f48;return $gameTemp[_0x3ea326(0x37c)](this),VisuMZ[_0x3ea326(0x248)][_0x3ea326(0x237)]['call'](this,_0x164aed);},Game_Interpreter[_0x5e5f48(0x363)]['pluginCommandCallEvent']=function(_0x9511f6){const _0x3c35fe=_0x5e5f48;this['_callEventData']=_0x9511f6;const _0x10b7e4='Map%1.json'[_0x3c35fe(0x2ac)](_0x9511f6[_0x3c35fe(0x21e)][_0x3c35fe(0x317)](0x3));this['_callEventMap']='$callEventMap'+Graphics[_0x3c35fe(0x366)]+'_'+this['eventId'](),DataManager[_0x3c35fe(0x1a3)](this['_callEventMap'],_0x10b7e4),window[this[_0x3c35fe(0x32e)]]?this[_0x3c35fe(0x369)]():this[_0x3c35fe(0x26b)](_0x3c35fe(0x408));},Game_Interpreter[_0x5e5f48(0x363)]['startCallEvent']=function(){const _0x83feaf=_0x5e5f48,_0x45faec=this[_0x83feaf(0x36e)],_0x4a3c48=window[this[_0x83feaf(0x32e)]],_0x24affb=_0x4a3c48[_0x83feaf(0x3f5)][_0x45faec[_0x83feaf(0x1f7)]];if(_0x24affb&&_0x24affb[_0x83feaf(0x365)][_0x45faec[_0x83feaf(0x11c)]-0x1]){const _0x19540d=_0x24affb['pages'][_0x45faec['pageId']-0x1][_0x83feaf(0x3ee)];this['setupChild'](_0x19540d,this[_0x83feaf(0x1f7)]());}window[this[_0x83feaf(0x32e)]]=undefined,this['_callEventMap']=undefined,this['_callEventData']=undefined;};function Game_CPCInterpreter(){const _0x5714db=_0x5e5f48;this[_0x5714db(0x2b9)][_0x5714db(0x463)](this,arguments);};Game_CPCInterpreter[_0x5e5f48(0x363)]=Object[_0x5e5f48(0x29c)](Game_Interpreter['prototype']),Game_CPCInterpreter[_0x5e5f48(0x363)][_0x5e5f48(0x205)]=Game_CPCInterpreter,Game_CPCInterpreter[_0x5e5f48(0x363)][_0x5e5f48(0x21c)]=function(){const _0x441aa2=_0x5e5f48;Game_Interpreter[_0x441aa2(0x363)][_0x441aa2(0x21c)][_0x441aa2(0x2a6)](this),this[_0x441aa2(0x3a1)]=![];},Game_CPCInterpreter['prototype']['execute']=function(){const _0x16118a=_0x5e5f48;while(this[_0x16118a(0x252)]()){this[_0x16118a(0x1c8)]();}},Game_CPCInterpreter[_0x5e5f48(0x363)]['command108']=function(_0x229959){const _0x5112ef=_0x5e5f48;return Game_Interpreter[_0x5112ef(0x363)]['command108']['call'](this,_0x229959),this[_0x5112ef(0x2e5)][_0x5112ef(0x25f)](_0x1a978d=>_0x1a978d[_0x5112ef(0x1b1)](/<(?:CONDITION|CONDITIONS) MET>/i))&&(this['_cpc']=!![]),!![];},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x15a)]=Scene_Map[_0x5e5f48(0x363)]['startEncounterEffect'],Scene_Map['prototype']['startEncounterEffect']=function(){const _0x3a12a7=_0x5e5f48;VisuMZ[_0x3a12a7(0x248)][_0x3a12a7(0x15a)]['call'](this),this[_0x3a12a7(0x469)][_0x3a12a7(0x3e4)]();},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x423)]=Scene_Load[_0x5e5f48(0x363)]['onLoadSuccess'],Scene_Load[_0x5e5f48(0x363)]['onLoadSuccess']=function(){const _0x7af048=_0x5e5f48;if($gameMap)$gameMap[_0x7af048(0x265)]();VisuMZ[_0x7af048(0x248)][_0x7af048(0x423)]['call'](this);},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x1fd)]=Sprite_Character[_0x5e5f48(0x363)][_0x5e5f48(0x3d5)],Sprite_Character[_0x5e5f48(0x363)]['initMembers']=function(){const _0x39608e=_0x5e5f48;VisuMZ['EventsMoveCore'][_0x39608e(0x1fd)]['call'](this),this[_0x39608e(0x3d1)](),this[_0x39608e(0x2a8)]();},Sprite_Character[_0x5e5f48(0x363)][_0x5e5f48(0x3d1)]=function(){this['_shadowOpacity']=0xff;},Sprite_Character[_0x5e5f48(0x363)][_0x5e5f48(0x2a8)]=function(){const _0x35f63f=_0x5e5f48;this[_0x35f63f(0x202)]=new Sprite(),this[_0x35f63f(0x202)][_0x35f63f(0x243)]=ImageManager['loadSystem']('IconSet'),this[_0x35f63f(0x202)][_0x35f63f(0x243)][_0x35f63f(0x31f)]=![],this[_0x35f63f(0x202)]['setFrame'](0x0,0x0,0x0,0x0),this[_0x35f63f(0x202)]['anchor']['x']=0.5,this[_0x35f63f(0x202)][_0x35f63f(0x118)]['y']=0x1,this['addChild'](this[_0x35f63f(0x202)]);},Sprite_Character[_0x5e5f48(0x363)][_0x5e5f48(0x1cd)]=function(){const _0x3cead5=_0x5e5f48;return this[_0x3cead5(0x13d)]&&this[_0x3cead5(0x13d)][_0x3cead5(0x1b1)](/\[VS8\]/i);},Sprite_Character[_0x5e5f48(0x363)][_0x5e5f48(0x46c)]=function(){const _0x5a5d74=_0x5e5f48;return this[_0x5a5d74(0x1cd)]()&&VisuMZ['EventsMoveCore'][_0x5a5d74(0x1e5)][_0x5a5d74(0x30e)]['AutoBuffer'];},VisuMZ[_0x5e5f48(0x248)]['Sprite_Character_update']=Sprite_Character[_0x5e5f48(0x363)][_0x5e5f48(0x169)],Sprite_Character[_0x5e5f48(0x363)][_0x5e5f48(0x169)]=function(){const _0x462b94=_0x5e5f48;VisuMZ['EventsMoveCore'][_0x462b94(0x42a)][_0x462b94(0x2a6)](this),VisuMZ[_0x462b94(0x248)][_0x462b94(0x1e5)][_0x462b94(0x1b0)][_0x462b94(0x4a1)]&&this['updateTilt'](),this[_0x462b94(0x38f)]&&this['updateShadow'](),this[_0x462b94(0x202)]&&this[_0x462b94(0x24b)]();},VisuMZ[_0x5e5f48(0x248)]['Sprite_Character_setTileBitmap']=Sprite_Character[_0x5e5f48(0x363)]['setTileBitmap'],Sprite_Character[_0x5e5f48(0x363)]['setTileBitmap']=function(){const _0x196f1a=_0x5e5f48;VisuMZ[_0x196f1a(0x248)][_0x196f1a(0x300)][_0x196f1a(0x2a6)](this),this[_0x196f1a(0x243)][_0x196f1a(0x1fe)](this['updateBitmapSmoothing']['bind'](this));},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x4a3)]=Sprite_Character[_0x5e5f48(0x363)][_0x5e5f48(0x1ae)],Sprite_Character[_0x5e5f48(0x363)][_0x5e5f48(0x1ae)]=function(){const _0xf82189=_0x5e5f48;VisuMZ[_0xf82189(0x248)][_0xf82189(0x4a3)][_0xf82189(0x2a6)](this),this[_0xf82189(0x243)][_0xf82189(0x1fe)](this[_0xf82189(0x217)][_0xf82189(0x3e5)](this));},Sprite_Character[_0x5e5f48(0x363)][_0x5e5f48(0x217)]=function(){const _0x513528=_0x5e5f48;if(!this[_0x513528(0x243)])return;this[_0x513528(0x243)]['smooth']=!!VisuMZ[_0x513528(0x248)][_0x513528(0x1e5)]['Movement'][_0x513528(0x2b0)];},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x3e8)]=Sprite_Character['prototype'][_0x5e5f48(0x30a)],Sprite_Character['prototype'][_0x5e5f48(0x30a)]=function(){const _0x560963=_0x5e5f48;return this[_0x560963(0x1cd)]()?this[_0x560963(0x13c)]():VisuMZ[_0x560963(0x248)]['Sprite_Character_characterPatternY'][_0x560963(0x2a6)](this);},Sprite_Character[_0x5e5f48(0x363)]['characterPatternYVS8']=function(){const _0x118b46=_0x5e5f48,_0x25ea4e=this[_0x118b46(0x3c0)][_0x118b46(0x2dd)](),_0x169268=[0x2,0x2,0x2,0x4,0x4,0x2,0x6,0x6,0x8,0x8];return(_0x169268[_0x25ea4e]-0x2)/0x2;},Sprite_Character[_0x5e5f48(0x363)][_0x5e5f48(0x48a)]=function(){const _0x157f96=_0x5e5f48;this[_0x157f96(0x2cd)]=0x0;if(this[_0x157f96(0x1ee)]()){const _0x3da441=VisuMZ[_0x157f96(0x248)][_0x157f96(0x1e5)][_0x157f96(0x1b0)],_0x4f2252=this['_character']['direction']();let _0x1f71df=0x0;if([0x1,0x4,0x7][_0x157f96(0x2d5)](_0x4f2252))_0x1f71df=_0x3da441[_0x157f96(0x364)];if([0x3,0x6,0x9][_0x157f96(0x2d5)](_0x4f2252))_0x1f71df=_0x3da441[_0x157f96(0x2c4)];[0x2,0x8]['includes'](_0x4f2252)&&(_0x1f71df=[-_0x3da441[_0x157f96(0x207)],0x0,_0x3da441['TiltVert']][this[_0x157f96(0x3c0)][_0x157f96(0x16f)]()]);if(this[_0x157f96(0x3c8)])_0x1f71df*=-0x1;this[_0x157f96(0x2cd)]=_0x1f71df;}},Sprite_Character['prototype'][_0x5e5f48(0x1ee)]=function(){const _0x516ea1=_0x5e5f48;if(this[_0x516ea1(0x25b)])return![];return this[_0x516ea1(0x3c0)][_0x516ea1(0x344)]()&&!this[_0x516ea1(0x3c0)][_0x516ea1(0x247)]()&&!this[_0x516ea1(0x3c0)][_0x516ea1(0x1b3)]()&&this[_0x516ea1(0x47f)]()===0x0;},Sprite_Character[_0x5e5f48(0x363)][_0x5e5f48(0x3ad)]=function(){const _0x4945a8=_0x5e5f48;this[_0x4945a8(0x38f)]['x']=this[_0x4945a8(0x3c0)][_0x4945a8(0x2d8)](),this[_0x4945a8(0x38f)]['y']=this[_0x4945a8(0x3c0)][_0x4945a8(0x46f)](),this[_0x4945a8(0x38f)]['opacity']=this[_0x4945a8(0x1e6)],this[_0x4945a8(0x38f)][_0x4945a8(0x381)]=this[_0x4945a8(0x3c0)][_0x4945a8(0x292)](),this[_0x4945a8(0x38f)]['_hidden']=this[_0x4945a8(0x490)],!this[_0x4945a8(0x3c0)][_0x4945a8(0x147)]()?(this[_0x4945a8(0x38f)]['scale']['x']=Math[_0x4945a8(0x34c)](0x1,this['_shadowSprite'][_0x4945a8(0x3c5)]['x']+0.1),this[_0x4945a8(0x38f)][_0x4945a8(0x3c5)]['y']=Math[_0x4945a8(0x34c)](0x1,this[_0x4945a8(0x38f)][_0x4945a8(0x3c5)]['y']+0.1)):(this[_0x4945a8(0x38f)][_0x4945a8(0x3c5)]['x']=Math[_0x4945a8(0x27f)](0x0,this['_shadowSprite'][_0x4945a8(0x3c5)]['x']-0.1),this[_0x4945a8(0x38f)][_0x4945a8(0x3c5)]['y']=Math[_0x4945a8(0x27f)](0x0,this[_0x4945a8(0x38f)][_0x4945a8(0x3c5)]['y']-0.1));},Sprite_Character['prototype'][_0x5e5f48(0x24b)]=function(){const _0x132ba3=_0x5e5f48,_0x46b191=this['_eventIconSprite'],_0x1db702=this[_0x132ba3(0x47f)]();if(_0x1db702<=0x0)return _0x46b191[_0x132ba3(0x3ed)](0x0,0x0,0x0,0x0);else{const _0x24e1c0=ImageManager[_0x132ba3(0x15d)],_0x57098c=ImageManager['iconHeight'],_0x7aac4e=_0x1db702%0x10*_0x24e1c0,_0x1468cf=Math['floor'](_0x1db702/0x10)*_0x57098c;_0x46b191[_0x132ba3(0x3ed)](_0x7aac4e,_0x1468cf,_0x24e1c0,_0x57098c),this[_0x132ba3(0x381)]=!![];}const _0x57a72b=this['_character'][_0x132ba3(0x23f)]();this[_0x132ba3(0x46c)]()?this[_0x132ba3(0x274)](_0x46b191):(_0x46b191['x']=_0x57a72b?_0x57a72b[_0x132ba3(0x429)]:0x0,_0x46b191['y']=_0x57a72b?-this[_0x132ba3(0x356)]+_0x57a72b['bufferY']:0x0),_0x46b191[_0x132ba3(0x4e3)]=_0x57a72b?_0x57a72b[_0x132ba3(0x4e3)]:0x0,this['removeChild'](_0x46b191),this[_0x132ba3(0x36f)](_0x46b191),_0x46b191[_0x132ba3(0x2cd)]=-this[_0x132ba3(0x2cd)];},Sprite_Character[_0x5e5f48(0x363)][_0x5e5f48(0x274)]=function(_0x3870b8){const _0x2cd4bb=_0x5e5f48;_0x3870b8['x']=0x0,_0x3870b8['y']=-this['height']+this[_0x2cd4bb(0x356)]*0x2/0x5,this[_0x2cd4bb(0x3c0)]['pattern']()!==0x1&&(_0x3870b8['y']+=0x1);},Sprite_Character['prototype'][_0x5e5f48(0x47f)]=function(){const _0x5a44e8=_0x5e5f48;if(!this[_0x5a44e8(0x3c0)])return 0x0;if(this[_0x5a44e8(0x3c0)][_0x5a44e8(0x11b)])return 0x0;const _0x7516bd=this['_character']['getEventIconData']();return _0x7516bd?_0x7516bd[_0x5a44e8(0x1e4)]||0x0:0x0;},VisuMZ[_0x5e5f48(0x248)]['Sprite_Balloon_setup']=Sprite_Balloon[_0x5e5f48(0x363)][_0x5e5f48(0x34f)],Sprite_Balloon[_0x5e5f48(0x363)]['setup']=function(_0x48fc3a,_0x66e9e2){const _0x221687=_0x5e5f48;VisuMZ[_0x221687(0x248)][_0x221687(0x3f9)][_0x221687(0x2a6)](this,_0x48fc3a,_0x66e9e2),VisuMZ['EventsMoveCore'][_0x221687(0x1e5)]['VS8'][_0x221687(0x238)]&&this['_target'][_0x221687(0x3c0)]['setBalloonPose'](_0x66e9e2,this[_0x221687(0x4ad)]);},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x1aa)]=Sprite_Balloon['prototype']['updatePosition'],Sprite_Balloon['prototype']['updatePosition']=function(){const _0x273e7=_0x5e5f48;VisuMZ[_0x273e7(0x248)][_0x273e7(0x1aa)][_0x273e7(0x2a6)](this),this[_0x273e7(0x295)]();},Sprite_Balloon[_0x5e5f48(0x363)][_0x5e5f48(0x295)]=function(){const _0x448348=_0x5e5f48;this[_0x448348(0x25e)]['_character'][_0x448348(0x1cd)]()&&(this['x']+=VisuMZ[_0x448348(0x248)][_0x448348(0x1e5)][_0x448348(0x30e)][_0x448348(0x40a)],this['y']+=VisuMZ[_0x448348(0x248)]['Settings']['VS8'][_0x448348(0x22b)]);},Sprite_Timer[_0x5e5f48(0x363)]['createBitmap']=function(){const _0x52a1e6=_0x5e5f48;this[_0x52a1e6(0x243)]=new Bitmap(Math[_0x52a1e6(0x1e9)](Graphics[_0x52a1e6(0x1ca)]/0x2),0x30),this[_0x52a1e6(0x243)][_0x52a1e6(0x1f3)]=this['fontFace'](),this[_0x52a1e6(0x243)][_0x52a1e6(0x2b2)]=this[_0x52a1e6(0x2b2)](),this['bitmap'][_0x52a1e6(0x218)]=ColorManager[_0x52a1e6(0x218)]();},Sprite_Timer[_0x5e5f48(0x363)]['timerText']=function(){const _0x58e919=_0x5e5f48,_0x4b7fdb=Math['floor'](this[_0x58e919(0x17c)]/0x3c/0x3c),_0x57e94b=Math['floor'](this[_0x58e919(0x17c)]/0x3c)%0x3c,_0x59f1ad=this[_0x58e919(0x17c)]%0x3c;let _0x53d43a=_0x57e94b[_0x58e919(0x317)](0x2)+':'+_0x59f1ad['padZero'](0x2);if(_0x4b7fdb>0x0)_0x53d43a=_0x58e919(0x182)[_0x58e919(0x2ac)](_0x4b7fdb,_0x53d43a);return _0x53d43a;},VisuMZ['EventsMoveCore']['Spriteset_Map_createLowerLayer']=Spriteset_Map[_0x5e5f48(0x363)][_0x5e5f48(0x254)],Spriteset_Map[_0x5e5f48(0x363)][_0x5e5f48(0x254)]=function(){const _0x228dd4=_0x5e5f48;VisuMZ[_0x228dd4(0x248)][_0x228dd4(0x4ca)][_0x228dd4(0x2a6)](this),this[_0x228dd4(0x3df)]();},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x14e)]=Spriteset_Map[_0x5e5f48(0x363)][_0x5e5f48(0x179)],Spriteset_Map['prototype'][_0x5e5f48(0x179)]=function(){const _0x108986=_0x5e5f48;VisuMZ[_0x108986(0x248)][_0x108986(0x14e)][_0x108986(0x2a6)](this),this[_0x108986(0x251)]();},Spriteset_Map['prototype']['createShadows']=function(){const _0x4ecf21=_0x5e5f48;if(!VisuMZ['EventsMoveCore'][_0x4ecf21(0x1e5)]['Movement'][_0x4ecf21(0x2e2)])return;for(const _0x4da7d3 of this[_0x4ecf21(0x1b2)]){this['createCharacterShadow'](_0x4da7d3);}},Spriteset_Map[_0x5e5f48(0x363)][_0x5e5f48(0x3d7)]=function(_0x1d064f){const _0x5c8d9a=_0x5e5f48;_0x1d064f[_0x5c8d9a(0x38f)]=new Sprite(),_0x1d064f[_0x5c8d9a(0x38f)][_0x5c8d9a(0x2cc)]=_0x1d064f[_0x5c8d9a(0x3c0)][_0x5c8d9a(0x199)](),_0x1d064f['_shadowSprite']['bitmap']=ImageManager[_0x5c8d9a(0x2fe)](_0x1d064f[_0x5c8d9a(0x38f)][_0x5c8d9a(0x2cc)]),_0x1d064f['_shadowSprite'][_0x5c8d9a(0x118)]['x']=0.5,_0x1d064f[_0x5c8d9a(0x38f)][_0x5c8d9a(0x118)]['y']=0x1,_0x1d064f[_0x5c8d9a(0x38f)]['z']=0x0,this[_0x5c8d9a(0x1d5)][_0x5c8d9a(0x36f)](_0x1d064f[_0x5c8d9a(0x38f)]);},Spriteset_Map[_0x5e5f48(0x363)][_0x5e5f48(0x3e4)]=function(){const _0x7b8373=_0x5e5f48;if(!VisuMZ['EventsMoveCore'][_0x7b8373(0x1e5)]['Movement']['ShowShadows'])return;for(const _0x488be0 of this['_characterSprites']){this[_0x7b8373(0x1d5)][_0x7b8373(0x2d1)](_0x488be0[_0x7b8373(0x38f)]);}},Spriteset_Map['prototype']['createLabelWindows']=function(){const _0x43e197=_0x5e5f48;this['_labelWindows']=[];for(const _0x135c2f of $gameMap['events']()){this[_0x43e197(0x362)](_0x135c2f);}},Spriteset_Map['prototype'][_0x5e5f48(0x362)]=function(_0x372108){const _0x21d77a=_0x5e5f48;if(!this['isTargetEventValidForLabelWindow'](_0x372108))return;const _0xa0c54=new Window_EventLabel(_0x372108);_0xa0c54['z']=0x8,_0xa0c54[_0x21d77a(0x475)]=Sprite[_0x21d77a(0x465)]++,this[_0x21d77a(0x1d5)][_0x21d77a(0x36f)](_0xa0c54),this[_0x21d77a(0x43a)][_0x21d77a(0x4bf)](_0xa0c54);},Spriteset_Map[_0x5e5f48(0x363)][_0x5e5f48(0x397)]=function(_0x24303a){const _0x30f3ec=_0x5e5f48,_0xd3f5e6=_0x24303a['event']();if(_0xd3f5e6[_0x30f3ec(0x270)][_0x30f3ec(0x1b1)](/<LABEL:[ ](.*?)>/i))return!![];if(_0xd3f5e6['note']['match'](/<LABEL>\s*([\s\S]*)\s*<\/LABEL>/i))return!![];for(const _0x1fca94 of _0xd3f5e6[_0x30f3ec(0x365)]){let _0x10af4b='';for(const _0x46e16b of _0x1fca94['list']){[0x6c,0x198][_0x30f3ec(0x2d5)](_0x46e16b['code'])&&(_0x10af4b+=_0x46e16b['parameters'][0x0]);}if(_0x10af4b[_0x30f3ec(0x1b1)](/<LABEL:[ ](.*?)>/i))return!![];if(_0x10af4b[_0x30f3ec(0x1b1)](/<LABEL>\s*([\s\S]*)\s*<\/LABEL>/i))return!![];}return![];},Spriteset_Map[_0x5e5f48(0x363)][_0x5e5f48(0x222)]=function(_0x1830d0){const _0x46cd3d=_0x5e5f48;this[_0x46cd3d(0x1b2)]=this[_0x46cd3d(0x1b2)]||[];const _0x376149=new Sprite_Character(_0x1830d0);this[_0x46cd3d(0x1b2)][_0x46cd3d(0x4bf)](_0x376149),this[_0x46cd3d(0x1d5)][_0x46cd3d(0x36f)](_0x376149),this[_0x46cd3d(0x3d7)](_0x376149),this[_0x46cd3d(0x362)](_0x1830d0),_0x376149[_0x46cd3d(0x169)]();},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x45a)]=Game_Message[_0x5e5f48(0x363)][_0x5e5f48(0x1d1)],Game_Message[_0x5e5f48(0x363)][_0x5e5f48(0x1d1)]=function(_0x1057e9,_0x4efb59){const _0x106568=_0x5e5f48;this['_selfTargetNumberInput']=$gameTemp['getSelfTarget'](),VisuMZ[_0x106568(0x248)]['Game_Message_setNumberInput'][_0x106568(0x2a6)](this,_0x1057e9,_0x4efb59);},VisuMZ[_0x5e5f48(0x248)]['Window_NumberInput_start']=Window_NumberInput['prototype']['start'],Window_NumberInput[_0x5e5f48(0x363)][_0x5e5f48(0x2de)]=function(){const _0x30e782=_0x5e5f48;$gameTemp[_0x30e782(0x152)]($gameMessage['_selfTargetNumberInput']),VisuMZ['EventsMoveCore'][_0x30e782(0x2f7)][_0x30e782(0x2a6)](this),$gameTemp[_0x30e782(0x29e)]();},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x200)]=Window_NumberInput['prototype'][_0x5e5f48(0x370)],Window_NumberInput[_0x5e5f48(0x363)][_0x5e5f48(0x370)]=function(){const _0x4c98ab=_0x5e5f48;$gameTemp['registerSelfTarget']($gameMessage[_0x4c98ab(0x178)]),VisuMZ[_0x4c98ab(0x248)]['Window_NumberInput_processOk'][_0x4c98ab(0x2a6)](this),$gameTemp[_0x4c98ab(0x29e)](),$gameMessage[_0x4c98ab(0x178)]=undefined;},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x43d)]=Game_Message['prototype']['setItemChoice'],Game_Message[_0x5e5f48(0x363)][_0x5e5f48(0x497)]=function(_0xaf427c,_0x361011){const _0x3b2c68=_0x5e5f48;this['_selfTargetItemChoice']=$gameTemp[_0x3b2c68(0x23a)](),VisuMZ[_0x3b2c68(0x248)]['Game_Message_setItemChoice']['call'](this,_0xaf427c,_0x361011);},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x367)]=Window_EventItem[_0x5e5f48(0x363)][_0x5e5f48(0x3c7)],Window_EventItem[_0x5e5f48(0x363)][_0x5e5f48(0x3c7)]=function(){const _0x12d722=_0x5e5f48;$gameTemp['registerSelfTarget']($gameMessage[_0x12d722(0x137)]),VisuMZ['EventsMoveCore'][_0x12d722(0x367)][_0x12d722(0x2a6)](this),$gameTemp[_0x12d722(0x29e)](),$gameMessage[_0x12d722(0x137)]=undefined;},VisuMZ[_0x5e5f48(0x248)]['Window_EventItem_onCancel']=Window_EventItem[_0x5e5f48(0x363)][_0x5e5f48(0x253)],Window_EventItem[_0x5e5f48(0x363)]['onCancel']=function(){const _0x1bb266=_0x5e5f48;$gameTemp[_0x1bb266(0x152)]($gameMessage[_0x1bb266(0x137)]),VisuMZ['EventsMoveCore'][_0x1bb266(0x345)][_0x1bb266(0x2a6)](this),$gameTemp['clearSelfTarget'](),$gameMessage[_0x1bb266(0x137)]=undefined;},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x348)]=Window_Message['prototype'][_0x5e5f48(0x4c3)],Window_Message[_0x5e5f48(0x363)][_0x5e5f48(0x4c3)]=function(){const _0x7100c5=_0x5e5f48;$gameMessage[_0x7100c5(0x288)](),VisuMZ['EventsMoveCore'][_0x7100c5(0x348)][_0x7100c5(0x2a6)](this),$gameTemp[_0x7100c5(0x29e)]();},VisuMZ[_0x5e5f48(0x248)][_0x5e5f48(0x3a9)]=Window_ScrollText[_0x5e5f48(0x363)][_0x5e5f48(0x4c3)],Window_ScrollText['prototype'][_0x5e5f48(0x4c3)]=function(){const _0x2ea30d=_0x5e5f48;$gameMessage[_0x2ea30d(0x288)](),VisuMZ[_0x2ea30d(0x248)]['Window_ScrollText_startMessage'][_0x2ea30d(0x2a6)](this),$gameTemp[_0x2ea30d(0x29e)]();};function Window_EventLabel(){this['initialize'](...arguments);}Window_EventLabel[_0x5e5f48(0x363)]=Object[_0x5e5f48(0x29c)](Window_Base[_0x5e5f48(0x363)]),Window_EventLabel['prototype'][_0x5e5f48(0x205)]=Window_EventLabel,Window_EventLabel['prototype'][_0x5e5f48(0x2b9)]=function(_0x37fd7f){const _0x21f9c2=_0x5e5f48;this[_0x21f9c2(0x466)]=_0x37fd7f;const _0x5e06ac=new Rectangle(0x0,0x0,Graphics[_0x21f9c2(0x1ca)]/0x4,this[_0x21f9c2(0x4c6)](0x1));this[_0x21f9c2(0x3d5)](),Window_Base[_0x21f9c2(0x363)][_0x21f9c2(0x2b9)]['call'](this,_0x5e06ac),this[_0x21f9c2(0x2b7)]=0x0,this['setBackgroundType'](0x2),this['_text']='';},Window_EventLabel[_0x5e5f48(0x363)][_0x5e5f48(0x3d5)]=function(){const _0x469eb7=_0x5e5f48;this['_eventErased']=![],this['_screenZoomScale']=$gameScreen[_0x469eb7(0x30f)](),this[_0x469eb7(0x2a0)]=this[_0x469eb7(0x466)]['screenX'](),this[_0x469eb7(0x452)]=this[_0x469eb7(0x466)]['screenY'](),this['_eventLabelOffsetX']=this[_0x469eb7(0x466)][_0x469eb7(0x2c1)]['offsetX'],this[_0x469eb7(0x24d)]=this[_0x469eb7(0x466)][_0x469eb7(0x2c1)][_0x469eb7(0x468)],this[_0x469eb7(0x2f5)]=this[_0x469eb7(0x466)]['_pageIndex'],this['_cacheVisibility']=this['isLabelVisible'](),this[_0x469eb7(0x373)]=$gamePlayer['x'],this['_visiblePlayerY']=$gamePlayer['y'],this['_visibleEventX']=this[_0x469eb7(0x466)]['x'],this[_0x469eb7(0x35b)]=this[_0x469eb7(0x466)]['y'];},Window_EventLabel[_0x5e5f48(0x363)][_0x5e5f48(0x169)]=function(){const _0x404b21=_0x5e5f48;Window_Base[_0x404b21(0x363)][_0x404b21(0x169)][_0x404b21(0x2a6)](this);if(!this[_0x404b21(0x4ce)]())return;this[_0x404b21(0x3a8)](),this[_0x404b21(0x2d9)](),this['updatePosition'](),this[_0x404b21(0x4c7)]();},Window_EventLabel['prototype'][_0x5e5f48(0x4ce)]=function(){const _0x26dd0d=_0x5e5f48;if(!this['_event'])return![];if(!this[_0x26dd0d(0x466)][_0x26dd0d(0x2c1)])return![];if(this[_0x26dd0d(0x2f5)]!==this[_0x26dd0d(0x466)][_0x26dd0d(0x3d3)])return!![];if(this[_0x26dd0d(0x466)][_0x26dd0d(0x11b)]&&!this[_0x26dd0d(0x41e)])return!![];if(this[_0x26dd0d(0x466)][_0x26dd0d(0x2c1)][_0x26dd0d(0x289)]==='')return![];if(this[_0x26dd0d(0x1d3)]!==$gameScreen['zoomScale']())return!![];if(this['_eventScreenX']!==this[_0x26dd0d(0x466)][_0x26dd0d(0x34a)]())return!![];if(this[_0x26dd0d(0x452)]!==this[_0x26dd0d(0x466)][_0x26dd0d(0x416)]())return!![];if(this[_0x26dd0d(0x1c5)]!==this['_event'][_0x26dd0d(0x2c1)][_0x26dd0d(0x2b6)])return!![];if(this[_0x26dd0d(0x24d)]!==this['_event'][_0x26dd0d(0x2c1)][_0x26dd0d(0x468)])return!![];if(this[_0x26dd0d(0x373)]!==$gamePlayer['x'])return!![];if(this[_0x26dd0d(0x2fa)]!==$gamePlayer['y'])return!![];if(this[_0x26dd0d(0x3b0)]!==this[_0x26dd0d(0x466)]['x'])return!![];if(this['_visibleEventY']!==this[_0x26dd0d(0x466)]['y'])return!![];if(this[_0x26dd0d(0x45d)]&&this[_0x26dd0d(0x2b7)]<0xff)return!![];if(!this[_0x26dd0d(0x45d)]&&this[_0x26dd0d(0x2b7)]>0x0)return!![];if(SceneManager[_0x26dd0d(0x15b)][_0x26dd0d(0x29f)]>0x0)return!![];return![];},Window_EventLabel['prototype'][_0x5e5f48(0x3a8)]=function(){const _0x5d810c=_0x5e5f48;this[_0x5d810c(0x466)][_0x5d810c(0x3e2)]()!==this[_0x5d810c(0x3cc)]&&(this['_text']=this['_event'][_0x5d810c(0x3e2)](),this[_0x5d810c(0x3c6)]());},Window_EventLabel[_0x5e5f48(0x363)]['updateScale']=function(){const _0x5c7267=_0x5e5f48;this['scale']['x']=0x1/$gameScreen[_0x5c7267(0x30f)](),this[_0x5c7267(0x3c5)]['y']=0x1/$gameScreen[_0x5c7267(0x30f)](),this[_0x5c7267(0x1d3)]=$gameScreen[_0x5c7267(0x30f)]();},Window_EventLabel[_0x5e5f48(0x363)][_0x5e5f48(0x360)]=function(){const _0x4e4d6f=_0x5e5f48;if(!SceneManager['_scene'])return;if(!SceneManager[_0x4e4d6f(0x15b)][_0x4e4d6f(0x469)])return;const _0x391ec1=SceneManager[_0x4e4d6f(0x15b)][_0x4e4d6f(0x469)]['findTargetSprite'](this[_0x4e4d6f(0x466)]);if(!_0x391ec1)return;this['x']=Math[_0x4e4d6f(0x1e9)](this[_0x4e4d6f(0x466)]['screenX']()-Math[_0x4e4d6f(0x115)](this[_0x4e4d6f(0x3bc)]*this['scale']['x']/0x2)),this['x']+=this[_0x4e4d6f(0x466)]['_labelWindow'][_0x4e4d6f(0x2b6)],this['y']=this[_0x4e4d6f(0x466)]['screenY']()-_0x391ec1[_0x4e4d6f(0x356)],this['y']+=Math['round']($gameSystem[_0x4e4d6f(0x2bd)]()*0.5),this['y']-=Math[_0x4e4d6f(0x1e9)](this[_0x4e4d6f(0x356)]*this[_0x4e4d6f(0x3c5)]['y']),this['y']+=this['_event'][_0x4e4d6f(0x2c1)][_0x4e4d6f(0x468)],this[_0x4e4d6f(0x41e)]=this[_0x4e4d6f(0x466)]['_erased'],this['_eventScreenX']=this['_event'][_0x4e4d6f(0x34a)](),this[_0x4e4d6f(0x452)]=this[_0x4e4d6f(0x466)]['screenY'](),this['_eventLabelOffsetX']=this[_0x4e4d6f(0x466)][_0x4e4d6f(0x2c1)][_0x4e4d6f(0x2b6)],this[_0x4e4d6f(0x24d)]=this['_event'][_0x4e4d6f(0x2c1)]['offsetY'],this[_0x4e4d6f(0x2f5)]=this[_0x4e4d6f(0x466)][_0x4e4d6f(0x3d3)],this['_eventErased']&&(this[_0x4e4d6f(0x2b7)]=0x0);},Window_EventLabel[_0x5e5f48(0x363)]['updateOpacity']=function(){const _0x42ae7e=_0x5e5f48;if(this[_0x42ae7e(0x26e)]())this[_0x42ae7e(0x2b7)]+=this[_0x42ae7e(0x4a9)]();else SceneManager['_scene'][_0x42ae7e(0x29f)]>0x0?this['contentsOpacity']=0x0:this[_0x42ae7e(0x2b7)]-=this[_0x42ae7e(0x4a9)]();},Window_EventLabel[_0x5e5f48(0x363)][_0x5e5f48(0x26e)]=function(){const _0x3d35fa=_0x5e5f48;if(!$gameSystem[_0x3d35fa(0x4e6)]())return![];if(this[_0x3d35fa(0x466)]?.[_0x3d35fa(0x11b)])return![];if(SceneManager[_0x3d35fa(0x15b)][_0x3d35fa(0x29f)]>0x0)return![];const _0xd11fcb=$gamePlayer['x'],_0x4dcdf5=$gamePlayer['y'],_0x2e8bc9=this[_0x3d35fa(0x466)]['x'],_0x2f1b90=this[_0x3d35fa(0x466)]['y'];if(this[_0x3d35fa(0x373)]===_0xd11fcb&&this[_0x3d35fa(0x2fa)]===_0x4dcdf5&&this['_visibleEventX']===_0x2e8bc9&&this['_visibleEventY']===_0x2f1b90)return this[_0x3d35fa(0x45d)];this[_0x3d35fa(0x373)]=$gamePlayer['x'],this[_0x3d35fa(0x2fa)]=$gamePlayer['y'],this[_0x3d35fa(0x3b0)]=this[_0x3d35fa(0x466)]['x'],this[_0x3d35fa(0x35b)]=this[_0x3d35fa(0x466)]['y'];if($gameMap['absDistance'](_0xd11fcb,_0x4dcdf5,_0x2e8bc9,_0x2f1b90)>this[_0x3d35fa(0x466)][_0x3d35fa(0x4b8)]())return this[_0x3d35fa(0x45d)]=![],![];return this['_cacheVisibility']=!![],!![];},Window_EventLabel[_0x5e5f48(0x363)][_0x5e5f48(0x4a9)]=function(){const _0x1f5d43=_0x5e5f48;return VisuMZ[_0x1f5d43(0x248)]['Settings']['Label'][_0x1f5d43(0x1a0)];},Window_EventLabel[_0x5e5f48(0x363)][_0x5e5f48(0x153)]=function(){const _0x345365=_0x5e5f48,_0x2c05dc=this[_0x345365(0x358)](this['_text']);this[_0x345365(0x3bc)]=_0x2c05dc[_0x345365(0x3bc)]+($gameSystem['windowPadding']()+this[_0x345365(0x2eb)]())*0x2,this[_0x345365(0x356)]=Math[_0x345365(0x27f)](this[_0x345365(0x383)](),_0x2c05dc['height'])+$gameSystem['windowPadding']()*0x2,this[_0x345365(0x47d)]();},Window_EventLabel[_0x5e5f48(0x363)][_0x5e5f48(0x383)]=function(){const _0x4daa18=_0x5e5f48;return VisuMZ['EventsMoveCore'][_0x4daa18(0x1e5)][_0x4daa18(0x220)]['LineHeight'];},Window_EventLabel[_0x5e5f48(0x363)][_0x5e5f48(0x1f9)]=function(){const _0x23d40b=_0x5e5f48;Window_Base[_0x23d40b(0x363)][_0x23d40b(0x1f9)][_0x23d40b(0x2a6)](this),this[_0x23d40b(0x48d)]['fontSize']=this[_0x23d40b(0x126)]();},Window_EventLabel[_0x5e5f48(0x363)][_0x5e5f48(0x126)]=function(){const _0x1bb5e2=_0x5e5f48;return VisuMZ[_0x1bb5e2(0x248)][_0x1bb5e2(0x1e5)][_0x1bb5e2(0x220)]['FontSize'];},Window_EventLabel[_0x5e5f48(0x363)][_0x5e5f48(0x3c6)]=function(){const _0x20e8b7=_0x5e5f48;this[_0x20e8b7(0x153)](),this[_0x20e8b7(0x48d)][_0x20e8b7(0x21c)]();const _0x3e5b10=this[_0x20e8b7(0x3cc)]['split'](/[\r\n]+/);let _0xe6ba42=0x0;for(const _0x2d52f4 of _0x3e5b10){const _0x159016=this[_0x20e8b7(0x358)](_0x2d52f4),_0x5aacb3=Math[_0x20e8b7(0x115)]((this[_0x20e8b7(0x36a)]-_0x159016['width'])/0x2);this[_0x20e8b7(0x388)](_0x2d52f4,_0x5aacb3,_0xe6ba42),_0xe6ba42+=_0x159016[_0x20e8b7(0x356)];}},Window_EventLabel[_0x5e5f48(0x363)][_0x5e5f48(0x4f4)]=function(_0x219c9a,_0x343d02){const _0x34d2a4=_0x5e5f48;_0x343d02[_0x34d2a4(0x233)]&&this['drawIcon'](_0x219c9a,_0x343d02['x']+0x2,_0x343d02['y']),_0x343d02['x']+=Math['min'](this['iconSize'](),ImageManager[_0x34d2a4(0x15d)])+0x4;},Window_EventLabel[_0x5e5f48(0x363)][_0x5e5f48(0x1ad)]=function(_0x4c39af,_0x1c3603,_0x341db2){const _0x58c3f9=_0x5e5f48,_0x4529b1=ImageManager[_0x58c3f9(0x2fe)](_0x58c3f9(0x4cc)),_0x40c7b6=ImageManager['iconWidth'],_0x203087=ImageManager[_0x58c3f9(0x2e9)],_0x2ed476=_0x4c39af%0x10*_0x40c7b6,_0x3b65e6=Math[_0x58c3f9(0x115)](_0x4c39af/0x10)*_0x203087,_0x43fc48=Math[_0x58c3f9(0x34c)](this['iconSize']()),_0x345dec=Math['min'](this[_0x58c3f9(0x313)]());this[_0x58c3f9(0x48d)]['blt'](_0x4529b1,_0x2ed476,_0x3b65e6,_0x40c7b6,_0x203087,_0x1c3603,_0x341db2,_0x43fc48,_0x345dec);},Window_EventLabel[_0x5e5f48(0x363)][_0x5e5f48(0x313)]=function(){const _0x444900=_0x5e5f48;return VisuMZ[_0x444900(0x248)][_0x444900(0x1e5)][_0x444900(0x220)][_0x444900(0x1b7)];};