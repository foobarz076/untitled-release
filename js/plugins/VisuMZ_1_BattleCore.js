//=============================================================================
// VisuStella MZ - Battle Core
// VisuMZ_1_BattleCore.js
//=============================================================================

var Imported = Imported || {};
Imported.VisuMZ_1_BattleCore = true;

var VisuMZ = VisuMZ || {};
VisuMZ.BattleCore = VisuMZ.BattleCore || {};
VisuMZ.BattleCore.version = 1.26;

//=============================================================================
 /*:
 * @target MZ
 * @plugindesc [RPG Maker MZ] [Tier 1] [Version 1.26] [BattleCore]
 * @author VisuStella
 * @url http://www.yanfly.moe/wiki/Battle_Core_VisuStella_MZ
 * @orderAfter VisuMZ_0_CoreEngine
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 *
 * The Battle Core plugin revamps the battle engine provided by RPG Maker MZ to
 * become more flexible, streamlined, and support a variety of features. The
 * updated battle engine allows for custom Action Sequences, battle layout
 * styles, and a lot of control over the battle mechanics, too.
 *
 * Features include all (but not limited to) the following:
 * 
 * * Action Sequence Plugin Commands to give you full control over what happens
 *   during the course of a skill or item.
 * * Animated Sideview Battler support for enemies!
 * * Auto Battle options for party-wide and actor-only instances.
 * * Base Troop Events to quickly streamline events for all Troop events.
 * * Battle Command control to let you change which commands appear for actors.
 * * Battle Layout styles to change the way the battle scene looks.
 * * Casting animation support for skills.
 * * Critical Hit control over the success rate formula and damage multipliers.
 * * Custom target scopes added for skills and items.
 * * Damage formula control, including Damage Styles.
 * * Damage caps, both hard caps and soft caps.
 * * Damage traits such Armor Penetration/Reduction to bypass defenses.
 * * Elements & Status Menu Core support for traits.
 * * Multitude of JavaScript notetags and global Plugin Parameters to let you
 *   make a variety of effects across various instances during battle.
 * * Party Command window can be skipped/disabled entirely.
 * * Weather effects now show in battle.
 * * Streamlined Battle Log to remove redundant information and improve the
 *   flow of battle.
 * * Visual HP Gauges can be displayed above the heads of actors and/or enemies
 *   with a possible requirement for enemies to be defeated at least once first
 *   in order for them to show.
 *
 * ============================================================================
 * Requirements
 * ============================================================================
 *
 * This plugin is made for RPG Maker MZ. This will not work in other iterations
 * of RPG Maker.
 *
 * ------ Tier 1 ------
 *
 * This plugin is a Tier 1 plugin. Place it under other plugins of lower tier
 * value on your Plugin Manager list (ie: 0, 1, 2, 3, 4, 5). This is to ensure
 * that your plugins will have the best compatibility with the rest of the
 * VisuStella MZ library.
 *
 * ============================================================================
 * Major Changes
 * ============================================================================
 *
 * This plugin will overwrite some core parts of the RPG Maker MZ base code in
 * order to ensure the Battle Core plugin will work at full capacity. The
 * following are explanations of what has been changed.
 *
 * ---
 *
 * Action Sequences
 *
 * - Action sequences are now done either entirely by the Battle Log Window or
 * through common events if the <Custom Action Sequence> notetag is used.
 * In RPG Maker MZ by default, Action Sequences would be a mixture of using the
 * Battle Log Window, the Battle Manager, and the Battle Scene, making it hard
 * to fully grab control of the situation.
 *
 * ---
 *
 * Action Speed
 *
 * - Action speeds determine the turn order in the default battle system. The
 * AGI of a battle unit is also taken into consideration. However, the random
 * variance applied to the action speed system makes the turn order extremely
 * chaotic and hard for the player to determine. Thus, the random variance
 * aspect of it has been turned off. This can be reenabled by default through
 * Plugin Parameters => Mechanics Settings => Allow Random Speed?
 *
 * ---
 *
 * Animated Sideview Battler Support For Enemies
 *
 * - Enemies can now use Sideview Actor sprites for themselves! They will
 * behave like actors and can even carry their own set of weapons for physical
 * attacks. These must be set up using notetags. More information can be found
 * in the notetag section.
 *
 * - As the sprites are normally used for actors, some changes have been made
 * to Sprite_Actor to be able to support both actors and enemies. These changes
 * should have minimal impact on other plugins.
 *
 * ---
 *
 * Battle Sprite Updates
 *
 * - A lot of functions in Sprite_Battler, Sprite_Actor, and Sprite_Enemy have
 * been overwritten to make the new Action Sequence system added by this plugin
 * possible. These changes make it possible for the sprites to move anywhere on
 * the screen, jump, float, change visibility, and more.
 *
 * ---
 *
 * Change Battle Back in Battle
 * 
 * - By default, the Change Battle Back event command does not work in battle.
 * Any settings made to it will only reflect in the following battle. Now, if
 * the battle back event command is used during battle, it will reflect upon
 * any new changes immediately.
 *
 * ---
 *
 * Critical Hit - LUK Influence
 *
 * - The LUK Buffs now affect the critical hit rate based off how the formula
 * is now calculated. Each stack of a LUK Buff will double the critical hit
 * rate and compound upon that. That means a x1 LUK Buff stack will raise it by
 * x2, a x2 LUK Buff stack will raise the critical hit rate by x4, a x3 LUK
 * Buff Stack will raise the critical hit rate stack by x8, and so on.
 *
 * - LUK also plays a role in how much damage is dealt with critical hits. The
 * default critical hit multiplier has been reduced from x3 to x2. However, a
 * percentage of LUK will added on (based off the user's CRI rate) onto the
 * finalized critical damage. If the user's CRI rate is 4%, then 4% of the user
 * LUK value will also be added onto the damage.
 *
 * - This change can be altered through Plugin Parameters => Damage Settings =>
 * Critical Hits => JS: Rate Formula and JS: Damage Formula.
 *
 * ---
 * 
 * Damage Popups
 * 
 * - Damage popups are now formatted with + and - to determine healing and
 * damage. MP Damage will also include "MP" at the back. This is to make it
 * clearer what each colored variant of the damage popup means as well as help
 * color blind players read the on-screen data properly.
 * 
 * - Damage popups have also been rewritten to show all changed aspects instead
 * of just one. Previously with RPG Maker MZ, if an action would deal both HP
 * and MP damage, only one of them would show. Now, everything is separated and
 * both HP and MP changes will at a time.
 * 
 * ---
 * 
 * Dual Wielding
 * 
 * - Previously, RPG Maker MZ had "Dual Wielding" attack using both weapon
 * animations at once, with the combined ATK of each weapon. It's confusing to
 * look at and does not portray the nature of "Dual Wielding".
 * 
 * - Dual Wielding, or in the case of users adding in third and fourth weapons,
 * Multi Wielding is now changed. Each weapon is displayed individually, each
 * producing its own attack animation, showing each weapon type, and applying
 * only that weapon's ATK, Traits, and related effects. It is no longer a
 * combined effect to display everything at once like RPG Maker MZ default.
 * 
 * - If an actor has multiple weapon slots but some of them are unequipped,
 * then the action will treat the attack as a single attack. There will be no
 * barehanded attack to add on top of it. This is to match RPG Maker MZ's
 * decision to omit a second animation if the same scenario is applied.
 * 
 * ---
 *
 * Force Action
 *
 * - Previously, Forced Actions would interrupt the middle of an event to
 * perform an action. However, with the addition of more flexible Action
 * Sequences, the pre-existing Force Action system would not be able to exist
 * and would require being remade.
 *
 * - Forced Actions now are instead, added to a separate queue from the action
 * battler list. Whenever an action and/or common event is completed, then if
 * there's a Forced Action battler queued, then the Forced Action battler will
 * have its turn. This is the cleanest method available and avoids the most
 * conflicts possible.
 *
 * - This means if you planned to make cinematic sequences with Forced Actions,
 * you will need to account for the queued Force Actions. However, in the case
 * of battle cinematics, we would highly recommend that you use the newly added
 * Action Sequence Plugin Commands instead as those give you more control than
 * any Force Action ever could.
 *
 * ---
 *
 * Random Scope
 *
 * - The skill and item targeting scopes for Random Enemy, 2 Random Enemies,
 * 3 Random Enemies, 4 Random Enemies will now ignore TGR and utilize true
 * randomness.
 *
 * ---
 *
 * Spriteset_Battle Update
 *
 * - The spriteset now has extra containers to separate battlers (actors and
 * enemies), animations, and damage. This is to make actors and enemy battler
 * sprites more efficient to sort (if enabled), so that animations won't
 * interfere with and cover damage sprites, and to make sure damage sprites are
 * unaffected by screen tints in order to ensure the player will always have a
 * clear read on the information relaying sprites.
 *
 * ---
 *
 * Weather Displayed in Battle
 *
 * - Previously, weather has not been displayed in battle. This means that any
 * weather effects placed on the map do not transfer over to battle and causes
 * a huge disconnect for players. The Battle Core plugin will add weather
 * effects to match the map's weather conditions. Any changes made to weather
 * through event commands midway through battle will also be reflected.
 *
 * ---
 *
 * ============================================================================
 * Base Troops
 * ============================================================================
 *
 * Base Troops can be found, declared, and modified in the Plugin Parameters =>
 * Mechanics Settings => Base Troop ID's. All of the listed Troop ID's here
 * will have their page events replicated and placed under all other troops
 * found in the database.
 *
 * ---
 *
 * This means that if you have an event that runs on Turn 1 of a Base Troop,
 * then for every troop out there, that same event will also run on Turn 1,
 * as well. This is useful for those who wish to customize their battle system
 * further and to reduce the amount of work needed to copy/paste said event
 * pages into every database troop object manually.
 *
 * ---
 *
 * ============================================================================
 * Damage Styles
 * ============================================================================
 *
 * Damage Styles are a new feature added through the Battle Core plugin. When
 * using certain Battle Styles, you can completely ignore typing in the whole
 * damage formula inside the damage formula input box, and instead, insert
 * either a power amount or a multiplier depending on the Damage Style. The
 * plugin will then automatically calculate damage using that value factoring
 * in ATK, DEF, MAT, MDF values.
 *
 * ---
 *
 * Here is a list of the Damage Styles that come with this plugin by default.
 * You can add in your own and even edit them to your liking.
 * Or just remove them if you want.
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
 * Style          Use Formula As   PH/MA Disparity   Stat Scale   Damage Scale
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
 * Standard       Formula          No                Varies       Varies
 * ArmorScaling   Formula          No                Varies       Varies
 * CT             Multiplier       Yes               Low          Normal
 * D4             Multiplier       No                High         Normal
 * DQ             Multiplier       No                Low          Low
 * FF7            Power            Yes               Low          High
 * FF8            Power            Yes               Medium       Normal
 * FF9            Power            Yes               Low          Normal
 * FF10           Power            Yes               Medium       High
 * MK             Multiplier       No                Medium       Low
 * MOBA           Multiplier       No                Medium       Normal
 * PKMN           Power            No                Low          Normal
 *
 * Use the above chart to figure out which Damage Style best fits your game,
 * if you plan on using them.
 *
 * The 'Standard' style is the same as the 'Manual' formula input, except that
 * it allows for the support of <Armor Penetration> and <Armor Reduction>
 * notetags.
 *
 * The 'Armor Scaling' style allows you to type in the base damage calculation
 * without the need to type in any defending modifiers.
 *
 * NOTE: While these are based off the damage formulas found in other games,
 * not all of them are exact replicas. Many of them are adapted for use in
 * RPG Maker MZ since not all RPG's use the same set of parameters and not all
 * external multipliers function the same way as RPG Maker MZ.
 * 
 * ---
 *
 * Style:
 * - This is what the Damage Style is.
 *
 * Use Formula As:
 * - This is what you insert into the formula box.
 * - Formula: Type in the formula for the action just as you would normally.
 * - Multiplier: Type in the multiplier for the action.
 *     Use float values. This means 250% is typed out as 2.50
 * - Power: Type in the power constant for the action.
 *     Use whole numbers. Type in something like 16 for a power constant.
 * 
 * PH/MA Disparity:
 * - Is there a disparity between how Physical Attacks and Magical Attacks
 *   are calculated?
 * - If yes, then physical attacks and magical attacks will have different
 *   formulas used.
 * - If no, then physical attacks and magical attacks will share similar
 *   formulas for how they're calculated.
 *
 * Stat Scale:
 * - How much should stats scale throughout the game?
 * - Low: Keep them under 100 for the best results.
 * - Medium: Numbers work from low to mid 400's for best results.
 * - High: The numbers really shine once they're higher.
 *
 * Damage Scale:
 * - How much does damage vary depending on small parameter changes?
 * - Low: Very little increase from parameter changes.
 * - Normal: Damage scales close to proportionally with parameter changes.
 * - High: Damage can boost itself drastically with parameter changes.
 *
 * ---
 *
 * To determine what kind of parameters are used for the Damage Styles, they
 * will depend on two things: the action's 'Hit Type' (ie Physical Attack,
 * Magical Attack, and Certain Hit) and the action's 'Damage Type' (ie. Damage,
 * Recovery, or Drain).
 *
 * Certain Hit tends to use whichever value is higher: ATK or MAT, and then
 * ignores the target's defense values. Use Certain Hits for 'True Damage'.
 *
 * Use the chart below to figure out everything else:
 * 
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
 * Hit Type      Damage Type   Attacker Parameter   Defender Parameter
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
 * Physical      Damage        ATK                  DEF
 * Magical       Damage        MAT                  MDF
 * Certain Hit   Damage        Larger (ATK, MAT)    -Ignores-
 * Physical      Recover       DEF                  -Ignores-
 * Magical       Recover       MDF                  -Ignores-
 * Certain Hit   Recover       Larger (ATK, MAT)    -Ignores-
 * Physical      Drain         ATK                  DEF
 * Magical       Drain         MAT                  MDF
 * Certain Hit   Drain         Larger (ATK, MAT)    -Ignores-
 *
 * These can be modified within the Plugin Parameters in the individual
 * Damage Styles themselves.
 *
 * ---
 *
 * Skills and Items can use different Damage Styles from the setting you've
 * selected in the Plugin Parameters. They can be altered to have different
 * Damage Styles through the usage of a notetag:
 *
 * <Damage Style: name>
 *
 * This will use whichever style is found in the Plugin Parameters.
 *
 * If "Manual" is used, then no style will be used and all calculations will be
 * made strictly based off the formula found inside the formula box.
 *
 * ---
 *
 * ============================================================================
 * Notetags
 * ============================================================================
 *
 * The following are notetags that have been added through this plugin. These
 * notetags will not work with your game if this plugin is OFF or not present.
 * 
 * === HP Gauge-Related Notetags ===
 * 
 * The following notetags allow you to set whether or not HP Gauges can be
 * displayed by enemies regardless of Plugin Parameter settings.
 * 
 * ---
 *
 * <Show HP Gauge>
 *
 * - Used for: Enemy Notetags
 * - Will always show the HP Gauge for the enemy regardless of the defeat
 *   requirement setting.
 * - This does not bypass the player's Options preferences.
 * - This does not bypass disabling enemy HP Gauges as a whole.
 * 
 * ---
 *
 * <Hide HP Gauge>
 *
 * - Used for: Enemy Notetags
 * - Will always hide the HP Gauge for the enemy regardless of the defeat
 *   requirement setting.
 * - This does not bypass the player's Options preferences.
 * 
 * ---
 * 
 * <Battle UI Offset: +x, +y>
 * <Battle UI Offset: -x, -y>
 * 
 * <Battle UI Offset X: +x>
 * <Battle UI Offset X: -x>
 * 
 * <Battle UI Offset Y: +y>
 * <Battle UI Offset Y: -y>
 * 
 * - Used for: Actor and Enemy Notetags
 * - Adjusts the offset of HP Gauges and State Icons above the heads of actors
 *   and enemies.
 * - Replace 'x' with a number value that offsets the x coordinate.
 * - Negative x values offset left. Positive x values offset right.
 * - Replace 'y' with a number value that offsets the y coordinate.
 * - Negative y values offset up. Positive x values offset down.
 * 
 * ---
 *
 * === Animation-Related Notetags ===
 *
 * The following notetags allow you to set animations to play at certain
 * instances and/or conditions.
 *
 * ---
 *
 * <Slip Animation: x>
 *
 * - Requires VisuMZ_0_CoreEngine!
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - During the phase at which the user regenerates HP, MP, or TP, this
 *   animation will play as long as the user is alive and visible.
 * - Replace 'x' with a number value representing the Animation ID to play.
 *
 * ---
 *
 * <Cast Animation: x>
 *
 * - Used for: Skill Notetags
 * - Plays a battle animation at the start of the skill.
 * - Replace 'x' with a number value representing the Animation ID to play.
 *
 * ---
 *
 * <Attack Animation: x>
 *
 * - Used for: Enemy Notetags
 * - Gives an enemy an attack animation to play for its basic attack.
 * - Replace 'x' with a number value representing the Animation ID to play.
 *
 * ---
 *
 * === Battleback-Related Notetags ===
 *
 * You can apply these notetags to have some control over the battlebacks that
 * appear in different regions of the map for random or touch encounters.
 *
 * ---
 *
 * <Region x Battleback1: filename>
 * <Region x Battleback2: filename>
 * 
 * - Used for: Map Notetags
 * - If the player starts a battle while standing on 'x' region, then the
 *   'filename' battleback will be used.
 * - Replace 'x' with a number representing the region ID you wish to use.
 * - Replace 'filename' with the filename of the graphic to use. Do not insert
 *   any extensions. This means the file 'Castle1.png' will be only inserted
 *   as 'Castle1' without the '.png' at the end.
 * - *NOTE: This will override any specified battleback settings.
 *
 * ---
 *
 * === Battle Command-Related Notetags ===
 *
 * You can use notetags to change how the battle commands of playable
 * characters appear in battle as well as whether or not they can be used.
 *
 * ---
 *
 * <Seal Attack>
 * <Seal Guard>
 * <Seal Item>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Prevents specific battle commands from being able to be used.
 *
 * ---
 *
 * <Battle Commands>
 *  Attack
 *  Skills
 *  SType: x
 *  SType: name
 *  All Skills
 *  Skill: x
 *  Skill: name
 *  Guard
 *  Item
 *  Party
 *  Escape
 *  Auto Battle
 *  Combat Log
 * </Battle Commands>
 *
 * - Used for: Class Notetags
 * - Changes which commands appear in the Actor Command Window in battle.
 *   If this notetag is not used, then the default commands determined in
 *   Plugin Parameters => Actor Command Window => Command List will be used.
 * - Add/remove/modify entries as needed.
 *
 * - Attack 
 *   - Adds the basic attack command.
 * 
 * - Skills
 *   - Displays all the skill types available to the actor.
 * 
 * - SType: x
 * - Stype: name
 *   - Adds in a specific skill type.
 *   - Replace 'x' with the ID of the skill type.
 *   - Replace 'name' with the name of the skill type (without text codes).
 *
 * - All Skills
 *   - Adds all usable battle skills as individual actions.
 * 
 * - Skill: x
 * - Skill: name
 *   - Adds in a specific skill as a usable action.
 *   - Replace 'x' with the ID of the skill.
 *   - Replace 'name' with the name of the skill.
 * 
 * - Guard
 *   - Adds the basic guard command.
 * 
 * - Item
 *   - Adds the basic item command.
 *
 * - Party
 *   - Requires VisuMZ_2_PartySystem.
 *   - Allows this actor to switch out with a different party member.
 * 
 * - Escape
 *   - Adds the escape command.
 * 
 * - Auto Battle
 *   - Adds the auto battle command.
 *
 * Example:
 *
 * <Battle Commands>
 *  Attack
 *  Skill: Heal
 *  Skills
 *  Guard
 *  Item
 *  Escape
 * </Battle Commands>
 *
 * ---
 *
 * <Command Text: x>
 *
 * - Used for: Skill Notetags
 * - When a skill is used in a <Battle Commands> notetag set, you can change
 *   the skill name text that appears to something else.
 * - Replace 'x' with the skill's name you want to shown in the Actor Battle
 *   Command window.
 * - Recommended Usage: Shorten skill names that are otherwise too big to fit
 *   inside of the Actor Battle Command window.
 *
 * ---
 *
 * <Command Icon: x>
 *
 * - Used for: Skill Notetags
 * - When a skill is used in a <Battle Commands> notetag set, you can change
 *   the skill icon that appears to something else.
 * - Replace 'x' with the ID of icon you want shown in the Actor Battle Command
 *   window to represent the skill.
 *
 * ---
 * 
 * <Command Show Switch: x>
 * 
 * <Command Show All Switches: x,x,x>
 * <Command Show Any Switches: x,x,x>
 * 
 * - Used for: Skill Notetags
 * - Determines if a battle command is visible or not through switches.
 * - Replace 'x' with the switch ID to determine the skill's visibility.
 * - If 'All' notetag variant is used, item will be hidden until all
 *   switches are ON. Then, it would be shown.
 * - If 'Any' notetag variant is used, item will be shown if any of the
 *   switches are ON. Otherwise, it would be hidden.
 * - This can be applied to Attack and Guard commands, too.
 * 
 * ---
 * 
 * <Command Hide Switch: x>
 * 
 * <Command Hide All Switches: x,x,x>
 * <Command Hide Any Switches: x,x,x>
 * 
 * - Used for: Skill Notetags
 * - Determines if a battle command is visible or not through switches.
 * - Replace 'x' with the switch ID to determine the skill's visibility.
 * - If 'All' notetag variant is used, item will be shown until all
 *   switches are ON. Then, it would be hidden.
 * - If 'Any' notetag variant is used, item will be hidden if any of the
 *   switches are ON. Otherwise, it would be shown.
 * - This can be applied to Attack and Guard commands, too.
 * 
 * ---
 * 
 * <Battle Portrait: filename>
 *
 * - Used for: Actor
 * - This is used with the "Portrait" Battle Layout.
 * - Sets the battle portrait image for the actor to 'filename'.
 * - Replace 'filename' with a picture found within your game project's
 *   img/pictures/ folder. Filenames are case sensitive. Leave out the filename
 *   extension from the notetag.
 * - This will override any menu images used for battle only.
 * 
 * ---
 * 
 * <Battle Portrait Offset: +x, +y>
 * <Battle Portrait Offset: -x, -y>
 * 
 * <Battle Portrait Offset X: +x>
 * <Battle Portrait Offset X: -x>
 * 
 * <Battle Portrait Offset Y: +y>
 * <Battle Portrait Offset Y: -y>
 *
 * - Used for: Actor
 * - This is used with the "Portrait" and "Border" Battle Layouts.
 * - Offsets the X and Y coordinates for the battle portrait.
 * - Replace 'x' with a number value that offsets the x coordinate.
 * - Negative x values offset left. Positive x values offset right.
 * - Replace 'y' with a number value that offsets the y coordinate.
 * - Negative y values offset up. Positive x values offset down.
 * 
 * ---
 * 
 * === JavaScript Notetag: Battle Command-Related ===
 *
 * The following are notetags made for users with JavaScript knowledge to
 * determine if skill-based battle commands are visible or hidden.
 * 
 * ---
 * 
 * <JS Command Visible>
 *  code
 *  code
 *  visible = code;
 * </JS Command Visible>
 * 
 * - Used for: Skill Notetags
 * - The 'visible' variable is the final returned variable to determine the
 *   skill's visibility in the Battle Command Window.
 * - Replace 'code' with JavaScript code to determine the skill's visibility in
 *   the Battle Command Window.
 * - The 'user' variable represents the user who will perform the skill.
 * - The 'skill' variable represents the skill to be used.
 * 
 * ---
 *
 * === Targeting-Related Notetags ===
 *
 * The following notetags are related to the targeting aspect of skills and
 * items and may adjust the scope of how certain skills/items work.
 *
 * ---
 *
 * <Always Hit>
 *
 * <Always Hit Rate: x%>
 *
 * - Used for: Skill, Item Notetags
 * - Causes the action to always hit or to always have a hit rate of exactly
 *   the marked x%.
 * - Replace 'x' with a number value representing the hit success percentage.
 *
 * ---
 *
 * <Repeat Hits: x>
 *
 * - Used for: Skill, Item Notetags
 * - Changes the number of hits the action will produce.
 * - Replace 'x' with a number value representing the number of hits to incur.
 *
 * ---
 *
 * <Target: x Random Any>
 *
 * - Used for: Skill, Item Notetags
 * - Makes the skill pick 'x' random targets when used.
 * - Targets can be both actors and enemies.
 * - Replace 'x' with a number value representing the number of random targets.
 *
 * ---
 *
 * <Target: x Random Enemies>
 *
 * - Used for: Skill, Item Notetags
 * - Makes the skill pick 'x' random targets when used.
 * - Targets are only enemies.
 * - Replace 'x' with a number value representing the number of random targets.
 *
 * ---
 *
 * <Target: x Random Allies>
 *
 * - Used for: Skill, Item Notetags
 * - Makes the skill pick 'x' random targets when used.
 * - Targets are only actors.
 * - Replace 'x' with a number value representing the number of random targets.
 *
 * ---
 *
 * <Target: All Allies But User>
 *
 * - Used for: Skill, Item Notetags
 * - Targets all allies with the exception of the user.
 *
 * ---
 *
 * === JavaScript Notetag: Targeting-Related ===
 *
 * ---
 * 
 * <JS Targets>
 *  code
 *  code
 *  targets = [code];
 * </JS Targets>
 *
 * - Used for: Skill, Item Notetags
 * - The 'targets' variable is an array that is returned to be used as a
 *   container for all the valid action targets.
 * - Replace 'code' with JavaScript code to determine valid targets.
 *
 * ---
 *
 * === Damage-Related Notetags ===
 *
 * ---
 *
 * <Damage Style: name>
 *
 * - Used for: Skill, Item Notetags
 * - Replace 'name' with a Damage Style name to change the way calculations are
 *   made using the damage formula input box.
 * - Names can be found in Plugin Parameters => Damage Settings => Style List
 *
 * ---
 *
 * <Armor Reduction: x>
 * <Armor Reduction: x%>
 * - Used for: Actor, Class, Skill, Item, Weapon, Armor, Enemy, State Notetags
 * - If used on skills and/or items, sets the current skill/item's armor
 *   reduction properties to 'x' and/or 'x%'.
 * - If used on trait objects, adds 'x' and/or 'x%' armor reduction properties
 *   when calculating one's own armor.
 * - This applies to physical attacks.
 * - Use the 'x' notetag variant to determine a flat reduction value.
 * - Use the 'x%' notetag variant to determine a percentile reduction value.
 *
 * ---
 *
 * <Armor Penetration: x>
 * <Armor Penetration: x%>
 * - Used for: Actor, Class, Skill, Item, Weapon, Armor, Enemy, State Notetags
 * - If used on skills and/or items, sets the current skill/item's armor
 *   penetration properties to 'x' and/or 'x%'.
 * - If used on trait objects, adds 'x' and/or 'x%' armor penetration
 *   properties when calculating a target's armor.
 * - This applies to physical attacks.
 * - Use the 'x' notetag variant to determine a flat penetration value.
 * - Use the 'x%' notetag variant to determine a percentile penetration value.
 *
 * ---
 *
 * <Magic Reduction: x>
 * <Magic Reduction: x%>
 * - Used for: Actor, Class, Skill, Item, Weapon, Armor, Enemy, State Notetags
 * - If used on skills and/or items, sets the current skill/item's armor
 *   reduction properties to 'x' and/or 'x%'.
 * - If used on trait objects, adds 'x' and/or 'x%' armor reduction properties
 *   when calculating one's own armor.
 * - This applies to magical attacks.
 * - Use the 'x' notetag variant to determine a flat reduction value.
 * - Use the 'x%' notetag variant to determine a percentile reduction value.
 *
 * ---
 *
 * <Magic Penetration: x>
 * <Magic Penetration: x%>
 * - Used for: Actor, Class, Skill, Item, Weapon, Armor, Enemy, State Notetags
 * - If used on skills and/or items, sets the current skill/item's armor
 *   penetration properties to 'x' and/or 'x%'.
 * - If used on trait objects, adds 'x' and/or 'x%' armor penetration
 *   properties when calculating a target's armor.
 * - This applies to magical attacks.
 * - Use the 'x' notetag variant to determine a flat penetration value.
 * - Use the 'x%' notetag variant to determine a percentile penetration value.
 *
 * ---
 *
 * <Bypass Damage Cap>
 * 
 * - Used for: Actor, Class, Skill, Item, Weapon, Armor, Enemy, State Notetags
 * - If used on skills and/or items, this will cause the action to never have
 *   its damage capped.
 * - If used on trait objects, this will cause the affected unit to never have
 *   its damage capped.
 *
 * ---
 *
 * <Damage Cap: x>
 *
 * - Used for: Actor, Class, Skill, Item, Weapon, Armor, Enemy, State Notetags
 * - If used on skills and/or items, this will declare the hard damage cap to
 *   be the 'x' value.
 * - If used on trait objects, this will raise the affect unit's hard damage
 *   cap to 'x' value. If another trait object has a higher value, use that
 *   value instead.
 *
 * ---
 *
 * <Bypass Soft Damage Cap>
 *
 * - Used for: Actor, Class, Skill, Item, Weapon, Armor, Enemy, State Notetags
 * - If used on skills and/or items, this will cause the action to never have
 *   its damage scaled downward to the soft cap.
 * - If used on trait objects, this will cause the affected unit to never have
 *   its damage scaled downward to the soft cap.
 *
 * ---
 *
 * <Soft Damage Cap: +x%>
 * <Soft Damage Cap: -x%>
 *
 * - Used for: Actor, Class, Skill, Item, Weapon, Armor, Enemy, State Notetags
 * - If used on skills and/or items, this will increase/decrease the action's
 *   soft cap by x% where 'x' is a percentage value representing the increment
 *   changed by the hard cap value.
 * - If used on trait objects, this will raise the affect unit's soft damage
 *   limit by x% where 'x' is a percentage value representing the increment
 *   changed by the hard cap value.
 *
 * ---
 *
 * <Unblockable>
 *
 * - Used for: Skill, Item Notetags
 * - Using "Guard" against this skill will not reduce any damage.
 *
 * ---
 *
 * === Critical-Related Notetags ===
 *
 * The following notetags affect skill and item critical hit rates and the
 * critical damage multiplier.
 *
 * ---
 *
 * <Always Critical>
 *
 * - Used for: Skill, Item Notetags
 * - This skill/item will always land a critical hit regardless of the
 *   user's CRI parameter value.
 *
 * ---
 *
 * <Set Critical Rate: x%>
 *
 * - Used for: Skill, Item Notetags
 * - This skill/item will always have a x% change to land a critical hit
 *   regardless of user's CRI parameter value.
 * - Replace 'x' with a percerntage value representing the success rate.
 *
 * ---
 *
 * <Modify Critical Rate: x%>
 * <Modify Critical Rate: +x%>
 * <Modify Critical Rate: -x%>
 *
 * - Used for: Skill, Item Notetags
 * - Modifies the user's CRI parameter calculation for this skill/item.
 * - The 'x%' notetag variant will multiply the user's CRI parameter value
 *   for this skill/item.
 * - The '+x%' and '-x%' notetag variants will incremenetally increase/decrease
 *   the user's CRI parameter value for this skill/item.
 *
 * ---
 *
 * <Modify Critical Multiplier: x%>
 * <Modify Critical Multiplier: +x%>
 * <Modify Critical Multiplier: -x%>
 *
 * - Used for: Skill, Item Notetags
 * - These notetags determine the damage multiplier when a critical hit lands.
 * - The 'x%' notetag variant multiply the multiplier to that exact percentage.
 * - The '+x%' and '-x%' notetag variants will change the multiplier with an
 *   incremenetal rate for this skill/item.
 *
 * ---
 *
 * <Modify Critical Bonus Damage: x%>
 * <Modify Critical Bonus Damage: +x%>
 * <Modify Critical Bonus Damage: -x%>
 *
 * - Used for: Skill, Item Notetags
 * - These notetags determine the bonus damage added when a critical hit lands.
 * - The 'x%' notetag variant multiply the damage to that exact percentage.
 * - The '+x%' and '-x%' notetag variants will change the bonus damage with an
 *   incremenetal rate for this skill/item.
 *
 * ---
 *
 * === JavaScript Notetags: Critical-Related ===
 *
 * The following are notetags made for users with JavaScript knowledge to
 * determine how critical hit-related aspects are calculated.
 *
 * ---
 *
 * <JS Critical Rate>
 *  code
 *  code
 *  rate = code;
 * </JS Critical Rate>
 *
 * - Used for: Skill, Item Notetags
 * - The 'rate' variable is the final returned amount to determine the
 *   critical hit success rate.
 * - Replace 'code' with JavaScript code to determine the final 'rate' to be
 *   returned as the critical hit success rate.
 * - The 'user' variable represents the one using the skill/item.
 * - The 'target' variable represents the one receiving the skill/item hit.
 *
 * ---
 *
 * <JS Critical Damage>
 *  code
 *  code
 *  multiplier = code;
 *  bonusDamage = code;
 * </JS Critical Damage>
 *
 * - Used for: Skill, Item Notetags
 * - The 'multiplier' variable is returned later and used as the damage
 *   multiplier used to amplify the critical damage amount.
 * - The 'bonusDamage' variable is returned later and used as extra added
 *   damage for the critical damage amount.
 * - Replace 'code' with JavaScript code to determine how the 'multiplier' and
 *   'bonusDamage' variables are calculated.
 * - The 'user' variable represents the one using the skill/item.
 * - The 'target' variable represents the one receiving the skill/item hit.
 *
 * ---
 *
 * === Action Sequence-Related Notetags ===
 *
 * Action Sequences allow you full control over how a skill and/or item plays
 * through its course. These notetags give you control over various aspects of
 * those Action Sequences. More information is found in the Action Sequences
 * help section.
 *
 * ---
 *
 * <Custom Action Sequence>
 *
 * - Used for: Skill, Item Notetags
 * - Removes all automated Action Sequence parts from the skill.
 * - Everything Action Sequence-related will be done by Common Events.
 * - Insert Common Event(s) into the skill/item's effects list to make use of
 *   the Custom Action Sequences.
 * - This will prevent common events from loading in the Item Scene and Skill
 *   Scene when used outside of battle.
 *
 * ---
 * 
 * <Auto Action Sequence>
 * 
 * - Used for: Skill, Item Notetags
 * - If the Action Sequence Plugin Parameter "Auto Notetag" is enabled, this
 *   plugin will prevent custom action sequences from happening for the skill
 *   or item, and instead, use an Automatic Action Sequence instead.
 * - Ignore this if you have "Auto Notetag" disabled or set to false.
 * 
 * ---
 * 
 * <Common Event: name>
 *
 * - Used for: Skill, Item Notetags
 * - Battle only: calls forth a Common Event of a matching name.
 * - Replace 'name' with the name of a Common Event to call from when this
 *   skill/item is used in battle.
 *   - Remove any \I[x] in the name.
 * - Insert multiple notetags to call multiple Common Events in succession.
 * - This will occur after any Common Event Trait Effects for the skill/item's
 *   database entry.
 * - This is primarily used for users who are reorganizing around their Common
 *   Events and would still like to have their skills/items perform the correct
 *   Action Sequences in case the ID's are different.
 * 
 * ---
 *
 * <Display Icon: x>
 * <Display Text: string>
 *
 * - Used for: Skill, Item Notetags
 * - When displaying the skill/item name in the Action Sequence, determine the
 *   icon and/or text displayed.
 * - Replace 'x' with a number value representing the icon ID to be displayed.
 * - Replace 'string' with a text value representing the displayed name.
 *
 * ---
 *
 * === Animated Sideview Battler-Related Notetags ===
 *
 * Enemies can use Animated Sideview Actor graphics thanks to this plugin.
 * These notetags give you control over that aspect. Some of these also affect
 * actors in addition to enemies.
 *
 * ---
 *
 * <Sideview Battler: filename>
 *
 * <Sideview Battlers>
 *  filename: weight
 *  filename: weight
 *  filename: weight
 * </Sideview Battlers>
 *
 * - Used for: Enemy Notetags
 * - Replaces the enemy's battler graphic with an animated Sideview Actor
 *   graphic found in the img/sv_actors/ folder.
 * - Replace 'filename' with the filename of the graphic to use. Do not insert
 *   any extensions. This means the file 'Actor1_1.png' will be only inserted
 *   as 'Actor1_1' without the '.png' at the end.
 * - If the multiple notetag vaiant is used, then a random filename is selected
 *   from the list upon the enemy's creation.
 * - Replace 'weight' with a number value representing how often the 'filename'
 *   would come up. The higher the weight, the more often. You may omit this
 *   and the colon(:) and just type in the 'filename' instead.
 * - Add/remove lines as you see fit.
 *
 * Example:
 *
 * <Sideview Battlers>
 *  Actor1_1: 25
 *  Actor1_3: 10
 *  Actor1_5
 *  Actor1_7
 * </Sideview Battlers>
 *
 * ---
 *
 * <Sideview Anchor: x, y>
 *
 * - Used for: Actor, Enemy Notetags
 * - Sets the sprite anchor positions for the sideview sprite.
 * - Replace 'x' and 'y' with numbers depicting where the anchors should be for
 *   the sideview sprite.
 * - By default, the x and y anchors are 0.5 and 1.0.
 *
 * ---
 * 
 * <Sideview Home Offset: +x, +y>
 * <Sideview Home Offset: -x, -y>
 * 
 * - Used for: Actor, Class, Weapon, Armor, State Notetags
 * - Offsets the sideview actor sprite's home position by +/-x, +/-y.
 * - Replace 'x' and 'y' with numbers depicting how much to offset each of the
 *   coordinates by. For '0' values, use +0 or -0.
 * - This notetag will not work if you remove it from the JavaScript code in
 *   Plugin Parameters > Actor > JS:  Home Position
 * 
 * ---
 * 
 * <Sideview Weapon Offset: +x, +y>
 * <Sideview Weapon Offset: -x, -y>
 * 
 * - Used for: Actor, Class, Weapon, Armor, Enemy State Notetags
 * - Offsets the sideview weapon sprite's position by +/-x, +/-y.
 * - Replace 'x' and 'y' with numbers depicting how much to offset each of the
 *   coordinates by. For '0' values, use +0 or -0.
 * 
 * ---
 *
 * <Sideview Show Shadow>
 * <Sideview Hide Shadow>
 *
 * - Used for: Actor, Enemy Notetags
 * - Sets it so the sideview battler's shadow will be visible or hidden.
 *
 * ---
 *
 * <Sideview Collapse>
 * <Sideview No Collapse>
 *
 * - Used for: Enemy Notetags
 * - Either shows the collapse graphic or does not show the collapse graphic.
 * - Collapse graphic means the enemy will 'fade away' once it's defeated.
 * - No collapse graphic means the enemy's corpse will remain on the screen.
 *
 * ---
 *
 * <Sideview Idle Motion: name>
 *
 * <Sideview Idle Motions>
 *  name: weight
 *  name: weight
 *  name: weight
 * </Sideview Idle Motions>
 *
 * - Used for: Enemy Notetags
 * - Changes the default idle motion for the enemy.
 * - Replace 'name' with any of the following motion names:
 *   - 'walk', 'wait', 'chant', 'guard', 'damage', 'evade', 'thrust', 'swing',
 *     'missile', 'skill', 'spell', 'item', 'escape', 'victory', 'dying',
 *     'abnormal', 'sleep', 'dead'
 * - If the multiple notetag vaiant is used, then a random motion name is
 *   selected from the list upon the enemy's creation.
 * - Replace 'weight' with a number value representing how often the 'name'
 *   would come up. The higher the weight, the more often. You may omit this
 *   and the colon(:) and just type in the 'name' instead.
 * - Add/remove lines as you see fit.
 *
 * Example:
 *
 * <Sideview Idle Motions>
 *  walk: 25
 *  wait: 50
 *  guard
 *  victory
 *  abnormal
 * </Sideview Idle Motions>
 *
 * ---
 *
 * <Sideview Size: width, height>
 *
 * - Used for: Enemy Notetags
 * - When using a sideview battler, its width and height will default to the
 *   setting made in Plugin Parameters => Enemy Settings => Size: Width/Height.
 * - This notetag lets you change that value to something else.
 * - Replace 'width' and 'height' with numbers representing how many pixels
 *   wide/tall the sprite will be treated as.
 *
 * ---
 *
 * <Sideview Weapon: weapontype>
 *
 * <Sideview Weapons>
 *  weapontype: weight
 *  weapontype: weight
 *  weapontype: weight
 * </Sideview Weapons>
 *
 * - Used for: Enemy Notetags
 * - Give your sideview enemies weapons to use.
 * - Replace 'weapontype' with the name of the weapon type found under the
 *   Database => Types => Weapon Types list (without text codes).
 * - If the multiple notetag vaiant is used, then a random weapon type is
 *   selected from the list upon the enemy's creation.
 * - Replace 'weight' with a number value representing how often the weapontype
 *   would come up. The higher the weight, the more often. You may omit this
 *   and the colon(:) and just type in the 'weapontype' instead.
 * - Add/remove lines as you see fit.
 *
 * Example:
 *
 * <Sideview Weapons>
 *  Dagger: 25
 *  Sword: 25
 *  Axe
 * </Sideview Weapons>
 *
 * ---
 *
 * <traitname Sideview Battler: filename>
 *
 * <traitname Sideview Battlers>
 *  filename: weight
 *  filename: weight
 *  filename: weight
 * </traitname Sideview Battlers>
 *
 * - Used for: Enemy Notetags
 * - Requires VisuMZ_1_ElementStatusCore
 * - Allows certain Trait Sets to cause battlers to have a unique appearance.
 * - Replace 'filename' with the filename of the graphic to use. Do not insert
 *   any extensions. This means the file 'Actor1_1.png' will be only inserted
 *   as 'Actor1_1' without the '.png' at the end.
 * - If the multiple notetag vaiant is used, then a random filename is selected
 *   from the list upon the enemy's creation.
 * - Replace 'weight' with a number value representing how often the 'filename'
 *   would come up. The higher the weight, the more often. You may omit this
 *   and the colon(:) and just type in the 'filename' instead.
 * - Add/remove lines as you see fit.
 *
 * Examples:
 *
 * <Male Sideview Battlers>
 *  Actor1_1: 25
 *  Actor1_3: 10
 *  Actor1_5
 *  Actor1_7
 * </Male Sideview Battlers>
 *
 * <Female Sideview Battlers>
 *  Actor1_2: 25
 *  Actor1_4: 10
 *  Actor1_6
 *  Actor1_8
 * </Female Sideview Battlers>
 *
 * ---
 *
 * <traitname Sideview Idle Motion: name>
 *
 * <traitname Sideview Idle Motions>
 *  name: weight
 *  name: weight
 *  name: weight
 * </traitname Sideview Idle Motions>
 *
 * - Used for: Enemy Notetags
 * - Requires VisuMZ_1_ElementStatusCore
 * - Allows certain Trait Sets to cause battlers to have unique idle motions.
 * - Replace 'name' with any of the following motion names:
 *   - 'walk', 'wait', 'chant', 'guard', 'damage', 'evade', 'thrust', 'swing',
 *     'missile', 'skill', 'spell', 'item', 'escape', 'victory', 'dying',
 *     'abnormal', 'sleep', 'dead'
 * - If the multiple notetag vaiant is used, then a random motion name is
 *   selected from the list upon the enemy's creation.
 * - Replace 'weight' with a number value representing how often the 'name'
 *   would come up. The higher the weight, the more often. You may omit this
 *   and the colon(:) and just type in the 'name' instead.
 * - Add/remove lines as you see fit.
 *
 * Examples:
 *
 * <Jolly Sideview Idle Motions>
 *  wait: 25
 *  victory: 10
 *  walk
 * </Jolly Sideview Idle Motions>
 *
 * <Serious Sideview Idle Motions>
 *  walk: 25
 *  guard: 10
 *  wait
 * </Jolly Sideview Idle Motions>
 *
 * ---
 *
 * <traitname Sideview Weapon: weapontype>
 *
 * <traitname Sideview Weapons>
 *  weapontype: weight
 *  weapontype: weight
 *  weapontype: weight
 * </traitname Sideview Weapons>
 *
 * - Used for: Enemy Notetags
 * - Requires VisuMZ_1_ElementStatusCore
 * - Allows certain Trait Sets to cause battlers to have unique weapons.
 * - Replace 'weapontype' with the name of the weapon type found under the
 *   Database => Types => Weapon Types list (without text codes).
 * - If the multiple notetag vaiant is used, then a random weapon type is
 *   selected from the list upon the enemy's creation.
 * - Replace 'weight' with a number value representing how often the weapontype
 *   would come up. The higher the weight, the more often. You may omit this
 *   and the colon(:) and just type in the 'weapontype' instead.
 * - Add/remove lines as you see fit.
 *
 * Examples:
 *
 * <Male Sideview Weapons>
 *  Dagger: 25
 *  Sword: 25
 *  Axe
 * </Male Sideview Weapons>
 *
 * <Female Sideview Weapons>
 *  Dagger: 25
 *  Spear: 25
 *  Cane
 * </Female Sideview Weapons>
 *
 * ---
 *
 * === Enemy-Related Notetags ===
 *
 * ---
 *
 * <Battler Sprite Cannot Move>
 *
 * - Used for: Enemy Notetags
 * - Prevents the enemy from being able to move, jump, and/or float due to
 *   Action Sequences. Useful for rooted enemies.
 *
 * ---
 * 
 * <Battler Sprite Grounded>
 *
 * - Used for: Enemy Notetags
 * - Prevents the enemy from being able to jumping and/or floating due to
 *   Action Sequences but still able to move. Useful for rooted enemies.
 * 
 * ---
 *
 * <Swap Enemies>
 *  name: weight
 *  name: weight
 *  name: weight
 * </Swap Enemies>
 *
 * - Used for: Enemy Notetags
 * - Causes this enemy database object to function as a randomizer for any of
 *   the listed enemies inside the notetag. When the enemy is loaded into the
 *   battle scene, the enemy is immediately replaced with one of the enemies
 *   listed. The randomization is based off the 'weight' given to each of the
 *   enemy 'names'.
 * - Replace 'name' with the database enemy of the enemy you wish to replace
 *   the enemy with.
 * - Replace 'weight' with a number value representing how often the 'name'
 *   would come up. The higher the weight, the more often. You may omit this
 *   and the colon(:) and just type in the 'name' instead.
 * - Add/remove lines as you see fit.
 *
 * Example:
 *
 * <Swap Enemies>
 *  Bat: 50
 *  Slime: 25
 *  Orc
 *  Minotaur
 * </Swap Enemies>
 *
 * ---
 *
 * === JavaScript Notetags: Mechanics-Related ===
 *
 * These JavaScript notetags allow you to run code at specific instances during
 * battle provided that the unit has that code associated with them in a trait
 * object (actor, class, weapon, armor, enemy, or state). How you use these is
 * entirely up to you and will depend on your ability to understand the code
 * used and driven for each case.
 *
 * ---
 *
 * <JS Pre-Start Battle>
 *  code
 *  code
 *  code
 * </JS Pre-Start Battle>
 *
 * <JS Post-Start Battle>
 *  code
 *  code
 *  code
 * </JS Post-Start Battle>
 * 
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs JavaScript code at the start of battle aimed at the function:
 *   BattleManager.startBattle()
 *   - 'Pre' runs before the function runs.
 *   - 'Post' runs after the function runs.
 * - Replace 'code' with JavaScript code to run desired effects.
 * - The 'user' variable represents the one affected by the trait object.
 *
 * ---
 *
 * <JS Pre-Start Turn>
 *  code
 *  code
 *  code
 * </JS Pre-Start Turn>
 *
 * <JS Post-Start Turn>
 *  code
 *  code
 *  code
 * </JS Post-Start Turn>
 * 
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs JavaScript code at the start of a turn aimed at the function:
 *   BattleManager.startTurn()
 *   - 'Pre' runs before the function runs.
 *   - 'Post' runs after the function runs.
 * - Replace 'code' with JavaScript code to run desired effects.
 * - The 'user' variable represents the one affected by the trait object.
 *
 * ---
 *
 * <JS Pre-Start Action>
 *  code
 *  code
 *  code
 * </JS Pre-Start Action>
 *
 * <JS Post-Start Action>
 *  code
 *  code
 *  code
 * </JS Post-Start Action>
 * 
 * - Used for: Actor, Class, Skill, Item, Weapon, Armor, Enemy, State Notetags
 * - Runs JavaScript code at the start of an action aimed at the function:
 *   BattleManager.startAction()
 *   - 'Pre' runs before the function runs.
 *   - 'Post' runs after the function runs.
 * - If used on skills and/or items, this will only apply to the skill/item
 *   being used and does not affect other skills and items.
 * - If used on trait objects, this will apply to any skills/items used as long
 *   as the unit affected by the trait object has access to the trait object.
 * - Replace 'code' with JavaScript code to run desired effects.
 * - The 'user' variable represents the one affected by the trait object.
 *
 * ---
 *
 * <JS Pre-Apply>
 *  code
 *  code
 *  code
 * </JS Pre-Apply>
 * 
 * - Used for: Skill, Item Notetags
 * - Runs JavaScript code at the start of an action hit aimed at the function:
 *   Game_Action.prototype.apply()
 *   - 'Pre' runs before the function runs.
 * - If used on skills and/or items, this will only apply to the skill/item
 *   being used and does not affect other skills and items.
 * - Replace 'code' with JavaScript code to run desired effects.
 * - The 'user' variable represents the one using the skill/item.
 * - The 'target' variable represents the one receiving the skill/item hit.
 *
 * ---
 *
 * <JS Pre-Apply as User>
 *  code
 *  code
 *  code
 * </JS Pre-Apply as User>
 *
 * <JS Pre-Apply as Target>
 *  code
 *  code
 *  code
 * </JS Pre-Apply as Target>
 * 
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs JavaScript code at the start of an action hit aimed at the function:
 *   Game_Action.prototype.apply()
 *   - 'Pre' runs before the function runs.
 * - If used on trait objects, this will apply to any skills/items used as long
 *   as the unit affected by the trait object has access to the trait object.
 * - If the 'as User' notetag variant is used, this code will be run as a
 *   response to the action from the action user end.
 * - If the 'as Target' notetag variant is used, this code will be run as a
 *   response to the action from the action target end.
 * - Replace 'code' with JavaScript code to run desired effects.
 * - The 'user' variable represents the one using the skill/item.
 * - The 'target' variable represents the one receiving the skill/item hit.
 *
 * ---
 *
 * <JS Pre-Damage>
 *  code
 *  code
 *  code
 * </JS Pre-Damage>
 * 
 * - Used for: Skill, Item Notetags
 * - Runs JavaScript code before damage is dealt aimed at the function:
 *   Game_Action.prototype.executeDamage()
 *   - 'Pre' runs before the function runs.
 * - If used on skills and/or items, this will only apply to the skill/item
 *   being used and does not affect other skills and items.
 * - Replace 'code' with JavaScript code to run desired effects.
 * - The 'user' variable represents the one using the skill/item.
 * - The 'target' variable represents the one receiving the skill/item hit.
 *
 * ---
 *
 * <JS Pre-Damage as User>
 *  code
 *  code
 *  code
 * </JS Pre-Damage as User>
 *
 * <JS Pre-Damage as Target>
 *  code
 *  code
 *  code
 * </JS Pre-Damage as Target>
 * 
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs JavaScript code before damage is dealt aimed at the function:
 *   Game_Action.prototype.executeDamage()
 *   - 'Pre' runs before the function runs.
 * - If used on trait objects, this will apply to any skills/items used as long
 *   as the unit affected by the trait object has access to the trait object.
 * - If the 'as User' notetag variant is used, this code will be run as a
 *   response to the action from the action user end.
 * - If the 'as Target' notetag variant is used, this code will be run as a
 *   response to the action from the action target end.
 * - Replace 'code' with JavaScript code to run desired effects.
 * - The 'user' variable represents the one using the skill/item.
 * - The 'target' variable represents the one receiving the skill/item hit.
 *
 * ---
 *
 * <JS Post-Damage>
 *  code
 *  code
 *  code
 * </JS Post-Damage>
 * 
 * - Used for: Skill, Item Notetags
 * - Runs JavaScript code after damage is dealt aimed at the function:
 *   Game_Action.prototype.executeDamage()
 *   - 'Post' runs after the function runs.
 * - If used on skills and/or items, this will only apply to the skill/item
 *   being used and does not affect other skills and items.
 * - Replace 'code' with JavaScript code to run desired effects.
 * - The 'user' variable represents the one using the skill/item.
 * - The 'target' variable represents the one receiving the skill/item hit.
 *
 * ---
 *
 * <JS Post-Damage as User>
 *  code
 *  code
 *  code
 * </JS Post-Damage as User>
 *
 * <JS Post-Damage as Target>
 *  code
 *  code
 *  code
 * </JS Post-Damage as Target>
 * 
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs JavaScript code after damage is dealt aimed at the function:
 *   Game_Action.prototype.executeDamage()
 *   - 'Post' runs after the function runs.
 * - If used on trait objects, this will apply to any skills/items used as long
 *   as the unit affected by the trait object has access to the trait object.
 * - If the 'as User' notetag variant is used, this code will be run as a
 *   response to the action from the action user end.
 * - If the 'as Target' notetag variant is used, this code will be run as a
 *   response to the action from the action target end.
 * - Replace 'code' with JavaScript code to run desired effects.
 * - The 'user' variable represents the one using the skill/item.
 * - The 'target' variable represents the one receiving the skill/item hit.
 *
 * ---
 *
 * <JS Post-Apply>
 *  code
 *  code
 *  code
 * </JS Post-Apply>
 * 
 * - Used for: Skill, Item Notetags
 * - Runs JavaScript code at the end of an action hit aimed at the function:
 *   Game_Action.prototype.apply()
 *   - 'Post' runs after the function runs.
 * - If used on skills and/or items, this will only apply to the skill/item
 *   being used and does not affect other skills and items.
 * - Replace 'code' with JavaScript code to run desired effects.
 * - The 'user' variable represents the one using the skill/item.
 * - The 'target' variable represents the one receiving the skill/item hit.
 *
 * ---
 *
 * <JS Post-Apply as User>
 *  code
 *  code
 *  code
 * </JS Post-Apply as User>
 *
 * <JS Post-Apply as Target>
 *  code
 *  code
 *  code
 * </JS Post-Apply as Target>
 * 
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs JavaScript code at the end of an action hit aimed at the function:
 *   Game_Action.prototype.apply()
 *   - 'Post' runs after the function runs.
 * - If used on trait objects, this will apply to any skills/items used as long
 *   as the unit affected by the trait object has access to the trait object.
 * - If the 'as User' notetag variant is used, this code will be run as a
 *   response to the action from the action user end.
 * - If the 'as Target' notetag variant is used, this code will be run as a
 *   response to the action from the action target end.
 * - Replace 'code' with JavaScript code to run desired effects.
 *
 * ---
 *
 * <JS Pre-End Action>
 *  code
 *  code
 *  code
 * </JS Pre-End Action>
 *
 * <JS Post-End Action>
 *  code
 *  code
 *  code
 * </JS Post-End Action>
 * 
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs JavaScript code at the end of an action aimed at the function:
 *   BattleManager.endAction()
 *   - 'Pre' runs before the function runs.
 *   - 'Post' runs after the function runs.
 * - If used on trait objects, this will apply to any skills/items used as long
 *   as the unit affected by the trait object has access to the trait object.
 * - Replace 'code' with JavaScript code to run desired effects.
 * - The 'user' variable represents the one affected by the trait object.
 *
 * ---
 *
 * <JS Pre-End Turn>
 *  code
 *  code
 *  code
 * </JS Pre-End Turn>
 *
 * <JS Post-End Turn>
 *  code
 *  code
 *  code
 * </JS Post-End Turn>
 * 
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs JavaScript code at the end of a turn aimed at the function:
 *   Game_Battler.prototype.onTurnEnd()
 *   - 'Pre' runs before the function runs.
 *   - 'Post' runs after the function runs.
 * - Replace 'code' with JavaScript code to run desired effects.
 * - The 'user' variable represents the one affected by the trait object.
 *
 * ---
 *
 * <JS Pre-Regenerate>
 *  code
 *  code
 *  code
 * </JS Pre-Regenerate>
 *
 * <JS Post-Regenerate>
 *  code
 *  code
 *  code
 * </JS Post-Regenerate>
 * 
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs JavaScript code when a unit regenerates HP/MP aimed at the function:
 *   Game_Battler.prototype.regenerateAll()
 *   - 'Pre' runs before the function runs.
 *   - 'Post' runs after the function runs.
 * - Replace 'code' with JavaScript code to run desired effects.
 * - The 'user' variable represents the one affected by the trait object.
 *
 * ---
 *
 * <JS Battle Victory>
 *  code
 *  code
 *  code
 * </JS Battle Victory>
 * 
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs JavaScript code when a battle is won aimed at the function:
 *   BattleManager.processVictory()
 * - Replace 'code' with JavaScript code to run desired effects.
 * - The 'user' variable represents the one affected by the trait object.
 *
 * ---
 *
 * <JS Escape Success>
 *  code
 *  code
 *  code
 * </JS Escape Success>
 * 
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs JavaScript code when escaping succeeds aimed at the function:
 *   BattleManager.onEscapeSuccess()
 * - Replace 'code' with JavaScript code to run desired effects.
 * - The 'user' variable represents the one affected by the trait object.
 *
 * ---
 *
 * <JS Escape Failure>
 *  code
 *  code
 *  code
 * </JS Escape Failure>
 * 
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs JavaScript code when escaping fails aimed at the function:
 *   BattleManager.onEscapeFailure()
 * - Replace 'code' with JavaScript code to run desired effects.
 * - The 'user' variable represents the one affected by the trait object.
 *
 * ---
 *
 * <JS Battle Defeat>
 *  code
 *  code
 *  code
 * </JS Battle Defeat>
 * 
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs JavaScript code when a battle is lost aimed at the function:
 *   BattleManager.processDefeat()
 * - Replace 'code' with JavaScript code to run desired effects.
 * - The 'user' variable represents the one affected by the trait object.
 *
 * ---
 *
 * <JS Pre-End Battle>
 *  code
 *  code
 *  code
 * </JS Pre-End Battle>
 *
 * <JS Post-End Battle>
 *  code
 *  code
 *  code
 * </JS Post-End Battle>
 * 
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs JavaScript code when the battle is over aimed at the function:
 *   BattleManager.endBattle()
 *   - 'Pre' runs before the function runs.
 *   - 'Post' runs after the function runs.
 * - Replace 'code' with JavaScript code to run desired effects.
 * - The 'user' variable represents the one affected by the trait object.
 *
 * ---
 * 
 * === Battle Layout-Related Notetags ===
 * 
 * These tags will change the battle layout for a troop regardless of how the
 * plugin parameters are set up normally. Insert these tags in either the
 * noteboxes of maps or the names of troops for them to take effect. If both
 * are present for a specific battle, then priority goes to the setting found
 * in the troop name.
 * 
 * ---
 * 
 * <Layout: type>
 * <Battle Layout: type>
 * 
 * - Used for: Map Notetags and Troop Name Tags
 * - Changes the battle layout style used for this specific map or battle.
 * - Replace 'type' with 'default', 'list', 'xp', 'portrait', or 'border'.
 * 
 * ---
 *
 * ============================================================================
 * Action Sequence - Plugin Commands
 * ============================================================================
 *
 * Skills and items, when used in battle, have a pre-determined series of
 * actions to display to the player as a means of representing what's going on
 * with the action. For some game devs, this may not be enough and they would
 * like to get more involved with the actions themselves.
 *
 * Action Sequences, added through this plugin, enable this. To give a skill or
 * item a Custom Action Sequence, a couple of steps must be followed:
 *
 * ---
 *
 * 1. Insert the <Custom Action Sequence> notetag into the skill or item's
 *    notebox (or else this would not work as intended).
 * 2. Give that skill/item a Common Event through the Effects box. The selected
 *    Common Event will contain all the Action Sequence data.
 * 3. Create the Common Event with Action Sequence Plugin Commands and/or event
 *    commands to make the skill/item do what you want it to do.
 *
 * ---
 *
 * The Plugin Commands added through the Battle Core plugin focus entirely on
 * Action Sequences. However, despite the fact that they're made for skills and
 * items, some of these Action Sequence Plugin Commands can still be used for
 * regular Troop events and Common Events.
 *
 * ---
 *
 * === Action Sequence - Action Sets ===
 *
 * Action Sequence Action Sets are groups of commonly used
 * Action Sequence Commands put together for more efficient usage.
 *
 * ---
 *
 * ACSET: Setup Action Set
 * - The generic start to most actions.
 *
 *   Display Action:
 *   Immortal: On:
 *   Battle Step:
 *   Wait For Movement:
 *   Cast Animation:
 *   Wait For Animation:
 *   - Use this part of the action sequence?
 *
 * ---
 *
 * ACSET: All Targets Action Set
 * - Affects all targets simultaneously performing the following.
 *
 *   Dual/Multi Wield?
 *   - Add times struck based on weapon quantity equipped?
 * 
 *   Perform Action:
 *   Wait Count:
 *   Action Animation:
 *   Wait For Animation:
 *   Action Effect:
 *   Immortal: Off:
 *   - Use this part of the action sequence?
 *   - Insert values for the Wait Count(s).
 *
 * ---
 *
 * ACSET: Each Target Action Set
 * - Goes through each target one by one to perform the following.
 *
 *   Dual/Multi Wield?
 *   - Add times struck based on weapon quantity equipped?
 *
 *   Perform Action:
 *   Wait Count:
 *   Action Animation:
 *   Wait Count:
 *   Action Effect:
 *   Immortal: Off:
 *   - Use this part of the action sequence?
 *   - Insert values for the Wait Count(s).
 *
 * ---
 *
 * ACSET: Finish Action
 * - The generic ending to most actions.
 *
 *   Wait For New Line:
 *   Wait For Effects:
 *   Clear Battle Log:
 *   Home Reset:
 *   Wait For Movement:
 *   - Use this part of the action sequence?
 *
 * ---
 * 
 * === Action Sequences - Angle ===
 * 
 * These action sequences allow you to have control over the camera angle.
 * Requires VisuMZ_3_ActSeqCamera!
 * 
 * ---
 *
 * ANGLE: Change Angle
 * - Changes the camera angle.
 * - Requires VisuMZ_3_ActSeqCamera!
 *
 *   Angle:
 *   - Change the camera angle to this many degrees.
 *
 *   Duration:
 *   - Duration in frames to change camera angle.
 *
 *   Angle Easing:
 *   - Select which easing type you wish to apply.
 *   - Requires VisuMZ_0_CoreEngine.
 *
 *   Wait For Angle?:
 *   - Wait for angle changes to complete before performing next command?
 *
 * ---
 *
 * ANGLE: Reset Angle
 * - Reset any angle settings.
 * - Requires VisuMZ_3_ActSeqCamera!
 *
 *   Duration:
 *   - Duration in frames to reset camera angle.
 *
 *   Angle Easing:
 *   - Select which easing type you wish to apply.
 *   - Requires VisuMZ_0_CoreEngine.
 *
 *   Wait For Angle?:
 *   - Wait for angle changes to complete before performing next command?
 *
 * ---
 *
 * ANGLE: Wait For Angle
 * - Waits for angle changes to complete before performing next command.
 * - Requires VisuMZ_3_ActSeqCamera!
 *
 * ---
 *
 * === Action Sequences - Animations ===
 *
 * These Action Sequences are related to the 'Animations' that can be found in
 * the Animations tab of the Database.
 *
 * ---
 *
 * ANIM: Action Animation
 * - Plays the animation associated with the action.
 *
 *   Targets:
 *   - Select unit(s) to play the animation on.
 *
 *   Mirror Animation:
 *   - Mirror the animation?
 *
 *   Wait For Animation?:
 *   - Wait for animation to complete before performing next command?
 *
 * ---
 *
 * ANIM: Attack Animation
 * - Plays the animation associated with the user's weapon.
 *
 *   Targets:
 *   - Select unit(s) to play the animation on.
 *
 *   Mirror Animation:
 *   - Mirror the animation?
 *
 *   Wait For Animation?:
 *   - Wait for animation to complete before performing next command?
 *
 * ---
 *
 * ANIM: Attack Animation 2+
 * - Plays the animation associated with the user's other weapons.
 * - Plays nothing if there is no other weapon equipped.
 *
 *   Targets:
 *   - Select unit(s) to play the animation on.
 * 
 *   Slot:
 *   - Which weapon slot to get this data from?
 *   - Main-hand weapon is weapon slot 1.
 *
 *   Mirror Animation:
 *   - Mirror the animation?
 *
 *   Wait For Animation?:
 *   - Wait for animation to complete before performing next command?
 *
 * ---
 *
 * ANIM: Cast Animation
 * - Plays the cast animation associated with the action.
 *
 *   Targets:
 *   - Select unit(s) to play the animation on.
 *
 *   Mirror Animation:
 *   - Mirror the animation?
 *
 *   Wait For Animation?:
 *   - Wait for animation to complete before performing next command?
 *
 * ---
 *
 * ANIM: Change Battle Portrait
 * - Changes the battle portrait of the actor (if it's an actor).
 * - Can be used outside of battle/action sequences.
 *
 *   Targets:
 *   - Select unit(s) to play the animation on.
 *   - Valid units can only be actors.
 *
 *   Filename:
 *   - Select the file to change the actor's portrait to.
 *
 * ---
 *
 * ANIM: Show Animation
 * - Plays the a specific animation on unit(s).
 *
 *   Targets:
 *   - Select unit(s) to play the animation on.
 *
 *   Animation ID:
 *   - Select which animation to play on unit(s).
 *
 *   Mirror Animation:
 *   - Mirror the animation?
 *
 *   Wait For Animation?:
 *   - Wait for animation to complete before performing next command?
 *
 * ---
 *
 * ANIM: Wait For Animation
 * - Causes the interpreter to wait for any animation(s) to finish.
 *
 * ---
 *
 * === Action Sequences - Battle Log ===
 *
 * These Action Sequences are related to the Battle Log Window, the window
 * found at the top of the battle screen.
 *
 * ---
 *
 * BTLOG: Add Text
 * - Adds a new line of text into the Battle Log.
 *
 *   Text:
 *   - Add this text into the Battle Log.
 *   - Text codes allowed.
 * 
 *   Copy to Combat Log?:
 *   - Copies text to the Combat Log.
 *   - Requires VisuMZ_4_CombatLog
 * 
 *     Combat Log Icon:
 *     - What icon would you like to bind to this entry?
 *     - Requires VisuMZ_4_CombatLog
 *
 * ---
 *
 * BTLOG: Clear Battle Log
 * - Clears all the text in the Battle Log.
 *
 * ---
 *
 * BTLOG: Display Action
 * - plays the current action in the Battle Log.
 *
 * ---
 *
 * BTLOG: Pop Base Line
 * - Removes the Battle Log's last added base line and  all text up to its
 *   former location.
 *
 * ---
 *
 * BTLOG: Push Base Line
 * - Adds a new base line to where the Battle Log currently is at.
 *
 * ---
 *
 * BTLOG: Refresh Battle Log
 * - Refreshes the Battle Log.
 *
 * ---
 *
 * BTLOG: UI Show/Hide
 * - Shows or hides the Battle UI (including the Battle Log).
 *
 *   Show/Hide?:
 *   - Shows/hides the Battle UI.
 *
 * ---
 *
 * BTLOG: Wait For Battle Log
 * - Causes the interpreter to wait for the Battle Log to finish.
 *
 * ---
 *
 * BTLOG: Wait For New Line
 * - Causes the interpreter to wait for a new line in the Battle Log.
 *
 * ---
 *
 * === Action Sequences - Camera ===
 *
 * These Action Sequences are battle camera-related.
 * Requires VisuMZ_3_ActSeqCamera!
 *
 * ---
 *
 * CAMERA: Clamp ON/OFF
 * - Turns battle camera clamping on/off.
 * - Requires VisuMZ_3_ActSeqCamera!
 *
 *   Setting:
 *   - Turns camera clamping on/off.
 *
 * ---
 *
 * CAMERA: Focus Point
 * - Focus the battle camera on a certain point in the screen.
 * - Requires VisuMZ_3_ActSeqCamera!
 *
 *   X Coordinate:
 *   - Insert the point to focus the camera on.
 *   - You may use JavaScript code.
 *
 *   Y Coordinate:
 *   - Insert the point to focus the camera on.
 *   - You may use JavaScript code.
 *
 *   Duration:
 *   - Duration in frames for camera focus change.
 *
 *   Camera Easing:
 *   - Select which easing type you wish to apply.
 *   - Requires VisuMZ_0_CoreEngine.
 *
 *   Wait For Camera?
 *   - Wait for camera changes to complete before performing next command?
 *
 * ---
 *
 * CAMERA: Focus Target(s)
 * - Focus the battle camera on certain battler target(s).
 * - Requires VisuMZ_3_ActSeqCamera!
 *
 *   Targets:
 *   - Select unit(s) to focus the battle camera on.
 *
 *   Duration:
 *   - Duration in frames for camera focus change.
 *
 *   Camera Easing:
 *   - Select which easing type you wish to apply.
 *   - Requires VisuMZ_0_CoreEngine.
 *
 *   Wait For Camera?
 *   - Wait for camera changes to complete before performing next command?
 *
 * ---
 *
 * CAMERA: Offset
 * - Offset the battle camera from the focus target.
 * - Requires VisuMZ_3_ActSeqCamera!
 *
 *   Offset X:
 *   - How much to offset the camera X by.
 *   - Negative: left. Positive: right.
 *
 *   Offset Y:
 *   - How much to offset the camera Y by.
 *   - Negative: up. Positive: down.
 *
 *   Duration:
 *   - Duration in frames for offset change.
 *
 *   Camera Easing:
 *   - Select which easing type you wish to apply.
 *   - Requires VisuMZ_0_CoreEngine.
 *
 *   Wait For Camera?
 *   - Wait for camera changes to complete before performing next command?
 *
 * ---
 *
 * CAMERA: Reset
 * - Reset the battle camera settings.
 * - Requires VisuMZ_3_ActSeqCamera!
 *
 *   Reset Focus?:
 *   - Reset the focus point?
 *
 *   Reset Offset?:
 *   - Reset the camera offset?
 *
 *   Duration:
 *   - Duration in frames for reset change.
 *
 *   Camera Easing:
 *   - Select which easing type you wish to apply.
 *   - Requires VisuMZ_0_CoreEngine.
 *
 *   Wait For Camera?
 *   - Wait for camera changes to complete before performing next command?
 *
 * ---
 *
 * CAMERA: Wait For Camera
 * - Waits for camera changes to complete before performing next command.
 * - Requires VisuMZ_3_ActSeqCamera!
 *
 * ---
 *
 * === Action Sequences - Dragonbones ===
 *
 * These Action Sequences are Dragonbones-related.
 * Requires VisuMZ_2_DragonbonesUnion!
 *
 * ---
 *
 * DB: Dragonbones Animation
 * - Causes the unit(s) to play a Dragonbones motion animation.
 * - Requires VisuMZ_2_DragonbonesUnion!
 *
 *   Targets:
 *   - Select which unit(s) to perform a motion animation.
 *
 *   Motion Animation:
 *   - What is the name of the Dragonbones motion animation you wish to play?
 *
 * ---
 *
 * DB: Dragonbones Time Scale
 * - Causes the unit(s) to change their Dragonbones time scale.
 * - Requires VisuMZ_2_DragonbonesUnion!
 *
 *   Targets:
 *   - Select which unit(s) to perform a motion animation.
 *
 *   Time Scale:
 *   - Change the value of the Dragonbones time scale to this.
 *
 * ---
 *
 * === Action Sequences - Elements ===
 *
 * These Action Sequences can change up the element(s) used for the action's
 * damage calculation midway through an action.
 *
 * They also require the VisuMZ_1_ElementStatusCore plugin to be present in
 * order for them to work.
 *
 * ---
 *
 * ELE: Add Elements
 * - Adds element(s) to be used when calculating damage.
 * - Requires VisuMZ_1_ElementStatusCore!
 *
 *   Elements:
 *   - Select which element ID to add onto the action.
 *   - Insert multiple element ID's to add multiple at once.
 *
 * ---
 *
 * ELE: Clear Element Changes
 * - Clears all element changes made through Action Sequences.
 * - Requires VisuMZ_1_ElementStatusCore!
 *
 * ---
 *
 * ELE: Force Elements
 * - Forces only specific element(s) when calculating damage.
 * - Requires VisuMZ_1_ElementStatusCore!
 *
 *   Elements:
 *   - Select which element ID to force in the action.
 *   - Insert multiple element ID's to force multiple at once.
 *
 * ---
 *
 * ELE: Null Element
 * - Forces no element to be used when calculating damage.
 * - Requires VisuMZ_1_ElementStatusCore!
 *
 * ---
 * 
 * === Action Sequences - Horror Effects ===
 * 
 * These Action Sequences are Horror Effects-related.
 * Requires VisuMZ_2_HorrorEffects!
 * 
 * ---
 *
 * HORROR: Clear All Filters
 * - Clear all Horror Effects filters on the target battler(s).
 *
 *   Targets:
 *   - Select unit(s) to remove Horror Effects for.
 *
 * ---
 *
 * HORROR: Glitch Create
 * - Creates the glitch effect on the target battler(s).
 *
 *   Targets:
 *   - Select unit(s) to create the Horror Effect for.
 *
 *   Glitch Slices:
 *   - Glitch slices to be used with the target.
 *
 *   Glitch Offset:
 *   - Default offset value.
 *
 *   Glitch Animated?:
 *   - Animate the glitch effect?
 *
 *   Glitch Frequency:
 *   - If animated, how frequent to make the glitch effect?
 *   - Lower = often     Higher = rarer
 *
 *   Glitch Strength:
 *   - If animated, how strong is the glitch effect?
 *   - Lower = weaker     Higher = stronger
 *
 * ---
 *
 * HORROR: Glitch Remove
 * - Removes the glitch effect on the target battler(s).
 *
 *   Targets:
 *   - Select unit(s) to remove the Horror Effect for.
 *
 * ---
 *
 * HORROR: Noise Create
 * - Creates the noise effect on the target battler(s).
 *
 *   Targets:
 *   - Select unit(s) to create the Horror Effect for.
 *
 *   Noise Rate:
 *   - Noise rate to be used with the target.
 *
 *   Noise Animated:
 *   - Animate the noise for the target?
 *
 * ---
 *
 * HORROR: Noise Remove
 * - Removes the noise effect on the target battler(s).
 *
 *   Targets:
 *   - Select unit(s) to remove the Horror Effect for.
 *
 * ---
 *
 * HORROR: TV Create
 * - Creates the TV effect on the target battler(s).
 *
 *   Targets:
 *   - Select unit(s) to create the Horror Effect for.
 *
 *   TV Line Thickness:
 *   - Default TV line thickness
 *   - Lower = thinner     Higher = thicker
 *
 *   TV Corner Size:
 *   - Default TV line corner size
 *   - Lower = smaller     Higher = bigger
 *
 *   TV Animated:
 *   - Animate the TV?
 *
 *   TV Speed:
 *   - Speed used to animate the TV if animated
 *   - Lower = slower     Higher = faster
 *
 * ---
 *
 * HORROR: TV Remove
 * - Removes the TV effect on the target battler(s).
 *
 *   Targets:
 *   - Select unit(s) to remove the Horror Effect for.
 *
 * ---
 * 
 * === Action Sequences - Impact ===
 * 
 * These Action Sequences are related to creating impact.
 * Requires VisuMZ_3_ActSeqImpact!
 * 
 * ---
 *
 * IMPACT: Color Break
 * - Breaks the colors on the screen before reassembling.
 * - Requires VisuMZ_3_ActSeqImpact!
 *
 *   Intensity:
 *   - What is the intensity of the color break effect?
 *
 *   Duration:
 *   - What is the duration of the color break effect?
 *
 *   Easing Type:
 *   - Select which easing type you wish to apply.
 *
 * ---
 *
 * IMPACT: Motion Blur Screen
 * - Creates a motion blur on the whole screen.
 * - Requires VisuMZ_3_ActSeqImpact!
 *
 *   Angle:
 *   - Determine what angle to make the motion blur at.
 *
 *   Intensity Rate:
 *   - This determines intensity rate of the motion blur.
 *   - Use a number between 0 and 1.
 *
 *   Duration:
 *   - How many frames should the motion blur last?
 *   - What do you want to be its duration?
 *
 *   Easing Type:
 *   - Select which easing type you wish to apply.
 *
 * ---
 *
 * IMPACT: Motion Blur Target(s)
 * - Creates a motion blur on selected target(s).
 * - Requires VisuMZ_3_ActSeqImpact!
 *
 *   Targets:
 *   - Select unit(s) to create motion blur effects for.
 *
 *   Angle:
 *   - Determine what angle to make the motion blur at.
 *
 *   Intensity Rate:
 *   - This determines intensity rate of the motion blur.
 *   - Use a number between 0 and 1.
 *
 *   Duration:
 *   - How many frames should the motion blur last?
 *   - What do you want to be its duration?
 *
 *   Easing Type:
 *   - Select which easing type you wish to apply.
 *
 * ---
 *
 * IMPACT: Motion Trail Create
 * - Creates a motion trail effect for the target(s).
 * - Requires VisuMZ_3_ActSeqImpact!
 *
 *   Targets:
 *   - Select unit(s) to create motion trail effects for.
 *
 *   Delay:
 *   - How many frames to delay by when creating a motion trail?
 *   - The higher the delay, the less motion trails there are.
 *
 *   Duration:
 *   - How many frames should the motion trail last?
 *   - What do you want to be its duration?
 *
 *   Hue:
 *   - What do you want to be the hue for the motion trail?
 *
 *   Starting Opacity:
 *   - What starting opacity value do you want for the motion trail?
 *   - Opacity values decrease over time.
 *
 *   Tone:
 *   - What tone do you want for the motion trail?
 *   - Format: [Red, Green, Blue, Gray]
 *
 * ---
 *
 * IMPACT: Motion Trail Remove
 * - Removes the motion trail effect from the target(s).
 * - Requires VisuMZ_3_ActSeqImpact!
 *
 *   Targets:
 *   - Select unit(s) to clear motion trail effects for.
 *
 * ---
 *
 * IMPACT: Shockwave at Point
 * - Creates a shockwave at the designated coordinates.
 * - Requires VisuMZ_3_ActSeqImpact!
 *
 *   Point: X:
 *   Point: Y:
 *   - What x/y coordinate do you want to create a shockwave at?
 *   - You can use JavaScript code.
 *
 *   Amplitude:
 *   - What is the aplitude of the shockwave effect?
 *
 *   Wavelength:
 *   - What is the wavelength of the shockwave effect?
 *
 *   Duration:
 *   - What is the duration of the shockwave?
 *
 * ---
 *
 * IMPACT: Shockwave from Each Target(s)
 * - Creates a shockwave at each of the target(s) location(s).
 * - Requires VisuMZ_3_ActSeqImpact!
 *
 *   Targets:
 *   - Select unit(s) to start a shockwave from.
 *
 *   Target Location:
 *   - Select which part target group to start a shockwave from.
 * 
 *     Offset X:
 *     Offset Y:
 *     - How much to offset the shockwave X/Y point by.
 *
 *   Amplitude:
 *   - What is the aplitude of the shockwave effect?
 *
 *   Wavelength:
 *   - What is the wavelength of the shockwave effect?
 *
 *   Duration:
 *   - What is the duration of the shockwave?
 *
 * ---
 *
 * IMPACT: Shockwave from Target(s) Center
 * - Creates a shockwave from the center of the target(s).
 * - Requires VisuMZ_3_ActSeqImpact!
 *
 *   Targets:
 *   - Select unit(s) to start a shockwave from.
 *
 *   Target Location:
 *   - Select which part target group to start a shockwave from.
 * 
 *     Offset X:
 *     Offset Y:
 *     - How much to offset the shockwave X/Y point by.
 *
 *   Amplitude:
 *   - What is the aplitude of the shockwave effect?
 *
 *   Wavelength:
 *   - What is the wavelength of the shockwave effect?
 *
 *   Duration:
 *   - What is the duration of the shockwave?
 *
 * ---
 *
 * IMPACT: Zoom Blur at Point
 * - Creates a zoom blur at the designated coordinates.
 * - Requires VisuMZ_3_ActSeqImpact!
 *
 *   Point: X:
 *   Point: Y:
 *   - What x/y coordinate do you want to focus the zoom at?
 *   - You can use JavaScript code.
 *
 *   Zoom Strength:
 *   - What is the strength of the zoom effect?
 *   - Use a number between 0 and 1.
 *
 *   Visible Radius:
 *   - How much of a radius should be visible from the center?
 *
 *   Duration:
 *   - What is the duration of the zoom blur?
 *
 *   Easing Type:
 *   - Select which easing type you wish to apply.
 *
 * ---
 *
 * IMPACT: Zoom Blur at Target(s) Center
 * - Creates a zoom blur at the center of targets.
 * - Requires VisuMZ_3_ActSeqImpact!
 *
 *   Targets:
 *   - Select unit(s) to start a zoom blur from.
 *
 *   Target Location:
 *   - Select which part target group to start a zoom blur from.
 * 
 *     Offset X:
 *     Offset Y:
 *     - How much to offset the zoom blur X/Y point by.
 *
 *   Zoom Strength:
 *   - What is the strength of the zoom effect?
 *   - Use a number between 0 and 1.
 *
 *   Visible Radius:
 *   - How much of a radius should be visible from the center?
 *
 *   Duration:
 *   - What is the duration of the zoom blur?
 *
 *   Easing Type:
 *   - Select which easing type you wish to apply.
 *
 * ---
 *
 * === Action Sequences - Mechanics ===
 *
 * These Action Sequences are related to various mechanics related to the
 * battle system.
 *
 * ---
 *
 * MECH: Action Effect
 * - Causes the unit(s) to take damage/healing from action and incurs any
 *   changes made such as buffs and states.
 *
 *   Targets:
 *   - Select unit(s) to receive the current action's effects.
 *
 * ---
 *
 * MECH: Add Buff/Debuff
 * - Adds buff(s)/debuff(s) to unit(s). 
 * - Determine which parameters are affected and their durations.
 *
 *   Targets:
 *   - Select unit(s) to receive the buff(s) and/or debuff(s).
 *
 *   Buff Parameters:
 *   - Select which parameter(s) to buff.
 *   - Insert a parameter multiple times to raise its stacks.
 *
 *   Debuff Parameters:
 *   - Select which parameter(s) to debuff.
 *   - Insert a parameter multiple times to raise its stacks.
 *
 *   Turns:
 *   - Number of turns to set the parameter(s) buffs to.
 *   - You may use JavaScript code.
 *
 * ---
 *
 * MECH: Add State
 * - Adds state(s) to unit(s).
 *
 *   Targets:
 *   - Select unit(s) to receive the buff(s).
 *
 *   States:
 *   - Select which state ID(s) to add to unit(s).
 *   - Insert multiple state ID's to add multiple at once.
 *
 * ---
 *
 * MECH: Armor Penetration
 * - Adds an extra layer of defensive penetration/reduction.
 * - You may use JavaScript code for any of these.
 *
 *   Armor/Magic Penetration:
 *
 *     Rate:
 *     - Penetrates an extra multiplier of armor by this value.
 *
 *     Flat:
 *     - Penetrates a flat amount of armor by this value.
 *
 *   Armor/Magic Reduction:
 *
 *     Rate:
 *     - Reduces an extra multiplier of armor by this value.
 *
 *     Flat:
 *     - Reduces a flat amount of armor by this value.
 *
 * ---
 * 
 * MECH: ATB Gauge
 * - Alters the ATB/TPB Gauges.
 * - Requires VisuMZ_2_BattleSystemATB!
 * 
 *   Targets:
 *   - Select unit(s) to alter the ATB/TPB Gauges for.
 * 
 *   Charging:
 *   
 *     Charge Rate:
 *     - Changes made to the ATB Gauge if it is currently charging.
 * 
 *   Casting:
 *   
 *     Cast Rate:
 *     - Changes made to the ATB Gauge if it is currently casting.
 *   
 *     Interrupt?:
 *     - Interrupt the ATB Gauge if it is currently casting?
 * 
 * ---
 * 
 * MECH: BTB Brave Points
 * - Alters the target(s) Brave Points to an exact value.
 * - Requires VisuMZ_2_BattleSystemBTB!
 * 
 *   Targets:
 *   - Select unit(s) to alter the ATB/TPB Gauges for.
 * 
 *   Alter Brave Points By:
 *   - Alters the target(s) Brave Points.
 *   - Positive for gaining BP.
 *   - Negative for losing BP.
 * 
 * ---
 *
 * MECH: Collapse
 * - Causes the unit(s) to perform its collapse animation if the unit(s)
 *   has died.
 *
 *   Targets:
 *   - Select unit(s) to process a death collapse.
 *
 *   Force Death:
 *   - Force death even if the unit has not reached 0 HP?
 *   - This will remove immortality.
 *
 *   Wait For Effect?:
 *   - Wait for the collapse effect to complete before performing next command?
 *
 * ---
 * 
 * MECH: CTB Order
 * - Alters the CTB Turn Order.
 * - Requires VisuMZ_2_BattleSystemCTB!
 * 
 *   Targets:
 *   - Select unit(s) to alter the CTB Turn Order for.
 * 
 *   Change Order By:
 *   - Changes turn order for target(s) by this amount.
 *   - Positive increases wait. Negative decreases wait.
 * 
 * ---
 * 
 * MECH: CTB Speed
 * - Alters the CTB Speed.
 * - Requires VisuMZ_2_BattleSystemCTB!
 * 
 *   Targets:
 *   - Select unit(s) to alter the CTB Speed for.
 * 
 *   Charge Rate:
 *   - Changes made to the CTB Speed if it is currently charging.
 * 
 *   Cast Rate:
 *   - Changes made to the CTB Speed if it is currently casting.
 * 
 * ---
 * 
 * MECH: Custom Damage Formula
 * - Changes the current action's damage formula to custom.
 * - This will assume the MANUAL damage style.
 * 
 *   Formula:
 *   - Changes the current action's damage formula to custom.
 *   - Use 'default' to revert the damage formula.
 * 
 * ---
 *
 * MECH: Damage Popup
 * - Causes the unit(s) to display the current state of damage received
 *   or healed.
 *
 *   Targets:
 *   - Select unit(s) to prompt a damage popup.
 *
 * ---
 *
 * MECH: Dead Label Jump
 * - If the active battler is dead, jump to a specific label in the
 *   common event.
 *
 *   Jump To Label:
 *   - If the active battler is dead, jump to this specific label in the
 *     common event.
 *
 * ---
 *
 * MECH: HP, MP, TP
 * - Alters the HP, MP, and TP values for unit(s).
 * - Positive values for healing. Negative values for damage.
 *
 *   Targets:
 *   - Select unit(s) to receive the current action's effects.
 *
 *   HP, MP, TP:
 *
 *     Rate:
 *     - Changes made to the parameter based on rate.
 *     - Positive values for healing. Negative values for damage.
 *
 *     Flat:
 *     - Flat changes made to the parameter.
 *     - Positive values for healing. Negative values for damage.
 *
 *   Damage Popup?:
 *   - Display a damage popup after?
 *
 * ---
 *
 * MECH: Immortal
 * - Changes the immortal flag of targets. If immortal flag is removed and a
 *   unit would die, collapse that unit.
 *
 *   Targets:
 *   - Alter the immortal flag of these groups. If immortal flag is removed and
 *     a unit would die, collapse that unit.
 *
 *   Immortal:
 *   - Turn immortal flag for unit(s) on/off?
 *
 * ---
 *
 * MECH: Multipliers
 * - Changes the multipliers for the current action.
 * - You may use JavaScript code for any of these.
 *
 *   Critical Hit%:
 *
 *     Rate:
 *     - Affects chance to land a critical hit by this multiplier.
 *
 *     Flat:
 *     - Affects chance to land a critical hit by this flat bonus.
 *
 *   Critical Damage
 *
 *     Rate:
 *     - Affects critical damage by this multiplier.
 *
 *     Flat:
 *     - Affects critical damage by this flat bonus.
 *
 *   Damage/Healing
 *
 *     Rate:
 *     - Sets the damage/healing multiplier for current action.
 *
 *     Flat:
 *     - Sets the damage/healing bonus for current action.
 *
 *   Hit Rate
 *
 *     Rate:
 *     - Affects chance to connect attack by this multiplier.
 *
 *     Flat:
 *     - Affects chance to connect attack by this flat bonus.
 *
 * ---
 *
 * MECH: Remove Buff/Debuff
 * - Removes buff(s)/debuff(s) from unit(s). 
 * - Determine which parameters are removed.
 *
 *   Targets:
 *   - Select unit(s) to have the buff(s) and/or debuff(s) removed.
 *
 *   Buff Parameters:
 *   - Select which buffed parameter(s) to remove.
 *
 *   Debuff Parameters:
 *   - Select which debuffed parameter(s) to remove.
 *
 * ---
 *
 * MECH: Remove State
 * - Remove state(s) from unit(s).
 *
 *   Targets:
 *   - Select unit(s) to have states removed from.
 *
 *   States:
 *   - Select which state ID(s) to remove from unit(s).
 *   - Insert multiple state ID's to remove multiple at once.
 *
 * ---
 * 
 * MECH: STB Exploit Effect
 * - Utilize the STB Exploitation mechanics!
 * - Requires VisuMZ_2_BattleSystemSTB!
 * 
 *   Target(s) Exploited?:
 *   - Exploit the below targets?
 * 
 *     Targets:
 *     - Select unit(s) to become exploited.
 * 
 *     Force Exploitation:
 *     - Force the exploited status?
 * 
 *   User Exploiter?:
 *   - Allow the user to become the exploiter?
 * 
 *     Force Exploitation:
 *     - Force the exploiter status?
 * 
 * ---
 * 
 * MECH: STB Extra Action
 * - Adds an extra action for the currently active battler.
 * - Requires VisuMZ_2_BattleSystemSTB!
 * 
 *   Extra Actions:
 *   - How many extra actions should the active battler gain?
 *   - You may use JavaScript code.
 * 
 * ---
 * 
 * MECH: STB Remove Excess Actions
 * - Removes excess actions from the active battler.
 * - Requires VisuMZ_2_BattleSystemSTB!
 * 
 *   Remove Actions:
 *   - How many actions to remove from the active battler?
 *   - You may use JavaScript code.
 * 
 * ---
 * 
 * MECH: Text Popup
 * - Causes the unit(s) to display a text popup.
 * 
 *   Targets:
 *   - Select unit(s) to prompt a text popup.
 * 
 *   Text:
 *   - What text do you wish to display?
 * 
 *   Text Color:
 *   - Use #rrggbb for custom colors or regular numbers for text colors from
 *     the Window Skin.
 * 
 *   Flash Color:
 *   - Adjust the popup's flash color.
 *   - Format: [red, green, blue, alpha]
 * 
 *   Flash Duration:
 *   - What is the frame duration of the flash effect?
 * 
 * ---
 * 
 * MECH: Variable Popup
 * - Causes the unit(s) to display a popup using the data stored inside
 *   a variable.
 * 
 *   Targets:
 *   - Select unit(s) to prompt a text popup.
 * 
 *   Variable:
 *   - Get data from which variable to display as a popup?
 * 
 *   Digit Grouping:
 *   - Use digit grouping to separate numbers?
 *   - Requires VisuMZ_0_CoreEngine!
 * 
 *   Text Color:
 *   - Use #rrggbb for custom colors or regular numbers for text colors from
 *     the Window Skin.
 * 
 *   Flash Color:
 *   - Adjust the popup's flash color.
 *   - Format: [red, green, blue, alpha]
 * 
 *   Flash Duration:
 *   - What is the frame duration of the flash effect?
 * 
 * ---
 *
 * MECH: Wait For Effect
 * - Waits for the effects to complete before performing next command.
 *
 * ---
 *
 * === Action Sequences - Motion ===
 *
 * These Action Sequences allow you the ability to control the motions of
 * sideview sprites.
 *
 * ---
 * 
 * MOTION: Clear Freeze Frame
 * - Clears any freeze frames from the unit(s).
 * 
 *   Targets:
 *   - Select which unit(s) to clear freeze frames for.
 * 
 * ---
 * 
 * MOTION: Freeze Motion Frame
 * - Forces a freeze frame instantly at the selected motion.
 * - Automatically clears with a new motion.
 * 
 *   Targets:
 *   - Select which unit(s) to freeze motions for.
 * 
 *   Motion Type:
 *   - Freeze this motion for the unit(s).
 * 
 *   Frame Index:
 *   - Which frame do you want to freeze the motion on?
 *   - Frame index values start at 0.
 * 
 *   Show Weapon?:
 *   - If using 'attack', 'thrust', 'swing', or 'missile', display the
 *     weapon sprite?
 * 
 * ---
 *
 * MOTION: Motion Type
 * - Causes the unit(s) to play the selected motion.
 *
 *   Targets:
 *   - Select which unit(s) to perform a motion.
 *
 *   Motion Type:
 *   - Play this motion for the unit(s).
 *
 *   Show Weapon?:
 *   - If using 'attack', 'thrust', 'swing', or 'missile', display the
 *     weapon sprite?
 *
 * ---
 *
 * MOTION: Perform Action
 * - Causes the unit(s) to play the proper motion based on the current action.
 *
 *   Targets:
 *   - Select which unit(s) to perform a motion.
 *
 * ---
 *
 * MOTION: Refresh Motion
 * - Cancels any set motions unit(s) has to do and use their most natural
 *   motion at the moment.
 *
 *   Targets:
 *   - Select which unit(s) to refresh their motion state.
 *
 * ---
 *
 * MOTION: Wait By Motion Frame
 * - Creates a wait equal to the number of motion frames passing.
 * - Time is based on Plugin Parameters => Actors => Motion Speed.
 *
 *   Motion Frames to Wait?:
 *   - Each "frame" is equal to the value found in 
 *     Plugin Parameters => Actors => Motion Speed
 *
 * ---
 *
 * === Action Sequences - Movement ===
 *
 * These Action Sequences allow you the ability to control the sprites of
 * actors and enemies in battle.
 *
 * ---
 *
 * MOVE: Battle Step
 * - Causes the unit(s) to move forward past their home position to prepare
 *   for action.
 *
 *   Targets:
 *   - Select which unit(s) to move.
 *
 *   Wait For Movement?:
 *   - Wait for movement to complete before performing next command?
 *
 * ---
 *
 * MOVE: Face Direction
 * - Causes the unit(s) to face forward or backward.
 * - Sideview-only!
 *
 *   Targets:
 *   - Select which unit(s) to change direction.
 *
 *   Direction:
 *   - Select which direction to face.
 *
 * ---
 *
 * MOVE: Face Point
 * - Causes the unit(s) to face a point on the screen.
 * - Sideview-only!
 *
 *   Targets:
 *   - Select which unit(s) to change direction.
 *
 *   Point:
 *   - Select which point to face.
 *     - Home
 *     - Center
 *     - Point X, Y
 *       - Replace 'x' and 'y' with coordinates
 *
 *   Face Away From?:
 *   - Face away from the point instead?
 *
 * ---
 *
 * MOVE: Face Target(s)
 * - Causes the unit(s) to face other targets on the screen.
 * - Sideview-only!
 *
 *   Targets (facing):
 *   - Select which unit(s) to change direction.
 *
 *   Targets (destination):
 *   - Select which unit(s) for the turning unit(s) to face.
 *
 *   Face Away From?:
 *   - Face away from the unit(s) instead?
 *
 * ---
 *
 * MOVE: Float
 * - Causes the unit(s) to float above the ground.
 * - Sideview-only!
 *
 *   Targets:
 *   - Select which unit(s) to make float.
 *
 *   Desired Height:
 *   - Vertical distance to float upward.
 *   - You may use JavaScript code.
 *
 *   Duration:
 *   - Duration in frames for total float amount.
 *
 *   Float Easing:
 *   - Select which easing type you wish to apply.
 *   - Requires VisuMZ_0_CoreEngine.
 *
 *   Wait For Float?:
 *   - Wait for floating to complete before performing next command?
 *
 * ---
 *
 * MOVE: Home Reset
 * - Causes the unit(s) to move back to their home position(s) and face back to
 *   their original direction(s).
 *
 *   Targets:
 *   - Select which unit(s) to move.
 *
 *   Wait For Movement?:
 *   - Wait for movement to complete before performing next command?
 *
 * ---
 *
 * MOVE: Jump
 * - Causes the unit(s) to jump into the air.
 * - Sideview-only!
 *
 *   Targets:
 *   - Select which unit(s) to make jump.
 *
 *   Desired Height:
 *   - Max jump height to go above the ground
 *   - You may use JavaScript code.
 *
 *   Duration:
 *   - Duration in frames for total jump amount.
 *
 *   Wait For Jump?:
 *   - Wait for jumping to complete before performing next command?
 *
 * ---
 *
 * MOVE: Move Distance
 * - Moves unit(s) by a distance from their current position(s).
 * - Sideview-only!
 *
 *   Targets:
 *   - Select which unit(s) to move.
 *
 *   Distance Adjustment:
 *   - Makes adjustments to distance values to determine which direction to
 *     move unit(s).
 *     - Normal - No adjustments made
 *     - Horizontal - Actors adjust left, Enemies adjust right
 *     - Vertical - Actors adjust Up, Enemies adjust down
 *     - Both - Applies both Horizontal and Vertical
 *
 *     Distance: X:
 *     - Horizontal distance to move.
 *     - You may use JavaScript code.
 *
 *     Distance: Y:
 *     - Vertical distance to move.
 *     - You may use JavaScript code.
 *
 *   Duration:
 *   - Duration in frames for total movement amount.
 *
 *   Face Destination?:
 *   - Turn and face the destination?
 *
 *   Movement Easing:
 *   - Select which easing type you wish to apply.
 *   - Requires VisuMZ_0_CoreEngine.
 *
 *   Movement Motion:
 *   - Play this motion for the unit(s).
 *
 *   Wait For Movement?:
 *   - Wait for movement to complete before performing next command?
 *
 * ---
 *
 * MOVE: Move To Point
 * - Moves unit(s) to a designated point on the screen.
 * - Sideview-only! Points based off Graphics.boxWidth/Height.
 *
 *   Targets:
 *   - Select which unit(s) to move.
 *
 *   Destination Point:
 *   - Select which point to face.
 *     - Home
 *     - Center
 *     - Point X, Y
 *       - Replace 'x' and 'y' with coordinates
 *
 *   Offset Adjustment:
 *   - Makes adjustments to offset values to determine which direction to
 *     adjust the destination by.
 *
 *     Offset: X:
 *     - Horizontal offset to move.
 *     - You may use JavaScript code.
 *
 *     Offset: Y:
 *     - Vertical offset to move.
 *     - You may use JavaScript code.
 *
 *   Duration:
 *   - Duration in frames for total movement amount.
 *
 *   Face Destination?:
 *   - Turn and face the destination?
 *
 *   Movement Easing:
 *   - Select which easing type you wish to apply.
 *   - Requires VisuMZ_0_CoreEngine.
 *
 *   Movement Motion:
 *   - Play this motion for the unit(s).
 *
 *   Wait For Movement?:
 *   - Wait for movement to complete before performing next command?
 *
 * ---
 *
 * MOVE: Move To Target(s)
 * - Moves unit(s) to another unit(s) on the battle field.
 * - Sideview-only!
 *
 *   Targets (Moving):
 *   - Select which unit(s) to move.
 *
 *   Targets (Destination):
 *   - Select which unit(s) to move to.
 *
 *     Target Location:
 *     - Select which part target group to move to.
 *       - front head
 *       - front center
 *       - front base
 *       - middle head
 *       - middle center
 *       - middle base
 *       - back head
 *       - back center
 *       - back base
 *
 *     Melee Distance:
 *     - The melee distance away from the target location in addition to the
 *       battler's width.
 *
 *   Offset Adjustment:
 *   - Makes adjustments to offset values to determine which direction to
 *     adjust the destination by.
 *
 *     Offset: X:
 *     - Horizontal offset to move.
 *     - You may use JavaScript code.
 *
 *     Offset: Y:
 *     - Vertical offset to move.
 *     - You may use JavaScript code.
 *
 *   Duration:
 *   - Duration in frames for total movement amount.
 *
 *   Face Destination?:
 *   - Turn and face the destination?
 *
 *   Movement Easing:
 *   - Select which easing type you wish to apply.
 *   - Requires VisuMZ_0_CoreEngine.
 *
 *   Movement Motion:
 *   - Play this motion for the unit(s).
 *
 *   Wait For Movement?:
 *   - Wait for movement to complete before performing next command?
 *
 * ---
 *
 * MOVE: Opacity
 * - Causes the unit(s) to change opacity.
 * - Sideview-only!
 *
 *   Targets:
 *   - Select which unit(s) to change opacity.
 *
 *   Desired Opacity:
 *   - Change to this opacity value.
 *   - You may use JavaScript code.
 *
 *   Duration:
 *   - Duration in frames for opacity change.
 *
 *   Opacity Easing:
 *   - Select which easing type you wish to apply.
 *   - Requires VisuMZ_0_CoreEngine.
 *
 *   Wait For Opacity?:
 *   - Wait for opacity changes to complete before performing next command?
 *
 * ---
 *
 * MOVE: Scale/Grow/Shrink
 * - Causes the unit(s) to scale, grow, or shrink?.
 * - Sideview-only!
 *
 *   Targets:
 *   - Select which unit(s) to change the scale of.
 *
 *   Scale X:
 *   Scale Y:
 *   - What target scale value do you want?
 *   - 1.0 is normal size.
 *
 *   Duration:
 *   - Duration in frames to scale for.
 *
 *   Scale Easing:
 *   - Select which easing type you wish to apply.
 *   - Requires VisuMZ_0_CoreEngine.
 *
 *   Wait For Scale?:
 *   - Wait for scaling to complete before performing next command?
 *
 * ---
 *
 * MOVE: Skew/Distort
 * - Causes the unit(s) to skew.
 * - Sideview-only!
 *
 *   Targets:
 *   - Select which unit(s) to skew.
 *
 *   Skew X:
 *   Skew Y:
 *   - What variance to skew?
 *   - Use small values for the best results.
 *
 *   Duration:
 *   - Duration in frames to skew for.
 *
 *   Skew Easing:
 *   - Select which easing type you wish to apply.
 *   - Requires VisuMZ_0_CoreEngine.
 *
 *   Wait For Skew?:
 *   - Wait for skew to complete before performing next command?
 *
 * ---
 *
 * MOVE: Spin/Rotate
 * - Causes the unit(s) to spin.
 * - Sideview-only!
 *
 *   Targets:
 *   - Select which unit(s) to spin.
 *
 *   Angle:
 *   - How many degrees to spin?
 *
 *   Duration:
 *   - Duration in frames to spin for.
 *
 *   Spin Easing:
 *   - Select which easing type you wish to apply.
 *   - Requires VisuMZ_0_CoreEngine.
 * 
 *   Revert Angle on Finish:
 *   - Upon finishing the spin, revert the angle back to 0.
 *
 *   Wait For Spin?:
 *   - Wait for spin to complete before performing next command?
 *
 * ---
 *
 * MOVE: Wait For Float
 * - Waits for floating to complete before performing next command.
 *
 * ---
 *
 * MOVE: Wait For Jump
 * - Waits for jumping to complete before performing next command.
 *
 * ---
 *
 * MOVE: Wait For Movement
 * - Waits for movement to complete before performing next command.
 *
 * ---
 *
 * MOVE: Wait For Opacity
 * - Waits for opacity changes to complete before performing next command.
 *
 * ---
 *
 * MOVE: Wait For Scale
 * - Waits for scaling to complete before performing next command.
 *
 * ---
 *
 * MOVE: Wait For Skew
 * - Waits for skewing to complete before performing next command.
 *
 * ---
 *
 * MOVE: Wait For Spin
 * - Waits for spinning to complete before performing next command.
 *
 * ---
 * 
 * === Action Sequences - Projectiles ===
 * 
 * Create projectiles on the screen and fire them off at a target.
 * Requires VisuMZ_3_ActSeqProjectiles!
 * 
 * ---
 *
 * PROJECTILE: Animation
 * - Create an animation projectile and fire it at a target.
 * - Requires VisuMZ_3_ActSeqProjectiles!
 *
 *   Coordinates:
 *
 *     Start Location:
 *     - Settings to determine where the projectile(s) start from.
 *
 *       Type:
 *       - Select where the projectile should start from.
 *         - Target - Start from battler target(s)
 *         - Point - Start from a point on the screen
 *
 *         Target(s):
 *         - Select which unit(s) to start the projectile from.
 *
 *           Centralize:
 *           - Create one projectile at the center of the targets?
 *           - Or create a projectile for each target?
 *
 *         Point X:
 *         Point Y:
 *         - Insert the X/Y coordinate to start the projectile at.
 *         - You may use JavaScript code.
 *
 *       Offset X:
 *       Offset Y:
 *       - Insert how many pixels to offset the X/Y coordinate by.
 *       - You may use JavaScript code.
 *
 *     Goal Location:
 *     - Settings to determine where the projectile(s) start from.
 *
 *       Type:
 *       - Select where the projectile should go to.
 *         - Target - Goal is battler target(s)
 *         - Point - Goal is a point on the screen
 *
 *         Target(s):
 *         - Select which unit(s) for projectile to go to.
 *
 *           Centralize:
 *           - Create one projectile at the center of the targets?
 *           - Or create a projectile for each target?
 *
 *         Point X:
 *         Point Y:
 *         - Insert the X/Y coordinate to send the projectile to.
 *         - You may use JavaScript code.
 *
 *       Offset X:
 *       Offset Y:
 *       - Insert how many pixels to offset the X/Y coordinate by.
 *       - You may use JavaScript code.
 *
 *   Settings:
 *
 *     Animation ID:
 *     - Determine which animation to use as a projectile.
 *
 *     Duration:
 *     - Duration for the projectile(s) to travel.
 *
 *     Wait For Projectile?:
 *     - Wait for projectile(s) to reach their destination before going onto
 *       the next command?
 *
 *     Extra Settings:
 *     - Add extra settings to the projectile?
 *
 *       Auto Angle?:
 *       - Automatically angle the projectile to tilt the direction
 *         it's moving?
 *
 *       Angle Offset:
 *       - Alter the projectile's tilt by this many degrees.
 *
 *       Arc Peak:
 *       - This is the height of the project's trajectory arc in pixels.
 *
 *       Easing:
 *       - Select which easing type to apply to the projectile's trajectory.
 *
 *       Spin Speed:
 *       - Determine how much angle the projectile spins per frame.
 *       - Does not work well with "Auto Angle".
 *
 * ---
 *
 * PROJECTILE: Icon
 * - Create an icon projectile and fire it at a target.
 * - Requires VisuMZ_3_ActSeqProjectiles!
 *
 *   Coordinates:
 *
 *     Start Location:
 *     - Settings to determine where the projectile(s) start from.
 *
 *       Type:
 *       - Select where the projectile should start from.
 *         - Target - Start from battler target(s)
 *         - Point - Start from a point on the screen
 *
 *         Target(s):
 *         - Select which unit(s) to start the projectile from.
 *
 *           Centralize:
 *           - Create one projectile at the center of the targets?
 *           - Or create a projectile for each target?
 *
 *         Point X:
 *         Point Y:
 *         - Insert the X/Y coordinate to start the projectile at.
 *         - You may use JavaScript code.
 *
 *       Offset X:
 *       Offset Y:
 *       - Insert how many pixels to offset the X/Y coordinate by.
 *       - You may use JavaScript code.
 *
 *     Goal Location:
 *     - Settings to determine where the projectile(s) start from.
 *
 *       Type:
 *       - Select where the projectile should go to.
 *         - Target - Goal is battler target(s)
 *         - Point - Goal is a point on the screen
 *
 *         Target(s):
 *         - Select which unit(s) for projectile to go to.
 *
 *           Centralize:
 *           - Create one projectile at the center of the targets?
 *           - Or create a projectile for each target?
 *
 *         Point X:
 *         Point Y:
 *         - Insert the X/Y coordinate to send the projectile to.
 *         - You may use JavaScript code.
 *
 *       Offset X:
 *       Offset Y:
 *       - Insert how many pixels to offset the X/Y coordinate by.
 *       - You may use JavaScript code.
 *
 *   Settings:
 *
 *     Icon:
 *     - Determine which icon to use as a projectile.
 *       - You may use JavaScript code.
 *
 *     Duration:
 *     - Duration for the projectile(s) to travel.
 *
 *     Wait For Projectile?:
 *     - Wait for projectile(s) to reach their destination before going onto
 *       the next command?
 *
 *     Extra Settings:
 *     - Add extra settings to the projectile?
 *
 *       Auto Angle?:
 *       - Automatically angle the projectile to tilt the direction
 *         it's moving?
 *
 *       Angle Offset:
 *       - Alter the projectile's tilt by this many degrees.
 *
 *       Arc Peak:
 *       - This is the height of the project's trajectory arc in pixels.
 *
 *       Blend Mode:
 *       - What kind of blend mode do you wish to apply to the projectile?
 *         - Normal
 *         - Additive
 *         - Multiply
 *         - Screen
 *
 *       Easing:
 *       - Select which easing type to apply to the projectile's trajectory.
 *
 *       Hue:
 *       - Adjust the hue of the projectile.
 *       - Insert a number between 0 and 360.
 *
 *       Scale:
 *       - Adjust the size scaling of the projectile.
 *       - Use decimals for exact control.
 *
 *       Spin Speed:
 *       - Determine how much angle the projectile spins per frame.
 *       - Does not work well with "Auto Angle".
 *
 * ---
 *
 * PROJECTILE: Picture
 * - Create a picture projectile and fire it at a target.
 * - Requires VisuMZ_3_ActSeqProjectiles!
 *
 *   Coordinates:
 *
 *     Start Location:
 *     - Settings to determine where the projectile(s) start from.
 *
 *       Type:
 *       - Select where the projectile should start from.
 *         - Target - Start from battler target(s)
 *         - Point - Start from a point on the screen
 *
 *         Target(s):
 *         - Select which unit(s) to start the projectile from.
 *
 *           Centralize:
 *           - Create one projectile at the center of the targets?
 *           - Or create a projectile for each target?
 *
 *         Point X:
 *         Point Y:
 *         - Insert the X/Y coordinate to start the projectile at.
 *         - You may use JavaScript code.
 *
 *       Offset X:
 *       Offset Y:
 *       - Insert how many pixels to offset the X/Y coordinate by.
 *       - You may use JavaScript code.
 *
 *     Goal Location:
 *     - Settings to determine where the projectile(s) start from.
 *
 *       Type:
 *       - Select where the projectile should go to.
 *         - Target - Goal is battler target(s)
 *         - Point - Goal is a point on the screen
 *
 *         Target(s):
 *         - Select which unit(s) for projectile to go to.
 *
 *           Centralize:
 *           - Create one projectile at the center of the targets?
 *           - Or create a projectile for each target?
 *
 *         Point X:
 *         Point Y:
 *         - Insert the X/Y coordinate to send the projectile to.
 *         - You may use JavaScript code.
 *
 *       Offset X:
 *       Offset Y:
 *       - Insert how many pixels to offset the X/Y coordinate by.
 *       - You may use JavaScript code.
 *
 *   Settings:
 *
 *     Picture Filename:
 *     - Determine which picture to use as a projectile.
 *
 *     Duration:
 *     - Duration for the projectile(s) to travel.
 *
 *     Wait For Projectile?:
 *     - Wait for projectile(s) to reach their destination before going onto
 *       the next command?
 *
 *     Extra Settings:
 *     - Add extra settings to the projectile?
 *
 *       Auto Angle?:
 *       - Automatically angle the projectile to tilt the direction
 *         it's moving?
 *
 *       Angle Offset:
 *       - Alter the projectile's tilt by this many degrees.
 *
 *       Arc Peak:
 *       - This is the height of the project's trajectory arc in pixels.
 *
 *       Blend Mode:
 *       - What kind of blend mode do you wish to apply to the projectile?
 *         - Normal
 *         - Additive
 *         - Multiply
 *         - Screen
 *
 *       Easing:
 *       - Select which easing type to apply to the projectile's trajectory.
 *
 *       Hue:
 *       - Adjust the hue of the projectile.
 *       - Insert a number between 0 and 360.
 *
 *       Scale:
 *       - Adjust the size scaling of the projectile.
 *       - Use decimals for exact control.
 *
 *       Spin Speed:
 *       - Determine how much angle the projectile spins per frame.
 *       - Does not work well with "Auto Angle".
 *
 * ---
 * 
 * === Action Sequences - Skew ===
 * 
 * These action sequences allow you to have control over the camera skew.
 * Requires VisuMZ_3_ActSeqCamera!
 * 
 * ---
 *
 * SKEW: Change Skew
 * - Changes the camera skew.
 * - Requires VisuMZ_3_ActSeqCamera!
 *
 *   Skew X:
 *   - Change the camera skew X to this value.
 *
 *   Skew Y:
 *   - Change the camera skew Y to this value.
 *
 *   Duration:
 *   - Duration in frames to change camera skew.
 *
 *   Skew Easing:
 *   - Select which easing type you wish to apply.
 *   - Requires VisuMZ_0_CoreEngine.
 *
 *   Wait For Skew?:
 *   - Wait for skew changes to complete before performing next command?
 *
 * ---
 *
 * SKEW: Reset Skew
 * - Reset any skew settings.
 * - Requires VisuMZ_3_ActSeqCamera!
 *
 *   Duration:
 *   - Duration in frames to reset camera skew.
 *
 *   Skew Easing:
 *   - Select which easing type you wish to apply.
 *   - Requires VisuMZ_0_CoreEngine.
 *
 *   Wait For Skew?:
 *   - Wait for skew changes to complete before performing next command?
 *
 * ---
 *
 * SKEW: Wait For Skew
 * - Waits for skew changes to complete before performing next command.
 * - Requires VisuMZ_3_ActSeqCamera!
 *
 * ---
 *
 * === Action Sequences - Target ===
 *
 * If using a manual target by target Action Sequence, these commands will give
 * you full control over its usage.
 *
 * ---
 *
 * TARGET: Current Index
 * - Sets the current index to this value.
 * - Then decide to jump to a label (optional).
 *
 *   Set Index To:
 *   - Sets current targeting index to this value.
 *   - 0 is the starting index of a target group.
 *
 *   Jump To Label:
 *   - If a target is found after the index change, jump to this label in the
 *     Common Event.
 *
 * ---
 *
 * TARGET: Next Target
 * - Moves index forward by 1 to select a new current target.
 * - Then decide to jump to a label (optional).
 *
 *   Jump To Label:
 *   - If a target is found after the index change, jump to this label in the
 *     Common Event.
 *
 * ---
 *
 * TARGET: Previous Target
 * - Moves index backward by 1 to select a new current target.
 * - Then decide to jump to a label (optional).
 *
 *   Jump To Label:
 *   - If a target is found after the index change, jump to this label in the
 *     Common Event.
 *
 * ---
 *
 * TARGET: Random Target
 * - Sets index randomly to determine new currernt target.
 * - Then decide to jump to a label (optional).
 *
 *   Force Random?:
 *   - Index cannot be its previous index amount after random.
 *
 *   Jump To Label:
 *   - If a target is found after the index change, jump to this label in the
 *     Common Event.
 *
 * ---
 *
 * === Action Sequences - Weapon ===
 *
 * Allows for finer control over Dual/Multi Wielding actors.
 * Only works for Actors.
 *
 * ---
 *
 * WEAPON: Clear Weapon Slot
 * - Clears the active weapon slot (making others valid again).
 * - Only works for Actors.
 *
 *   Targets:
 *   - Select unit(s) to clear the active weapon slot for.
 *
 * ---
 *
 * WEAPON: Next Weapon Slot
 * - Goes to next active weapon slot (making others invalid).
 * - If next slot is weaponless, don't label jump.
 *
 *   Targets:
 *   - Select unit(s) to change the next active weapon slot for.
 *
 * ---
 *
 * WEAPON: Set Weapon Slot
 * - Sets the active weapon slot (making others invalid).
 * - Only works for Actors.
 *
 *   Targets:
 *   - Select unit(s) to change the active weapon slot for.
 *
 *   Weapon Slot ID:
 *   - Select weapon slot to make active (making others invalid).
 *   - Use 0 to clear and normalize. You may use JavaScript code.
 *
 * ---
 *
 * === Action Sequences - Zoom ===
 *
 * These Action Sequences are zoom-related.
 * Requires VisuMZ_3_ActSeqCamera!
 *
 * ---
 *
 * ZOOM: Change Scale
 * - Changes the zoom scale.
 * - Requires VisuMZ_3_ActSeqCamera!
 *
 *   Scale:
 *   - The zoom scale to change to.
 *
 *   Duration:
 *   - Duration in frames to reset battle zoom.
 *
 *   Zoom Easing:
 *   - Select which easing type you wish to apply.
 *   - Requires VisuMZ_0_CoreEngine.
 *
 *   Wait For Zoom?
 *   - Wait for zoom changes to complete before performing next command?
 *
 * ---
 *
 * ZOOM: Reset Zoom
 * - Reset any zoom settings.
 * - Requires VisuMZ_3_ActSeqCamera!
 *
 *   Duration:
 *   - Duration in frames to reset battle zoom.
 *
 *   Zoom Easing:
 *   - Select which easing type you wish to apply.
 *   - Requires VisuMZ_0_CoreEngine.
 *
 *   Wait For Zoom?
 *   - Wait for zoom changes to complete before performing next command?
 *
 * ---
 *
 * ZOOM: Wait For Zoom
 * - Waits for zoom changes to complete before performing next command.
 * Requires VisuMZ_3_ActSeqCamera!
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Auto Battle Settings
 * ============================================================================
 *
 * These Plugin Parameter settings allow you to change the aspects added by
 * this plugin that support Auto Battle and the Auto Battle commands.
 *
 * Auto Battle commands can be added to the Party Command Window and/or Actor
 * Command Window. The one used by the Party Command Window will cause the
 * whole party to enter an Auto Battle state until stopped by a button input.
 * The command used by the Actor Command Window, however, will cause the actor
 * to select an action based off the Auto Battle A.I. once for the current turn
 * instead.
 *
 * ---
 *
 * Battle Display
 * 
 *   Message:
 *   - Message that's displayed when Auto Battle is on.
 *     Text codes allowed. %1 - OK button, %2 - Cancel button
 * 
 *   OK Button:
 *   - Text used to represent the OK button.
 *   - If VisuMZ_0_CoreEngine is present, ignore this.
 * 
 *   Cancel Button:
 *   - Text used to represent the Cancel button.
 *   - If VisuMZ_0_CoreEngine is present, ignore this.
 * 
 *   Background Type:
 *   - Select background type for Auto Battle window.
 *     - 0 - Window
 *     - 1 - Dim
 *     - 2 - Transparent
 * 
 *   JS: X, Y, W, H:
 *   - Code used to determine the dimensions for this window.
 *
 * ---
 *
 * Options
 * 
 *   Add Option?:
 *   - Add the Auto Battle options to the Options menu?
 * 
 *   Adjust Window Height:
 *   - Automatically adjust the options window height?
 * 
 *   Startup Name:
 *   - Command name of the option.
 * 
 *   Style Name:
 *   - Command name of the option.
 * 
 *   OFF:
 *   - Text displayed when Auto Battle Style is OFF.
 * 
 *   ON:
 *   - Text displayed when Auto Battle Style is ON.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Damage Settings
 * ============================================================================
 *
 * These Plugin Parameters add a variety of things to how damage is handled in
 * battle. These range from hard damage caps to soft damage caps to how damage
 * popups appear, how the formulas for various aspects are handled and more.
 *
 * Damage Styles are also a feature added through this plugin. More information
 * can be found in the help section above labeled 'Damage Styles'.
 *
 * ---
 *
 * Damage Cap
 * 
 *   Enable Damage Cap?:
 *   - Put a maximum hard damage cap on how far damage can go?
 *   - This can be broken through the usage of notetags.
 * 
 *   Default Hard Cap:
 *   - The default hard damage cap used before applying damage.
 * 
 *   Enable Soft Cap?:
 *   - Soft caps ease in the damage values leading up to the  hard damage cap.
 *   - Requires hard Damage Cap enabled.
 * 
 *     Base Soft Cap Rate:
 *     - The default soft damage cap used before applying damage.
 * 
 *     Soft Scale Constant:
 *     - The default soft damage cap used before applying damage.
 *
 * ---
 *
 * Popups
 * 
 *   Popup Duration:
 *   - Adjusts how many frames a popup stays visible.
 * 
 *   Newest Popups Bottom:
 *   - Puts the newest popups at the bottom.
 * 
 *   Offset X:
 *   Offset Y:
 *   - Sets how much to offset the sprites by horizontally/vertically.
 * 
 *   Shift X:
 *   Shift Y:
 *   - Sets how much to shift the sprites by horizontally/vertically.
 * 
 *   Shift Y:
 * 
 *   Critical Flash Color:
 *   - Adjust the popup's flash color.
 *   - Format: [red, green, blue, alpha]
 * 
 *   Critical Duration:
 *   - Adjusts how many frames a the flash lasts.
 *
 * ---
 *
 * Formulas
 * 
 *   JS: Overall Formula:
 *   - The overall formula used when calculating damage.
 * 
 *   JS: Variance Formula:
 *   - The formula used when damage variance.
 * 
 *   JS: Guard Formula:
 *   - The formula used when damage is guarded.
 *
 * ---
 *
 * Critical Hits
 * 
 *   JS: Rate Formula:
 *   - The formula used to calculate Critical Hit Rates.
 * 
 *   JS: Damage Formula:
 *   - The formula used to calculate Critical Hit Damage modification.
 *
 * ---
 *
 * Damage Styles
 * 
 *   Default Style:
 *   - Which Damage Style do you want to set as default?
 *   - Use 'Manual' to not use any styles at all.
 *     - The 'Manual' style will not support <Armor Penetration> notetags.
 *     - The 'Manual' style will not support <Armor Reduction> notetags.
 * 
 *   Style List:
 *   - A list of the damage styles available.
 *   - These are used to calculate base damage.
 * 
 *     Name:
 *     - Name of this Damage Style.
 *     -Used for notetags and such.
 * 
 *     JS: Formula:
 *     - The base formula for this Damage Style.
 * 
 *     Items & Equips Core:
 * 
 *       HP Damage:
 *       MP Damage:
 *       HP Recovery:
 *       MP Recovery:
 *       HP Drain:
 *       MP Drain:
 *       - Vocabulary used for this data entry.
 * 
 *       JS: Damage Display:
 *       - Code used the data displayed for this category.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Mechanics Settings
 * ============================================================================
 *
 * Some of the base settings for the various mechanics found in the battle
 * system can be altered here in these Plugin Parameters. Most of these will
 * involve JavaScript code and require you to have to good understanding of
 * how the RPG Maker MZ code works before tampering with it.
 *
 * ---
 *
 * Action Speed
 * 
 *   Allow Random Speed?:
 *   - Allow speed to be randomized base off the user's AGI?
 * 
 *   JS: Calculate:
 *   - Code used to calculate action speed.
 *
 * ---
 *
 * Base Troop
 * 
 *   Base Troop ID's:
 *   - Select the Troop ID(s) to duplicate page events from for all
 *     other troops.
 *   - More information can be found in the dedicated Help section above.
 *
 * ---
 *
 * Escape
 * 
 *   JS: Calc Escape Ratio:
 *   - Code used to calculate the escape success ratio.
 * 
 *   JS: Calc Escape Raise:
 *   - Code used to calculate how much the escape success ratio raises upon
 *     each failure.
 * 
 * ---
 * 
 * Common Events (on Map)
 * 
 *   Pre-Battle Event:
 *   Post-Battle Event:
 *   Victory Event:
 *   Defeat Event:
 *   Escape Success Event:
 *   Escape Fail Event:
 *   - Queued Common Event to run upon meeting the condition.
 *   - Use to 0 to not run any Common Event at all.
 *   - "Post-Battle Event" will always run regardless.
 *   - If any events are running before the battle, they will continue running
 *     to the end first before the queued Common Events will run.
 *   - These common events only run on the map scene. They're not meant to run
 *     in the battle scene.
 *   - If the "Defeat Event" has a common event attached to it, then random
 *     encounters will be changed to allow defeat without being sent to the
 *     Game Over scene. Instead, the game will send the player to the map scene
 *     where the Defeat Event will run.
 *
 * ---
 *
 * JS: Battle-Related
 * 
 *   JS: Pre-Start Battle:
 *   - Target function: BattleManager.startBattle()
 *   - JavaScript code occurs before function is run.
 * 
 *   JS: Post-Start Battle:
 *   - Target function: BattleManager.startBattle()
 *   - JavaScript code occurs after function is run.
 * 
 *   JS: Battle Victory:
 *   - Target function: BattleManager.processVictory()
 *   - JavaScript code occurs before function is run.
 * 
 *   JS: Escape Success:
 *   - Target function: BattleManager.onEscapeSuccess()
 *   - JavaScript code occurs before function is run.
 * 
 *   JS: Escape Failure:
 *   - Target function: BattleManager.onEscapeFailure()
 *   - JavaScript code occurs before function is run.
 * 
 *   JS: Battle Defeat:
 *   - Target function: BattleManager.processDefeat()
 *   - JavaScript code occurs before function is run.
 * 
 *   JS: Pre-End Battle:
 *   - Target function: BattleManager.endBattle()
 *   - JavaScript code occurs before function is run.
 * 
 *   JS: Post-End Battle:
 *   - Target function: BattleManager.endBattle()
 *   - JavaScript code occurs after function is run.
 *
 * ---
 *
 * JS: Turn-Related
 * 
 *   JS: Pre-Start Turn:
 *   - Target function: BattleManager.startTurn()
 *   - JavaScript code occurs before function is run.
 * 
 *   JS: Post-Start Turn:
 *   - Target function: BattleManager.startTurn()
 *   - JavaScript code occurs after function is run.
 * 
 *   JS: Pre-End Turn:
 *   - Target function: Game_Battler.prototype.onTurnEnd()
 *   - JavaScript code occurs before function is run.
 * 
 *   JS: Post-End Turn:
 *   - Target function: Game_Battler.prototype.onTurnEnd()
 *   - JavaScript code occurs after function is run.
 * 
 *   JS: Pre-Regenerate:
 *   - Target function: Game_Battler.prototype.regenerateAll()
 *   - JavaScript code occurs before function is run.
 * 
 *   JS: Post-Regenerate:
 *   - Target function: Game_Battler.prototype.regenerateAll()
 *   - JavaScript code occurs after function is run.
 *
 * ---
 *
 * JS: Action-Related
 * 
 *   JS: Pre-Start Action:
 *   - Target function: BattleManager.startAction()
 *   - JavaScript code occurs before function is run.
 * 
 *   JS: Post-Start Action:
 *   - Target function: BattleManager.startAction()
 *   - JavaScript code occurs after function is run.
 * 
 *   JS: Pre-Apply:
 *   - Target function: Game_Action.prototype.apply()
 *   - JavaScript code occurs before function is run.
 * 
 *   JS: Pre-Damage:
 *   - Target function: Game_Action.prototype.executeDamage()
 *   - JavaScript code occurs before function is run.
 * 
 *   JS: Post-Damage:
 *   - Target function: Game_Action.prototype.executeDamage()
 *   - JavaScript code occurs after function is run.
 * 
 *   JS: Post-Apply:
 *   - Target function: Game_Action.prototype.apply()
 *   - JavaScript code occurs after function is run.
 * 
 *   JS: Pre-End Action:
 *   - Target function: BattleManager.endAction()
 *   - JavaScript code occurs before function is run.
 * 
 *   JS: Post-End Action:
 *   - DescriTarget function: BattleManager.endAction()
 *   - JavaScript code occurs after function is run.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Battle Layout Settings
 * ============================================================================
 *
 * The Battle Layout Settings Plugin Parameter gives you control over the look,
 * style, and appearance of certain UI elements. These range from the way the
 * Battle Status Window presents its information to the way certain windows
 * like the Party Command Window and Actor Command Window appear.
 *
 * ---
 *
 * Battle Layout Style
 * - The style used for the battle layout.
 * 
 *   Default:
 *   - Shows actor faces in Battle Status.
 * 
 *   List:
 *   - Lists actors in Battle Status.
 * 
 *   XP:
 *   - Shows actor battlers in a stretched Battle Status.
 * 
 *   Portrait:
 *   - Shows portraits in a stretched Battle Status.
 * 
 *   Border:
 *   - Displays windows around the screen border.
 *
 * ---
 *
 * List Style
 * 
 *   Show Faces:
 *   - Shows faces in List Style?
 * 
 *   Command Window Width:
 *   - Determine the window width for the Party and Actor Command Windows.
 *   - Affects Default and List Battle Layout styles.
 *
 * ---
 *
 * XP Style
 * 
 *   Command Lines:
 *   - Number of action lines in the Actor Command Window for the XP Style.
 * 
 *   Sprite Height:
 *   - Default sprite height used when if the sprite's height has not been
 *     determined yet.
 * 
 *   Sprite Base Location:
 *   - Determine where the sprite is located on the Battle Status Window.
 *     - Above Name - Sprite is located above the name.
 *     - Bottom - Sprite is located at the bottom of the window.
 *     - Centered - Sprite is centered in the window.
 *     - Top - Sprite is located at the top of the window.
 *
 * ---
 *
 * Portrait Style
 * 
 *   Show Portraits?:
 *   - Requires VisuMZ_1_MainMenuCore.
 *   - Shows the actor's portrait instead of a face.
 * 
 *   Portrait Scaling:
 *   - If portraits are used, scale them by this much.
 *
 * ---
 *
 * Border Style
 * 
 *   Columns:
 *   - The total number of columns for Skill & Item Windows in the battle scene
 * 
 *   Show Portraits?:
 *   - Requires VisuMZ_1_MainMenuCore.
 *   - Shows the actor's portrait at the edge of the screen.
 * 
 *   Portrait Scaling:
 *   - If portraits are used, scale them by this much.
 *
 * ---
 *
 * Skill & Item Windows
 * 
 *   Middle Layout:
 *   - Shows the Skill & Item Windows in mid-screen?
 * 
 *   Columns:
 *   - The total number of columns for Skill & Item Windows in the battle scene
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Battle Log Settings
 * ============================================================================
 *
 * These Plugin Parameters give you control over how the Battle Log Window, the
 * window shown at the top of the screen in the battle layout, appears, its
 * various properties, and which text will be displayed.
 *
 * The majority of the text has been disabled by default with this plugin to
 * make the flow of battle progress faster.
 *
 * ---
 *
 * General
 * 
 *   Back Color:
 *   - Use #rrggbb for a hex color.
 * 
 *   Max Lines:
 *   - Maximum number of lines to be displayed.
 * 
 *   Message Wait:
 *   - Number of frames for a usual message wait.
 * 
 *   Text Align:
 *   - Text alignment for the Window_BattleLog.
 * 
 *   JS: X, Y, W, H:
 *   - Code used to determine the dimensions for the battle log.
 *
 * ---
 *
 * Start Turn
 * 
 *   Show Start Turn?:
 *   - Display turn changes at the start of the turn?
 * 
 *   Start Turn Message:
 *   - Message displayed at turn start.
 *   - %1 - Turn Count
 * 
 *   Start Turn Wait:
 *   - Number of frames to wait after a turn started.
 *
 * ---
 *
 * Display Action
 * 
 *   Show Centered Action?:
 *   - Display a centered text of the action name?
 * 
 *   Show Skill Message 1?:
 *   - Display the 1st skill message?
 * 
 *   Show Skill Message 2?:
 *   - Display the 2nd skill message?
 * 
 *   Show Item Message?:
 *   - Display the item use message?
 *
 * ---
 *
 * Action Changes
 * 
 *   Show Counter?:
 *   - Display counter text?
 * 
 *   Show Reflect?:
 *   - Display magic reflection text?
 * 
 *   Show Substitute?:
 *   - Display substitute text?
 *
 * ---
 *
 * Action Results
 * 
 *   Show No Effect?:
 *   - Display no effect text?
 * 
 *   Show Critical?:
 *   - Display critical text?
 * 
 *   Show Miss/Evasion?:
 *   - Display miss/evasion text?
 * 
 *   Show HP Damage?:
 *   - Display HP Damage text?
 * 
 *   Show MP Damage?:
 *   - Display MP Damage text?
 * 
 *   Show TP Damage?:
 *   - Display TP Damage text?
 *
 * ---
 *
 * Display States
 * 
 *   Show Added States?:
 *   - Display added states text?
 * 
 *   Show Removed States?:
 *   - Display removed states text?
 * 
 *   Show Current States?:
 *   - Display the currently affected state text?
 * 
 *   Show Added Buffs?:
 *   - Display added buffs text?
 * 
 *   Show Added Debuffs?:
 *   - Display added debuffs text?
 * 
 *   Show Removed Buffs?:
 *   - Display removed de/buffs text?
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Battleback Scaling Settings
 * ============================================================================
 *
 * By default, the battlebacks in RPG Maker MZ scale as if the screen size is
 * a static 816x624 resolution, which isn't always the case. These settings
 * here allow you to dictate how you want the battlebacks to scale for the
 * whole game. These settings CANNOT be changed midgame or per battle.
 *
 * ---
 *
 * Settings
 * 
 *   Default Style:
 *   - The default scaling style used for battlebacks.
 *   - MZ (MZ's default style)
 *   - 1:1 (No Scaling)
 *   - Scale To Fit (Scale to screen size)
 *   - Scale Down (Scale Downward if Larger than Screen)
 *   - Scale Up (Scale Upward if Smaller than Screen)
 * 
 *   JS: 1:1:
 *   JS: Scale To Fit:
 *   JS: Scale Down:
 *   JS: Scale Up:
 *   JS: 1:1:
 *   JS: 1:1:
 *   - This code gives you control over the scaling for this style.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Party Command Window
 * ============================================================================
 *
 * These Plugin Parameters allow you control over how the Party Command Window
 * operates in the battle scene. You can turn disable it from appearing or make
 * it so that it doesn't 
 *
 * ---
 *
 * Command Window
 * 
 *   Style:
 *   - How do you wish to draw commands in the Party Command Window?
 *   - Text Only: Display only the text.
 *   - Icon Only: Display only the icon.
 *   - Icon + Text: Display the icon first, then the text.
 *   - Auto: Determine which is better to use based on the size of the cell.
 * 
 *   Text Align:
 *   - Text alignment for the Party Command Window.
 * 
 *   Fight Icon:
 *   - The icon used for the Fight command.
 * 
 *   Add Auto Battle?:
 *   - Add the "Auto Battle" command to the Command Window?
 * 
 *     Auto Battle Icon:
 *     - The icon used for the Auto Battle command.
 * 
 *     Auto Battle Text:
 *     - The text used for the Auto Battle command.
 * 
 *   Add Options?:
 *   - Add the "Options" command to the Command Window?
 * 
 *     Options Icon:
 *     - The icon used for the Options command.
 * 
 *     Active TPB Message:
 *     - Message that will be displayed when selecting options during the
 *       middle of an action.
 * 
 *   Escape Icon:
 *   - The icon used for the Escape command.
 *
 * ---
 *
 * Access
 * 
 *   Skip Party Command:
 *   - DTB: Skip Party Command selection on turn start.
 *   - TPB: Skip Party Command selection at battle start.
 * 
 *   Disable Party Command:
 *   - Disable the Party Command Window entirely?
 *
 * ---
 *
 * Help Window
 * 
 *   Fight:
 *   - Text displayed when selecting a skill type.
 *   - %1 - Skill Type Name
 * 
 *   Auto Battle:
 *   - Text displayed when selecting the Auto Battle command.
 * 
 *   Options:
 *   - Text displayed when selecting the Options command.
 * 
 *   Escape:
 *   - Text displayed when selecting the escape command.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Actor Command Window
 * ============================================================================
 *
 * These Plugin Parameters allow you to change various aspects regarding the
 * Actor Command Window and how it operates in the battle scene. This ranges
 * from how it appears to the default battle commands given to all players
 * without a custom <Battle Commands> notetag.
 *
 * ---
 *
 * Command Window
 * 
 *   Style:
 *   - How do you wish to draw commands in the Actor Command Window?
 *   - Text Only: Display only the text.
 *   - Icon Only: Display only the icon.
 *   - Icon + Text: Display the icon first, then the text.
 *   - Auto: Determine which is better to use based on the size of the cell.
 * 
 *   Text Align:
 *   - Text alignment for the Actor Command Window.
 * 
 *   Item Icon:
 *   - The icon used for the Item command.
 * 
 *   Normal SType Icon:
 *   - Icon used for normal skill types that aren't assigned any icons.
 *   - Ignore if VisuMZ_1_SkillsStatesCore is installed.
 * 
 *   Magic SType Icon:
 *   - Icon used for magic skill types that aren't assigned any icons.
 *   - Ignore if VisuMZ_1_SkillsStatesCore is installed.
 *
 * ---
 *
 * Battle Commands
 * 
 *   Command List:
 *   - List of battle commands that appear by default if the <Battle Commands>
 *     notetag isn't present.
 *
 *     - Attack 
 *       - Adds the basic attack command.
 * 
 *     - Skills
 *       - Displays all the skill types available to the actor.
 * 
 *     - SType: x
 *     - Stype: name
 *       - Adds in a specific skill type.
 *       - Replace 'x' with the ID of the skill type.
 *       - Replace 'name' with the name of the skill type (without text codes).
 *
 *     - All Skills
 *       - Adds all usable battle skills as individual actions.
 * 
 *     - Skill: x
 *     - Skill: name
 *       - Adds in a specific skill as a usable action.
 *       - Replace 'x' with the ID of the skill.
 *       - Replace 'name' with the name of the skill.
 * 
 *     - Guard
 *       - Adds the basic guard command.
 * 
 *     - Item
 *       - Adds the basic item command.
 * 
 *     - Escape
 *       - Adds the escape command.
 * 
 *     - Auto Battle
 *       - Adds the auto battle command.
 *
 * ---
 *
 * Help Window
 * 
 *   Skill Types:
 *   - Text displayed when selecting a skill type.
 *   - %1 - Skill Type Name
 * 
 *   Items:
 *   - Text displayed when selecting the item command.
 * 
 *   Escape:
 *   - Text displayed when selecting the escape command.
 * 
 *   Auto Battle:
 *   - Text displayed when selecting the Auto Battle command.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Actor Battler Settings
 * ============================================================================
 *
 * These Plugin Parameter settings adjust how the sideview battlers behave for
 * the actor sprites. Some of these settings are shared with enemies if they
 * use sideview battler graphics.
 *
 * ---
 *
 * Flinch
 * 
 *   Flinch Distance X:
 *   - The normal X distance when flinching.
 * 
 *   Flinch Distance Y:
 *   - The normal Y distance when flinching.
 * 
 *   Flinch Duration:
 *   - The number of frames for a flinch to complete.
 *
 * ---
 *
 * Sideview Battlers
 * 
 *   Anchor: X:
 *   - Default X anchor for Sideview Battlers.
 * 
 *   Anchor: Y:
 *   - Default Y anchor for Sideview Battlers.
 * 
 *   Chant Style:
 *   - What determines the chant motion?
 *   - Hit type or skill type?
 * 
 *   Offset X:
 *   - Offsets X position where actor is positioned.
 *   - Negative values go left. Positive values go right.
 * 
 *   Offset Y:
 *   - Offsets Y position where actor is positioned.
 *   - Negative values go up. Positive values go down.
 * 
 *   Motion Speed:
 *   - The number of frames in between each motion.
 * 
 *   Priority: Active:
 *   - Place the active actor on top of actor and enemy sprites.
 * 
 *   Priority: Actors:
 *   - Prioritize actors over enemies when placing sprites on top of each other
 * 
 *   Shadow Visible:
 *   - Show or hide the shadow for Sideview Battlers.
 * 
 *   Smooth Image:
 *   - Smooth out the battler images or pixelate them?
 * 
 *   JS: Home Position:
 *   - Code used to calculate the home position of actors.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Enemy Battler Settings
 * ============================================================================
 *
 * These Plugin Parameter settings adjust how enemies appear visually in the
 * battle scene. Some of these settings will override the settings used for
 * actors if used as sideview battlers. Other settings include changing up the
 * default attack animation for enemies, how the enemy select window functions,
 * and more.
 *
 * ---
 *
 * Visual
 * 
 *   Attack Animation:
 *   - Default attack animation used for enemies.
 *   - Use <Attack Animation: x> for custom animations.
 * 
 *   Emerge Text:
 *   - Show or hide the 'Enemy emerges!' text at the start of battle.
 * 
 *   Offset X:
 *   - Offsets X position where enemy is positioned.
 *   - Negative values go left. Positive values go right.
 * 
 *   Offset Y:
 *   - Offsets Y position where enemy is positioned.
 *   - Negative values go up. Positive values go down.
 * 
 *   Smooth Image:
 *   - Smooth out the battler images or pixelate them?
 *
 * ---
 *
 * Select Window
 * 
 *   FV: Right Priority:
 *   - If using frontview, auto select the enemy furthest right.
 * 
 *   SV: Right Priority:
 *   - If using sideview, auto select the enemy furthest right.
 * 
 *   Name: Font Size:
 *   - Font size used for enemy names.
 * 
 *   Name: Offset X:
 *   Name: Offset Y:
 *   - Offset the enemy name's position by this much.
 *
 * ---
 *
 * Sideview Battlers
 * 
 *   Allow Collapse:
 *   - Causes defeated enemies with SV Battler graphics to "fade away"
 *     when defeated?
 * 
 *   Anchor: X:
 *   - Default X anchor for Sideview Battlers.
 *   - Use values between 0 and 1 to be safe.
 * 
 *   Anchor: Y:
 *   - Default Y anchor for Sideview Battlers.
 *   - Use values between 0 and 1 to be safe.
 * 
 *   Motion: Idle:
 *   - Sets default idle animation used by Sideview Battlers.
 * 
 *   Shadow Visible:
 *   - Show or hide the shadow for Sideview Battlers.
 * 
 *   Size: Width:
 *   - Default width for enemies that use Sideview Battlers.
 * 
 *   Size: Height:
 *   - Default height for enemies that use Sideview Battlers.
 * 
 *   Weapon Type:
 *   - Sets default weapon type used by Sideview Battlers.
 *   - Use 0 for Bare Hands.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: HP Gauge Settings
 * ============================================================================
 *
 * Settings that adjust the visual HP Gauge displayed in battle.
 *
 * ---
 *
 * Show Gauges For
 * 
 *   Actors:
 *   - Show HP Gauges over the actor sprites' heads?
 *   - Requires SV Actors to be visible.
 * 
 *   Enemies:
 *   - Show HP Gauges over the enemy sprites' heads?
 *   - Can be bypassed with <Hide HP Gauge> notetag.
 * 
 *     Requires Defeat?:
 *     - Requires defeating the enemy once to show HP Gauge?
 *     - Can be bypassed with <Show HP Gauge> notetag.
 * 
 *       Battle Test Bypass?:
 *       - Bypass the defeat requirement in battle test?
 *
 * ---
 *
 * Settings
 * 
 *   Anchor X:
 *   Anchor Y:
 *   - Where do you want the HP Gauge sprite's anchor X/Y to be?
 *     Use values between 0 and 1 to be safe.
 * 
 *   Scale:
 *   - How large/small do you want the HP Gauge to be scaled?
 * 
 *   Offset X:
 *   Offset Y:
 *   - How many pixels to offset the HP Gauge's X/Y by?
 *
 * ---
 *
 * Options
 * 
 *   Add Option?:
 *   - Add the 'Show HP Gauge' option to the Options menu?
 * 
 *   Adjust Window Height:
 *   - Automatically adjust the options window height?
 * 
 *   Option Name:
 *   - Command name of the option.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Action Sequence Settings
 * ============================================================================
 *
 * Action Sequence Plugin Parameters allow you to decide if you want automatic
 * Action Sequences to be used for physical attacks, the default casting
 * animations used, how counters and reflects appear visually, and what the
 * default stepping distances are.
 *
 * ---
 *
 * Automatic Sequences
 * 
 *   Melee Single Target:
 *   - Allow this auto sequence for physical, single target actions?
 * 
 *   Melee Multi Target:
 *   - Allow this auto sequence for physical, multi-target actions?
 *
 * ---
 * 
 * Quality of Life
 * 
 *   Auto Notetag:
 *   - Automatically apply the <Custom Action Sequence> notetag effect to any
 *     item or skill that has a Common Event?
 *   - Any item or skill without a Common Event attached to it will use the
 *     Automatic Action Sequences instead.
 *   - The <Auto Action Sequence> notetag will disable this effect for that
 *     particular skill or item.
 * 
 * ---
 *
 * Cast Animations
 * 
 *   Certain Hit:
 *   - Cast animation for Certain Hit skills.
 * 
 *   Physical:
 *   - Cast animation for Physical skills.
 * 
 *   Magical:
 *   - Cast animation for Magical skills.
 *
 * ---
 *
 * Counter/Reflect
 * 
 *   Counter Back:
 *   - Play back the attack animation used?
 * 
 *   Reflect Animation:
 *   - Animation played when an action is reflected.
 * 
 *   Reflect Back:
 *   - Play back the attack animation used?
 *
 * ---
 *
 * Stepping
 * 
 *   Melee Distance:
 *   - Minimum distance in pixels for Movement Action Sequences.
 * 
 *   Step Distance X:
 *   - The normal X distance when stepping forward.
 * 
 *   Step Distance Y:
 *   - The normal Y distance when stepping forward.
 * 
 *   Step Duration:
 *   - The number of frames for a stepping action to complete.
 *
 * ---
 *
 * ============================================================================
 * Terms of Use
 * ============================================================================
 *
 * 1. These plugins may be used in free or commercial games provided that they
 * have been acquired through legitimate means at VisuStella.com and/or any
 * other official approved VisuStella sources. Exceptions and special
 * circumstances that may prohibit usage will be listed on VisuStella.com.
 * 
 * 2. All of the listed coders found in the Credits section of this plugin must
 * be given credit in your games or credited as a collective under the name:
 * "VisuStella".
 * 
 * 3. You may edit the source code to suit your needs, so long as you do not
 * claim the source code belongs to you. VisuStella also does not take
 * responsibility for the plugin if any changes have been made to the plugin's
 * code, nor does VisuStella take responsibility for user-provided custom code
 * used for custom control effects including advanced JavaScript notetags
 * and/or plugin parameters that allow custom JavaScript code.
 * 
 * 4. You may NOT redistribute these plugins nor take code from this plugin to
 * use as your own. These plugins and their code are only to be downloaded from
 * VisuStella.com and other official/approved VisuStella sources. A list of
 * official/approved sources can also be found on VisuStella.com.
 *
 * 5. VisuStella is not responsible for problems found in your game due to
 * unintended usage, incompatibility problems with plugins outside of the
 * VisuStella MZ library, plugin versions that aren't up to date, nor
 * responsible for the proper working of compatibility patches made by any
 * third parties. VisuStella is not responsible for errors caused by any
 * user-provided custom code used for custom control effects including advanced
 * JavaScript notetags and/or plugin parameters that allow JavaScript code.
 *
 * 6. If a compatibility patch needs to be made through a third party that is
 * unaffiliated with VisuStella that involves using code from the VisuStella MZ
 * library, contact must be made with a member from VisuStella and have it
 * approved. The patch would be placed on VisuStella.com as a free download
 * to the public. Such patches cannot be sold for monetary gain, including
 * commissions, crowdfunding, and/or donations.
 *
 * ============================================================================
 * Credits
 * ============================================================================
 * 
 * If you are using this plugin, credit the following people in your game:
 * 
 * Team VisuStella
 * * Yanfly
 * * Arisu
 * * Olivia
 * * Irina
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 * 
 * Version 1.26: February 19, 2021
 * * Bug Fixes!
 * ** Battles with branching event paths found within a conditional branch or
 *    choice tree will no longer be skipped over. Fix made by Arisu.
 * * Compatibility Update
 * ** Returning to the battle scene from the options scene in a Tpb-base battle
 *    system now links the current actor. Update by Irina.
 * 
 * Version 1.25: February 5, 2021
 * * Compatibility Update
 * ** Added compatibility update with VisuStella MZ Skills and States Core's
 *    Plugin Parameter > State Settings > Action End Update
 * * Feature Update!
 * ** <Common Event: name> notetag no longer requires <Custom Action Sequence>
 *    notetag if the Plugin Parameter: Auto Notetag is enabled.
 * 
 * Version 1.24: January 29, 2021
 * * Documentation Update!
 * ** Help file updated for new features.
 * * Feature Update!
 * ** MOVE: Move To Point and MOVE: Move To Target(s) Action Sequences'
 *    "Offset Adjustment" normal setting will now factor in Offset X and
 *    Offset Y positions unlike before where it cancels them. Update by Irina.
 * * New Features!
 * ** New notetag added by Arisu:
 * *** <Common Event: name>
 * **** Battle only: calls forth a Common Event of a matching name.
 * **** This is primarily used for users who are reorganizing around their
 *      Common Events and would still like to have their skills/items perform
 *      the correct Action Sequences in case the ID's are different.
 * 
 * Version 1.23: January 22, 2021
 * * Documentation Update!
 * ** Help file updated for new features.
 * * Feature Update!
 * ** ACSET: All Targets Action Set and ACSET: Each Target Action Set updated
 * *** New parameter added: Dual/Multi Wield?
 * **** Add times struck based on weapon quantity equipped?
 * * New Features!
 * ** Dual Wielding now functions differently. Made by Olivia.
 * *** Previously, RPG Maker MZ had "Dual Wielding" attack using both weapon
 *     animations at once, with the combined ATK of each weapon. It's confusing
 *     to look at and does not portray the nature of "Dual Wielding".
 * *** Dual Wielding, or in the case of users adding in third and fourth
 *     weapons, Multi Wielding is now changed. Each weapon is displayed
 *     individually, each producing its own attack animation, showing each
 *     weapon type, and applying only that weapon's ATK, Traits, and related
 *     effects. It is no longer a combined effect to display everything at once
 *     like RPG Maker MZ default.
 * *** If an actor has multiple weapon slots but some of them are unequipped,
 *     then the action will treat the attack as a single attack. There will be
 *     no barehanded attack to add on top of it. This is to match RPG Maker
 *     MZ's decision to omit a second animation if the same scenario is
 *     applied.
 * ** New Action Sequence Plugin Commands added by Yanfly
 * *** ANIM: Attack Animation 2+
 * **** Plays the animation associated with the user's 2nd weapon.
 *      Plays nothing if there is no 2nd weapon equipped.
 * ** New Action Sequence Plugin Commands added by Olivia
 * *** WEAPON: Clear Weapon Slot
 * *** WEAPON: Next Weapon Slot
 * *** WEAPON: Set Weapon Slot
 * **** These are Action Sequence Plugin Commands for devs who want finer
 *      control over Dual/Multi Wielding weapons.
 * 
 * Version 1.22: January 15, 2021
 * * Compatibility Update
 * ** Compatibility with "All Skills" Actor Command should now work with the
 *    Skills & States Core hide skill notetags.
 * 
 * Version 1.21: January 8, 2021
 * * Bug Fixes!
 * ** "MOVE: Home Reset" Plugin Command Action Sequence should work properly.
 *    Fix made by Yanfly.
 * * Documentation Update!
 * ** Added documentation for new feature(s)!
 * * New Features!
 * ** New Notetag snuck in by Arisu
 * *** <Auto Action Sequence>
 * **** Used for those who have the "Auto Notetag" Plugin Parameter enabled and
 *      just want to use an automatic Action Sequence instead.
 * ** New Plugin Parameter snuck in by Arisu!
 * *** Plugin Parameters > Action Sequences > Quality of Life > Auto Notetag
 * **** Automatically apply the <Custom Action Sequence> notetag effect to any
 *      item or skill that has a Common Event?
 * **** Any item or skill without a Common Event attached to it will use the
 *      Automatic Action Sequences instead.
 * **** The <Auto Action Sequence> notetag will disable this effect for that
 *      particular skill or item.
 * ** Arisu, you're going to be responsible for any bugs these may cause.
 * *** Bring it!!!!
 * **** And handling any bug report emails that are sent because this was
 *      turned on by accident.
 * ***** Please read the documentation, guys!
 * 
 * Version 1.20: January 1, 2021
 * * Bug Fixes!
 * ** For TPB Active or ATB Active, inputting actors that have received damage
 *    will return back to place after flinching. Fix made by Yanfly.
 * * Documentation Update!
 * ** Added documentation for new feature(s)!
 * * New Features!
 * ** New notetags added by Yanfly:
 * *** <Battle Portrait Offset: +x, +y>
 * *** <Battle Portrait Offset X: +x>
 * *** <Battle Portrait Offset Y: +y>
 * **** This is used with the "Portrait" and "Border" Battle Layouts.
 * **** Offsets the X and Y coordinates for the battle portrait.
 * 
 * Version 1.19: December 25, 2020
 * * Bug Fixes!
 * ** Removing a state from a Sideview Enemy during the middle of their a non-
 *    looping motion will no longer reset their motion to neutral.
 *    Fix made by Yanfly.
 * * Compatibility Update!
 * ** Plugins should be more compatible with one another.
 * * Documentation Update!
 * ** Added documentation for updated feature(s)!
 * * Feature Update!
 * ** Action Sequence "PROJECTILE: Icon" now supports code for the "Icon"
 *    parameter. Update made by Yanfly.
 * 
 * Version 1.18: December 18, 2020
 * * Bug Fixes!
 * ** For TPB Active or ATB Active, inputting actors will no longer step back
 *    after an enemy's action is finished. Fix made by Yanfly and Shiro.
 * * Documentation Update!
 * ** Added documentation for new feature(s)!
 * * New Features!
 * ** Action Sequence "BTLOG: Add Text" is updated for the convenience of a new
 *    option to quickly copy the displayed text to the VisuStella MZ Combat Log
 *    if that plugin is installed. Added by Yanfly.
 * 
 * Version 1.17: December 11, 2020
 * * Bug Fixes!
 * ** Common Events in TPB Active that cause forced actions will no longer
 *    cause currently inputting actors that match the forced action battler to
 *    crash the game. Fix made by Yanfly and Shiro.
 * * Compatibility Update!
 * ** Added compatibility functionality for future plugins.
 * ** Plugins should be more compatible with one another.
 * * Documentation Update!
 * ** Added documentation for new feature(s)!
 * * Feature Update!
 * ** Action Sequence Impact Action Sequences "Shockwave from Each Target(s)",
 *    "Shockwave from Target(s) Center", and "Zoom Blur at Target(s) Center"
 *    now have "Offset X" and "Offset Y" plugin parameters. Added by Yanfly.
 * ** Action Sequence "MOVE: Move To Target(s)" is now changed so that if the
 *    "Melee Distance" value is set to 0, battlers will no longer stand a half
 *    body distance away. Added by Yanfly.
 * 
 * Version 1.16: December 4, 2020
 * * Bug Fixes!
 * ** Bug fixes made for the RPG Maker MZ base code. If a battler has no
 *    actions, then their action speed will not be Infinity. Fix by Olivia.
 * * Compatibility Update!
 * ** Plugins should be more compatible with one another.
 * * Optimization Update!
 * ** Plugin should run more optimized.
 * 
 * Version 1.15: November 29, 2020
 * * Bug Fixes!
 * ** Completely replacing the whole party at once will no longer cause the
 *    battle system to crash. Fix made by Olivia.
 * ** Pre-Battle Common Events will no longer cancel out any win/lose branches.
 *    Fix made by Arisu.
 * * Feature Update!
 * ** Custom Action Sequences will no longer close the Actor Command Input
 *    window unless absolutely necessary (like for Show Message events) during
 *    Active TPB/ATB. Change made by Arisu.
 * 
 * Version 1.14: November 22, 2020
 * * Feature Update!
 * ** Natural Miss and Evasion motions now have flinch distance.
 *    Added by Yanfly.
 * 
 * Version 1.13: November 15, 2020
 * * Optimization Update!
 * ** Plugin should run more optimized.
 * 
 * Version 1.12: November 8, 2020
 * * Bug Fixes!
 * ** Failsafes added to prevent common events from running if they're empty.
 *    Fix made by Irina.
 * ** Skip Party Command will now work properly with TPB-based battle systems.
 *    Fix made by Yanfly.
 * * Compatibility Update!
 * ** Plugins should be more compatible with one another.
 * * Documentation Update!
 * ** In preparation for upcoming VisuStella MZ plugins.
 * 
 * Version 1.11: November 1, 2020
 * * Compatibility Update!
 * ** Plugins should be more compatible with one another.
 * * Documentation Update!
 * ** Added clarity for the Plugin Parameters for the Common Events settings
 *    found in the mechanics section. The common events are only meant to run
 *    in the map scene and not for the battle scene. Update made by Irina.
 * * Feature Update!
 * ** The Plugin Parameter for Mechanics, Common Events (on Map), Defeat Event
 *    now has updated functionality. If this has a common event attached to it,
 *    then losing to random encounters will no longer send the player to the
 *    Game Over scene, but instead, send the player back to the map scene,
 *    where the Defeat Common Event will run. Update made by Irina.
 * 
 * Version 1.10: October 25, 2020
 * * Documentation Update!
 * ** Added documentation for new feature(s)!
 * * New Features!
 * ** New Action Sequence Plugin Command added by Olivia:
 * *** MECH: Custom Damage Formula
 * **** Changes the current action's damage formula to custom.
 *      This will assume the MANUAL damage style.
 * ** New Notetag added by Irina:
 * ** New Plugin Parameters added by Irina:
 * *** Plugin Parameters > Battleback Scaling Settings
 * **** These settings allow you to adjust how battlebacks scale to the screen
 *      in the game.
 * *** <Battler Sprite Grounded>
 * **** Prevents the enemy from being able to jumping and/or floating due to
 *      Action Sequences but still able to move. Useful for rooted enemies.
 * 
 * Version 1.09: October 18, 2020
 * * Bug Fixes!
 * ** Exiting out of the Options menu scene or Party menu scene will no longer
 *    cause party members to reset their starting position. Fix made by Arisu
 * * Documentation Update!
 * ** Added documentation for new feature(s)!
 * ** There was a documentation error with <JS Pre-Regenerate> and
 *    <JS Post-Regenerate>. Fix made by Yanfly.
 * *** Before, these were written as <JS Pre-Regenerate Turn> and
 *     <JS Post-Regenerate Turn>. The "Turn" part of the notetag has been
 *     removed in the documentation.
 * * Feature Update!
 * ** Damage sprites on actors are now centered relative to the actor's anchor.
 *    Change made by Yanfly.
 * * New Features!
 * ** New Action Sequence Plugin Command added by Yanfly:
 * *** MECH: Variable Popup
 * **** Causes the unit(s) to display a popup using the data stored inside
 *      a variable.
 * 
 * Version 1.08: October 11, 2020
 * * Bug Fixes!
 * ** Dead party members at the start of battle no longer start offscreen.
 *    Fix made by Arisu.
 * ** Removed party members from battle no longer count as moving battlers.
 *    Fix made by Yanfly.
 * ** Using specific motions should now have the weapons showing and not
 *    showing properly. Fix made by Yanfly.
 * 
 * Version 1.07: October 4, 2020
 * * Bug Fixes!
 * ** Adding and removing actors will now refresh the battle status display.
 *    Fix made by Irina.
 * ** Adding new states that would change the affected battler's state motion
 *    will automatically refresh the battler's motion. Fix made by Irina.
 * ** Boss Collapse animation fixed and will sink into the ground.
 *    Fix made by Irina.
 * ** Failsafes added for certain animation types. Fix made by Yanfly.
 * ** Freeze Motion for thrust, swing, and missile animations will now show the
 *    weapons properly. Fix made by Yanfly.
 * ** The Guard command will no longer display the costs of the Attack command.
 *    Fix made by Irina.
 * * Documentation Update!
 * ** Updated help file for newly added plugin parameters.
 * * Feature Updates!
 * ** When using the Change Battleback event command in battle, the game client
 *    will wait until both battlebacks are loaded before changing the both of
 *    them so that the appearance is synched together. Change made by Yanfly.
 * * New Features!
 * ** New plugin parameters added by Irina!
 * *** Plugin Parameters > Actor Battler Settings > Chant Style
 * **** What determines the chant motion? Hit type or skill type?
 * 
 * Version 1.06: September 27, 2020
 * * Bug Fixes!
 * ** Enemy Battler Plugin Parameter "Shadow Visible" should now work again.
 *    Fix made by Irina.
 * * Compatibility Update!
 * ** Added compatibility functionality for future plugins. Added by Yanfly.
 * * Documentation Update!
 * ** Updated the help file for all the new plugin parameters.
 * * Feature Update!
 * ** Action Sequence "MECH: HP, MP, TP" will now automatically collapse an
 *    enemy if it has been killed by the effect.
 * ** All battle systems for front view will now have damage popups appear
 *    in front of the status window instead of just the Portrait battle layout.
 *    Update made by Yanfly.
 * * New Features!
 * ** New Action Sequence Plugin Commands from Irina!
 * *** MOTION: Clear Freeze Frame
 * *** MOTION: Freeze Motion Frame
 * **** You can freeze a battler's sprite's motion with a specific frame.
 * ** New notetags for Maps and name tags for Troops added by Yanfly!
 * *** <Battle Layout: type> to change the battle layout style used for
 *     specific maps and/or troops.
 * ** New plugin parameters added by Yanfly!
 * *** Plugin Parameters > Battle Layout Settings > Command Window Width
 * **** This plugin parameter lets you adjust the window width for Party and
 *      Actor Command windows in the Default and List Battle Layout styles.
 * *** Plugin Parameters > Enemy Battler Settings > Name: Offset X
 * *** Plugin Parameters > Enemy Battler Settings > Name: Offset Y
 * **** These plugin parameters allow you to offset the position of the enemy
 *      name positions on the screen by a specific amount.
 * 
 * Version 1.05: September 20, 2020
 * * Bug Fixes!
 * ** Actors now use their casting or charging animations again during TPB/ATB.
 *    Fix made by Yanfly.
 * ** Defeat requirement for enemies will no longer crash the game if turned on
 *    after creating
 * ** Escaping animation no longer has actors stay in place. Fixed by Yanfly.
 * ** Failsafes added for newly added weapon types that have not been adjusted
 *    in the Database > System 2 tab. Fixed by Irina.
 * ** Shadows now appear under the actor sprites. Fix made by Yanfly.
 * ** Victory during TPB will no longer cancel the victory animations of
 *    actors that will have their turn after. Fixed by Yanfly.
 * * Documentation Update!
 * ** All Anchor Plugin Parameter descriptions now state to use values between
 *    0 and 1 to be safe. Update made by Yanfly.
 * * Feature Update!
 * ** During Active TPB / ATB, canceling out of the actor command window will
 *    go directly into the party window without having to sort through all of
 *    the available active actors.
 * ** Going from the Party Command Window's Fight command will immediately
 *    return back to the actor command window that was canceled from.
 * * New Features!
 * ** Action Sequence Plugin Command "MOVE: Spin/Rotate" has been updated.
 * *** A new parameter has been added: "Revert Angle on Finish"
 * *** Added by Yanfly.
 * ** New plugin parameters have been added to Damage Settings.
 * *** Appear Position: Selects where you want popups to appear relative to the
 *     battler. Head, Center, Base. Added by Yanfly.
 * *** Offset X: Sets how much to offset the sprites by vertically.
 *     Added by Yanfly.
 * *** Offset Y: Sets how much to offset the sprites by horizontally.
 *     Added by Yanfly.
 * ** New plugin parameters have been added to Actor Battler Settings.
 * *** Priority: Active - Place the active actor on top of actor and
 *     enemy sprites. Added by Yanfly.
 * *** Priority: Actors - Prioritize actors over enemies when placing 
 *     sprites on top of each other. Added by Yanfly.
 * 
 * Version 1.04: September 13, 2020
 * * Bug Fixes!
 * ** Active Battler Sprites now remain on top and won't be hidden behind
 *    other sprites for better visual clarity. Fix made by Arisu.
 * ** Collapsing battlers will now show the dead motion properly. Fix made by
 *    Olivia.
 * ** Dead battlers can no longer be given immortality. Fix made by Olivia.
 * ** Going into the Options menu with no battleback set will no longer set a
 *    battle snapshot.
 * ** HP Gauges for Sideview Enemies are no longer flipped! Fix made by Yanfly.
 * ** Moving a dead battler would no longer reset their animation. Fix made by
 *    Olivia.
 * ** Pre-Battle Common Events now work with events instead of just random
 *    encounters. Fix made by Yanfly.
 * ** Sideview Enemy shadows no longer twitch. Fix made by Irina.
 * * Documentation Updates!
 * ** Added further explanations for Anchor X and Anchor Y plugin parameters.
 *    This is because there's a lot of confusion for users who aren't familiar
 *    with how sprites work. Added by Irina.
 * ** <Magic Reduction: x> notetag updated to say magical damage instead of
 *    physical damage. Fix made by Yanfly.
 * * New Features!
 * ** Additional Action Sequence Plugin Commands have been added in preparation
 *    of upcoming plugins! Additions made by Irina.
 * *** Action Sequences - Angle (for VisuMZ_3_ActSeqCamera)
 * *** Action Sequences - Camera (for VisuMZ_3_ActSeqCamera)
 * *** Action Sequences - Skew (for VisuMZ_3_ActSeqCamera)
 * *** Action Sequences - Zoom (for VisuMZ_3_ActSeqCamera)
 * ** Additional Action Sequence Plugin Commands have been made available now
 *    and added to Battle Core! Additions made by Irina.
 * *** MOVE: Scale/Grow/Shrink
 * *** MOVE: Skew/Distort
 * *** MOVE: Spin/Rotate
 * *** MOVE: Wait For Scale
 * *** MOVE: Wait For Skew
 * *** MOVE: Wait For Spin
 * ** Plugin Parameters Additions. Additions made by Irina.
 * *** Plugin Params > Actor Battler Settings > Offset X
 * *** Plugin Params > Actor Battler Settings > Offset Y
 * *** Plugin Params > Actor Battler Settings > Smooth Image
 * *** Plugin Params > Enemy Battler Settings > Offset X
 * *** Plugin Params > Enemy Battler Settings > Offset Y
 * *** Plugin Params > Enemy Battler Settings > Smooth Image
 * 
 * Version 1.03: September 6, 2020
 * * Bug Fixes!
 * ** Animated Battlers will refresh their motions from the death motion once
 *    they're revived instead of waiting for their next input phase. Fix made
 *    by Yanfly.
 * ** Battle Log speed sometimes went by too fast for certain enabled messages.
 *    Wait timers are now added to them, like state results, buff results, and
 *    debuff results. Fix made by Yanfly.
 * ** Boss Collapse animation now works properly. Fix made by Yanfly.
 * ** Freeze fix for TPB (Wait) if multiple actors get a turn at the same time.
 *    Fix made by Olivia.
 * ** Pressing cancel on a target window after selecting a single skill no
 *    longer causes the status window to twitch.
 * ** Sideview Enemies had a split frame of being visible if they were to start
 *    off hidden in battle. Fix made by Shaz.
 * * Compatibility Update:
 * ** Battle Core's Sprite_Damage.setup() function is now separated fro the
 *    default to allow for better compatibility. Made by Yanfly.
 * * Documentation Update:
 * ** Inserted more information for "Damage Popups" under "Major Changes"
 * * New Features!
 * ** <Magic Penetration: x>, <Magic Penetration: x%> notetags added.
 * ** <Magic Reduction: x>, <Magic Reduction: x%> notetags added.
 * ** <Battle UI Offset: +x, +y>, <Battle UI Offset X: +x>, and
 *    <Battle UI Offset Y: +y> notetags added for adjusting the positions of
 *    HP Gauges and State Icons.
 * *** Notetags added by Yanfly.
 * 
 * Version 1.02: August 30, 2020
 * * Bug Fixes!
 * ** Failsafes added for parsing battle targets. Fix made by Yanfly.
 * ** Immortality is no longer ignored by skills/items with the Normal Attack
 *    state effect. Fix made by Yanfly.
 * ** Miss and Evasion sound effects work again! Fix made by Yanfly.
 * ** Selecting "Escape" from the Actor Command Window will now have the
 *    Inputting Battler show its escape motion. Fix made by Yanfly.
 * ** Wait for Movement now applies to SV Enemies. Fix made by Yanfly.
 * * New Features!
 * ** Plugin Command "ACSET: Finish Action" now has an option to turn off the
 *    Immortality of targets. Feature added by Yanfly.
 * * Optimization Update
 * ** Uses less resources when making checks for Pre-Battle Battle Start events
 * 
 * Version 1.01: August 23, 2020
 * * Bug Fixes!
 * ** Plugin Parameters > Damage Settings > Damage Formats are now fixed.
 *    Fix made by Olivia.
 * ** TPB Battle System with Disable Party Command fixed. Fix made by Olivia.
 * ** States now show in list format if faces are disabled. Fix made by Yanfly.
 * ** The default damage styles were missing the 'v' variable to allow for
 *    variable data input. These are back now. Fix made by Yanfly.
 * *** Users updating from version 1.00 will need to fix this problem by either
 *     removing the plugin from the Plugin Manager list and reinstalling it, or
 *     going to Plugin Parameters > Damage Settings > Style List > the style
 *     you want, and adding "const v = $gameVariables._data;" to JS: Formula
 * * New Notetags Added:
 * ** <Command Show Switch: x> added by Olivia
 * ** <Command Show All Switches: x,x,x> added by Olivia
 * ** <Command Show Any Switches: x,x,x> added by Olivia
 * ** <Command Hide Switch: x> added by Olivia
 * ** <Command Hide All Switches: x,x,x> added by Olivia
 * ** <Command Hide Any Switches: x,x,x> added by Olivia
 * ** <JS Command Visible> added by Olivia
 *
 * Version 1.00: August 20, 2020
 * * Finished Plugin!
 *
 * ============================================================================
 * End of Helpfile
 * ============================================================================
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceSpaceStart
 * @text -
 * @desc The following are Action Sequences commands/sets.
 * These Plugin Commands only work in battle.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceBreakSet
 * @text Action Sequence - Action Sets
 * @desc Action Sequence Action Sets are groups of commonly used
 * Action Sequence Commands put together for more efficient usage.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Set_SetupAction
 * @text ACSET: Setup Action Set
 * @desc The generic start to most actions.
 * 
 * @arg DisplayAction:eval
 * @text Display Action
 * @type boolean
 * @on Use
 * @off Don't Use
 * @desc Use this part of the action sequence?
 * @default true
 * 
 * @arg ApplyImmortal:eval
 * @text Immortal: On
 * @type boolean
 * @on Use
 * @off Don't Use
 * @desc Use this part of the action sequence?
 * @default true
 * 
 * @arg ActionStart:eval
 * @text Battle Step
 * @type boolean
 * @on Use
 * @off Don't Use
 * @desc Use this part of the action sequence?
 * @default true
 * 
 * @arg WaitForMovement:eval
 * @text Wait For Movement
 * @type boolean
 * @on Use
 * @off Don't Use
 * @desc Use this part of the action sequence?
 * @default true
 * 
 * @arg CastAnimation:eval
 * @text Cast Animation
 * @type boolean
 * @on Use
 * @off Don't Use
 * @desc Use this part of the action sequence?
 * @default true
 * 
 * @arg WaitForAnimation:eval
 * @text Wait For Animation
 * @type boolean
 * @on Use
 * @off Don't Use
 * @desc Use this part of the action sequence?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Set_WholeActionSet
 * @text ACSET: All Targets Action Set
 * @desc Affects all targets simultaneously performing the following.
 * 
 * @arg DualWield:eval
 * @text Dual/Multi Wield?
 * @type boolean
 * @on Apply
 * @off Don't
 * @desc Add times struck based on weapon quantity equipped?
 * @default false
 * 
 * @arg PerformAction:eval
 * @text Perform Action
 * @type boolean
 * @on Use
 * @off Don't Use
 * @desc Use this part of the action sequence?
 * @default true
 * 
 * @arg WaitCount:eval
 * @text Wait Count
 * @desc How many frames should the action sequence wait?
 * You may use JavaScript code.
 * @default Sprite_Battler._motionSpeed
 * 
 * @arg ActionAnimation:eval
 * @text Action Animation
 * @type boolean
 * @on Use
 * @off Don't Use
 * @desc Use this part of the action sequence?
 * @default true
 * 
 * @arg WaitForAnimation:eval
 * @text Wait For Animation
 * @type boolean
 * @on Use
 * @off Don't Use
 * @desc Use this part of the action sequence?
 * @default true
 * 
 * @arg ActionEffect:eval
 * @text Action Effect
 * @type boolean
 * @on Use
 * @off Don't Use
 * @desc Use this part of the action sequence?
 * @default true
 * 
 * @arg ApplyImmortal:eval
 * @text Immortal: Off
 * @type boolean
 * @on Use
 * @off Don't Use
 * @desc Use this part of the action sequence?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Set_TargetActionSet
 * @text ACSET: Each Target Action Set
 * @desc Goes through each target one by one to perform the following.
 * 
 * @arg DualWield:eval
 * @text Dual/Multi Wield?
 * @type boolean
 * @on Apply
 * @off Don't
 * @desc Add times struck based on weapon quantity equipped?
 * @default false
 * 
 * @arg PerformAction:eval
 * @text Perform Action
 * @type boolean
 * @on Use
 * @off Don't Use
 * @desc Use this part of the action sequence?
 * @default true
 * 
 * @arg WaitCount1:eval
 * @text Wait Count
 * @desc How many frames should the action sequence wait?
 * You may use JavaScript code.
 * @default Sprite_Battler._motionSpeed
 * 
 * @arg ActionAnimation:eval
 * @text Action Animation
 * @type boolean
 * @on Use
 * @off Don't Use
 * @desc Use this part of the action sequence?
 * @default true
 * 
 * @arg WaitCount2:eval
 * @text Wait Count
 * @desc How many frames should the action sequence wait?
 * You may use JavaScript code.
 * @default Sprite_Battler._motionSpeed * 2
 * 
 * @arg ActionEffect:eval
 * @text Action Effect
 * @type boolean
 * @on Use
 * @off Don't Use
 * @desc Use this part of the action sequence?
 * @default true
 * 
 * @arg ApplyImmortal:eval
 * @text Immortal: Off
 * @type boolean
 * @on Use
 * @off Don't Use
 * @desc Use this part of the action sequence?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Set_FinishAction
 * @text ACSET: Finish Action
 * @desc The generic ending to most actions.
 * 
 * @arg ApplyImmortal:eval
 * @text Immortal: Off
 * @type boolean
 * @on Use
 * @off Don't Use
 * @desc Use this part of the action sequence?
 * @default true
 * 
 * @arg WaitForNewLine:eval
 * @text Wait For New Line
 * @type boolean
 * @on Use
 * @off Don't Use
 * @desc Use this part of the action sequence?
 * @default true
 * 
 * @arg WaitForEffect:eval
 * @text Wait For Effects
 * @type boolean
 * @on Use
 * @off Don't Use
 * @desc Use this part of the action sequence?
 * @default true
 * 
 * @arg ClearBattleLog:eval
 * @text Clear Battle Log
 * @type boolean
 * @on Use
 * @off Don't Use
 * @desc Use this part of the action sequence?
 * @default true
 * 
 * @arg ActionEnd:eval
 * @text Home Reset
 * @type boolean
 * @on Use
 * @off Don't Use
 * @desc Use this part of the action sequence?
 * @default true
 * 
 * @arg WaitForMovement:eval
 * @text Wait For Movement
 * @type boolean
 * @on Use
 * @off Don't Use
 * @desc Use this part of the action sequence?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceSpaceAngle
 * @text -
 * @desc -
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceBreakAngle
 * @text Action Sequences - Angle
 * @desc Allows you to have control over the camera angle.
 * Requires VisuMZ_3_ActSeqCamera!
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_ChangeAngle
 * @text ANGLE: Change Angle
 * @desc Changes the camera angle.
 * Requires VisuMZ_3_ActSeqCamera!
 * 
 * @arg Angle:eval
 * @text Angle
 * @desc Change the camera angle to this many degrees.
 * @default 0
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc Duration in frames to change camera angle.
 * @default 60
 *
 * @arg EasingType:str
 * @text Angle Easing
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * Requires VisuMZ_0_CoreEngine.
 * @default InOutSine
 * 
 * @arg WaitForAngle:eval
 * @text Wait For Angle?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for angle changes to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Angle_Reset
 * @text ANGLE: Reset Angle
 * @desc Reset any angle settings.
 * Requires VisuMZ_3_ActSeqCamera!
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc Duration in frames to reset camera angle.
 * @default 60
 *
 * @arg EasingType:str
 * @text Angle Easing
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * Requires VisuMZ_0_CoreEngine.
 * @default InOutSine
 * 
 * @arg WaitForAngle:eval
 * @text Wait For Angle?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for angle changes to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Angle_WaitForAngle
 * @text ANGLE: Wait For Angle
 * @desc Waits for angle changes to complete before performing next command.
 * Requires VisuMZ_3_ActSeqCamera!
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceSpaceAnimation
 * @text -
 * @desc -
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceBreakAnimation
 * @text Action Sequences - Animations
 * @desc These Action Sequences are related to the 'Animations' that
 * can be found in the Animations tab of the Database.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Animation_ActionAnimation
 * @text ANIM: Action Animation
 * @desc Plays the animation associated with the action.
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to play the animation on.
 * @default ["all targets"]
 * 
 * @arg Mirror:eval
 * @text Mirror Animation
 * @type boolean
 * @on Mirror
 * @off Normal
 * @desc Mirror the animation?
 * @default false
 * 
 * @arg WaitForAnimation:eval
 * @text Wait For Animation?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for animation to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Animation_AttackAnimation
 * @text ANIM: Attack Animation
 * @desc Plays the animation associated with the user's 1st weapon.
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to play the animation on.
 * @default ["all targets"]
 * 
 * @arg Mirror:eval
 * @text Mirror Animation
 * @type boolean
 * @on Mirror
 * @off Normal
 * @desc Mirror the animation?
 * @default false
 * 
 * @arg WaitForAnimation:eval
 * @text Wait For Animation?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for animation to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Animation_AttackAnimation2
 * @text ANIM: Attack Animation 2+
 * @desc Plays the animation associated with the user's other weapons.
 * Plays nothing if there is no other weapon equipped.
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to play the animation on.
 * @default ["all targets"]
 * 
 * @arg Slot:eval
 * @text Slot
 * @desc Which weapon slot to get this data from?
 * Main-hand weapon is weapon slot 1.
 * @default 2
 * 
 * @arg Mirror:eval
 * @text Mirror Animation
 * @type boolean
 * @on Mirror
 * @off Normal
 * @desc Mirror the animation?
 * @default true
 * 
 * @arg WaitForAnimation:eval
 * @text Wait For Animation?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for animation to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Animation_CastAnimation
 * @text ANIM: Cast Animation
 * @desc Plays the cast animation associated with the action.
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to play the animation on.
 * @default ["user"]
 * 
 * @arg Mirror:eval
 * @text Mirror Animation
 * @type boolean
 * @on Mirror
 * @off Normal
 * @desc Mirror the animation?
 * @default false
 * 
 * @arg WaitForAnimation:eval
 * @text Wait For Animation?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for animation to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Animation_ChangeBattlePortrait
 * @text ANIM: Change Battle Portrait
 * @desc Changes the battle portrait of the actor (if it's an actor).
 * Can be used outside of battle/action sequences.
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to change the portraits for.
 * Valid units can only be actors.
 * @default ["user"]
 * 
 * @arg Filename:str
 * @text Filename
 * @type file
 * @dir img/pictures/
 * @desc Select the file to change the actor's portrait to.
 * @default Untitled
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Animation_ShowAnimation
 * @text ANIM: Show Animation
 * @desc Plays the a specific animation on unit(s).
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to play the animation on.
 * @default ["all targets"]
 * 
 * @arg AnimationID:num
 * @text Animation ID
 * @type animation
 * @desc Select which animation to play on unit(s).
 * @default 1
 * 
 * @arg Mirror:eval
 * @text Mirror Animation
 * @type boolean
 * @on Mirror
 * @off Normal
 * @desc Mirror the animation?
 * @default false
 * 
 * @arg WaitForAnimation:eval
 * @text Wait For Animation?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for animation to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Animation_WaitForAnimation
 * @text ANIM: Wait For Animation
 * @desc Causes the interpreter to wait for any animation(s) to finish.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceSpaceBattleLog
 * @text -
 * @desc -
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceBreakBattleLog
 * @text Action Sequences - Battle Log
 * @desc These Action Sequences are related to the Battle Log Window,
 * the window found at the top of the battle screen.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_BattleLog_AddText
 * @text BTLOG: Add Text
 * @desc Adds a new line of text into the Battle Log.
 * 
 * @arg Text:str
 * @text Text
 * @desc Add this text into the Battle Log.
 * Text codes allowed.
 * @default Insert text here.
 * 
 * @arg CopyCombatLog:eval
 * @text Copy to Combat Log?
 * @type boolean
 * @on Copy Text
 * @off Don't Copy
 * @desc Copies text to the Combat Log.
 * Requires VisuMZ_4_CombatLog
 * @default true
 *
 * @arg CombatLogIcon:num
 * @text Combat Log Icon
 * @parent CopyCombatLog:eval
 * @desc What icon would you like to bind to this entry?
 * Requires VisuMZ_4_CombatLog
 * @default 87
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_BattleLog_Clear
 * @text BTLOG: Clear Battle Log
 * @desc Clears all the text in the Battle Log.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_BattleLog_DisplayAction
 * @text BTLOG: Display Action
 * @desc Displays the current action in the Battle Log.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_BattleLog_PopBaseLine
 * @text BTLOG: Pop Base Line
 * @desc Removes the Battle Log's last added base line and 
 * all text up to its former location.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_BattleLog_PushBaseLine
 * @text BTLOG: Push Base Line
 * @desc Adds a new base line to where the Battle Log currently is at.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_BattleLog_Refresh
 * @text BTLOG: Refresh Battle Log
 * @desc Refreshes the Battle Log.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_BattleLog_UI
 * @text BTLOG: UI Show/Hide
 * @desc Shows or hides the Battle UI (including the Battle Log).
 * 
 * @arg ShowHide:eval
 * @text Show/Hide?
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Shows/hides the Battle UI.
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_BattleLog_WaitForBattleLog
 * @text BTLOG: Wait For Battle Log
 * @desc Causes the interpreter to wait for the Battle Log to finish.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_BattleLog_WaitForNewLine
 * @text BTLOG: Wait For New Line
 * @desc Causes the interpreter to wait for a new line in the Battle Log.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceSpaceCamera
 * @text -
 * @desc -
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceBreakCamera
 * @text Action Sequences - Camera
 * @desc Allows you to have control over the camera.
 * Requires VisuMZ_3_ActSeqCamera!
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Camera_Clamp
 * @text CAMERA: Clamp ON/OFF
 * @desc Turns battle camera clamping on/off.
 * Requires VisuMZ_3_ActSeqCamera!
 * 
 * @arg Setting:eval
 * @text ON/OFF
 * @type boolean
 * @on ON
 * @off OFF
 * @desc Turns camera clamping on/off.
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Camera_FocusPoint
 * @text CAMERA: Focus Point
 * @desc Focus the battle camera on a certain point in the screen.
 * Requires VisuMZ_3_ActSeqCamera!
 * 
 * @arg FocusX:eval
 * @text X Coordinate
 * @desc Insert the point to focus the camera on.
 * You may use JavaScript code.
 * @default Graphics.width / 2
 * 
 * @arg FocusY:eval
 * @text Y Coordinate
 * @desc Insert the point to focus the camera on.
 * You may use JavaScript code.
 * @default Graphics.height / 2
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc Duration in frames for camera focus change.
 * @default 60
 *
 * @arg EasingType:str
 * @text Camera Easing
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * Requires VisuMZ_0_CoreEngine.
 * @default InOutSine
 * 
 * @arg WaitForCamera:eval
 * @text Wait For Camera?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for camera changes to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Camera_FocusTarget
 * @text CAMERA: Focus Target(s)
 * @desc Focus the battle camera on certain battler target(s).
 * Requires VisuMZ_3_ActSeqCamera!
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to focus the battle camera on.
 * @default ["user"]
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc Duration in frames for camera focus change.
 * @default 60
 *
 * @arg EasingType:str
 * @text Camera Easing
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * Requires VisuMZ_0_CoreEngine.
 * @default InOutSine
 * 
 * @arg WaitForCamera:eval
 * @text Wait For Camera?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for camera changes to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Camera_Offset
 * @text CAMERA: Offset
 * @desc Offset the battle camera from the focus target.
 * Requires VisuMZ_3_ActSeqCamera!
 * 
 * @arg OffsetX:eval
 * @text Offset X
 * @desc How much to offset the camera X by.
 * Negative: left. Positive: right.
 * @default +0
 * 
 * @arg OffsetY:eval
 * @text Offset Y
 * @desc How much to offset the camera Y by.
 * Negative: up. Positive: down.
 * @default +0
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc Duration in frames for offset change.
 * @default 60
 *
 * @arg EasingType:str
 * @text Camera Easing
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * Requires VisuMZ_0_CoreEngine.
 * @default InOutSine
 * 
 * @arg WaitForCamera:eval
 * @text Wait For Camera?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for camera changes to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Camera_Reset
 * @text CAMERA: Reset
 * @desc Reset the battle camera settings.
 * Requires VisuMZ_3_ActSeqCamera!
 * 
 * @arg ResetFocus:eval
 * @text Reset Focus?
 * @type boolean
 * @on On
 * @off Off
 * @desc Reset the focus point?
 * @default true
 * 
 * @arg ResetOffset:eval
 * @text Reset Offset?
 * @type boolean
 * @on On
 * @off Off
 * @desc Reset the camera offset?
 * @default true
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc Duration in frames for reset change.
 * @default 60
 *
 * @arg EasingType:str
 * @text Camera Easing
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * Requires VisuMZ_0_CoreEngine.
 * @default InOutSine
 * 
 * @arg WaitForCamera:eval
 * @text Wait For Camera?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for camera changes to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Camera_WaitForCamera
 * @text CAMERA: Wait For Camera
 * @desc Waits for camera to complete before performing next command.
 * Requires VisuMZ_3_ActSeqCamera!
 *
 * @ --------------------------------------------------------------------------
 *
 *
 * @command ActionSequenceSpaceDragonbones
 * @text -
 * @desc -
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceBreaDragonbones
 * @text Action Sequences - Dragonbones
 * @desc These Action Sequences are Dragonbones-related.
 * Requires VisuMZ_2_DragonbonesUnion!
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_DB_DragonbonesMotionAni
 * @text DB: Dragonbones Animation
 * @desc Causes the unit(s) to play a Dragonbones motion animation.
 * Requires VisuMZ_2_DragonbonesUnion!
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) to perform a motion animation.
 * @default ["user"]
 *
 * @arg MotionAni:str
 * @text Motion Animation
 * @desc What is the name of the Dragonbones motion animation you wish to play?
 * @default attack
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_DB_DragonbonesTimeScale
 * @text DB: Dragonbones Time Scale
 * @desc Causes the unit(s) to change their Dragonbones time scale.
 * Requires VisuMZ_2_DragonbonesUnion!
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) to perform a motion animation.
 * @default ["user"]
 *
 * @arg TimeScale:num
 * @text Time Scale
 * @desc Change the value of the Dragonbones time scale to this.
 * @default 1.0
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceSpaceElements
 * @text -
 * @desc -
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceBreakElements
 * @text Action Sequences - Elements
 * @desc These Action Sequences are related to elements.
 * Requires VisuMZ_1_ElementStatusCore!
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Element_AddElements
 * @text ELE: Add Elements
 * @desc Adds element(s) to be used when calculating damage.
 * Requires VisuMZ_1_ElementStatusCore!
 *
 * @arg Elements:arraynum
 * @text Elements
 * @type number[]
 * @min 1
 * @max 99
 * @desc Select which element ID to add onto the action.
 * Insert multiple element ID's to add multiple at once.
 * @default ["1"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Element_Clear
 * @text ELE: Clear Element Changes
 * @desc Clears all element changes made through Action Sequences.
 * Requires VisuMZ_1_ElementStatusCore!
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Element_ForceElements
 * @text ELE: Force Elements
 * @desc Forces only specific element(s) when calculating damage.
 * Requires VisuMZ_1_ElementStatusCore!
 *
 * @arg Elements:arraynum
 * @text Elements
 * @type number[]
 * @min 1
 * @max 99
 * @desc Select which element ID to force in the action.
 * Insert multiple element ID's to force multiple at once.
 * @default ["1"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Element_NullElements
 * @text ELE: Null Element
 * @desc Forces no element to be used when calculating damage.
 * Requires VisuMZ_1_ElementStatusCore!
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceSpaceHorror
 * @text -
 * @desc -
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceBreakHorror
 * @text Action Sequences - Horror Effects
 * @desc These Action Sequences are Horror Effects-related.
 * Requires VisuMZ_2_HorrorEffects!
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Horror_Clear
 * @text HORROR: Clear All Filters
 * @desc Clear all Horror Effects filters on the target battler(s).
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to remove Horror Effects for.
 * @default ["user"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Horror_GlitchCreate
 * @text HORROR: Glitch Create
 * @desc Creates the glitch effect on the target battler(s).
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to create the Horror Effect for.
 * @default ["user"]
 *
 * @arg slices:num
 * @text Glitch Slices
 * @parent FilterGlitch
 * @type number
 * @min 1
 * @desc Glitch slices to be used with the target.
 * @default 10
 *
 * @arg offset:num
 * @text Glitch Offset
 * @parent FilterGlitch
 * @type number
 * @min 1
 * @desc Default offset value.
 * @default 100
 *
 * @arg animated:eval
 * @text Glitch Animated?
 * @parent FilterGlitch
 * @type boolean
 * @on Animate
 * @off Static
 * @desc Animate the glitch effect?
 * @default true
 *
 * @arg aniFrequency:num
 * @text Glitch Frequency
 * @parent FilterGlitch
 * @type number
 * @min 1
 * @desc If animated, how frequent to make the glitch effect?
 * Lower = often     Higher = rarer
 * @default 300
 *
 * @arg aniStrength:num
 * @text Glitch Strength
 * @parent FilterGlitch
 * @type number
 * @min 1
 * @desc If animated, how strong is the glitch effect?
 * Lower = weaker     Higher = stronger
 * @default 30
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Horror_GlitchRemove
 * @text HORROR: Glitch Remove
 * @desc Removes the glitch effect on the target battler(s).
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to remove the Horror Effect for.
 * @default ["user"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Horror_NoiseCreate
 * @text HORROR: Noise Create
 * @desc Creates the noise effect on the target battler(s).
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to create the Horror Effect for.
 * @default ["user"]
 *
 * @arg noise:num
 * @text Noise Rate
 * @parent FilterNoise
 * @desc Noise rate to be used with the target.
 * @default 0.3
 *
 * @arg animated:eval
 * @text Noise Animated
 * @parent FilterNoise
 * @type boolean
 * @on Animate
 * @off Static
 * @desc Animate the noise for the target?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Horror_NoiseRemove
 * @text HORROR: Noise Remove
 * @desc Removes the noise effect on the target battler(s).
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to remove the Horror Effect for.
 * @default ["user"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Horror_TVCreate
 * @text HORROR: TV Create
 * @desc Creates the TV effect on the target battler(s).
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to create the Horror Effect for.
 * @default ["user"]
 *
 * @arg lineWidth:num
 * @text TV Line Thickness
 * @parent FilterTV
 * @type number
 * @min 1
 * @desc Default TV line thickness
 * Lower = thinner     Higher = thicker
 * @default 5
 *
 * @arg vignetting:num
 * @text TV Corner Size
 * @parent FilterTV
 * @desc Default TV line corner size
 * Lower = smaller     Higher = bigger
 * @default 0.3
 *
 * @arg animated:eval
 * @text TV Animated
 * @parent FilterTV
 * @type boolean
 * @on Animate
 * @off Static
 * @desc Animate the TV?
 * @default true
 *
 * @arg aniSpeed:num
 * @text TV Speed
 * @parent FilterTV
 * @desc Speed used to animate the TV if animated
 * Lower = slower     Higher = faster
 * @default 0.25
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Horror_TVRemove
 * @text HORROR: TV Remove
 * @desc Removes the TV effect on the target battler(s).
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to remove the Horror Effect for.
 * @default ["user"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceSpaceImpact
 * @text -
 * @desc -
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceBreakImpact
 * @text Action Sequences - Impact
 * @desc These Action Sequences are related to creating impact.
 * Requires VisuMZ_3_ActSeqImpact!
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Impact_ColorBreak
 * @text IMPACT: Color Break
 * @desc Breaks the colors on the screen before reassembling.
 * Requires VisuMZ_3_ActSeqImpact!
 * 
 * @arg Intensity:eval
 * @text Intensity
 * @desc What is the intensity of the color break effect?
 * @default 60
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc What is the duration of the color break effect?
 * @default 60
 *
 * @arg EasingType:str
 * @text Easing Type
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * @default OutBack
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Impact_MotionBlurScreen
 * @text IMPACT: Motion Blur Screen
 * @desc Creates a motion blur on the whole screen.
 * Requires VisuMZ_3_ActSeqImpact!
 *
 * @arg Angle:eval
 * @text Angle
 * @desc Determine what angle to make the motion blur at.
 * @default Math.randomInt(360)
 *
 * @arg Rate:eval
 * @text Intensity Rate
 * @desc This determines intensity rate of the motion blur.
 * Use a number between 0 and 1.
 * @default 0.1
 *
 * @arg Duration:num
 * @text Duration
 * @type Number
 * @min 1
 * @desc How many frames should the motion blur last?
 * What do you want to be its duration?
 * @default 30
 *
 * @arg EasingType:str
 * @text Easing Type
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * @default InOutSine
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Impact_MotionBlurTarget
 * @text IMPACT: Motion Blur Target(s)
 * @desc Creates a motion blur on selected target(s).
 * Requires VisuMZ_3_ActSeqImpact!
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to create motion blur effects for.
 * @default ["user"]
 *
 * @arg Angle:eval
 * @text Angle
 * @desc Determine what angle to make the motion blur at.
 * @default Math.randomInt(360)
 *
 * @arg Rate:eval
 * @text Intensity Rate
 * @desc This determines intensity rate of the motion blur.
 * Use a number between 0 and 1.
 * @default 0.5
 *
 * @arg Duration:num
 * @text Duration
 * @type Number
 * @min 1
 * @desc How many frames should the motion blur last?
 * What do you want to be its duration?
 * @default 30
 *
 * @arg EasingType:str
 * @text Easing Type
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * @default InOutSine
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Impact_MotionTrailCreate
 * @text IMPACT: Motion Trail Create
 * @desc Creates a motion trail effect for the target(s).
 * Requires VisuMZ_3_ActSeqImpact!
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to create motion trail effects for.
 * @default ["user"]
 *
 * @arg delay:num
 * @text Delay
 * @type Number
 * @min 1
 * @desc How many frames to delay by when creating a motion trail?
 * The higher the delay, the less after images there are.
 * @default 1
 *
 * @arg duration:num
 * @text Duration
 * @type Number
 * @min 1
 * @desc How many frames should the motion trail last?
 * What do you want to be its duration?
 * @default 30
 *
 * @arg hue:num
 * @text Hue
 * @type Number
 * @min 0
 * @max 255
 * @desc What do you want to be the hue for the motion trail?
 * @default 0
 *
 * @arg opacityStart:num
 * @text Starting Opacity
 * @type Number
 * @min 0
 * @max 255
 * @desc What starting opacity value do you want for the motion
 * trail? Opacity values decrease over time.
 * @default 200
 *
 * @arg tone:eval
 * @text Tone
 * @desc What tone do you want for the motion trail?
 * Format: [Red, Green, Blue, Gray]
 * @default [0, 0, 0, 0]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Impact_MotionTrailRemove
 * @text IMPACT: Motion Trail Remove
 * @desc Removes the motion trail effect from the target(s).
 * Requires VisuMZ_3_ActSeqImpact!
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to clear motion trail effects for.
 * @default ["user"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Impact_ShockwavePoint
 * @text IMPACT: Shockwave at Point
 * @desc Creates a shockwave at the designated coordinates.
 * Requires VisuMZ_3_ActSeqImpact!
 * 
 * @arg Coordinates
 * 
 * @arg X:eval
 * @text Point: X
 * @parent Coordinates
 * @desc What x coordinate do you want to create a shockwave at?
 * You can use JavaScript code.
 * @default Graphics.width / 2
 * 
 * @arg Y:eval
 * @text Point: Y
 * @parent Coordinates
 * @desc What y coordinate do you want to create a shockwave at?
 * You can use JavaScript code.
 * @default (Graphics.height - 200) / 2
 * 
 * @arg Amp:eval
 * @text Amplitude
 * @desc What is the aplitude of the shockwave effect?
 * @default 30
 * 
 * @arg Wave:eval
 * @text Wavelength
 * @desc What is the wavelength of the shockwave effect?
 * @default 160
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc What is the duration of the shockwave?
 * @default 60
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Impact_ShockwaveEachTargets
 * @text IMPACT: Shockwave from Each Target(s)
 * @desc Creates a shockwave at each of the target(s) location(s).
 * Requires VisuMZ_3_ActSeqImpact!
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to start a shockwave from.
 * @default ["all targets"]
 * 
 * @arg TargetLocation:str
 * @text Target Location
 * @parent Targets2:arraystr
 * @type combo
 * @option front head
 * @option front center
 * @option front base
 * @option middle head
 * @option middle center
 * @option middle base
 * @option back head
 * @option back center
 * @option back base
 * @desc Select which part target group to start a shockwave from.
 * @default middle center
 * 
 * @arg OffsetX:eval
 * @text Offset X
 * @parent TargetLocation:str
 * @desc How much to offset the shockwave X point by.
 * Negative: left. Positive: right.
 * @default +0
 * 
 * @arg OffsetY:eval
 * @text Offset Y
 * @parent TargetLocation:str
 * @desc How much to offset the shockwave Y point by.
 * Negative: up. Positive: down.
 * @default +0
 * 
 * @arg Amp:eval
 * @text Amplitude
 * @desc What is the aplitude of the shockwave effect?
 * @default 30
 * 
 * @arg Wave:eval
 * @text Wavelength
 * @desc What is the wavelength of the shockwave effect?
 * @default 160
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc What is the duration of the shockwave?
 * @default 60
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Impact_ShockwaveCenterTargets
 * @text IMPACT: Shockwave from Target(s) Center
 * @desc Creates a shockwave from the center of the target(s).
 * Requires VisuMZ_3_ActSeqImpact!
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to start a shockwave from.
 * @default ["all targets"]
 * 
 * @arg TargetLocation:str
 * @text Target Location
 * @parent Targets2:arraystr
 * @type combo
 * @option front head
 * @option front center
 * @option front base
 * @option middle head
 * @option middle center
 * @option middle base
 * @option back head
 * @option back center
 * @option back base
 * @desc Select which part target group to start a shockwave from.
 * @default middle center
 * 
 * @arg OffsetX:eval
 * @text Offset X
 * @parent TargetLocation:str
 * @desc How much to offset the shockwave X point by.
 * Negative: left. Positive: right.
 * @default +0
 * 
 * @arg OffsetY:eval
 * @text Offset Y
 * @parent TargetLocation:str
 * @desc How much to offset the shockwave Y point by.
 * Negative: up. Positive: down.
 * @default +0
 * 
 * @arg Amp:eval
 * @text Amplitude
 * @desc What is the aplitude of the shockwave effect?
 * @default 30
 * 
 * @arg Wave:eval
 * @text Wavelength
 * @desc What is the wavelength of the shockwave effect?
 * @default 160
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc What is the duration of the shockwave?
 * @default 60
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Impact_ZoomBlurPoint
 * @text IMPACT: Zoom Blur at Point
 * @desc Creates a zoom blur at the designated coordinates.
 * Requires VisuMZ_3_ActSeqImpact!
 * 
 * @arg Coordinates
 * 
 * @arg X:eval
 * @text Point: X
 * @parent Coordinates
 * @desc What x coordinate do you want to focus the zoom at?
 * You can use JavaScript code.
 * @default Graphics.width / 2
 * 
 * @arg Y:eval
 * @text Point: Y
 * @parent Coordinates
 * @desc What y coordinate do you want to focus the zoom at?
 * You can use JavaScript code.
 * @default (Graphics.height - 200) / 2
 * 
 * @arg Strength:eval
 * @text Zoom Strength
 * @desc What is the strength of the zoom effect?
 * Use a number between 0 and 1.
 * @default 0.5
 * 
 * @arg Radius:eval
 * @text Visible Radius
 * @desc How much of a radius should be visible from the center?
 * @default 0
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc What is the duration of the zoom blur?
 * @default 60
 *
 * @arg EasingType:str
 * @text Easing Type
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * @default OutSine
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Impact_ZoomBlurTargetCenter
 * @text IMPACT: Zoom Blur at Target(s) Center
 * @desc Creates a zoom blur at the center of targets.
 * Requires VisuMZ_3_ActSeqImpact!
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to start a zoom blur from.
 * @default ["user"]
 * 
 * @arg TargetLocation:str
 * @text Target Location
 * @parent Targets2:arraystr
 * @type combo
 * @option front head
 * @option front center
 * @option front base
 * @option middle head
 * @option middle center
 * @option middle base
 * @option back head
 * @option back center
 * @option back base
 * @desc Select which part target group to start a zoom blur from.
 * @default middle center
 * 
 * @arg OffsetX:eval
 * @text Offset X
 * @parent TargetLocation:str
 * @desc How much to offset the zoom blur X point by.
 * Negative: left. Positive: right.
 * @default +0
 * 
 * @arg OffsetY:eval
 * @text Offset Y
 * @parent TargetLocation:str
 * @desc How much to offset the zoom blur Y point by.
 * Negative: up. Positive: down.
 * @default +0
 * 
 * @arg Strength:eval
 * @text Zoom Strength
 * @desc What is the strength of the zoom effect?
 * Use a number between 0 and 1.
 * @default 0.5
 * 
 * @arg Radius:eval
 * @text Visible Radius
 * @desc How much of a radius should be visible from the center?
 * @default 0
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc What is the duration of the zoom blur?
 * @default 60
 *
 * @arg EasingType:str
 * @text Easing Type
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * @default OutSine
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceSpaceMechanics
 * @text -
 * @desc -
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceBreakMechanics
 * @text Action Sequences - Mechanics
 * @desc These Action Sequences are related to various mechanics
 * related to the battle system.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_ActionEffect
 * @text MECH: Action Effect
 * @desc Causes the unit(s) to take damage/healing from action and
 * incurs any changes made such as buffs and states.
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to receive the current action's effects.
 * @default ["all targets"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_AddBuffDebuff
 * @text MECH: Add Buff/Debuff
 * @desc Adds buff(s)/debuff(s) to unit(s). 
 * Determine which parameters are affected and their durations.
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to receive the buff(s) and/or debuff(s).
 * @default ["user"]
 * 
 * @arg Buffs:arraystr
 * @text Buff Parameters
 * @type combo[]
 * @option MaxHP
 * @option MaxMP
 * @option ATK
 * @option DEF
 * @option MAT
 * @option MDF
 * @option AGI
 * @option LUK
 * @desc Select which parameter(s) to buff.
 * Insert a parameter multiple times to raise its stacks.
 * @default ["ATK"]
 *
 * @arg Debuffs:arraystr
 * @text Debuff Parameters
 * @type combo[]
 * @option MaxHP
 * @option MaxMP
 * @option ATK
 * @option DEF
 * @option MAT
 * @option MDF
 * @option AGI
 * @option LUK
 * @desc Select which parameter(s) to debuff.
 * Insert a parameter multiple times to raise its stacks.
 * @default ["DEF"]
 * 
 * @arg Turns:eval
 * @text Turns
 * @desc Number of turns to set the parameter(s) buffs to.
 * You may use JavaScript code.
 * @default 5
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_AddState
 * @text MECH: Add State
 * @desc Adds state(s) to unit(s).
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to receive the buff(s).
 * @default ["user"]
 * 
 * @arg States:arraynum
 * @text States
 * @type state[]
 * @desc Select which state ID(s) to add to unit(s).
 * Insert multiple state ID's to add multiple at once.
 * @default ["4"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_ArmorPenetration
 * @text MECH: Armor Penetration
 * @desc Adds an extra layer of defensive penetration/reduction.
 * You may use JavaScript code for any of these.
 *
 * @arg ArmorPenetration
 * @text Armor/Magic Penetration
 * 
 * @arg ArPenRate:eval
 * @text Rate
 * @parent ArmorPenetration
 * @desc Penetrates an extra multiplier of armor by this value.
 * @default 0.00
 * 
 * @arg ArPenFlat:eval
 * @text Flat
 * @parent ArmorPenetration
 * @desc Penetrates a flat amount of armor by this value.
 * @default 0
 *
 * @arg ArmorReduction
 * @text Armor/Magic Reduction
 * 
 * @arg ArRedRate:eval
 * @text Rate
 * @parent ArmorReduction
 * @desc Reduces an extra multiplier of armor by this value.
 * @default 0.00
 * 
 * @arg ArRedFlat:eval
 * @text Flat
 * @parent ArmorReduction
 * @desc Reduces a flat amount of armor by this value.
 * @default 0
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_AtbGauge
 * @text MECH: ATB Gauge
 * @desc Alters the ATB/TPB Gauges.
 * Requires VisuMZ_2_BattleSystemATB!
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to alter the ATB/TPB Gauges for.
 * @default ["all targets"]
 *
 * @arg Charging
 * 
 * @arg ChargeRate:eval
 * @text Charge Rate
 * @parent Charging
 * @desc Changes made to the ATB Gauge if it is currently charging.
 * @default -0.00
 * 
 * @arg Casting
 * 
 * @arg CastRate:eval
 * @text Cast Rate
 * @parent Casting
 * @desc Changes made to the ATB Gauge if it is currently casting.
 * @default -0.00
 * 
 * @arg Interrupt:eval
 * @text Interrupt?
 * @parent Casting
 * @type boolean
 * @on Interrupt
 * @off Don't Interrupt
 * @desc Interrupt the ATB Gauge if it is currently casting?
 * @default false
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_BtbGain
 * @text MECH: BTB Brave Points
 * @desc Alters the target(s) Brave Points to an exact value.
 * Requires VisuMZ_2_BattleSystemBTB!
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to alter the ATB/TPB Gauges for.
 * @default ["all targets"]
 * 
 * @arg BravePoints:eval
 * @text Alter Brave Points By
 * @desc Alters the target(s) Brave Points.
 * Positive for gaining BP. Negative for losing BP.
 * @default +1
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_Collapse
 * @text MECH: Collapse
 * @desc Causes the unit(s) to perform its collapse animation
 * if the unit(s) has died.
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to process a death collapse.
 * @default ["all targets"]
 * 
 * @arg ForceDeath:eval
 * @text Force Death
 * @type boolean
 * @on On
 * @off Off
 * @desc Force death even if the unit has not reached 0 HP?
 * This will remove immortality.
 * @default false
 * 
 * @arg WaitForEffect:eval
 * @text Wait For Effect?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for the collapse effect to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_CtbOrder
 * @text MECH: CTB Order
 * @desc Alters the CTB Turn Order.
 * Requires VisuMZ_2_BattleSystemCTB!
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to alter the CTB Turn Order for.
 * @default ["all targets"]
 *
 * @arg ChangeOrderBy:eval
 * @text Change Order By
 * @parent Charging
 * @desc Changes turn order for target(s) by this amount.
 * Positive increases wait. Negative decreases wait.
 * @default +1
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_CtbSpeed
 * @text MECH: CTB Speed
 * @desc Alters the CTB Speed.
 * Requires VisuMZ_2_BattleSystemCTB!
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to alter the CTB Speed for.
 * @default ["all targets"]
 *
 * @arg ChargeRate:eval
 * @text Charge Rate
 * @parent Charging
 * @desc Changes made to the CTB Speed if it is currently charging.
 * @default -0.00
 * 
 * @arg CastRate:eval
 * @text Cast Rate
 * @parent Casting
 * @desc Changes made to the CTB Speed if it is currently casting.
 * @default -0.00
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_CustomDmgFormula
 * @text MECH: Custom Damage Formula
 * @desc Changes the current action's damage formula to custom.
 * This will assume the MANUAL damage style.
 * 
 * @arg Formula:str
 * @text Formula
 * @desc Changes the current action's damage formula to custom.
 * Use 'default' to revert the damage formula.
 * @default default
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_DamagePopup
 * @text MECH: Damage Popup
 * @desc Causes the unit(s) to display the current state of
 * damage received or healed.
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to prompt a damage popup.
 * @default ["all targets"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_DeathBreak
 * @text MECH: Dead Label Jump
 * @desc If the active battler is dead, jump to a specific label in the common event.
 * 
 * @arg JumpToLabel:str
 * @text Jump To Label
 * @desc If the active battler is dead, jump to this specific label in the common event.
 * @default Untitled
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_FtbAction
 * @text MECH: FTB Action Count
 * @desc Alters the subject team's available Action Count.
 * Requires VisuMZ_2_BattleSystemFTB!
 * 
 * @arg ActionCount:eval
 * @text Action Count
 * @desc Alters the subject team's available Action Count.
 * Positive for gaining actions. Negative for losing actions.
 * @default +1
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_HpMpTp
 * @text MECH: HP, MP, TP
 * @desc Alters the HP, MP, and TP values for unit(s).
 * Positive values for healing. Negative values for damage.
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to receive the current action's effects.
 * @default ["user"]
 *
 * @arg HP
 * 
 * @arg HP_Rate:eval
 * @text HP Rate
 * @parent HP
 * @desc Changes made to HP based on rate.
 * Positive values for healing. Negative values for damage.
 * @default +0.00
 * 
 * @arg HP_Flat:eval
 * @text HP Flat
 * @parent HP
 * @desc Flat changes made to HP.
 * Positive values for healing. Negative values for damage.
 * @default +0
 * 
 * @arg MP
 * 
 * @arg MP_Rate:eval
 * @text MP Rate
 * @parent MP
 * @desc Changes made to MP based on rate.
 * Positive values for healing. Negative values for damage.
 * @default +0.00
 * 
 * @arg MP_Flat:eval
 * @text MP Flat
 * @parent MP
 * @desc Flat changes made to MP.
 * Positive values for healing. Negative values for damage.
 * @default +0
 *
 * @arg TP
 * 
 * @arg TP_Rate:eval
 * @text TP Rate
 * @parent TP
 * @desc Changes made to TP based on rate.
 * Positive values for healing. Negative values for damage.
 * @default +0.00
 * 
 * @arg TP_Flat:eval
 * @text TP Flat
 * @parent TP
 * @desc Flat changes made to TP.
 * Positive values for healing. Negative values for damage.
 * @default +0
 * 
 * @arg ShowPopup:eval
 * @text Damage Popup?
 * @type boolean
 * @on On
 * @off Off
 * @desc Display a damage popup after?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_Immortal
 * @text MECH: Immortal
 * @desc Changes the immortal flag of targets. If immortal flag is
 * removed and a unit would die, collapse that unit.
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Alter the immortal flag of these groups. If immortal flag
 * is removed and a unit would die, collapse that unit.
 * @default ["user","all targets"]
 * 
 * @arg Immortal:eval
 * @text Immortal
 * @type boolean
 * @on On
 * @off Off
 * @desc Turn immortal flag for unit(s) on/off?
 * @default false
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_Multipliers
 * @text MECH: Multipliers
 * @desc Changes the multipliers for the current action.
 * You may use JavaScript code for any of these.
 *
 * @arg CriticalHit
 * @text Critical Hit%
 * 
 * @arg CriticalHitRate:eval
 * @text Rate
 * @parent CriticalHit
 * @desc Affects chance to land a critical hit by this multiplier.
 * @default 1.00
 * 
 * @arg CriticalHitFlat:eval
 * @text Flat
 * @parent CriticalHit
 * @desc Affects chance to land a critical hit by this flat bonus.
 * @default +0.00
 *
 * @arg CriticalDmg
 * @text Critical Damage
 * 
 * @arg CriticalDmgRate:eval
 * @text Rate
 * @parent CriticalDmg
 * @desc Affects critical damage by this multiplier.
 * @default 1.00
 * 
 * @arg CriticalDmgFlat:eval
 * @text Flat
 * @parent CriticalDmg
 * @desc Affects critical damage by this flat bonus.
 * @default +0.00
 *
 * @arg Damage
 * @text Damage/Healing
 * 
 * @arg DamageRate:eval
 * @text Rate
 * @parent Damage
 * @desc Sets the damage/healing multiplier for current action.
 * @default 1.00
 * 
 * @arg DamageFlat:eval
 * @text Flat
 * @parent Damage
 * @desc Sets the damage/healing bonus for current action.
 * @default +0.00
 *
 * @arg HitRate
 * @text Hit Rate
 * 
 * @arg HitRate:eval
 * @text Rate
 * @parent HitRate
 * @desc Affects chance to connect attack by this multiplier.
 * @default 1.00
 * 
 * @arg HitFlat:eval
 * @text Flat
 * @parent HitRate
 * @desc Affects chance to connect attack by this flat bonus.
 * @default +0.00
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_RemoveBuffDebuff
 * @text MECH: Remove Buff/Debuff
 * @desc Removes buff(s)/debuff(s) from unit(s). 
 * Determine which parameters are removed.
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to have the buff(s) and/or debuff(s) removed.
 * @default ["user"]
 * 
 * @arg Buffs:arraystr
 * @text Buff Parameters
 * @type combo[]
 * @option MaxHP
 * @option MaxMP
 * @option ATK
 * @option DEF
 * @option MAT
 * @option MDF
 * @option AGI
 * @option LUK
 * @desc Select which buffed parameter(s) to remove.
 * @default ["MaxHP","MaxMP","ATK","DEF","MAT","MDF","AGI","LUK"]
 *
 * @arg Debuffs:arraystr
 * @text Debuff Parameters
 * @type combo[]
 * @option MaxHP
 * @option MaxMP
 * @option ATK
 * @option DEF
 * @option MAT
 * @option MDF
 * @option AGI
 * @option LUK
 * @desc Select which debuffed parameter(s) to remove.
 * @default ["MaxHP","MaxMP","ATK","DEF","MAT","MDF","AGI","LUK"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_RemoveState
 * @text MECH: Remove State
 * @desc Remove state(s) from unit(s).
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to have states removed from.
 * @default ["user"]
 * 
 * @arg States:arraynum
 * @text States
 * @type state[]
 * @desc Select which state ID(s) to remove from unit(s).
 * Insert multiple state ID's to remove multiple at once.
 * @default ["4"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_StbExploit
 * @text MECH: STB Exploit Effect
 * @desc Utilize the STB Exploitation mechanics!
 * Requires VisuMZ_2_BattleSystemSTB!
 * 
 * @arg Exploited:eval
 * @text Target(s) Exploited?
 * @type boolean
 * @on Exploit
 * @off Don't
 * @desc Exploit the below targets?
 * @default true
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to become exploited.
 * @default ["all targets"]
 * 
 * @arg ForceExploited:eval
 * @text Force Exploitation
 * @type boolean
 * @on Force
 * @off Don't
 * @desc Force the exploited status?
 * @default false
 * 
 * @arg Exploiter:eval
 * @text User Exploiter?
 * @type boolean
 * @on Exploit
 * @off Don't
 * @desc Allow the user to become the exploiter?
 * @default true
 * 
 * @arg ForceExploited:eval
 * @text Force Exploitation
 * @type boolean
 * @on Force
 * @off Don't
 * @desc Force the exploiter status?
 * @default false
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_StbExtraAction
 * @text MECH: STB Extra Action
 * @desc Adds an extra action for the currently active battler.
 * Requires VisuMZ_2_BattleSystemSTB!
 * 
 * @arg Actions:eval
 * @text Extra Actions
 * @parent Charging
 * @desc How many extra actions should the active battler gain?
 * You may use JavaScript code.
 * @default 1
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_StbRemoveExcessActions
 * @text MECH: STB Remove Excess Actions
 * @desc Removes excess actions from the active battler.
 * Requires VisuMZ_2_BattleSystemSTB!
 * 
 * @arg Actions:eval
 * @text Remove Actions
 * @parent Charging
 * @desc How many actions to remove from the active battler?
 * You may use JavaScript code.
 * @default 99
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_TextPopup
 * @text MECH: Text Popup
 * @desc Causes the unit(s) to display a text popup.
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to prompt a text popup.
 * @default ["target"]
 * 
 * @arg Text:str
 * @text Text
 * @desc What text do you wish to display?
 * @default Text
 * 
 * @arg TextColor:str
 * @text Text Color
 * @parent Text:str
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default #ffffff
 *
 * @arg FlashColor:eval
 * @text Flash Color
 * @parent Popups
 * @desc Adjust the popup's flash color.
 * Format: [red, green, blue, alpha]
 * @default [255, 0, 0, 160]
 * 
 * @arg FlashDuration:num
 * @text Flash Duration
 * @parent FlashColor:eval
 * @type Number
 * @desc What is the frame duration of the flash effect?
 * @default 60
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_VariablePopup
 * @text MECH: Variable Popup
 * @desc Causes the unit(s) to display a popup using the data
 * stored inside a variable.
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select unit(s) to prompt a text popup.
 * @default ["target"]
 * 
 * @arg Variable:num
 * @text Variable ID
 * @type variable
 * @desc Get data from which variable to display as a popup?
 * @default 1
 * 
 * @arg DigitGrouping:eval
 * @text Digit Grouping
 * @parent Variable:num
 * @type boolean
 * @on Group Digits
 * @off Don't Group
 * @desc Use digit grouping to separate numbers?
 * Requires VisuMZ_0_CoreEngine!
 * @default true
 * 
 * @arg TextColor:str
 * @text Text Color
 * @parent Variable:num
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default #ffffff
 *
 * @arg FlashColor:eval
 * @text Flash Color
 * @parent Popups
 * @desc Adjust the popup's flash color.
 * Format: [red, green, blue, alpha]
 * @default [0, 0, 0, 0]
 * 
 * @arg FlashDuration:num
 * @text Flash Duration
 * @parent FlashColor:eval
 * @type Number
 * @desc What is the frame duration of the flash effect?
 * @default 60
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Mechanics_WaitForEffect
 * @text MECH: Wait For Effect
 * @desc Waits for the effects to complete before performing next command.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceSpaceMotion
 * @text -
 * @desc -
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceBreakMotion
 * @text Action Sequences - Motion
 * @desc These Action Sequences allow you the ability to control
 * the motions of sideview sprites.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Motion_ClearFreezeFrame
 * @text MOTION: Clear Freeze Frame
 * @desc Clears any freeze frames from the unit(s).
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) to clear freeze frames for.
 * @default ["user"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Motion_FreezeMotionFrame
 * @text MOTION: Freeze Motion Frame
 * @desc Forces a freeze frame instantly at the selected motion.
 * Automatically clears with a new motion.
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) to freeze motions for.
 * @default ["user"]
 *
 * @arg MotionType:str
 * @text Motion Type
 * @type combo
 * @option walk
 * @option wait
 * @option chant
 * @option guard
 * @option damage
 * @option evade
 * @option attack
 * @option thrust
 * @option swing
 * @option missile
 * @option skill
 * @option spell
 * @option item
 * @option escape
 * @option victory
 * @option dying
 * @option abnormal
 * @option sleep
 * @option dead
 * @desc Freeze this motion for the unit(s).
 * @default attack
 * 
 * @arg Frame:num
 * @text Frame Index
 * @desc Which frame do you want to freeze the motion on?
 * Frame index values start at 0.
 * @default 2
 *
 * @arg ShowWeapon:eval
 * @text Show Weapon?
 * @type combo
 * @type boolean
 * @on Show
 * @off Hide
 * @desc If using 'attack', 'thrust', 'swing', or 'missile',
 * display the weapon sprite?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Motion_MotionType
 * @text MOTION: Motion Type
 * @desc Causes the unit(s) to play the selected motion.
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) to perform a motion.
 * @default ["user"]
 *
 * @arg MotionType:str
 * @text Motion Type
 * @type combo
 * @option walk
 * @option wait
 * @option chant
 * @option guard
 * @option damage
 * @option evade
 * @option attack
 * @option thrust
 * @option swing
 * @option missile
 * @option skill
 * @option spell
 * @option item
 * @option escape
 * @option victory
 * @option dying
 * @option abnormal
 * @option sleep
 * @option dead
 * @desc Play this motion for the unit(s).
 * @default attack
 *
 * @arg ShowWeapon:eval
 * @text Show Weapon?
 * @type combo
 * @type boolean
 * @on Show
 * @off Hide
 * @desc If using 'attack', 'thrust', 'swing', or 'missile',
 * display the weapon sprite?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Motion_PerformAction
 * @text MOTION: Perform Action
 * @desc Causes the unit(s) to play the proper motion based
 * on the current action.
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) to perform a motion.
 * @default ["user"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Motion_RefreshMotion
 * @text MOTION: Refresh Motion
 * @desc Cancels any set motions unit(s) has to do and use
 * their most natural motion at the moment.
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) to refresh their motion state.
 * @default ["user"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Motion_WaitMotionFrame
 * @text MOTION: Wait By Motion Frame
 * @desc Creates a wait equal to the number of motion frames passing.
 * Time is based on Plugin Parameters => Actors => Motion Speed.
 *
 * @arg MotionFrameWait:num
 * @text Motion Frames to Wait?
 * @type number
 * @min 1
 * @desc Each "frame" is equal to the value found in
 * Plugin Parameters => Actors => Motion Speed
 * @default 1
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceSpaceMovement
 * @text -
 * @desc -
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceBreakMovement
 * @text Action Sequences - Movement
 * @desc These Action Sequences allow you the ability to control
 * the sprites of actors and enemies in battle.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Movement_BattleStep
 * @text MOVE: Battle Step
 * @desc Causes the unit(s) to move forward past their home position
 * to prepare for action.
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) to move.
 * @default ["user"]
 * 
 * @arg WaitForMovement:eval
 * @text Wait For Movement?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for movement to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Movement_FaceDirection
 * @text MOVE: Face Direction
 * @desc Causes the unit(s) to face forward or backward.
 * Sideview-only!
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) to change direction.
 * @default ["user"]
 * 
 * @arg Direction:str
 * @text Direction
 * @type combo
 * @option forward
 * @option backward
 * @option random
 * @desc Select which direction to face.
 * @default forward
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Movement_FacePoint
 * @text MOVE: Face Point
 * @desc Causes the unit(s) to face a point on the screen.
 * Sideview-only!
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) to change direction.
 * @default ["user"]
 * 
 * @arg Point:str
 * @text Point
 * @type combo
 * @option home
 * @option center
 * @option point x, y
 * @desc Select which point to face.
 * Replace 'x' and 'y' with coordinates
 * @default home
 * 
 * @arg FaceAway:eval
 * @text Face Away From?
 * @type boolean
 * @on Turn Away
 * @off Face Directly
 * @desc Face away from the point instead?
 * @default false
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Movement_FaceTarget
 * @text MOVE: Face Target(s)
 * @desc Causes the unit(s) to face other targets on the screen.
 * Sideview-only!
 * 
 * @arg Targets1:arraystr
 * @text Targets (facing)
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) to change direction.
 * @default ["user"]
 * 
 * @arg Targets2:arraystr
 * @text Targets (destination)
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) for the turning unit(s) to face.
 * @default ["current target"]
 * 
 * @arg FaceAway:eval
 * @text Face Away From?
 * @type boolean
 * @on Turn Away
 * @off Face Directly
 * @desc Face away from the unit(s) instead?
 * @default false
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Movement_Float
 * @text MOVE: Float
 * @desc Causes the unit(s) to float above the ground.
 * Sideview-only!
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) to make float.
 * @default ["user"]
 * 
 * @arg Height:eval
 * @text Desired Height
 * @desc Vertical distance to float upward.
 * You may use JavaScript code.
 * @default 100
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc Duration in frames for total float amount.
 * @default 12
 *
 * @arg EasingType:str
 * @text Float Easing
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * Requires VisuMZ_0_CoreEngine.
 * @default Linear
 * 
 * @arg WaitForFloat:eval
 * @text Wait For Float?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for floating to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Movement_HomeReset
 * @text MOVE: Home Reset
 * @desc Causes the unit(s) to move back to their home position(s)
 * and face back to their original direction(s).
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) to move.
 * @default ["alive battlers"]
 * 
 * @arg WaitForMovement:eval
 * @text Wait For Movement?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for movement to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Movement_Jump
 * @text MOVE: Jump
 * @desc Causes the unit(s) to jump into the air.
 * Sideview-only!
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) to make jump.
 * @default ["user"]
 * 
 * @arg Height:eval
 * @text Desired Height
 * @desc Max jump height to go above the ground
 * You may use JavaScript code.
 * @default 100
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc Duration in frames for total jump amount.
 * @default 12
 * 
 * @arg WaitForJump:eval
 * @text Wait For Jump?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for jumping to complete before performing next command?
 * @default false
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Movement_MoveBy
 * @text MOVE: Move Distance
 * @desc Moves unit(s) by a distance from their current position(s).
 * Sideview-only!
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) to move.
 * @default ["user"]
 *
 * @arg DistanceAdjust:str
 * @text Distance Adjustment
 * @type select
 * @option Normal - No adjustments made
 * @value none
 * @option Horizontal - Actors adjust left, Enemies adjust right
 * @value horz
 * @option Vertical - Actors adjust Up, Enemies adjust down
 * @value vert
 * @option Both - Applies both Horizontal and Vertical
 * @value horz + vert
 * @desc Makes adjustments to distance values to determine
 * which direction to move unit(s).
 * @default horz
 * 
 * @arg DistanceX:eval
 * @text Distance: X
 * @parent DistanceAdjust:str
 * @desc Horizontal distance to move.
 * You may use JavaScript code.
 * @default 48
 * 
 * @arg DistanceY:eval
 * @text Distance: Y
 * @parent DistanceAdjust:str
 * @desc Vertical distance to move.
 * You may use JavaScript code.
 * @default 0
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc Duration in frames for total movement amount.
 * @default 12
 * 
 * @arg FaceDirection:eval
 * @text Face Destination?
 * @type boolean
 * @on Turn
 * @off Don't
 * @desc Turn and face the destination?
 * @default true
 *
 * @arg EasingType:str
 * @text Movement Easing
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * Requires VisuMZ_0_CoreEngine.
 * @default Linear
 *
 * @arg MotionType:str
 * @text Movement Motion
 * @type combo
 * @option walk
 * @option wait
 * @option chant
 * @option guard
 * @option damage
 * @option evade
 * @option thrust
 * @option swing
 * @option missile
 * @option skill
 * @option spell
 * @option item
 * @option escape
 * @option victory
 * @option dying
 * @option abnormal
 * @option sleep
 * @option dead
 * @desc Play this motion for the unit(s).
 * @default walk
 * 
 * @arg WaitForMovement:eval
 * @text Wait For Movement?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for movement to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Movement_MoveToPoint
 * @text MOVE: Move To Point
 * @desc Moves unit(s) to a designated point on the screen.
 * Sideview-only! Points based off Graphics.boxWidth/Height.
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) to move.
 * @default ["user"]
 * 
 * @arg Destination:str
 * @text Destination Point
 * @type combo
 * @option home
 * @option center
 * @option point x, y
 * @desc Select which point to face.
 * Replace 'x' and 'y' with coordinates
 * @default home
 *
 * @arg OffsetAdjust:str
 * @text Offset Adjustment
 * @parent Destination:str
 * @type select
 * @option Normal - No adjustments made
 * @value none
 * @option Horizontal - Actors adjust left, Enemies adjust right
 * @value horz
 * @option Vertical - Actors adjust Up, Enemies adjust down
 * @value vert
 * @option Both - Applies both Horizontal and Vertical
 * @value horz + vert
 * @desc Makes adjustments to offset values to determine
 * which direction to adjust the destination by.
 * @default horz
 * 
 * @arg OffsetX:eval
 * @text Offset: X
 * @parent OffsetAdjust:str
 * @desc Horizontal offset to move.
 * You may use JavaScript code.
 * @default 0
 * 
 * @arg OffsetY:eval
 * @text Offset: Y
 * @parent OffsetAdjust:str
 * @desc Vertical offset to move.
 * You may use JavaScript code.
 * @default 0
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc Duration in frames for total movement amount.
 * @default 12
 * 
 * @arg FaceDirection:eval
 * @text Face Destination?
 * @type boolean
 * @on Turn
 * @off Don't
 * @desc Turn and face the destination?
 * @default true
 *
 * @arg EasingType:str
 * @text Movement Easing
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * Requires VisuMZ_0_CoreEngine.
 * @default Linear
 *
 * @arg MotionType:str
 * @text Movement Motion
 * @type combo
 * @option walk
 * @option wait
 * @option chant
 * @option guard
 * @option damage
 * @option evade
 * @option thrust
 * @option swing
 * @option missile
 * @option skill
 * @option spell
 * @option item
 * @option escape
 * @option victory
 * @option dying
 * @option abnormal
 * @option sleep
 * @option dead
 * @desc Play this motion for the unit(s).
 * @default walk
 * 
 * @arg WaitForMovement:eval
 * @text Wait For Movement?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for movement to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Movement_MoveToTarget
 * @text MOVE: Move To Target(s)
 * @desc Moves unit(s) to another unit(s) on the battle field.
 * Sideview-only!
 * 
 * @arg Targets1:arraystr
 * @text Targets (Moving)
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) to move.
 * @default ["user"]
 * 
 * @arg Targets2:arraystr
 * @text Targets (Destination)
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) to move to.
 * @default ["all targets"]
 * 
 * @arg TargetLocation:str
 * @text Target Location
 * @parent Targets2:arraystr
 * @type combo
 * @option front head
 * @option front center
 * @option front base
 * @option middle head
 * @option middle center
 * @option middle base
 * @option back head
 * @option back center
 * @option back base
 * @desc Select which part target group to move to.
 * @default front base
 * 
 * @arg MeleeDistance:eval
 * @text Melee Distance
 * @parent TargetLocation:str
 * @desc The melee distance away from the target location
 * in addition to the battler's width.
 * @default 24
 *
 * @arg OffsetAdjust:str
 * @text Offset Adjustment
 * @parent Targets2:arraystr
 * @type select
 * @option Normal - No adjustments made
 * @value none
 * @option Horizontal - Actors adjust left, Enemies adjust right
 * @value horz
 * @option Vertical - Actors adjust Up, Enemies adjust down
 * @value vert
 * @option Both - Applies both Horizontal and Vertical
 * @value horz + vert
 * @desc Makes adjustments to offset values to determine
 * which direction to adjust the destination by.
 * @default horz
 * 
 * @arg OffsetX:eval
 * @text Offset: X
 * @parent OffsetAdjust:str
 * @desc Horizontal offset to move.
 * You may use JavaScript code.
 * @default 0
 * 
 * @arg OffsetY:eval
 * @text Offset: Y
 * @parent OffsetAdjust:str
 * @desc Vertical offset to move.
 * You may use JavaScript code.
 * @default 0
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc Duration in frames for total movement amount.
 * @default 12
 * 
 * @arg FaceDirection:eval
 * @text Face Destination?
 * @type boolean
 * @on Turn
 * @off Don't
 * @desc Turn and face the destination?
 * @default true
 *
 * @arg EasingType:str
 * @text Movement Easing
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * Requires VisuMZ_0_CoreEngine.
 * @default Linear
 *
 * @arg MotionType:str
 * @text Movement Motion
 * @type combo
 * @option walk
 * @option wait
 * @option chant
 * @option guard
 * @option damage
 * @option evade
 * @option thrust
 * @option swing
 * @option missile
 * @option skill
 * @option spell
 * @option item
 * @option escape
 * @option victory
 * @option dying
 * @option abnormal
 * @option sleep
 * @option dead
 * @desc Play this motion for the unit(s).
 * @default walk
 * 
 * @arg WaitForMovement:eval
 * @text Wait For Movement?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for movement to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Movement_Opacity
 * @text MOVE: Opacity
 * @desc Causes the unit(s) to change opacity.
 * Sideview-only!
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) to change opacity.
 * @default ["user"]
 * 
 * @arg Opacity:eval
 * @text Desired Opacity
 * @desc Change to this opacity value.
 * You may use JavaScript code.
 * @default 255
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc Duration in frames for opacity change.
 * @default 12
 *
 * @arg EasingType:str
 * @text Opacity Easing
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * Requires VisuMZ_0_CoreEngine.
 * @default Linear
 * 
 * @arg WaitForOpacity:eval
 * @text Wait For Opacity?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for opacity changes to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Movement_Scale
 * @text MOVE: Scale/Grow/Shrink
 * @desc Causes the unit(s) to scale, grow, or shrink?.
 * Sideview-only!
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) to change the scale of.
 * @default ["user"]
 * 
 * @arg ScaleX:eval
 * @text Scale X
 * @desc What target scale value do you want?
 * 1.0 is normal size.
 * @default 1.00
 * 
 * @arg ScaleY:eval
 * @text Scale Y
 * @desc What target scale value do you want?
 * 1.0 is normal size.
 * @default 1.00
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc Duration in frames to scale for.
 * @default 12
 *
 * @arg EasingType:str
 * @text Scale Easing
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * Requires VisuMZ_0_CoreEngine.
 * @default Linear
 * 
 * @arg WaitForScale:eval
 * @text Wait For Scale?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for scaling to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Movement_Skew
 * @text MOVE: Skew/Distort
 * @desc Causes the unit(s) to skew.
 * Sideview-only!
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) to skew.
 * @default ["user"]
 * 
 * @arg SkewX:eval
 * @text Skew X
 * @desc X variance to skew?
 * Use small values for the best results.
 * @default 0.00
 * 
 * @arg SkewY:eval
 * @text Skew Y
 * @desc Y variance to skew?
 * Use small values for the best results.
 * @default 0.00
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc Duration in frames to skew for.
 * @default 12
 *
 * @arg EasingType:str
 * @text Skew Easing
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * Requires VisuMZ_0_CoreEngine.
 * @default Linear
 * 
 * @arg WaitForSkew:eval
 * @text Wait For Skew?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for skew to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Movement_Spin
 * @text MOVE: Spin/Rotate
 * @desc Causes the unit(s) to spin.
 * Sideview-only!
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) to spin.
 * @default ["user"]
 * 
 * @arg Angle:eval
 * @text Angle
 * @desc How many degrees to spin?
 * @default 360
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc Duration in frames to spin for.
 * @default 12
 *
 * @arg EasingType:str
 * @text Spin Easing
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * Requires VisuMZ_0_CoreEngine.
 * @default Linear
 * 
 * @arg RevertAngle:eval
 * @text Revert Angle on Finish
 * @type boolean
 * @on Revert
 * @off Don't
 * @desc Revert angle after spinning?
 * @default true
 * 
 * @arg WaitForSpin:eval
 * @text Wait For Spin?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for spin to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Movement_WaitForFloat
 * @text MOVE: Wait For Float
 * @desc Waits for floating to complete before performing next command.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Movement_WaitForJump
 * @text MOVE: Wait For Jump
 * @desc Waits for jumping to complete before performing next command.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Movement_WaitForMovement
 * @text MOVE: Wait For Movement
 * @desc Waits for movement to complete before performing next command.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Movement_WaitForOpacity
 * @text MOVE: Wait For Opacity
 * @desc Waits for opacity changes to complete before performing next command.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Movement_WaitForScale
 * @text MOVE: Wait For Scale
 * @desc Waits for scaling to complete before performing next command.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Movement_WaitForSkew
 * @text MOVE: Wait For Skew
 * @desc Waits for skewing to complete before performing next command.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Movement_WaitForSpin
 * @text MOVE: Wait For Spin
 * @desc Waits for spinning to complete before performing next command.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceSpaceProjectile
 * @text -
 * @desc -
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceBreakProjectile
 * @text Action Sequences - Projectiles
 * @desc Create projectiles on the screen and fire them off at a target.
 * Requires VisuMZ_3_ActSeqProjectiles!
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Projectile_Animation
 * @text PROJECTILE: Animation
 * @desc Create an animation projectile and fire it at a target.
 * Requires VisuMZ_3_ActSeqProjectiles!
 * 
 * @arg Coordinates
 *
 * @arg Start:struct
 * @text Start Location
 * @parent Coordinates
 * @type struct<ProjectileStart>
 * @desc Settings to determine where the projectile(s) start from.
 * @default {"Type:str":"target","Targets:arraystr":"[\"user\"]","TargetCenter:eval":"false","PointX:eval":"Graphics.width / 2","PointY:eval":"Graphics.height / 2","OffsetX:eval":"+0","OffsetY:eval":"+0"}
 *
 * @arg Goal:struct
 * @text Goal Location
 * @parent Coordinates
 * @type struct<ProjectileGoal>
 * @desc Settings to determine where the projectile(s) start from.
 * @default {"Type:str":"target","Targets:arraystr":"[\"all targets\"]","TargetCenter:eval":"false","PointX:eval":"Graphics.width / 2","PointY:eval":"Graphics.height / 2","OffsetX:eval":"+0","OffsetY:eval":"+0"}
 * 
 * @arg Settings
 *
 * @arg AnimationID:num
 * @text Animation ID
 * @parent Settings
 * @type animation
 * @desc Determine which animation to use as a projectile.
 * @default 77
 * 
 * @arg Duration:eval
 * @text Duration
 * @parent Settings
 * @desc Duration for the projectile(s) to travel.
 * @default 20
 * 
 * @arg WaitForProjectile:eval
 * @text Wait For Projectile?
 * @parent Settings
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for projectile(s) to reach their destination before
 * going onto the next command?
 * @default true
 * 
 * @arg Extra:struct
 * @text Extra Settings
 * @type struct<ProjectileExAni>
 * @desc Add extra settings to the projectile?
 * @default {"AutoAngle:eval":"true","AngleOffset:eval":"+0","Arc:eval":"0","EasingType:str":"Linear","Spin:eval":"+0.0"}
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Projectile_Icon
 * @text PROJECTILE: Icon
 * @desc Create an icon projectile and fire it at a target.
 * Requires VisuMZ_3_ActSeqProjectiles!
 * 
 * @arg Coordinates
 *
 * @arg Start:struct
 * @text Start Location
 * @parent Coordinates
 * @type struct<ProjectileStart>
 * @desc Settings to determine where the projectile(s) start from.
 * @default {"Type:str":"target","Targets:arraystr":"[\"user\"]","TargetCenter:eval":"false","PointX:eval":"Graphics.width / 2","PointY:eval":"Graphics.height / 2","OffsetX:eval":"+0","OffsetY:eval":"+0"}
 *
 * @arg Goal:struct
 * @text Goal Location
 * @parent Coordinates
 * @type struct<ProjectileGoal>
 * @desc Settings to determine where the projectile(s) start from.
 * @default {"Type:str":"target","Targets:arraystr":"[\"all targets\"]","TargetCenter:eval":"false","PointX:eval":"Graphics.width / 2","PointY:eval":"Graphics.height / 2","OffsetX:eval":"+0","OffsetY:eval":"+0"}
 * 
 * @arg Settings
 *
 * @arg Icon:eval
 * @text Icon Index
 * @parent Settings
 * @desc Determine which icon to use as a projectile.
 * You may use JavaScript code.
 * @default 118
 * 
 * @arg Duration:eval
 * @text Duration
 * @parent Settings
 * @desc Duration for the projectile(s) to travel.
 * @default 20
 * 
 * @arg WaitForProjectile:eval
 * @text Wait For Projectile?
 * @parent Settings
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for projectile(s) to reach their destination before
 * going onto the next command?
 * @default true
 * 
 * @arg Extra:struct
 * @text Extra Settings
 * @type struct<ProjectileExtra>
 * @desc Add extra settings to the projectile?
 * @default {"AutoAngle:eval":"true","AngleOffset:eval":"+0","Arc:eval":"0","BlendMode:num":"0","EasingType:str":"Linear","Hue:eval":"0","Scale:eval":"1.0","Spin:eval":"+0.0"}
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Projectile_Picture
 * @text PROJECTILE: Picture
 * @desc Create a picture projectile and fire it at a target.
 * Requires VisuMZ_3_ActSeqProjectiles!
 * 
 * @arg Coordinates
 *
 * @arg Start:struct
 * @text Start Location
 * @parent Coordinates
 * @type struct<ProjectileStart>
 * @desc Settings to determine where the projectile(s) start from.
 * @default {"Type:str":"target","Targets:arraystr":"[\"user\"]","TargetCenter:eval":"false","PointX:eval":"Graphics.width / 2","PointY:eval":"Graphics.height / 2","OffsetX:eval":"+0","OffsetY:eval":"+0"}
 *
 * @arg Goal:struct
 * @text Goal Location
 * @parent Coordinates
 * @type struct<ProjectileGoal>
 * @desc Settings to determine where the projectile(s) start from.
 * @default {"Type:str":"target","Targets:arraystr":"[\"all targets\"]","TargetCenter:eval":"false","PointX:eval":"Graphics.width / 2","PointY:eval":"Graphics.height / 2","OffsetX:eval":"+0","OffsetY:eval":"+0"}
 * 
 * @arg Settings
 *
 * @arg Picture:str
 * @text Picture Filename
 * @parent Settings
 * @type file
 * @dir img/pictures/
 * @desc Determine which picture to use as a projectile.
 * @default Untitled
 * 
 * @arg Duration:eval
 * @text Duration
 * @parent Settings
 * @desc Duration for the projectile(s) to travel.
 * @default 20
 * 
 * @arg WaitForProjectile:eval
 * @text Wait For Projectile?
 * @parent Settings
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for projectile(s) to reach their destination before
 * going onto the next command?
 * @default true
 * 
 * @arg Extra:struct
 * @text Extra Settings
 * @type struct<ProjectileExtra>
 * @desc Add extra settings to the projectile?
 * @default {"AutoAngle:eval":"true","AngleOffset:eval":"+0","Arc:eval":"0","BlendMode:num":"0","EasingType:str":"Linear","Hue:eval":"0","Scale:eval":"1.0","Spin:eval":"+0.0"}
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceSpaceSkew
 * @text -
 * @desc -
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceBreakSkew
 * @text Action Sequences - Skew
 * @desc Allows you to have control over the camera skew.
 * Requires VisuMZ_3_ActSeqCamera!
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_ChangeSkew
 * @text SKEW: Change Skew
 * @desc Changes the camera skew.
 * Requires VisuMZ_3_ActSeqCamera!
 * 
 * @arg SkewX:eval
 * @text Skew X
 * @desc Change the camera skew X to this value.
 * @default 0
 * 
 * @arg SkewY:eval
 * @text Skew Y
 * @desc Change the camera skew Y to this value.
 * @default 0
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc Duration in frames to change camera skew.
 * @default 60
 *
 * @arg EasingType:str
 * @text Skew Easing
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * Requires VisuMZ_0_CoreEngine.
 * @default InOutSine
 * 
 * @arg WaitForSkew:eval
 * @text Wait For Skew?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for skew changes to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Skew_Reset
 * @text SKEW: Reset Skew
 * @desc Reset any skew settings.
 * Requires VisuMZ_3_ActSeqCamera!
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc Duration in frames to reset camera skew.
 * @default 60
 *
 * @arg EasingType:str
 * @text Skew Easing
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * Requires VisuMZ_0_CoreEngine.
 * @default InOutSine
 * 
 * @arg WaitForSkew:eval
 * @text Wait For Skew?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for skew changes to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Skew_WaitForSkew
 * @text SKEW: Wait For Skew
 * @desc Waits for skew changes to complete before performing next command.
 * Requires VisuMZ_3_ActSeqCamera!
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceSpaceTarget
 * @text -
 * @desc -
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceBreakTarget
 * @text Action Sequences - Target
 * @desc If using a manual target by target Action Sequence,
 * these commands will give you full control over its usage.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Target_CurrentIndex
 * @text TARGET: Current Index
 * @desc Sets the current index to this value.
 * Then decide to jump to a label (optional).
 * 
 * @arg Index:eval
 * @text Set Index To
 * @desc Sets current targeting index to this value.
 * 0 is the starting index of a target group.
 * @default 0
 * 
 * @arg JumpToLabel:str
 * @text Jump To Label
 * @desc If a target is found after the index change,
 * jump to this label in the Common Event.
 * @default Untitled
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Target_NextTarget
 * @text TARGET: Next Target
 * @desc Moves index forward by 1 to select a new current target.
 * Then decide to jump to a label (optional).
 * 
 * @arg JumpToLabel:str
 * @text Jump To Label
 * @desc If a target is found after the index change,
 * jump to this label in the Common Event.
 * @default Untitled
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Target_PrevTarget
 * @text TARGET: Previous Target
 * @desc Moves index backward by 1 to select a new current target.
 * Then decide to jump to a label (optional).
 * 
 * @arg JumpToLabel:str
 * @text Jump To Label
 * @desc If a target is found after the index change,
 * jump to this label in the Common Event.
 * @default Untitled
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Target_RandTarget
 * @text TARGET: Random Target
 * @desc Sets index randomly to determine new currernt target.
 * Then decide to jump to a label (optional).
 * 
 * @arg ForceRandom:eval
 * @text Force Random?
 * @type boolean
 * @on On
 * @off Off
 * @desc Index cannot be its previous index amount after random.
 * @default false
 * 
 * @arg JumpToLabel:str
 * @text Jump To Label
 * @desc If a target is found after the index change,
 * jump to this label in the Common Event.
 * @default Untitled
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceSpaceWeapon
 * @text -
 * @desc -
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceBreakWeapon
 * @text Action Sequences - Weapon
 * @desc Allows for finer control over Dual/Multi Wielding actors.
 * Only works for Actors.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Weapon_ClearActiveWeapon
 * @text WEAPON: Clear Weapon Slot
 * @desc Clears the active weapon slot (making others valid again).
 * Only works for Actors.
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @desc Select unit(s) to clear the active weapon slot for.
 * @default ["user"]
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Weapon_NextActiveWeapon
 * @text WEAPON: Next Weapon Slot
 * @desc Goes to next active weapon slot (making others invalid).
 * If next slot is weaponless, don't label jump.
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @desc Select unit(s) to change the next active weapon slot for.
 * @default ["user"]
 * 
 * @arg JumpToLabel:str
 * @text Jump To Label
 * @desc If a weapon is found after the index change,
 * jump to this label in the Common Event.
 * @default Untitled
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Weapon_SetActiveWeapon
 * @text WEAPON: Set Weapon Slot
 * @desc Sets the active weapon slot (making others invalid).
 * Only works for Actors.
 * 
 * @arg Targets:arraystr
 * @text Targets
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @desc Select unit(s) to change the active weapon slot for.
 * @default ["user"]
 * 
 * @arg SlotID:eval
 * @text Weapon Slot ID
 * @desc Select weapon slot to make active (making others invalid).
 * Use 0 to clear and normalize. You may use JavaScript code.
 * @default 1
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceSpaceZoom
 * @text -
 * @desc -
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceBreakZoom
 * @text Action Sequences - Zoom
 * @desc Allows you to have control over the screen zoom.
 * Requires VisuMZ_3_ActSeqCamera!
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Zoom_Scale
 * @text ZOOM: Change Scale
 * @desc Changes the zoom scale.
 * Requires VisuMZ_3_ActSeqCamera!
 * 
 * @arg Scale:eval
 * @text Scale
 * @desc The zoom scale to change to.
 * @default 1.0
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc Duration in frames to change battle zoom.
 * @default 60
 *
 * @arg EasingType:str
 * @text Zoom Easing
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * Requires VisuMZ_0_CoreEngine.
 * @default InOutSine
 * 
 * @arg WaitForZoom:eval
 * @text Wait For Zoom?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for zoom changes to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Zoom_Reset
 * @text ZOOM: Reset Zoom
 * @desc Reset any zoom settings.
 * Requires VisuMZ_3_ActSeqCamera!
 * 
 * @arg Duration:eval
 * @text Duration
 * @desc Duration in frames to reset battle zoom.
 * @default 60
 *
 * @arg EasingType:str
 * @text Zoom Easing
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * Requires VisuMZ_0_CoreEngine.
 * @default InOutSine
 * 
 * @arg WaitForZoom:eval
 * @text Wait For Zoom?
 * @type boolean
 * @on On
 * @off Off
 * @desc Wait for zoom changes to complete before performing next command?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActSeq_Zoom_WaitForZoom
 * @text ZOOM: Wait For Zoom
 * @desc Waits for zoom to complete before performing next command.
 * Requires VisuMZ_3_ActSeqCamera!
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ActionSequenceSpaceEnd
 * @text -
 * @desc -
 *
 * @ --------------------------------------------------------------------------
 *
 * @ ==========================================================================
 * @ Plugin Parameters
 * @ ==========================================================================
 *
 * @param BreakHead
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param BattleCore
 * @default Plugin Parameters
 *
 * @param ATTENTION
 * @default READ THE HELP FILE
 *
 * @param BreakSettings
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param AutoBattle:struct
 * @text Auto Battle Settings
 * @type struct<AutoBattle>
 * @desc Settings pertaining to Auto Battle.
 * @default {"BattleDisplay":"","AutoBattleMsg:str":"Press %1 or %2 to stop Auto Battle","AutoBattleOK:str":"OK","AutoBattleCancel:str":"Cancel","AutoBattleBgType:num":"1","AutoBattleRect:func":"\"const width = Graphics.width;\\nconst height = this.calcWindowHeight(1, false);\\nconst x = 0;\\nconst y = (Graphics.height - height) / 2;\\nreturn new Rectangle(x, y, width, height);\"","Options":"","AddOption:eval":"true","AdjustRect:eval":"true","StartName:str":"Auto Battle Start","StyleName:str":"Auto Battle Style","StyleOFF:str":"Attack","StyleON:str":"Skills"}
 *
 * @param Damage:struct
 * @text Damage Settings
 * @type struct<Damage>
 * @desc Settings pertaining to damage calculations.
 * @default {"Cap":"","EnableDamageCap:eval":"false","DefaultHardCap:num":"9999","EnableSoftCap:eval":"false","DefaultSoftCap:num":"0.80","DefaultSoftScaler:num":"0.1275","Popups":"","PopupDuration:num":"128","NewPopupBottom:eval":"true","PopupPosition:str":"base","PopupOffsetX:num":"0","PopupOffsetY:num":"0","PopupShiftX:num":"8","PopupShiftY:num":"-28","hpDamageFmt:str":"-%1","hpHealingFmt:str":"+%1","mpDamageFmt:str":"-%1 %2","mpHealingFmt:str":"+%1 %2","CriticalColor:eval":"[255, 0, 0, 160]","CriticalDuration:num":"128","Formulas":"","OverallFormulaJS:func":"\"// Declare Constants\\nconst target = arguments[0];\\nconst critical = arguments[1];\\nconst item = this.item();\\n\\n// Get Base Damage\\nconst baseValue = this.evalDamageFormula(target);\\n\\n// Calculate Element Modifiers\\nlet value = baseValue * this.calcElementRate(target);\\n\\n// Calculate Physical and Magical Modifiers\\nif (this.isPhysical()) {\\n    value *= target.pdr;\\n}\\nif (this.isMagical()) {\\n    value *= target.mdr;\\n}\\n\\n// Apply Healing Modifiers\\nif (baseValue < 0) {\\n    value *= target.rec;\\n}\\n\\n// Apply Critical Modifiers\\nif (critical) {\\n    value = this.applyCritical(value);\\n}\\n\\n// Apply Variance and Guard Modifiers\\nvalue = this.applyVariance(value, item.damage.variance);\\nvalue = this.applyGuard(value, target);\\n\\n// Finalize Damage\\nvalue = Math.round(value);\\nreturn value;\"","VarianceFormulaJS:func":"\"// Declare Constants\\nconst damage = arguments[0];\\nconst variance = arguments[1];\\n\\n// Calculate Variance\\nconst amp = Math.floor(Math.max((Math.abs(damage) * variance) / 100, 0));\\nconst v = Math.randomInt(amp + 1) + Math.randomInt(amp + 1) - amp;\\n\\n// Return Damage\\nreturn damage >= 0 ? damage + v : damage - v;\"","GuardFormulaJS:func":"\"// Declare Constants\\nconst damage = arguments[0];\\nconst target = arguments[1];\\n\\n// Return Damage Early\\nconst note = this.item().note;\\nif (note.match(/<UNBLOCKABLE>/i)) return damage;\\nif (!target.isGuard()) return damage;\\nif (damage < 0) return damage;\\n\\n// Declare Guard Rate\\nlet guardRate = 0.5;\\nguardRate /= target.grd;\\n\\n// Return Damage\\nreturn damage * guardRate;\"","Critical":"","CriticalHitRateJS:func":"\"// Declare Constants\\nconst user = this.subject();\\nconst target = arguments[0];\\n\\n// Create Base Critical Rate\\nlet rate = this.subject().cri * (1 - target.cev);\\n\\n// Apply Notetags\\nconst note = this.item().note;\\nif (note.match(/<ALWAYS CRITICAL>/i)) {\\n    return 1;\\n}\\nif (note.match(/<SET CRITICAL RATE:[ ](\\\\d+)([%％])>/i)) {\\n    return Number(RegExp.$1) / 100;\\n}\\nif (note.match(/<MODIFY CRITICAL RATE:[ ](\\\\d+)([%％])>/i)) {\\n    rate *= Number(RegExp.$1) / 100;\\n}\\nif (note.match(/<MODIFY CRITICAL RATE:[ ]([\\\\+\\\\-]\\\\d+)([%％])>/i)) {\\n    rate += Number(RegExp.$1) / 100;\\n}\\nif (note.match(/<JS CRITICAL RATE>\\\\s*([\\\\s\\\\S]*)\\\\s*<\\\\/JS CRITICAL RATE>/i)) {\\n    const code = String(RegExp.$1);\\n    try {\\n        eval(code);\\n    } catch (e) {\\n        if ($gameTemp.isPlaytest()) console.log(e);\\n    }\\n}\\n\\n// Apply LUK Buffs/Debuffs\\nconst lukStack = this.subject().buff(7);\\nrate *= 2 ** lukStack;\\n\\n// Return Rate\\nreturn rate;\"","CriticalHitMultiplier:func":"\"// Declare Constants\\nconst user = this.subject();\\nlet damage = arguments[0];\\nlet multiplier = 2.0;\\nlet bonusDamage = this.subject().luk * this.subject().cri;\\n\\n// Apply Notetags\\nconst note = this.item().note;\\nif (note.match(/<MODIFY CRITICAL MULTIPLIER:[ ](\\\\d+)([%％])>/i)) {\\n    multiplier = Number(RegExp.$1) / 100;\\n}\\nif (note.match(/<MODIFY CRITICAL MULTIPLIER:[ ]([\\\\+\\\\-]\\\\d+)([%％])>/i)) {\\n    multiplier += Number(RegExp.$1) / 100;\\n}\\nif (note.match(/<MODIFY CRITICAL BONUS DAMAGE:[ ](\\\\d+)([%％])>/i)) {\\n    bonusDamage *= Number(RegExp.$1) / 100;\\n}\\nif (note.match(/<MODIFY CRITICAL BONUS DAMAGE:[ ]([\\\\+\\\\-]\\\\d+)([%％])>/i)) {\\n    bonusDamage += bonusDamage * (RegExp.$1) / 100;\\n}\\nif (note.match(/<JS CRITICAL DAMAGE>\\\\s*([\\\\s\\\\S]*)\\\\s*<\\\\/JS CRITICAL DAMAGE>/i)) {\\n    const code = String(RegExp.$1);\\n    try {\\n        eval(code);\\n    } catch (e) {\\n        if ($gameTemp.isPlaytest()) console.log(e);\\n    }\\n}\\n\\n// Return Damage\\nreturn damage * multiplier + bonusDamage;\"","DamageStyles":"","DefaultDamageStyle:str":"Standard","DamageStyleList:arraystruct":"[\"{\\\"Name:str\\\":\\\"Standard\\\",\\\"Formula:func\\\":\\\"\\\\\\\"// Declare Constants\\\\\\\\nconst user = this.subject();\\\\\\\\nconst target = arguments[0];\\\\\\\\nconst item = this.item();\\\\\\\\nconst a = this.subject();\\\\\\\\nconst b = target;\\\\\\\\nconst v = $gameVariables._data;\\\\\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\\\\\n\\\\\\\\n// Replace Formula\\\\\\\\nlet formula = item.damage.formula;\\\\\\\\nif (SceneManager.isSceneBattle() && !this.isCertainHit()) {\\\\\\\\n    const fmt = 'Math.max(this.applyArmorModifiers(b, %1), 0)';\\\\\\\\n    formula = formula.replace(/b.def/g, fmt.format('b.def'));\\\\\\\\n    formula = formula.replace(/b.mdf/g, fmt.format('b.mdf'));\\\\\\\\n    formula = formula.replace(/b.agi/g, fmt.format('b.agi'));\\\\\\\\n    formula = formula.replace(/b.luk/g, fmt.format('b.luk'));\\\\\\\\n}\\\\\\\\n\\\\\\\\n// Calculate Damage\\\\\\\\nlet value = Math.max(eval(formula), 0);\\\\\\\\n\\\\\\\\n// Return Value\\\\\\\\nreturn (isNaN(value) ? 0 : value) * sign;\\\\\\\"\\\",\\\"ItemsEquipsCore\\\":\\\"\\\",\\\"DamageType\\\":\\\"\\\",\\\"DamageType1:str\\\":\\\"%1 Damage Multiplier\\\",\\\"DamageType2:str\\\":\\\"%1 Damage Multiplier\\\",\\\"DamageType3:str\\\":\\\"%1 Recovery Multiplier\\\",\\\"DamageType4:str\\\":\\\"%1 Recovery Multiplier\\\",\\\"DamageType5:str\\\":\\\"%1 Drain Multiplier\\\",\\\"DamageType6:str\\\":\\\"%1 Drain Multiplier\\\",\\\"DamageDisplay:func\\\":\\\"\\\\\\\"return this.getItemDamageAmountTextOriginal();\\\\\\\"\\\"}\",\"{\\\"Name:str\\\":\\\"Armor Scaling\\\",\\\"Formula:func\\\":\\\"\\\\\\\"// Declare Constants\\\\\\\\nconst user = this.subject();\\\\\\\\nconst target = arguments[0];\\\\\\\\nconst item = this.item();\\\\\\\\nconst a = this.subject();\\\\\\\\nconst b = target;\\\\\\\\nconst v = $gameVariables._data;\\\\\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\\\\\n\\\\\\\\n// Replace Formula\\\\\\\\nlet formula = item.damage.formula;\\\\\\\\nif (SceneManager.isSceneBattle() && !this.isCertainHit()) {\\\\\\\\n    const fmt = 'Math.max(this.applyArmorModifiers(b, %1), 1)';\\\\\\\\n    formula = formula.replace(/b.def/g, fmt.format('b.def'));\\\\\\\\n    formula = formula.replace(/b.mdf/g, fmt.format('b.mdf'));\\\\\\\\n    formula = formula.replace(/b.agi/g, fmt.format('b.agi'));\\\\\\\\n    formula = formula.replace(/b.luk/g, fmt.format('b.luk'));\\\\\\\\n}\\\\\\\\n\\\\\\\\n// Calculate Damage\\\\\\\\nlet value = Math.max(eval(formula), 0);\\\\\\\\n\\\\\\\\n// Apply Defender's Defense Parameter\\\\\\\\nif (this.isDamage() && !this.isCertainHit()) {\\\\\\\\n\\\\\\\\n    // Calculate Base Armor\\\\\\\\n    let armor = this.isPhysical() ? b.def : b.mdf;\\\\\\\\n    armor = this.applyArmorModifiers(target, armor);\\\\\\\\n\\\\\\\\n    // Apply Armor to Damage\\\\\\\\n    if (armor >= 0) {\\\\\\\\n        value *= 100 / (100 + armor);\\\\\\\\n    } else {\\\\\\\\n        value *= 2 - (100 / (100 - armor));\\\\\\\\n    }\\\\\\\\n}\\\\\\\\n\\\\\\\\n// Return Value\\\\\\\\nreturn (isNaN(value) ? 0 : value) * sign;\\\\\\\"\\\",\\\"ItemsEquipsCore\\\":\\\"\\\",\\\"DamageType\\\":\\\"\\\",\\\"DamageType1:str\\\":\\\"%1 Damage Multiplier\\\",\\\"DamageType2:str\\\":\\\"%1 Damage Multiplier\\\",\\\"DamageType3:str\\\":\\\"%1 Recovery Multiplier\\\",\\\"DamageType4:str\\\":\\\"%1 Recovery Multiplier\\\",\\\"DamageType5:str\\\":\\\"%1 Drain Multiplier\\\",\\\"DamageType6:str\\\":\\\"%1 Drain Multiplier\\\",\\\"DamageDisplay:func\\\":\\\"\\\\\\\"return this.getItemDamageAmountTextOriginal();\\\\\\\"\\\"}\",\"{\\\"Name:str\\\":\\\"CT\\\",\\\"Formula:func\\\":\\\"\\\\\\\"// Define Constants\\\\\\\\nconst user = this.subject();\\\\\\\\nconst target = arguments[0];\\\\\\\\nconst item = this.item();\\\\\\\\nconst a = this.subject();\\\\\\\\nconst b = target;\\\\\\\\nconst v = $gameVariables._data;\\\\\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\\\\\n\\\\\\\\n// Create Multiplier\\\\\\\\nconst multiplier = Math.max(eval(item.damage.formula), 0);\\\\\\\\n\\\\\\\\n// Declare Values\\\\\\\\nlet value = 0;\\\\\\\\nlet level = Math.max(a.level || a.luk, 1);\\\\\\\\nlet armor = this.isPhysical() ? b.def : b.mdf;\\\\\\\\narmor = Math.max(this.applyArmorModifiers(target, armor), 0);\\\\\\\\nlet attackStat = 0;\\\\\\\\nif (this.isPhysical() && (this.isDamage() || this.isDrain())) {\\\\\\\\n    attackStat = a.atk;\\\\\\\\n} else if (this.isMagical() && (this.isDamage() || this.isDrain())) {\\\\\\\\n    attackStat =  a.mat;\\\\\\\\n} else if (this.isPhysical() && this.isRecover()) {\\\\\\\\n    attackStat =  a.def;\\\\\\\\n} else if (this.isMagical() && this.isRecover()) {\\\\\\\\n    attackStat =  a.mdf;\\\\\\\\n}\\\\\\\\n\\\\\\\\n// Calculate Damage\\\\\\\\nattackStat = (attackStat * 1.75) + (level ** 2 / 45.5);\\\\\\\\nvalue = attackStat * 4;\\\\\\\\nif (this.isPhysical() && (this.isDamage() || this.isDrain())) {\\\\\\\\n    value *= Math.max(256 - armor, 0) / 256;\\\\\\\\n} else if (this.isMagical() && (this.isDamage() || this.isDrain())) {\\\\\\\\n    value *= Math.max(102.4 - armor, 0) / 128;\\\\\\\\n}\\\\\\\\nvalue *= multiplier;\\\\\\\\n\\\\\\\\n// Return Value\\\\\\\\nreturn (isNaN(value) ? 0 : value) * sign;\\\\\\\"\\\",\\\"ItemsEquipsCore\\\":\\\"\\\",\\\"DamageType\\\":\\\"\\\",\\\"DamageType1:str\\\":\\\"%1 Damage Multiplier\\\",\\\"DamageType2:str\\\":\\\"%1 Damage Multiplier\\\",\\\"DamageType3:str\\\":\\\"%1 Recovery Multiplier\\\",\\\"DamageType4:str\\\":\\\"%1 Recovery Multiplier\\\",\\\"DamageType5:str\\\":\\\"%1 Drain Multiplier\\\",\\\"DamageType6:str\\\":\\\"%1 Drain Multiplier\\\",\\\"DamageDisplay:func\\\":\\\"\\\\\\\"// Define Constants\\\\\\\\nconst item = this._item;\\\\\\\\nconst formula = item.damage.formula;\\\\\\\\nconst a = this._tempActorA;\\\\\\\\nconst b = this._tempActorB;\\\\\\\\nconst user = a;\\\\\\\\nconst target = b;\\\\\\\\n\\\\\\\\n// Return Value\\\\\\\\ntry {\\\\\\\\n    const value = Math.max(eval(formula), 0);\\\\\\\\n    return '%1%'.format(Math.round(value * 100));\\\\\\\\n} catch (e) {\\\\\\\\n    if ($gameTemp.isPlaytest()) {\\\\\\\\n        console.log('Damage Formula Error for %1'.format(this._item.name));\\\\\\\\n    }\\\\\\\\n    return '?????';\\\\\\\\n}\\\\\\\"\\\"}\",\"{\\\"Name:str\\\":\\\"D4\\\",\\\"Formula:func\\\":\\\"\\\\\\\"// Define Constants\\\\\\\\nconst user = this.subject();\\\\\\\\nconst target = arguments[0];\\\\\\\\nconst item = this.item();\\\\\\\\nconst a = this.subject();\\\\\\\\nconst b = target;\\\\\\\\nconst v = $gameVariables._data;\\\\\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\\\\\n\\\\\\\\n// Create Multiplier\\\\\\\\nconst multiplier = Math.max(eval(item.damage.formula), 0);\\\\\\\\n\\\\\\\\n// Declare Values\\\\\\\\nlet armor = this.isPhysical() ? b.def : b.mdf;\\\\\\\\narmor = this.applyArmorModifiers(target, armor);\\\\\\\\nlet stat = 0;\\\\\\\\nif (this.isPhysical() && (this.isDamage() || this.isDrain())) {\\\\\\\\n    stat = a.atk;\\\\\\\\n} else if (this.isMagical() && (this.isDamage() || this.isDrain())) {\\\\\\\\n    stat = a.mat;\\\\\\\\n} else if (this.isPhysical() && this.isRecover()) {\\\\\\\\n    stat = a.def;\\\\\\\\n    armor = 0;\\\\\\\\n} else if (this.isMagical() && this.isRecover()) {\\\\\\\\n    stat = a.mdf;\\\\\\\\n    armor = 0;\\\\\\\\n}\\\\\\\\n\\\\\\\\n// Calculate Damage \\\\\\\\nlet value = 1.5 * Math.max(2 * stat * multiplier - armor, 1) * multiplier / 5;\\\\\\\\n\\\\\\\\n// Return Value\\\\\\\\nreturn (isNaN(value) ? 0 : value) * sign;\\\\\\\"\\\",\\\"ItemsEquipsCore\\\":\\\"\\\",\\\"DamageType\\\":\\\"\\\",\\\"DamageType1:str\\\":\\\"%1 Damage Multiplier\\\",\\\"DamageType2:str\\\":\\\"%1 Damage Multiplier\\\",\\\"DamageType3:str\\\":\\\"%1 Recovery Multiplier\\\",\\\"DamageType4:str\\\":\\\"%1 Recovery Multiplier\\\",\\\"DamageType5:str\\\":\\\"%1 Drain Multiplier\\\",\\\"DamageType6:str\\\":\\\"%1 Drain Multiplier\\\",\\\"DamageDisplay:func\\\":\\\"\\\\\\\"// Define Constants\\\\\\\\nconst item = this._item;\\\\\\\\nconst formula = item.damage.formula;\\\\\\\\nconst a = this._tempActorA;\\\\\\\\nconst b = this._tempActorB;\\\\\\\\nconst user = a;\\\\\\\\nconst target = b;\\\\\\\\n\\\\\\\\n// Return Value\\\\\\\\ntry {\\\\\\\\n    const value = Math.max(eval(formula), 0);\\\\\\\\n    return '%1%'.format(Math.round(value * 100));\\\\\\\\n} catch (e) {\\\\\\\\n    if ($gameTemp.isPlaytest()) {\\\\\\\\n        console.log('Damage Formula Error for %1'.format(this._item.name));\\\\\\\\n    }\\\\\\\\n    return '?????';\\\\\\\\n}\\\\\\\"\\\"}\",\"{\\\"Name:str\\\":\\\"DQ\\\",\\\"Formula:func\\\":\\\"\\\\\\\"// Define Constants\\\\\\\\nconst user = this.subject();\\\\\\\\nconst target = arguments[0];\\\\\\\\nconst item = this.item();\\\\\\\\nconst a = this.subject();\\\\\\\\nconst b = target;\\\\\\\\nconst v = $gameVariables._data;\\\\\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\\\\\n\\\\\\\\n// Create Multiplier\\\\\\\\nlet multiplier = Math.max(eval(item.damage.formula), 0);\\\\\\\\nif (this.isCertainHit()) {\\\\\\\\n    let value = multiplier * Math.max(a.atk, a.mat);\\\\\\\\n    return (isNaN(value) ? 0 : value) * sign;\\\\\\\\n}\\\\\\\\n\\\\\\\\n// Get Primary Stats\\\\\\\\nlet armor = this.isPhysical() ? b.def : b.mdf;\\\\\\\\narmor = this.applyArmorModifiers(b, armor);\\\\\\\\nlet stat = 1;\\\\\\\\nif (this.isPhysical() && (this.isDamage() || this.isDrain())) {\\\\\\\\n    stat = a.atk;\\\\\\\\n} else if (this.isMagical() && (this.isDamage() || this.isDrain())) {\\\\\\\\n    stat = a.mat;\\\\\\\\n} else if (this.isPhysical() && this.isRecover()) {\\\\\\\\n    stat = a.def;\\\\\\\\n} else if (this.isMagical() && this.isRecover()) {\\\\\\\\n    stat = a.mdf;\\\\\\\\n}\\\\\\\\n\\\\\\\\n// Check for Recovery\\\\\\\\nif (this.isRecover()) {\\\\\\\\n    let value = stat * multiplier * sign;\\\\\\\\n    return isNaN(value) ? 0 : value;\\\\\\\\n}\\\\\\\\n\\\\\\\\n// Calculate Damage\\\\\\\\nlet value = 0;\\\\\\\\nif (stat < ((2 + armor) / 2)) {\\\\\\\\n    // Plink Damage\\\\\\\\n    let baseline = Math.max(stat - ((12 * (armor - stat + 1)) / stat), 5);\\\\\\\\n    value = baseline / 3;\\\\\\\\n} else {\\\\\\\\n    // Normal Damage\\\\\\\\n    let baseline = Math.max(stat - (armor / 2), 1);\\\\\\\\n    value = baseline / 2;\\\\\\\\n}\\\\\\\\nvalue *= multiplier;\\\\\\\\n\\\\\\\\n// Return Value\\\\\\\\nreturn isNaN(value) ? 0 : value;\\\\\\\"\\\",\\\"ItemsEquipsCore\\\":\\\"\\\",\\\"DamageType\\\":\\\"\\\",\\\"DamageType1:str\\\":\\\"%1 Damage Multiplier\\\",\\\"DamageType2:str\\\":\\\"%1 Damage Multiplier\\\",\\\"DamageType3:str\\\":\\\"%1 Recovery Multiplier\\\",\\\"DamageType4:str\\\":\\\"%1 Recovery Multiplier\\\",\\\"DamageType5:str\\\":\\\"%1 Drain Multiplier\\\",\\\"DamageType6:str\\\":\\\"%1 Drain Multiplier\\\",\\\"DamageDisplay:func\\\":\\\"\\\\\\\"// Define Constants\\\\\\\\nconst item = this._item;\\\\\\\\nconst formula = item.damage.formula;\\\\\\\\nconst a = this._tempActorA;\\\\\\\\nconst b = this._tempActorB;\\\\\\\\nconst user = a;\\\\\\\\nconst target = b;\\\\\\\\n\\\\\\\\n// Return Value\\\\\\\\ntry {\\\\\\\\n    const value = Math.max(eval(formula), 0);\\\\\\\\n    return '%1%'.format(Math.round(value * 100));\\\\\\\\n} catch (e) {\\\\\\\\n    if ($gameTemp.isPlaytest()) {\\\\\\\\n        console.log('Damage Formula Error for %1'.format(this._item.name));\\\\\\\\n    }\\\\\\\\n    return '?????';\\\\\\\\n}\\\\\\\"\\\"}\",\"{\\\"Name:str\\\":\\\"FF7\\\",\\\"Formula:func\\\":\\\"\\\\\\\"// Define Constants\\\\\\\\nconst user = this.subject();\\\\\\\\nconst target = arguments[0];\\\\\\\\nconst item = this.item();\\\\\\\\nconst a = this.subject();\\\\\\\\nconst b = target;\\\\\\\\nconst v = $gameVariables._data;\\\\\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\\\\\n\\\\\\\\n// Create Power\\\\\\\\nconst power = Math.max(eval(item.damage.formula), 0);\\\\\\\\n\\\\\\\\n// Declare base Damage\\\\\\\\nlet baseDamage = 0;\\\\\\\\nlet level = Math.max(a.level || a.luk, 1);\\\\\\\\nif (this.isPhysical() && (this.isDamage() || this.isDrain())) {\\\\\\\\n    baseDamage = a.atk + ((a.atk + level) / 32) * ((a.atk * level) / 32);\\\\\\\\n} else if (this.isMagical() && (this.isDamage() || this.isDrain())) {\\\\\\\\n    baseDamage = 6 * (a.mat + level);\\\\\\\\n} else if (this.isPhysical() && this.isRecover()) {\\\\\\\\n    baseDamage = 6 * (a.def + level);\\\\\\\\n} else if (this.isMagical() && this.isRecover()) {\\\\\\\\n    baseDamage = 6 * (a.mdf + level);\\\\\\\\n}\\\\\\\\n\\\\\\\\n// Calculate Final Damage\\\\\\\\nlet value = baseDamage;\\\\\\\\nlet armor = this.isPhysical() ? b.def : b.mdf;\\\\\\\\narmor = this.applyArmorModifiers(target, armor);\\\\\\\\nif (this.isRecover()) {\\\\\\\\n    value += 22 * power;\\\\\\\\n} else {\\\\\\\\n    value = (power * Math.max(512 - armor, 1) * baseDamage) / (16 * 512);\\\\\\\\n}\\\\\\\\n\\\\\\\\n// Return Value\\\\\\\\nreturn (isNaN(value) ? 0 : value) * sign;\\\\\\\"\\\",\\\"ItemsEquipsCore\\\":\\\"\\\",\\\"DamageType\\\":\\\"\\\",\\\"DamageType1:str\\\":\\\"%1 Damage Power\\\",\\\"DamageType2:str\\\":\\\"%1 Damage Power\\\",\\\"DamageType3:str\\\":\\\"%1 Recovery Power\\\",\\\"DamageType4:str\\\":\\\"%1 Recovery Power\\\",\\\"DamageType5:str\\\":\\\"%1 Drain Power\\\",\\\"DamageType6:str\\\":\\\"%1 Drain Power\\\",\\\"DamageDisplay:func\\\":\\\"\\\\\\\"// Define Constants\\\\\\\\nconst item = this._item;\\\\\\\\nconst formula = item.damage.formula;\\\\\\\\nconst a = this._tempActorA;\\\\\\\\nconst b = this._tempActorB;\\\\\\\\nconst user = a;\\\\\\\\nconst target = b;\\\\\\\\n\\\\\\\\n// Return Value\\\\\\\\ntry {\\\\\\\\n    return formula;\\\\\\\\n} catch (e) {\\\\\\\\n    if ($gameTemp.isPlaytest()) {\\\\\\\\n        console.log('Damage Formula Error for %1'.format(this._item.name));\\\\\\\\n    }\\\\\\\\n    return '?????';\\\\\\\\n}\\\\\\\"\\\"}\",\"{\\\"Name:str\\\":\\\"FF8\\\",\\\"Formula:func\\\":\\\"\\\\\\\"// Define Constants\\\\\\\\nconst user = this.subject();\\\\\\\\nconst target = arguments[0];\\\\\\\\nconst item = this.item();\\\\\\\\nconst a = this.subject();\\\\\\\\nconst b = target;\\\\\\\\nconst v = $gameVariables._data;\\\\\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\\\\\n\\\\\\\\n// Create Power\\\\\\\\nconst power = Math.max(eval(item.damage.formula), 0);\\\\\\\\n\\\\\\\\n// Declare Damage\\\\\\\\nlet Value = 0;\\\\\\\\nlet level = Math.max(a.level || a.luk, 1);\\\\\\\\nlet armor = this.isPhysical() ? b.def : b.mdf;\\\\\\\\narmor = this.applyArmorModifiers(target, armor);\\\\\\\\nif (this.isPhysical() && (this.isDamage() || this.isDrain())) {\\\\\\\\n    value = a.atk ** 2 / 16 + a.atk;\\\\\\\\n    value *= Math.max(265 - armor, 1) / 256;\\\\\\\\n    value *= power / 16;\\\\\\\\n} else if (this.isMagical() && (this.isDamage() || this.isDrain())) {\\\\\\\\n    value = a.mat + power;\\\\\\\\n    value *= Math.max(265 - armor, 1) / 4;\\\\\\\\n    value *= power / 256;\\\\\\\\n} else if (this.isPhysical() && this.isRecover()) {\\\\\\\\n    value = (power + a.def) * power / 2;\\\\\\\\n} else if (this.isMagical() && this.isRecover()) {\\\\\\\\n    value = (power + a.mdf) * power / 2;\\\\\\\\n}\\\\\\\\n\\\\\\\\n// Return Value\\\\\\\\nreturn (isNaN(value) ? 0 : value) * sign;\\\\\\\"\\\",\\\"ItemsEquipsCore\\\":\\\"\\\",\\\"DamageType\\\":\\\"\\\",\\\"DamageType1:str\\\":\\\"%1 Damage Power\\\",\\\"DamageType2:str\\\":\\\"%1 Damage Power\\\",\\\"DamageType3:str\\\":\\\"%1 Recovery Power\\\",\\\"DamageType4:str\\\":\\\"%1 Recovery Power\\\",\\\"DamageType5:str\\\":\\\"%1 Drain Power\\\",\\\"DamageType6:str\\\":\\\"%1 Drain Power\\\",\\\"DamageDisplay:func\\\":\\\"\\\\\\\"// Define Constants\\\\\\\\nconst item = this._item;\\\\\\\\nconst formula = item.damage.formula;\\\\\\\\nconst a = this._tempActorA;\\\\\\\\nconst b = this._tempActorB;\\\\\\\\nconst user = a;\\\\\\\\nconst target = b;\\\\\\\\n\\\\\\\\n// Return Value\\\\\\\\ntry {\\\\\\\\n    return formula;\\\\\\\\n} catch (e) {\\\\\\\\n    if ($gameTemp.isPlaytest()) {\\\\\\\\n        console.log('Damage Formula Error for %1'.format(this._item.name));\\\\\\\\n    }\\\\\\\\n    return '?????';\\\\\\\\n}\\\\\\\"\\\"}\",\"{\\\"Name:str\\\":\\\"FF9\\\",\\\"Formula:func\\\":\\\"\\\\\\\"// Define Constants\\\\\\\\nconst user = this.subject();\\\\\\\\nconst target = arguments[0];\\\\\\\\nconst item = this.item();\\\\\\\\nconst a = this.subject();\\\\\\\\nconst b = target;\\\\\\\\nconst v = $gameVariables._data;\\\\\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\\\\\n\\\\\\\\n// Create Damage Constant\\\\\\\\nconst power = Math.max(eval(item.damage.formula), 0);\\\\\\\\nif (this.isCertainHit()) {\\\\\\\\n    return (isNaN(power) ? 0 : power) * sign;\\\\\\\\n}\\\\\\\\n\\\\\\\\n// Declare Main Stats\\\\\\\\nlet armor = this.isPhysical() ? b.def : b.mdf;\\\\\\\\narmor = this.applyArmorModifiers(b, armor);\\\\\\\\nlet stat = 1;\\\\\\\\nif (this.isPhysical() && (this.isDamage() || this.isDrain())) {\\\\\\\\n    stat = a.atk;\\\\\\\\n} else if (this.isMagical() && (this.isDamage() || this.isDrain())) {\\\\\\\\n    stat = a.mat;\\\\\\\\n} else if (this.isPhysical() && this.isRecover()) {\\\\\\\\n    stat = a.def;\\\\\\\\n} else if (this.isMagical() && this.isRecover()) {\\\\\\\\n    stat = a.mdf;\\\\\\\\n}\\\\\\\\n\\\\\\\\n// Declare Base Damage\\\\\\\\nlet baseDamage = power;\\\\\\\\nif (this.isPhysical()) {\\\\\\\\n    baseDamage += stat;\\\\\\\\n}\\\\\\\\nif (this.isDamage() || this.isDrain()) {\\\\\\\\n    baseDamage -= armor;\\\\\\\\n    baseDamage = Math.max(1, baseDamage);\\\\\\\\n}\\\\\\\\n\\\\\\\\n// Declare Bonus Damage\\\\\\\\nlet bonusDamage = stat + (((a.level || a.luk) + stat) / 8);\\\\\\\\n\\\\\\\\n// Declare Final Damage\\\\\\\\nlet value = baseDamage * bonusDamage * sign;\\\\\\\\n\\\\\\\\n// Return Value\\\\\\\\nreturn isNaN(value) ? 0 : value;\\\\\\\"\\\",\\\"ItemsEquipsCore\\\":\\\"\\\",\\\"DamageType\\\":\\\"\\\",\\\"DamageType1:str\\\":\\\"%1 Damage Power\\\",\\\"DamageType2:str\\\":\\\"%1 Damage Power\\\",\\\"DamageType3:str\\\":\\\"%1 Recovery Power\\\",\\\"DamageType4:str\\\":\\\"%1 Recovery Power\\\",\\\"DamageType5:str\\\":\\\"%1 Drain Power\\\",\\\"DamageType6:str\\\":\\\"%1 Drain Power\\\",\\\"DamageDisplay:func\\\":\\\"\\\\\\\"// Define Constants\\\\\\\\nconst item = this._item;\\\\\\\\nconst formula = item.damage.formula;\\\\\\\\nconst a = this._tempActorA;\\\\\\\\nconst b = this._tempActorB;\\\\\\\\nconst user = a;\\\\\\\\nconst target = b;\\\\\\\\n\\\\\\\\n// Return Value\\\\\\\\ntry {\\\\\\\\n    return formula;\\\\\\\\n} catch (e) {\\\\\\\\n    if ($gameTemp.isPlaytest()) {\\\\\\\\n        console.log('Damage Formula Error for %1'.format(this._item.name));\\\\\\\\n    }\\\\\\\\n    return '?????';\\\\\\\\n}\\\\\\\"\\\"}\",\"{\\\"Name:str\\\":\\\"FF10\\\",\\\"Formula:func\\\":\\\"\\\\\\\"// Define Constants\\\\\\\\nconst user = this.subject();\\\\\\\\nconst target = arguments[0];\\\\\\\\nconst item = this.item();\\\\\\\\nconst a = this.subject();\\\\\\\\nconst b = target;\\\\\\\\nconst v = $gameVariables._data;\\\\\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\\\\\n\\\\\\\\n// Create Damage Constant\\\\\\\\nconst power = Math.max(eval(item.damage.formula), 0);\\\\\\\\nif (this.isCertainHit()) {\\\\\\\\n    return (isNaN(power) ? 0 : power) * sign;\\\\\\\\n}\\\\\\\\n\\\\\\\\n// Create Damage Offense Value\\\\\\\\nlet value = power;\\\\\\\\n\\\\\\\\nif (this.isPhysical() && (this.isDamage() || this.isDrain())) {\\\\\\\\n    value = (((a.atk ** 3) / 32) + 32) * power / 16;\\\\\\\\n} else if (this.isMagical() && (this.isDamage() || this.isDrain())) {\\\\\\\\n    value = power * ((a.mat ** 2 / 6) + power) / 4;\\\\\\\\n} else if (this.isPhysical() && this.isRecover()) {\\\\\\\\n    value = power * ((a.def + power) / 2);\\\\\\\\n} else if (this.isMagical() && this.isRecover()) {\\\\\\\\n    value = power * ((a.mdf + power) / 2);\\\\\\\\n}\\\\\\\\n\\\\\\\\n// Apply Damage Defense Value\\\\\\\\nif (this.isDamage() || this.isDrain()) {\\\\\\\\n    let armor = this.isPhysical() ? b.def : b.mdf;\\\\\\\\n    armor = this.applyArmorModifiers(b, armor);\\\\\\\\n    armor = Math.max(armor, 1);\\\\\\\\n    value *= ((((armor - 280.4) ** 2) / 110) / 16) / 730;\\\\\\\\n    value *= (730 - (armor * 51 - (armor ** 2) / 11) / 10) / 730;\\\\\\\\n} else if (this.isRecover()) {\\\\\\\\n    value *= -1;\\\\\\\\n}\\\\\\\\n\\\\\\\\n// Return Value\\\\\\\\nreturn isNaN(value) ? 0 : value;\\\\\\\"\\\",\\\"ItemsEquipsCore\\\":\\\"\\\",\\\"DamageType\\\":\\\"\\\",\\\"DamageType1:str\\\":\\\"%1 Damage Power\\\",\\\"DamageType2:str\\\":\\\"%1 Damage Power\\\",\\\"DamageType3:str\\\":\\\"%1 Recovery Power\\\",\\\"DamageType4:str\\\":\\\"%1 Recovery Power\\\",\\\"DamageType5:str\\\":\\\"%1 Drain Power\\\",\\\"DamageType6:str\\\":\\\"%1 Drain Power\\\",\\\"DamageDisplay:func\\\":\\\"\\\\\\\"// Define Constants\\\\\\\\nconst item = this._item;\\\\\\\\nconst formula = item.damage.formula;\\\\\\\\nconst a = this._tempActorA;\\\\\\\\nconst b = this._tempActorB;\\\\\\\\nconst user = a;\\\\\\\\nconst target = b;\\\\\\\\n\\\\\\\\n// Return Value\\\\\\\\ntry {\\\\\\\\n    return formula;\\\\\\\\n} catch (e) {\\\\\\\\n    if ($gameTemp.isPlaytest()) {\\\\\\\\n        console.log('Damage Formula Error for %1'.format(this._item.name));\\\\\\\\n    }\\\\\\\\n    return '?????';\\\\\\\\n}\\\\\\\"\\\"}\",\"{\\\"Name:str\\\":\\\"MK\\\",\\\"Formula:func\\\":\\\"\\\\\\\"// Define Constants\\\\\\\\nconst user = this.subject();\\\\\\\\nconst target = arguments[0];\\\\\\\\nconst item = this.item();\\\\\\\\nconst a = this.subject();\\\\\\\\nconst b = target;\\\\\\\\nconst v = $gameVariables._data;\\\\\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\\\\\n\\\\\\\\n// Create Multiplier\\\\\\\\nconst multiplier = Math.max(eval(item.damage.formula), 0);\\\\\\\\n\\\\\\\\n// Declare Values\\\\\\\\nlet armor = this.isPhysical() ? b.def : b.mdf;\\\\\\\\narmor = this.applyArmorModifiers(target, armor);\\\\\\\\nconst denominator = Math.max(200 + armor, 1);\\\\\\\\n\\\\\\\\n// Calculate Damage \\\\\\\\nlet value = 0;\\\\\\\\nif (this.isPhysical() && (this.isDamage() || this.isDrain())) {\\\\\\\\n    value = 200 * a.atk / denominator;\\\\\\\\n} else if (this.isMagical() && (this.isDamage() || this.isDrain())) {\\\\\\\\n    value = 200 * a.mat / denominator;\\\\\\\\n} else if (this.isPhysical() && this.isRecover()) {\\\\\\\\n    value = 200 * a.def / 200;\\\\\\\\n} else if (this.isMagical() && this.isRecover()) {\\\\\\\\n    value = 200 * a.mdf / 200;\\\\\\\\n}\\\\\\\\nvalue *= multiplier;\\\\\\\\n\\\\\\\\n// Return Value\\\\\\\\nreturn (isNaN(value) ? 0 : value) * sign;\\\\\\\"\\\",\\\"ItemsEquipsCore\\\":\\\"\\\",\\\"DamageType\\\":\\\"\\\",\\\"DamageType1:str\\\":\\\"%1 Damage Multiplier\\\",\\\"DamageType2:str\\\":\\\"%1 Damage Multiplier\\\",\\\"DamageType3:str\\\":\\\"%1 Recovery Multiplier\\\",\\\"DamageType4:str\\\":\\\"%1 Recovery Multiplier\\\",\\\"DamageType5:str\\\":\\\"%1 Drain Multiplier\\\",\\\"DamageType6:str\\\":\\\"%1 Drain Multiplier\\\",\\\"DamageDisplay:func\\\":\\\"\\\\\\\"// Define Constants\\\\\\\\nconst item = this._item;\\\\\\\\nconst formula = item.damage.formula;\\\\\\\\nconst a = this._tempActorA;\\\\\\\\nconst b = this._tempActorB;\\\\\\\\nconst user = a;\\\\\\\\nconst target = b;\\\\\\\\n\\\\\\\\n// Return Value\\\\\\\\ntry {\\\\\\\\n    const value = Math.max(eval(formula), 0);\\\\\\\\n    return '%1%'.format(Math.round(value * 100));\\\\\\\\n} catch (e) {\\\\\\\\n    if ($gameTemp.isPlaytest()) {\\\\\\\\n        console.log('Damage Formula Error for %1'.format(this._item.name));\\\\\\\\n    }\\\\\\\\n    return '?????';\\\\\\\\n}\\\\\\\"\\\"}\",\"{\\\"Name:str\\\":\\\"MOBA\\\",\\\"Formula:func\\\":\\\"\\\\\\\"// Define Constants\\\\\\\\nconst user = this.subject();\\\\\\\\nconst target = arguments[0];\\\\\\\\nconst item = this.item();\\\\\\\\nconst a = this.subject();\\\\\\\\nconst b = target;\\\\\\\\nconst v = $gameVariables._data;\\\\\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\\\\\n\\\\\\\\n// Create Damage Value\\\\\\\\nlet value = Math.max(eval(item.damage.formula), 0) * sign;\\\\\\\\n\\\\\\\\n// Apply Attacker's Offense Parameter\\\\\\\\nif (this.isPhysical() && (this.isDamage() || this.isDrain())) {\\\\\\\\n    value *= a.atk;\\\\\\\\n} else if (this.isMagical() && (this.isDamage() || this.isDrain())) {\\\\\\\\n    value *= a.mat;\\\\\\\\n} else if (this.isPhysical() && this.isRecover()) {\\\\\\\\n    value *= a.def;\\\\\\\\n} else if (this.isMagical() && this.isRecover()) {\\\\\\\\n    value *= a.mdf;\\\\\\\\n}\\\\\\\\n\\\\\\\\n// Apply Defender's Defense Parameter\\\\\\\\nif (this.isDamage() && !this.isCertainHit()) {\\\\\\\\n\\\\\\\\n    // Calculate Base Armor\\\\\\\\n    let armor = this.isPhysical() ? b.def : b.mdf;\\\\\\\\n    armor = this.applyArmorModifiers(target, armor);\\\\\\\\n\\\\\\\\n    // Apply Armor to Damage\\\\\\\\n    if (armor >= 0) {\\\\\\\\n        value *= 100 / (100 + armor);\\\\\\\\n    } else {\\\\\\\\n        value *= 2 - (100 / (100 - armor));\\\\\\\\n    }\\\\\\\\n}\\\\\\\\n\\\\\\\\n// Return Value\\\\\\\\nreturn isNaN(value) ? 0 : value;\\\\\\\"\\\",\\\"ItemsEquipsCore\\\":\\\"\\\",\\\"DamageType\\\":\\\"\\\",\\\"DamageType1:str\\\":\\\"%1 Damage Multiplier\\\",\\\"DamageType2:str\\\":\\\"%1 Damage Multiplier\\\",\\\"DamageType3:str\\\":\\\"%1 Recovery Multiplier\\\",\\\"DamageType4:str\\\":\\\"%1 Recovery Multiplier\\\",\\\"DamageType5:str\\\":\\\"%1 Drain Multiplier\\\",\\\"DamageType6:str\\\":\\\"%1 Drain Multiplier\\\",\\\"DamageDisplay:func\\\":\\\"\\\\\\\"// Define Constants\\\\\\\\nconst item = this._item;\\\\\\\\nconst formula = item.damage.formula;\\\\\\\\nconst a = this._tempActorA;\\\\\\\\nconst b = this._tempActorB;\\\\\\\\nconst user = a;\\\\\\\\nconst target = b;\\\\\\\\n\\\\\\\\n// Return Value\\\\\\\\ntry {\\\\\\\\n    const value = Math.max(eval(formula), 0);\\\\\\\\n    return '%1%'.format(Math.round(value * 100));\\\\\\\\n} catch (e) {\\\\\\\\n    if ($gameTemp.isPlaytest()) {\\\\\\\\n        console.log('Damage Formula Error for %1'.format(this._item.name));\\\\\\\\n    }\\\\\\\\n    return '?????';\\\\\\\\n}\\\\\\\"\\\"}\",\"{\\\"Name:str\\\":\\\"PKMN\\\",\\\"Formula:func\\\":\\\"\\\\\\\"// Define Constants\\\\\\\\nconst user = this.subject();\\\\\\\\nconst target = arguments[0];\\\\\\\\nconst item = this.item();\\\\\\\\nconst a = this.subject();\\\\\\\\nconst b = target;\\\\\\\\nconst v = $gameVariables._data;\\\\\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\\\\\n\\\\\\\\n// Create Power\\\\\\\\nconst power = Math.max(eval(item.damage.formula), 0);\\\\\\\\n\\\\\\\\n// Declare Values\\\\\\\\nlet value = 0;\\\\\\\\nlet level = Math.max(a.level || a.luk, 1);\\\\\\\\nlet armor = this.isPhysical() ? b.def : b.mdf;\\\\\\\\narmor = Math.max(this.applyArmorModifiers(target, armor), 0);\\\\\\\\nlet attackStat = 0;\\\\\\\\nif (this.isPhysical() && (this.isDamage() || this.isDrain())) {\\\\\\\\n    attackStat = a.atk;\\\\\\\\n} else if (this.isMagical() && (this.isDamage() || this.isDrain())) {\\\\\\\\n    attackStat =  a.mat;\\\\\\\\n} else if (this.isPhysical() && this.isRecover()) {\\\\\\\\n    attackStat =  a.def;\\\\\\\\n} else if (this.isMagical() && this.isRecover()) {\\\\\\\\n    attackStat =  a.mdf;\\\\\\\\n}\\\\\\\\n\\\\\\\\n// Calculate Damage\\\\\\\\nvalue = (((((2 * level) / 5) + 2) * power * (attackStat / armor)) / 50) + 2;\\\\\\\\n\\\\\\\\n// Return Value\\\\\\\\nreturn (isNaN(value) ? 0 : value) * sign;\\\\\\\"\\\",\\\"ItemsEquipsCore\\\":\\\"\\\",\\\"DamageType\\\":\\\"\\\",\\\"DamageType1:str\\\":\\\"%1 Damage Power\\\",\\\"DamageType2:str\\\":\\\"%1 Damage Power\\\",\\\"DamageType3:str\\\":\\\"%1 Recovery Power\\\",\\\"DamageType4:str\\\":\\\"%1 Recovery Power\\\",\\\"DamageType5:str\\\":\\\"%1 Drain Power\\\",\\\"DamageType6:str\\\":\\\"%1 Drain Power\\\",\\\"DamageDisplay:func\\\":\\\"\\\\\\\"// Define Constants\\\\\\\\nconst item = this._item;\\\\\\\\nconst formula = item.damage.formula;\\\\\\\\nconst a = this._tempActorA;\\\\\\\\nconst b = this._tempActorB;\\\\\\\\nconst user = a;\\\\\\\\nconst target = b;\\\\\\\\n\\\\\\\\n// Return Value\\\\\\\\ntry {\\\\\\\\n    return formula;\\\\\\\\n} catch (e) {\\\\\\\\n    if ($gameTemp.isPlaytest()) {\\\\\\\\n        console.log('Damage Formula Error for %1'.format(this._item.name));\\\\\\\\n    }\\\\\\\\n    return '?????';\\\\\\\\n}\\\\\\\"\\\"}\"]"}
 *
 * @param Mechanics:struct
 * @text Mechanics Settings
 * @type struct<Mechanics>
 * @desc Settings pertaining to damage calculations.
 * @default {"ActionSpeed":"","AllowRandomSpeed:eval":"false","CalcActionSpeedJS:func":"\"// Declare Constants\\nconst agi = this.subject().agi;\\n\\n// Create Speed\\nlet speed = agi;\\nif (this.allowRandomSpeed()) {\\n    speed += Math.randomInt(Math.floor(5 + agi / 4));\\n}\\nif (this.item()) {\\n    speed += this.item().speed;\\n}\\nif (this.isAttack()) {\\n    speed += this.subject().attackSpeed();\\n}\\n\\n// Return Speed\\nreturn speed;\"","BaseTroop":"","BaseTroopIDs:arraynum":"[\"1\"]","CommonEvents":"","BattleStartEvent:num":"0","BattleEndEvent:num":"0","VictoryEvent:num":"0","DefeatEvent:num":"0","EscapeSuccessEvent:num":"0","EscapeFailEvent:num":"0","Escape":"","CalcEscapeRatioJS:func":"\"// Calculate Escape Ratio\\nlet ratio = 0.5;\\nratio *= $gameParty.agility();\\nratio /= $gameTroop.agility();\\n\\n// Return Ratio\\nreturn ratio;\"","CalcEscapeRaiseJS:func":"\"// Calculate Escape Ratio\\nlet value = 0.1;\\nvalue += $gameParty.aliveMembers().length;\\n\\n// Return Value\\nreturn value;\"","BattleJS":"","PreStartBattleJS:func":"\"// Declare Constants\\nconst user = this;\\nconst target = user;\\nconst a = user;\\nconst b = user;\\n\\n// Perform Actions\\n\"","PostStartBattleJS:func":"\"// Declare Constants\\nconst user = this;\\nconst target = user;\\nconst a = user;\\nconst b = user;\\n\\n// Perform Actions\\n\"","BattleVictoryJS:func":"\"// Declare Constants\\nconst user = this;\\nconst target = user;\\nconst a = user;\\nconst b = user;\\n\\n// Perform Actions\\n\"","EscapeSuccessJS:func":"\"// Declare Constants\\nconst user = this;\\nconst target = user;\\nconst a = user;\\nconst b = user;\\n\\n// Perform Actions\\n\"","EscapeFailureJS:func":"\"// Declare Constants\\nconst user = this;\\nconst target = user;\\nconst a = user;\\nconst b = user;\\n\\n// Perform Actions\\n\"","BattleDefeatJS:func":"\"// Declare Constants\\nconst user = this;\\nconst target = user;\\nconst a = user;\\nconst b = user;\\n\\n// Perform Actions\\n\"","PreEndBattleJS:func":"\"// Declare Constants\\nconst user = this;\\nconst target = user;\\nconst a = user;\\nconst b = user;\\n\\n// Perform Actions\\n\"","PostEndBattleJS:func":"\"// Declare Constants\\nconst user = this;\\nconst target = user;\\nconst a = user;\\nconst b = user;\\n\\n// Perform Actions\\n\"","TurnJS":"","PreStartTurnJS:func":"\"// Declare Constants\\nconst user = this;\\nconst target = user;\\nconst a = user;\\nconst b = user;\\n\\n// Perform Actions\\n\"","PostStartTurnJS:func":"\"// Declare Constants\\nconst user = this;\\nconst target = user;\\nconst a = user;\\nconst b = user;\\n\\n// Perform Actions\\n\"","PreEndTurnJS:func":"\"// Declare Constants\\nconst user = this;\\nconst target = user;\\nconst a = user;\\nconst b = user;\\n\\n// Perform Actions\\n\"","PostEndTurnJS:func":"\"// Declare Constants\\nconst user = this;\\nconst target = user;\\nconst a = user;\\nconst b = user;\\n\\n// Perform Actions\\n\"","PreRegenerateJS:func":"\"// Declare Constants\\nconst user = this;\\nconst target = user;\\nconst a = user;\\nconst b = user;\\n\\n// Perform Actions\\n\"","PostRegenerateJS:func":"\"// Declare Constants\\nconst user = this;\\nconst target = user;\\nconst a = user;\\nconst b = user;\\n\\n// Perform Actions\\n\"","ActionJS":"","PreStartActionJS:func":"\"// Declare Constants\\nconst value = arguments[0];\\nconst user = this.subject();\\nconst target = user;\\nconst a = user;\\nconst b = user;\\nconst action = this;\\nconst item = this.item();\\nconst skill = this.item();\\n\\n// Perform Actions\\n\"","PostStartActionJS:func":"\"// Declare Constants\\nconst value = arguments[0];\\nconst user = this.subject();\\nconst target = user;\\nconst a = user;\\nconst b = user;\\nconst action = this;\\nconst item = this.item();\\nconst skill = this.item();\\n\\n// Perform Actions\\n\"","PreApplyJS:func":"\"// Declare Constants\\nconst value = arguments[0];\\nconst target = arguments[1];\\nconst user = this.subject();\\nconst a = user;\\nconst b = target;\\nconst action = this;\\nconst item = this.item();\\nconst skill = this.item();\\n\\n// Perform Actions\\n\\n// Return Value\\nreturn value;\"","PreDamageJS:func":"\"// Declare Constants\\nconst value = arguments[0];\\nconst target = arguments[1];\\nconst user = this.subject();\\nconst a = user;\\nconst b = target;\\nconst action = this;\\nconst item = this.item();\\nconst skill = this.item();\\n\\n// Perform Actions\\n\\n// Return Value\\nreturn value;\"","PostDamageJS:func":"\"// Declare Constants\\nconst value = arguments[0];\\nconst target = arguments[1];\\nconst user = this.subject();\\nconst a = user;\\nconst b = target;\\nconst action = this;\\nconst item = this.item();\\nconst skill = this.item();\\n\\n// Perform Actions\\n\\n// Return Value\\nreturn value;\"","PostApplyJS:func":"\"// Declare Constants\\nconst value = arguments[0];\\nconst target = arguments[1];\\nconst user = this.subject();\\nconst a = user;\\nconst b = target;\\nconst action = this;\\nconst item = this.item();\\nconst skill = this.item();\\n\\n// Perform Actions\\n\\n// Return Value\\nreturn value;\"","PreEndActionJS:func":"\"// Declare Constants\\nconst value = arguments[0];\\nconst user = this.subject();\\nconst target = user;\\nconst a = user;\\nconst b = user;\\nconst action = this;\\nconst item = this.item();\\nconst skill = this.item();\\n\\n// Perform Actions\\n\"","PostEndActionJS:func":"\"// Declare Constants\\nconst value = arguments[0];\\nconst user = this.subject();\\nconst target = user;\\nconst a = user;\\nconst b = user;\\nconst action = this;\\nconst item = this.item();\\nconst skill = this.item();\\n\\n// Perform Actions\\n\""}
 *
 * @param CmdWindows
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param BattleLayout:struct
 * @text Battle Layout Settings
 * @type struct<BattleLayout>
 * @desc Settings that adjust how the battle layout appears.
 * @default {"Style:str":"default","ListStyle":"","ShowFacesListStyle:eval":"true","CommandWidth:num":"192","XPStyle":"","XPActorCommandLines:num":"4","XPActorDefaultHeight:num":"64","XPSpriteYLocation:str":"name","PotraitStyle":"","ShowPortraits:eval":"true","PortraitScale:num":"0.5","BorderStyle":"","SkillItemBorderCols:num":"1","ShowPortraitsBorderStyle:eval":"true","PortraitScaleBorderStyle:num":"1.25","SkillItemWindows":"","SkillItemMiddleLayout:eval":"false","SkillItemStandardCols:num":"2"}
 *
 * @param BattleLog:struct
 * @text Battle Log Settings
 * @type struct<BattleLog>
 * @desc Settings that adjust how Window_BattleLog behaves.
 * @default {"General":"","BackColor:str":"#000000","MaxLines:num":"10","MessageWait:num":"16","TextAlign:str":"center","BattleLogRectJS:func":"\"const wx = 0;\\nconst wy = 0;\\nconst ww = Graphics.boxWidth;\\nconst wh = this.calcWindowHeight(10, false);\\nreturn new Rectangle(wx, wy, ww, wh);\"","StartTurn":"","StartTurnShow:eval":"true","StartTurnMsg:str":"Turn %1","StartTurnWait:num":"40","DisplayAction":"","ActionCenteredName:eval":"true","ActionSkillMsg1:eval":"false","ActionSkillMsg2:eval":"true","ActionItemMsg:eval":"false","ActionChanges":"","ShowCounter:eval":"true","ShowReflect:eval":"true","ShowSubstitute:eval":"true","ActionResults":"","ShowFailure:eval":"false","ShowCritical:eval":"false","ShowMissEvasion:eval":"false","ShowHpDmg:eval":"false","ShowMpDmg:eval":"false","ShowTpDmg:eval":"false","DisplayStates":"","ShowAddedState:eval":"false","ShowRemovedState:eval":"false","ShowCurrentState:eval":"false","ShowAddedBuff:eval":"false","ShowAddedDebuff:eval":"false","ShowRemovedBuff:eval":"false"}
 *
 * @param Battleback:struct
 * @text Battleback Scaling
 * @type struct<Battleback>
 * @desc Settings that adjust how battlebacks scale.
 * @default {"DefaultStyle:str":"MZ","jsOneForOne:func":"\"// Adjust Size\\nthis.width = Graphics.width;\\nthis.height = Graphics.height;\\n\\n// Adjust Scale\\nconst scale = 1.0;\\nthis.scale.x = scale;\\nthis.scale.y = scale;\\n\\n// Adjust Coordinates\\nthis.x = 0;\\nthis.y = 0;\"","jsScaleToFit:func":"\"// Adjust Size\\nthis.width = Graphics.width;\\nthis.height = Graphics.height;\\n\\n// Adjust Scale\\nconst ratioX = this.width / this.bitmap.width;\\nconst ratioY = this.height / this.bitmap.height;\\nconst scale = Math.max(ratioX, ratioY);\\nthis.scale.x = scale;\\nthis.scale.y = scale;\\n\\n// Adjust Coordinates\\nthis.x = (Graphics.width - this.width) / 2;\\nthis.y = Graphics.height - this.height;\"","jsScaleDown:func":"\"// Adjust Size\\nthis.width = Graphics.width;\\nthis.height = Graphics.height;\\n\\n// Adjust Scale\\nconst ratioX = Math.min(1, this.width / this.bitmap.width);\\nconst ratioY = Math.min(1, this.height / this.bitmap.height);\\nconst scale = Math.max(ratioX, ratioY);\\nthis.scale.x = scale;\\nthis.scale.y = scale;\\n\\n// Adjust Coordinates\\nthis.x = (Graphics.width - this.width) / 2;\\nthis.y = Graphics.height - this.height;\"","jsScale Up:func":"\"// Adjust Size\\nthis.width = Graphics.width;\\nthis.height = Graphics.height;\\n\\n// Adjust Scale\\nconst ratioX = Math.max(1, this.width / this.bitmap.width);\\nconst ratioY = Math.max(1, this.height / this.bitmap.height);\\nconst scale = Math.max(ratioX, ratioY);\\nthis.scale.x = scale;\\nthis.scale.y = scale;\\n\\n// Adjust Coordinates\\nthis.x = (Graphics.width - this.width) / 2;\\nthis.y = Graphics.height - this.height;\""}
 *
 * @param PartyCmd:struct
 * @text Party Command Window
 * @type struct<PartyCmd>
 * @desc Settings that alter the Party Command Window in battle.
 * @default {"Cmd":"","CmdStyle:str":"auto","CmdTextAlign:str":"left","CmdIconFight:num":"76","CommandAddAutoBattle:eval":"true","CmdIconAutoBattle:num":"78","CmdTextAutoBattle:str":"Auto","CommandAddOptions:eval":"true","CmdIconOptions:num":"83","ActiveTpbOptionsMessage:str":"Options Menu queued after action is complete.","CmdIconEscape:num":"82","Access":"","SkipPartyCmd:eval":"true","DisablePartyCmd:eval":"false","HelpWindow":"","HelpFight:str":"Select actions to fight.","HelpAutoBattle:str":"Sets party to Auto Battle mode.","HelpOptions:str":"Opens up the Options Menu.","HelpEscape:str":"Attempt to escape the battle."}
 *
 * @param ActorCmd:struct
 * @text Actor Command Window
 * @type struct<ActorCmd>
 * @desc Settings that alter the Actor Command Window in battle.
 * @default {"Cmd":"","CmdStyle:str":"auto","CmdTextAlign:str":"left","CmdIconItem:num":"176","IconStypeNorm:num":"78","IconStypeMagic:num":"79","BattleCmd":"","BattleCmdList:arraystr":"[\"attack\",\"skills\",\"guard\",\"item\",\"escape\"]","HelpWindow":"","HelpSkillType:str":"Opens up a list of skills under the \\C[16]%1\\C[0] category.","HelpItem:str":"Opens up a list of items that you can use.","HelpEscape:str":"Attempt to escape the battle.","HelpAutoBattle:str":"Automatically choose an action suitable for combat."}
 *
 * @param VisualBreak
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param Actor:struct
 * @text Actor Battler Settings
 * @type struct<Actor>
 * @desc Settings that alter various properties for actors.
 * @default {"Flinch":"","FlinchDistanceX:num":"12","FlinchDistanceY:num":"0","FlinchDuration:num":"6","SvBattlers":"","AnchorX:num":"0.5","AnchorY:num":"1.0","ChantStyle:eval":"true","OffsetX:num":"0","OffsetY:num":"0","MotionSpeed:num":"12","PrioritySortActive:eval":"true","PrioritySortActors:eval":"false","Shadow:eval":"true","SmoothImage:eval":"true","HomePosJS:func":"\"// Declare Constants\\nconst sprite = this;\\nconst actor = this._actor;\\nconst index = arguments[0];\\n\\n// Make Calculations\\nlet x = Math.round((Graphics.width / 2) + 192)\\nx -= Math.floor((Graphics.width - Graphics.boxWidth) / 2);\\nx += index * 32;\\nlet y = (Graphics.height - 200) - ($gameParty.maxBattleMembers() * 48);\\ny -= Math.floor((Graphics.height - Graphics.boxHeight) / 2);\\ny += index * 48;\\n\\n// Home Position Offsets\\nconst offsetNote = /<SIDEVIEW HOME OFFSET:[ ]([\\\\+\\\\-]\\\\d+),[ ]([\\\\+\\\\-]\\\\d+)>/i;\\nconst xOffsets = actor.traitObjects().map((obj) => (obj && obj.note.match(offsetNote) ? Number(RegExp.$1) : 0));\\nconst yOffsets = actor.traitObjects().map((obj) => (obj && obj.note.match(offsetNote) ? Number(RegExp.$2) : 0));\\nx = xOffsets.reduce((r, offset) => r + offset, x);\\ny = yOffsets.reduce((r, offset) => r + offset, y);\\n\\n// Set Home Position\\nthis.setHome(x, y);\""}
 *
 * @param Enemy:struct
 * @text Enemy Battler Settings
 * @type struct<Enemy>
 * @desc Settings that alter various properties for enemies.
 * @default {"Visual":"","AttackAnimation:num":"1","EmergeText:eval":"false","OffsetX:num":"0","OffsetY:num":"0","SmoothImage:eval":"true","SelectWindow":"","FrontViewSelect:eval":"false","SideviewSelect:eval":"true","NameFontSize:num":"22","SvBattlers":"","AllowCollapse:eval":"false","AnchorX:num":"0.5","AnchorY:num":"1.0","MotionIdle:str":"walk","Shadow:eval":"true","Width:num":"64","Height:num":"64","WtypeId:num":"0"}
 *
 * @param HpGauge:struct
 * @text HP Gauge Settings
 * @type struct<HpGauge>
 * @desc Settings that adjust the visual HP Gauge displayed in battle.
 * @default {"Display":"","ShowActorGauge:eval":"false","ShowEnemyGauge:eval":"true","RequiresDefeat:eval":"false","BTestBypass:eval":"true","Settings":"","AnchorX:num":"0.5","AnchorY:num":"1.0","Scale:num":"0.5","OffsetX:num":"0","OffsetY:num":"-3","Options":"","AddHpGaugeOption:eval":"true","AdjustRect:eval":"true","Name:str":"Show HP Gauge"}
 *
 * @param ActionSequence:struct
 * @text Action Sequence Settings
 * @type struct<ActionSequence>
 * @desc Settings that adjust how certain Action Sequences work.
 * @default {"AutoSequences":"","AutoMeleeSolo:eval":"true","AutoMeleeAoE:eval":"true","CastAnimations":"","CastCertain:num":"120","CastPhysical:num":"52","CastMagical:num":"51","CounterReflection":"","CounterPlayback:eval":"true","ReflectAnimation:num":"1","ReflectPlayback:eval":"true","Stepping":"","MeleeDistance:num":"24","StepDistanceX:num":"48","StepDistanceY:num":"0","StepDuration:num":"12"}
 *
 * @param BreakEnd1
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param End Of
 * @default Plugin Parameters
 *
 * @param BreakEnd2
 * @text --------------------------
 * @default ----------------------------------
 *
 */
/* ----------------------------------------------------------------------------
 * Auto Battle Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~AutoBattle:
 *
 * @param BattleDisplay
 * @text Battle Display
 *
 * @param AutoBattleMsg:str
 * @text Message
 * @parent BattleDisplay
 * @desc Message that's displayed when Auto Battle is on.
 * Text codes allowed. %1 - OK button, %2 - Cancel button
 * @default Press %1 or %2 to stop Auto Battle
 *
 * @param AutoBattleOK:str
 * @text OK Button
 * @parent BattleDisplay
 * @desc Text used to represent the OK button.
 * If VisuMZ_0_CoreEngine is present, ignore this.
 * @default OK
 *
 * @param AutoBattleCancel:str
 * @text Cancel Button
 * @parent BattleDisplay
 * @desc Text used to represent the Cancel button.
 * If VisuMZ_0_CoreEngine is present, ignore this.
 * @default Cancel
 *
 * @param AutoBattleBgType:num
 * @text Background Type
 * @parent BattleDisplay
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for Auto Battle window.
 * @default 1
 *
 * @param AutoBattleRect:func
 * @text JS: X, Y, W, H
 * @parent BattleDisplay
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const width = Graphics.width;\nconst height = this.calcWindowHeight(1, false);\nconst x = 0;\nconst y = (Graphics.height - height) / 2;\nreturn new Rectangle(x, y, width, height);"
 *
 * @param Options
 *
 * @param AddOption:eval
 * @text Add Option?
 * @parent Options
 * @type boolean
 * @on Add
 * @off Don't Add
 * @desc Add the Auto Battle options to the Options menu?
 * @default true
 *
 * @param AdjustRect:eval
 * @text Adjust Window Height
 * @parent Options
 * @type boolean
 * @on Adjust
 * @off Don't
 * @desc Automatically adjust the options window height?
 * @default true
 *
 * @param StartName:str
 * @text Startup Name
 * @parent Options
 * @desc Command name of the option.
 * @default Auto Battle Start
 *
 * @param StyleName:str
 * @text Style Name
 * @parent Options
 * @desc Command name of the option.
 * @default Auto Battle Style
 *
 * @param StyleOFF:str
 * @text OFF
 * @parent StyleName:str
 * @desc Text displayed when Auto Battle Style is OFF.
 * @default Attack
 *
 * @param StyleON:str
 * @text ON
 * @parent StyleName:str
 * @desc Text displayed when Auto Battle Style is ON.
 * @default Skills
 *
 */
/* ----------------------------------------------------------------------------
 * Damage Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Damage:
 *
 * @param Cap
 * @text Damage Cap
 *
 * @param EnableDamageCap:eval
 * @text Enable Damage Cap?
 * @parent Cap
 * @type boolean
 * @on Enable
 * @off Disable
 * @desc Put a maximum hard damage cap on how far damage can go?
 * This can be broken through the usage of notetags.
 * @default false
 *
 * @param DefaultHardCap:num
 * @text Default Hard Cap
 * @parent EnableDamageCap:eval
 * @type number
 * @min 1
 * @desc The default hard damage cap used before applying damage.
 * @default 9999
 *
 * @param EnableSoftCap:eval
 * @text Enable Soft Cap?
 * @parent Cap
 * @type boolean
 * @on Enable
 * @off Disable
 * @desc Soft caps ease in the damage values leading up to the 
 * hard damage cap. Requires hard Damage Cap enabled.
 * @default false
 *
 * @param DefaultSoftCap:num
 * @text Base Soft Cap Rate
 * @parent EnableSoftCap:eval
 * @desc The default soft damage cap used before applying damage.
 * @default 0.80
 *
 * @param DefaultSoftScaler:num
 * @text Soft Scale Constant
 * @parent EnableSoftCap:eval
 * @desc The default soft damage cap used before applying damage.
 * @default 0.1275
 *
 * @param Popups
 *
 * @param PopupDuration:num
 * @text Popup Duration
 * @parent Popups
 * @type number
 * @min 1
 * @desc Adjusts how many frames a popup stays visible.
 * @default 128
 *
 * @param NewPopupBottom:eval
 * @text Newest Popups Bottom
 * @parent Popups
 * @type boolean
 * @on Bottom
 * @off Top
 * @desc Puts the newest popups at the bottom.
 * @default true
 *
 * @param PopupPosition:str
 * @text Appear Position
 * @parent Popups
 * @type select
 * @option Head - At the top of the battler.
 * @value head
 * @option Center - At the center of the battler.
 * @value center
 * @option Base - At the foot of the battler.
 * @value base
 * @desc Selects where you want popups to appear relative to the battler.
 * @default base
 *
 * @param PopupOffsetX:num
 * @text Offset X
 * @parent Popups
 * @desc Sets how much to offset the sprites by horizontally.
 * Negative values go left. Positive values go right.
 * @default 0
 *
 * @param PopupOffsetY:num
 * @text Offset Y
 * @parent Popups
 * @desc Sets how much to offset the sprites by vertically.
 * Negative values go up. Positive values go down.
 * @default 0
 *
 * @param PopupShiftX:num
 * @text Shift X
 * @parent Popups
 * @desc Sets how much to shift the sprites by horizontally.
 * Negative values go left. Positive values go right.
 * @default 8
 *
 * @param PopupShiftY:num
 * @text Shift Y
 * @parent Popups
 * @desc Sets how much to shift the sprites by vertically.
 * Negative values go up. Positive values go down.
 * @default -28
 *
 * @param hpDamageFmt:str
 * @text HP Damage Format
 * @parent Popups
 * @desc Determines HP damage format for popup.
 * %1 - Value, %2 - HP Text
 * @default -%1
 *
 * @param hpHealingFmt:str
 * @text HP Healing Format
 * @parent Popups
 * @desc Determines HP healing format for popup.
 * %1 - Value, %2 - HP Text
 * @default +%1
 *
 * @param mpDamageFmt:str
 * @text MP Damage Format
 * @parent Popups
 * @desc Determines MP damage format for popup.
 * %1 - Value, %2 - MP Text
 * @default -%1 %2
 *
 * @param mpHealingFmt:str
 * @text MP Healing Format
 * @parent Popups
 * @desc Determines MP healing format for popup.
 * %1 - Value, %2 - MP Text
 * @default +%1 %2
 *
 * @param CriticalColor:eval
 * @text Critical Flash Color
 * @parent Popups
 * @desc Adjust the popup's flash color.
 * Format: [red, green, blue, alpha]
 * @default [255, 0, 0, 160]
 *
 * @param CriticalDuration:num
 * @text Critical Duration
 * @parent Popups
 * @type number
 * @min 1
 * @desc Adjusts how many frames a the flash lasts.
 * @default 128
 *
 * @param Formulas
 *
 * @param OverallFormulaJS:func
 * @text JS: Overall Formula
 * @parent Formulas
 * @type note
 * @desc The overall formula used when calculating damage.
 * @default "// Declare Constants\nconst target = arguments[0];\nconst critical = arguments[1];\nconst item = this.item();\n\n// Get Base Damage\nconst baseValue = this.evalDamageFormula(target);\n\n// Calculate Element Modifiers\nlet value = baseValue * this.calcElementRate(target);\n\n// Calculate Physical and Magical Modifiers\nif (this.isPhysical()) {\n    value *= target.pdr;\n}\nif (this.isMagical()) {\n    value *= target.mdr;\n}\n\n// Apply Healing Modifiers\nif (baseValue < 0) {\n    value *= target.rec;\n}\n\n// Apply Critical Modifiers\nif (critical) {\n    value = this.applyCritical(value);\n}\n\n// Apply Variance and Guard Modifiers\nvalue = this.applyVariance(value, item.damage.variance);\nvalue = this.applyGuard(value, target);\n\n// Finalize Damage\nvalue = Math.round(value);\nreturn value;"
 *
 * @param VarianceFormulaJS:func
 * @text JS: Variance Formula
 * @parent Formulas
 * @type note
 * @desc The formula used when damage variance.
 * @default "// Declare Constants\nconst damage = arguments[0];\nconst variance = arguments[1];\n\n// Calculate Variance\nconst amp = Math.floor(Math.max((Math.abs(damage) * variance) / 100, 0));\nconst v = Math.randomInt(amp + 1) + Math.randomInt(amp + 1) - amp;\n\n// Return Damage\nreturn damage >= 0 ? damage + v : damage - v;"
 *
 * @param GuardFormulaJS:func
 * @text JS: Guard Formula
 * @parent Formulas
 * @type note
 * @desc The formula used when damage is guarded.
 * @default "// Declare Constants\nconst damage = arguments[0];\nconst target = arguments[1];\n\n// Return Damage Early\nconst note = this.item().note;\nif (note.match(/<UNBLOCKABLE>/i)) return damage;\nif (!target.isGuard()) return damage;\nif (damage < 0) return damage;\n\n// Declare Guard Rate\nlet guardRate = 0.5;\nguardRate /= target.grd;\n\n// Return Damage\nreturn damage * guardRate;"
 *
 * @param Critical
 * @text Critical Hits
 *
 * @param CriticalHitRateJS:func
 * @text JS: Rate Formula
 * @parent Critical
 * @type note
 * @desc The formula used to calculate Critical Hit Rates.
 * @default "// Declare Constants\nconst user = this.subject();\nconst target = arguments[0];\n\n// Create Base Critical Rate\nlet rate = this.subject().cri * (1 - target.cev);\n\n// Apply Notetags\nconst note = this.item().note;\nif (note.match(/<ALWAYS CRITICAL>/i)) {\n    return 1;\n}\nif (note.match(/<SET CRITICAL RATE:[ ](\\d+)([%％])>/i)) {\n    return Number(RegExp.$1) / 100;\n}\nif (note.match(/<MODIFY CRITICAL RATE:[ ](\\d+)([%％])>/i)) {\n    rate *= Number(RegExp.$1) / 100;\n}\nif (note.match(/<MODIFY CRITICAL RATE:[ ]([\\+\\-]\\d+)([%％])>/i)) {\n    rate += Number(RegExp.$1) / 100;\n}\nif (note.match(/<JS CRITICAL RATE>\\s*([\\s\\S]*)\\s*<\\/JS CRITICAL RATE>/i)) {\n    const code = String(RegExp.$1);\n    try {\n        eval(code);\n    } catch (e) {\n        if ($gameTemp.isPlaytest()) console.log(e);\n    }\n}\n\n// Apply LUK Buffs/Debuffs\nconst lukStack = this.subject().buff(7);\nrate *= 2 ** lukStack;\n\n// Return Rate\nreturn rate;"
 *
 * @param CriticalHitMultiplier:func
 * @text JS: Damage Formula
 * @parent Critical
 * @type note
 * @desc The formula used to calculate Critical Hit Damage modification.
 * @default "// Declare Constants\nconst user = this.subject();\nlet damage = arguments[0];\nlet multiplier = 2.0;\nlet bonusDamage = this.subject().luk * this.subject().cri;\n\n// Apply Notetags\nconst note = this.item().note;\nif (note.match(/<MODIFY CRITICAL MULTIPLIER:[ ](\\d+)([%％])>/i)) {\n    multiplier = Number(RegExp.$1) / 100;\n}\nif (note.match(/<MODIFY CRITICAL MULTIPLIER:[ ]([\\+\\-]\\d+)([%％])>/i)) {\n    multiplier += Number(RegExp.$1) / 100;\n}\nif (note.match(/<MODIFY CRITICAL BONUS DAMAGE:[ ](\\d+)([%％])>/i)) {\n    bonusDamage *= Number(RegExp.$1) / 100;\n}\nif (note.match(/<MODIFY CRITICAL BONUS DAMAGE:[ ]([\\+\\-]\\d+)([%％])>/i)) {\n    bonusDamage += bonusDamage * (RegExp.$1) / 100;\n}\nif (note.match(/<JS CRITICAL DAMAGE>\\s*([\\s\\S]*)\\s*<\\/JS CRITICAL DAMAGE>/i)) {\n    const code = String(RegExp.$1);\n    try {\n        eval(code);\n    } catch (e) {\n        if ($gameTemp.isPlaytest()) console.log(e);\n    }\n}\n\n// Return Damage\nreturn damage * multiplier + bonusDamage;"
 *
 * @param DamageStyles
 * @text Damage Styles
 *
 * @param DefaultDamageStyle:str
 * @text Default Style
 * @parent DamageStyles
 * @desc Which Damage Style do you want to set as default?
 * Use 'Manual' to not use any styles at all.
 * @default Standard
 *
 * @param DamageStyleList:arraystruct
 * @text Style List
 * @parent DamageStyles
 * @type struct<DamageStyle>[]
 * @desc A list of the damage styles available.
 * These are used to calculate base damage.
 * @default ["{\"Name:str\":\"Standard\",\"Formula:func\":\"\\\"// Declare Constants\\\\nconst user = this.subject();\\\\nconst target = arguments[0];\\\\nconst item = this.item();\\\\nconst a = this.subject();\\\\nconst b = target;\\\\nconst v = $gameVariables._data;\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\n\\\\n// Replace Formula\\\\nlet formula = item.damage.formula;\\\\nif (SceneManager.isSceneBattle() && !this.isCertainHit()) {\\\\n    const fmt = 'Math.max(this.applyArmorModifiers(b, %1), 0)';\\\\n    formula = formula.replace(/b.def/g, fmt.format('b.def'));\\\\n    formula = formula.replace(/b.mdf/g, fmt.format('b.mdf'));\\\\n    formula = formula.replace(/b.agi/g, fmt.format('b.agi'));\\\\n    formula = formula.replace(/b.luk/g, fmt.format('b.luk'));\\\\n}\\\\n\\\\n// Calculate Damage\\\\nlet value = Math.max(eval(formula), 0);\\\\n\\\\n// Return Value\\\\nreturn (isNaN(value) ? 0 : value) * sign;\\\"\",\"ItemsEquipsCore\":\"\",\"DamageType\":\"\",\"DamageType1:str\":\"%1 Damage Multiplier\",\"DamageType2:str\":\"%1 Damage Multiplier\",\"DamageType3:str\":\"%1 Recovery Multiplier\",\"DamageType4:str\":\"%1 Recovery Multiplier\",\"DamageType5:str\":\"%1 Drain Multiplier\",\"DamageType6:str\":\"%1 Drain Multiplier\",\"DamageDisplay:func\":\"\\\"return this.getItemDamageAmountTextOriginal();\\\"\"}","{\"Name:str\":\"Armor Scaling\",\"Formula:func\":\"\\\"// Declare Constants\\\\nconst user = this.subject();\\\\nconst target = arguments[0];\\\\nconst item = this.item();\\\\nconst a = this.subject();\\\\nconst b = target;\\\\nconst v = $gameVariables._data;\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\n\\\\n// Replace Formula\\\\nlet formula = item.damage.formula;\\\\nif (SceneManager.isSceneBattle() && !this.isCertainHit()) {\\\\n    const fmt = 'Math.max(this.applyArmorModifiers(b, %1), 1)';\\\\n    formula = formula.replace(/b.def/g, fmt.format('b.def'));\\\\n    formula = formula.replace(/b.mdf/g, fmt.format('b.mdf'));\\\\n    formula = formula.replace(/b.agi/g, fmt.format('b.agi'));\\\\n    formula = formula.replace(/b.luk/g, fmt.format('b.luk'));\\\\n}\\\\n\\\\n// Calculate Damage\\\\nlet value = Math.max(eval(formula), 0);\\\\n\\\\n// Apply Defender's Defense Parameter\\\\nif (this.isDamage() && !this.isCertainHit()) {\\\\n\\\\n    // Calculate Base Armor\\\\n    let armor = this.isPhysical() ? b.def : b.mdf;\\\\n    armor = this.applyArmorModifiers(target, armor);\\\\n\\\\n    // Apply Armor to Damage\\\\n    if (armor >= 0) {\\\\n        value *= 100 / (100 + armor);\\\\n    } else {\\\\n        value *= 2 - (100 / (100 - armor));\\\\n    }\\\\n}\\\\n\\\\n// Return Value\\\\nreturn (isNaN(value) ? 0 : value) * sign;\\\"\",\"ItemsEquipsCore\":\"\",\"DamageType\":\"\",\"DamageType1:str\":\"%1 Damage Multiplier\",\"DamageType2:str\":\"%1 Damage Multiplier\",\"DamageType3:str\":\"%1 Recovery Multiplier\",\"DamageType4:str\":\"%1 Recovery Multiplier\",\"DamageType5:str\":\"%1 Drain Multiplier\",\"DamageType6:str\":\"%1 Drain Multiplier\",\"DamageDisplay:func\":\"\\\"return this.getItemDamageAmountTextOriginal();\\\"\"}","{\"Name:str\":\"CT\",\"Formula:func\":\"\\\"// Define Constants\\\\nconst user = this.subject();\\\\nconst target = arguments[0];\\\\nconst item = this.item();\\\\nconst a = this.subject();\\\\nconst b = target;\\\\nconst v = $gameVariables._data;\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\n\\\\n// Create Multiplier\\\\nconst multiplier = Math.max(eval(item.damage.formula), 0);\\\\n\\\\n// Declare Values\\\\nlet value = 0;\\\\nlet level = Math.max(a.level || a.luk, 1);\\\\nlet armor = this.isPhysical() ? b.def : b.mdf;\\\\narmor = Math.max(this.applyArmorModifiers(target, armor), 0);\\\\nlet attackStat = 0;\\\\nif (this.isPhysical() && (this.isDamage() || this.isDrain())) {\\\\n    attackStat = a.atk;\\\\n} else if (this.isMagical() && (this.isDamage() || this.isDrain())) {\\\\n    attackStat =  a.mat;\\\\n} else if (this.isPhysical() && this.isRecover()) {\\\\n    attackStat =  a.def;\\\\n} else if (this.isMagical() && this.isRecover()) {\\\\n    attackStat =  a.mdf;\\\\n}\\\\n\\\\n// Calculate Damage\\\\nattackStat = (attackStat * 1.75) + (level ** 2 / 45.5);\\\\nvalue = attackStat * 4;\\\\nif (this.isPhysical() && (this.isDamage() || this.isDrain())) {\\\\n    value *= Math.max(256 - armor, 0) / 256;\\\\n} else if (this.isMagical() && (this.isDamage() || this.isDrain())) {\\\\n    value *= Math.max(102.4 - armor, 0) / 128;\\\\n}\\\\nvalue *= multiplier;\\\\n\\\\n// Return Value\\\\nreturn (isNaN(value) ? 0 : value) * sign;\\\"\",\"ItemsEquipsCore\":\"\",\"DamageType\":\"\",\"DamageType1:str\":\"%1 Damage Multiplier\",\"DamageType2:str\":\"%1 Damage Multiplier\",\"DamageType3:str\":\"%1 Recovery Multiplier\",\"DamageType4:str\":\"%1 Recovery Multiplier\",\"DamageType5:str\":\"%1 Drain Multiplier\",\"DamageType6:str\":\"%1 Drain Multiplier\",\"DamageDisplay:func\":\"\\\"// Define Constants\\\\nconst item = this._item;\\\\nconst formula = item.damage.formula;\\\\nconst a = this._tempActorA;\\\\nconst b = this._tempActorB;\\\\nconst user = a;\\\\nconst target = b;\\\\n\\\\n// Return Value\\\\ntry {\\\\n    const value = Math.max(eval(formula), 0);\\\\n    return '%1%'.format(Math.round(value * 100));\\\\n} catch (e) {\\\\n    if ($gameTemp.isPlaytest()) {\\\\n        console.log('Damage Formula Error for %1'.format(this._item.name));\\\\n    }\\\\n    return '?????';\\\\n}\\\"\"}","{\"Name:str\":\"D4\",\"Formula:func\":\"\\\"// Define Constants\\\\nconst user = this.subject();\\\\nconst target = arguments[0];\\\\nconst item = this.item();\\\\nconst a = this.subject();\\\\nconst b = target;\\\\nconst v = $gameVariables._data;\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\n\\\\n// Create Multiplier\\\\nconst multiplier = Math.max(eval(item.damage.formula), 0);\\\\n\\\\n// Declare Values\\\\nlet armor = this.isPhysical() ? b.def : b.mdf;\\\\narmor = this.applyArmorModifiers(target, armor);\\\\nlet stat = 0;\\\\nif (this.isPhysical() && (this.isDamage() || this.isDrain())) {\\\\n    stat = a.atk;\\\\n} else if (this.isMagical() && (this.isDamage() || this.isDrain())) {\\\\n    stat = a.mat;\\\\n} else if (this.isPhysical() && this.isRecover()) {\\\\n    stat = a.def;\\\\n    armor = 0;\\\\n} else if (this.isMagical() && this.isRecover()) {\\\\n    stat = a.mdf;\\\\n    armor = 0;\\\\n}\\\\n\\\\n// Calculate Damage \\\\nlet value = 1.5 * Math.max(2 * stat * multiplier - armor, 1) * multiplier / 5;\\\\n\\\\n// Return Value\\\\nreturn (isNaN(value) ? 0 : value) * sign;\\\"\",\"ItemsEquipsCore\":\"\",\"DamageType\":\"\",\"DamageType1:str\":\"%1 Damage Multiplier\",\"DamageType2:str\":\"%1 Damage Multiplier\",\"DamageType3:str\":\"%1 Recovery Multiplier\",\"DamageType4:str\":\"%1 Recovery Multiplier\",\"DamageType5:str\":\"%1 Drain Multiplier\",\"DamageType6:str\":\"%1 Drain Multiplier\",\"DamageDisplay:func\":\"\\\"// Define Constants\\\\nconst item = this._item;\\\\nconst formula = item.damage.formula;\\\\nconst a = this._tempActorA;\\\\nconst b = this._tempActorB;\\\\nconst user = a;\\\\nconst target = b;\\\\n\\\\n// Return Value\\\\ntry {\\\\n    const value = Math.max(eval(formula), 0);\\\\n    return '%1%'.format(Math.round(value * 100));\\\\n} catch (e) {\\\\n    if ($gameTemp.isPlaytest()) {\\\\n        console.log('Damage Formula Error for %1'.format(this._item.name));\\\\n    }\\\\n    return '?????';\\\\n}\\\"\"}","{\"Name:str\":\"DQ\",\"Formula:func\":\"\\\"// Define Constants\\\\nconst user = this.subject();\\\\nconst target = arguments[0];\\\\nconst item = this.item();\\\\nconst a = this.subject();\\\\nconst b = target;\\\\nconst v = $gameVariables._data;\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\n\\\\n// Create Multiplier\\\\nlet multiplier = Math.max(eval(item.damage.formula), 0);\\\\nif (this.isCertainHit()) {\\\\n    let value = multiplier * Math.max(a.atk, a.mat);\\\\n    return (isNaN(value) ? 0 : value) * sign;\\\\n}\\\\n\\\\n// Get Primary Stats\\\\nlet armor = this.isPhysical() ? b.def : b.mdf;\\\\narmor = this.applyArmorModifiers(b, armor);\\\\nlet stat = 1;\\\\nif (this.isPhysical() && (this.isDamage() || this.isDrain())) {\\\\n    stat = a.atk;\\\\n} else if (this.isMagical() && (this.isDamage() || this.isDrain())) {\\\\n    stat = a.mat;\\\\n} else if (this.isPhysical() && this.isRecover()) {\\\\n    stat = a.def;\\\\n} else if (this.isMagical() && this.isRecover()) {\\\\n    stat = a.mdf;\\\\n}\\\\n\\\\n// Check for Recovery\\\\nif (this.isRecover()) {\\\\n    let value = stat * multiplier * sign;\\\\n    return isNaN(value) ? 0 : value;\\\\n}\\\\n\\\\n// Calculate Damage\\\\nlet value = 0;\\\\nif (stat < ((2 + armor) / 2)) {\\\\n    // Plink Damage\\\\n    let baseline = Math.max(stat - ((12 * (armor - stat + 1)) / stat), 5);\\\\n    value = baseline / 3;\\\\n} else {\\\\n    // Normal Damage\\\\n    let baseline = Math.max(stat - (armor / 2), 1);\\\\n    value = baseline / 2;\\\\n}\\\\nvalue *= multiplier;\\\\n\\\\n// Return Value\\\\nreturn isNaN(value) ? 0 : value;\\\"\",\"ItemsEquipsCore\":\"\",\"DamageType\":\"\",\"DamageType1:str\":\"%1 Damage Multiplier\",\"DamageType2:str\":\"%1 Damage Multiplier\",\"DamageType3:str\":\"%1 Recovery Multiplier\",\"DamageType4:str\":\"%1 Recovery Multiplier\",\"DamageType5:str\":\"%1 Drain Multiplier\",\"DamageType6:str\":\"%1 Drain Multiplier\",\"DamageDisplay:func\":\"\\\"// Define Constants\\\\nconst item = this._item;\\\\nconst formula = item.damage.formula;\\\\nconst a = this._tempActorA;\\\\nconst b = this._tempActorB;\\\\nconst user = a;\\\\nconst target = b;\\\\n\\\\n// Return Value\\\\ntry {\\\\n    const value = Math.max(eval(formula), 0);\\\\n    return '%1%'.format(Math.round(value * 100));\\\\n} catch (e) {\\\\n    if ($gameTemp.isPlaytest()) {\\\\n        console.log('Damage Formula Error for %1'.format(this._item.name));\\\\n    }\\\\n    return '?????';\\\\n}\\\"\"}","{\"Name:str\":\"FF7\",\"Formula:func\":\"\\\"// Define Constants\\\\nconst user = this.subject();\\\\nconst target = arguments[0];\\\\nconst item = this.item();\\\\nconst a = this.subject();\\\\nconst b = target;\\\\nconst v = $gameVariables._data;\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\n\\\\n// Create Power\\\\nconst power = Math.max(eval(item.damage.formula), 0);\\\\n\\\\n// Declare base Damage\\\\nlet baseDamage = 0;\\\\nlet level = Math.max(a.level || a.luk, 1);\\\\nif (this.isPhysical() && (this.isDamage() || this.isDrain())) {\\\\n    baseDamage = a.atk + ((a.atk + level) / 32) * ((a.atk * level) / 32);\\\\n} else if (this.isMagical() && (this.isDamage() || this.isDrain())) {\\\\n    baseDamage = 6 * (a.mat + level);\\\\n} else if (this.isPhysical() && this.isRecover()) {\\\\n    baseDamage = 6 * (a.def + level);\\\\n} else if (this.isMagical() && this.isRecover()) {\\\\n    baseDamage = 6 * (a.mdf + level);\\\\n}\\\\n\\\\n// Calculate Final Damage\\\\nlet value = baseDamage;\\\\nlet armor = this.isPhysical() ? b.def : b.mdf;\\\\narmor = this.applyArmorModifiers(target, armor);\\\\nif (this.isRecover()) {\\\\n    value += 22 * power;\\\\n} else {\\\\n    value = (power * Math.max(512 - armor, 1) * baseDamage) / (16 * 512);\\\\n}\\\\n\\\\n// Return Value\\\\nreturn (isNaN(value) ? 0 : value) * sign;\\\"\",\"ItemsEquipsCore\":\"\",\"DamageType\":\"\",\"DamageType1:str\":\"%1 Damage Power\",\"DamageType2:str\":\"%1 Damage Power\",\"DamageType3:str\":\"%1 Recovery Power\",\"DamageType4:str\":\"%1 Recovery Power\",\"DamageType5:str\":\"%1 Drain Power\",\"DamageType6:str\":\"%1 Drain Power\",\"DamageDisplay:func\":\"\\\"// Define Constants\\\\nconst item = this._item;\\\\nconst formula = item.damage.formula;\\\\nconst a = this._tempActorA;\\\\nconst b = this._tempActorB;\\\\nconst user = a;\\\\nconst target = b;\\\\n\\\\n// Return Value\\\\ntry {\\\\n    return formula;\\\\n} catch (e) {\\\\n    if ($gameTemp.isPlaytest()) {\\\\n        console.log('Damage Formula Error for %1'.format(this._item.name));\\\\n    }\\\\n    return '?????';\\\\n}\\\"\"}","{\"Name:str\":\"FF8\",\"Formula:func\":\"\\\"// Define Constants\\\\nconst user = this.subject();\\\\nconst target = arguments[0];\\\\nconst item = this.item();\\\\nconst a = this.subject();\\\\nconst b = target;\\\\nconst v = $gameVariables._data;\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\n\\\\n// Create Power\\\\nconst power = Math.max(eval(item.damage.formula), 0);\\\\n\\\\n// Declare Damage\\\\nlet Value = 0;\\\\nlet level = Math.max(a.level || a.luk, 1);\\\\nlet armor = this.isPhysical() ? b.def : b.mdf;\\\\narmor = this.applyArmorModifiers(target, armor);\\\\nif (this.isPhysical() && (this.isDamage() || this.isDrain())) {\\\\n    value = a.atk ** 2 / 16 + a.atk;\\\\n    value *= Math.max(265 - armor, 1) / 256;\\\\n    value *= power / 16;\\\\n} else if (this.isMagical() && (this.isDamage() || this.isDrain())) {\\\\n    value = a.mat + power;\\\\n    value *= Math.max(265 - armor, 1) / 4;\\\\n    value *= power / 256;\\\\n} else if (this.isPhysical() && this.isRecover()) {\\\\n    value = (power + a.def) * power / 2;\\\\n} else if (this.isMagical() && this.isRecover()) {\\\\n    value = (power + a.mdf) * power / 2;\\\\n}\\\\n\\\\n// Return Value\\\\nreturn (isNaN(value) ? 0 : value) * sign;\\\"\",\"ItemsEquipsCore\":\"\",\"DamageType\":\"\",\"DamageType1:str\":\"%1 Damage Power\",\"DamageType2:str\":\"%1 Damage Power\",\"DamageType3:str\":\"%1 Recovery Power\",\"DamageType4:str\":\"%1 Recovery Power\",\"DamageType5:str\":\"%1 Drain Power\",\"DamageType6:str\":\"%1 Drain Power\",\"DamageDisplay:func\":\"\\\"// Define Constants\\\\nconst item = this._item;\\\\nconst formula = item.damage.formula;\\\\nconst a = this._tempActorA;\\\\nconst b = this._tempActorB;\\\\nconst user = a;\\\\nconst target = b;\\\\n\\\\n// Return Value\\\\ntry {\\\\n    return formula;\\\\n} catch (e) {\\\\n    if ($gameTemp.isPlaytest()) {\\\\n        console.log('Damage Formula Error for %1'.format(this._item.name));\\\\n    }\\\\n    return '?????';\\\\n}\\\"\"}","{\"Name:str\":\"FF9\",\"Formula:func\":\"\\\"// Define Constants\\\\nconst user = this.subject();\\\\nconst target = arguments[0];\\\\nconst item = this.item();\\\\nconst a = this.subject();\\\\nconst b = target;\\\\nconst v = $gameVariables._data;\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\n\\\\n// Create Damage Constant\\\\nconst power = Math.max(eval(item.damage.formula), 0);\\\\nif (this.isCertainHit()) {\\\\n    return (isNaN(power) ? 0 : power) * sign;\\\\n}\\\\n\\\\n// Declare Main Stats\\\\nlet armor = this.isPhysical() ? b.def : b.mdf;\\\\narmor = this.applyArmorModifiers(b, armor);\\\\nlet stat = 1;\\\\nif (this.isPhysical() && (this.isDamage() || this.isDrain())) {\\\\n    stat = a.atk;\\\\n} else if (this.isMagical() && (this.isDamage() || this.isDrain())) {\\\\n    stat = a.mat;\\\\n} else if (this.isPhysical() && this.isRecover()) {\\\\n    stat = a.def;\\\\n} else if (this.isMagical() && this.isRecover()) {\\\\n    stat = a.mdf;\\\\n}\\\\n\\\\n// Declare Base Damage\\\\nlet baseDamage = power;\\\\nif (this.isPhysical()) {\\\\n    baseDamage += stat;\\\\n}\\\\nif (this.isDamage() || this.isDrain()) {\\\\n    baseDamage -= armor;\\\\n    baseDamage = Math.max(1, baseDamage);\\\\n}\\\\n\\\\n// Declare Bonus Damage\\\\nlet bonusDamage = stat + (((a.level || a.luk) + stat) / 8);\\\\n\\\\n// Declare Final Damage\\\\nlet value = baseDamage * bonusDamage * sign;\\\\n\\\\n// Return Value\\\\nreturn isNaN(value) ? 0 : value;\\\"\",\"ItemsEquipsCore\":\"\",\"DamageType\":\"\",\"DamageType1:str\":\"%1 Damage Power\",\"DamageType2:str\":\"%1 Damage Power\",\"DamageType3:str\":\"%1 Recovery Power\",\"DamageType4:str\":\"%1 Recovery Power\",\"DamageType5:str\":\"%1 Drain Power\",\"DamageType6:str\":\"%1 Drain Power\",\"DamageDisplay:func\":\"\\\"// Define Constants\\\\nconst item = this._item;\\\\nconst formula = item.damage.formula;\\\\nconst a = this._tempActorA;\\\\nconst b = this._tempActorB;\\\\nconst user = a;\\\\nconst target = b;\\\\n\\\\n// Return Value\\\\ntry {\\\\n    return formula;\\\\n} catch (e) {\\\\n    if ($gameTemp.isPlaytest()) {\\\\n        console.log('Damage Formula Error for %1'.format(this._item.name));\\\\n    }\\\\n    return '?????';\\\\n}\\\"\"}","{\"Name:str\":\"FF10\",\"Formula:func\":\"\\\"// Define Constants\\\\nconst user = this.subject();\\\\nconst target = arguments[0];\\\\nconst item = this.item();\\\\nconst a = this.subject();\\\\nconst b = target;\\\\nconst v = $gameVariables._data;\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\n\\\\n// Create Damage Constant\\\\nconst power = Math.max(eval(item.damage.formula), 0);\\\\nif (this.isCertainHit()) {\\\\n    return (isNaN(power) ? 0 : power) * sign;\\\\n}\\\\n\\\\n// Create Damage Offense Value\\\\nlet value = power;\\\\n\\\\nif (this.isPhysical() && (this.isDamage() || this.isDrain())) {\\\\n    value = (((a.atk ** 3) / 32) + 32) * power / 16;\\\\n} else if (this.isMagical() && (this.isDamage() || this.isDrain())) {\\\\n    value = power * ((a.mat ** 2 / 6) + power) / 4;\\\\n} else if (this.isPhysical() && this.isRecover()) {\\\\n    value = power * ((a.def + power) / 2);\\\\n} else if (this.isMagical() && this.isRecover()) {\\\\n    value = power * ((a.mdf + power) / 2);\\\\n}\\\\n\\\\n// Apply Damage Defense Value\\\\nif (this.isDamage() || this.isDrain()) {\\\\n    let armor = this.isPhysical() ? b.def : b.mdf;\\\\n    armor = this.applyArmorModifiers(b, armor);\\\\n    armor = Math.max(armor, 1);\\\\n    value *= ((((armor - 280.4) ** 2) / 110) / 16) / 730;\\\\n    value *= (730 - (armor * 51 - (armor ** 2) / 11) / 10) / 730;\\\\n} else if (this.isRecover()) {\\\\n    value *= -1;\\\\n}\\\\n\\\\n// Return Value\\\\nreturn isNaN(value) ? 0 : value;\\\"\",\"ItemsEquipsCore\":\"\",\"DamageType\":\"\",\"DamageType1:str\":\"%1 Damage Power\",\"DamageType2:str\":\"%1 Damage Power\",\"DamageType3:str\":\"%1 Recovery Power\",\"DamageType4:str\":\"%1 Recovery Power\",\"DamageType5:str\":\"%1 Drain Power\",\"DamageType6:str\":\"%1 Drain Power\",\"DamageDisplay:func\":\"\\\"// Define Constants\\\\nconst item = this._item;\\\\nconst formula = item.damage.formula;\\\\nconst a = this._tempActorA;\\\\nconst b = this._tempActorB;\\\\nconst user = a;\\\\nconst target = b;\\\\n\\\\n// Return Value\\\\ntry {\\\\n    return formula;\\\\n} catch (e) {\\\\n    if ($gameTemp.isPlaytest()) {\\\\n        console.log('Damage Formula Error for %1'.format(this._item.name));\\\\n    }\\\\n    return '?????';\\\\n}\\\"\"}","{\"Name:str\":\"MK\",\"Formula:func\":\"\\\"// Define Constants\\\\nconst user = this.subject();\\\\nconst target = arguments[0];\\\\nconst item = this.item();\\\\nconst a = this.subject();\\\\nconst b = target;\\\\nconst v = $gameVariables._data;\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\n\\\\n// Create Multiplier\\\\nconst multiplier = Math.max(eval(item.damage.formula), 0);\\\\n\\\\n// Declare Values\\\\nlet armor = this.isPhysical() ? b.def : b.mdf;\\\\narmor = this.applyArmorModifiers(target, armor);\\\\nconst denominator = Math.max(200 + armor, 1);\\\\n\\\\n// Calculate Damage \\\\nlet value = 0;\\\\nif (this.isPhysical() && (this.isDamage() || this.isDrain())) {\\\\n    value = 200 * a.atk / denominator;\\\\n} else if (this.isMagical() && (this.isDamage() || this.isDrain())) {\\\\n    value = 200 * a.mat / denominator;\\\\n} else if (this.isPhysical() && this.isRecover()) {\\\\n    value = 200 * a.def / 200;\\\\n} else if (this.isMagical() && this.isRecover()) {\\\\n    value = 200 * a.mdf / 200;\\\\n}\\\\nvalue *= multiplier;\\\\n\\\\n// Return Value\\\\nreturn (isNaN(value) ? 0 : value) * sign;\\\"\",\"ItemsEquipsCore\":\"\",\"DamageType\":\"\",\"DamageType1:str\":\"%1 Damage Multiplier\",\"DamageType2:str\":\"%1 Damage Multiplier\",\"DamageType3:str\":\"%1 Recovery Multiplier\",\"DamageType4:str\":\"%1 Recovery Multiplier\",\"DamageType5:str\":\"%1 Drain Multiplier\",\"DamageType6:str\":\"%1 Drain Multiplier\",\"DamageDisplay:func\":\"\\\"// Define Constants\\\\nconst item = this._item;\\\\nconst formula = item.damage.formula;\\\\nconst a = this._tempActorA;\\\\nconst b = this._tempActorB;\\\\nconst user = a;\\\\nconst target = b;\\\\n\\\\n// Return Value\\\\ntry {\\\\n    const value = Math.max(eval(formula), 0);\\\\n    return '%1%'.format(Math.round(value * 100));\\\\n} catch (e) {\\\\n    if ($gameTemp.isPlaytest()) {\\\\n        console.log('Damage Formula Error for %1'.format(this._item.name));\\\\n    }\\\\n    return '?????';\\\\n}\\\"\"}","{\"Name:str\":\"MOBA\",\"Formula:func\":\"\\\"// Define Constants\\\\nconst user = this.subject();\\\\nconst target = arguments[0];\\\\nconst item = this.item();\\\\nconst a = this.subject();\\\\nconst b = target;\\\\nconst v = $gameVariables._data;\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\n\\\\n// Create Damage Value\\\\nlet value = Math.max(eval(item.damage.formula), 0) * sign;\\\\n\\\\n// Apply Attacker's Offense Parameter\\\\nif (this.isPhysical() && (this.isDamage() || this.isDrain())) {\\\\n    value *= a.atk;\\\\n} else if (this.isMagical() && (this.isDamage() || this.isDrain())) {\\\\n    value *= a.mat;\\\\n} else if (this.isPhysical() && this.isRecover()) {\\\\n    value *= a.def;\\\\n} else if (this.isMagical() && this.isRecover()) {\\\\n    value *= a.mdf;\\\\n}\\\\n\\\\n// Apply Defender's Defense Parameter\\\\nif (this.isDamage() && !this.isCertainHit()) {\\\\n\\\\n    // Calculate Base Armor\\\\n    let armor = this.isPhysical() ? b.def : b.mdf;\\\\n    armor = this.applyArmorModifiers(target, armor);\\\\n\\\\n    // Apply Armor to Damage\\\\n    if (armor >= 0) {\\\\n        value *= 100 / (100 + armor);\\\\n    } else {\\\\n        value *= 2 - (100 / (100 - armor));\\\\n    }\\\\n}\\\\n\\\\n// Return Value\\\\nreturn isNaN(value) ? 0 : value;\\\"\",\"ItemsEquipsCore\":\"\",\"DamageType\":\"\",\"DamageType1:str\":\"%1 Damage Multiplier\",\"DamageType2:str\":\"%1 Damage Multiplier\",\"DamageType3:str\":\"%1 Recovery Multiplier\",\"DamageType4:str\":\"%1 Recovery Multiplier\",\"DamageType5:str\":\"%1 Drain Multiplier\",\"DamageType6:str\":\"%1 Drain Multiplier\",\"DamageDisplay:func\":\"\\\"// Define Constants\\\\nconst item = this._item;\\\\nconst formula = item.damage.formula;\\\\nconst a = this._tempActorA;\\\\nconst b = this._tempActorB;\\\\nconst user = a;\\\\nconst target = b;\\\\n\\\\n// Return Value\\\\ntry {\\\\n    const value = Math.max(eval(formula), 0);\\\\n    return '%1%'.format(Math.round(value * 100));\\\\n} catch (e) {\\\\n    if ($gameTemp.isPlaytest()) {\\\\n        console.log('Damage Formula Error for %1'.format(this._item.name));\\\\n    }\\\\n    return '?????';\\\\n}\\\"\"}","{\"Name:str\":\"PKMN\",\"Formula:func\":\"\\\"// Define Constants\\\\nconst user = this.subject();\\\\nconst target = arguments[0];\\\\nconst item = this.item();\\\\nconst a = this.subject();\\\\nconst b = target;\\\\nconst v = $gameVariables._data;\\\\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\\\\n\\\\n// Create Power\\\\nconst power = Math.max(eval(item.damage.formula), 0);\\\\n\\\\n// Declare Values\\\\nlet value = 0;\\\\nlet level = Math.max(a.level || a.luk, 1);\\\\nlet armor = this.isPhysical() ? b.def : b.mdf;\\\\narmor = Math.max(this.applyArmorModifiers(target, armor), 0);\\\\nlet attackStat = 0;\\\\nif (this.isPhysical() && (this.isDamage() || this.isDrain())) {\\\\n    attackStat = a.atk;\\\\n} else if (this.isMagical() && (this.isDamage() || this.isDrain())) {\\\\n    attackStat =  a.mat;\\\\n} else if (this.isPhysical() && this.isRecover()) {\\\\n    attackStat =  a.def;\\\\n} else if (this.isMagical() && this.isRecover()) {\\\\n    attackStat =  a.mdf;\\\\n}\\\\n\\\\n// Calculate Damage\\\\nvalue = (((((2 * level) / 5) + 2) * power * (attackStat / armor)) / 50) + 2;\\\\n\\\\n// Return Value\\\\nreturn (isNaN(value) ? 0 : value) * sign;\\\"\",\"ItemsEquipsCore\":\"\",\"DamageType\":\"\",\"DamageType1:str\":\"%1 Damage Power\",\"DamageType2:str\":\"%1 Damage Power\",\"DamageType3:str\":\"%1 Recovery Power\",\"DamageType4:str\":\"%1 Recovery Power\",\"DamageType5:str\":\"%1 Drain Power\",\"DamageType6:str\":\"%1 Drain Power\",\"DamageDisplay:func\":\"\\\"// Define Constants\\\\nconst item = this._item;\\\\nconst formula = item.damage.formula;\\\\nconst a = this._tempActorA;\\\\nconst b = this._tempActorB;\\\\nconst user = a;\\\\nconst target = b;\\\\n\\\\n// Return Value\\\\ntry {\\\\n    return formula;\\\\n} catch (e) {\\\\n    if ($gameTemp.isPlaytest()) {\\\\n        console.log('Damage Formula Error for %1'.format(this._item.name));\\\\n    }\\\\n    return '?????';\\\\n}\\\"\"}"]
 *
 */
/* ----------------------------------------------------------------------------
 * Damage Formula Style
 * ----------------------------------------------------------------------------
 */
/*~struct~DamageStyle:
 *
 * @param Name:str
 * @text Name
 * @desc Name of this Damage Style.
 * Used for notetags and such.
 * @default Untitled
 *
 * @param Formula:func
 * @text JS: Formula
 * @parent Name:str
 * @type note
 * @desc The base formula for this Damage Style.
 * @default "// Define Constants\nconst item = this.item();\nconst a = this.subject();\nconst b = target;\nconst sign = [3, 4].includes(item.damage.type) ? -1 : 1;\n\n// Create Damage Value\nlet value = Math.max(eval(item.damage.formula), 0) * sign;\n\n// Return Value\nreturn isNaN(value) ? 0 : value;"
 *
 * @param ItemsEquipsCore
 * @text Items & Equips Core
 *
 * @param DamageType
 * @text Damage Label
 * @parent ItemsEquipsCore
 *
 * @param DamageType1:str
 * @text HP Damage
 * @parent DamageType
 * @desc Vocabulary used for this data entry.
 * @default %1 Damage Multiplier
 *
 * @param DamageType2:str
 * @text MP Damage
 * @parent DamageType
 * @desc Vocabulary used for this data entry.
 * @default %1 Damage Multiplier
 *
 * @param DamageType3:str
 * @text HP Recovery
 * @parent DamageType
 * @desc Vocabulary used for this data entry.
 * @default %1 Recovery Multiplier
 *
 * @param DamageType4:str
 * @text MP Recovery
 * @parent DamageType
 * @desc Vocabulary used for this data entry.
 * @default %1 Recovery Multiplier
 *
 * @param DamageType5:str
 * @text HP Drain
 * @parent DamageType
 * @desc Vocabulary used for this data entry.
 * @default %1 Drain Multiplier
 *
 * @param DamageType6:str
 * @text MP Drain
 * @parent DamageType
 * @desc Vocabulary used for this data entry.
 * @default %1 Drain Multiplier
 *
 * @param DamageDisplay:func
 * @text JS: Damage Display
 * @parent ItemsEquipsCore
 * @type note
 * @desc Code used the data displayed for this category.
 * @default "// Define Constants\nconst item = this._item;\nconst formula = item.damage.formula;\nconst a = this._tempActorA;\nconst b = this._tempActorB;\nconst user = a;\nconst target = b;\n\n// Return Value\ntry {\n    const value = Math.max(eval(formula), 0);\n    return '%1%'.format(Math.round(value * 100));\n} catch (e) {\n    if ($gameTemp.isPlaytest()) {\n        console.log('Damage Formula Error for %1'.format(this._item.name));\n    }\n    return '?????';\n}"
 *
 */
/* ----------------------------------------------------------------------------
 * Mechanics Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Mechanics:
 *
 * @param ActionSpeed
 * @text Action Speed
 *
 * @param AllowRandomSpeed:eval
 * @text Allow Random Speed?
 * @parent ActionSpeed
 * @type boolean
 * @on Allow
 * @off Disable
 * @desc Allow speed to be randomized base off the user's AGI?
 * @default false
 *
 * @param CalcActionSpeedJS:func
 * @text JS: Calculate
 * @parent ActionSpeed
 * @type note
 * @desc Code used to calculate action speed.
 * @default "// Declare Constants\nconst agi = this.subject().agi;\n\n// Create Speed\nlet speed = agi;\nif (this.allowRandomSpeed()) {\n    speed += Math.randomInt(Math.floor(5 + agi / 4));\n}\nif (this.item()) {\n    speed += this.item().speed;\n}\nif (this.isAttack()) {\n    speed += this.subject().attackSpeed();\n}\n\n// Return Speed\nreturn speed;"
 *
 * @param BaseTroop
 * @text Base Troop
 *
 * @param BaseTroopIDs:arraynum
 * @text Base Troop ID's
 * @parent BaseTroop
 * @type troop[]
 * @desc Select the Troop ID(s) to duplicate page events from for all other troops.
 * @default ["1"]
 *
 * @param CommonEvents
 * @text Common Events (on Map)
 *
 * @param BattleStartEvent:num
 * @text Pre-Battle Event
 * @parent CommonEvents
 * @type common_event
 * @desc Common Event to run before each battle on map.
 * Use to 0 to not run any Common Event at all.
 * @default 0
 *
 * @param BattleEndEvent:num
 * @text Post-Battle Event
 * @parent CommonEvents
 * @type common_event
 * @desc Queued Common Event to run after each battle on map.
 * Use to 0 to not run any Common Event at all.
 * @default 0
 *
 * @param VictoryEvent:num
 * @text Victory Event
 * @parent CommonEvents
 * @type common_event
 * @desc Queued Common Event to run upon victory on map.
 * Use to 0 to not run any Common Event at all.
 * @default 0
 *
 * @param DefeatEvent:num
 * @text Defeat Event
 * @parent CommonEvents
 * @type common_event
 * @desc Queued Common Event to run upon defeat on map.
 * Use to 0 to not run any Common Event at all.
 * @default 0
 *
 * @param EscapeSuccessEvent:num
 * @text Escape Success Event
 * @parent CommonEvents
 * @type common_event
 * @desc Queued Common Event to run upon escape success on map.
 * Use to 0 to not run any Common Event at all.
 * @default 0
 *
 * @param EscapeFailEvent:num
 * @text Escape Fail Event
 * @parent CommonEvents
 * @type common_event
 * @desc Queued Common Event to run upon escape failure on map.
 * Use to 0 to not run any Common Event at all.
 * @default 0
 *
 * @param Escape
 *
 * @param CalcEscapeRatioJS:func
 * @text JS: Calc Escape Ratio
 * @parent Escape
 * @type note
 * @desc Code used to calculate the escape success ratio.
 * @default "// Calculate Escape Ratio\nlet ratio = 0.5;\nratio *= $gameParty.agility();\nratio /= $gameTroop.agility();\n\n// Return Ratio\nreturn ratio;"
 *
 * @param CalcEscapeRaiseJS:func
 * @text JS: Calc Escape Raise
 * @parent Escape
 * @type note
 * @desc Code used to calculate how much the escape success ratio raises upon each failure.
 * @default "// Calculate Escape Ratio\nlet value = 0.1;\nvalue += $gameParty.aliveMembers().length;\n\n// Return Value\nreturn value;"
 *
 * @param BattleJS
 * @text JS: Battle-Related
 * 
 * @param PreStartBattleJS:func
 * @text JS: Pre-Start Battle
 * @parent BattleJS
 * @type note
 * @desc Target function: BattleManager.startBattle()
 * JavaScript code occurs before function is run.
 * @default "// Declare Constants\nconst user = this;\nconst target = user;\nconst a = user;\nconst b = user;\n\n// Perform Actions\n"
 *
 * @param PostStartBattleJS:func
 * @text JS: Post-Start Battle
 * @parent BattleJS
 * @type note
 * @desc Target function: BattleManager.startBattle()
 * JavaScript code occurs after function is run.
 * @default "// Declare Constants\nconst user = this;\nconst target = user;\nconst a = user;\nconst b = user;\n\n// Perform Actions\n"
 * 
 * @param BattleVictoryJS:func
 * @text JS: Battle Victory
 * @parent BattleJS
 * @type note
 * @desc Target function: BattleManager.processVictory()
 * JavaScript code occurs before function is run.
 * @default "// Declare Constants\nconst user = this;\nconst target = user;\nconst a = user;\nconst b = user;\n\n// Perform Actions\n"
 *
 * @param EscapeSuccessJS:func
 * @text JS: Escape Success
 * @parent BattleJS
 * @type note
 * @desc Target function: BattleManager.onEscapeSuccess()
 * JavaScript code occurs before function is run.
 * @default "// Declare Constants\nconst user = this;\nconst target = user;\nconst a = user;\nconst b = user;\n\n// Perform Actions\n"
 *
 * @param EscapeFailureJS:func
 * @text JS: Escape Failure
 * @parent BattleJS
 * @type note
 * @desc Target function: BattleManager.onEscapeFailure()
 * JavaScript code occurs before function is run.
 * @default "// Declare Constants\nconst user = this;\nconst target = user;\nconst a = user;\nconst b = user;\n\n// Perform Actions\n"
 * 
 * @param BattleDefeatJS:func
 * @text JS: Battle Defeat
 * @parent BattleJS
 * @type note
 * @desc Target function: BattleManager.processDefeat()
 * JavaScript code occurs before function is run.
 * @default "// Declare Constants\nconst user = this;\nconst target = user;\nconst a = user;\nconst b = user;\n\n// Perform Actions\n"
 * 
 * @param PreEndBattleJS:func
 * @text JS: Pre-End Battle
 * @parent BattleJS
 * @type note
 * @desc Target function: BattleManager.endBattle()
 * JavaScript code occurs before function is run.
 * @default "// Declare Constants\nconst user = this;\nconst target = user;\nconst a = user;\nconst b = user;\n\n// Perform Actions\n"
 *
 * @param PostEndBattleJS:func
 * @text JS: Post-End Battle
 * @parent BattleJS
 * @type note
 * @desc Target function: BattleManager.endBattle()
 * JavaScript code occurs after function is run.
 * @default "// Declare Constants\nconst user = this;\nconst target = user;\nconst a = user;\nconst b = user;\n\n// Perform Actions\n"
 *
 * @param TurnJS
 * @text JS: Turn-Related
 *
 * @param PreStartTurnJS:func
 * @text JS: Pre-Start Turn
 * @parent TurnJS
 * @type note
 * @desc Target function: BattleManager.startTurn()
 * JavaScript code occurs before function is run.
 * @default "// Declare Constants\nconst user = this;\nconst target = user;\nconst a = user;\nconst b = user;\n\n// Perform Actions\n"
 *
 * @param PostStartTurnJS:func
 * @text JS: Post-Start Turn
 * @parent TurnJS
 * @type note
 * @desc Target function: BattleManager.startTurn()
 * JavaScript code occurs after function is run.
 * @default "// Declare Constants\nconst user = this;\nconst target = user;\nconst a = user;\nconst b = user;\n\n// Perform Actions\n"
 *
 * @param PreEndTurnJS:func
 * @text JS: Pre-End Turn
 * @parent TurnJS
 * @type note
 * @desc Target function: Game_Battler.prototype.onTurnEnd()
 * JavaScript code occurs before function is run.
 * @default "// Declare Constants\nconst user = this;\nconst target = user;\nconst a = user;\nconst b = user;\n\n// Perform Actions\n"
 *
 * @param PostEndTurnJS:func
 * @text JS: Post-End Turn
 * @parent TurnJS
 * @type note
 * @desc Target function: Game_Battler.prototype.onTurnEnd()
 * JavaScript code occurs after function is run.
 * @default "// Declare Constants\nconst user = this;\nconst target = user;\nconst a = user;\nconst b = user;\n\n// Perform Actions\n"
 *
 * @param PreRegenerateJS:func
 * @text JS: Pre-Regenerate
 * @parent TurnJS
 * @type note
 * @desc Target function: Game_Battler.prototype.regenerateAll()
 * JavaScript code occurs before function is run.
 * @default "// Declare Constants\nconst user = this;\nconst target = user;\nconst a = user;\nconst b = user;\n\n// Perform Actions\n"
 *
 * @param PostRegenerateJS:func
 * @text JS: Post-Regenerate
 * @parent TurnJS
 * @type note
 * @desc Target function: Game_Battler.prototype.regenerateAll()
 * JavaScript code occurs after function is run.
 * @default "// Declare Constants\nconst user = this;\nconst target = user;\nconst a = user;\nconst b = user;\n\n// Perform Actions\n"
 *
 * @param ActionJS
 * @text JS: Action-Related
 *
 * @param PreStartActionJS:func
 * @text JS: Pre-Start Action
 * @parent ActionJS
 * @type note
 * @desc Target function: BattleManager.startAction()
 * JavaScript code occurs before function is run.
 * @default "// Declare Constants\nconst value = arguments[0];\nconst user = this.subject();\nconst target = user;\nconst a = user;\nconst b = user;\nconst action = this;\nconst item = this.item();\nconst skill = this.item();\n\n// Perform Actions\n"
 *
 * @param PostStartActionJS:func
 * @text JS: Post-Start Action
 * @parent ActionJS
 * @type note
 * @desc Target function: BattleManager.startAction()
 * JavaScript code occurs after function is run.
 * @default "// Declare Constants\nconst value = arguments[0];\nconst user = this.subject();\nconst target = user;\nconst a = user;\nconst b = user;\nconst action = this;\nconst item = this.item();\nconst skill = this.item();\n\n// Perform Actions\n"
 *
 * @param PreApplyJS:func
 * @text JS: Pre-Apply
 * @parent ActionJS
 * @type note
 * @desc Target function: Game_Action.prototype.apply()
 * JavaScript code occurs before function is run.
 * @default "// Declare Constants\nconst value = arguments[0];\nconst target = arguments[1];\nconst user = this.subject();\nconst a = user;\nconst b = target;\nconst action = this;\nconst item = this.item();\nconst skill = this.item();\n\n// Perform Actions\n\n// Return Value\nreturn value;"
 *
 * @param PreDamageJS:func
 * @text JS: Pre-Damage
 * @parent ActionJS
 * @type note
 * @desc Target function: Game_Action.prototype.executeDamage()
 * JavaScript code occurs before function is run.
 * @default "// Declare Constants\nconst value = arguments[0];\nconst target = arguments[1];\nconst user = this.subject();\nconst a = user;\nconst b = target;\nconst action = this;\nconst item = this.item();\nconst skill = this.item();\n\n// Perform Actions\n\n// Return Value\nreturn value;"
 *
 * @param PostDamageJS:func
 * @text JS: Post-Damage
 * @parent ActionJS
 * @type note
 * @desc Target function: Game_Action.prototype.executeDamage()
 * JavaScript code occurs after function is run.
 * @default "// Declare Constants\nconst value = arguments[0];\nconst target = arguments[1];\nconst user = this.subject();\nconst a = user;\nconst b = target;\nconst action = this;\nconst item = this.item();\nconst skill = this.item();\n\n// Perform Actions\n\n// Return Value\nreturn value;"
 *
 * @param PostApplyJS:func
 * @text JS: Post-Apply
 * @parent ActionJS
 * @type note
 * @desc Target function: Game_Action.prototype.apply()
 * JavaScript code occurs after function is run.
 * @default "// Declare Constants\nconst value = arguments[0];\nconst target = arguments[1];\nconst user = this.subject();\nconst a = user;\nconst b = target;\nconst action = this;\nconst item = this.item();\nconst skill = this.item();\n\n// Perform Actions\n\n// Return Value\nreturn value;"
 *
 * @param PreEndActionJS:func
 * @text JS: Pre-End Action
 * @parent ActionJS
 * @type note
 * @desc Target function: BattleManager.endAction()
 * JavaScript code occurs before function is run.
 * @default "// Declare Constants\nconst value = arguments[0];\nconst user = this.subject();\nconst target = user;\nconst a = user;\nconst b = user;\nconst action = this;\nconst item = this.item();\nconst skill = this.item();\n\n// Perform Actions\n"
 *
 * @param PostEndActionJS:func
 * @text JS: Post-End Action
 * @parent ActionJS
 * @type note
 * @desc Target function: BattleManager.endAction()
 * JavaScript code occurs after function is run.
 * @default "// Declare Constants\nconst value = arguments[0];\nconst user = this.subject();\nconst target = user;\nconst a = user;\nconst b = user;\nconst action = this;\nconst item = this.item();\nconst skill = this.item();\n\n// Perform Actions\n"
 *
 */
/* ----------------------------------------------------------------------------
 * Battle Layout Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~BattleLayout:
 *
 * @param Style:str
 * @text Battle Layout Style
 * @type select
 * @option Default - Shows actor faces in Battle Status.
 * @value default
 * @option List - Lists actors in Battle Status.
 * @value list
 * @option XP - Shows actor battlers in a stretched Battle Status.
 * @value xp
 * @option Portrait - Shows portraits in a stretched Battle Status.
 * @value portrait
 * @option Border - Displays windows around the screen border.
 * @value border
 * @desc The style used for the battle layout.
 * @default default
 *
 * @param ListStyle
 * @text List Style
 * @parent Style:str
 *
 * @param ShowFacesListStyle:eval
 * @text Show Faces
 * @parent ListStyle
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Shows faces in List Style?
 * @default true
 *
 * @param CommandWidth:num
 * @text Command Window Width
 * @parent ListStyle
 * @type number
 * @min 1
 * @desc Determine the window width for the Party and Actor Command
 * Windows. Affects Default and List Battle Layout styles.
 * @default 192
 *
 * @param XPStyle
 * @text XP Style
 * @parent Style:str
 *
 * @param XPActorCommandLines:num
 * @text Command Lines
 * @parent XPStyle
 * @type number
 * @min 1
 * @desc Number of action lines in the Actor Command Window for the XP Style.
 * @default 4
 *
 * @param XPActorDefaultHeight:num
 * @text Sprite Height
 * @parent XPStyle
 * @type number
 * @min 1
 * @desc Default sprite height used when if the sprite's height has not been determined yet.
 * @default 64
 *
 * @param XPSpriteYLocation:str
 * @text Sprite Base Location
 * @parent XPStyle
 * @type select
 * @option Above Name - Sprite is located above the name.
 * @value name
 * @option Bottom - Sprite is located at the bottom of the window.
 * @value bottom
 * @option Centered - Sprite is centered in the window.
 * @value center
 * @option Top - Sprite is located at the top of the window.
 * @value top
 * @desc Determine where the sprite is located on the Battle Status Window.
 * @default name
 *
 * @param PotraitStyle
 * @text Portrait Style
 * @parent Style:str
 *
 * @param ShowPortraits:eval
 * @text Show Portraits?
 * @parent PotraitStyle
 * @type boolean
 * @on Portraits
 * @off Faces
 * @desc Requires VisuMZ_1_MainMenuCore.
 * Shows the actor's portrait instead of a face.
 * @default true
 *
 * @param PortraitScale:num
 * @text Portrait Scaling
 * @parent PotraitStyle
 * @desc If portraits are used, scale them by this much.
 * @default 0.5
 *
 * @param BorderStyle
 * @text Border Style
 * @parent Style:str
 *
 * @param SkillItemBorderCols:num
 * @text Columns
 * @parent BorderStyle
 * @type number
 * @min 1
 * @desc The total number of columns for Skill & Item Windows
 * in the battle scene.
 * @default 1
 *
 * @param ShowPortraitsBorderStyle:eval
 * @text Show Portraits?
 * @parent BorderStyle
 * @type boolean
 * @on Portraits
 * @off Faces
 * @desc Requires VisuMZ_1_MainMenuCore.
 * Shows the actor's portrait at the edge of the screen.
 * @default true
 *
 * @param PortraitScaleBorderStyle:num
 * @text Portrait Scaling
 * @parent BorderStyle
 * @desc If portraits are used, scale them by this much.
 * @default 1.0
 *
 * @param SkillItemWindows
 * @text Skill & Item Windows
 *
 * @param SkillItemMiddleLayout:eval
 * @text Middle Layout
 * @parent SkillItemWindows
 * @type boolean
 * @on Middle
 * @off Bottom
 * @desc Shows the Skill & Item Windows in mid-screen?
 * @default false
 *
 * @param SkillItemStandardCols:num
 * @text Columns
 * @parent SkillItemWindows
 * @type number
 * @min 1
 * @desc The total number of columns for Skill & Item Windows
 * in the battle scene.
 * @default 2
 *
 */
/* ----------------------------------------------------------------------------
 * Battle Log Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~BattleLog:
 *
 * @param General
 *
 * @param BackColor:str
 * @text Back Color
 * @parent General
 * @desc Use #rrggbb for a hex color.
 * @default #000000
 *
 * @param MaxLines:num
 * @text Max Lines
 * @parent General
 * @type number
 * @min 1
 * @desc Maximum number of lines to be displayed.
 * @default 10
 *
 * @param MessageWait:num
 * @text Message Wait
 * @parent General
 * @type number
 * @min 1
 * @desc Number of frames for a usual message wait.
 * @default 16
 *
 * @param TextAlign:str
 * @text Text Align
 * @parent General
 * @type combo
 * @option left
 * @option center
 * @option right
 * @desc Text alignment for the Window_BattleLog.
 * @default center
 *
 * @param BattleLogRectJS:func
 * @text JS: X, Y, W, H
 * @parent General
 * @type note
 * @desc Code used to determine the dimensions for the battle log.
 * @default "const wx = 0;\nconst wy = 0;\nconst ww = Graphics.boxWidth;\nconst wh = this.calcWindowHeight(10, false);\nreturn new Rectangle(wx, wy, ww, wh);"
 *
 * @param StartTurn
 * @text Start Turn
 *
 * @param StartTurnShow:eval
 * @text Show Start Turn?
 * @parent StartTurn
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Display turn changes at the start of the turn?
 * @default false
 *
 * @param StartTurnMsg:str
 * @text Start Turn Message
 * @parent StartTurn
 * @desc Message displayed at turn start.
 * %1 - Turn Count
 * @default Turn %1
 *
 * @param StartTurnWait:num
 * @text Start Turn Wait
 * @parent StartTurn
 * @type number
 * @min 1
 * @desc Number of frames to wait after a turn started.
 * @default 40
 *
 * @param DisplayAction
 * @text Display Action
 *
 * @param ActionCenteredName:eval
 * @text Show Centered Action?
 * @parent DisplayAction
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Display a centered text of the action name?
 * @default true
 *
 * @param ActionSkillMsg1:eval
 * @text Show Skill Message 1?
 * @parent DisplayAction
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Display the 1st skill message?
 * @default false
 *
 * @param ActionSkillMsg2:eval
 * @text Show Skill Message 2?
 * @parent DisplayAction
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Display the 2nd skill message?
 * @default true
 *
 * @param ActionItemMsg:eval
 * @text Show Item Message?
 * @parent DisplayAction
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Display the item use message?
 * @default false
 *
 * @param ActionChanges
 * @text Action Changes
 *
 * @param ShowCounter:eval
 * @text Show Counter?
 * @parent ActionChanges
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Display counter text?
 * @default true
 *
 * @param ShowReflect:eval
 * @text Show Reflect?
 * @parent ActionChanges
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Display magic reflection text?
 * @default true
 *
 * @param ShowSubstitute:eval
 * @text Show Substitute?
 * @parent ActionChanges
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Display substitute text?
 * @default true
 *
 * @param ActionResults
 * @text Action Results
 *
 * @param ShowFailure:eval
 * @text Show No Effect?
 * @parent ActionResults
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Display no effect text?
 * @default false
 *
 * @param ShowCritical:eval
 * @text Show Critical?
 * @parent ActionResults
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Display critical text?
 * @default false
 *
 * @param ShowMissEvasion:eval
 * @text Show Miss/Evasion?
 * @parent ActionResults
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Display miss/evasion text?
 * @default false
 *
 * @param ShowHpDmg:eval
 * @text Show HP Damage?
 * @parent ActionResults
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Display HP Damage text?
 * @default false
 *
 * @param ShowMpDmg:eval
 * @text Show MP Damage?
 * @parent ActionResults
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Display MP Damage text?
 * @default false
 *
 * @param ShowTpDmg:eval
 * @text Show TP Damage?
 * @parent ActionResults
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Display TP Damage text?
 * @default false
 *
 * @param DisplayStates
 * @text Display States
 *
 * @param ShowAddedState:eval
 * @text Show Added States?
 * @parent DisplayStates
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Display added states text?
 * @default false
 *
 * @param ShowRemovedState:eval
 * @text Show Removed States?
 * @parent DisplayStates
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Display removed states text?
 * @default false
 *
 * @param ShowCurrentState:eval
 * @text Show Current States?
 * @parent DisplayStates
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Display the currently affected state text?
 * @default false
 *
 * @param ShowAddedBuff:eval
 * @text Show Added Buffs?
 * @parent DisplayStates
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Display added buffs text?
 * @default false
 *
 * @param ShowAddedDebuff:eval
 * @text Show Added Debuffs?
 * @parent DisplayStates
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Display added debuffs text?
 * @default false
 *
 * @param ShowRemovedBuff:eval
 * @text Show Removed Buffs?
 * @parent DisplayStates
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Display removed de/buffs text?
 * @default false
 *
 */
/* ----------------------------------------------------------------------------
 * Battleback Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Battleback:
 *
 * @param DefaultStyle:str
 * @text Default Style
 * @type select
 * @option MZ (MZ's default style)
 * @value MZ
 * @option 1:1 (No Scaling)
 * @value 1:1
 * @option Scale To Fit (Scale to screen size)
 * @value ScaleToFit
 * @option Scale Down (Scale Downward if Larger than Screen)
 * @value ScaleDown
 * @option Scale Up (Scale Upward if Smaller than Screen)
 * @value ScaleUp
 * @desc The default scaling style used for battlebacks.
 * @default MZ
 *
 * @param jsOneForOne:func
 * @text JS: 1:1
 * @type note
 * @desc This code gives you control over the scaling for this style.
 * @default "// Adjust Size\nthis.width = Graphics.width;\nthis.height = Graphics.height;\n\n// Adjust Scale\nconst scale = 1.0;\nthis.scale.x = scale;\nthis.scale.y = scale;\n\n// Adjust Coordinates\nthis.x = 0;\nthis.y = 0;"
 *
 * @param jsScaleToFit:func
 * @text JS: Scale To Fit
 * @type note
 * @desc This code gives you control over the scaling for this style.
 * @default "// Adjust Size\nthis.width = Graphics.width;\nthis.height = Graphics.height;\n\n// Adjust Scale\nconst ratioX = this.width / this.bitmap.width;\nconst ratioY = this.height / this.bitmap.height;\nconst scale = Math.max(ratioX, ratioY);\nthis.scale.x = scale;\nthis.scale.y = scale;\n\n// Adjust Coordinates\nthis.x = (Graphics.width - this.width) / 2;\nthis.y = Graphics.height - this.height;"
 *
 * @param jsScaleDown:func
 * @text JS: Scale Down
 * @type note
 * @desc This code gives you control over the scaling for this style.
 * @default "// Adjust Size\nthis.width = Graphics.width;\nthis.height = Graphics.height;\n\n// Adjust Scale\nconst ratioX = Math.min(1, this.width / this.bitmap.width);\nconst ratioY = Math.min(1, this.height / this.bitmap.height);\nconst scale = Math.max(ratioX, ratioY);\nthis.scale.x = scale;\nthis.scale.y = scale;\n\n// Adjust Coordinates\nthis.x = (Graphics.width - this.width) / 2;\nthis.y = Graphics.height - this.height;"
 *
 * @param jsScale Up:func
 * @text JS: Scale Up
 * @type note
 * @desc This code gives you control over the scaling for this style.
 * @default "// Adjust Size\nthis.width = Graphics.width;\nthis.height = Graphics.height;\n\n// Adjust Scale\nconst ratioX = Math.max(1, this.width / this.bitmap.width);\nconst ratioY = Math.max(1, this.height / this.bitmap.height);\nconst scale = Math.max(ratioX, ratioY);\nthis.scale.x = scale;\nthis.scale.y = scale;\n\n// Adjust Coordinates\nthis.x = (Graphics.width - this.width) / 2;\nthis.y = Graphics.height - this.height;"
 *
 */
/* ----------------------------------------------------------------------------
 * Party Command Window Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~PartyCmd:
 *
 * @param Cmd
 * @text Command Window
 *
 * @param CmdStyle:str
 * @text Style
 * @parent Cmd
 * @type select
 * @option Text Only
 * @value text
 * @option Icon Only
 * @value icon
 * @option Icon + Text
 * @value iconText
 * @option Automatic
 * @value auto
 * @desc How do you wish to draw commands in the Party Command Window?
 * @default auto
 *
 * @param CmdTextAlign:str
 * @text Text Align
 * @parent Cmd
 * @type combo
 * @option left
 * @option center
 * @option right
 * @desc Text alignment for the Party Command Window.
 * @default left
 *
 * @param CmdIconFight:num
 * @text Fight Icon
 * @parent Cmd
 * @desc The icon used for the Fight command.
 * @default 76
 *
 * @param CommandAddAutoBattle:eval
 * @text Add Auto Battle?
 * @parent Cmd
 * @type boolean
 * @on Add
 * @off Don't
 * @desc Add the "Auto Battle" command to the Command Window?
 * @default true
 *
 * @param CmdIconAutoBattle:num
 * @text Auto Battle Icon
 * @parent CommandAddAutoBattle:eval
 * @desc The icon used for the Auto Battle command.
 * @default 78
 *
 * @param CmdTextAutoBattle:str
 * @text Auto Battle Text
 * @parent CommandAddAutoBattle:eval
 * @desc The text used for the Auto Battle command.
 * @default Auto
 *
 * @param CommandAddOptions:eval
 * @text Add Options?
 * @parent Cmd
 * @type boolean
 * @on Add
 * @off Don't
 * @desc Add the "Options" command to the Command Window?
 * @default true
 *
 * @param CmdIconOptions:num
 * @text Options Icon
 * @parent CommandAddOptions:eval
 * @desc The icon used for the Options command.
 * @default 83
 *
 * @param ActiveTpbOptionsMessage:str
 * @text Active TPB Message
 * @parent CommandAddOptions:eval
 * @desc Message that will be displayed when selecting options during the middle of an action.
 * @default Options Menu queued after action is complete.
 *
 * @param CmdIconEscape:num
 * @text Escape Icon
 * @parent Cmd
 * @desc The icon used for the Escape command.
 * @default 82
 *
 * @param Access
 *
 * @param SkipPartyCmd:eval
 * @text Skip Party Command
 * @parent Access
 * @type boolean
 * @on Skip
 * @off Don't
 * @desc DTB: Skip Party Command selection on turn start.
 * TPB: Skip Party Command selection at battle start.
 * @default true
 *
 * @param DisablePartyCmd:eval
 * @text Disable Party Command
 * @parent Access
 * @type boolean
 * @on Disable
 * @off Don't
 * @desc Disable the Party Command Window entirely?
 * @default false
 *
 * @param HelpWindow
 * @text Help Window
 *
 * @param HelpFight:str
 * @text Fight
 * @parent HelpWindow
 * @desc Text displayed when selecting a skill type.
 * %1 - Skill Type Name
 * @default Select actions to fight.
 *
 * @param HelpAutoBattle:str
 * @text Auto Battle
 * @parent HelpWindow
 * @desc Text displayed when selecting the Auto Battle command.
 * @default Sets party to Auto Battle mode.
 *
 * @param HelpOptions:str
 * @text Options
 * @parent HelpWindow
 * @desc Text displayed when selecting the Options command.
 * @default Opens up the Options Menu.
 *
 * @param HelpEscape:str
 * @text Escape
 * @parent HelpWindow
 * @desc Text displayed when selecting the escape command.
 * @default Attempt to escape the battle.
 *
 */
/* ----------------------------------------------------------------------------
 * Actor Command Window Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~ActorCmd:
 *
 * @param Cmd
 * @text Command Window
 *
 * @param CmdStyle:str
 * @text Style
 * @parent Cmd
 * @type select
 * @option Text Only
 * @value text
 * @option Icon Only
 * @value icon
 * @option Icon + Text
 * @value iconText
 * @option Automatic
 * @value auto
 * @desc How do you wish to draw commands in the Actor Command Window?
 * @default auto
 *
 * @param CmdTextAlign:str
 * @text Text Align
 * @parent Cmd
 * @type combo
 * @option left
 * @option center
 * @option right
 * @desc Text alignment for the Actor Command Window.
 * @default left
 *
 * @param CmdIconItem:num
 * @text Item Icon
 * @parent Cmd
 * @desc The icon used for the Item command.
 * @default 176
 *
 * @param IconStypeNorm:num
 * @text Normal SType Icon
 * @parent Cmd
 * @desc Icon used for normal skill types that aren't assigned any
 * icons. Ignore if VisuMZ_1_SkillsStatesCore is installed.
 * @default 78
 *
 * @param IconStypeMagic:num
 * @text Magic SType Icon
 * @parent Cmd
 * @desc Icon used for magic skill types that aren't assigned any
 * icons. Ignore if VisuMZ_1_SkillsStatesCore is installed.
 * @default 79
 *
 * @param BattleCmd
 * @text Battle Commands
 *
 * @param BattleCmdList:arraystr
 * @text Command List
 * @parent BattleCmd
 * @type combo[]
 * @option attack
 * @option skills
 * @option guard
 * @option item
 * @option party
 * @option escape
 * @option auto battle
 * @option stypes
 * @option stype: x
 * @option stype: name
 * @option all skills
 * @option skill: x
 * @option skill: name
 * @option combat log
 * @desc List of battle commands that appear by default
 * if the <Battle Commands> notetag isn't present.
 * @default ["attack","skills","guard","party","item"]
 *
 * @param HelpWindow
 * @text Help Window
 *
 * @param HelpSkillType:str
 * @text Skill Types
 * @parent HelpWindow
 * @desc Text displayed when selecting a skill type.
 * %1 - Skill Type Name
 * @default Opens up a list of skills under the \C[16]%1\C[0] category.
 *
 * @param HelpItem:str
 * @text Items
 * @parent HelpWindow
 * @desc Text displayed when selecting the item command.
 * @default Opens up a list of items that you can use.
 *
 * @param HelpEscape:str
 * @text Escape
 * @parent HelpWindow
 * @desc Text displayed when selecting the escape command.
 * @default Attempt to escape the battle.
 *
 * @param HelpAutoBattle:str
 * @text Auto Battle
 * @parent HelpWindow
 * @desc Text displayed when selecting the Auto Battle command.
 * @default Automatically choose an action suitable for combat.
 *
 */
/* ----------------------------------------------------------------------------
 * Actor Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Actor:
 *
 * @param Flinch
 *
 * @param FlinchDistanceX:num
 * @text Flinch Distance X
 * @parent Flinch
 * @desc The normal X distance when flinching.
 * @default 12
 *
 * @param FlinchDistanceY:num
 * @text Flinch Distance Y
 * @parent Flinch
 * @desc The normal Y distance when flinching.
 * @default 0
 *
 * @param FlinchDuration:num
 * @text Flinch Duration
 * @parent Flinch
 * @desc The number of frames for a flinch to complete.
 * @default 6
 *
 * @param SvBattlers
 * @text Sideview Battlers
 *
 * @param AnchorX:num
 * @text Anchor: X
 * @parent SvBattlers
 * @desc Default X anchor for Sideview Battlers.
 * Use values between 0 and 1 to be safe.
 * @default 0.5
 *
 * @param AnchorY:num
 * @text Anchor: Y
 * @parent SvBattlers
 * @desc Default Y anchor for Sideview Battlers.
 * Use values between 0 and 1 to be safe.
 * @default 1.0
 *
 * @param ChantStyle:eval
 * @text Chant Style
 * @parent SvBattlers
 * @type boolean
 * @on Magical Hit Type
 * @off Magical Skill Type
 * @desc What determines the chant motion?
 * Hit type or skill type?
 * @default true
 *
 * @param OffsetX:num
 * @text Offset: X
 * @parent SvBattlers
 * @desc Offsets X position where actor is positioned.
 * Negative values go left. Positive values go right.
 * @default 0
 *
 * @param OffsetY:num
 * @text Offset: Y
 * @parent SvBattlers
 * @desc Offsets Y position where actor is positioned.
 * Negative values go up. Positive values go down.
 * @default 0
 *
 * @param MotionSpeed:num
 * @text Motion Speed
 * @parent SvBattlers
 * @type number
 * @min 1
 * @desc The number of frames in between each motion.
 * @default 12
 *
 * @param PrioritySortActive:eval
 * @text Priority: Active
 * @parent SvBattlers
 * @type boolean
 * @on Active Actor over All Else
 * @off Active Actor is Sorted Normally
 * @desc Place the active actor on top of actor and enemy sprites.
 * @default false
 *
 * @param PrioritySortActors:eval
 * @text Priority: Actors
 * @parent SvBattlers
 * @type boolean
 * @on Actors over Enemies
 * @off Sort by Y Position
 * @desc Prioritize actors over enemies when placing sprites on top
 * of each other.
 * @default true
 *
 * @param Shadow:eval
 * @text Shadow Visible
 * @parent SvBattlers
 * @type boolean
 * @on Visible
 * @off Hidden
 * @desc Show or hide the shadow for Sideview Battlers.
 * @default true
 *
 * @param SmoothImage:eval
 * @text Smooth Image
 * @parent SvBattlers
 * @type boolean
 * @on Smooth
 * @off Pixelated
 * @desc Smooth out the battler images or pixelate them?
 * @default false
 *
 * @param HomePosJS:func
 * @text JS: Home Position
 * @parent SvBattlers
 * @type note
 * @desc Code used to calculate the home position of actors.
 * @default "// Declare Constants\nconst sprite = this;\nconst actor = this._actor;\nconst index = arguments[0];\n\n// Make Calculations\nlet x = Math.round((Graphics.width / 2) + 192)\nx -= Math.floor((Graphics.width - Graphics.boxWidth) / 2);\nx += index * 32;\nlet y = (Graphics.height - 200) - ($gameParty.maxBattleMembers() * 48);\ny -= Math.floor((Graphics.height - Graphics.boxHeight) / 2);\ny += index * 48;\n\n// Home Position Offsets\nconst offsetNote = /<SIDEVIEW HOME OFFSET:[ ]([\\+\\-]\\d+),[ ]([\\+\\-]\\d+)>/i;\nconst xOffsets = actor.traitObjects().map((obj) => (obj && obj.note.match(offsetNote) ? Number(RegExp.$1) : 0));\nconst yOffsets = actor.traitObjects().map((obj) => (obj && obj.note.match(offsetNote) ? Number(RegExp.$2) : 0));\nx = xOffsets.reduce((r, offset) => r + offset, x);\ny = yOffsets.reduce((r, offset) => r + offset, y);\n\n// Set Home Position\nthis.setHome(x, y);"
 *
 */
/* ----------------------------------------------------------------------------
 * Enemy Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Enemy:
 *
 * @param Visual
 *
 * @param AttackAnimation:num
 * @text Attack Animation
 * @parent Visual
 * @type animation
 * @desc Default attack animation used for enemies.
 * Use <Attack Animation: x> for custom animations.
 * @default 1
 *
 * @param EmergeText:eval
 * @text Emerge Text
 * @parent Visual
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show or hide the 'Enemy emerges!' text at the start of battle.
 * @default false
 *
 * @param OffsetX:num
 * @text Offset: X
 * @parent Visual
 * @desc Offsets X position where enemy is positioned.
 * Negative values go left. Positive values go right.
 * @default 0
 *
 * @param OffsetY:num
 * @text Offset: Y
 * @parent Visual
 * @desc Offsets Y position where enemy is positioned.
 * Negative values go up. Positive values go down.
 * @default 0
 *
 * @param SmoothImage:eval
 * @text Smooth Image
 * @parent Visual
 * @type boolean
 * @on Smooth
 * @off Pixelated
 * @desc Smooth out the battler images or pixelate them?
 * @default true
 *
 * @param SelectWindow
 * @text Select Window
 *
 * @param LastSelected:eval
 * @text Any: Last Selected
 * @parent SelectWindow
 * @type boolean
 * @on Last Selected
 * @off FV/SV Priority
 * @desc Prioritize last selected enemy over front view or sideview settings?
 * @default true
 *
 * @param FrontViewSelect:eval
 * @text FV: Right Priority
 * @parent SelectWindow
 * @type boolean
 * @on Right
 * @off Normal
 * @desc If using frontview, auto select the enemy furthest right.
 * @default false
 *
 * @param SideviewSelect:eval
 * @text SV: Right Priority
 * @parent SelectWindow
 * @type boolean
 * @on Right
 * @off Normal
 * @desc If using sideview, auto select the enemy furthest right.
 * @default true
 *
 * @param NameFontSize:num
 * @text Name: Font Size
 * @parent SelectWindow
 * @desc Font size used for enemy names.
 * @default 22
 *
 * @param NameOffsetX:num
 * @text Name: Offset X
 * @parent SelectWindow
 * @desc Offset the enemy name's X position by this much.
 * @default 0
 *
 * @param NameOffsetY:num
 * @text Name: Offset Y
 * @parent SelectWindow
 * @desc Offset the enemy name's Y position by this much.
 * @default 0
 *
 * @param SvBattlers
 * @text Sideview Battlers
 *
 * @param AllowCollapse:eval
 * @text Allow Collapse
 * @parent SvBattlers
 * @type boolean
 * @on Allow
 * @off Don't
 * @desc Causes defeated enemies with SV Battler graphics
 * to "fade away" when defeated?
 * @default false
 *
 * @param AnchorX:num
 * @text Anchor: X
 * @parent SvBattlers
 * @desc Default X anchor for Sideview Battlers.
 * Use values between 0 and 1 to be safe.
 * @default 0.5
 *
 * @param AnchorY:num
 * @text Anchor: Y
 * @parent SvBattlers
 * @desc Default Y anchor for Sideview Battlers.
 * Use values between 0 and 1 to be safe.
 * @default 1.0
 *
 * @param MotionIdle:str
 * @text Motion: Idle
 * @parent SvBattlers
 * @type combo
 * @option walk
 * @option wait
 * @option chant
 * @option guard
 * @option damage
 * @option evade
 * @option thrust
 * @option swing
 * @option missile
 * @option skill
 * @option spell
 * @option item
 * @option escape
 * @option victory
 * @option dying
 * @option abnormal
 * @option sleep
 * @option dead
 * @desc Sets default idle animation used by Sideview Battlers.
 * @default walk
 *
 * @param Shadow:eval
 * @text Shadow Visible
 * @parent SvBattlers
 * @type boolean
 * @on Visible
 * @off Hidden
 * @desc Show or hide the shadow for Sideview Battlers.
 * @default true
 *
 * @param Width:num
 * @text Size: Width
 * @parent SvBattlers
 * @type number
 * @min 1
 * @desc Default width for enemies that use Sideview Battlers.
 * @default 64
 *
 * @param Height:num
 * @text Size: Height
 * @parent SvBattlers
 * @type number
 * @min 1
 * @desc Default height for enemies that use Sideview Battlers.
 * @default 64
 *
 * @param WtypeId:num
 * @text Weapon Type
 * @parent SvBattlers
 * @type number
 * @min 0
 * @desc Sets default weapon type used by Sideview Battlers.
 * Use 0 for Bare Hands.
 * @default 0
 *
 */
/* ----------------------------------------------------------------------------
 * HP Gauge Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~HpGauge:
 *
 * @param Display
 * @text Show Gauges For
 *
 * @param ShowActorGauge:eval
 * @text Actors
 * @parent Display
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show HP Gauges over the actor sprites' heads?
 * Requires SV Actors to be visible.
 * @default true
 *
 * @param ShowEnemyGauge:eval
 * @text Enemies
 * @parent Display
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show HP Gauges over the enemy sprites' heads?
 * Can be bypassed with <Hide HP Gauge> notetag.
 * @default true
 *
 * @param RequiresDefeat:eval
 * @text Requires Defeat?
 * @parent ShowEnemyGauge:eval
 * @type boolean
 * @on Require Defeat First
 * @off No Requirement
 * @desc Requires defeating the enemy once to show HP Gauge?
 * Can be bypassed with <Show HP Gauge> notetag.
 * @default true
 *
 * @param BTestBypass:eval
 * @text Battle Test Bypass?
 * @parent RequiresDefeat:eval
 * @type boolean
 * @on Bypass
 * @off Don't Bypass
 * @desc Bypass the defeat requirement in battle test?
 * @default true
 *
 * @param Settings
 *
 * @param AnchorX:num
 * @text Anchor X
 * @parent Settings
 * @desc Where do you want the HP Gauge sprite's anchor X to be?
 * Use values between 0 and 1 to be safe.
 * @default 0.5
 *
 * @param AnchorY:num
 * @text Anchor Y
 * @parent Settings
 * @desc Where do you want the HP Gauge sprite's anchor Y to be?
 * Use values between 0 and 1 to be safe.
 * @default 1.0
 *
 * @param Scale:num
 * @text Scale
 * @parent Settings
 * @desc How large/small do you want the HP Gauge to be scaled?
 * @default 0.5
 *
 * @param OffsetX:num
 * @text Offset X
 * @parent Settings
 * @desc How many pixels to offset the HP Gauge's X by?
 * @default 0
 *
 * @param OffsetY:num
 * @text Offset Y
 * @parent Settings
 * @desc How many pixels to offset the HP Gauge's Y by?
 * @default -3
 *
 * @param Options
 * @text Options
 *
 * @param AddHpGaugeOption:eval
 * @text Add Option?
 * @parent Options
 * @type boolean
 * @on Add
 * @off Don't Add
 * @desc Add the 'Show HP Gauge' option to the Options menu?
 * @default true
 *
 * @param AdjustRect:eval
 * @text Adjust Window Height
 * @parent Options
 * @type boolean
 * @on Adjust
 * @off Don't
 * @desc Automatically adjust the options window height?
 * @default true
 *
 * @param Name:str
 * @text Option Name
 * @parent Options
 * @desc Command name of the option.
 * @default Show HP Gauge
 *
 */
/* ----------------------------------------------------------------------------
 * Action Sequence Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~ActionSequence:
 *
 * @param AutoSequences
 * @text Automatic Sequences
 *
 * @param AutoMeleeSolo:eval
 * @text Melee Single Target
 * @parent AutoSequences
 * @type boolean
 * @on Allow
 * @off Ignore
 * @desc Allow this auto sequence for physical, single target actions?
 * @default true
 *
 * @param AutoMeleeAoE:eval
 * @text Melee Multi Target
 * @parent AutoSequences
 * @type boolean
 * @on Allow
 * @off Ignore
 * @desc Allow this auto sequence for physical, multi-target actions?
 * @default true
 *
 * @param QoL
 * @text Quality of Life
 *
 * @param AutoNotetag:eval
 * @text Auto Notetag
 * @parent QoL
 * @type boolean
 * @on Automatic
 * @off Manual
 * @desc Automatically apply the <Custom Action Sequence> notetag
 * effect to any item or skill that has a Common Event?
 * @default false
 *
 * @param CastAnimations
 * @text Cast Animations
 *
 * @param CastCertain:num
 * @text Certain Hit
 * @parent CastAnimations
 * @type animation
 * @desc Cast animation for Certain Hit skills.
 * @default 120
 *
 * @param CastPhysical:num
 * @text Physical
 * @parent CastAnimations
 * @type animation
 * @desc Cast animation for Physical skills.
 * @default 52
 *
 * @param CastMagical:num
 * @text Magical
 * @parent CastAnimations
 * @type animation
 * @desc Cast animation for Magical skills.
 * @default 51
 *
 * @param CounterReflection
 * @text Counter/Reflect
 *
 * @param CounterPlayback:eval
 * @text Counter Back
 * @parent CounterReflection
 * @type boolean
 * @on Play Back
 * @off Ignore
 * @desc Play back the attack animation used?
 * @default true
 *
 * @param ReflectAnimation:num
 * @text Reflect Animation
 * @parent CounterReflection
 * @type animation
 * @desc Animation played when an action is reflected.
 * @default 1
 *
 * @param ReflectPlayback:eval
 * @text Reflect Back
 * @parent CounterReflection
 * @type boolean
 * @on Play Back
 * @off Ignore
 * @desc Play back the attack animation used?
 * @default true
 *
 * @param Stepping
 *
 * @param MeleeDistance:num
 * @text Melee Distance
 * @parent Stepping
 * @desc Minimum distance in pixels for Movement Action Sequences.
 * @default 24
 *
 * @param StepDistanceX:num
 * @text Step Distance X
 * @parent Stepping
 * @desc The normal X distance when stepping forward.
 * @default 48
 *
 * @param StepDistanceY:num
 * @text Step Distance Y
 * @parent Stepping
 * @desc The normal Y distance when stepping forward.
 * @default 0
 *
 * @param StepDuration:num
 * @text Step Duration
 * @parent Stepping
 * @desc The number of frames for a stepping action to complete.
 * @default 12
 *
 */
/* ----------------------------------------------------------------------------
 * Projectile Start Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~ProjectileStart:
 * 
 * @param Type:str
 * @text Type
 * @type select
 * @option Target - Start from battler target(s)
 * @value target
 * @option Point - Start from a point on the screen
 * @value point
 * @desc Select where the projectile should start from.
 * @default target
 * 
 * @param Targets:arraystr
 * @text Target(s)
 * @parent Type:str
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) to start the projectile from.
 * @default ["user"]
 * 
 * @param TargetCenter:eval
 * @text Centralize
 * @parent Targets:arraystr
 * @type boolean
 * @on Center Projectile
 * @off Create Each
 * @desc Create one projectile at the center of the targets?
 * Or create a projectile for each target?
 * @default false
 * 
 * @param PointX:eval
 * @text Point X
 * @parent Type:str
 * @desc Insert the X coordinate to start the projectile at.
 * You may use JavaScript code.
 * @default Graphics.width / 2
 * 
 * @param PointY:eval
 * @text Point Y
 * @parent Type:str
 * @desc Insert the Y coordinate to start the projectile at.
 * You may use JavaScript code.
 * @default Graphics.height / 2
 * 
 * @param OffsetX:eval
 * @text Offset X
 * @desc Insert how many pixels to offset the X coordinate by.
 * You may use JavaScript code.
 * @default +0
 * 
 * @param OffsetY:eval
 * @text Offset Y
 * @desc Insert how many pixels to offset the Y coordinate by.
 * You may use JavaScript code.
 * @default +0
 *
 */
/* ----------------------------------------------------------------------------
 * Projectile Goal Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~ProjectileGoal:
 * 
 * @param Type:str
 * @text Type
 * @type select
 * @option Target - Goal is battler target(s)
 * @value target
 * @option Point - Goal is a point on the screen
 * @value point
 * @desc Select where the projectile should go to.
 * @default target
 * 
 * @param Targets:arraystr
 * @text Target(s)
 * @parent Type:str
 * @type combo[]
 * @option user
 * @option current target
 * @option prev target
 * @option next target
 * @option all targets
 * @option focus
 * @option not focus
 * @option 
 * @option alive friends
 * @option alive friends not user
 * @option alive friends not target
 * @option dead friends
 * @option friend index x
 * @option 
 * @option alive opponents
 * @option alive opponents not target
 * @option dead opponents
 * @option opponent index x
 * @option 
 * @option alive actors
 * @option alive actors not user
 * @option alive actors not target
 * @option dead actors
 * @option actor index x
 * @option actor ID x
 * @option 
 * @option alive enemies
 * @option alive enemies not user
 * @option alive enemies not target
 * @option dead enemies
 * @option enemy index x
 * @option enemy ID x
 * @option 
 * @option alive battlers
 * @option alive battlers not user
 * @option alive battlers not target
 * @option dead battlers
 * @option 
 * @desc Select which unit(s) for projectile to go to.
 * @default ["all targets"]
 * 
 * @param TargetCenter:eval
 * @text Centralize
 * @parent Targets:arraystr
 * @type boolean
 * @on Center Projectile
 * @off Create Each
 * @desc Set goal in the center of targets?
 * Or create a projectile to go to each target?
 * @default false
 * 
 * @param PointX:eval
 * @text Point X
 * @parent Type:str
 * @desc Insert the X coordinate to send the projectile to.
 * You may use JavaScript code.
 * @default Graphics.width / 2
 * 
 * @param PointY:eval
 * @text Point Y
 * @parent Type:str
 * @desc Insert the Y coordinate to send the projectile to.
 * You may use JavaScript code.
 * @default Graphics.height / 2
 * 
 * @param OffsetX:eval
 * @text Offset X
 * @desc Insert how many pixels to offset the X coordinate by.
 * You may use JavaScript code.
 * @default +0
 * 
 * @param OffsetY:eval
 * @text Offset Y
 * @desc Insert how many pixels to offset the Y coordinate by.
 * You may use JavaScript code.
 * @default +0
 *
 */
/* ----------------------------------------------------------------------------
 * Projectile Extra Animation Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~ProjectileExAni:
 * 
 * @param AutoAngle:eval
 * @text Auto Angle?
 * @parent Settings
 * @type boolean
 * @on Automatically Angle
 * @off Normal
 * @desc Automatically angle the projectile to tilt the direction it's moving?
 * @default true
 * 
 * @param AngleOffset:eval
 * @text Angle Offset
 * @desc Alter the projectile's tilt by this many degrees.
 * @default +0
 * 
 * @param Arc:eval
 * @text Arc Peak
 * @parent Settings
 * @desc This is the height of the project's trajectory arc
 * in pixels.
 * @default 0
 *
 * @param EasingType:str
 * @text Easing
 * @parent Settings
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type to apply to the projectile's trajectory.
 * @default Linear
 * 
 * @param Spin:eval
 * @text Spin Speed
 * @parent Settings
 * @desc Determine how much angle the projectile spins per frame.
 * Does not work well with "Auto Angle".
 * @default +0.0
 *
 */
/* ----------------------------------------------------------------------------
 * Projectile Extra Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~ProjectileExtra:
 * 
 * @param AutoAngle:eval
 * @text Auto Angle?
 * @parent Settings
 * @type boolean
 * @on Automatically Angle
 * @off Normal
 * @desc Automatically angle the projectile to tilt the direction it's moving?
 * @default true
 * 
 * @param AngleOffset:eval
 * @text Angle Offset
 * @desc Alter the projectile's tilt by this many degrees.
 * @default +0
 * 
 * @param Arc:eval
 * @text Arc Peak
 * @parent Settings
 * @desc This is the height of the project's trajectory arc
 * in pixels.
 * @default 0
 *
 * @param BlendMode:num
 * @text Blend Mode
 * @type select
 * @option 0 - Normal
 * @value 0
 * @option 1 - Additive
 * @value 1
 * @option 2 - Multiply
 * @value 2
 * @option 3 - Screen
 * @value 3
 * @desc What kind of blend mode do you wish to apply to the projectile?
 * @default 0
 *
 * @param EasingType:str
 * @text Easing
 * @parent Settings
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type to apply to the projectile's trajectory.
 * @default Linear
 * 
 * @param Hue:eval
 * @text Hue
 * @parent Settings
 * @desc Adjust the hue of the projectile.
 * Insert a number between 0 and 360.
 * @default 0
 * 
 * @param Scale:eval
 * @text Scale
 * @parent Settings
 * @desc Adjust the size scaling of the projectile.
 * Use decimals for exact control.
 * @default 1.0
 * 
 * @param Spin:eval
 * @text Spin Speed
 * @parent Settings
 * @desc Determine how much angle the projectile spins per frame.
 * Does not work well with "Auto Angle".
 * @default +0.0
 *
 */
//=============================================================================

const _0x4aef=['makeHpDamageText','Variable','updateBattlebackBitmap1','drawItemImage','Mirror','COMBAT\x20LOG','reverse','constructor','_battleCoreForcedElements','statusWindowRectXPStyle','loadBitmap','setMoveEasingType','logWindowRect','refreshCursor','SvWeaponMass-%1-%2','gainTp','walk','ARRAYJSON','ActionEffect','BattleManager_isTpbMainPhase','WaitCount2','Sprite_Enemy_updateCollapse','maxItems','%1EndActionJS','ActSeq_BattleLog_DisplayAction','onJumpEnd','_svBattlerData','currentExt','isFrameVisible','remove','_currentAngle','setWaitMode','battleGrow','isJumping','clone','TP_Flat','svAnchorX','terminate','callOkHandler','isAlive','removeAnimationFromContainer','_angleRevertOnFinish','displayCritical','concat','checkTpbInputOpen','zoomDuration','skills','119047wEXjUI','_interpreter','gaugeX','_updateClientArea','_list','setBattlerMotionTrailData','some','dimColor1','displayReflection','startJump','setBattleAngle','applyGuard','setHelpWindowItem','Skills','_targets','FocusY','canUseItemCommand','_weaponSprite','BattleVictoryJS','ActSeq_Weapon_ClearActiveWeapon','PopupPosition','attackSkillId','performCollapse','PortraitScaleBorderStyle','PreEndBattleJS','currentValue','_flashDuration','arPenRate','createPartyCommandWindow','isEscapeCommandEnabled','Game_Temp_requestAnimation','statusWindowRectBorderStyle','AllowRandomSpeed','PostStartTurnJS','motionIdle','boxHeight','PerformAction','_baseLineStack','OverallFormulaJS','Shadow2','MotionType','ARRAYSTR','skillTypes','addActor','isDebuffAffected','updateShadowVisibility','CriticalDmgRate','onEnemyOk','AdjustRect','setupBattleCore','setupMotion','_immortal','isCertainHit','makeActionOrders','loadSvEnemy','addState','_shadowSprite','anchor','VisuMZ_3_ActSeqProjectiles','_baseY','requestRefresh','Game_Interpreter_terminate','addSingleSkillCommand','max','SkipPartyCmd','autoBattleWindowRect','AnchorY','ActSeq_Motion_PerformAction','OffsetX','Battleback','Armor-%1-%2','innerHeight','Game_Battler_performMiss','clearWeaponAnimation','battleOpacity','ActSeq_Mechanics_ActionEffect','StartTurnWait','svAnchorY','randomTargets','exit','VisuMZ_2_HorrorEffects','text','autoMeleeMultiTargetActionSet','onActorCancel','TextAlign','VisuMZ_3_ActSeqCamera','VisuMZ_3_ActSeqImpact','clearResult','updatePositionBattleCore','STRUCT','isPreviousSceneBattleTransitionable','DistanceX','isMeleeSingleTargetAction','deadMembers','_callSceneOptions','_endBattle','performReflection','isShownOnBattlePortrait','ActSeq_Camera_Offset','hitRate','_mainSprite','battlelog','ParseClassNotetags','NameFontSize','Scene_Battle_startActorCommandSelection','border','ActSeq_Impact_MotionTrailCreate','loadPicture','Scene_Battle_updateBattleProcess','performWeaponAnimation','iconText','AutoMeleeAoE','ShowFacesListStyle','isAutoBattleCommandAdded','-%1\x20MP','ActSeq_Mechanics_Collapse','Game_BattlerBase_canAttack','drawLineText','processPostBattleCommonEvents','command357','11hsTOqK','drawSkillCost','TextColor','formula','StyleName','statusWindowRectDefaultStyle','_angleWholeDuration','smooth','_floatWholeDuration','MAT','user','allowCollapse','isOnCurrentMap','isForFriendBattleCore','createLowerLayer','_skillWindow','animationShouldMirror','CriticalHitRate','Setting','setBattleCameraTargets','PARTY','canAddSkillCommand','Shadow','ActSeq_Mechanics_Multipliers','Sprite_Enemy_initVisibility','Class-%1-%2','CmdIconAutoBattle','updateAngleCalculations','_animationSprites','getBattlePortraitOffsetX','Game_Interpreter_PluginCommand','parent','_battlerName','needsSelection','_battleField','message1','repeats','setBattlePortrait','basicGaugesY','performCastAnimation','_preemptive','Exploiter','_growY','isWaiting','%1StartActionJS','guardSkillId','visualHpGauge','actor','missed','ActSeq_Projectile_Picture','updateShadowBattleCore','Filename','\x5cI[%1]%2','setCursorRect','ARRAYSTRUCT','retreat','Sprite_Battler_initMembers','start','turn','helpWindowRect','clamp','onActorOk','ParseWeaponNotetags','WaitForNewLine','_forceAction','_actorSprites','_skewWholeDuration','Exploited','getCommonEventIdWithName','clearForcedGameTroopSettingsBattleCore','floor','blockWidth','_damagePopupArray','ActSeq_Movement_FacePoint','Sprite_Battler_update','PostStartActionJS','Game_Action_needsSelection','battleSpriteSkew','getColor','adjustFlippedBattlefield','attackMotions','applyCritical','evade','CommandAddOptions','isFlipped','deathStateId','_index','isForFriend','isActor','%1Damage%2JS','skillId','isForAll','ActSeq_Skew_WaitForSkew','onSelectAction','Scene_Battle_skillWindowRect','isGuard','_freezeMotionData','setHue','Damage','transform','setSTBExploited','battleCoreResumeLaunchBattle','removeHorrorEffect','EscapeFail','ActSeq_BattleLog_PushBaseLine','getItemDamageAmountTextBattleCore','prototype','startActorCommandSelection','Rate','updateBitmap','setupZoomBlurImpactFilter','thrust','_motionType','startWeaponAnimation','startTurn','applyAngleChange','isBuffAffected','updatePosition','Window_BattleLog_performRecovery','addAttackCommand','_opacityEasing','CriticalHitRateJS','BattleManager_onEncounter','mpDamageFmt','updateSpin','endAction','drawItemStatusListStyle','createActorCommandWindowBattleCore','_borderPortraitDuration','itemTextAlign','makeBattleCommand','enemyId','Window_BattleLog_displayTpDamage','VisuMZ_2_PartySystem','hasBeenDefeatedBefore','updateBorderStyle','ShowPortraits','Window_PartyCommand_initialize','isBattlerGrounded','actionSplicePoint','_growX','_actionInputIndex','ElementStatusCore','isConfused','speed','ActSeq_Movement_WaitForFloat','3TRKION','updateCollapse','applyData','PopupDuration','Sprite_Battler_startMove','\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20//\x20Declare\x20Arguments\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20user\x20=\x20arguments[0];\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20target\x20=\x20arguments[1];\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20obj\x20=\x20arguments[2];\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20let\x20value\x20=\x20arguments[3]\x20||\x200;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20let\x20originalValue\x20=\x20value;\x0a\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20//\x20Declare\x20Constants\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20action\x20=\x20(this.constructor\x20===\x20Game_Action)\x20?\x20this\x20:\x20user.currentAction();\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20a\x20=\x20user;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20b\x20=\x20target;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20attacker\x20=\x20user;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20defender\x20=\x20target;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20healer\x20=\x20user;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20receiver\x20=\x20target;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20actor\x20=\x20obj;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20currentClass\x20=\x20obj;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20skill\x20=\x20(this.constructor\x20===\x20Game_Action)\x20?\x20this.item()\x20:\x20obj;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20item\x20=\x20(this.constructor\x20===\x20Game_Action)\x20?\x20this.item()\x20:\x20obj;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20weapon\x20=\x20obj;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20armor\x20=\x20obj;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20enemy\x20=\x20obj;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20state\x20=\x20obj;\x0a\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20//\x20Create\x20Compatibility\x20Variables\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20let\x20origin\x20=\x20user;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20if\x20(Imported.VisuMZ_1_SkillsStatesCore\x20&&\x20$dataStates.includes(obj))\x20{\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20origin\x20=\x20target.getStateOrigin(obj.id);\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20}\x0a\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20//\x20Process\x20Code\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20try\x20{\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20%1\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20}\x20catch\x20(e)\x20{\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20if\x20($gameTemp.isPlaytest())\x20console.log(e);\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20}\x0a\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20//\x20NaN\x20Check\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20if\x20(isNaN(value)){\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20if\x20($gameTemp.isPlaytest())\x20{\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20console.log(\x27NaN\x20value\x20created\x20by\x20%2\x27.format(\x27\x27,obj.name));\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20console.log(\x27Restoring\x20value\x20to\x20%2\x27.format(\x27\x27,originalValue));\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20}\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20value\x20=\x20originalValue;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20}\x0a\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20//\x20Return\x20Value\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20return\x20value;\x0a\x20\x20\x20\x20\x20\x20\x20\x20','commandEscape','mainSpriteHeight','compareBattlerSprites','Actions','_activeWeaponSlot','ActSeq_Mechanics_AtbGauge','Scene_Battle_startPartyCommandSelection','updateHelp','selectNextActor','_dragonbonesSpriteContainer','weapons','statusTextAutoBattleStyle','PostDamage%1JS','initialize','cancelTargetSelectionVisibility','stepFlinch','getNextDamagePopup','_flinched','createDamageSprite','finishActorInput','_targetSkewX','isSkillItemWindowsMiddle','_motion','DefeatEvent','getDefeatedEnemies','apply','Spriteset_Battle_updateActors','isActing','battleSpin','isSideView','motionSpeed','command301_PreBattleEvent','softDamageCap','hide','performMoveToTargets','addSkillTypeCommand','addedBuffs','Game_Battler_onBattleStart','_totalValue','DefaultHardCap','Game_Action_isForRandom','_enemyID','battleback1Name','CopyCombatLog','setVisibleUI','2SZmbcz','_growDuration','updateFloat','_commandNameWindow','Game_BattlerBase_canGuard','animationWait','BattleLayout','DualWield','WaitForOpacity','isPlaytest','BattleManager_makeActionOrders','SkillItemMiddleLayout','startSpin','addImmortal','Sprite_Battleback_adjustPosition','Scene_Map_launchBattle','Game_Interpreter_command301','_back2Sprite','performDamage','setBattlerFacePoint','actorCommandWindowRect','applyBattleCoreJS','battleCommandName','ChargeRate','MOTIONS','_animation','isDTB','_escapeRatio','format','removeBuff','_visualHpGauge_JustDied','onBattleStart','startEnemySelection','BattleManager_inputtingAction','fittingHeight','forceSelect','changePaintOpacity','opponentsUnit','_cancelButton','displayActionResults','-%1','TP_Rate','applyItem','battleMembers','_hpGaugeSprite','alive\x20battlers','setHome','missle','addShowHpGaugeCommand','makeActionListAutoAttack','displayRemovedStates','ActSeq_Mechanics_AddState','setActiveWeaponSlot','_methods','commandNameWindowDrawBackground','_borderPortraitSprite','createBattleUIOffsetY','createWeather','setActionState','PopupOffsetX','Window_BattleLog_displayMpDamage','_angleDuration','createCancelButton','centerFrontViewSprite','getTraitSetKeys','backColor','cancelButtonText','onOpacityEnd','_scene','swapEnemyIDs','Scene_Battle_helpWindowRect','ParseSkillNotetags','dataId','DistanceAdjust','SkewX','partyCommandWindowRectBorderStyle','requestDragonbonesAnimation','isSpinning','battleLayoutStyle','showAnimation','BaseTroopIDs','cancelActorInput','skew','createTargetsJS','move','action','casting','iconWidth','drawGauge','createDigits','_padding','repeatTargets','damageOffsetY','_appeared','_actions','_branch','changeBattlerOpacity','addCustomCommands','contents','swing','updateStyleOpacity','createBattleUIOffsetX','Sprite_Actor_updateBitmap','makeTargetSprites','Game_Action_makeTargets','_waitMode','addAutoBattleCommand','attackAnimationIdSlot','setup','CriticalDmgFlat','makeData','maxCols','ActSeq_BattleLog_WaitForBattleLog','ActSeq_Movement_Skew','drawItemStyleIconText','setActorHome','Sprite_Actor_update','ActSeq_Camera_Reset','refreshBattlerMotions','frameVisible','isAnyProjectilePresent','isMagical','VisuMZ_2_DragonbonesUnion','_commonEventQueue','damageOffsetX','revertTpbCachedActor','JS\x20ESCAPE\x20FAILURE','alive\x20enemies','getWtypeIdWithName','addTextToCombatLog','Window_SkillList_maxCols','_floatDuration','displayMpDamage','criticalHitRate','alive\x20battlers\x20not\x20target','createKeyJS','updateStateSpriteBattleCore','animationBaseDelay','inHomePosition','_actorCommandWindow','uiInputPosition','_wtypeIDs','subject','onFloatEnd','makeSpeed','MotionAni','CastMagical','Scene_Battle_updateStatusWindowPosition','Debuffs','ActSeq_Horror_NoiseCreate','_growWholeDuration','Game_Action_isForFriend','iconHeight','nameY','HelpItem','isAnyoneGrowing','clearActiveWeaponSlot','_stypeIDs','svBattlerAnchorX','BattleManager_cancelActorInput','wtypeId','ForceDeath','waitForNewLine','processRandomizedData','HpGauge','createPartyCommandWindowBattleCore','icon','isDeathStateAffected','updateCommandNameWindow','Game_Battler_makeSpeed','processBattleCoreJS','ApplyImmortal','string','displayType','startActorSelection','ChantStyle','setSkill','map','placeActorName','sortDamageSprites','findTargetSprite','adjustWeaponSpriteOffset','removeAnimation','ActSeq_Animation_CastAnimation','callNextMethod','_enemyIDs','Window_BattleLog_performAction','ActiveTpbOptionsMessage','AutoBattleCancel','dead\x20opponents','setupRgbSplitImpactFilter','Scene_Options_maxCommands','clearBattlerMotionTrailData','ActSeq_Projectile_Animation','addSkillCommands','DTB','showHelpWindow','refreshDimmerBitmap','Game_Battler_onTurnEnd','frontviewSpriteY','createInnerPortrait','mainSpriteScaleX','Game_BattlerBase_eraseState','isTickBased','timeScale','getChildIndex','movement','_regionBattleback1','setBackgroundType','Sprite_Actor_setBattler','_stateSprite','ActSeq_Mechanics_BtbGain','SvBattlerMass-%1-%2','endAnimation','isAnyoneChangingOpacity','battleCommandIcon','_colorType','delay','cancel','isHiddenSkill','Enemy','DamageFlat','CastAnimation','BattleManager_processDefeat','MIN_SAFE_INTEGER','attackAnimationId1','alive\x20enemies\x20not\x20target','jumpBattler','default','indexOf','ParseArmorNotetags','sliceMax','EFFECT_COMMON_EVENT','dead\x20battlers','_battler','updateVisibility','ActSeq_Animation_ChangeBattlePortrait','critical','MeleeDistance','Game_System_initialize','isCharging','createActionSequenceProjectile','sliceMin','1:1','%1EndTurnJS','freezeFrame','changeCtbCastTime','setupWeaponAnimation','CriticalDuration','XPActorCommandLines','clearFreezeMotion','ActSeq_Mechanics_CtbSpeed','createDamageContainer','_weather','_target','_indent','Sprite_Enemy_createStateIconSprite','_battlerHue','EscapeFailureJS','scope','_actionBattlers','displayChangedBuffs','isDying','drawItemImageListStyle','isOpen','abs','SKILLS','weaponImageId','WaitForProjectile','onDatabaseLoaded','Scene_Battle_createPartyCommandWindow','updateForceAction','checkCacheKey','ActSeq_Target_CurrentIndex','_cache','drawBackgroundRect','initMembers','setBattleSkew','resetFontSettings','parse','CalcEscapeRatioJS','setupCriticalEffect','PostApplyAsTargetJS','BattleEndEvent','ActSeq_Mechanics_RemoveBuffDebuff','onRegeneratePlayStateAnimation','criticalDmgFlat','ActSeq_Motion_MotionType','onEncounterBattleCore','itemHit','showEnemyAttackAnimation','isFriendly','1sTiyOo','commandOptions','center','ActSeq_Target_RandTarget','_action','chantStyle','getConfigValue','AutoBattle','processRefresh','itemEffectAddAttackState','clearRect','create','initBattlePortrait','preemptive','process_VisuMZ_BattleCore_TraitObject_Notetags','WaitCount1','regenerateAll','FlashColor','enemy','ActionItemMsg','createHelpWindowBattleCore','commandNameWindowCenter','Game_Action_applyGlobal','updateBattlerContainer','isFightCommandEnabled','_growEasing','message2','helpAreaBottom','Game_Action_isForOpponent','isClicked','Window_BattleLog_pushBaseLine','actions','actionEffect','friendsUnit','startSkew','currentSymbol','NameOffsetX','moveBattlerToPoint','battleUIOffsetY','note','commandAutoBattle','getLastPluginCommandInterpreter','_waitCount','VisuMZ_0_CoreEngine','applySoftDamageCap','ArRedRate','useDigitGrouping','ActSeq_Movement_MoveBy','ShowRemovedBuff','IconStypeMagic','BattleManager_selectNextCommand','maxCommands','setEventCallback','rowSpacing','PreDamageAsTargetJS','snapForBackground','Spriteset_Battle_update','_effectDuration','Window_BattleLog_performCollapse','_itemWindow','value','ActSeq_Mechanics_CustomDmgFormula','AsUser','isAnyoneSkewing','ActSeq_Camera_FocusTarget','close','resizeWindowXPStyle','Window_ActorCommand_setup','battlerSprites','ActionEnd','isBattleCoreTargetScope','Window_BattleLog_clear','process_VisuMZ_BattleCore_Failsafes','displayBuffs','drawTextEx','isDisplayEmergedEnemies','applyDamageCaps','battleUIOffsetX','PostDamageAsUserJS','Window_BattleLog_performCounter','_active','currentClass','width','invokeAction','getSimilarSTypes','Targets2','evaded','ActSeq_Mechanics_RemoveState','prepareBorderActor','Game_Interpreter_updateWaitMode','Game_Party_addActor','_targetOpacity','addEscapeCommand','isEnemy','children','NameOffsetY','VisuMZ_2_BattleSystemCTB','regenerateAllBattleCore','collapse','drawItemStyleIcon','_stateIconSprite','%1StartBattleJS','initMembersBattleCore','_updateCursorArea','getNextSubjectFromPool','_customDamageFormula','AGI','isBattleSys','undecided','onDisabledPartyCommandSelection','active','SideviewSelect','COMBATLOG','EasingType','isOptionsCommandAdded','battleAnimation','mainSpriteScaleY','result','battleMove','applyHardDamageCap','getDamageStyle','updateSkew','logActionList','ActSeq_Element_Clear','_requestRefresh','isNextSceneBattleTransitionable','commandStyle','onAllActionsEnd','BattleLogRectJS','process_VisuMZ_BattleCore_Notetags','sleep','windowAreaHeight','itemRect','ARRAYEVAL','_skillIDs','createShadowSprite','_inputting','displayHpDamage','gainBravePoints','collapseType','isMagicSkill','ActSeq_Weapon_SetActiveWeapon','motionType','arRedRate','ShowPortraitsBorderStyle','Scene_ItemBase_applyItem','Sprite_Actor_moveToStartPosition','evalDamageFormulaBattleCore','partyCommandWindowRect','getMenuImage','Scene_Battle_itemWindowRect','parameters','AUTO\x20BATTLE','getBattlePortrait','requestMotion','stepForward','EVAL','pages','Window_BattleLog_performMagicEvasion','updatePadding','isAttack','StyleON','PreEndActionJS','stop','left','CalcEscapeRaiseJS','_executedValue','_flashColor','Window_BattleLog_refresh','commandSymbol','damageFlat','escape','removeBuffsAuto','setBattleZoom','ActionSkillMsg1','JS\x20BATTLE\x20DEFEAT','push','setBattleCameraOffset','growBattler','Window_BattleLog_performSubstitute','Duration','ShowMpDmg','ITEM','ConvertActionSequenceTarget','command301','startDamagePopup','ActSeq_Animation_AttackAnimation2','Window_BattleLog_performActionStart','_duration','BattleCore','Game_Actor_equips','2194lBCJnQ','CheckMapBattleEventValid','ParseItemNotetags','createAnimationContainer','onBattleStartBattleCore','startAttackWeaponAnimation','_targetAngle','ActSeq_Set_WholeActionSet','maxTp','updatePhase','%1\x20is\x20missing\x20a\x20required\x20plugin.\x0aPlease\x20install\x20%2\x20into\x20the\x20Plugin\x20Manager.','optDisplayTp','forceEscapeSprite','Parse_Notetags_Targets','itemHeight','scale','compareEnemySprite','State-%1-%2','VisuMZ_2_BattleSystemBTB','Elements','loop','PreStartActionJS','displayItemMessage','createAutoBattleWindow','updateWeather','Strength','PRE-','PrioritySortActors','_lastPluginCommandInterpreter','Sprite_Enemy_updateBossCollapse','21iRnwrg','PreApply%1JS','stepBack','changeInputWindow','PostEndBattleJS','createSeparateDamagePopups','finishActionSet','displayReflectionPlayBack','ActSeq_Angle_Reset','_forcedBattlers','clearDamagePopup','StartName','isAppeared','updateCustomActionSequence','addOptionsCommand','Window_ItemList_maxCols','ActSeq_Skew_Reset','ActSeq_Movement_Spin','JS\x20%1END\x20BATTLE','Mechanics','makeTargetSelectionMoreVisible','Game_Battler_clearDamagePopup','ActSeq_Element_NullElements','ActSeq_Horror_TVRemove','duration','ceil','bgType','top','requestAnimation','status','onGrowEnd','Game_Battler_performActionStart','getBattlePortraitOffsetY','split','Scene_Battle_start','actorCommandSingleSkill','Window_Options_statusText','initBattleCore','svBattlerShadowVisible','_shake','isAnyoneFloating','process_VisuMZ_BattleCore_PluginParams','targetObjects','Scene_Battle_createActorCommandWindow','bitmapHeight','unshift','fight','ActorCmd','Game_Action_itemEffectAddAttackState','CombatLogIcon','usePremadeActionSequence','recoverAll','updateGrow','all\x20targets','getSkillTypes','log','Window_BattleStatus_initialize','cameraDuration','startMotion','Index','evalDamageFormula','Window_BattleLog_performEvasion','isBattleFlipped','createCommandNameWindow','boxWidth','ConfigManager_makeData','applyForcedGameTroopSettingsBattleCore','setBattleCameraPoint','setCustomDamageFormula','_forcing','Window_BattleLog_displayCritical','hpDamage','isImmortal','battleback2Name','processEscape','placeStateIcon','ActSeq_Mechanics_Immortal','helpWindowRectBorderStyle','_armorPenetration','_jumpHeight','Interrupt','WaitForMovement','getAttackMotionSlot','damageContainer','alive\x20actors\x20not\x20user','trueRandomTarget','battleAngle','length','removeStatesAuto','addedStateObjects','removeImmortal','dead\x20friends','FlinchDuration','getInputButtonString','extraPositionY','Game_Battler_performDamage','isAnyoneMoving','_preBattleCommonEvent','updateWaitMode','faceRect','battleCameraData','numTargets','applyArmorModifiers','VarianceFormulaJS','isAnimationShownOnBattlePortrait','Sprite_Weapon_loadBitmap','addAutoBattleCommands','traitObjects','performSubstitute','show','CmdTextAlign','ForceExploited','maxBattleMembers','isTurnBased','isPartyTpbInputtable','createEnemies','commandNameWindowDrawText','performAction','displayAction','createMainSprite','ActSeq_BattleLog_WaitForNewLine','command3011','hasSvBattler','VisuMZ_2_BattleSystemSTB','missile','Game_Actor_setup','allBattleMembers','setupActionSet','Window_BattleLog_displayCurrentState','_back1Sprite','Scene_Battle_onEnemyCancel','clearHorrorEffects','_allTargets','pattern','CommandWidth','RequiresDefeat','CmdStyle','drawText','BattleManager_endBattle','_effectsContainer','GroupDigits','Scene_Battle_onEnemyOk','makeDeepCopy','_weaponImageId','autoBattleStart','WaitForSkew','Sprite_Battler_setHome','attachSpritesToDistortionSprite','AddOption','ScaleToFit','windowPadding','popBaseLine','AS\x20TARGET','HelpEscape','getBattlePortraitFilename','processVictory','getAttackWeaponAnimationId','applyEasing','isForOpponent','angleDuration','UNTITLED','SmoothImage','Scale','Actor','isAutoBattle','ActSeq_Angle_WaitForAngle','commandName','_enemyId','setActiveWeaponSet','dead\x20enemies','_baseX','requestFauxAnimation','opacity','Window_BattleLog_update','front\x20center','Game_BattlerBase_refresh','displayTpDamage','isHidden','getDualWieldTimes','Window_BattleLog_displayMiss','battleCoreTpbMainPhase','ActSeq_Movement_WaitForScale','filterArea','_windowLayer','_flipScaleX','onSkewEnd','DigitGroupingDamageSprites','ActSeq_Movement_WaitForJump','lineRect','changeWeather','MAXMP','updateShadow','ALL\x20SKILLS','LastSelected','WaitForScale','SkillItemStandardCols','isGuardWaiting','TargetLocation','BravePoints','animationId','_offsetX','isPhysical','checkShowHideBattleNotetags','PreDamageJS','skill','ShowMissEvasion','Window_BattleLog_performActionEnd','_iconIndex','wholeActionSet','AttackAnimation','createString','dragonbonesData','svShadow','Sprite_Enemy_loadBitmap','_tpbState','Game_BattlerBase_addNewState','displayFailure','contentsOpacity','XPSpriteYLocation','DEF','placeBasicGauges','forceWeaponAnimation','PreRegenerateJS','dying','_phase','isAnyoneJumping','type','CmdIconFight','isForRandomBattleCore','skillItemWindowRectMiddle','itemLineRect','applyTargetFilters','hpHealingFmt','MANUAL','replace','PartyCmd','SvBattlerSolo-%1-%2','setHelpWindow','adjustPosition_ScaleDown','redraw','_jumpWholeDuration','_angleEasing','ActSeq_Mechanics_FtbAction','atbInterrupt','isDamagePopupRequested','Window_BattleEnemy_show','setImmortal','ScaleY','changeAtbChargeTime','DamageType%1','SkillItemBorderCols','ShowHpDmg','3zWkpVi','startAction','destroy','autoSelect','updateStart','update','WaitCount','_commonEventIDs','svBattlerData','makeTargets','visible','HelpAutoBattle','addChildAt','Parse_Notetags_Action','softDamageCapRate','_createEffectsContainer','spinBattler','ResetOffset','hpAffected','_spriteset','VisuMZ_2_BattleSystemATB','head','_createDamageContainer','checkAutoCustomActionSequenceNotetagEffect','ShowCritical','updateFrame','%1EndBattleJS','WaitForFloat','ShowCounter','ParseActorNotetags','_cursorSprite','_targetSkewY','traitSet','Window_Options_addGeneralOptions','ActSeq_Set_TargetActionSet','updateFlip','Angle','ChangeOrderBy','isQueueOptionsMenu','skewBattler','HomePosJS','setFrame','Game_Battler_startTpbTurn','min','CommandVisible','Sprite_Battler_updateMain','ReflectPlayback','Sprite_Battler_updatePosition','clearFreezeMotionForWeapons','1492788PHylyx','DigitGrouping','_enemies','selectNextCommandTpb','ActSeq_Horror_GlitchCreate','_animationCount','createBattleField','_jumpMaxHeight','51701xXOaHQ','ParseEnemyNotetags','criticalDmgRate','_battlePortrait','ActSeq_Animation_ActionAnimation','AutoBattleMsg','ActSeq_Movement_WaitForMovement','ext','worldTransform','Window_BattleLog_performDamage','ActSeq_Zoom_Scale','updateStateSprite','Scene_Battle_selectPreviousCommand','okTargetSelectionVisibility','_statusWindow','ActSeq_Motion_WaitMotionFrame','ActionSequence','PreStartTurnJS','isDuringNonLoopingMotion','Height','Game_Map_setupBattleback','ParseAllNotetags','Window_BattleLog_displayFailure','AutoNotetag','origin','Game_Map_battleback2Name','chant','drawItemStatus','HP_Flat','battleJump','command119','CriticalHitFlat','fillRect','ActSeq_Mechanics_StbExploit','Formula','#ffffff','Scene_Battle_createCancelButton','placeGauge','adjustPosition_ScaleToFit','Game_Map_battleback1Name','createStateIconSprite','ActionEndUpdate','message4','JS\x20%1END\x20ACTION','SlotID','updateMain','_checkOn','moveBattlerDistance','createAnimationSprite','ActSeq_Weapon_NextActiveWeapon','canBattlerMove','onEscapeSuccess','setSvBattlerSprite','isBusy','base','battleCorePreBattleCommonEvent','ActSeq_ChangeAngle','AnimationID','flashColor','Sprite_Enemy_setHue','isLearnedSkill','MP_Flat','isSceneBattle','ActSeq_ChangeSkew','battleProjectiles','initVisibility','performRecovery','NewPopupBottom','magicSkills','Victory','ActionCenteredName','changeCtbChargeTime','battleStatusWindowAnimationContainer','isForOne','isNonSubmenuCancel','ShowCurrentState','DefaultDamageStyle','repositionCancelButtonBorderStyle','_jumpDuration','alive\x20battlers\x20not\x20user','canEscape','addSingleSkillCommands','match','turnCount','needsSelectionBattleCore','FaceAway','charging','_motionCount','Actor-%1-%2','_logWindow','version','_opacityWholeDuration','_enemy','svBattlerAnchorY','floatBattler','Wave','hardDamageCap','itemEffectAddNormalState','EnableDamageCap','autoBattle','CoreEngine','nextActiveWeaponSlot','showNormalAnimation','CheckSkillCommandShowSwitches','Targets','bossCollapse','updateBattlebackBitmap','ActSeq_BattleLog_PopBaseLine','inputtingAction','battleEffect','attack','_skewY','mainSpriteWidth','displayStartMessages','ShowAddedDebuff','isGrowing','getItemDamageAmountLabelOriginal','JS\x20BATTLE\x20VICTORY','displayCurrentState','Game_Action_apply','isOptionsCommandEnabled','Game_Party_removeActor','isNextScene','isBorderStylePortraitShown','PortraitScale','ActSeq_Movement_Opacity','ActionStart','performAttack','isAutoBattleCommandEnabled','actionBattleCoreJS','_floatHeight','createChildSprite','_homeY','Width','setupBattleCoreData','createBattleFieldBattleCore','list','setBattlerFlip','ClearBattleLog','Sprite_Battler_damageOffsetX','isMVAnimation','IconStypeNorm','float','WaitForAngle','regionId','removeDamageSprite','Settings','Game_Enemy_transform','ShowFailure','States','item','_actorWindow','MotionIdle','itemWindowRect','_svBattlerSprite','RegExp','forceMotion','makeTargetsBattleCore','createEmptyBitmap','mainSprite','Scene_Map_initialize','_text','makeAutoBattleActions','BattleManager_startInput','showPortraits','playEnemyDamage','bind','_animationContainer','battleDisplayText','IconSet','Window_BattleLog_popBaseLine','updateBorderSprite','battleSkew','updateRefresh','registerCommand','JS\x20%1REGENERATE','selectNextCommand','_distortionSprite','addDebuff','Sprite_Enemy_update','isCustomActionSequence','removeChild','aliveMembers','getSkillIdWithName','ForceExploiter','startGrow','BattleDefeatJS','isChanting','_offsetY','startPartyCommandSelection','FlashDuration','ConfigManager_applyData','sort','drawIcon','members','performFlinch','isOpponent','endBattle','setText','ActSeq_DB_DragonbonesMotionAni','ShowPopup','isTpb','Sprite_Enemy_updateStateSprite','_item','Intensity','ActSeq_Mechanics_VariablePopup','_targetFloatHeight','ActSeq_Horror_GlitchRemove','stateMotionIndex','slices','drain','removedBuffs','canMove','ActSeq_Mechanics_DamagePopup','focus','dead','POST-','addedDebuffs','PostEndActionJS','refresh','moveToStartPosition','FlinchDistanceX','createActorCommandWindow','popupDamage','applyGlobal','CommandAddAutoBattle','isActiveTpb','effects','displayEvasion','QoL','occasion','setupDamagePopup','Name','addLoadListener','ActSeq_Impact_ShockwaveCenterTargets','debuffAdd','%1\x27s\x20version\x20does\x20not\x20match\x20plugin\x27s.\x20Please\x20update\x20it\x20in\x20the\x20Plugin\x20Manager.','Game_Battler_forceAction','isSceneChanging','ActSeq_Mechanics_DeathBreak','checkShowHideSkillNotetags','refreshStatusWindow','right','Game_Action_itemEffectAddNormalState','isPartyCommandWindowDisabled','Text','_surprise','loadSystem','_forcedBattleLayout','drawItemImagePortraitStyle','_currentActor','canGuard','updateShadowPosition','LUK','_pattern','BattleManager_startBattle','isEffecting','currentAction','ActSeq_BattleLog_AddText','ScaleUp','isAnimationPlaying','setupBattleback','filter','_damages','BattleStartEvent','_borderPortraitTargetX','_targetGrowY','ActSeq_Impact_MotionBlurTarget','attackStates','glitch','WaitForCamera','front\x20base','pow','Scene_Battle_createAllWindows','WaitForZoom','toLowerCase','_enemySprites','actorCommandCancelTPB','createBorderStylePortraitSprite','startInput','ReflectAnimation','DamageStyleList','ActSeq_Mechanics_ArmorPenetration','ShowSubstitute','createBattleFieldContainer','performMagicEvasion','Sprite_Enemy_setBattler','_createClientArea','SceneManager_isSceneChanging','reduce','commandFight','_skewDuration','return\x200','AlphaFilter','performMiss','ActSeq_Impact_ColorBreak','options','putActiveBattlerOnTop','updateScale','processForcedAction','ActSeq_Mechanics_WaitForEffect','WaitForAnimation','MotionSpeed','ScaleX','updateBattlebackBitmap2','_targetIndex','914534bOctpP','addAnimationSpriteToContainer','VisuMZ_4_CombatLog','finalizeScale','_isBattlerFlipped','isBattleMember','_effectType','changeBattlebacks','createAllWindows','onTurnEnd','NUM','Game_Action_clear','wait','DamageStyles','addBuff','Amp','BattleLog','isDead','buffAdd','Spriteset_Battle_createLowerLayer','getAttackMotion','index','Frame','startFloat','VisuMZ_1_ElementStatusCore','executeDamage','Post','_actor','ArRedFlat','_lastEnemy','notFocusValid','Game_Action_executeDamage','ActSeq_Impact_MotionBlurScreen','createHelpWindow','autoBattleStyle','setHorrorEffectSettings','EmergeText','enemyNames','waitForAnimation','StepDuration','Sprite_Battler_setBattler','startTpbTurn','itemCri','ParseStateNotetags','isAtbCastingState','effect','actorCommandEscape','31793FKBnri','PostApplyAsUserJS','Scene_Battle_logWindowRect','lineHeight','Spriteset_Battle_createBattleField','criticalHitFlat','battlerSmoothImage','FUNC','addItemCommand','CmdTextAutoBattle','VisuMZ_1_SkillsStatesCore','weaponTypes','isForRandom','prev\x20target','drawActorFace','AutoBattleOK','ShowAddedState','isPreviousScene','isMoving','isItem','performAttackSlot','ShowHide','onEnemyCancel','slice','guard','createHpGaugeSprite','emerge','PrioritySortActive','becomeSTBExploited','setBattlerBattleCore','Opacity','process_VisuMZ_BattleCore_CreateRegExp','fontSize','\x0a\x20\x20\x20\x20\x20\x20\x20\x20//\x20Declare\x20Arguments\x0a\x20\x20\x20\x20\x20\x20\x20\x20const\x20user\x20=\x20arguments[0];\x0a\x20\x20\x20\x20\x20\x20\x20\x20const\x20skill\x20=\x20arguments[1];\x0a\x20\x20\x20\x20\x20\x20\x20\x20const\x20a\x20=\x20user;\x0a\x20\x20\x20\x20\x20\x20\x20\x20const\x20b\x20=\x20user;\x0a\x20\x20\x20\x20\x20\x20\x20\x20let\x20visible\x20=\x20true;\x0a\x0a\x20\x20\x20\x20\x20\x20\x20\x20//\x20Process\x20Code\x0a\x20\x20\x20\x20\x20\x20\x20\x20try\x20{\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20%1\x0a\x20\x20\x20\x20\x20\x20\x20\x20}\x20catch\x20(e)\x20{\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20if\x20($gameTemp.isPlaytest())\x20console.log(e);\x0a\x20\x20\x20\x20\x20\x20\x20\x20}\x0a\x0a\x20\x20\x20\x20\x20\x20\x20\x20//\x20Return\x20Value\x0a\x20\x20\x20\x20\x20\x20\x20\x20return\x20visible;\x0a\x20\x20\x20\x20','PostRegenerateJS','_createCursorArea','_additionalSprites','ActSeq_Motion_FreezeMotionFrame','performActionEnd','_cursorArea','calcWindowHeight','Game_BattlerBase_die','DefaultStyle','reserveCommonEvent','isItemCommandEnabled','Enemy-%1-%2','updateEffectContainers','BattleManager_startAction','waitForMovement','WaitForSpin','callUpdateHelp','_battlerContainer','ConvertParams','isCustomBattleScope','Game_Actor_makeActionList','jump','actorCommandAutoBattle','clearBattleCoreData','EscapeSuccessJS','Scene_Battle_onActorOk','ActSeq_Movement_MoveToTarget','ActSeq_Mechanics_AddBuffDebuff','waitCount','onAngleEnd','setupIconTextPopup','#%1','Game_Action_itemHit','PreApplyAsTargetJS','isCommandEnabled','isSkill','_tempEquipCheck','BattleManager_initMembers','displayMiss','removeActor','BackColor','activate','Game_Enemy_setup','addCommand','DistanceY','performActionStart','BattleCmdList','addText','performEvasion','alive\x20opponents','iconIndex','actor%1-portrait','messageSpeed','randomInt','spriteId','BattleManager_onEscapeSuccess','ATK','_damageContainer','battleCamera','isChangingOpacity','updateHpGaugePosition','ActSeq_Movement_WaitForOpacity','Targets1','ActSeq_BattleLog_Clear','splice','_tpbSceneChangeCacheActor','damageRate','DisplayAction','addGuardCommand','HitRate','partyCommandWindowRectXPStyle','_helpWindow','PopupShiftY','CriticalHitMultiplier','PostStartBattleJS','battler','filters','drawItem','surprise','Window_BattleEnemy_initialize','161517MukHEY','MAXHP','commandStyleCheck','%1\x20is\x20incorrectly\x20placed\x20on\x20the\x20plugin\x20list.\x0aIt\x20is\x20a\x20Tier\x20%2\x20plugin\x20placed\x20over\x20other\x20Tier\x20%3\x20plugins.\x0aPlease\x20reorder\x20the\x20plugin\x20list\x20from\x20smallest\x20to\x20largest\x20tier\x20numbers.','ActSeq_Motion_ClearFreezeFrame','%1StartTurnJS','StartTurnMsg','battleCommands','skillWindowRect','onMoveEnd','Window_BattleStatus_drawItemImage','resizeWindowBorderStyle','damage','shadow','gradientFillRect','die','ActSeq_Camera_WaitForCamera','dead\x20actors','PreStartBattleJS','placeTimeGauge','not\x20focus','addDamageSprite','\x0a\x20\x20\x20\x20\x20\x20\x20\x20//\x20Declare\x20Arguments\x0a\x20\x20\x20\x20\x20\x20\x20\x20const\x20user\x20=\x20arguments[0];\x0a\x20\x20\x20\x20\x20\x20\x20\x20const\x20a\x20=\x20user;\x0a\x20\x20\x20\x20\x20\x20\x20\x20const\x20b\x20=\x20user;\x0a\x20\x20\x20\x20\x20\x20\x20\x20let\x20targets\x20=\x20arguments[1];\x0a\x0a\x20\x20\x20\x20\x20\x20\x20\x20//\x20Process\x20Code\x0a\x20\x20\x20\x20\x20\x20\x20\x20try\x20{\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20%1\x0a\x20\x20\x20\x20\x20\x20\x20\x20}\x20catch\x20(e)\x20{\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20if\x20($gameTemp.isPlaytest())\x20console.log(e);\x0a\x20\x20\x20\x20\x20\x20\x20\x20}\x0a\x0a\x20\x20\x20\x20\x20\x20\x20\x20//\x20Return\x20Value\x0a\x20\x20\x20\x20\x20\x20\x20\x20return\x20targets\x20||\x20[];\x0a\x20\x20\x20\x20','CmdIconEscape','_autoBattleWindow','BattleManager_processVictory','makeActions','createMiss','HelpFight','Linear','applyGlobalCommonEventNotetags','DefaultSoftScaler','launchBattle','trim','Pre','AutoMeleeSolo','adjustPosition','DisablePartyCmd','StepDistanceY','CalcActionSpeedJS','waitForEffect','toUpperCase','isBypassDamageCap','setupShockwaveImpactFilter','addFightCommand','performActionEndMembers','loadEnemy','autoSelectPriority','Immortal','getEnemyIdWithName','TPB','performActionMotions','_multipliers','helpAreaHeight','isBattleRefreshRequested','PreDamage%1JS','DefaultSoftCap','drawSingleSkillCost','createEnemyNameContainer','addChild','opacityStart','addChildToBack','arPenFlat','setLastPluginCommandInterpreter','Sprite_Battler_damageOffsetY','createJS','equips','extraHeight','waitForFloat','_defeatedEnemies','_updateFilterArea','Sprite_Actor_createStateSprite','Game_Action_evalDamageFormula','SkillsStatesCore','victory','canGuardBattleCore','Scene_Battle_selectNextCommand','_battleCoreBattleStartEvent','checkShowHideSwitchNotetags','JS\x20%1START\x20BATTLE','Radius','Sprite_Battler_isMoving','AnchorX','_battleLayoutStyle','textColor','isBattleTest','startMove','WaitForJump','FlinchDistanceY','auto','_partyCommandWindow','isRightInputMode','_subject','hasSkill','applyImmortal','ActSeq_Set_SetupAction','Point','onEscapeFailure','faceWidth','bitmap','isCancelled','dimColor2','pushBaseLine','_skewEasing','createStateSprite','setHandler','prepareCustomActionSequence','isMeleeMultiTargetAction','canAttackBattleCore','getNextSubject','magicReflection','MessageWait','actorId','Game_BattlerBase_isStateResist','Scene_Battle_createHelpWindow','gainMp','createContents','counterAttack','createCommandVisibleJS','updateCancel','isTeamBased','callOptions','Destination','process_VisuMZ_BattleCore_BaseTroops','isSpriteVisible','XPActorDefaultHeight','CriticalColor','addNewState','autoBattleAtStart','ShowTpDmg','PostApply%1JS','isUndecided','open','updateBattleProcess','_emptyBitmap','sortEnemies','_homeX','autoBattleUseSkills','targetActionSet','ActSeq_Movement_BattleStep','StartTurnShow','Game_Interpreter_command283','clearActiveWeaponSet','Style','_dimmerSprite','AutoBattleBgType','Game_BattlerBase_initMembers','battleFloat','DamageRate','Direction','_attackAnimationId','extraPositionX','shift','selectPreviousCommand','description','createEffectActionSet','setupBattlebackBattleCore','eraseState','skillItemWindowRectBorderStyle','canInput','performJump','isAtbChargingState','command339','ActSeq_Camera_FocusPoint','RepositionEnemies','Sprite_Actor_updateFrame','spell','Scene_Battle_commandFight','PopupShiftX','updateActors','Scene_Battle_windowAreaHeight','updateOpacity','Scene_Battle_partyCommandWindowRect','isFloating','alive\x20friends\x20not\x20user','getItemDamageAmountLabelBattleCore','forceAction','textSizeEx','includes','BattleManager_startTurn','CreateActionSequenceTargets','JS\x20%1DAMAGE\x20%2','makeCommandList','height','partyCommandWindowRectDefaultStyle','flashDuration','FaceDirection','VisuMZ_2_BattleSystemFTB','isTriggered','stypeId','performSTBExploiter','getStypeIdWithName','invokeMagicReflection','Sprite_Actor_updateShadow','padding','adjustPosition_ScaleUp','okButtonText','call','resize','ActSeq_BattleLog_Refresh','statusWindowRect','AS\x20USER','canAttack','portrait','Parse_Notetags_TraitObjects','performCounter','custom','attackAnimationId2','round','_canLose','Window_BattleLog_performReflection','_tpbNeedsPartyCommand','isInputting','Window_ActorCommand_initialize','isForOpponentBattleCore','singleSkill','OffsetY','addBattleCoreAutoBattleStyleCommand','alive\x20friends\x20not\x20target','isAnyoneSpinning','allowRandomSpeed','Skill-%1-%2','innerWidth','_opacityDuration','mpDamage','refreshActorPortrait','_enemyWindow','processDefeat','ARRAYNUM','PostEndTurnJS','setupHpGaugeSprite','mpHealingFmt','ShowWeapon','Scene_Boot_onDatabaseLoaded','setBattler','ActionSkillMsg2','setupTextPopup','clearMotion','isStateResist','anchorX','SkewY','noise','power','ShowActorGauge','performMoveToPoint','arRedFlat','_enemyNameContainer','ActSeq_Mechanics_CtbOrder','_autoBattle','PreApplyAsUserJS','ArPenFlat','_targetGrowX','Scene_Battle_startActorSelection','isActionSelectionValid','_floatEasing','+%1','updateTargetPosition','_regionBattleback2','SvWeaponSolo-%1-%2','requestMotionRefresh','displayCounter','ActSeq_Animation_AttackAnimation','repositionEnemiesByResolution','_motionSpeed','BattleManager_updatePhase','JumpToLabel','clear','PostDamageAsTargetJS','battleZoom','name','customDamageFormula','applyFreezeMotionFrames','isSkipPartyCommandWindow','refreshMotion','removedStateObjects','isBattlerFlipped'];const _0x567a=function(_0x4979ec,_0x55789b){_0x4979ec=_0x4979ec-0xd5;let _0x4aefee=_0x4aef[_0x4979ec];return _0x4aefee;};const _0x53c255=_0x567a;(function(_0x1cb9d2,_0x43d197){const _0x347791=_0x567a;while(!![]){try{const _0x4d35f7=-parseInt(_0x347791(0x419))+-parseInt(_0x347791(0x737))*parseInt(_0x347791(0x20f))+-parseInt(_0x347791(0x7af))*-parseInt(_0x347791(0x2ce))+parseInt(_0x347791(0x2ec))*parseInt(_0x347791(0x421))+-parseInt(_0x347791(0x3e8))*-parseInt(_0x347791(0x582))+parseInt(_0x347791(0x553))*parseInt(_0x347791(0xeb))+parseInt(_0x347791(0x841))*-parseInt(_0x347791(0x5f4));if(_0x4d35f7===_0x43d197)break;else _0x1cb9d2['push'](_0x1cb9d2['shift']());}catch(_0x2a11ad){_0x1cb9d2['push'](_0x1cb9d2['shift']());}}}(_0x4aef,0xe4fbc));var label=_0x53c255(0x2cc),tier=tier||0x0,dependencies=[],pluginData=$plugins[_0x53c255(0x527)](function(_0x1742f9){const _0x92ed2a=_0x53c255;return _0x1742f9[_0x92ed2a(0x309)]&&_0x1742f9[_0x92ed2a(0x68e)]['includes']('['+label+']');})[0x0];VisuMZ[label][_0x53c255(0x4b3)]=VisuMZ[label][_0x53c255(0x4b3)]||{},VisuMZ['ConvertParams']=function(_0x357135,_0x3bd18e){const _0x568581=_0x53c255;for(const _0x3be7c7 in _0x3bd18e){if(_0x3be7c7[_0x568581(0x473)](/(.*):(.*)/i)){const _0x31fe8c=String(RegExp['$1']),_0x581022=String(RegExp['$2'])[_0x568581(0x61d)]()['trim']();let _0x34ba0f,_0x209049,_0x2aa030;switch(_0x581022){case _0x568581(0x55d):_0x34ba0f=_0x3bd18e[_0x3be7c7]!==''?Number(_0x3bd18e[_0x3be7c7]):0x0;break;case _0x568581(0x6d8):_0x209049=_0x3bd18e[_0x3be7c7]!==''?JSON[_0x568581(0x202)](_0x3bd18e[_0x3be7c7]):[],_0x34ba0f=_0x209049[_0x568581(0x19c)](_0x5d6423=>Number(_0x5d6423));break;case _0x568581(0x2ab):_0x34ba0f=_0x3bd18e[_0x3be7c7]!==''?eval(_0x3bd18e[_0x3be7c7]):null;break;case _0x568581(0x294):_0x209049=_0x3bd18e[_0x3be7c7]!==''?JSON[_0x568581(0x202)](_0x3bd18e[_0x3be7c7]):[],_0x34ba0f=_0x209049[_0x568581(0x19c)](_0xdcdcbb=>eval(_0xdcdcbb));break;case'JSON':_0x34ba0f=_0x3bd18e[_0x3be7c7]!==''?JSON[_0x568581(0x202)](_0x3bd18e[_0x3be7c7]):'';break;case _0x568581(0x719):_0x209049=_0x3bd18e[_0x3be7c7]!==''?JSON[_0x568581(0x202)](_0x3bd18e[_0x3be7c7]):[],_0x34ba0f=_0x209049[_0x568581(0x19c)](_0x421f65=>JSON[_0x568581(0x202)](_0x421f65));break;case _0x568581(0x589):_0x34ba0f=_0x3bd18e[_0x3be7c7]!==''?new Function(JSON['parse'](_0x3bd18e[_0x3be7c7])):new Function(_0x568581(0x545));break;case'ARRAYFUNC':_0x209049=_0x3bd18e[_0x3be7c7]!==''?JSON[_0x568581(0x202)](_0x3bd18e[_0x3be7c7]):[],_0x34ba0f=_0x209049[_0x568581(0x19c)](_0x1a719a=>new Function(JSON[_0x568581(0x202)](_0x1a719a)));break;case'STR':_0x34ba0f=_0x3bd18e[_0x3be7c7]!==''?String(_0x3bd18e[_0x3be7c7]):'';break;case _0x568581(0x760):_0x209049=_0x3bd18e[_0x3be7c7]!==''?JSON['parse'](_0x3bd18e[_0x3be7c7]):[],_0x34ba0f=_0x209049['map'](_0xbba15d=>String(_0xbba15d));break;case _0x568581(0x790):_0x2aa030=_0x3bd18e[_0x3be7c7]!==''?JSON['parse'](_0x3bd18e[_0x3be7c7]):{},_0x357135[_0x31fe8c]={},VisuMZ['ConvertParams'](_0x357135[_0x31fe8c],_0x2aa030);continue;case _0x568581(0x7e5):_0x209049=_0x3bd18e[_0x3be7c7]!==''?JSON[_0x568581(0x202)](_0x3bd18e[_0x3be7c7]):[],_0x34ba0f=_0x209049['map'](_0xe8bf3d=>VisuMZ[_0x568581(0x5b6)]({},JSON[_0x568581(0x202)](_0xe8bf3d)));break;default:continue;}_0x357135[_0x31fe8c]=_0x34ba0f;}}return _0x357135;},(_0x30498e=>{const _0x2404fe=_0x53c255,_0x2aa78b=_0x30498e[_0x2404fe(0x701)];for(const _0x3d1e44 of dependencies){if(!Imported[_0x3d1e44]){alert(_0x2404fe(0x2d8)['format'](_0x2aa78b,_0x3d1e44)),SceneManager[_0x2404fe(0x786)]();break;}}const _0x59e450=_0x30498e[_0x2404fe(0x68e)];if(_0x59e450['match'](/\[Version[ ](.*?)\]/i)){const _0x16fb72=Number(RegExp['$1']);_0x16fb72!==VisuMZ[label][_0x2404fe(0x47b)]&&(alert(_0x2404fe(0x50d)[_0x2404fe(0x107)](_0x2aa78b,_0x16fb72)),SceneManager[_0x2404fe(0x786)]());}if(_0x59e450['match'](/\[Tier[ ](\d+)\]/i)){const _0x267b25=Number(RegExp['$1']);_0x267b25<tier?(alert(_0x2404fe(0x5f7)[_0x2404fe(0x107)](_0x2aa78b,_0x267b25,tier)),SceneManager[_0x2404fe(0x786)]()):tier=Math[_0x2404fe(0x776)](_0x267b25,tier);}VisuMZ['ConvertParams'](VisuMZ[label][_0x2404fe(0x4b3)],_0x30498e[_0x2404fe(0x2a6)]);})(pluginData),VisuMZ[_0x53c255(0x6a8)]=function(_0x5c1285){const _0x910409=_0x53c255;let _0x196aeb=[];for(const _0x572739 of _0x5c1285){_0x196aeb=_0x196aeb[_0x910409(0x733)](VisuMZ[_0x910409(0x2c6)](_0x572739));}return _0x196aeb[_0x910409(0x527)](_0x2a5d67=>_0x2a5d67);},VisuMZ['ConvertActionSequenceTarget']=function(_0x1d3144){const _0x382197=_0x53c255,_0x771d9d=BattleManager['allBattleMembers']()[_0x382197(0x527)](_0x253e7b=>_0x253e7b&&_0x253e7b[_0x382197(0x2f8)]()),_0x548e4b=BattleManager[_0x382197(0x650)],_0x1e5efc=BattleManager[_0x382197(0x1e9)],_0x3be17a=BattleManager[_0x382197(0x370)]?BattleManager[_0x382197(0x370)][_0x382197(0x599)](0x0):_0x771d9d;_0x1d3144=_0x1d3144['toLowerCase']()[_0x382197(0x615)]();if(_0x1d3144===_0x382197(0x7b9))return[_0x548e4b];else{if(_0x1d3144==='current\x20target')return[_0x1e5efc];else{if(_0x1d3144===_0x382197(0x58f)){if(_0x1e5efc){const _0x33fe1c=_0x3be17a['indexOf'](_0x1e5efc);return _0x33fe1c>=0x0?[_0x3be17a[_0x33fe1c-0x1]||_0x1e5efc]:[_0x1e5efc];}}else{if(_0x1d3144==='text\x20target'){if(_0x1e5efc){const _0x6c5019=_0x3be17a[_0x382197(0x1d0)](_0x1e5efc);return _0x6c5019>=0x0?[_0x3be17a[_0x6c5019+0x1]||_0x1e5efc]:[_0x1e5efc];}}else{if(_0x1d3144===_0x382197(0x321))return _0x3be17a;else{if(_0x1d3144===_0x382197(0x4f7))return[_0x548e4b][_0x382197(0x733)](_0x3be17a);else{if(_0x1d3144===_0x382197(0x608))return _0x771d9d[_0x382197(0x527)](_0x20af0f=>_0x20af0f!==_0x548e4b&&!_0x3be17a[_0x382197(0x6a6)](_0x20af0f)&&_0x20af0f[_0x382197(0x571)]());}}}}}}if(_0x548e4b){if(_0x1d3144==='alive\x20friends')return _0x548e4b['friendsUnit']()[_0x382197(0x4d7)]();else{if(_0x1d3144===_0x382197(0x6a2))return _0x548e4b[_0x382197(0x230)]()[_0x382197(0x4d7)]()['filter'](_0x2d847c=>_0x2d847c!==_0x548e4b);else{if(_0x1d3144===_0x382197(0x6ce))return _0x548e4b[_0x382197(0x230)]()[_0x382197(0x4d7)]()[_0x382197(0x527)](_0x5420bf=>_0x5420bf!==_0x1e5efc);else{if(_0x1d3144===_0x382197(0x347))return _0x548e4b[_0x382197(0x230)]()['deadMembers']();else{if(_0x1d3144['match'](/FRIEND INDEX (\d+)/i)){const _0x3f42c9=Number(RegExp['$1']);return[_0x548e4b[_0x382197(0x230)]()['members']()[_0x3f42c9]];}}}}}if(_0x1d3144===_0x382197(0x5d5))return _0x548e4b[_0x382197(0x110)]()['aliveMembers']();else{if(_0x1d3144==='alive\x20opponents\x20not\x20target')return _0x548e4b[_0x382197(0x110)]()['aliveMembers']()[_0x382197(0x527)](_0x28b15f=>_0x28b15f!==_0x1e5efc);else{if(_0x1d3144===_0x382197(0x1a8))return _0x548e4b['opponentsUnit']()[_0x382197(0x794)]();else{if(_0x1d3144['match'](/OPPONENT INDEX (\d+)/i)){const _0x3f0f8d=Number(RegExp['$1']);return[_0x548e4b['opponentsUnit']()['members']()[_0x3f0f8d]];}}}}}if(_0x1d3144==='alive\x20actors')return $gameParty[_0x382197(0x4d7)]();else{if(_0x1d3144===_0x382197(0x340))return $gameParty[_0x382197(0x4d7)]()[_0x382197(0x527)](_0x52e159=>_0x52e159!==_0x548e4b);else{if(_0x1d3144==='alive\x20actors\x20not\x20target')return $gameParty[_0x382197(0x4d7)]()['filter'](_0x5b09af=>_0x5b09af!==_0x1e5efc);else{if(_0x1d3144===_0x382197(0x605))return $gameParty[_0x382197(0x794)]();else{if(_0x1d3144[_0x382197(0x473)](/ACTOR INDEX (\d+)/i)){const _0x4cc1b3=Number(RegExp['$1']);return[$gameParty[_0x382197(0x4e3)]()[_0x4cc1b3]];}else{if(_0x1d3144[_0x382197(0x473)](/ACTOR ID (\d+)/i)){const _0x32f9bf=Number(RegExp['$1']);return[$gameActors[_0x382197(0x7de)](_0x32f9bf)];}}}}}}if(_0x1d3144===_0x382197(0x16a))return $gameTroop[_0x382197(0x4d7)]();else{if(_0x1d3144==='alive\x20enemies\x20not\x20user')return $gameTroop['aliveMembers']()['filter'](_0x26e5c2=>_0x26e5c2!==_0x548e4b);else{if(_0x1d3144===_0x382197(0x1cd))return $gameTroop['aliveMembers']()['filter'](_0x6f14c7=>_0x6f14c7!==_0x1e5efc);else{if(_0x1d3144===_0x382197(0x395))return $gameTroop[_0x382197(0x794)]();else{if(_0x1d3144[_0x382197(0x473)](/ENEMY INDEX (\d+)/i)){const _0x4ecba1=Number(RegExp['$1']);return[$gameTroop['members']()[_0x4ecba1]];}else{if(_0x1d3144[_0x382197(0x473)](/ENEMY ID (\d+)/i)){const _0x2faeb2=Number(RegExp['$1']);return $gameTroop[_0x382197(0x4d7)]()[_0x382197(0x527)](_0x21817b=>_0x21817b[_0x382197(0x832)]()===_0x2faeb2);}}}}}}if(_0x1d3144===_0x382197(0x118))return _0x771d9d[_0x382197(0x527)](_0x6af48a=>_0x6af48a[_0x382197(0x72f)]());else{if(_0x1d3144===_0x382197(0x470))return _0x771d9d[_0x382197(0x527)](_0xbe07a=>_0xbe07a[_0x382197(0x72f)]()&&_0xbe07a!==_0x548e4b);else{if(_0x1d3144===_0x382197(0x171))return _0x771d9d['filter'](_0x5dcecd=>_0x5dcecd[_0x382197(0x72f)]()&&_0x5dcecd!==_0x1e5efc);else{if(_0x1d3144===_0x382197(0x1d4))return _0x771d9d['filter'](_0x5c2d07=>_0x5c2d07['isDead']());}}}return[];},PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x653),_0x5a7335=>{const _0x3c17d9=_0x53c255;if(!SceneManager[_0x3c17d9(0x45f)]())return;VisuMZ['ConvertParams'](_0x5a7335,_0x5a7335);const _0x224937=$gameTemp['getLastPluginCommandInterpreter'](),_0x558757=BattleManager[_0x3c17d9(0x213)],_0x366333=BattleManager[_0x3c17d9(0x650)],_0x3c58c3=BattleManager['_allTargets']?BattleManager[_0x3c17d9(0x370)][_0x3c17d9(0x599)](0x0):[],_0x541203=BattleManager[_0x3c17d9(0x47a)];if(!_0x224937||!_0x558757||!_0x366333)return;if(!_0x558757[_0x3c17d9(0x4b7)]())return;if(_0x5a7335[_0x3c17d9(0x5e7)])_0x541203[_0x3c17d9(0x362)](_0x366333,_0x558757[_0x3c17d9(0x4b7)]());_0x5a7335[_0x3c17d9(0x196)]&&_0x541203[_0x3c17d9(0x2bf)](_0x3c17d9(0x652),_0x366333,_0x3c58c3,!![]);if(_0x5a7335[_0x3c17d9(0x49f)])_0x541203[_0x3c17d9(0x2bf)](_0x3c17d9(0x5d1),_0x366333,_0x558757);if(_0x5a7335['WaitForMovement'])_0x541203[_0x3c17d9(0x2bf)](_0x3c17d9(0x5b2));if(_0x5a7335[_0x3c17d9(0x1c9)])_0x541203[_0x3c17d9(0x2bf)](_0x3c17d9(0x7d6),_0x366333,_0x558757);if(_0x5a7335[_0x3c17d9(0x54e)])_0x541203[_0x3c17d9(0x2bf)](_0x3c17d9(0x579));_0x224937[_0x3c17d9(0x727)]('battlelog');}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],_0x53c255(0x2d5),_0x580b74=>{const _0x5e5617=_0x53c255;if(!SceneManager[_0x5e5617(0x45f)]())return;VisuMZ['ConvertParams'](_0x580b74,_0x580b74);const _0x506ea5=$gameTemp[_0x5e5617(0x238)](),_0x13d68f=BattleManager[_0x5e5617(0x213)],_0x5d1770=BattleManager[_0x5e5617(0x650)],_0x235184=BattleManager[_0x5e5617(0x370)]?BattleManager[_0x5e5617(0x370)][_0x5e5617(0x599)](0x0):[],_0x25ce07=BattleManager[_0x5e5617(0x47a)],_0x480414=_0x580b74[_0x5e5617(0xf2)]??![];if(!_0x506ea5||!_0x13d68f||!_0x5d1770)return;if(!_0x13d68f[_0x5e5617(0x4b7)]())return;let _0x3c3abe=_0x480414?_0x25ce07['getDualWieldTimes'](_0x5d1770):0x1;for(let _0x13c35a=0x0;_0x13c35a<_0x3c3abe;_0x13c35a++){_0x480414&&_0x5d1770[_0x5e5617(0x807)]()&&_0x25ce07['push'](_0x5e5617(0x394),_0x5d1770,_0x13c35a);if(_0x580b74[_0x5e5617(0x75b)])_0x25ce07['push'](_0x5e5617(0x361),_0x5d1770,_0x13d68f);if(_0x580b74[_0x5e5617(0x3ee)]>0x0)_0x25ce07[_0x5e5617(0x2bf)](_0x5e5617(0x5c0),_0x580b74[_0x5e5617(0x3ee)]);if(_0x580b74['ActionAnimation'])_0x25ce07['push'](_0x5e5617(0x13a),_0x5d1770,_0x235184,_0x13d68f['item']()[_0x5e5617(0x3b3)]);if(_0x580b74['WaitForAnimation'])_0x25ce07[_0x5e5617(0x2bf)]('waitForAnimation');for(const _0x4c4405 of _0x235184){if(!_0x4c4405)continue;if(_0x580b74[_0x5e5617(0x71a)])_0x25ce07['push'](_0x5e5617(0x22f),_0x5d1770,_0x4c4405);}}_0x480414&&_0x5d1770['isActor']()&&_0x25ce07[_0x5e5617(0x2bf)](_0x5e5617(0x682),_0x5d1770);if(_0x580b74[_0x5e5617(0x196)])_0x25ce07['push'](_0x5e5617(0x652),_0x5d1770,_0x235184,![]);_0x506ea5[_0x5e5617(0x727)](_0x5e5617(0x79c));}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],_0x53c255(0x40a),_0x5306ab=>{const _0x370cc5=_0x53c255;if(!SceneManager[_0x370cc5(0x45f)]())return;VisuMZ[_0x370cc5(0x5b6)](_0x5306ab,_0x5306ab);const _0x5b9f93=$gameTemp['getLastPluginCommandInterpreter'](),_0x480178=BattleManager['_action'],_0x162862=BattleManager[_0x370cc5(0x650)],_0x1f3108=BattleManager[_0x370cc5(0x370)]?BattleManager[_0x370cc5(0x370)][_0x370cc5(0x599)](0x0):[],_0x582878=BattleManager['_logWindow'],_0xb7f7b3=_0x5306ab[_0x370cc5(0xf2)]??![];if(!_0x5b9f93||!_0x480178||!_0x162862)return;if(!_0x480178[_0x370cc5(0x4b7)]())return;let _0x3dcd68=_0xb7f7b3?_0x582878[_0x370cc5(0x39e)](_0x162862):0x1;for(let _0x53230c=0x0;_0x53230c<_0x3dcd68;_0x53230c++){for(const _0x22b698 of _0x1f3108){if(!_0x22b698)continue;_0xb7f7b3&&_0x162862[_0x370cc5(0x807)]()&&_0x582878[_0x370cc5(0x2bf)](_0x370cc5(0x394),_0x162862,_0x53230c);if(_0x5306ab[_0x370cc5(0x75b)])_0x582878[_0x370cc5(0x2bf)](_0x370cc5(0x361),_0x162862,_0x480178);if(_0x5306ab[_0x370cc5(0x21e)]>0x0)_0x582878[_0x370cc5(0x2bf)](_0x370cc5(0x5c0),_0x5306ab[_0x370cc5(0x21e)]);if(_0x5306ab['ActionAnimation'])_0x582878[_0x370cc5(0x2bf)](_0x370cc5(0x13a),_0x162862,[_0x22b698],_0x480178[_0x370cc5(0x4b7)]()[_0x370cc5(0x3b3)]);if(_0x5306ab[_0x370cc5(0x71c)]>0x0)_0x582878[_0x370cc5(0x2bf)](_0x370cc5(0x5c0),_0x5306ab[_0x370cc5(0x71c)]);if(_0x5306ab[_0x370cc5(0x71a)])_0x582878[_0x370cc5(0x2bf)](_0x370cc5(0x22f),_0x162862,_0x22b698);}}_0xb7f7b3&&_0x162862[_0x370cc5(0x807)]()&&_0x582878['push']('clearActiveWeaponSet',_0x162862);if(_0x5306ab['ApplyImmortal'])_0x582878[_0x370cc5(0x2bf)](_0x370cc5(0x652),_0x162862,_0x1f3108,![]);_0x5b9f93[_0x370cc5(0x727)](_0x370cc5(0x79c));}),PluginManager['registerCommand'](pluginData[_0x53c255(0x701)],'ActSeq_Set_FinishAction',_0x36bb2c=>{const _0xd1c4f6=_0x53c255;if(!SceneManager[_0xd1c4f6(0x45f)]())return;VisuMZ['ConvertParams'](_0x36bb2c,_0x36bb2c);const _0x334fd4=$gameTemp[_0xd1c4f6(0x238)](),_0x1d92ed=BattleManager[_0xd1c4f6(0x213)],_0x75ef18=BattleManager[_0xd1c4f6(0x650)],_0x2047c6=BattleManager[_0xd1c4f6(0x370)]?BattleManager['_allTargets'][_0xd1c4f6(0x599)](0x0):[],_0x2345ea=BattleManager[_0xd1c4f6(0x47a)];if(!_0x334fd4||!_0x1d92ed||!_0x75ef18)return;if(!_0x1d92ed[_0xd1c4f6(0x4b7)]())return;if(_0x36bb2c[_0xd1c4f6(0x196)])_0x2345ea[_0xd1c4f6(0x2bf)](_0xd1c4f6(0x652),_0x75ef18,_0x2047c6,![]);if(_0x36bb2c[_0xd1c4f6(0x7ee)])_0x2345ea[_0xd1c4f6(0x2bf)](_0xd1c4f6(0x18d));if(_0x36bb2c['WaitForEffect'])_0x2345ea['push']('waitForEffect');if(_0x36bb2c[_0xd1c4f6(0x4ab)])_0x2345ea[_0xd1c4f6(0x2bf)](_0xd1c4f6(0x6fe));if(_0x36bb2c[_0xd1c4f6(0x254)])_0x2345ea[_0xd1c4f6(0x2bf)](_0xd1c4f6(0x5a8),_0x75ef18);if(_0x36bb2c['WaitForMovement'])_0x2345ea['push'](_0xd1c4f6(0x5b2));_0x334fd4[_0xd1c4f6(0x727)]('battlelog');}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x459),_0x119073=>{const _0x477527=_0x53c255;if(!SceneManager['isSceneBattle']())return;if(!Imported[_0x477527(0x78c)])return;VisuMZ[_0x477527(0x5b6)](_0x119073,_0x119073);const _0x2c741a=$gameTemp[_0x477527(0x238)](),_0x252d72=_0x119073[_0x477527(0x4b0)];if(!_0x2c741a)return;$gameScreen[_0x477527(0x741)](_0x119073['Angle'],_0x119073[_0x477527(0x2c3)],_0x119073[_0x477527(0x280)]);if(_0x252d72)_0x2c741a[_0x477527(0x727)](_0x477527(0x342));}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x2f4),_0x74968d=>{const _0x508596=_0x53c255;if(!SceneManager[_0x508596(0x45f)]())return;if(!Imported[_0x508596(0x78c)])return;VisuMZ[_0x508596(0x5b6)](_0x74968d,_0x74968d);const _0x285676=$gameTemp[_0x508596(0x238)](),_0x480484=_0x74968d[_0x508596(0x4b0)];if(!_0x285676)return;$gameScreen[_0x508596(0x741)](0x0,_0x74968d[_0x508596(0x2c3)],_0x74968d[_0x508596(0x280)]);if(_0x480484)_0x285676[_0x508596(0x727)](_0x508596(0x342));}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],_0x53c255(0x391),_0x244bff=>{const _0x2f721e=_0x53c255;if(!SceneManager[_0x2f721e(0x45f)]())return;if(!Imported[_0x2f721e(0x78c)])return;const _0x3400bf=$gameTemp[_0x2f721e(0x238)]();if(!_0x3400bf)return;_0x3400bf['setWaitMode'](_0x2f721e(0x342));}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],_0x53c255(0x425),_0x5a7497=>{const _0x131e3a=_0x53c255;if(!SceneManager[_0x131e3a(0x45f)]())return;VisuMZ['ConvertParams'](_0x5a7497,_0x5a7497);const _0x28cfb3=$gameTemp[_0x131e3a(0x238)](),_0x2e1b67=BattleManager['_action'],_0x5b3942=BattleManager[_0x131e3a(0x650)],_0x4483be=VisuMZ['CreateActionSequenceTargets'](_0x5a7497[_0x131e3a(0x489)]),_0x57852c=_0x5a7497['Mirror'],_0xbbf660=BattleManager[_0x131e3a(0x47a)];if(!_0x28cfb3||!_0x2e1b67||!_0x5b3942)return;if(!_0x2e1b67[_0x131e3a(0x4b7)]())return;let _0x19068e=_0x2e1b67[_0x131e3a(0x4b7)]()[_0x131e3a(0x3b3)];if(_0x19068e<0x0)_0x19068e=_0x5b3942['attackAnimationId1']();$gameTemp[_0x131e3a(0x308)](_0x4483be,_0x19068e,_0x57852c),_0x5a7497[_0x131e3a(0x54e)]&&_0x28cfb3[_0x131e3a(0x727)](_0x131e3a(0x282));}),PluginManager['registerCommand'](pluginData[_0x53c255(0x701)],_0x53c255(0x6f9),_0x3d7d29=>{const _0x29cd25=_0x53c255;if(!SceneManager[_0x29cd25(0x45f)]())return;VisuMZ[_0x29cd25(0x5b6)](_0x3d7d29,_0x3d7d29);const _0x455796=$gameTemp['getLastPluginCommandInterpreter'](),_0x435f98=BattleManager[_0x29cd25(0x650)],_0x2b69d3=VisuMZ[_0x29cd25(0x6a8)](_0x3d7d29[_0x29cd25(0x489)]),_0xa0e4a6=_0x3d7d29[_0x29cd25(0x70c)],_0x470103=BattleManager[_0x29cd25(0x47a)];if(!_0x455796||!_0x435f98)return;const _0x5e5762=_0x435f98[_0x29cd25(0x1cc)]();$gameTemp[_0x29cd25(0x308)](_0x2b69d3,_0x5e5762,_0xa0e4a6),_0x3d7d29['WaitForAnimation']&&_0x455796['setWaitMode']('battleAnimation');}),PluginManager['registerCommand'](pluginData['name'],_0x53c255(0x2c9),_0x15a6e0=>{const _0x24f690=_0x53c255;if(!SceneManager[_0x24f690(0x45f)]())return;VisuMZ[_0x24f690(0x5b6)](_0x15a6e0,_0x15a6e0);const _0x243eb7=_0x31a085['attackAnimationIdSlot'](_0x15a6e0['Slot']);if(_0x243eb7<=0x0)return;const _0x14967f=$gameTemp[_0x24f690(0x238)](),_0x31a085=BattleManager[_0x24f690(0x650)],_0x4a4082=VisuMZ[_0x24f690(0x6a8)](_0x15a6e0[_0x24f690(0x489)]),_0x19af10=_0x15a6e0[_0x24f690(0x70c)],_0x201cdf=BattleManager[_0x24f690(0x47a)];if(!_0x14967f||!_0x31a085)return;$gameTemp['requestAnimation'](_0x4a4082,_0x243eb7,_0x19af10),_0x15a6e0[_0x24f690(0x54e)]&&_0x14967f[_0x24f690(0x727)](_0x24f690(0x282));}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x1a2),_0x5e5127=>{const _0x45902f=_0x53c255;if(!SceneManager[_0x45902f(0x45f)]())return;VisuMZ[_0x45902f(0x5b6)](_0x5e5127,_0x5e5127);const _0xd39cfd=$gameTemp[_0x45902f(0x238)](),_0x52f2d1=BattleManager[_0x45902f(0x213)],_0x3c5934=_0x5e5127[_0x45902f(0x70c)],_0x16379c=VisuMZ[_0x45902f(0x6a8)](_0x5e5127['Targets']);if(!_0xd39cfd||!_0x52f2d1)return;if(!_0x52f2d1[_0x45902f(0x4b7)]())return;for(const _0x5edb91 of _0x16379c){if(!_0x5edb91)continue;_0x5edb91[_0x45902f(0x7d6)](_0x52f2d1,_0x3c5934);}if(_0x5e5127[_0x45902f(0x54e)])_0xd39cfd['setWaitMode']('battleAnimation');}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x1d7),_0x55543a=>{const _0x1ac13e=_0x53c255;VisuMZ[_0x1ac13e(0x5b6)](_0x55543a,_0x55543a);const _0x5bfdb2=$gameTemp[_0x1ac13e(0x238)](),_0x16c06b=VisuMZ[_0x1ac13e(0x6a8)](_0x55543a[_0x1ac13e(0x489)]),_0x24f0a6=_0x55543a[_0x1ac13e(0x7e2)];if(!_0x24f0a6)return;for(const _0x3edda9 of _0x16c06b){if(!_0x3edda9)continue;if(!_0x3edda9[_0x1ac13e(0x807)]())continue;_0x3edda9[_0x1ac13e(0x7d4)](_0x24f0a6);}}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],'ActSeq_Animation_ShowAnimation',_0x262ea9=>{const _0xf7a3be=_0x53c255;if(!SceneManager[_0xf7a3be(0x45f)]())return;VisuMZ[_0xf7a3be(0x5b6)](_0x262ea9,_0x262ea9);const _0x518360=$gameTemp['getLastPluginCommandInterpreter'](),_0xce6399=VisuMZ[_0xf7a3be(0x6a8)](_0x262ea9[_0xf7a3be(0x489)]),_0x9810a2=_0x262ea9[_0xf7a3be(0x45a)],_0x49b719=_0x262ea9['Mirror'];if(!_0x518360)return;$gameTemp[_0xf7a3be(0x308)](_0xce6399,_0x9810a2,_0x49b719);if(_0x262ea9[_0xf7a3be(0x54e)])_0x518360['setWaitMode']('battleAnimation');}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],'ActSeq_Animation_WaitForAnimation',_0x1249ff=>{const _0x48ae64=_0x53c255;if(!SceneManager[_0x48ae64(0x45f)]())return;const _0x1643a2=$gameTemp[_0x48ae64(0x238)]();if(!_0x1643a2)return;_0x1643a2[_0x48ae64(0x727)](_0x48ae64(0x282));}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x523),_0x374555=>{const _0x358500=_0x53c255;if(!SceneManager[_0x358500(0x45f)]())return;VisuMZ[_0x358500(0x5b6)](_0x374555,_0x374555);const _0x3e2703=BattleManager[_0x358500(0x47a)],_0x4073d=_0x374555[_0x358500(0xe9)]&&Imported[_0x358500(0x555)];_0x3e2703[_0x358500(0x5d3)](_0x374555[_0x358500(0x516)]),_0x4073d&&Imported[_0x358500(0x555)]&&$gameSystem[_0x358500(0x16c)](_0x374555[_0x358500(0x516)]||'',_0x374555[_0x358500(0x31d)]||0x0);}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x5e3),_0x4cd67a=>{const _0x29af86=_0x53c255;if(!SceneManager[_0x29af86(0x45f)]())return;const _0x542e6f=BattleManager[_0x29af86(0x47a)];_0x542e6f[_0x29af86(0x6fe)]();}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x720),_0x1d5624=>{const _0x17be74=_0x53c255;if(!SceneManager[_0x17be74(0x45f)]())return;const _0x2f1167=$gameTemp['getLastPluginCommandInterpreter'](),_0x50ddd2=BattleManager['_action'],_0x5c8164=BattleManager[_0x17be74(0x650)],_0x508cf1=BattleManager[_0x17be74(0x47a)];if(!_0x2f1167||!_0x50ddd2||!_0x5c8164)return;if(!_0x50ddd2['item']())return;_0x508cf1[_0x17be74(0x362)](_0x5c8164,_0x50ddd2['item']()),_0x2f1167['setWaitMode'](_0x17be74(0x79c));}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],_0x53c255(0x48c),_0x3583bd=>{const _0x42056a=_0x53c255;if(!SceneManager['isSceneBattle']())return;const _0x3e4226=BattleManager['_logWindow'];_0x3e4226[_0x42056a(0x383)]();}),PluginManager['registerCommand'](pluginData[_0x53c255(0x701)],_0x53c255(0x817),_0x2f2c09=>{const _0x4fc67a=_0x53c255;if(!SceneManager['isSceneBattle']())return;const _0x57c14c=BattleManager['_logWindow'];_0x57c14c[_0x4fc67a(0x65a)]();}),PluginManager['registerCommand'](pluginData[_0x53c255(0x701)],_0x53c255(0x6bb),_0x5b67a0=>{const _0x5507e0=_0x53c255;if(!SceneManager[_0x5507e0(0x45f)]())return;const _0x3143d4=BattleManager[_0x5507e0(0x47a)];_0x3143d4[_0x5507e0(0x4fc)]();}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],'ActSeq_BattleLog_UI',_0x4b35eb=>{const _0x42ae2c=_0x53c255;if(!SceneManager[_0x42ae2c(0x45f)]())return;VisuMZ[_0x42ae2c(0x5b6)](_0x4b35eb,_0x4b35eb),SceneManager[_0x42ae2c(0x12f)][_0x42ae2c(0xea)](_0x4b35eb[_0x42ae2c(0x597)]);}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x15b),_0x5aab42=>{const _0x44d59c=_0x53c255;if(!SceneManager['isSceneBattle']())return;const _0x1fd14c=$gameTemp['getLastPluginCommandInterpreter']();_0x1fd14c[_0x44d59c(0x727)](_0x44d59c(0x79c));}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x364),_0x242c99=>{const _0x3a7754=_0x53c255;if(!SceneManager['isSceneBattle']())return;const _0x364c6c=$gameTemp[_0x3a7754(0x238)](),_0x320daa=BattleManager[_0x3a7754(0x47a)];_0x320daa['waitForNewLine'](),_0x364c6c[_0x3a7754(0x727)](_0x3a7754(0x79c));}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],'ActSeq_Camera_Clamp',_0x59eaea=>{const _0x48dc49=_0x53c255;if(!SceneManager[_0x48dc49(0x45f)]())return;if(!Imported['VisuMZ_3_ActSeqCamera'])return;VisuMZ[_0x48dc49(0x5b6)](_0x59eaea,_0x59eaea);const _0x3125a9=$gameScreen[_0x48dc49(0x350)]();_0x3125a9['cameraClamp']=_0x59eaea[_0x48dc49(0x7c1)];}),PluginManager['registerCommand'](pluginData[_0x53c255(0x701)],_0x53c255(0x697),_0x35bbd2=>{const _0x291661=_0x53c255;if(!SceneManager[_0x291661(0x45f)]())return;if(!Imported[_0x291661(0x78c)])return;VisuMZ[_0x291661(0x5b6)](_0x35bbd2,_0x35bbd2);const _0x3bf1c0=$gameTemp[_0x291661(0x238)](),_0x3ab651=_0x35bbd2[_0x291661(0x52f)];$gameScreen[_0x291661(0x32f)](_0x35bbd2['FocusX'],_0x35bbd2[_0x291661(0x746)],_0x35bbd2[_0x291661(0x2c3)],_0x35bbd2['EasingType']);if(_0x3ab651)_0x3bf1c0[_0x291661(0x727)](_0x291661(0x5de));}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],_0x53c255(0x24f),_0x2e3ef4=>{const _0x2ded51=_0x53c255;if(!SceneManager['isSceneBattle']())return;if(!Imported['VisuMZ_3_ActSeqCamera'])return;VisuMZ[_0x2ded51(0x5b6)](_0x2e3ef4,_0x2e3ef4);const _0x2a71ac=$gameTemp[_0x2ded51(0x238)](),_0x54c57a=VisuMZ[_0x2ded51(0x6a8)](_0x2e3ef4['Targets']),_0xf29f65=_0x2e3ef4[_0x2ded51(0x52f)];$gameScreen[_0x2ded51(0x7c2)](_0x54c57a,_0x2e3ef4['Duration'],_0x2e3ef4[_0x2ded51(0x280)]);if(_0xf29f65)_0x2a71ac[_0x2ded51(0x727)](_0x2ded51(0x5de));}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],_0x53c255(0x799),_0x4d91f6=>{const _0x1164e5=_0x53c255;if(!SceneManager[_0x1164e5(0x45f)]())return;if(!Imported[_0x1164e5(0x78c)])return;VisuMZ[_0x1164e5(0x5b6)](_0x4d91f6,_0x4d91f6);const _0xc5ed3e=$gameTemp[_0x1164e5(0x238)](),_0x2188af=_0x4d91f6['WaitForCamera'];$gameScreen[_0x1164e5(0x2c0)](_0x4d91f6[_0x1164e5(0x77b)],_0x4d91f6[_0x1164e5(0x6cc)],_0x4d91f6[_0x1164e5(0x2c3)],_0x4d91f6['EasingType']);if(_0x2188af)_0xc5ed3e[_0x1164e5(0x727)](_0x1164e5(0x5de));}),PluginManager['registerCommand'](pluginData[_0x53c255(0x701)],_0x53c255(0x160),_0xce7411=>{const _0x452878=_0x53c255;if(!SceneManager[_0x452878(0x45f)]())return;if(!Imported[_0x452878(0x78c)])return;VisuMZ[_0x452878(0x5b6)](_0xce7411,_0xce7411);const _0x901a2e=$gameTemp[_0x452878(0x238)](),_0x1a31df=_0xce7411['ResetFocus'],_0xb41f37=_0xce7411[_0x452878(0x3f9)],_0x8a312e=_0xce7411[_0x452878(0x52f)];if(_0x1a31df){const _0x335a22=Math[_0x452878(0x6c4)](Graphics[_0x452878(0x261)]/0x2),_0x6f842a=Math['round'](Graphics[_0x452878(0x6ab)]/0x2);$gameScreen['setBattleCameraPoint'](_0x335a22,_0x6f842a,_0xce7411[_0x452878(0x2c3)],_0xce7411[_0x452878(0x280)]);}_0xb41f37&&$gameScreen[_0x452878(0x2c0)](0x0,0x0,_0xce7411[_0x452878(0x2c3)],_0xce7411['EasingType']);if(_0x8a312e)_0x901a2e[_0x452878(0x727)](_0x452878(0x5de));}),PluginManager['registerCommand'](pluginData[_0x53c255(0x701)],_0x53c255(0x604),_0x1c6fe0=>{const _0x18aa52=_0x53c255;if(!SceneManager['isSceneBattle']())return;if(!Imported['VisuMZ_3_ActSeqCamera'])return;const _0x46fd7b=$gameTemp[_0x18aa52(0x238)]();if(!_0x46fd7b)return;_0x46fd7b[_0x18aa52(0x727)](_0x18aa52(0x5de));}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x4e8),_0x223185=>{const _0x4fcbd3=_0x53c255;if(!SceneManager[_0x4fcbd3(0x45f)]())return;if(!Imported[_0x4fcbd3(0x165)])return;VisuMZ[_0x4fcbd3(0x5b6)](_0x223185,_0x223185);const _0x5833c2=VisuMZ[_0x4fcbd3(0x6a8)](_0x223185[_0x4fcbd3(0x489)]),_0x6eaa2=_0x223185[_0x4fcbd3(0x17c)]['toLowerCase']()[_0x4fcbd3(0x615)]();for(const _0x356c1b of _0x5833c2){if(!_0x356c1b)continue;_0x356c1b[_0x4fcbd3(0x137)](_0x6eaa2);}}),PluginManager['registerCommand'](pluginData[_0x53c255(0x701)],'ActSeq_DB_DragonbonesTimeScale',_0x4d3543=>{const _0x3f3735=_0x53c255;if(!SceneManager[_0x3f3735(0x45f)]())return;if(!Imported[_0x3f3735(0x165)])return;VisuMZ[_0x3f3735(0x5b6)](_0x4d3543,_0x4d3543);const _0x3d14ee=VisuMZ[_0x3f3735(0x6a8)](_0x4d3543[_0x3f3735(0x489)]),_0x322423=_0x4d3543['TimeScale'];for(const _0x2a0e2b of _0x3d14ee){if(!_0x2a0e2b)continue;_0x2a0e2b[_0x3f3735(0x3bf)]()[_0x3f3735(0x1b7)]=_0x322423;}}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],'ActSeq_Element_AddElements',_0x442bf2=>{const _0x476e8c=_0x53c255;if(!SceneManager[_0x476e8c(0x45f)]())return;if(!Imported['VisuMZ_1_ElementStatusCore'])return;VisuMZ[_0x476e8c(0x5b6)](_0x442bf2,_0x442bf2);const _0x1b809d=BattleManager[_0x476e8c(0x213)],_0x25d527=_0x442bf2[_0x476e8c(0x2e1)];if(!_0x1b809d)return;_0x1b809d['_battleCoreAddedElements']=_0x25d527;}),PluginManager['registerCommand'](pluginData[_0x53c255(0x701)],_0x53c255(0x28a),_0x5d5674=>{const _0x186db4=_0x53c255;if(!SceneManager[_0x186db4(0x45f)]())return;if(!Imported[_0x186db4(0x56b)])return;const _0x186dfa=BattleManager[_0x186db4(0x213)];if(!_0x186dfa)return;_0x186dfa['clearElementChanges']();}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],'ActSeq_Element_ForceElements',_0x517a9e=>{const _0x491f3e=_0x53c255;if(!SceneManager[_0x491f3e(0x45f)]())return;if(!Imported[_0x491f3e(0x56b)])return;VisuMZ['ConvertParams'](_0x517a9e,_0x517a9e);const _0x59035d=BattleManager['_action'],_0x573664=_0x517a9e[_0x491f3e(0x2e1)];if(!_0x59035d)return;_0x59035d[_0x491f3e(0x710)]=_0x573664;}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x302),_0x1c95d3=>{const _0x4ecf76=_0x53c255;if(!SceneManager[_0x4ecf76(0x45f)]())return;if(!Imported['VisuMZ_1_ElementStatusCore'])return;const _0x537a0b=BattleManager[_0x4ecf76(0x213)];if(!_0x537a0b)return;_0x537a0b['_battleCoreNoElement']=!![];}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],'ActSeq_Horror_Clear',_0x2e7bd6=>{const _0x430889=_0x53c255;if(!Imported[_0x430889(0x787)])return;if(!SceneManager[_0x430889(0x45f)]())return;VisuMZ[_0x430889(0x5b6)](_0x2e7bd6,_0x2e7bd6);const _0x2c4339=VisuMZ[_0x430889(0x6a8)](_0x2e7bd6[_0x430889(0x489)]);for(const _0x58aac6 of _0x2c4339){if(!_0x58aac6)continue;_0x58aac6[_0x430889(0x815)]('noise'),_0x58aac6[_0x430889(0x815)]('glitch'),_0x58aac6[_0x430889(0x815)]('tv'),_0x58aac6[_0x430889(0x36f)]();}$gamePlayer[_0x430889(0x4fc)]();}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x41d),_0x5df615=>{const _0x4abbcc=_0x53c255;if(!Imported[_0x4abbcc(0x787)])return;if(!SceneManager[_0x4abbcc(0x45f)]())return;VisuMZ[_0x4abbcc(0x5b6)](_0x5df615,_0x5df615);const _0x2fd5f7=VisuMZ[_0x4abbcc(0x6a8)](_0x5df615[_0x4abbcc(0x489)]),_0x11b518=_0x4abbcc(0x52e);_0x5df615[_0x4abbcc(0x1dd)]=Math['ceil'](_0x5df615[_0x4abbcc(0x4f2)]/0x2),_0x5df615[_0x4abbcc(0x1d2)]=_0x5df615[_0x4abbcc(0x4f2)],_0x5df615['refreshRequest']=!![];for(const _0x3833d3 of _0x2fd5f7){if(!_0x3833d3)continue;_0x3833d3['setHorrorEffectSettings'](_0x11b518,_0x5df615);}$gamePlayer[_0x4abbcc(0x4fc)]();}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x4f0),_0x42aa75=>{const _0x50d39d=_0x53c255;if(!Imported[_0x50d39d(0x787)])return;if(!SceneManager[_0x50d39d(0x45f)]())return;VisuMZ[_0x50d39d(0x5b6)](_0x42aa75,_0x42aa75);const _0x1c29ac=VisuMZ[_0x50d39d(0x6a8)](_0x42aa75['Targets']);for(const _0x1d85f1 of _0x1c29ac){if(!_0x1d85f1)continue;_0x1d85f1[_0x50d39d(0x815)]('glitch');}$gamePlayer[_0x50d39d(0x4fc)]();}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x180),_0x6ad87e=>{const _0x448457=_0x53c255;if(!Imported[_0x448457(0x787)])return;if(!SceneManager[_0x448457(0x45f)]())return;VisuMZ[_0x448457(0x5b6)](_0x6ad87e,_0x6ad87e);const _0x1b86b3=VisuMZ['CreateActionSequenceTargets'](_0x6ad87e['Targets']),_0x4deec1=_0x448457(0x6e5);for(const _0x1abba4 of _0x1b86b3){if(!_0x1abba4)continue;_0x1abba4[_0x448457(0x576)](_0x4deec1,_0x6ad87e);}$gamePlayer[_0x448457(0x4fc)]();}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],'ActSeq_Horror_NoiseRemove',_0x36541f=>{const _0x1d055a=_0x53c255;if(!Imported[_0x1d055a(0x787)])return;if(!SceneManager[_0x1d055a(0x45f)]())return;VisuMZ[_0x1d055a(0x5b6)](_0x36541f,_0x36541f);const _0x726ba2=VisuMZ[_0x1d055a(0x6a8)](_0x36541f['Targets']);for(const _0x26de58 of _0x726ba2){if(!_0x26de58)continue;_0x26de58[_0x1d055a(0x815)]('noise');}$gamePlayer[_0x1d055a(0x4fc)]();}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],'ActSeq_Horror_TVCreate',_0x28d761=>{const _0xf8363e=_0x53c255;if(!Imported[_0xf8363e(0x787)])return;if(!SceneManager[_0xf8363e(0x45f)]())return;VisuMZ[_0xf8363e(0x5b6)](_0x28d761,_0x28d761);const _0x2148d3=VisuMZ[_0xf8363e(0x6a8)](_0x28d761['Targets']),_0x1d221e='tv';for(const _0x451f44 of _0x2148d3){if(!_0x451f44)continue;_0x451f44['setHorrorEffectSettings'](_0x1d221e,_0x28d761);}$gamePlayer[_0xf8363e(0x4fc)]();}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x303),_0x319180=>{const _0x1a9a6a=_0x53c255;if(!Imported[_0x1a9a6a(0x787)])return;if(!SceneManager[_0x1a9a6a(0x45f)]())return;VisuMZ['ConvertParams'](_0x319180,_0x319180);const _0x3e54f0=VisuMZ[_0x1a9a6a(0x6a8)](_0x319180['Targets']);for(const _0xfe3af8 of _0x3e54f0){if(!_0xfe3af8)continue;_0xfe3af8[_0x1a9a6a(0x815)]('tv');}$gamePlayer[_0x1a9a6a(0x4fc)]();}),PluginManager['registerCommand'](pluginData[_0x53c255(0x701)],_0x53c255(0x548),_0x3a7bc4=>{const _0x1f1bb2=_0x53c255;if(!SceneManager[_0x1f1bb2(0x45f)]())return;if(!Imported['VisuMZ_3_ActSeqImpact'])return;const _0x1dcf8=SceneManager[_0x1f1bb2(0x12f)]['_spriteset'];if(!_0x1dcf8)return;VisuMZ[_0x1f1bb2(0x5b6)](_0x3a7bc4,_0x3a7bc4);const _0x5152bc=_0x3a7bc4[_0x1f1bb2(0x4ed)]||0x1,_0x37a559=_0x3a7bc4[_0x1f1bb2(0x2c3)]||0x1,_0x374421=_0x3a7bc4[_0x1f1bb2(0x280)]||_0x1f1bb2(0x611);_0x1dcf8[_0x1f1bb2(0x1a9)](_0x5152bc,_0x37a559,_0x374421);}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x573),_0x5b951f=>{const _0x2700bb=_0x53c255;if(!SceneManager[_0x2700bb(0x45f)]())return;if(!Imported['VisuMZ_3_ActSeqImpact'])return;const _0x25b4f9=SceneManager[_0x2700bb(0x12f)][_0x2700bb(0x3fb)];if(!_0x25b4f9)return;VisuMZ[_0x2700bb(0x5b6)](_0x5b951f,_0x5b951f);const _0x4a778f=Number(_0x5b951f[_0x2700bb(0x40c)])||0x0,_0x56ff8c=Number(_0x5b951f['Rate']),_0x46bd36=_0x5b951f[_0x2700bb(0x2c3)]||0x1,_0x44f04b=_0x5b951f[_0x2700bb(0x280)]||_0x2700bb(0x611);_0x25b4f9['setupMotionBlurImpactFilter'](_0x4a778f,_0x56ff8c,_0x46bd36,_0x44f04b);}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],_0x53c255(0x52c),_0x4a13b4=>{const _0x444902=_0x53c255;if(!SceneManager[_0x444902(0x45f)]())return;if(!Imported[_0x444902(0x78d)])return;const _0x1436ed=SceneManager[_0x444902(0x12f)][_0x444902(0x3fb)];if(!_0x1436ed)return;VisuMZ[_0x444902(0x5b6)](_0x4a13b4,_0x4a13b4);const _0x135c22=Number(_0x4a13b4[_0x444902(0x40c)])||0x0,_0x3bd61c=Number(_0x4a13b4[_0x444902(0x81b)]),_0x196172=_0x4a13b4['Duration']||0x1,_0xcdfc0e=_0x4a13b4['EasingType']||_0x444902(0x611),_0xff6e92=VisuMZ['CreateActionSequenceTargets'](_0x4a13b4[_0x444902(0x489)]);for(const _0x6e4249 of _0xff6e92){if(!_0x6e4249)continue;if(!_0x6e4249['battler']())continue;_0x6e4249[_0x444902(0x5ef)]()['setupMotionBlurImpactFilter'](_0x135c22,_0x3bd61c,_0x196172,_0xcdfc0e);}}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x7a1),_0x1ca02c=>{const _0x1fcc4f=_0x53c255;if(!SceneManager[_0x1fcc4f(0x45f)]())return;if(!Imported[_0x1fcc4f(0x78d)])return;VisuMZ[_0x1fcc4f(0x5b6)](_0x1ca02c,_0x1ca02c);const _0x536e11={'delay':_0x1ca02c[_0x1fcc4f(0x1c4)],'duration':_0x1ca02c[_0x1fcc4f(0x304)],'hue':_0x1ca02c['hue'],'opacityStart':_0x1ca02c[_0x1fcc4f(0x630)],'tone':_0x1ca02c['tone'],'visible':!![]},_0x9305ee=VisuMZ[_0x1fcc4f(0x6a8)](_0x1ca02c[_0x1fcc4f(0x489)]);for(const _0x1ee653 of _0x9305ee){if(!_0x1ee653)continue;_0x1ee653[_0x1fcc4f(0x73c)](_0x536e11);}}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],'ActSeq_Impact_MotionTrailRemove',_0x46d585=>{const _0x24543f=_0x53c255;if(!SceneManager[_0x24543f(0x45f)]())return;if(!Imported[_0x24543f(0x78d)])return;VisuMZ[_0x24543f(0x5b6)](_0x46d585,_0x46d585);const _0x3a2d3d=VisuMZ[_0x24543f(0x6a8)](_0x46d585['Targets']);for(const _0x61bccb of _0x3a2d3d){if(!_0x61bccb)continue;_0x61bccb[_0x24543f(0x1ab)]();}}),PluginManager['registerCommand'](pluginData[_0x53c255(0x701)],'ActSeq_Impact_ShockwavePoint',_0x2699ed=>{const _0xdff3e7=_0x53c255;if(!Imported[_0xdff3e7(0x78d)])return;const _0x10e93e=SceneManager[_0xdff3e7(0x12f)][_0xdff3e7(0x3fb)];if(!_0x10e93e)return;VisuMZ[_0xdff3e7(0x5b6)](_0x2699ed,_0x2699ed);const _0x2002e5=_0x2699ed['X']||0x0,_0x545faf=_0x2699ed['Y']||0x0,_0x1ec93f=_0x2699ed[_0xdff3e7(0x562)]||0x0,_0x16d9c5=_0x2699ed['Wave']||0x0,_0x447d9d=_0x2699ed['Duration']||0x1;_0x10e93e[_0xdff3e7(0x61f)](_0x2002e5,_0x545faf,_0x1ec93f,_0x16d9c5,_0x447d9d);}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],'ActSeq_Impact_ShockwaveEachTargets',_0x2b0aab=>{const _0x4ddfca=_0x53c255;if(!SceneManager[_0x4ddfca(0x45f)]())return;if(!Imported[_0x4ddfca(0x78d)])return;const _0x25c8ff=SceneManager[_0x4ddfca(0x12f)][_0x4ddfca(0x3fb)];if(!_0x25c8ff)return;VisuMZ['ConvertParams'](_0x2b0aab,_0x2b0aab);const _0x325d0c=VisuMZ['CreateActionSequenceTargets'](_0x2b0aab['Targets']),_0x5a66ec=_0x2b0aab['TargetLocation'],_0x4531ab=_0x2b0aab[_0x4ddfca(0x77b)]||0x0,_0x2e91f5=_0x2b0aab[_0x4ddfca(0x6cc)]||0x0,_0x2f7fe4=_0x2b0aab[_0x4ddfca(0x562)]||0x0,_0x14004b=_0x2b0aab[_0x4ddfca(0x480)]||0x0,_0xd789ad=_0x2b0aab['Duration']||0x1;for(const _0x346f6a of _0x325d0c){if(!_0x346f6a)continue;if(!_0x346f6a['battler']())continue;const _0x252b3c=_0x346f6a[_0x4ddfca(0x5ef)]();let _0x4a323f=_0x252b3c[_0x4ddfca(0x396)],_0x1f95e4=_0x252b3c[_0x4ddfca(0x772)];_0x4a323f+=(Graphics['width']-Graphics[_0x4ddfca(0x32c)])/0x2,_0x1f95e4+=(Graphics[_0x4ddfca(0x6ab)]-Graphics[_0x4ddfca(0x75a)])/0x2;if(_0x5a66ec[_0x4ddfca(0x473)](/front/i))_0x4a323f+=(_0x346f6a['isEnemy']()?0x1:-0x1)*_0x252b3c[_0x4ddfca(0x491)]()/0x2;else _0x5a66ec['match'](/back/i)&&(_0x4a323f+=(_0x346f6a[_0x4ddfca(0x26c)]()?-0x1:0x1)*_0x252b3c[_0x4ddfca(0x491)]()/0x2);if(_0x5a66ec[_0x4ddfca(0x473)](/head/i))_0x1f95e4-=_0x252b3c[_0x4ddfca(0x848)]();else _0x5a66ec[_0x4ddfca(0x473)](/center/i)&&(_0x1f95e4-=_0x252b3c[_0x4ddfca(0x848)]()/0x2);_0x4a323f+=_0x4531ab,_0x1f95e4+=_0x2e91f5,_0x25c8ff['setupShockwaveImpactFilter'](_0x4a323f,_0x1f95e4,_0x2f7fe4,_0x14004b,_0xd789ad);}}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],_0x53c255(0x50b),_0x2878fd=>{const _0x324eac=_0x53c255;if(!SceneManager[_0x324eac(0x45f)]())return;if(!Imported[_0x324eac(0x78d)])return;const _0x3367e8=SceneManager[_0x324eac(0x12f)][_0x324eac(0x3fb)];if(!_0x3367e8)return;VisuMZ[_0x324eac(0x5b6)](_0x2878fd,_0x2878fd);const _0x479039=VisuMZ[_0x324eac(0x6a8)](_0x2878fd[_0x324eac(0x489)]),_0xc2525b=_0x2878fd[_0x324eac(0x3b1)],_0xdf27de=_0x2878fd['OffsetX']||0x0,_0x12c117=_0x2878fd[_0x324eac(0x6cc)]||0x0,_0xdc9451=_0x2878fd[_0x324eac(0x562)]||0x0,_0x37a04f=_0x2878fd[_0x324eac(0x480)]||0x0,_0x41b752=_0x2878fd['Duration']||0x1,_0x12d6ba=Math['min'](..._0x479039[_0x324eac(0x19c)](_0x535e7a=>_0x535e7a[_0x324eac(0x5ef)]()['_baseX']-_0x535e7a[_0x324eac(0x5ef)]()[_0x324eac(0x491)]()/0x2)),_0x2f432b=Math[_0x324eac(0x776)](..._0x479039[_0x324eac(0x19c)](_0x25ad9a=>_0x25ad9a[_0x324eac(0x5ef)]()[_0x324eac(0x396)]+_0x25ad9a[_0x324eac(0x5ef)]()[_0x324eac(0x491)]()/0x2)),_0x38562c=Math['min'](..._0x479039[_0x324eac(0x19c)](_0x1eb47c=>_0x1eb47c['battler']()[_0x324eac(0x772)]-_0x1eb47c[_0x324eac(0x5ef)]()[_0x324eac(0x848)]())),_0x1906c1=Math['max'](..._0x479039[_0x324eac(0x19c)](_0x1234bd=>_0x1234bd[_0x324eac(0x5ef)]()['_baseY'])),_0x26ca98=_0x479039[_0x324eac(0x527)](_0x230969=>_0x230969[_0x324eac(0x807)]())['length'],_0x27b710=_0x479039[_0x324eac(0x527)](_0x58a618=>_0x58a618[_0x324eac(0x26c)]())[_0x324eac(0x343)];let _0x47d264=0x0,_0x3c3d49=0x0;if(_0xc2525b[_0x324eac(0x473)](/front/i))_0x47d264=_0x26ca98>=_0x27b710?_0x12d6ba:_0x2f432b;else{if(_0xc2525b[_0x324eac(0x473)](/middle/i))_0x47d264=(_0x12d6ba+_0x2f432b)/0x2,melee=-0x1;else _0xc2525b[_0x324eac(0x473)](/back/i)&&(_0x47d264=_0x26ca98>=_0x27b710?_0x2f432b:_0x12d6ba);}if(_0xc2525b[_0x324eac(0x473)](/head/i))_0x3c3d49=_0x38562c;else{if(_0xc2525b[_0x324eac(0x473)](/center/i))_0x3c3d49=(_0x38562c+_0x1906c1)/0x2;else _0xc2525b[_0x324eac(0x473)](/base/i)&&(_0x3c3d49=_0x1906c1);}_0x47d264+=(Graphics[_0x324eac(0x261)]-Graphics[_0x324eac(0x32c)])/0x2,_0x3c3d49+=(Graphics[_0x324eac(0x6ab)]-Graphics['boxHeight'])/0x2,_0x47d264+=_0xdf27de,_0x3c3d49+=_0x12c117,_0x3367e8[_0x324eac(0x61f)](_0x47d264,_0x3c3d49,_0xdc9451,_0x37a04f,_0x41b752);}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],'ActSeq_Impact_ZoomBlurPoint',_0x1dd5b2=>{const _0x46e1e2=_0x53c255;if(!Imported[_0x46e1e2(0x78d)])return;const _0x145e58=SceneManager[_0x46e1e2(0x12f)][_0x46e1e2(0x3fb)];if(!_0x145e58)return;VisuMZ[_0x46e1e2(0x5b6)](_0x1dd5b2,_0x1dd5b2);const _0xf243d7=_0x1dd5b2['X']||0x0,_0x412578=_0x1dd5b2['Y']||0x0,_0x5ab1fd=_0x1dd5b2[_0x46e1e2(0x2e7)]||0x0,_0x442862=_0x1dd5b2[_0x46e1e2(0x644)]||0x0,_0x560375=_0x1dd5b2[_0x46e1e2(0x2c3)]||0x1,_0x5045b9=_0x1dd5b2[_0x46e1e2(0x280)]||'Linear';_0x145e58[_0x46e1e2(0x81d)](_0x5ab1fd,_0xf243d7,_0x412578,_0x442862,_0x560375,_0x5045b9);}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],'ActSeq_Impact_ZoomBlurTargetCenter',_0x45fb61=>{const _0x2cd479=_0x53c255;if(!Imported[_0x2cd479(0x78d)])return;const _0x25b7ef=SceneManager[_0x2cd479(0x12f)]['_spriteset'];if(!_0x25b7ef)return;VisuMZ[_0x2cd479(0x5b6)](_0x45fb61,_0x45fb61);const _0x2ef040=VisuMZ[_0x2cd479(0x6a8)](_0x45fb61[_0x2cd479(0x489)]),_0x1b593d=_0x45fb61[_0x2cd479(0x3b1)],_0x227a1d=_0x45fb61[_0x2cd479(0x77b)]||0x0,_0x52d43e=_0x45fb61[_0x2cd479(0x6cc)]||0x0,_0x28c2b2=_0x45fb61['Strength']||0x0,_0x18f55b=_0x45fb61[_0x2cd479(0x644)]||0x0,_0x3c5f29=_0x45fb61[_0x2cd479(0x2c3)]||0x1,_0x309a53=_0x45fb61[_0x2cd479(0x280)]||_0x2cd479(0x611),_0x49af67=Math[_0x2cd479(0x413)](..._0x2ef040[_0x2cd479(0x19c)](_0x3c4c82=>_0x3c4c82[_0x2cd479(0x5ef)]()[_0x2cd479(0x396)]-_0x3c4c82[_0x2cd479(0x5ef)]()[_0x2cd479(0x491)]()/0x2)),_0x1951bd=Math[_0x2cd479(0x776)](..._0x2ef040[_0x2cd479(0x19c)](_0x4c1e2b=>_0x4c1e2b['battler']()[_0x2cd479(0x396)]+_0x4c1e2b[_0x2cd479(0x5ef)]()[_0x2cd479(0x491)]()/0x2)),_0x44723f=Math[_0x2cd479(0x413)](..._0x2ef040[_0x2cd479(0x19c)](_0x21752d=>_0x21752d[_0x2cd479(0x5ef)]()[_0x2cd479(0x772)]-_0x21752d[_0x2cd479(0x5ef)]()[_0x2cd479(0x848)]())),_0x582c91=Math[_0x2cd479(0x776)](..._0x2ef040[_0x2cd479(0x19c)](_0x39233f=>_0x39233f['battler']()[_0x2cd479(0x772)])),_0xfad455=_0x2ef040['filter'](_0xe99b62=>_0xe99b62['isActor']())['length'],_0x413b16=_0x2ef040[_0x2cd479(0x527)](_0x3d1865=>_0x3d1865['isEnemy']())[_0x2cd479(0x343)];let _0x4354b7=0x0,_0x4ccf29=0x0;if(_0x1b593d['match'](/front/i))_0x4354b7=_0xfad455>=_0x413b16?_0x49af67:_0x1951bd;else{if(_0x1b593d[_0x2cd479(0x473)](/middle/i))_0x4354b7=(_0x49af67+_0x1951bd)/0x2,melee=-0x1;else _0x1b593d[_0x2cd479(0x473)](/back/i)&&(_0x4354b7=_0xfad455>=_0x413b16?_0x1951bd:_0x49af67);}if(_0x1b593d['match'](/head/i))_0x4ccf29=_0x44723f;else{if(_0x1b593d[_0x2cd479(0x473)](/center/i))_0x4ccf29=(_0x44723f+_0x582c91)/0x2;else _0x1b593d[_0x2cd479(0x473)](/base/i)&&(_0x4ccf29=_0x582c91);}_0x4354b7+=(Graphics[_0x2cd479(0x261)]-Graphics[_0x2cd479(0x32c)])/0x2,_0x4ccf29+=(Graphics[_0x2cd479(0x6ab)]-Graphics[_0x2cd479(0x75a)])/0x2,_0x4354b7+=_0x227a1d,_0x4ccf29+=_0x52d43e,_0x25b7ef['setupZoomBlurImpactFilter'](_0x28c2b2,_0x4354b7,_0x4ccf29,_0x18f55b,_0x3c5f29,_0x309a53);}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x782),_0x545401=>{const _0x234983=_0x53c255;if(!SceneManager[_0x234983(0x45f)]())return;VisuMZ[_0x234983(0x5b6)](_0x545401,_0x545401);const _0x18f29e=$gameTemp[_0x234983(0x238)](),_0x41d112=BattleManager['_action'],_0x113155=BattleManager[_0x234983(0x650)],_0x3f9f2b=BattleManager[_0x234983(0x47a)];if(!_0x18f29e||!_0x41d112||!_0x113155)return;if(!_0x41d112[_0x234983(0x4b7)]())return;const _0x1d79c8=VisuMZ[_0x234983(0x6a8)](_0x545401[_0x234983(0x489)]);for(const _0x42da44 of _0x1d79c8){if(!_0x42da44)continue;_0x3f9f2b[_0x234983(0x2bf)](_0x234983(0x22f),_0x113155,_0x42da44);}_0x18f29e[_0x234983(0x727)](_0x234983(0x79c));}),PluginManager['registerCommand'](pluginData['name'],_0x53c255(0x5bf),_0x21ce54=>{const _0xdd81d4=_0x53c255;if(!SceneManager[_0xdd81d4(0x45f)]())return;VisuMZ[_0xdd81d4(0x5b6)](_0x21ce54,_0x21ce54);const _0x19b2a3=[_0xdd81d4(0x5f5),_0xdd81d4(0x3aa),_0xdd81d4(0x5dc),_0xdd81d4(0x3c7),_0xdd81d4(0x7b8),'MDF','AGI',_0xdd81d4(0x51e)],_0x2ffc2d=_0x21ce54['Buffs'],_0x1c9521=_0x21ce54['Debuffs'],_0x3adbb1=_0x21ce54['Turns'],_0x1f41cf=VisuMZ['CreateActionSequenceTargets'](_0x21ce54[_0xdd81d4(0x489)]);for(const _0x1a0356 of _0x1f41cf){if(!_0x1a0356)continue;for(const _0x22c15e of _0x2ffc2d){const _0x2958ba=_0x19b2a3[_0xdd81d4(0x1d0)](_0x22c15e['toUpperCase']()[_0xdd81d4(0x615)]());_0x2958ba>=0x0&&_0x2958ba<=0x7&&_0x1a0356[_0xdd81d4(0x561)](_0x2958ba,_0x3adbb1);}for(const _0x465eaa of _0x1c9521){const _0x370799=_0x19b2a3[_0xdd81d4(0x1d0)](_0x465eaa['toUpperCase']()[_0xdd81d4(0x615)]());_0x370799>=0x0&&_0x370799<=0x7&&_0x1a0356[_0xdd81d4(0x4d3)](_0x370799,_0x3adbb1);}}}),PluginManager['registerCommand'](pluginData['name'],_0x53c255(0x11e),_0xe7eadc=>{const _0x1250a4=_0x53c255;if(!SceneManager[_0x1250a4(0x45f)]())return;VisuMZ[_0x1250a4(0x5b6)](_0xe7eadc,_0xe7eadc);const _0x42c260=_0xe7eadc[_0x1250a4(0x4b6)],_0x69fc69=VisuMZ['CreateActionSequenceTargets'](_0xe7eadc[_0x1250a4(0x489)]);for(const _0x5758ef of _0x69fc69){if(!_0x5758ef)continue;for(const _0x40d364 of _0x42c260){_0x5758ef[_0x1250a4(0x76e)](_0x40d364);}}}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],_0x53c255(0x53b),_0x542e1b=>{const _0x14c7d5=_0x53c255;if(!SceneManager[_0x14c7d5(0x45f)]())return;VisuMZ['ConvertParams'](_0x542e1b,_0x542e1b);const _0x3462a4=BattleManager[_0x14c7d5(0x213)],_0x169135={'arPenRate':_0x542e1b['ArPenRate'],'arPenFlat':_0x542e1b[_0x14c7d5(0x6ee)],'arRedRate':_0x542e1b[_0x14c7d5(0x23c)],'arRedFlat':_0x542e1b[_0x14c7d5(0x56f)]};_0x3462a4[_0x14c7d5(0x33a)]=_0x169135;}),PluginManager['registerCommand'](pluginData[_0x53c255(0x701)],_0x53c255(0x84c),_0x48d30e=>{const _0x5259e6=_0x53c255;if(!SceneManager[_0x5259e6(0x45f)]())return;if(!Imported[_0x5259e6(0x3fc)])return;VisuMZ[_0x5259e6(0x5b6)](_0x48d30e,_0x48d30e);const _0x528ee2=VisuMZ[_0x5259e6(0x6a8)](_0x48d30e[_0x5259e6(0x489)]),_0x289a31=_0x48d30e['ChargeRate'],_0x447670=_0x48d30e['ChargeRate'],_0xf6117a=_0x48d30e[_0x5259e6(0x33c)];for(const _0x5c589e of _0x528ee2){if(!_0x5c589e)continue;if(_0x5c589e[_0x5259e6(0x695)]())_0x5c589e[_0x5259e6(0x3e4)](_0x289a31);else{if(_0x5c589e[_0x5259e6(0x57f)]()){_0x5c589e['changeAtbCastTime'](_0x447670);if(_0xf6117a)_0x5c589e[_0x5259e6(0x3df)]();}}}}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x1be),_0x37d658=>{const _0x34e45c=_0x53c255;if(!SceneManager[_0x34e45c(0x45f)]())return;if(!Imported[_0x34e45c(0x2e0)])return;VisuMZ['ConvertParams'](_0x37d658,_0x37d658);const _0x43041f=VisuMZ[_0x34e45c(0x6a8)](_0x37d658[_0x34e45c(0x489)]),_0x474ef7=_0x37d658[_0x34e45c(0x3b2)];for(const _0x40d456 of _0x43041f){if(!_0x40d456)continue;_0x40d456[_0x34e45c(0x299)](_0x474ef7);}}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x7aa),_0x4e8888=>{const _0x15905e=_0x53c255;if(!SceneManager[_0x15905e(0x45f)]())return;VisuMZ[_0x15905e(0x5b6)](_0x4e8888,_0x4e8888);const _0x222d6e=$gameTemp[_0x15905e(0x238)](),_0x815ae3=BattleManager[_0x15905e(0x213)],_0x3c684f=BattleManager['_subject'];if(!_0x222d6e||!_0x815ae3||!_0x3c684f)return;if(!_0x815ae3[_0x15905e(0x4b7)]())return;const _0x129b60=VisuMZ[_0x15905e(0x6a8)](_0x4e8888[_0x15905e(0x489)]);for(const _0x563f2f of _0x129b60){if(!_0x563f2f)continue;_0x4e8888[_0x15905e(0x18c)]&&(_0x563f2f[_0x15905e(0x346)](),_0x563f2f[_0x15905e(0x76e)](_0x563f2f[_0x15905e(0x804)]())),_0x563f2f[_0x15905e(0x192)]()&&_0x563f2f[_0x15905e(0x74d)]();}_0x222d6e['setWaitMode'](_0x15905e(0x48e));}),PluginManager['registerCommand'](pluginData['name'],_0x53c255(0x6eb),_0x5b4b4b=>{const _0x308911=_0x53c255;if(!SceneManager['isSceneBattle']())return;if(!Imported[_0x308911(0x26f)])return;VisuMZ[_0x308911(0x5b6)](_0x5b4b4b,_0x5b4b4b);const _0x45bb43=VisuMZ[_0x308911(0x6a8)](_0x5b4b4b[_0x308911(0x489)]),_0x470db6=_0x5b4b4b[_0x308911(0x40d)];for(const _0x41025d of _0x45bb43){if(!_0x41025d)continue;_0x41025d['changeTurnOrderByCTB'](_0x470db6);}}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x1e6),_0x56d654=>{const _0x3f5a13=_0x53c255;if(!SceneManager[_0x3f5a13(0x45f)]())return;if(!Imported[_0x3f5a13(0x26f)])return;VisuMZ[_0x3f5a13(0x5b6)](_0x56d654,_0x56d654);const _0x574339=VisuMZ[_0x3f5a13(0x6a8)](_0x56d654[_0x3f5a13(0x489)]),_0x5aae16=_0x56d654['ChargeRate'],_0x1008a9=_0x56d654[_0x3f5a13(0x102)];for(const _0x5c646c of _0x574339){if(!_0x5c646c)continue;if(_0x5c646c[_0x3f5a13(0x3c2)]===_0x3f5a13(0x477))_0x5c646c[_0x3f5a13(0x468)](_0x5aae16);else _0x5c646c[_0x3f5a13(0x3c2)]===_0x3f5a13(0x141)&&_0x5c646c[_0x3f5a13(0x1e1)](_0x1008a9);}}),PluginManager['registerCommand'](pluginData[_0x53c255(0x701)],_0x53c255(0x24c),_0x19efb3=>{const _0x17c2cc=_0x53c255;if(!SceneManager[_0x17c2cc(0x45f)]())return;VisuMZ[_0x17c2cc(0x5b6)](_0x19efb3,_0x19efb3);const _0x232ac0=BattleManager['_action'];if(!_0x232ac0)return;let _0x40ce97=_0x19efb3[_0x17c2cc(0x443)];_0x232ac0[_0x17c2cc(0x330)](_0x40ce97);}),PluginManager['registerCommand'](pluginData[_0x53c255(0x701)],_0x53c255(0x4f6),_0x4f77cb=>{const _0x30a008=_0x53c255;if(!SceneManager[_0x30a008(0x45f)]())return;VisuMZ['ConvertParams'](_0x4f77cb,_0x4f77cb);const _0x43eb6c=VisuMZ['CreateActionSequenceTargets'](_0x4f77cb[_0x30a008(0x489)]);for(const _0x27ede9 of _0x43eb6c){if(!_0x27ede9)continue;if(_0x27ede9['shouldPopupDamage']())_0x27ede9[_0x30a008(0x2c8)]();}}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],_0x53c255(0x510),_0x3bcc46=>{const _0x402255=_0x53c255;if(!SceneManager[_0x402255(0x45f)]())return;VisuMZ['ConvertParams'](_0x3bcc46,_0x3bcc46);const _0x2956ed=$gameTemp['getLastPluginCommandInterpreter'](),_0x225a1f=BattleManager[_0x402255(0x650)],_0x4c5640=_0x3bcc46['JumpToLabel'];if(!_0x2956ed)return;if(!_0x225a1f)return;_0x225a1f&&_0x225a1f[_0x402255(0x564)]()&&_0x4c5640[_0x402255(0x61d)]()[_0x402255(0x615)]()!=='UNTITLED'&&_0x2956ed[_0x402255(0x43f)]([_0x4c5640]);}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x3de),_0xa5a7b6=>{const _0x3ba046=_0x53c255;if(!SceneManager['isSceneBattle']())return;if(!Imported[_0x3ba046(0x6af)])return;VisuMZ[_0x3ba046(0x5b6)](_0xa5a7b6,_0xa5a7b6);const _0x129a22=_0xa5a7b6['ActionCount'];BattleManager[_0x3ba046(0x650)]&&BattleManager[_0x3ba046(0x650)]['friendsUnit']()['gainCurrentActionsFTB'](_0x129a22);}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],'ActSeq_Mechanics_HpMpTp',_0x2cc5bc=>{const _0x33187=_0x53c255;if(!SceneManager[_0x33187(0x45f)]())return;VisuMZ[_0x33187(0x5b6)](_0x2cc5bc,_0x2cc5bc);const _0x45a8c9=VisuMZ['CreateActionSequenceTargets'](_0x2cc5bc[_0x33187(0x489)]),_0x197d90=_0x2cc5bc['HP_Rate'],_0x4e9245=_0x2cc5bc[_0x33187(0x43d)],_0x83d35=_0x2cc5bc['MP_Rate'],_0x380817=_0x2cc5bc[_0x33187(0x45e)],_0x2d85ba=_0x2cc5bc[_0x33187(0x114)],_0x4326a2=_0x2cc5bc[_0x33187(0x72b)],_0x37099a=_0x2cc5bc[_0x33187(0x4e9)];for(const _0x4b255b of _0x45a8c9){if(!_0x4b255b)continue;const _0x3070cb=_0x4b255b[_0x33187(0x72f)](),_0x17a222=Math['round'](_0x197d90*_0x4b255b['mhp']+_0x4e9245),_0x50689e=Math[_0x33187(0x6c4)](_0x83d35*_0x4b255b['mmp']+_0x380817),_0x521ab6=Math[_0x33187(0x6c4)](_0x2d85ba*_0x4b255b[_0x33187(0x2d6)]()+_0x4326a2);if(_0x17a222!==0x0)_0x4b255b['gainHp'](_0x17a222);if(_0x50689e!==0x0)_0x4b255b[_0x33187(0x667)](_0x50689e);if(_0x521ab6!==0x0)_0x4b255b[_0x33187(0x717)](_0x521ab6);if(_0x37099a)_0x4b255b[_0x33187(0x2c8)]();_0x3070cb&&_0x4b255b[_0x33187(0x564)]()&&_0x4b255b['performCollapse']();}}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x338),_0x56fd40=>{const _0x4fb4e0=_0x53c255;if(!SceneManager[_0x4fb4e0(0x45f)]())return;VisuMZ[_0x4fb4e0(0x5b6)](_0x56fd40,_0x56fd40);const _0x1ae96e=VisuMZ[_0x4fb4e0(0x6a8)](_0x56fd40[_0x4fb4e0(0x489)]);for(const _0x53f30f of _0x1ae96e){if(!_0x53f30f)continue;_0x53f30f[_0x4fb4e0(0x3e2)](_0x56fd40[_0x4fb4e0(0x624)]);}}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x7c6),_0x2730a4=>{const _0x56b60a=_0x53c255;if(!SceneManager[_0x56b60a(0x45f)]())return;VisuMZ[_0x56b60a(0x5b6)](_0x2730a4,_0x2730a4);const _0x37eeb5=BattleManager[_0x56b60a(0x213)],_0x489f9f={'criticalHitRate':_0x2730a4[_0x56b60a(0x7c0)],'criticalHitFlat':_0x2730a4[_0x56b60a(0x440)],'criticalDmgRate':_0x2730a4[_0x56b60a(0x765)],'criticalDmgFlat':_0x2730a4[_0x56b60a(0x158)],'damageRate':_0x2730a4[_0x56b60a(0x688)],'damageFlat':_0x2730a4[_0x56b60a(0x1c8)],'hitRate':_0x2730a4[_0x56b60a(0x5e9)],'hitFlat':_0x2730a4['HitFlat']};_0x37eeb5[_0x56b60a(0x628)]=_0x489f9f;}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x207),_0x4b6f01=>{const _0x5eefc6=_0x53c255;if(!SceneManager[_0x5eefc6(0x45f)]())return;VisuMZ['ConvertParams'](_0x4b6f01,_0x4b6f01);const _0x4ed885=[_0x5eefc6(0x5f5),_0x5eefc6(0x3aa),'ATK','DEF',_0x5eefc6(0x7b8),'MDF',_0x5eefc6(0x279),'LUK'],_0x4f25b2=_0x4b6f01['Buffs'],_0x3c8d77=_0x4b6f01[_0x5eefc6(0x17f)],_0xd9f2f8=VisuMZ[_0x5eefc6(0x6a8)](_0x4b6f01[_0x5eefc6(0x489)]);for(const _0x1ba7c1 of _0xd9f2f8){if(!_0x1ba7c1)continue;for(const _0x1527f9 of _0x4f25b2){const _0xd3e875=_0x4ed885[_0x5eefc6(0x1d0)](_0x1527f9[_0x5eefc6(0x61d)]()[_0x5eefc6(0x615)]());_0xd3e875>=0x0&&_0xd3e875<=0x7&&_0x1ba7c1[_0x5eefc6(0x823)](_0xd3e875)&&_0x1ba7c1[_0x5eefc6(0x108)](_0xd3e875);}for(const _0xd45582 of _0x3c8d77){const _0x147af0=_0x4ed885[_0x5eefc6(0x1d0)](_0xd45582[_0x5eefc6(0x61d)]()['trim']());_0x147af0>=0x0&&_0x147af0<=0x7&&_0x1ba7c1[_0x5eefc6(0x763)](_0x147af0)&&_0x1ba7c1[_0x5eefc6(0x108)](_0x147af0);}}}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x266),_0x48af42=>{const _0x2543fa=_0x53c255;if(!SceneManager['isSceneBattle']())return;VisuMZ[_0x2543fa(0x5b6)](_0x48af42,_0x48af42);const _0x47cdaa=_0x48af42[_0x2543fa(0x4b6)],_0x2f794a=VisuMZ['CreateActionSequenceTargets'](_0x48af42['Targets']);for(const _0x1b11b5 of _0x2f794a){if(!_0x1b11b5)continue;for(const _0x5cdc1b of _0x47cdaa){_0x1b11b5['removeState'](_0x5cdc1b);}}}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x442),_0x1a6279=>{const _0x26db66=_0x53c255;if(!SceneManager[_0x26db66(0x45f)]())return;if(!Imported[_0x26db66(0x367)])return;VisuMZ[_0x26db66(0x5b6)](_0x1a6279,_0x1a6279);const _0x3e1bb0=_0x1a6279[_0x26db66(0x7f2)],_0x34b890=VisuMZ[_0x26db66(0x6a8)](_0x1a6279[_0x26db66(0x489)]),_0x70b775=_0x1a6279[_0x26db66(0x35b)],_0x56ad97=_0x1a6279[_0x26db66(0x7d8)],_0x58e51a=_0x1a6279[_0x26db66(0x4d9)],_0x4e4d5c=BattleManager[_0x26db66(0x213)];if(_0x3e1bb0)for(const _0x397f8c of _0x34b890){if(!_0x397f8c)continue;if(_0x397f8c===user)continue;if(_0x70b775)_0x397f8c[_0x26db66(0x813)](![]);_0x397f8c[_0x26db66(0x59e)](BattleManager[_0x26db66(0x650)],_0x4e4d5c);}if(_0x56ad97&&BattleManager[_0x26db66(0x650)]){if(_0x58e51a)BattleManager[_0x26db66(0x650)][_0x26db66(0x813)](![]);const _0xa42698=_0x34b890[0x0];BattleManager[_0x26db66(0x6b2)](_0xa42698,_0x4e4d5c);}}),PluginManager['registerCommand'](pluginData['name'],'ActSeq_Mechanics_StbExtraAction',_0x4650ee=>{const _0x1e1f48=_0x53c255;if(!SceneManager[_0x1e1f48(0x45f)]())return;if(!Imported['VisuMZ_2_BattleSystemSTB'])return;VisuMZ[_0x1e1f48(0x5b6)](_0x4650ee,_0x4650ee);const _0x4b64e0=_0x4650ee['Actions'];BattleManager[_0x1e1f48(0x650)]&&BattleManager[_0x1e1f48(0x650)]['stbGainInstant'](_0x4b64e0);}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],'ActSeq_Mechanics_StbRemoveExcessActions',_0x1e1943=>{const _0x93d287=_0x53c255;if(!SceneManager['isSceneBattle']())return;if(!Imported[_0x93d287(0x367)])return;VisuMZ[_0x93d287(0x5b6)](_0x1e1943,_0x1e1943);let _0x404773=_0x1e1943[_0x93d287(0x84a)];if(BattleManager[_0x93d287(0x650)]){BattleManager['_subject'][_0x93d287(0x149)]=BattleManager[_0x93d287(0x650)][_0x93d287(0x149)]||[];while(_0x404773--){if(BattleManager['_subject']['_actions'][_0x93d287(0x343)]<=0x0)break;BattleManager['_subject'][_0x93d287(0x149)][_0x93d287(0x68c)]();}}}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],'ActSeq_Mechanics_TextPopup',_0xf87374=>{const _0x299ce8=_0x53c255;if(!SceneManager['isSceneBattle']())return;VisuMZ['ConvertParams'](_0xf87374,_0xf87374);const _0x58cde4=VisuMZ['CreateActionSequenceTargets'](_0xf87374[_0x299ce8(0x489)]),_0x22e290=_0xf87374[_0x299ce8(0x516)],_0xae2545={'textColor':ColorManager[_0x299ce8(0x7fd)](_0xf87374[_0x299ce8(0x7b1)]),'flashColor':_0xf87374[_0x299ce8(0x220)],'flashDuration':_0xf87374['FlashDuration']};for(const _0x3efb0e of _0x58cde4){if(!_0x3efb0e)continue;_0x3efb0e['setupTextPopup'](_0x22e290,_0xae2545);}}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x4ee),_0x1b5589=>{const _0x2c26c1=_0x53c255;if(!SceneManager['isSceneBattle']())return;VisuMZ[_0x2c26c1(0x5b6)](_0x1b5589,_0x1b5589);const _0x17e85d=VisuMZ[_0x2c26c1(0x6a8)](_0x1b5589[_0x2c26c1(0x489)]);let _0x54c316=$gameVariables[_0x2c26c1(0x24b)](_0x1b5589[_0x2c26c1(0x709)]);Imported[_0x2c26c1(0x23a)]&&_0x1b5589[_0x2c26c1(0x41a)]&&(_0x54c316=VisuMZ[_0x2c26c1(0x378)](_0x54c316));const _0x18ffc2=String(_0x54c316),_0x5ad694={'textColor':ColorManager[_0x2c26c1(0x7fd)](_0x1b5589[_0x2c26c1(0x7b1)]),'flashColor':_0x1b5589[_0x2c26c1(0x220)],'flashDuration':_0x1b5589[_0x2c26c1(0x4df)]};for(const _0xa71705 of _0x17e85d){if(!_0xa71705)continue;_0xa71705[_0x2c26c1(0x6e0)](_0x18ffc2,_0x5ad694);}}),PluginManager['registerCommand'](pluginData['name'],_0x53c255(0x54d),_0x415135=>{const _0x141641=_0x53c255;if(!SceneManager[_0x141641(0x45f)]())return;const _0x3cdfdf=$gameTemp[_0x141641(0x238)]();if(!_0x3cdfdf)return;_0x3cdfdf[_0x141641(0x727)](_0x141641(0x48e));}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],_0x53c255(0x5f8),_0x3135ad=>{const _0x598bd0=_0x53c255;if(!SceneManager['isSceneBattle']())return;VisuMZ['ConvertParams'](_0x3135ad,_0x3135ad);const _0x11d79b=VisuMZ['CreateActionSequenceTargets'](_0x3135ad[_0x598bd0(0x489)]);for(const _0x524cd1 of _0x11d79b){if(!_0x524cd1)continue;_0x524cd1[_0x598bd0(0x1e5)]();}}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x5a7),_0x15b27e=>{const _0x3fe631=_0x53c255;if(!SceneManager[_0x3fe631(0x45f)]())return;VisuMZ[_0x3fe631(0x5b6)](_0x15b27e,_0x15b27e);const _0x38b537=VisuMZ['CreateActionSequenceTargets'](_0x15b27e[_0x3fe631(0x489)]),_0x362e79=_0x15b27e[_0x3fe631(0x75f)][_0x3fe631(0x534)]()[_0x3fe631(0x615)](),_0x3aa8cd=_0x15b27e['ShowWeapon'],_0x200688=_0x15b27e[_0x3fe631(0x569)];for(const _0x1b57d1 of _0x38b537){if(!_0x1b57d1)continue;_0x1b57d1['freezeMotion'](_0x362e79,_0x3aa8cd,_0x200688);}}),PluginManager['registerCommand'](pluginData[_0x53c255(0x701)],_0x53c255(0x20a),_0x5e4e3e=>{const _0x2a9db1=_0x53c255;if(!SceneManager[_0x2a9db1(0x45f)]())return;VisuMZ[_0x2a9db1(0x5b6)](_0x5e4e3e,_0x5e4e3e);const _0x53d888=VisuMZ[_0x2a9db1(0x6a8)](_0x5e4e3e[_0x2a9db1(0x489)]),_0xf60d8d=_0x5e4e3e[_0x2a9db1(0x75f)][_0x2a9db1(0x534)]()[_0x2a9db1(0x615)](),_0x1f87ae=_0x5e4e3e[_0x2a9db1(0x6dc)];for(const _0x1ae4fd of _0x53d888){if(!_0x1ae4fd)continue;if(_0xf60d8d['match'](/ATTACK[ ](\d+)/i))_0x1ae4fd['performAttackSlot'](Number(RegExp['$1']));else _0xf60d8d===_0x2a9db1(0x48f)?_0x1ae4fd['performAttack']():_0x1ae4fd[_0x2a9db1(0x2a9)](_0xf60d8d);if(!_0x1f87ae)_0x1ae4fd[_0x2a9db1(0x820)](0x0);else{if(_0x1f87ae&&['thrust',_0x2a9db1(0x14e),_0x2a9db1(0x11a)]['includes'](_0xf60d8d)){}}}}),PluginManager['registerCommand'](pluginData[_0x53c255(0x701)],_0x53c255(0x77a),_0x50d039=>{const _0x36e312=_0x53c255;if(!SceneManager['isSceneBattle']())return;VisuMZ[_0x36e312(0x5b6)](_0x50d039,_0x50d039);const _0x26497d=BattleManager['_action'];if(!_0x26497d)return;if(!_0x26497d[_0x36e312(0x4b7)]())return;const _0x412867=VisuMZ[_0x36e312(0x6a8)](_0x50d039[_0x36e312(0x489)]);for(const _0x24d7f5 of _0x412867){if(!_0x24d7f5)continue;_0x24d7f5[_0x36e312(0x361)](_0x26497d);}}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],'ActSeq_Motion_RefreshMotion',_0x12ff47=>{const _0x3de58d=_0x53c255;if(!SceneManager['isSceneBattle']())return;VisuMZ['ConvertParams'](_0x12ff47,_0x12ff47);const _0x2afe31=VisuMZ[_0x3de58d(0x6a8)](_0x12ff47[_0x3de58d(0x489)]);for(const _0x2c37cc of _0x2afe31){if(!_0x2c37cc)continue;if(!_0x2c37cc['battler']())continue;_0x2c37cc[_0x3de58d(0x5ef)]()['refreshMotion']();}}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x430),_0x331911=>{const _0x1d0e9e=_0x53c255;if(!SceneManager[_0x1d0e9e(0x45f)]())return;VisuMZ[_0x1d0e9e(0x5b6)](_0x331911,_0x331911);const _0x59367c=$gameTemp[_0x1d0e9e(0x238)](),_0x4e5945=_0x331911['MotionFrameWait']*Sprite_Battler[_0x1d0e9e(0x6fb)];_0x59367c[_0x1d0e9e(0x55f)](_0x4e5945);}),PluginManager['registerCommand'](pluginData[_0x53c255(0x701)],_0x53c255(0x67f),_0x225e20=>{const _0x1ebcce=_0x53c255;if(!SceneManager[_0x1ebcce(0x45f)]())return;VisuMZ[_0x1ebcce(0x5b6)](_0x225e20,_0x225e20);const _0x3a8c15=$gameTemp['getLastPluginCommandInterpreter'](),_0x3e0573=BattleManager[_0x1ebcce(0x213)];if(!_0x3a8c15||!_0x3e0573)return;if(!_0x3e0573['item']())return;const _0xa31650=VisuMZ[_0x1ebcce(0x6a8)](_0x225e20['Targets']);for(const _0x1af27a of _0xa31650){if(!_0x1af27a)continue;_0x1af27a[_0x1ebcce(0x5d1)](_0x3e0573);}if(_0x225e20['WaitForMovement'])_0x3a8c15[_0x1ebcce(0x727)](_0x1ebcce(0x285));}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],'ActSeq_Movement_FaceDirection',_0x4abdb3=>{const _0x447ed9=_0x53c255;if(!SceneManager[_0x447ed9(0x45f)]())return;if(!$gameSystem[_0x447ed9(0xdb)]())return;VisuMZ[_0x447ed9(0x5b6)](_0x4abdb3,_0x4abdb3);const _0xe33ec1=VisuMZ['CreateActionSequenceTargets'](_0x4abdb3['Targets']);let _0x283ae0=_0x4abdb3[_0x447ed9(0x689)][_0x447ed9(0x473)](/back/i);for(const _0x116c8c of _0xe33ec1){if(!_0x116c8c)continue;if(_0x4abdb3[_0x447ed9(0x689)][_0x447ed9(0x473)](/rand/i))_0x283ae0=Math[_0x447ed9(0x5d9)](0x2);_0x116c8c[_0x447ed9(0x4aa)](!!_0x283ae0);}}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x7f8),_0x4f2f1d=>{const _0x159663=_0x53c255;if(!SceneManager[_0x159663(0x45f)]())return;if(!$gameSystem[_0x159663(0xdb)]())return;VisuMZ['ConvertParams'](_0x4f2f1d,_0x4f2f1d);const _0x4fffaf=VisuMZ[_0x159663(0x6a8)](_0x4f2f1d[_0x159663(0x489)]);let _0x4c0f04=_0x4f2f1d[_0x159663(0x654)];const _0x2f5dad=_0x4f2f1d[_0x159663(0x476)];for(const _0x354885 of _0x4fffaf){if(!_0x354885)continue;let _0x2292c6=_0x354885[_0x159663(0x5ef)]()[_0x159663(0x396)],_0x52e2ff=_0x354885[_0x159663(0x5ef)]()[_0x159663(0x772)];if(_0x4c0f04[_0x159663(0x473)](/home/i))_0x2292c6=_0x354885[_0x159663(0x5ef)]()[_0x159663(0x67c)],_0x52e2ff=_0x354885['battler']()[_0x159663(0x4a5)];else{if(_0x4c0f04[_0x159663(0x473)](/center/i))_0x2292c6=Graphics[_0x159663(0x32c)]/0x2,_0x52e2ff=Graphics['boxHeight']/0x2;else _0x4c0f04[_0x159663(0x473)](/point (\d+), (\d+)/i)&&(_0x2292c6=Number(RegExp['$1']),_0x52e2ff=Number(RegExp['$2']));}_0x354885[_0x159663(0xfe)](Math['round'](_0x2292c6),Math[_0x159663(0x6c4)](_0x52e2ff),!!_0x2f5dad);}}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],'ActSeq_Movement_FaceTarget',_0x3a49e9=>{const _0x24a461=_0x53c255;if(!SceneManager[_0x24a461(0x45f)]())return;if(!$gameSystem['isSideView']())return;VisuMZ[_0x24a461(0x5b6)](_0x3a49e9,_0x3a49e9);const _0x2f7e87=VisuMZ[_0x24a461(0x6a8)](_0x3a49e9['Targets1']),_0x4dba17=VisuMZ[_0x24a461(0x6a8)](_0x3a49e9[_0x24a461(0x264)]),_0x4bcc59=_0x4dba17[_0x24a461(0x19c)](_0x4c6e1b=>_0x4c6e1b&&_0x4c6e1b[_0x24a461(0x5ef)]()?_0x4c6e1b['battler']()[_0x24a461(0x396)]:0x0)/(_0x4dba17[_0x24a461(0x343)]||0x1),_0x307573=_0x4dba17['map'](_0x2c3d84=>_0x2c3d84&&_0x2c3d84[_0x24a461(0x5ef)]()?_0x2c3d84[_0x24a461(0x5ef)]()[_0x24a461(0x772)]:0x0)/(_0x4dba17[_0x24a461(0x343)]||0x1),_0x1d3919=_0x3a49e9[_0x24a461(0x476)];for(const _0x2218f1 of _0x2f7e87){if(!_0x2218f1)continue;_0x2218f1[_0x24a461(0xfe)](Math[_0x24a461(0x6c4)](_0x4bcc59),Math[_0x24a461(0x6c4)](_0x307573),!!_0x1d3919);}}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],'ActSeq_Movement_Float',_0x5afb52=>{const _0x47d3b1=_0x53c255;if(!SceneManager[_0x47d3b1(0x45f)]())return;VisuMZ[_0x47d3b1(0x5b6)](_0x5afb52,_0x5afb52);const _0x4c07f0=$gameTemp[_0x47d3b1(0x238)](),_0x1ad127=VisuMZ['CreateActionSequenceTargets'](_0x5afb52[_0x47d3b1(0x489)]),_0xb67e71=_0x5afb52[_0x47d3b1(0x434)],_0x3c7985=_0x5afb52[_0x47d3b1(0x2c3)],_0x29636b=_0x5afb52[_0x47d3b1(0x280)],_0x415d17=_0x5afb52[_0x47d3b1(0x403)];if(!_0x4c07f0)return;for(const _0x5de704 of _0x1ad127){if(!_0x5de704)continue;_0x5de704['floatBattler'](_0xb67e71,_0x3c7985,_0x29636b);}if(_0x415d17)_0x4c07f0[_0x47d3b1(0x727)]('battleFloat');}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],'ActSeq_Movement_HomeReset',_0x3f862c=>{const _0x316ca4=_0x53c255;if(!SceneManager['isSceneBattle']())return;VisuMZ[_0x316ca4(0x5b6)](_0x3f862c,_0x3f862c);const _0x59b3f6=$gameTemp[_0x316ca4(0x238)]();if(!_0x59b3f6)return;const _0x307b35=VisuMZ[_0x316ca4(0x6a8)](_0x3f862c['Targets']);for(const _0x444171 of _0x307b35){if(!_0x444171)continue;_0x444171[_0x316ca4(0x5a8)](),_0x444171['performActionEndMembers']();}if(_0x3f862c['WaitForMovement'])_0x59b3f6['setWaitMode'](_0x316ca4(0x285));}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],'ActSeq_Movement_Jump',_0xacf554=>{const _0x1c9395=_0x53c255;if(!SceneManager[_0x1c9395(0x45f)]())return;VisuMZ[_0x1c9395(0x5b6)](_0xacf554,_0xacf554);const _0x15bb42=$gameTemp[_0x1c9395(0x238)](),_0x228eab=VisuMZ[_0x1c9395(0x6a8)](_0xacf554['Targets']),_0xd4e8ed=_0xacf554[_0x1c9395(0x434)],_0xbcf6b3=_0xacf554[_0x1c9395(0x2c3)],_0x4d7f91=_0xacf554[_0x1c9395(0x64b)];if(!_0x15bb42)return;for(const _0x2a316b of _0x228eab){if(!_0x2a316b)continue;_0x2a316b[_0x1c9395(0x1ce)](_0xd4e8ed,_0xbcf6b3);}if(_0x4d7f91)_0x15bb42['setWaitMode'](_0x1c9395(0x43e));}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x23e),_0x3f2a00=>{const _0x5059ea=_0x53c255;if(!SceneManager[_0x5059ea(0x45f)]())return;if(!$gameSystem[_0x5059ea(0xdb)]())return;VisuMZ['ConvertParams'](_0x3f2a00,_0x3f2a00);const _0x3ba427=$gameTemp[_0x5059ea(0x238)](),_0x16dc93=VisuMZ[_0x5059ea(0x6a8)](_0x3f2a00[_0x5059ea(0x489)]),_0x5dfbf5=_0x3f2a00[_0x5059ea(0x134)],_0x202c14=_0x3f2a00[_0x5059ea(0x792)],_0x1536f9=_0x3f2a00[_0x5059ea(0x5d0)],_0x561abb=_0x3f2a00['Duration'],_0x3a60ac=_0x3f2a00[_0x5059ea(0x6ae)],_0x292a82=_0x3f2a00[_0x5059ea(0x280)],_0x410fe8=_0x3f2a00[_0x5059ea(0x75f)],_0x8946e6=_0x3f2a00[_0x5059ea(0x33d)];if(!_0x3ba427)return;for(const _0x9a4e0f of _0x16dc93){if(!_0x9a4e0f)continue;let _0x749406=_0x202c14,_0x2ea9db=_0x1536f9;if(_0x5dfbf5[_0x5059ea(0x473)](/horz/i))_0x749406*=_0x9a4e0f['isActor']()?-0x1:0x1;if(_0x5dfbf5[_0x5059ea(0x473)](/vert/i))_0x2ea9db*=_0x9a4e0f[_0x5059ea(0x807)]()?-0x1:0x1;_0x9a4e0f[_0x5059ea(0x450)](_0x749406,_0x2ea9db,_0x561abb,_0x3a60ac,_0x292a82),_0x9a4e0f[_0x5059ea(0x2a9)](_0x410fe8);}if(_0x8946e6)_0x3ba427[_0x5059ea(0x727)](_0x5059ea(0x285));}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],'ActSeq_Movement_MoveToPoint',_0x1f86ed=>{const _0xc5d3b2=_0x53c255;if(!SceneManager[_0xc5d3b2(0x45f)]())return;if(!$gameSystem['isSideView']())return;VisuMZ[_0xc5d3b2(0x5b6)](_0x1f86ed,_0x1f86ed);const _0x2a262e=$gameTemp[_0xc5d3b2(0x238)](),_0x1d9b4f=VisuMZ[_0xc5d3b2(0x6a8)](_0x1f86ed[_0xc5d3b2(0x489)]),_0x20fbcf=_0x1f86ed[_0xc5d3b2(0x66e)],_0xb5a940=_0x1f86ed['OffsetAdjust'],_0x5f2f20=_0x1f86ed[_0xc5d3b2(0x77b)],_0x3fa22f=_0x1f86ed[_0xc5d3b2(0x6cc)],_0x5c63d5=_0x1f86ed[_0xc5d3b2(0x2c3)],_0x2fa28e=_0x1f86ed[_0xc5d3b2(0x6ae)],_0x47d756=_0x1f86ed[_0xc5d3b2(0x280)],_0x39ca7b=_0x1f86ed[_0xc5d3b2(0x75f)],_0x520fa4=_0x1f86ed[_0xc5d3b2(0x33d)];if(!_0x2a262e)return;for(const _0x27b9c9 of _0x1d9b4f){if(!_0x27b9c9)continue;let _0x78cb78=_0x27b9c9[_0xc5d3b2(0x5ef)]()[_0xc5d3b2(0x396)],_0x4a41fc=_0x27b9c9[_0xc5d3b2(0x5ef)]()[_0xc5d3b2(0x772)];if(_0x20fbcf[_0xc5d3b2(0x473)](/home/i))_0x78cb78=_0x27b9c9[_0xc5d3b2(0x5ef)]()[_0xc5d3b2(0x67c)],_0x4a41fc=_0x27b9c9['battler']()[_0xc5d3b2(0x4a5)];else{if(_0x20fbcf[_0xc5d3b2(0x473)](/center/i))_0x78cb78=Graphics[_0xc5d3b2(0x32c)]/0x2,_0x4a41fc=Graphics[_0xc5d3b2(0x75a)]/0x2;else _0x20fbcf[_0xc5d3b2(0x473)](/point (\d+), (\d+)/i)&&(_0x78cb78=Number(RegExp['$1']),_0x4a41fc=Number(RegExp['$2']));}if(_0xb5a940['match'](/none/i))_0x78cb78+=_0x5f2f20,_0x4a41fc+=_0x3fa22f;else{if(_0xb5a940[_0xc5d3b2(0x473)](/horz/i)&&_0xb5a940[_0xc5d3b2(0x473)](/vert/i))_0x78cb78+=_0x27b9c9['isActor']()?-_0x5f2f20:_0x5f2f20,_0x4a41fc+=_0x27b9c9['isActor']()?-_0x3fa22f:_0x3fa22f;else{if(_0xb5a940[_0xc5d3b2(0x473)](/horz/i))_0x78cb78+=_0x27b9c9[_0xc5d3b2(0x807)]()?-_0x5f2f20:_0x5f2f20,_0x4a41fc+=_0x3fa22f;else _0xb5a940[_0xc5d3b2(0x473)](/vert/i)&&(_0x78cb78+=_0x5f2f20,_0x4a41fc+=_0x27b9c9[_0xc5d3b2(0x807)]()?-_0x3fa22f:_0x3fa22f);}}_0x27b9c9[_0xc5d3b2(0x234)](_0x78cb78,_0x4a41fc,_0x5c63d5,_0x2fa28e,_0x47d756,-0x1),_0x27b9c9[_0xc5d3b2(0x2a9)](_0x39ca7b);}if(_0x520fa4)_0x2a262e['setWaitMode']('battleMove');}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x5be),_0x2915e5=>{const _0x25122e=_0x53c255;if(!SceneManager[_0x25122e(0x45f)]())return;if(!$gameSystem['isSideView']())return;VisuMZ['ConvertParams'](_0x2915e5,_0x2915e5);const _0xa58f1f=$gameTemp[_0x25122e(0x238)](),_0x27ca42=VisuMZ[_0x25122e(0x6a8)](_0x2915e5[_0x25122e(0x5e2)]),_0x17feda=VisuMZ['CreateActionSequenceTargets'](_0x2915e5[_0x25122e(0x264)]),_0x46676c=_0x2915e5[_0x25122e(0x3b1)];let _0x1889d2=_0x2915e5[_0x25122e(0x1d9)];const _0x5316f9=_0x2915e5['OffsetAdjust'],_0x517f8e=_0x2915e5[_0x25122e(0x77b)],_0x50d192=_0x2915e5[_0x25122e(0x6cc)],_0x1dcab2=_0x2915e5[_0x25122e(0x2c3)],_0x5d233b=_0x2915e5[_0x25122e(0x6ae)],_0x50637d=_0x2915e5[_0x25122e(0x280)],_0x51b1ce=_0x2915e5[_0x25122e(0x75f)],_0x10bcc3=_0x2915e5[_0x25122e(0x33d)],_0x3262a7=Math['min'](..._0x17feda[_0x25122e(0x19c)](_0x5778c6=>_0x5778c6[_0x25122e(0x5ef)]()[_0x25122e(0x396)]-_0x5778c6[_0x25122e(0x5ef)]()[_0x25122e(0x491)]()/0x2)),_0x303d34=Math[_0x25122e(0x776)](..._0x17feda[_0x25122e(0x19c)](_0x3bbda9=>_0x3bbda9['battler']()[_0x25122e(0x396)]+_0x3bbda9[_0x25122e(0x5ef)]()[_0x25122e(0x491)]()/0x2)),_0x2223f8=Math[_0x25122e(0x413)](..._0x17feda[_0x25122e(0x19c)](_0x3c3180=>_0x3c3180[_0x25122e(0x5ef)]()[_0x25122e(0x772)]-_0x3c3180['battler']()['mainSpriteHeight']())),_0x551dbb=Math[_0x25122e(0x776)](..._0x17feda['map'](_0x49d999=>_0x49d999[_0x25122e(0x5ef)]()[_0x25122e(0x772)])),_0x52b985=_0x17feda[_0x25122e(0x527)](_0x191c6b=>_0x191c6b[_0x25122e(0x807)]())[_0x25122e(0x343)],_0x5d3c26=_0x17feda['filter'](_0x5214c6=>_0x5214c6['isEnemy']())['length'];let _0x58ec94=0x0,_0x24397d=0x0;if(_0x46676c['match'](/front/i))_0x58ec94=_0x52b985>=_0x5d3c26?_0x3262a7:_0x303d34;else{if(_0x46676c['match'](/middle/i))_0x58ec94=(_0x3262a7+_0x303d34)/0x2,_0x1889d2=-0x1;else _0x46676c['match'](/back/i)&&(_0x58ec94=_0x52b985>=_0x5d3c26?_0x303d34:_0x3262a7);}if(_0x46676c[_0x25122e(0x473)](/head/i))_0x24397d=_0x2223f8;else{if(_0x46676c[_0x25122e(0x473)](/center/i))_0x24397d=(_0x2223f8+_0x551dbb)/0x2;else _0x46676c[_0x25122e(0x473)](/base/i)&&(_0x24397d=_0x551dbb);}if(!_0xa58f1f)return;for(const _0x184fa3 of _0x27ca42){if(!_0x184fa3)continue;let _0x1514c5=_0x58ec94,_0x2a9e0e=_0x24397d;if(_0x5316f9[_0x25122e(0x473)](/none/i))_0x1514c5+=_0x517f8e,_0x2a9e0e+=_0x50d192;else{if(_0x5316f9[_0x25122e(0x473)](/horz/i)&&_0x5316f9['match'](/vert/i))_0x1514c5+=_0x184fa3['isActor']()?-_0x517f8e:_0x517f8e,_0x2a9e0e+=_0x184fa3['isActor']()?-_0x50d192:_0x50d192;else{if(_0x5316f9[_0x25122e(0x473)](/horz/i))_0x1514c5+=_0x184fa3[_0x25122e(0x807)]()?-_0x517f8e:_0x517f8e,_0x2a9e0e+=_0x50d192;else _0x5316f9[_0x25122e(0x473)](/vert/i)&&(_0x1514c5+=_0x517f8e,_0x2a9e0e+=_0x184fa3[_0x25122e(0x807)]()?-_0x50d192:_0x50d192);}}_0x184fa3[_0x25122e(0x234)](_0x1514c5,_0x2a9e0e,_0x1dcab2,_0x5d233b,_0x50637d,_0x1889d2),_0x184fa3[_0x25122e(0x2a9)](_0x51b1ce);}if(_0x10bcc3)_0xa58f1f['setWaitMode'](_0x25122e(0x285));}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],_0x53c255(0x49e),_0x8b4111=>{const _0x4f7361=_0x53c255;if(!SceneManager[_0x4f7361(0x45f)]())return;VisuMZ[_0x4f7361(0x5b6)](_0x8b4111,_0x8b4111);const _0x5c9185=$gameTemp[_0x4f7361(0x238)](),_0x51d577=VisuMZ['CreateActionSequenceTargets'](_0x8b4111[_0x4f7361(0x489)]),_0x3c69cb=_0x8b4111[_0x4f7361(0x5a0)],_0x54cef7=_0x8b4111['Duration'],_0x198f72=_0x8b4111[_0x4f7361(0x280)],_0x4d49ed=_0x8b4111[_0x4f7361(0xf3)];if(!_0x5c9185)return;for(const _0xaa47c8 of _0x51d577){if(!_0xaa47c8)continue;_0xaa47c8[_0x4f7361(0x14b)](_0x3c69cb,_0x54cef7,_0x198f72);}if(_0x4d49ed)_0x5c9185[_0x4f7361(0x727)](_0x4f7361(0x781));}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],'ActSeq_Movement_Scale',_0x454e50=>{const _0x59a72b=_0x53c255;if(!SceneManager['isSceneBattle']())return;VisuMZ['ConvertParams'](_0x454e50,_0x454e50);const _0x31438b=$gameTemp[_0x59a72b(0x238)](),_0x48f744=VisuMZ[_0x59a72b(0x6a8)](_0x454e50[_0x59a72b(0x489)]),_0x38eff1=_0x454e50[_0x59a72b(0x550)],_0x39fd91=_0x454e50[_0x59a72b(0x3e3)],_0x4827cb=_0x454e50[_0x59a72b(0x2c3)],_0x15803f=_0x454e50[_0x59a72b(0x280)],_0x25c5f3=_0x454e50[_0x59a72b(0x3ae)];if(!_0x31438b)return;for(const _0xd53662 of _0x48f744){if(!_0xd53662)continue;_0xd53662[_0x59a72b(0x2c1)](_0x38eff1,_0x39fd91,_0x4827cb,_0x15803f);}if(_0x25c5f3)_0x31438b[_0x59a72b(0x727)](_0x59a72b(0x728));}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x15c),_0x368ad2=>{const _0x344ba4=_0x53c255;if(!SceneManager['isSceneBattle']())return;VisuMZ[_0x344ba4(0x5b6)](_0x368ad2,_0x368ad2);const _0x1fd0e4=$gameTemp[_0x344ba4(0x238)](),_0x1ec304=VisuMZ[_0x344ba4(0x6a8)](_0x368ad2[_0x344ba4(0x489)]),_0x99df8d=_0x368ad2[_0x344ba4(0x135)],_0x1a31d4=_0x368ad2['SkewY'],_0x39385b=_0x368ad2[_0x344ba4(0x2c3)],_0x220ebe=_0x368ad2[_0x344ba4(0x280)],_0x1db283=_0x368ad2[_0x344ba4(0x37d)];if(!_0x1fd0e4)return;for(const _0x1fd16f of _0x1ec304){if(!_0x1fd16f)continue;_0x1fd16f[_0x344ba4(0x40f)](_0x99df8d,_0x1a31d4,_0x39385b,_0x220ebe);}if(_0x1db283)_0x1fd0e4[_0x344ba4(0x727)]('battleSpriteSkew');}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x2fd),_0x11d61d=>{const _0x42d849=_0x53c255;if(!SceneManager[_0x42d849(0x45f)]())return;VisuMZ['ConvertParams'](_0x11d61d,_0x11d61d);const _0x5b38d4=$gameTemp['getLastPluginCommandInterpreter'](),_0x208964=VisuMZ[_0x42d849(0x6a8)](_0x11d61d[_0x42d849(0x489)]),_0xddaf51=_0x11d61d[_0x42d849(0x40c)],_0x37e5de=_0x11d61d[_0x42d849(0x2c3)],_0x1069e9=_0x11d61d[_0x42d849(0x280)],_0x44bcee=_0x11d61d['RevertAngle'],_0x7ef0be=_0x11d61d[_0x42d849(0x5b3)];if(!_0x5b38d4)return;for(const _0x45fd24 of _0x208964){if(!_0x45fd24)continue;_0x45fd24[_0x42d849(0x3f8)](_0xddaf51,_0x37e5de,_0x1069e9,_0x44bcee);}if(_0x7ef0be)_0x5b38d4[_0x42d849(0x727)]('battleSpin');}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x840),_0x21a7ae=>{const _0xce203f=_0x53c255;if(!SceneManager[_0xce203f(0x45f)]())return;const _0x57bb21=$gameTemp[_0xce203f(0x238)]();if(!_0x57bb21)return;_0x57bb21['setWaitMode'](_0xce203f(0x687));}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x3a7),_0x165e34=>{const _0x3eadec=_0x53c255;if(!SceneManager[_0x3eadec(0x45f)]())return;const _0x435605=$gameTemp[_0x3eadec(0x238)]();if(!_0x435605)return;_0x435605[_0x3eadec(0x727)](_0x3eadec(0x43e));}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x427),_0x81abb4=>{const _0x3261d0=_0x53c255;if(!SceneManager[_0x3261d0(0x45f)]())return;const _0x57d427=$gameTemp[_0x3261d0(0x238)]();if(!_0x57d427)return;_0x57d427['setWaitMode']('battleMove');}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x5e1),_0x22cec9=>{const _0x1fd71f=_0x53c255;if(!SceneManager[_0x1fd71f(0x45f)]())return;const _0x777361=$gameTemp['getLastPluginCommandInterpreter']();if(!_0x777361)return;_0x777361['setWaitMode'](_0x1fd71f(0x781));}),PluginManager['registerCommand'](pluginData['name'],_0x53c255(0x3a1),_0x2b01b6=>{const _0x2106cd=_0x53c255;if(!SceneManager[_0x2106cd(0x45f)]())return;const _0x94f770=$gameTemp[_0x2106cd(0x238)]();if(!_0x94f770)return;_0x94f770['setWaitMode'](_0x2106cd(0x728));}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],'ActSeq_Movement_WaitForSkew',_0x202d28=>{const _0x281285=_0x53c255;if(!SceneManager[_0x281285(0x45f)]())return;const _0x15967f=$gameTemp[_0x281285(0x238)]();if(!_0x15967f)return;_0x15967f['setWaitMode'](_0x281285(0x7fc));}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],'ActSeq_Movement_WaitForSpin',_0xf0af1a=>{const _0x512398=_0x53c255;if(!SceneManager[_0x512398(0x45f)]())return;const _0x45e47b=$gameTemp[_0x512398(0x238)]();if(!_0x45e47b)return;_0x45e47b[_0x512398(0x727)](_0x512398(0xda));}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x1ac),_0x562fb4=>{const _0x7a9101=_0x53c255;if(!SceneManager['isSceneBattle']())return;if(!Imported[_0x7a9101(0x771)])return;VisuMZ[_0x7a9101(0x5b6)](_0x562fb4,_0x562fb4);const _0x46ddf9=$gameTemp[_0x7a9101(0x238)](),_0xc58f4b=_0x562fb4[_0x7a9101(0x1f7)];if(!_0x46ddf9)return;const _0x587bb2=BattleManager[_0x7a9101(0x3fb)];if(!_0x587bb2)return;_0x587bb2['createActionSequenceProjectile'](_0x562fb4);if(_0xc58f4b)_0x46ddf9[_0x7a9101(0x727)](_0x7a9101(0x461));}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],'ActSeq_Projectile_Icon',_0x4f7290=>{const _0x376809=_0x53c255;if(!SceneManager[_0x376809(0x45f)]())return;if(!Imported[_0x376809(0x771)])return;VisuMZ['ConvertParams'](_0x4f7290,_0x4f7290);const _0x23bcfb=$gameTemp['getLastPluginCommandInterpreter'](),_0x2ad576=_0x4f7290[_0x376809(0x1f7)];if(!_0x23bcfb)return;const _0x22430a=BattleManager[_0x376809(0x3fb)];if(!_0x22430a)return;_0x22430a[_0x376809(0x1dc)](_0x4f7290);if(_0x2ad576)_0x23bcfb[_0x376809(0x727)](_0x376809(0x461));}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x7e0),_0x5e2694=>{const _0x8a1c53=_0x53c255;if(!SceneManager[_0x8a1c53(0x45f)]())return;if(!Imported[_0x8a1c53(0x771)])return;VisuMZ['ConvertParams'](_0x5e2694,_0x5e2694);const _0x186749=$gameTemp['getLastPluginCommandInterpreter'](),_0x1c3093=_0x5e2694['WaitForProjectile'];if(!_0x186749)return;const _0x76ce45=BattleManager['_spriteset'];if(!_0x76ce45)return;_0x76ce45[_0x8a1c53(0x1dc)](_0x5e2694);if(_0x1c3093)_0x186749['setWaitMode'](_0x8a1c53(0x461));}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],_0x53c255(0x460),_0x23d9d9=>{const _0x2d642c=_0x53c255;if(!SceneManager[_0x2d642c(0x45f)]())return;if(!Imported['VisuMZ_3_ActSeqCamera'])return;VisuMZ[_0x2d642c(0x5b6)](_0x23d9d9,_0x23d9d9);const _0x57714b=$gameTemp['getLastPluginCommandInterpreter'](),_0x2e654c=_0x23d9d9[_0x2d642c(0x37d)];if(!_0x57714b)return;$gameScreen['setBattleSkew'](_0x23d9d9[_0x2d642c(0x135)],_0x23d9d9[_0x2d642c(0x6e4)],_0x23d9d9['Duration'],_0x23d9d9[_0x2d642c(0x280)]);if(_0x2e654c)_0x57714b[_0x2d642c(0x727)](_0x2d642c(0x4cd));}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x2fc),_0x57d55d=>{const _0x4053d2=_0x53c255;if(!SceneManager[_0x4053d2(0x45f)]())return;if(!Imported[_0x4053d2(0x78c)])return;VisuMZ[_0x4053d2(0x5b6)](_0x57d55d,_0x57d55d);const _0x1d63a6=$gameTemp[_0x4053d2(0x238)](),_0x557169=_0x57d55d[_0x4053d2(0x37d)];if(!_0x1d63a6)return;$gameScreen[_0x4053d2(0x200)](0x0,0x0,_0x57d55d[_0x4053d2(0x2c3)],_0x57d55d[_0x4053d2(0x280)]);if(_0x557169)_0x1d63a6['setWaitMode']('battleSkew');}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x80b),_0xc521a8=>{const _0x1af76e=_0x53c255;if(!SceneManager['isSceneBattle']())return;if(!Imported[_0x1af76e(0x78c)])return;const _0x5d9556=$gameTemp['getLastPluginCommandInterpreter']();if(!_0x5d9556)return;_0x5d9556[_0x1af76e(0x727)]('battleSkew');}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x1fc),_0x3d1df0=>{const _0x1e45f1=_0x53c255;if(!SceneManager[_0x1e45f1(0x45f)]())return;VisuMZ[_0x1e45f1(0x5b6)](_0x3d1df0,_0x3d1df0);const _0x50a73e=$gameTemp[_0x1e45f1(0x238)](),_0x2b5219=_0x3d1df0[_0x1e45f1(0x327)],_0x29cb6c=_0x3d1df0[_0x1e45f1(0x6fd)];if(!_0x50a73e)return;BattleManager[_0x1e45f1(0x552)]=_0x2b5219,BattleManager[_0x1e45f1(0x1e9)]=BattleManager[_0x1e45f1(0x370)]?BattleManager[_0x1e45f1(0x370)][BattleManager['_targetIndex']]||null:null,BattleManager[_0x1e45f1(0x1e9)]&&_0x29cb6c['toUpperCase']()[_0x1e45f1(0x615)]()!=='UNTITLED'&&_0x50a73e['command119']([_0x29cb6c]);}),PluginManager['registerCommand'](pluginData['name'],'ActSeq_Target_NextTarget',_0x1604f3=>{const _0xe66743=_0x53c255;if(!SceneManager[_0xe66743(0x45f)]())return;VisuMZ[_0xe66743(0x5b6)](_0x1604f3,_0x1604f3);const _0x374f5d=$gameTemp['getLastPluginCommandInterpreter'](),_0x314bd9=_0x1604f3[_0xe66743(0x6fd)];if(!_0x374f5d)return;BattleManager[_0xe66743(0x552)]++,BattleManager[_0xe66743(0x1e9)]=BattleManager[_0xe66743(0x370)][BattleManager[_0xe66743(0x552)]]||null,BattleManager[_0xe66743(0x1e9)]&&_0x314bd9[_0xe66743(0x61d)]()[_0xe66743(0x615)]()!==_0xe66743(0x38c)&&_0x374f5d[_0xe66743(0x43f)]([_0x314bd9]);}),PluginManager['registerCommand'](pluginData[_0x53c255(0x701)],'ActSeq_Target_PrevTarget',_0x507110=>{const _0xed7272=_0x53c255;if(!SceneManager[_0xed7272(0x45f)]())return;VisuMZ[_0xed7272(0x5b6)](_0x507110,_0x507110);const _0x5416b=$gameTemp[_0xed7272(0x238)](),_0x5bef43=_0x507110['JumpToLabel'];if(!_0x5416b)return;BattleManager[_0xed7272(0x552)]--,BattleManager['_target']=BattleManager['_allTargets'][BattleManager[_0xed7272(0x552)]]||null,BattleManager[_0xed7272(0x1e9)]&&_0x5bef43['toUpperCase']()[_0xed7272(0x615)]()!=='UNTITLED'&&_0x5416b[_0xed7272(0x43f)]([_0x5bef43]);}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],_0x53c255(0x212),_0x2aba7e=>{const _0x84ad9b=_0x53c255;if(!SceneManager['isSceneBattle']())return;VisuMZ[_0x84ad9b(0x5b6)](_0x2aba7e,_0x2aba7e);const _0x37ffb8=$gameTemp['getLastPluginCommandInterpreter'](),_0xf37d4=_0x2aba7e['ForceRandom'],_0x32440d=_0x2aba7e['JumpToLabel'];if(!_0x37ffb8)return;const _0x1b4521=BattleManager[_0x84ad9b(0x552)];for(;;){BattleManager[_0x84ad9b(0x552)]=Math['randomInt'](BattleManager[_0x84ad9b(0x370)][_0x84ad9b(0x343)]);if(!_0xf37d4)break;if(BattleManager['_targetIndex']!==_0x1b4521)break;if(BattleManager[_0x84ad9b(0x370)][_0x84ad9b(0x343)]<=0x1){BattleManager[_0x84ad9b(0x552)]=0x0;break;}}BattleManager['_target']=BattleManager['_allTargets'][BattleManager[_0x84ad9b(0x552)]]||null,BattleManager[_0x84ad9b(0x1e9)]&&_0x32440d[_0x84ad9b(0x61d)]()[_0x84ad9b(0x615)]()!==_0x84ad9b(0x38c)&&_0x37ffb8[_0x84ad9b(0x43f)]([_0x32440d]);}),PluginManager['registerCommand'](pluginData[_0x53c255(0x701)],_0x53c255(0x74a),_0xaa38c5=>{const _0x46bb9b=_0x53c255;if(!SceneManager[_0x46bb9b(0x45f)]())return;VisuMZ['ConvertParams'](_0xaa38c5,_0xaa38c5);const _0x109d40=VisuMZ[_0x46bb9b(0x6a8)](_0xaa38c5[_0x46bb9b(0x489)]);for(const _0x1978b3 of _0x109d40){if(!_0x1978b3)continue;if(!_0x1978b3['isActor']())continue;_0x1978b3[_0x46bb9b(0x187)]();}}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],_0x53c255(0x452),_0x489379=>{const _0x4ecc65=_0x53c255;if(!SceneManager[_0x4ecc65(0x45f)]())return;VisuMZ[_0x4ecc65(0x5b6)](_0x489379,_0x489379);const _0x2c1649=$gameTemp['getLastPluginCommandInterpreter']();let _0x517f1d=![];const _0x4192f9=_0x489379[_0x4ecc65(0x6fd)],_0x53f599=VisuMZ[_0x4ecc65(0x6a8)](_0x489379[_0x4ecc65(0x489)]);for(const _0xb9d455 of _0x53f599){if(!_0xb9d455)continue;if(!_0xb9d455[_0x4ecc65(0x807)]())continue;_0xb9d455[_0x4ecc65(0x486)](),_0xb9d455[_0x4ecc65(0x851)]()[_0x4ecc65(0x343)]>0x0?_0x517f1d=!![]:_0xb9d455[_0x4ecc65(0x187)]();}_0x517f1d&&_0x4192f9[_0x4ecc65(0x61d)]()['trim']()!==_0x4ecc65(0x38c)&&_0x2c1649[_0x4ecc65(0x43f)]([_0x4192f9]);}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],_0x53c255(0x29c),_0x250cc8=>{const _0x3d0d31=_0x53c255;if(!SceneManager[_0x3d0d31(0x45f)]())return;VisuMZ[_0x3d0d31(0x5b6)](_0x250cc8,_0x250cc8);let _0x57415e=_0x250cc8[_0x3d0d31(0x44d)];_0x57415e--,_0x57415e=Math[_0x3d0d31(0x776)](_0x57415e,0x0);const _0x22fd9c=VisuMZ[_0x3d0d31(0x6a8)](_0x250cc8[_0x3d0d31(0x489)]);for(const _0x12b1a0 of _0x22fd9c){if(!_0x12b1a0)continue;if(!_0x12b1a0[_0x3d0d31(0x807)]())continue;_0x12b1a0['setActiveWeaponSlot'](_0x57415e);}}),PluginManager[_0x53c255(0x4cf)](pluginData['name'],_0x53c255(0x42b),_0x1dada1=>{const _0x449983=_0x53c255;if(!SceneManager[_0x449983(0x45f)]())return;if(!Imported[_0x449983(0x78c)])return;VisuMZ[_0x449983(0x5b6)](_0x1dada1,_0x1dada1);const _0x231af7=$gameTemp[_0x449983(0x238)](),_0x5b095e=_0x1dada1[_0x449983(0x533)];if(!_0x231af7)return;$gameScreen['setBattleZoom'](_0x1dada1[_0x449983(0x38e)],_0x1dada1[_0x449983(0x2c3)],_0x1dada1[_0x449983(0x280)]);if(_0x5b095e)_0x231af7[_0x449983(0x727)](_0x449983(0x700));}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],'ActSeq_Zoom_Reset',_0x21bdab=>{const _0x4fbc14=_0x53c255;if(!SceneManager[_0x4fbc14(0x45f)]())return;if(!Imported['VisuMZ_3_ActSeqCamera'])return;VisuMZ['ConvertParams'](_0x21bdab,_0x21bdab);const _0x35f529=$gameTemp[_0x4fbc14(0x238)](),_0x1b6762=_0x21bdab[_0x4fbc14(0x533)];if(!_0x35f529)return;$gameScreen[_0x4fbc14(0x2bc)](0x1,_0x21bdab[_0x4fbc14(0x2c3)],_0x21bdab['EasingType']);if(_0x1b6762)_0x35f529[_0x4fbc14(0x727)](_0x4fbc14(0x700));}),PluginManager[_0x53c255(0x4cf)](pluginData[_0x53c255(0x701)],'ActSeq_Zoom_WaitForZoom',_0x46ba26=>{const _0xe694a8=_0x53c255;if(!SceneManager['isSceneBattle']())return;if(!Imported['VisuMZ_3_ActSeqCamera'])return;const _0xffaa6c=$gameTemp[_0xe694a8(0x238)]();if(!_0xffaa6c)return;_0xffaa6c[_0xe694a8(0x727)](_0xe694a8(0x700));}),VisuMZ['BattleCore'][_0x53c255(0x6dd)]=Scene_Boot[_0x53c255(0x819)]['onDatabaseLoaded'],Scene_Boot[_0x53c255(0x819)][_0x53c255(0x1f8)]=function(){const _0x4888ab=_0x53c255;this[_0x4888ab(0x257)](),this[_0x4888ab(0x315)](),this['process_VisuMZ_BattleCore_DamageStyles'](),this[_0x4888ab(0x5a1)](),VisuMZ['BattleCore'][_0x4888ab(0x6dd)][_0x4888ab(0x6b9)](this),this[_0x4888ab(0x290)](),this[_0x4888ab(0x66f)]();},Scene_Boot[_0x53c255(0x819)][_0x53c255(0x290)]=function(){const _0x529482=_0x53c255;if(VisuMZ[_0x529482(0x436)])return;this['process_VisuMZ_BattleCore_Action_Notetags'](),this[_0x529482(0x21d)](),this['process_VisuMZ_BattleCore_jsFunctions']();},Scene_Boot[_0x53c255(0x819)]['process_VisuMZ_BattleCore_Failsafes']=function(){const _0x22f5ba=_0x53c255,_0x226c1c=$dataSystem['weaponTypes'][_0x22f5ba(0x343)];for(let _0x1a25e2=0x0;_0x1a25e2<_0x226c1c;_0x1a25e2++){const _0x594d1f=$dataSystem['attackMotions'][_0x1a25e2];if(_0x594d1f)continue;$dataSystem['attackMotions'][_0x1a25e2]=JsonEx[_0x22f5ba(0x37a)]($dataSystem[_0x22f5ba(0x7ff)][0x0]);}},Scene_Boot[_0x53c255(0x819)][_0x53c255(0x315)]=function(){const _0x4fb1eb=_0x53c255,_0x9d7a76=VisuMZ['BattleCore']['Settings'];_0x9d7a76[_0x4fb1eb(0x811)][_0x4fb1eb(0x74b)]===undefined&&(_0x9d7a76[_0x4fb1eb(0x811)][_0x4fb1eb(0x74b)]=_0x4fb1eb(0x457)),_0x9d7a76[_0x4fb1eb(0x38f)][_0x4fb1eb(0x38d)]===undefined&&(_0x9d7a76[_0x4fb1eb(0x38f)]['SmoothImage']=![]),_0x9d7a76['Enemy'][_0x4fb1eb(0x38d)]===undefined&&(_0x9d7a76[_0x4fb1eb(0x1c7)][_0x4fb1eb(0x38d)]=!![]),_0x9d7a76['Actor']['PrioritySortActive']===undefined&&(_0x9d7a76[_0x4fb1eb(0x38f)][_0x4fb1eb(0x59d)]=![]),_0x9d7a76['Actor']['PrioritySortActors']===undefined&&(_0x9d7a76['Actor'][_0x4fb1eb(0x2e9)]=!![]);},VisuMZ[_0x53c255(0x560)]={},Scene_Boot[_0x53c255(0x819)]['process_VisuMZ_BattleCore_DamageStyles']=function(){const _0x3a97bd=_0x53c255;for(const _0x2a8ea1 of VisuMZ[_0x3a97bd(0x2cc)][_0x3a97bd(0x4b3)][_0x3a97bd(0x811)][_0x3a97bd(0x53a)]){if(!_0x2a8ea1)continue;const _0x53e077=_0x2a8ea1[_0x3a97bd(0x509)][_0x3a97bd(0x61d)]()[_0x3a97bd(0x615)]();VisuMZ[_0x3a97bd(0x560)][_0x53e077]=_0x2a8ea1;}},VisuMZ['BattleCore'][_0x53c255(0x4bc)]={},Scene_Boot['prototype'][_0x53c255(0x5a1)]=function(){const _0x587343=_0x53c255,_0x1d9f94=VisuMZ[_0x587343(0x2cc)][_0x587343(0x4bc)],_0x24c40d='<%1>\x5cs*([\x5cs\x5cS]*)\x5cs*<\x5c/%1>',_0x89cd5f=[[_0x587343(0x616),_0x587343(0x2e8)],[_0x587343(0x56d),_0x587343(0x4f9)]],_0x4ada1a=[['%1Apply%2JS','JS\x20%1APPLY\x20%2'],[_0x587343(0x808),_0x587343(0x6a9)]],_0x19b0e9=[['',''],[_0x587343(0x24d),_0x587343(0x6bd)],['AsTarget',_0x587343(0x384)]];for(const _0x1c21f0 of _0x4ada1a){for(const _0x287af6 of _0x19b0e9){for(const _0x55c2eb of _0x89cd5f){const _0x37d40a=_0x1c21f0[0x0]['format'](_0x55c2eb[0x0],_0x287af6[0x0]),_0x463a53=_0x1c21f0[0x1][_0x587343(0x107)](_0x55c2eb[0x1],_0x287af6[0x1])[_0x587343(0x615)](),_0x5085e5=new RegExp(_0x24c40d['format'](_0x463a53),'i');_0x1d9f94[_0x37d40a]=_0x5085e5;}}}const _0x88237f=[[_0x587343(0x7db),'JS\x20%1START\x20ACTION'],[_0x587343(0x71f),_0x587343(0x44c)]];for(const _0x2e04b9 of _0x88237f){for(const _0x233e32 of _0x89cd5f){const _0x2c9a13=_0x2e04b9[0x0][_0x587343(0x107)](_0x233e32[0x0]),_0x29e15e=_0x2e04b9[0x1][_0x587343(0x107)](_0x233e32[0x1]),_0x41eaac=new RegExp(_0x24c40d[_0x587343(0x107)](_0x29e15e),'i');_0x1d9f94[_0x2c9a13]=_0x41eaac;}}const _0x3c3a1f=[[_0x587343(0x274),_0x587343(0x643)],[_0x587343(0x402),_0x587343(0x2fe)],[_0x587343(0x749),_0x587343(0x496)],[_0x587343(0x4db),_0x587343(0x2be)],[_0x587343(0x5bc),'JS\x20ESCAPE\x20SUCCESS'],[_0x587343(0x1ed),_0x587343(0x169)],[_0x587343(0x5f9),'JS\x20%1START\x20TURN'],[_0x587343(0x1df),'JS\x20%1END\x20TURN'],['%1RegenerateJS',_0x587343(0x4d0)]];for(const _0x58319a of _0x3c3a1f){for(const _0x31a95d of _0x89cd5f){const _0x23bf9f=_0x58319a[0x0][_0x587343(0x107)](_0x31a95d[0x0]),_0x225e85=_0x58319a[0x1]['format'](_0x31a95d[0x1]),_0x566770=new RegExp(_0x24c40d[_0x587343(0x107)](_0x225e85),'i');_0x1d9f94[_0x23bf9f]=_0x566770;}}},Scene_Boot['prototype']['process_VisuMZ_BattleCore_Action_Notetags']=function(){const _0x1a4fda=_0x53c255,_0x59e1c9=$dataSkills[_0x1a4fda(0x733)]($dataItems);for(const _0x4700db of _0x59e1c9){if(!_0x4700db)continue;VisuMZ[_0x1a4fda(0x2cc)][_0x1a4fda(0x3f5)](_0x4700db);}},Scene_Boot[_0x53c255(0x819)][_0x53c255(0x21d)]=function(){const _0x5be97b=_0x53c255,_0x29e918=$dataActors[_0x5be97b(0x733)]($dataClasses,$dataWeapons,$dataArmors,$dataEnemies,$dataStates);for(const _0x2ba184 of _0x29e918){if(!_0x2ba184)continue;VisuMZ[_0x5be97b(0x2cc)][_0x5be97b(0x6c0)](_0x2ba184);}},Scene_Boot[_0x53c255(0x819)][_0x53c255(0x66f)]=function(){const _0x1d8899=_0x53c255,_0xebcdf4=VisuMZ[_0x1d8899(0x2cc)][_0x1d8899(0x4b3)]['Mechanics'][_0x1d8899(0x13b)],_0x1db3a3=[];for(const _0x189f7c of _0xebcdf4){const _0x1fc15b=$dataTroops[_0x189f7c];if(_0x1fc15b)_0x1db3a3['push'](JsonEx[_0x1d8899(0x37a)](_0x1fc15b));}for(const _0x1dbf66 of $dataTroops){if(!_0x1dbf66)continue;for(const _0x45afb6 of _0x1db3a3){if(_0x45afb6['id']===_0x1dbf66['id'])continue;_0x1dbf66[_0x1d8899(0x2ac)]=_0x1dbf66[_0x1d8899(0x2ac)][_0x1d8899(0x733)](_0x45afb6[_0x1d8899(0x2ac)]);}}},Scene_Boot[_0x53c255(0x819)]['process_VisuMZ_BattleCore_jsFunctions']=function(){const _0x37d3e4=_0x53c255,_0x10ba99=$dataSkills[_0x37d3e4(0x733)]($dataItems);for(const _0x2e0a15 of _0x10ba99){if(!_0x2e0a15)continue;VisuMZ[_0x37d3e4(0x2cc)][_0x37d3e4(0x2db)](_0x2e0a15);}},VisuMZ[_0x53c255(0x2cc)]['ParseActorNotetags']=VisuMZ[_0x53c255(0x405)],VisuMZ[_0x53c255(0x405)]=function(_0x10481a){const _0x3db18e=_0x53c255;VisuMZ[_0x3db18e(0x2cc)][_0x3db18e(0x405)]&&VisuMZ[_0x3db18e(0x2cc)]['ParseActorNotetags'][_0x3db18e(0x6b9)](this,_0x10481a),VisuMZ['BattleCore']['Parse_Notetags_TraitObjects'](_0x10481a);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x79d)]=VisuMZ[_0x53c255(0x79d)],VisuMZ[_0x53c255(0x79d)]=function(_0xec1115){const _0x247fe0=_0x53c255;VisuMZ['BattleCore'][_0x247fe0(0x79d)]&&VisuMZ[_0x247fe0(0x2cc)][_0x247fe0(0x79d)][_0x247fe0(0x6b9)](this,_0xec1115),VisuMZ[_0x247fe0(0x2cc)][_0x247fe0(0x6c0)](_0xec1115);},VisuMZ['BattleCore'][_0x53c255(0x132)]=VisuMZ[_0x53c255(0x132)],VisuMZ[_0x53c255(0x132)]=function(_0x547949){const _0x8e4033=_0x53c255;VisuMZ[_0x8e4033(0x2cc)]['ParseSkillNotetags']&&VisuMZ['BattleCore']['ParseSkillNotetags'][_0x8e4033(0x6b9)](this,_0x547949),VisuMZ[_0x8e4033(0x2cc)][_0x8e4033(0x3f5)](_0x547949),VisuMZ['BattleCore'][_0x8e4033(0x2db)](_0x547949);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x2d0)]=VisuMZ[_0x53c255(0x2d0)],VisuMZ[_0x53c255(0x2d0)]=function(_0x5f2a20){const _0x4f9d0c=_0x53c255;VisuMZ[_0x4f9d0c(0x2cc)][_0x4f9d0c(0x2d0)]&&VisuMZ[_0x4f9d0c(0x2cc)][_0x4f9d0c(0x2d0)][_0x4f9d0c(0x6b9)](this,_0x5f2a20),VisuMZ[_0x4f9d0c(0x2cc)]['Parse_Notetags_Action'](_0x5f2a20),VisuMZ[_0x4f9d0c(0x2cc)][_0x4f9d0c(0x2db)](_0x5f2a20);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x7ed)]=VisuMZ['ParseWeaponNotetags'],VisuMZ[_0x53c255(0x7ed)]=function(_0x5e9085){const _0x4bf019=_0x53c255;VisuMZ[_0x4bf019(0x2cc)][_0x4bf019(0x7ed)]&&VisuMZ[_0x4bf019(0x2cc)][_0x4bf019(0x7ed)][_0x4bf019(0x6b9)](this,_0x5e9085),VisuMZ[_0x4bf019(0x2cc)][_0x4bf019(0x6c0)](_0x5e9085);},VisuMZ[_0x53c255(0x2cc)]['ParseArmorNotetags']=VisuMZ[_0x53c255(0x1d1)],VisuMZ[_0x53c255(0x1d1)]=function(_0x4b8057){const _0x1e22a5=_0x53c255;VisuMZ[_0x1e22a5(0x2cc)][_0x1e22a5(0x1d1)]&&VisuMZ[_0x1e22a5(0x2cc)][_0x1e22a5(0x1d1)][_0x1e22a5(0x6b9)](this,_0x4b8057),VisuMZ[_0x1e22a5(0x2cc)][_0x1e22a5(0x6c0)](_0x4b8057);},VisuMZ['BattleCore'][_0x53c255(0x422)]=VisuMZ[_0x53c255(0x422)],VisuMZ['ParseEnemyNotetags']=function(_0x23af9a){const _0x3d7a59=_0x53c255;VisuMZ[_0x3d7a59(0x2cc)][_0x3d7a59(0x422)]&&VisuMZ[_0x3d7a59(0x2cc)][_0x3d7a59(0x422)][_0x3d7a59(0x6b9)](this,_0x23af9a),VisuMZ[_0x3d7a59(0x2cc)]['Parse_Notetags_TraitObjects'](_0x23af9a);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x57e)]=VisuMZ[_0x53c255(0x57e)],VisuMZ['ParseStateNotetags']=function(_0xef8c){const _0x438cf5=_0x53c255;VisuMZ['BattleCore']['ParseStateNotetags']&&VisuMZ[_0x438cf5(0x2cc)][_0x438cf5(0x57e)][_0x438cf5(0x6b9)](this,_0xef8c),VisuMZ[_0x438cf5(0x2cc)][_0x438cf5(0x6c0)](_0xef8c);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x3f5)]=function(_0x588b7e){const _0x9bdff=_0x53c255,_0x40e395=['PreApplyJS','PostApplyJS',_0x9bdff(0x3b7),'PostDamageJS',_0x9bdff(0x2e3),_0x9bdff(0x7fa),_0x9bdff(0x2b1),_0x9bdff(0x4fb)];for(const _0x3a9da6 of _0x40e395){VisuMZ[_0x9bdff(0x2cc)][_0x9bdff(0x635)](_0x588b7e,_0x3a9da6);}const _0x4fc687=_0x588b7e[_0x9bdff(0x236)];_0x4fc687[_0x9bdff(0x473)](/<ALWAYS CRITICAL/i)&&(_0x588b7e['damage'][_0x9bdff(0x1d8)]=!![]),_0x4fc687[_0x9bdff(0x473)](/<(?:REPEAT|REPEATS|REPEAT HITS):[ ](\d+)/i)&&(_0x588b7e[_0x9bdff(0x7d3)]=Math[_0x9bdff(0x776)](0x1,Number(RegExp['$1']))),_0x4fc687[_0x9bdff(0x473)](/<TARGET:[ ](.*)>/i)&&(_0x588b7e[_0x9bdff(0x1ee)]=String(RegExp['$1'])[_0x9bdff(0x61d)]()[_0x9bdff(0x615)]());},VisuMZ[_0x53c255(0x2cc)]['Parse_Notetags_TraitObjects']=function(_0x3ca506){const _0x272450=_0x53c255,_0x2af7e4=[_0x272450(0x6ed),_0x272450(0x583),'PreDamageAsUserJS',_0x272450(0x25d),_0x272450(0x5c5),_0x272450(0x205),_0x272450(0x245),_0x272450(0x6ff),'PreStartActionJS',_0x272450(0x7fa),_0x272450(0x2b1),_0x272450(0x4fb),_0x272450(0x606),_0x272450(0x5ee),_0x272450(0x74f),_0x272450(0x2f0),_0x272450(0x749),_0x272450(0x4db),_0x272450(0x5bc),_0x272450(0x1ed),_0x272450(0x432),'PostStartTurnJS','PreEndTurnJS','PostEndTurnJS','PreRegenerateJS',_0x272450(0x5a4)];for(const _0x34c4c8 of _0x2af7e4){VisuMZ[_0x272450(0x2cc)][_0x272450(0x635)](_0x3ca506,_0x34c4c8);}},VisuMZ[_0x53c255(0x2cc)]['Parse_Notetags_Targets']=function(_0x109c85){const _0x95ec0b=_0x53c255,_0x9bfbab=_0x109c85['note'];if(_0x9bfbab[_0x95ec0b(0x473)](/<JS TARGETS>\s*([\s\S]*)\s*<\/JS TARGETS>/i)){const _0x4d5c1a=String(RegExp['$1']),_0x1975cb=VisuMZ[_0x95ec0b(0x2cc)][_0x95ec0b(0x172)](_0x109c85,'Targets');VisuMZ[_0x95ec0b(0x2cc)][_0x95ec0b(0x13e)](_0x4d5c1a,_0x1975cb);}if(_0x9bfbab[_0x95ec0b(0x473)](/<JS COMMAND (?:VISIBLE|SHOW|HIDE)>\s*([\s\S]*)\s*<\/JS COMMAND (?:VISIBLE|SHOW|HIDE)>/i)){const _0x1417f7=String(RegExp['$1']),_0x20e116=VisuMZ[_0x95ec0b(0x2cc)][_0x95ec0b(0x172)](_0x109c85,'CommandVisible');VisuMZ[_0x95ec0b(0x2cc)][_0x95ec0b(0x66a)](_0x1417f7,_0x20e116);}},VisuMZ[_0x53c255(0x2cc)]['JS']={},VisuMZ['BattleCore']['createJS']=function(_0x15c10a,_0x558363){const _0x8cb8aa=_0x53c255,_0x2f0bc2=_0x15c10a['note'];if(_0x2f0bc2[_0x8cb8aa(0x473)](VisuMZ['BattleCore'][_0x8cb8aa(0x4bc)][_0x558363])){const _0x234eda=RegExp['$1'],_0x1a891e=_0x8cb8aa(0x846)['format'](_0x234eda),_0x4dd7e9=VisuMZ[_0x8cb8aa(0x2cc)][_0x8cb8aa(0x172)](_0x15c10a,_0x558363);VisuMZ[_0x8cb8aa(0x2cc)]['JS'][_0x4dd7e9]=new Function(_0x1a891e);}},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x172)]=function(_0x3474c6,_0x5e084f){const _0xda373a=_0x53c255;let _0x285146='';if($dataActors[_0xda373a(0x6a6)](_0x3474c6))_0x285146=_0xda373a(0x479)[_0xda373a(0x107)](_0x3474c6['id'],_0x5e084f);if($dataClasses[_0xda373a(0x6a6)](_0x3474c6))_0x285146=_0xda373a(0x7c8)[_0xda373a(0x107)](_0x3474c6['id'],_0x5e084f);if($dataSkills[_0xda373a(0x6a6)](_0x3474c6))_0x285146=_0xda373a(0x6d1)[_0xda373a(0x107)](_0x3474c6['id'],_0x5e084f);if($dataItems['includes'](_0x3474c6))_0x285146='Item-%1-%2'[_0xda373a(0x107)](_0x3474c6['id'],_0x5e084f);if($dataWeapons[_0xda373a(0x6a6)](_0x3474c6))_0x285146='Weapon-%1-%2'[_0xda373a(0x107)](_0x3474c6['id'],_0x5e084f);if($dataArmors[_0xda373a(0x6a6)](_0x3474c6))_0x285146=_0xda373a(0x77d)[_0xda373a(0x107)](_0x3474c6['id'],_0x5e084f);if($dataEnemies[_0xda373a(0x6a6)](_0x3474c6))_0x285146=_0xda373a(0x5af)[_0xda373a(0x107)](_0x3474c6['id'],_0x5e084f);if($dataStates[_0xda373a(0x6a6)](_0x3474c6))_0x285146=_0xda373a(0x2df)[_0xda373a(0x107)](_0x3474c6['id'],_0x5e084f);return _0x285146;},VisuMZ[_0x53c255(0x2cc)]['createTargetsJS']=function(_0x4ec280,_0x5b3c3b){const _0x5abd24=_0x53c255,_0x36863a=_0x5abd24(0x60a)[_0x5abd24(0x107)](_0x4ec280);VisuMZ[_0x5abd24(0x2cc)]['JS'][_0x5b3c3b]=new Function(_0x36863a);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x66a)]=function(_0x5d71bb,_0x4de234){const _0x5a23e8=_0x53c255,_0x482f6e=_0x5a23e8(0x5a3)[_0x5a23e8(0x107)](_0x5d71bb);VisuMZ[_0x5a23e8(0x2cc)]['JS'][_0x4de234]=new Function(_0x482f6e);},TextManager[_0x53c255(0x484)]=VisuMZ['BattleCore'][_0x53c255(0x4b3)]['PartyCmd'][_0x53c255(0x58b)],TextManager['autoBattleStart']=VisuMZ['BattleCore']['Settings']['AutoBattle'][_0x53c255(0x2f7)],TextManager[_0x53c255(0x575)]=VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x4b3)][_0x53c255(0x216)][_0x53c255(0x7b3)],TextManager[_0x53c255(0x7dd)]=VisuMZ[_0x53c255(0x2cc)]['Settings']['HpGauge'][_0x53c255(0x509)],ColorManager[_0x53c255(0x7fd)]=function(_0x12c3fb){const _0x3dc26d=_0x53c255;return _0x12c3fb=String(_0x12c3fb),_0x12c3fb[_0x3dc26d(0x473)](/#(.*)/i)?_0x3dc26d(0x5c3)[_0x3dc26d(0x107)](String(RegExp['$1'])):this[_0x3dc26d(0x648)](Number(_0x12c3fb));},DataManager[_0x53c255(0x287)]=function(_0x14ab7a){const _0x45e936=_0x53c255;if(_0x14ab7a[_0x45e936(0x236)][_0x45e936(0x473)](/<DAMAGE STYLE:[ ](.*)>/i)){const _0x284be9=String(RegExp['$1'])[_0x45e936(0x61d)]()[_0x45e936(0x615)]();if(_0x284be9===_0x45e936(0x3d5))return'MANUAL';if(VisuMZ[_0x45e936(0x560)][_0x284be9])return _0x284be9;}const _0x2b2781=VisuMZ['BattleCore'][_0x45e936(0x4b3)][_0x45e936(0x811)][_0x45e936(0x46d)][_0x45e936(0x61d)]()['trim']();if(VisuMZ[_0x45e936(0x560)][_0x2b2781])return _0x2b2781;return _0x45e936(0x3d5);},DataManager[_0x53c255(0x6b3)]=function(_0x2cc69a){const _0x84bbb4=_0x53c255;_0x2cc69a=_0x2cc69a['toUpperCase']()[_0x84bbb4(0x615)](),this[_0x84bbb4(0x188)]=this[_0x84bbb4(0x188)]||{};if(this['_stypeIDs'][_0x2cc69a])return this['_stypeIDs'][_0x2cc69a];for(let _0x1d0214=0x1;_0x1d0214<0x64;_0x1d0214++){if(!$dataSystem['skillTypes'][_0x1d0214])continue;let _0x34c4d0=$dataSystem[_0x84bbb4(0x761)][_0x1d0214]['toUpperCase']()[_0x84bbb4(0x615)]();_0x34c4d0=_0x34c4d0['replace'](/\x1I\[(\d+)\]/gi,''),_0x34c4d0=_0x34c4d0['replace'](/\\I\[(\d+)\]/gi,''),this[_0x84bbb4(0x188)][_0x34c4d0]=_0x1d0214;}return this[_0x84bbb4(0x188)][_0x2cc69a]||0x0;},DataManager[_0x53c255(0x4d8)]=function(_0x17b2f3){const _0x20730a=_0x53c255;_0x17b2f3=_0x17b2f3[_0x20730a(0x61d)]()[_0x20730a(0x615)](),this['_skillIDs']=this[_0x20730a(0x295)]||{};if(this[_0x20730a(0x295)][_0x17b2f3])return this['_skillIDs'][_0x17b2f3];for(const _0x24216f of $dataSkills){if(!_0x24216f)continue;this['_skillIDs'][_0x24216f[_0x20730a(0x701)][_0x20730a(0x61d)]()['trim']()]=_0x24216f['id'];}return this[_0x20730a(0x295)][_0x17b2f3]||0x0;},DataManager[_0x53c255(0x625)]=function(_0x3e840d){const _0x22c787=_0x53c255;_0x3e840d=_0x3e840d['toUpperCase']()['trim'](),this['_enemyIDs']=this[_0x22c787(0x1a4)]||{};if(this[_0x22c787(0x1a4)][_0x3e840d])return this['_enemyIDs'][_0x3e840d];for(const _0x4d0e31 of $dataEnemies){if(!_0x4d0e31)continue;this[_0x22c787(0x1a4)][_0x4d0e31['name'][_0x22c787(0x61d)]()[_0x22c787(0x615)]()]=_0x4d0e31['id'];}return this[_0x22c787(0x1a4)][_0x3e840d]||0x0;},DataManager[_0x53c255(0x16b)]=function(_0x518386){const _0x3e90a9=_0x53c255;_0x518386=_0x518386[_0x3e90a9(0x61d)]()[_0x3e90a9(0x615)](),this[_0x3e90a9(0x178)]=this[_0x3e90a9(0x178)]||{};if(this[_0x3e90a9(0x178)][_0x518386])return this[_0x3e90a9(0x178)][_0x518386];for(let _0x2bab93=0x1;_0x2bab93<0x64;_0x2bab93++){if(!$dataSystem[_0x3e90a9(0x58d)][_0x2bab93])continue;let _0xf330c7=$dataSystem[_0x3e90a9(0x58d)][_0x2bab93]['toUpperCase']()[_0x3e90a9(0x615)]();_0xf330c7=_0xf330c7[_0x3e90a9(0x3d6)](/\x1I\[(\d+)\]/gi,''),_0xf330c7=_0xf330c7[_0x3e90a9(0x3d6)](/\\I\[(\d+)\]/gi,''),this[_0x3e90a9(0x178)][_0xf330c7]=_0x2bab93;}return this[_0x3e90a9(0x178)]['BARE\x20HANDS']=0x0,this[_0x3e90a9(0x178)][_0x518386]||0x0;},DataManager[_0x53c255(0x4c9)]=function(_0x23dbef){const _0x3142ad=_0x53c255,_0x2dd5ae=_0x3142ad(0x7e3);let _0x522168=_0x23dbef['iconIndex'],_0x5eb265=_0x23dbef[_0x3142ad(0x701)];const _0x279169=_0x23dbef[_0x3142ad(0x236)];return _0x279169[_0x3142ad(0x473)](/<DISPLAY ICON: (\d+)>/i)&&(_0x522168=Number(RegExp['$1'])),_0x279169[_0x3142ad(0x473)](/<DISPLAY TEXT: (.*)>/i)&&(_0x5eb265=String(RegExp['$1'])),_0x2dd5ae[_0x3142ad(0x107)](_0x522168,_0x5eb265);},DataManager['battleCommandName']=function(_0x1f2c04){const _0x41d6d9=_0x53c255;return _0x1f2c04['note'][_0x41d6d9(0x473)](/<COMMAND TEXT: (.*)>/i)?String(RegExp['$1']):_0x1f2c04[_0x41d6d9(0x701)];},DataManager[_0x53c255(0x1c2)]=function(_0x279fb7){const _0x55849f=_0x53c255;return _0x279fb7[_0x55849f(0x236)]['match'](/<COMMAND ICON: (\d+)>/i)?Number(RegExp['$1']):_0x279fb7[_0x55849f(0x5d6)];},DataManager[_0x53c255(0x130)]=function(_0x4ae281){const _0x3be8f5=_0x53c255,_0x2361b8=$dataEnemies[_0x4ae281];if(_0x2361b8){if(_0x2361b8[_0x3be8f5(0x236)][_0x3be8f5(0x473)](/<SWAP ENEMIES>\s*([\s\S]*)\s*<\/SWAP ENEMIES>/i)){const _0x38b64f=String(RegExp['$1'])[_0x3be8f5(0x30d)](/[\r\n]+/)['remove'](''),_0x2d26a4=this[_0x3be8f5(0x18e)](_0x38b64f);_0x4ae281=this[_0x3be8f5(0x625)](_0x2d26a4)||_0x4ae281,_0x4ae281=DataManager[_0x3be8f5(0x130)](_0x4ae281);}}return _0x4ae281;},DataManager['processRandomizedData']=function(_0x455e98){const _0x171975=_0x53c255;let _0xbdc47d=0x0;const _0x1b5303={};for(const _0x57ca2a of _0x455e98){if(_0x57ca2a[_0x171975(0x473)](/(.*):[ ](\d+)/i)){const _0x597f71=String(RegExp['$1'])[_0x171975(0x615)](),_0x2bab29=Number(RegExp['$2']);_0x1b5303[_0x597f71]=_0x2bab29,_0xbdc47d+=_0x2bab29;}else{if(_0x57ca2a[_0x171975(0x473)](/(.*):[ ](\d+\.?\d+)/i)){const _0x2231ab=String(RegExp['$1'])[_0x171975(0x615)](),_0x5b65a6=Number(RegExp['$2']);_0x1b5303[_0x2231ab]=_0x5b65a6,_0xbdc47d+=_0x5b65a6;}else _0x57ca2a!==''&&(_0x1b5303[_0x57ca2a]=0x1,_0xbdc47d++);}}if(_0xbdc47d<=0x0)return'';let _0x526f1a=Math['random']()*_0xbdc47d;for(const _0x437984 in _0x1b5303){_0x526f1a-=_0x1b5303[_0x437984];if(_0x526f1a<=0x0)return _0x437984;}return'';},DataManager[_0x53c255(0x3ff)]=function(_0x4f0913){const _0x546f33=_0x53c255;if(!_0x4f0913)return![];if(!VisuMZ[_0x546f33(0x2cc)][_0x546f33(0x4b3)]['ActionSequence'][_0x546f33(0x438)])return![];if(_0x4f0913['note'][_0x546f33(0x473)](/<AUTO ACTION SEQUENCE>/i))return![];if(_0x4f0913[_0x546f33(0x236)]['match'](/<COMMON (?:EVENT|EVENTS):[ ](.*)>/gi))return!![];for(const _0x76bc87 of _0x4f0913[_0x546f33(0x504)]){if(!_0x76bc87)continue;if(_0x76bc87['code']===Game_Action[_0x546f33(0x1d3)])return!![];}return![];},ConfigManager[_0x53c255(0x674)]=![],ConfigManager[_0x53c255(0x67d)]=![],ConfigManager[_0x53c255(0x7dd)]=!![],VisuMZ['BattleCore']['ConfigManager_makeData']=ConfigManager[_0x53c255(0x159)],ConfigManager[_0x53c255(0x159)]=function(){const _0x236ece=_0x53c255,_0x5d37c3=VisuMZ['BattleCore'][_0x236ece(0x32d)][_0x236ece(0x6b9)](this);return _0x5d37c3[_0x236ece(0x674)]=this['autoBattleAtStart'],_0x5d37c3['autoBattleUseSkills']=this[_0x236ece(0x67d)],_0x5d37c3[_0x236ece(0x7dd)]=this[_0x236ece(0x7dd)],_0x5d37c3;},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x4e0)]=ConfigManager[_0x53c255(0x843)],ConfigManager[_0x53c255(0x843)]=function(_0x4e4c56){const _0x35d90f=_0x53c255;VisuMZ[_0x35d90f(0x2cc)]['ConfigManager_applyData']['call'](this,_0x4e4c56),_0x35d90f(0x674)in _0x4e4c56?this[_0x35d90f(0x674)]=_0x4e4c56[_0x35d90f(0x674)]:this['autoBattleAtStart']=![],_0x35d90f(0x67d)in _0x4e4c56?this['autoBattleUseSkills']=_0x4e4c56[_0x35d90f(0x67d)]:this[_0x35d90f(0x67d)]=![],_0x35d90f(0x7dd)in _0x4e4c56?this['visualHpGauge']=_0x4e4c56[_0x35d90f(0x7dd)]:this[_0x35d90f(0x7dd)]=!![];},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x5c9)]=BattleManager[_0x53c255(0x1ff)],BattleManager[_0x53c255(0x1ff)]=function(){const _0x1e9b94=_0x53c255;VisuMZ[_0x1e9b94(0x2cc)][_0x1e9b94(0x5c9)][_0x1e9b94(0x6b9)](this),this[_0x1e9b94(0x2f5)]=[];},BattleManager[_0x53c255(0x512)]=function(){const _0x33e643=_0x53c255;if(!SceneManager[_0x33e643(0x45f)]())return;const _0x115da7=SceneManager[_0x33e643(0x12f)]['_statusWindow'];if(_0x115da7)_0x115da7['requestRefresh']();},BattleManager['battleSys']=function(){const _0x2dd234=_0x53c255;if(BattleManager[_0x2dd234(0x4ea)]())return _0x2dd234(0x626);return _0x2dd234(0x1ae);},BattleManager[_0x53c255(0x27a)]=function(_0xbe5dac){const _0xb2417d=_0x53c255;return _0xbe5dac=_0xbe5dac[_0xb2417d(0x61d)]()[_0xb2417d(0x615)](),this['battleSys']()===_0xbe5dac;},BattleManager[_0x53c255(0x105)]=function(){const _0x272c42=_0x53c255;return this[_0x272c42(0x27a)]('DTB');},BattleManager[_0x53c255(0x35d)]=function(){const _0x1a90a1=_0x53c255;return this[_0x1a90a1(0x105)]();},BattleManager['isTickBased']=function(){const _0x34cddf=_0x53c255;return!this[_0x34cddf(0x35d)]();},BattleManager[_0x53c255(0x66c)]=function(){const _0x201f33=_0x53c255;return!this['isTurnBased']()&&!this[_0x201f33(0x1b6)]();},BattleManager[_0x53c255(0x195)]=function(_0x1c5e36){const _0x5215d4=_0x53c255;$gameParty[_0x5215d4(0x195)](_0x1c5e36),$gameTroop[_0x5215d4(0x195)](_0x1c5e36);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x520)]=BattleManager['startBattle'],BattleManager['startBattle']=function(){const _0x1747df=_0x53c255;this[_0x1747df(0x796)]=![],this[_0x1747df(0x6ec)]=ConfigManager[_0x1747df(0x674)],this['processBattleCoreJS'](_0x1747df(0x606)),VisuMZ[_0x1747df(0x2cc)]['BattleManager_startBattle'][_0x1747df(0x6b9)](this),this[_0x1747df(0x195)](_0x1747df(0x5ee));},BattleManager['processPostBattleCommonEvents']=function(_0x4c00e9){const _0x1f99af=_0x53c255,_0x5d0d6b=VisuMZ['BattleCore'][_0x1f99af(0x4b3)][_0x1f99af(0x2ff)];_0x5d0d6b[_0x1f99af(0x206)]&&VisuMZ[_0x1f99af(0x2cc)][_0x1f99af(0x2cf)](_0x5d0d6b[_0x1f99af(0x206)])&&$gameTemp['reserveCommonEvent'](_0x5d0d6b[_0x1f99af(0x206)]);const _0x221922='%1Event'[_0x1f99af(0x107)](_0x4c00e9);_0x5d0d6b[_0x221922]&&VisuMZ[_0x1f99af(0x2cc)][_0x1f99af(0x2cf)](_0x5d0d6b[_0x221922])&&$gameTemp[_0x1f99af(0x5ad)](_0x5d0d6b[_0x221922]);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x60d)]=BattleManager[_0x53c255(0x387)],BattleManager[_0x53c255(0x387)]=function(){const _0x1d4288=_0x53c255;this['processBattleCoreJS'](_0x1d4288(0x749)),VisuMZ[_0x1d4288(0x2cc)][_0x1d4288(0x60d)][_0x1d4288(0x6b9)](this),this[_0x1d4288(0x7ad)](_0x1d4288(0x466));},VisuMZ['BattleCore'][_0x53c255(0x1ca)]=BattleManager[_0x53c255(0x6d7)],BattleManager['processDefeat']=function(){const _0x3cf6c8=_0x53c255;this[_0x3cf6c8(0x195)](_0x3cf6c8(0x4db)),VisuMZ[_0x3cf6c8(0x2cc)][_0x3cf6c8(0x1ca)]['call'](this),this['processPostBattleCommonEvents']('Defeat');},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x376)]=BattleManager[_0x53c255(0x4e6)],BattleManager[_0x53c255(0x4e6)]=function(_0x1e5d1e){const _0x496853=_0x53c255;this['_endBattle']=!![],this[_0x496853(0x6ec)]=![],this[_0x496853(0x195)](_0x496853(0x74f)),VisuMZ[_0x496853(0x2cc)][_0x496853(0x376)]['call'](this,_0x1e5d1e),this[_0x496853(0x195)](_0x496853(0x2f0));},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x6a7)]=BattleManager['startTurn'],BattleManager[_0x53c255(0x821)]=function(){const _0x4213f0=_0x53c255;if(this[_0x4213f0(0x35d)]())this[_0x4213f0(0x195)](_0x4213f0(0x432));VisuMZ[_0x4213f0(0x2cc)][_0x4213f0(0x6a7)][_0x4213f0(0x6b9)](this);if(this[_0x4213f0(0x35d)]())this[_0x4213f0(0x195)]('PostStartTurnJS');},VisuMZ[_0x53c255(0x2cc)]['BattleManager_startAction']=BattleManager[_0x53c255(0x3e9)],BattleManager[_0x53c255(0x3e9)]=function(){const _0x2bb37c=_0x53c255,_0x40efcb=this['_subject'][_0x2bb37c(0x522)]();if(_0x40efcb)_0x40efcb[_0x2bb37c(0x4a2)](_0x2bb37c(0x2e3));VisuMZ['BattleCore'][_0x2bb37c(0x5b1)][_0x2bb37c(0x6b9)](this);if(_0x40efcb)_0x40efcb['actionBattleCoreJS']('PostStartActionJS');},VisuMZ[_0x53c255(0x2cc)]['BattleManager_endAction']=BattleManager[_0x53c255(0x82c)],BattleManager['endAction']=function(){const _0x50776d=_0x53c255,_0x411fc7=this['_action'];_0x411fc7&&_0x411fc7[_0x50776d(0x4a2)]('PreEndActionJS'),VisuMZ[_0x50776d(0x2cc)]['BattleManager_endAction']['call'](this),_0x411fc7&&_0x411fc7[_0x50776d(0x4a2)](_0x50776d(0x4fb)),this['refreshBattlerMotions'](this[_0x50776d(0x36a)]());},BattleManager[_0x53c255(0x161)]=function(_0x1f72a0){const _0x25748a=_0x53c255;for(const _0x37b36e of _0x1f72a0){if(!_0x37b36e)continue;if(!_0x37b36e[_0x25748a(0x5ef)]())continue;_0x37b36e[_0x25748a(0x5ef)]()[_0x25748a(0x705)]();}},BattleManager['updateAction']=function(){!this['_logWindow']['isBusy']()&&this['endAction']();},Game_Battler[_0x53c255(0x819)][_0x53c255(0x28e)]=function(){const _0x8c40ae=_0x53c255;this[_0x8c40ae(0x78e)]();if(Imported[_0x8c40ae(0x58c)]){const _0x7abd74=VisuMZ[_0x8c40ae(0x63d)]['Settings'][_0x8c40ae(0x4b6)];_0x7abd74&&_0x7abd74[_0x8c40ae(0x44a)]===![]&&this['removeStatesAuto'](0x1);}else this[_0x8c40ae(0x344)](0x1);this[_0x8c40ae(0x2bb)]();},BattleManager['makeEscapeRatio']=function(){const _0x399dad=_0x53c255;this[_0x399dad(0x106)]=VisuMZ[_0x399dad(0x2cc)][_0x399dad(0x4b3)][_0x399dad(0x2ff)][_0x399dad(0x203)][_0x399dad(0x6b9)](this);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x5db)]=BattleManager[_0x53c255(0x454)],BattleManager[_0x53c255(0x454)]=function(){const _0x1b805f=_0x53c255;this['processBattleCoreJS'](_0x1b805f(0x5bc)),BattleManager[_0x1b805f(0x3fb)][_0x1b805f(0x336)](),VisuMZ[_0x1b805f(0x2cc)][_0x1b805f(0x5db)]['call'](this),this[_0x1b805f(0x7ad)]('EscapeSuccess');},VisuMZ[_0x53c255(0x2cc)]['BattleManager_onEscapeFailure']=BattleManager[_0x53c255(0x655)],BattleManager[_0x53c255(0x655)]=function(){const _0xfb19b4=_0x53c255;this['processBattleCoreJS'](_0xfb19b4(0x1ed));const _0x2b803b=this[_0xfb19b4(0x106)];VisuMZ['BattleCore']['BattleManager_onEscapeFailure'][_0xfb19b4(0x6b9)](this),this['_escapeRatio']=_0x2b803b+VisuMZ[_0xfb19b4(0x2cc)][_0xfb19b4(0x4b3)][_0xfb19b4(0x2ff)][_0xfb19b4(0x2b4)][_0xfb19b4(0x6b9)](this),this['processPostBattleCommonEvents'](_0xfb19b4(0x816));},BattleManager[_0x53c255(0x492)]=function(){const _0x377c3e=_0x53c255;let _0x3bc3bc=![];if(this[_0x377c3e(0x25a)]())for(const _0x18cbe2 of $gameTroop[_0x377c3e(0x578)]()){this[_0x377c3e(0x47a)][_0x377c3e(0x2bf)](_0x377c3e(0x5d3),TextManager[_0x377c3e(0x59c)][_0x377c3e(0x107)](_0x18cbe2)),this[_0x377c3e(0x47a)][_0x377c3e(0x2bf)](_0x377c3e(0x55f)),_0x3bc3bc=!![];}if(this[_0x377c3e(0x7d7)])this[_0x377c3e(0x47a)][_0x377c3e(0x2bf)](_0x377c3e(0x5d3),TextManager[_0x377c3e(0x21c)]['format']($gameParty[_0x377c3e(0x701)]())),this[_0x377c3e(0x47a)]['push']('wait');else this[_0x377c3e(0x517)]&&(this['_logWindow'][_0x377c3e(0x2bf)](_0x377c3e(0x5d3),TextManager[_0x377c3e(0x5f2)][_0x377c3e(0x107)]($gameParty[_0x377c3e(0x701)]())),this[_0x377c3e(0x47a)][_0x377c3e(0x2bf)](_0x377c3e(0x55f)));_0x3bc3bc&&(this[_0x377c3e(0x47a)][_0x377c3e(0x2bf)](_0x377c3e(0x55f)),this[_0x377c3e(0x47a)]['push'](_0x377c3e(0x6fe))),this['isTpb']()&&this[_0x377c3e(0x704)]()&&(this[_0x377c3e(0x6c7)]=![]);},BattleManager[_0x53c255(0x25a)]=function(){const _0x109e6f=_0x53c255;if(BattleManager['_autoBattle'])return![];return VisuMZ['BattleCore'][_0x109e6f(0x4b3)]['Enemy'][_0x109e6f(0x577)];},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x4c4)]=BattleManager[_0x53c255(0x538)],BattleManager[_0x53c255(0x538)]=function(){const _0x517eee=_0x53c255;VisuMZ[_0x517eee(0x2cc)][_0x517eee(0x4c4)][_0x517eee(0x6b9)](this),this[_0x517eee(0x105)]()&&this[_0x517eee(0x704)]()&&!this[_0x517eee(0x517)]&&$gameParty[_0x517eee(0x693)]()&&this[_0x517eee(0x4d1)]();},BattleManager[_0x53c255(0x704)]=function(){const _0x134a2d=_0x53c255;return VisuMZ['BattleCore'][_0x134a2d(0x4b3)][_0x134a2d(0x3d7)][_0x134a2d(0x777)];},BattleManager[_0x53c255(0x734)]=function(){const _0x37c645=_0x53c255;this[_0x37c645(0x35e)]()&&this[_0x37c645(0x4d1)]();},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x79f)]=Scene_Battle[_0x53c255(0x819)][_0x53c255(0x81a)],Scene_Battle['prototype']['startActorCommandSelection']=function(){const _0x43d0c1=_0x53c255;VisuMZ[_0x43d0c1(0x2cc)][_0x43d0c1(0x79f)]['call'](this),BattleManager[_0x43d0c1(0x4ea)]()&&BattleManager['_tpbNeedsPartyCommand']&&(BattleManager[_0x43d0c1(0x6c7)]=![],this[_0x43d0c1(0x536)]());},BattleManager[_0x53c255(0x6b4)]=function(_0x2d2d0d,_0x2807cc){const _0x12ba57=_0x53c255;this['_action']['_reflectionTarget']=_0x2807cc,this[_0x12ba57(0x47a)][_0x12ba57(0x73f)](_0x2807cc),this[_0x12ba57(0x47a)]['displayReflectionPlayBack'](_0x2d2d0d,this['_action']),this[_0x12ba57(0x213)][_0x12ba57(0xd7)](_0x2d2d0d),this[_0x12ba57(0x47a)][_0x12ba57(0x112)](_0x2d2d0d,_0x2d2d0d);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0xf5)]=BattleManager[_0x53c255(0x76c)],BattleManager[_0x53c255(0x76c)]=function(){const _0x214655=_0x53c255;VisuMZ[_0x214655(0x2cc)]['BattleManager_makeActionOrders']['call'](this),this[_0x214655(0x1ef)]=this[_0x214655(0x1ef)][_0x214655(0x527)](_0x1b4de0=>_0x1b4de0&&_0x1b4de0[_0x214655(0x2f8)]());},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x6fc)]=BattleManager[_0x53c255(0x2d7)],BattleManager[_0x53c255(0x2d7)]=function(_0x33ab5a){const _0x5c06b2=_0x53c255;if(this[_0x5c06b2(0x3cc)]==='custom')this[_0x5c06b2(0x2f9)]();else this['_phase']==='forceAction'?this[_0x5c06b2(0x1fa)]():VisuMZ[_0x5c06b2(0x2cc)][_0x5c06b2(0x6fc)][_0x5c06b2(0x6b9)](this,_0x33ab5a);},BattleManager[_0x53c255(0x65e)]=function(){const _0x286f45=_0x53c255;this['_allTargets']=this[_0x286f45(0x745)][_0x286f45(0x599)](0x0),this[_0x286f45(0x552)]=0x0,this[_0x286f45(0x1e9)]=this[_0x286f45(0x370)][0x0]||null,this[_0x286f45(0x3cc)]=_0x286f45(0x6c2);},BattleManager[_0x53c255(0x2f9)]=function(){const _0x254161=_0x53c255;!this['updateEventMain']()&&!this[_0x254161(0x47a)][_0x254161(0x456)]()&&(this[_0x254161(0x3cc)]=_0x254161(0x140));},BattleManager[_0x53c255(0x6a4)]=function(_0x121702){const _0x31c257=_0x53c255;this[_0x31c257(0x1ef)][_0x31c257(0x725)](_0x121702);if(_0x121702===this[_0x31c257(0x650)])return;const _0x3cd45e=JsonEx[_0x31c257(0x37a)](_0x121702['currentAction']());this[_0x31c257(0x2f5)][_0x31c257(0x2bf)]([_0x121702,_0x3cd45e]);},BattleManager[_0x53c255(0x54c)]=function(){},BattleManager[_0x53c255(0x3ec)]=function(){const _0x509f39=_0x53c255;if(this[_0x509f39(0x4ea)]())this['_phase']=_0x509f39(0x7e9);else this[_0x509f39(0x2f5)][_0x509f39(0x343)]>0x0?this[_0x509f39(0x3cc)]='turn':this[_0x509f39(0x538)]();},BattleManager[_0x53c255(0x661)]=function(){const _0xbd325b=_0x53c255,_0x107137=this[_0xbd325b(0x650)];_0x107137&&this[_0xbd325b(0x4ea)]()&&_0x107137[_0xbd325b(0x125)](_0xbd325b(0x27b));for(;;){const _0x3c29b0=this[_0xbd325b(0x277)]();if(!_0x3c29b0)return null;if(_0x3c29b0[_0xbd325b(0x558)]()&&_0x3c29b0['isAlive']())return _0x3c29b0;}},BattleManager[_0x53c255(0x277)]=function(){const _0xe78b25=_0x53c255;if(this[_0xe78b25(0x2f5)]['length']>0x0){const _0x1e27ed=this[_0xe78b25(0x2f5)]['shift'](),_0x168ed0=_0x1e27ed[0x0];return _0x168ed0['_actions']=_0x168ed0[_0xe78b25(0x149)]||[],_0x168ed0[_0xe78b25(0x149)][0x0]=_0x1e27ed[0x1],_0x168ed0;}else return this[_0xe78b25(0x1ef)][_0xe78b25(0x68c)]();},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x50e)]=Game_Battler[_0x53c255(0x819)][_0x53c255(0x6a4)],Game_Battler[_0x53c255(0x819)][_0x53c255(0x6a4)]=function(_0x5174de,_0x497f7e){const _0x175c39=_0x53c255;VisuMZ['BattleCore'][_0x175c39(0x50e)][_0x175c39(0x6b9)](this,_0x5174de,_0x497f7e),this[_0x175c39(0x149)][this[_0x175c39(0x149)][_0x175c39(0x343)]-0x1][_0x175c39(0x7ef)]=!![];},Game_Interpreter[_0x53c255(0x819)][_0x53c255(0x696)]=function(_0x1507eb){return this['iterateBattler'](_0x1507eb[0x0],_0x1507eb[0x1],_0x295890=>{const _0x4ba08d=_0x567a;!_0x295890[_0x4ba08d(0x192)]()&&(_0x295890[_0x4ba08d(0x6a4)](_0x1507eb[0x2],_0x1507eb[0x3]),BattleManager[_0x4ba08d(0x6a4)](_0x295890));}),!![];},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x194)]=Game_Battler[_0x53c255(0x819)][_0x53c255(0x17b)],Game_Battler[_0x53c255(0x819)][_0x53c255(0x17b)]=function(){const _0x4ea762=_0x53c255;VisuMZ[_0x4ea762(0x2cc)][_0x4ea762(0x194)][_0x4ea762(0x6b9)](this),this['_actions'][_0x4ea762(0x343)]<=0x0&&(this['_speed']=Number[_0x4ea762(0x1cb)]);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x241)]=BattleManager[_0x53c255(0x4d1)],BattleManager[_0x53c255(0x4d1)]=function(){const _0x2c52af=_0x53c255;this['isTpb']()?this[_0x2c52af(0x41c)]():VisuMZ[_0x2c52af(0x2cc)][_0x2c52af(0x241)][_0x2c52af(0x6b9)](this);},BattleManager[_0x53c255(0x41c)]=function(){const _0x4cd3c8=_0x53c255;if(this[_0x4cd3c8(0x51b)]){if(this[_0x4cd3c8(0x51b)][_0x4cd3c8(0x4d1)]())return;this[_0x4cd3c8(0x85a)](),this['checkTpbInputClose'](),!this[_0x4cd3c8(0x650)]&&!this['_currentActor']&&SceneManager[_0x4cd3c8(0x12f)][_0x4cd3c8(0x679)]();}else!this[_0x4cd3c8(0x650)]&&this[_0x4cd3c8(0x84f)]();},BattleManager['checkTpbInputClose']=function(){const _0x429737=_0x53c255;(!this[_0x429737(0x35e)]()||this['needsActorInputCancel']())&&(this[_0x429737(0x5e5)]&&(!$gameParty[_0x429737(0x116)]()[_0x429737(0x6a6)](this[_0x429737(0x5e5)])&&(this[_0x429737(0x5e5)]=null)),!this[_0x429737(0x5e5)]?(this[_0x429737(0x13c)](),this[_0x429737(0x51b)]=null,this[_0x429737(0x297)]=![]):this[_0x429737(0x168)]());},BattleManager[_0x53c255(0x168)]=function(){const _0x622109=_0x53c255;!$gameParty['battleMembers']()['includes'](this[_0x622109(0x5e5)])&&(this['_tpbSceneChangeCacheActor']=null),this[_0x622109(0x5e5)]?(this[_0x622109(0x51b)]=this[_0x622109(0x5e5)],this['_currentActor']['_tpbState']='charged',this[_0x622109(0x297)]=!![],this[_0x622109(0x5e5)]=null):(this[_0x622109(0x13c)](),this[_0x622109(0x51b)]=null,this[_0x622109(0x297)]=![]);},VisuMZ['BattleCore']['BattleManager_isTpbMainPhase']=BattleManager['isTpbMainPhase'],BattleManager['isTpbMainPhase']=function(){const _0x3fc5dd=_0x53c255;return this['_phase']==='custom'?this['battleCoreTpbMainPhase']():VisuMZ[_0x3fc5dd(0x2cc)][_0x3fc5dd(0x71b)][_0x3fc5dd(0x6b9)](this);},BattleManager[_0x53c255(0x3a0)]=function(){return this['isActiveTpb']();},VisuMZ['BattleCore'][_0x53c255(0x18a)]=BattleManager[_0x53c255(0x13c)],BattleManager[_0x53c255(0x13c)]=function(){const _0x43b033=_0x53c255;this[_0x43b033(0x4ea)]()&&this['_phase']==='battleEnd'&&(this['_currentActor']=null),VisuMZ[_0x43b033(0x2cc)][_0x43b033(0x18a)][_0x43b033(0x6b9)](this);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x10c)]=BattleManager[_0x53c255(0x48d)],BattleManager[_0x53c255(0x48d)]=function(){const _0x2945e4=_0x53c255,_0x47fb7c=this[_0x2945e4(0x51b)];if(_0x47fb7c&&!_0x47fb7c['inputtingAction']()){const _0x51f0b4=_0x47fb7c[_0x2945e4(0x83c)];_0x47fb7c[_0x2945e4(0x149)][_0x51f0b4]=new Game_Action(_0x47fb7c);}return VisuMZ[_0x2945e4(0x2cc)][_0x2945e4(0x10c)][_0x2945e4(0x6b9)](this);},SceneManager['isSceneBattle']=function(){const _0x3a1dd7=_0x53c255;return this[_0x3a1dd7(0x12f)]&&this['_scene'][_0x3a1dd7(0x70f)]===Scene_Battle;},SceneManager[_0x53c255(0x32a)]=function(){const _0xec8766=_0x53c255;return Spriteset_Battle[_0xec8766(0x819)]['isFlipped']();},SceneManager[_0x53c255(0x791)]=function(){const _0x467a58=_0x53c255;if(SceneManager[_0x467a58(0x593)](Scene_Options))return!![];return![];},SceneManager[_0x53c255(0x28c)]=function(){const _0x59e456=_0x53c255;if(SceneManager[_0x59e456(0x49b)](Scene_Options))return!![];return![];},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x755)]=Game_Temp[_0x53c255(0x819)][_0x53c255(0x308)],Game_Temp[_0x53c255(0x819)][_0x53c255(0x308)]=function(_0x5ee6f4,_0x459c3c,_0xedd110){const _0x54f8cc=_0x53c255;_0x5ee6f4=_0x5ee6f4[_0x54f8cc(0x527)]((_0x3e5703,_0x3bc736,_0x98e1be)=>_0x98e1be[_0x54f8cc(0x1d0)](_0x3e5703)===_0x3bc736),SceneManager[_0x54f8cc(0x45f)]()&&SceneManager['isBattleFlipped']()&&(_0xedd110=!_0xedd110),VisuMZ[_0x54f8cc(0x2cc)][_0x54f8cc(0x755)][_0x54f8cc(0x6b9)](this,_0x5ee6f4,_0x459c3c,_0xedd110),SceneManager[_0x54f8cc(0x45f)]()&&BattleManager[_0x54f8cc(0x3fb)]['processAnimationRequests']();},Game_Temp['prototype'][_0x53c255(0x633)]=function(_0x377566){const _0x36c351=_0x53c255;this[_0x36c351(0x2ea)]=_0x377566;},Game_Temp['prototype'][_0x53c255(0x238)]=function(){return this['_lastPluginCommandInterpreter'];},Game_Temp[_0x53c255(0x819)]['clearForcedGameTroopSettingsBattleCore']=function(){const _0xc87d1b=_0x53c255;this[_0xc87d1b(0x519)]=undefined;},Game_Temp[_0x53c255(0x819)][_0x53c255(0x32e)]=function(_0x4802dc){const _0x5269c1=_0x53c255;$gameMap&&$dataMap&&$dataMap[_0x5269c1(0x236)]&&this['parseForcedGameTroopSettingsBattleCore']($dataMap['note']);const _0x647a30=$dataTroops[_0x4802dc];_0x647a30&&this['parseForcedGameTroopSettingsBattleCore'](_0x647a30[_0x5269c1(0x701)]);},Game_Temp[_0x53c255(0x819)]['parseForcedGameTroopSettingsBattleCore']=function(_0x450086){const _0x219b1e=_0x53c255;if(!_0x450086)return;if(_0x450086['match'](/<(?:BATTLELAYOUT|BATTLE LAYOUT|LAYOUT):[ ](.*)>/i)){const _0xa2851b=String(RegExp['$1']);if(_0xa2851b['match'](/DEFAULT/i))this['_forcedBattleLayout']=_0x219b1e(0x1cf);else{if(_0xa2851b[_0x219b1e(0x473)](/LIST/i))this['_forcedBattleLayout']=_0x219b1e(0x4a9);else{if(_0xa2851b['match'](/XP/i))this[_0x219b1e(0x519)]='xp';else{if(_0xa2851b[_0x219b1e(0x473)](/PORTRAIT/i))this['_forcedBattleLayout']=_0x219b1e(0x6bf);else _0xa2851b[_0x219b1e(0x473)](/BORDER/i)&&(this[_0x219b1e(0x519)]='border');}}}}},VisuMZ[_0x53c255(0x2cc)]['Game_System_initialize']=Game_System[_0x53c255(0x819)]['initialize'],Game_System[_0x53c255(0x819)][_0x53c255(0x854)]=function(){const _0x28aedf=_0x53c255;VisuMZ['BattleCore'][_0x28aedf(0x1da)][_0x28aedf(0x6b9)](this),this['initBattleCore']();},Game_System['prototype']['initBattleCore']=function(){const _0x13fc86=_0x53c255;this[_0x13fc86(0x639)]=this[_0x13fc86(0x639)]||[];},Game_System['prototype']['getDefeatedEnemies']=function(){const _0x3637d8=_0x53c255;if(this[_0x3637d8(0x639)]===undefined)this['initBattleCore']();return this[_0x3637d8(0x639)];},Game_System[_0x53c255(0x819)]['registerDefeatedEnemy']=function(_0x1e1b10){const _0x145adb=_0x53c255;if(this[_0x145adb(0x639)]===undefined)this[_0x145adb(0x311)]();if(!_0x1e1b10)return;if(this[_0x145adb(0x639)][_0x145adb(0x6a6)](_0x1e1b10))return;this[_0x145adb(0x639)]['push'](_0x1e1b10),this[_0x145adb(0x639)]['sort']((_0x54c9c1,_0x38fabc)=>_0x54c9c1-_0x38fabc);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x3c3)]=Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x673)],Game_BattlerBase['prototype'][_0x53c255(0x673)]=function(_0x250aab){const _0x45b541=_0x53c255,_0x47b881=this[_0x45b541(0x72f)](),_0x5815c0=this[_0x45b541(0x4f1)]();VisuMZ[_0x45b541(0x2cc)]['Game_BattlerBase_addNewState']['call'](this,_0x250aab),this[_0x45b541(0x26c)]()&&_0x47b881&&this[_0x45b541(0x564)]()&&(this[_0x45b541(0x109)]=!this['hasBeenDefeatedBefore'](),$gameSystem['registerDefeatedEnemy'](this[_0x45b541(0x832)]())),SceneManager[_0x45b541(0x45f)]()&&_0x5815c0!==this[_0x45b541(0x4f1)]()&&(this[_0x45b541(0x5ef)]()&&this[_0x45b541(0x5ef)]()[_0x45b541(0x705)]());},Game_Enemy[_0x53c255(0x819)][_0x53c255(0x835)]=function(){const _0x24db85=_0x53c255;return $gameSystem[_0x24db85(0xd6)]()[_0x24db85(0x6a6)](this[_0x24db85(0x393)]);},VisuMZ['BattleCore']['Game_BattlerBase_eraseState']=Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x691)],Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x691)]=function(_0x39c1bb){const _0x45b2ef=_0x53c255;VisuMZ[_0x45b2ef(0x2cc)][_0x45b2ef(0x1b5)][_0x45b2ef(0x6b9)](this,_0x39c1bb),this[_0x45b2ef(0x26c)]()&&_0x39c1bb===this[_0x45b2ef(0x804)]()&&this[_0x45b2ef(0x72f)]()&&(this[_0x45b2ef(0x109)]=![]),SceneManager[_0x45b2ef(0x45f)]()&&this[_0x45b2ef(0x6f7)]();},VisuMZ['BattleCore'][_0x53c255(0x55e)]=Game_Action[_0x53c255(0x819)][_0x53c255(0x6fe)],Game_Action['prototype'][_0x53c255(0x6fe)]=function(){const _0x5e5892=_0x53c255;VisuMZ[_0x5e5892(0x2cc)][_0x5e5892(0x55e)]['call'](this),this['_armorPenetration']={'arPenRate':0x0,'arPenFlat':0x0,'arRedRate':0x0,'arRedFlat':0x0},this['_multipliers']={'criticalHitRate':0x1,'criticalHitFlat':0x0,'criticalDmgRate':0x1,'criticalDmgFlat':0x0,'damageRate':0x1,'damageFlat':0x0,'hitRate':0x1,'hitFlat':0x0},this['_customDamageFormula']=_0x5e5892(0x1cf);},Game_Action[_0x53c255(0x819)]['makeDamageValue']=function(_0x25d5cc,_0x11022f){const _0x1678b4=_0x53c255;return VisuMZ[_0x1678b4(0x2cc)]['Settings'][_0x1678b4(0x811)][_0x1678b4(0x75d)]['call'](this,_0x25d5cc,_0x11022f);},Game_Action[_0x53c255(0x819)]['applyVariance']=function(_0x28c803,_0x38c7f5){const _0x538ce6=_0x53c255;return VisuMZ['BattleCore'][_0x538ce6(0x4b3)][_0x538ce6(0x811)][_0x538ce6(0x353)][_0x538ce6(0x6b9)](this,_0x28c803,_0x38c7f5);},Game_Action[_0x53c255(0x819)][_0x53c255(0x742)]=function(_0x23454b,_0x1a9e2e){const _0x59fb8e=_0x53c255;return VisuMZ[_0x59fb8e(0x2cc)][_0x59fb8e(0x4b3)][_0x59fb8e(0x811)]['GuardFormulaJS'][_0x59fb8e(0x6b9)](this,_0x23454b,_0x1a9e2e);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x5c4)]=Game_Action['prototype'][_0x53c255(0x20c)],Game_Action[_0x53c255(0x819)][_0x53c255(0x20c)]=function(_0x379421){const _0x5b7a4d=_0x53c255,_0x29a1ef=this['item']()[_0x5b7a4d(0x236)];if(_0x29a1ef[_0x5b7a4d(0x473)](/<ALWAYS HIT>/i))return 0x1;else{if(_0x29a1ef['match'](/<ALWAYS HIT RATE: (\d+)([%％])>/i))return Number(RegExp['$1'])/0x64;else{let _0x344647=VisuMZ[_0x5b7a4d(0x2cc)][_0x5b7a4d(0x5c4)]['call'](this,_0x379421);return _0x344647=this['_multipliers'][_0x5b7a4d(0x79a)]*_0x344647+this[_0x5b7a4d(0x628)]['hitFlat'],_0x344647;}}},Game_Action[_0x53c255(0x819)][_0x53c255(0x57d)]=function(_0x75d437){const _0x25379d=_0x53c255;if(!this['item']()[_0x25379d(0x600)][_0x25379d(0x1d8)])return 0x0;let _0x59d3ac=VisuMZ[_0x25379d(0x2cc)][_0x25379d(0x4b3)]['Damage'][_0x25379d(0x828)]['call'](this,_0x75d437);return _0x59d3ac=this['_multipliers'][_0x25379d(0x170)]*_0x59d3ac+this[_0x25379d(0x628)][_0x25379d(0x587)],_0x59d3ac;},Game_Action[_0x53c255(0x819)][_0x53c255(0x800)]=function(_0x3b2ccd){const _0xa3f6d9=_0x53c255;return _0x3b2ccd=VisuMZ[_0xa3f6d9(0x2cc)][_0xa3f6d9(0x4b3)]['Damage'][_0xa3f6d9(0x5ed)][_0xa3f6d9(0x6b9)](this,_0x3b2ccd),_0x3b2ccd=this[_0xa3f6d9(0x628)][_0xa3f6d9(0x423)]*_0x3b2ccd+this[_0xa3f6d9(0x628)][_0xa3f6d9(0x209)],_0x3b2ccd;},VisuMZ['BattleCore'][_0x53c255(0x63c)]=Game_Action['prototype'][_0x53c255(0x328)],Game_Action[_0x53c255(0x819)]['evalDamageFormula']=function(_0x343fb6){const _0x471865=_0x53c255;if(this['_customDamageFormula']!==_0x471865(0x1cf))return this[_0x471865(0x702)](_0x343fb6);else return DataManager['getDamageStyle'](this[_0x471865(0x4b7)]())==='MANUAL'?VisuMZ[_0x471865(0x2cc)]['Game_Action_evalDamageFormula'][_0x471865(0x6b9)](this,_0x343fb6):this[_0x471865(0x2a2)](_0x343fb6);},Game_Action[_0x53c255(0x819)][_0x53c255(0x330)]=function(_0x4170c0){this['_customDamageFormula']=_0x4170c0;},Game_Action['prototype'][_0x53c255(0x702)]=function(_0x3eea46){const _0x223a31=_0x53c255,_0x2dffe9=this[_0x223a31(0x4b7)](),_0x31f3e4=_0x2dffe9['damage']['formula'];_0x2dffe9[_0x223a31(0x600)][_0x223a31(0x7b2)]=this[_0x223a31(0x278)];let _0x5e4b84=VisuMZ[_0x223a31(0x2cc)][_0x223a31(0x63c)]['call'](this,_0x3eea46);return _0x2dffe9['damage'][_0x223a31(0x7b2)]=_0x31f3e4,_0x5e4b84;},Game_Action['prototype']['damageStyle']=function(){const _0x4725b7=_0x53c255;if(this[_0x4725b7(0x4b7)]()[_0x4725b7(0x236)][_0x4725b7(0x473)](/<DAMAGE STYLE:[ ](.*)>/i)){const _0x2ac7c7=String(RegExp['$1'])[_0x4725b7(0x61d)]()[_0x4725b7(0x615)]();return _0x2ac7c7;}return _0x4725b7(0x3d5);},Game_Action['prototype'][_0x53c255(0x2a2)]=function(_0x1774d0){const _0x580eb0=_0x53c255,_0x586674=DataManager[_0x580eb0(0x287)](this[_0x580eb0(0x4b7)]()),_0x519124=VisuMZ[_0x580eb0(0x560)][_0x586674];try{return _0x519124['Formula'][_0x580eb0(0x6b9)](this,_0x1774d0);}catch(_0x169695){if($gameTemp[_0x580eb0(0xf4)]())console[_0x580eb0(0x323)](_0x169695);return VisuMZ['BattleCore'][_0x580eb0(0x63c)][_0x580eb0(0x6b9)](this);}},Game_Action[_0x53c255(0x819)][_0x53c255(0x352)]=function(_0x36d006,_0x23c5f7){const _0x375bde=_0x53c255;if(this[_0x375bde(0x76b)]())return _0x23c5f7;const _0x176d0b=this[_0x375bde(0x179)](),_0x1708e1=_0x36d006;let _0x8995b4=[],_0x494d93=[];_0x8995b4[_0x375bde(0x2bf)](this[_0x375bde(0x33a)][_0x375bde(0x632)],this[_0x375bde(0x33a)][_0x375bde(0x6e9)]),_0x494d93[_0x375bde(0x2bf)](this[_0x375bde(0x33a)][_0x375bde(0x752)],this[_0x375bde(0x33a)][_0x375bde(0x29e)]);const _0x164865=this[_0x375bde(0x3b5)]()?/<ARMOR REDUCTION:[ ](\d+\.?\d*)>/i:/<MAGIC REDUCTION:[ ](\d+\.?\d*)>/i,_0x40ade9=this[_0x375bde(0x3b5)]()?/<ARMOR REDUCTION:[ ](\d+\.?\d*)([%％])>/i:/<MAGIC REDUCTION:[ ](\d+\.?\d*)([%％])>/i,_0x4a69b9=this[_0x375bde(0x3b5)]()?/<ARMOR PENETRATION:[ ](\d+\.?\d*)>/i:/<MAGIC PENETRATION:[ ](\d+\.?\d*)>/i,_0x1771c6=this[_0x375bde(0x3b5)]()?/<ARMOR PENETRATION:[ ](\d+\.?\d*)([%％])>/i:/<MAGIC PENETRATION:[ ](\d+\.?\d*)([%％])>/i;return _0x8995b4=_0x8995b4[_0x375bde(0x733)](_0x1708e1[_0x375bde(0x357)]()[_0x375bde(0x19c)](_0x5333f5=>_0x5333f5&&_0x5333f5[_0x375bde(0x236)]['match'](_0x164865)?Number(RegExp['$1']):0x0)),_0x494d93=_0x494d93[_0x375bde(0x733)](_0x1708e1[_0x375bde(0x357)]()[_0x375bde(0x19c)](_0x16d8ac=>_0x16d8ac&&_0x16d8ac['note'][_0x375bde(0x473)](_0x40ade9)?Number(RegExp['$1'])/0x64:0x0)),_0x8995b4=_0x8995b4[_0x375bde(0x733)](_0x176d0b['traitObjects']()['map'](_0x1659a6=>_0x1659a6&&_0x1659a6[_0x375bde(0x236)][_0x375bde(0x473)](_0x4a69b9)?Number(RegExp['$1']):0x0)),_0x494d93=_0x494d93['concat'](_0x176d0b['traitObjects']()[_0x375bde(0x19c)](_0x4b9c26=>_0x4b9c26&&_0x4b9c26[_0x375bde(0x236)][_0x375bde(0x473)](_0x1771c6)?Number(RegExp['$1'])/0x64:0x0)),this['item']()[_0x375bde(0x236)][_0x375bde(0x473)](_0x4a69b9)&&_0x8995b4[_0x375bde(0x2bf)](Number(RegExp['$1'])),this[_0x375bde(0x4b7)]()[_0x375bde(0x236)][_0x375bde(0x473)](_0x1771c6)&&_0x494d93[_0x375bde(0x2bf)](Number(RegExp['$1'])),_0x23c5f7=_0x8995b4[_0x375bde(0x542)]((_0x16e4fa,_0x116bc3)=>_0x16e4fa-_0x116bc3,_0x23c5f7),_0x23c5f7>0x0&&(_0x23c5f7=_0x494d93[_0x375bde(0x542)]((_0x571424,_0x39fd38)=>_0x571424*(0x1-_0x39fd38),_0x23c5f7)),_0x23c5f7;},VisuMZ[_0x53c255(0x2cc)]['Game_Action_executeDamage']=Game_Action[_0x53c255(0x819)][_0x53c255(0x56c)],Game_Action[_0x53c255(0x819)][_0x53c255(0x56c)]=function(_0x58118b,_0x3e4c5a){const _0x401902=_0x53c255;_0x3e4c5a=_0x3e4c5a*this[_0x401902(0x628)][_0x401902(0x5e6)],_0x3e4c5a+=this[_0x401902(0x628)][_0x401902(0x2b9)]*(_0x3e4c5a>=0x0?0x1:-0x1),_0x3e4c5a=this[_0x401902(0x100)](_0x401902(0x62b),_0x58118b,_0x3e4c5a,![]),_0x3e4c5a=this['applyDamageCaps'](_0x3e4c5a),_0x3e4c5a=Math[_0x401902(0x6c4)](_0x3e4c5a),this[_0x401902(0x2b5)]=_0x3e4c5a,this['_totalValue']=this['_totalValue']||0x0,this[_0x401902(0xe4)]+=_0x3e4c5a,VisuMZ['BattleCore'][_0x401902(0x572)][_0x401902(0x6b9)](this,_0x58118b,_0x3e4c5a),this[_0x401902(0x100)](_0x401902(0x853),_0x58118b,_0x3e4c5a,!![]);},Game_Action['prototype'][_0x53c255(0x25b)]=function(_0x4f2e11){const _0x3866d2=_0x53c255;if(this[_0x3866d2(0x61e)]())return _0x4f2e11;return _0x4f2e11=this[_0x3866d2(0x23b)](_0x4f2e11),_0x4f2e11=this[_0x3866d2(0x286)](_0x4f2e11),_0x4f2e11;},Game_Action[_0x53c255(0x819)][_0x53c255(0x61e)]=function(){const _0xed61ae=_0x53c255,_0x411987=/<BYPASS DAMAGE CAP>/i;if(this[_0xed61ae(0x4b7)]()[_0xed61ae(0x236)][_0xed61ae(0x473)](_0x411987))return!![];if(this['subject']()[_0xed61ae(0x357)]()[_0xed61ae(0x73d)](_0x57b4ea=>_0x57b4ea&&_0x57b4ea[_0xed61ae(0x236)][_0xed61ae(0x473)](_0x411987)))return!![];return!VisuMZ[_0xed61ae(0x2cc)]['Settings'][_0xed61ae(0x811)][_0xed61ae(0x483)];},Game_Action[_0x53c255(0x819)][_0x53c255(0x23b)]=function(_0x258d99){const _0x4a1793=_0x53c255;if(!VisuMZ[_0x4a1793(0x2cc)][_0x4a1793(0x4b3)]['Damage']['EnableSoftCap'])return _0x258d99;const _0x10c630=/<BYPASS SOFT DAMAGE CAP>/i;if(this[_0x4a1793(0x4b7)]()[_0x4a1793(0x236)][_0x4a1793(0x473)](_0x10c630))return!![];if(this[_0x4a1793(0x179)]()[_0x4a1793(0x357)]()[_0x4a1793(0x73d)](_0x2e2270=>_0x2e2270&&_0x2e2270[_0x4a1793(0x236)][_0x4a1793(0x473)](_0x10c630)))return!![];const _0x129866=_0x258d99<0x0?-0x1:0x1;_0x258d99=Math[_0x4a1793(0x1f4)](_0x258d99);let _0x69f22b=this[_0x4a1793(0x179)]()[_0x4a1793(0x3f6)]();this[_0x4a1793(0x4b7)]()[_0x4a1793(0x236)][_0x4a1793(0x473)](/<SOFT DAMAGE CAP:[ ]([\+\-]\d+)([%％])>/i)&&(_0x69f22b+=Number(RegExp['$1'])/0x64);_0x69f22b=_0x69f22b[_0x4a1793(0x7eb)](0.01,0x1);const _0x17636c=this['getHardDamageCap'](),_0x51e5d4=_0x69f22b*_0x17636c;if(_0x258d99>_0x51e5d4&&_0x17636c>_0x51e5d4){_0x258d99-=_0x51e5d4;const _0x4d5712=VisuMZ['BattleCore'][_0x4a1793(0x4b3)][_0x4a1793(0x811)][_0x4a1793(0x613)],_0x42e0eb=Math[_0x4a1793(0x776)](0x1-_0x258d99/((_0x17636c-_0x51e5d4)*_0x4d5712+_0x258d99),0.01);_0x258d99*=_0x42e0eb,_0x258d99+=_0x51e5d4;}return _0x258d99*_0x129866;},Game_Action[_0x53c255(0x819)]['getHardDamageCap']=function(){const _0x5938a5=_0x53c255;return this[_0x5938a5(0x4b7)]()[_0x5938a5(0x236)][_0x5938a5(0x473)](/<DAMAGE CAP:[ ](\d+)>/i)?Number(RegExp['$1']):this['subject']()['hardDamageCap']();},Game_Action[_0x53c255(0x819)][_0x53c255(0x286)]=function(_0x22b4a7){const _0x2c0303=_0x53c255;let _0x4d435f=this['getHardDamageCap']();return _0x22b4a7[_0x2c0303(0x7eb)](-_0x4d435f,_0x4d435f);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x498)]=Game_Action[_0x53c255(0x819)][_0x53c255(0xd7)],Game_Action['prototype']['apply']=function(_0x536c7b){const _0x40a07a=_0x53c255;this[_0x40a07a(0x100)](_0x40a07a(0x2ed),_0x536c7b,0x0,!![]),VisuMZ[_0x40a07a(0x2cc)]['Game_Action_apply'][_0x40a07a(0x6b9)](this,_0x536c7b),this['applyBattleCoreJS'](_0x40a07a(0x676),_0x536c7b,this[_0x40a07a(0x2b5)]||0x0,!![]);},Game_Action[_0x53c255(0x819)]['applyBattleCoreJS']=function(_0x10dd07,_0x29fe32,_0x5b2b03,_0xdff90c){const _0x132709=_0x53c255;_0x5b2b03=_0x5b2b03||0x0;const _0x4feb7b=_0x5b2b03,_0x578069=VisuMZ[_0x132709(0x2cc)][_0x132709(0x4b3)][_0x132709(0x2ff)],_0xc9a6fe=_0x10dd07[_0x132709(0x107)]('');if(_0x578069[_0xc9a6fe]){_0x5b2b03=_0x578069[_0xc9a6fe][_0x132709(0x6b9)](this,_0x5b2b03,_0x29fe32);if(_0xdff90c)_0x5b2b03=_0x4feb7b;}let _0x652c6c=VisuMZ[_0x132709(0x2cc)][_0x132709(0x172)](this[_0x132709(0x4b7)](),_0x10dd07[_0x132709(0x107)](''));if(VisuMZ[_0x132709(0x2cc)]['JS'][_0x652c6c]){_0x5b2b03=VisuMZ[_0x132709(0x2cc)]['JS'][_0x652c6c][_0x132709(0x6b9)](this,this[_0x132709(0x179)](),_0x29fe32,this[_0x132709(0x4b7)](),_0x5b2b03);if(_0xdff90c)_0x5b2b03=_0x4feb7b;}for(const _0x3e5ef3 of this[_0x132709(0x179)]()[_0x132709(0x357)]()){if(!_0x3e5ef3)continue;_0x652c6c=VisuMZ[_0x132709(0x2cc)]['createKeyJS'](_0x3e5ef3,_0x10dd07[_0x132709(0x107)](_0x132709(0x24d)));if(VisuMZ[_0x132709(0x2cc)]['JS'][_0x652c6c]){_0x5b2b03=VisuMZ['BattleCore']['JS'][_0x652c6c][_0x132709(0x6b9)](this,this[_0x132709(0x179)](),_0x29fe32,_0x3e5ef3,_0x5b2b03);if(_0xdff90c)_0x5b2b03=_0x4feb7b;}}for(const _0x40251f of _0x29fe32['traitObjects']()){if(!_0x40251f)continue;_0x652c6c=VisuMZ['BattleCore']['createKeyJS'](_0x40251f,_0x10dd07[_0x132709(0x107)]('AsTarget'));if(VisuMZ[_0x132709(0x2cc)]['JS'][_0x652c6c]){_0x5b2b03=VisuMZ[_0x132709(0x2cc)]['JS'][_0x652c6c][_0x132709(0x6b9)](this,this['subject'](),_0x29fe32,_0x40251f,_0x5b2b03);if(_0xdff90c)_0x5b2b03=_0x4feb7b;}}return _0x5b2b03;},Game_Action[_0x53c255(0x819)][_0x53c255(0x4a2)]=function(_0x36bde8){const _0x3a296e=_0x53c255,_0x365134=this[_0x3a296e(0xe4)]||0x0,_0x38790d=VisuMZ[_0x3a296e(0x2cc)][_0x3a296e(0x4b3)][_0x3a296e(0x2ff)],_0x55e6c7=_0x36bde8[_0x3a296e(0x107)]('');_0x38790d[_0x55e6c7]&&_0x38790d[_0x55e6c7][_0x3a296e(0x6b9)](this,_0x365134);let _0x12b111=VisuMZ[_0x3a296e(0x2cc)][_0x3a296e(0x172)](this[_0x3a296e(0x4b7)](),_0x36bde8);VisuMZ[_0x3a296e(0x2cc)]['JS'][_0x12b111]&&VisuMZ[_0x3a296e(0x2cc)]['JS'][_0x12b111]['call'](this,this[_0x3a296e(0x179)](),this[_0x3a296e(0x179)](),this[_0x3a296e(0x4b7)](),_0x365134);for(const _0x43dc39 of this[_0x3a296e(0x179)]()['traitObjects']()){if(!_0x43dc39)continue;_0x12b111=VisuMZ[_0x3a296e(0x2cc)][_0x3a296e(0x172)](_0x43dc39,_0x36bde8),VisuMZ[_0x3a296e(0x2cc)]['JS'][_0x12b111]&&VisuMZ['BattleCore']['JS'][_0x12b111]['call'](this,this['subject'](),this['subject'](),_0x43dc39,_0x365134);}},Game_Action['prototype'][_0x53c255(0x83f)]=function(){const _0x51cbb8=_0x53c255;return VisuMZ[_0x51cbb8(0x2cc)][_0x51cbb8(0x4b3)][_0x51cbb8(0x2ff)][_0x51cbb8(0x61b)][_0x51cbb8(0x6b9)](this);},Game_Action[_0x53c255(0x819)][_0x53c255(0x6d0)]=function(){const _0x2582da=_0x53c255;return VisuMZ[_0x2582da(0x2cc)][_0x2582da(0x4b3)]['Mechanics'][_0x2582da(0x757)];},Game_Action[_0x53c255(0x819)][_0x53c255(0x5b7)]=function(){const _0x7dc8b0=_0x53c255;return this[_0x7dc8b0(0x4b7)]()[_0x7dc8b0(0x236)][_0x7dc8b0(0x473)](/<JS TARGETS>/i);},Game_Action['prototype'][_0x53c255(0x255)]=function(){const _0xbf801c=_0x53c255;if(!this[_0xbf801c(0x331)]&&this['subject']()['isConfused']())return![];if(this[_0xbf801c(0x5b7)]())return!![];return typeof this['item']()[_0xbf801c(0x1ee)]===_0xbf801c(0x197);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x22b)]=Game_Action[_0x53c255(0x819)][_0x53c255(0x38a)],Game_Action[_0x53c255(0x819)]['isForOpponent']=function(){const _0x12c5a8=_0x53c255;return this[_0x12c5a8(0x255)]()&&!this['isCustomBattleScope']()?this[_0x12c5a8(0x6ca)]():VisuMZ[_0x12c5a8(0x2cc)][_0x12c5a8(0x22b)][_0x12c5a8(0x6b9)](this);},Game_Action['prototype'][_0x53c255(0x6ca)]=function(){const _0x593267=_0x53c255,_0x3bfc12=this[_0x593267(0x4b7)]()['scope'];return _0x3bfc12[_0x593267(0x473)](/(?:ENEMY|ENEMIES|FOE|FOES)/i);},VisuMZ['BattleCore'][_0x53c255(0x182)]=Game_Action[_0x53c255(0x819)]['isForFriend'],Game_Action['prototype'][_0x53c255(0x806)]=function(){const _0x300b0d=_0x53c255;return this['isBattleCoreTargetScope']()&&!this[_0x300b0d(0x5b7)]()?this[_0x300b0d(0x7bc)]():VisuMZ[_0x300b0d(0x2cc)]['Game_Action_isForFriend'][_0x300b0d(0x6b9)](this);},Game_Action[_0x53c255(0x819)][_0x53c255(0x7bc)]=function(){const _0x59c38d=_0x53c255,_0x1e5926=this[_0x59c38d(0x4b7)]()[_0x59c38d(0x1ee)];return _0x1e5926['match'](/(?:ALLY|ALLIES|FRIEND|FRIENDS)/i);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0xe6)]=Game_Action[_0x53c255(0x819)][_0x53c255(0x58e)],Game_Action[_0x53c255(0x819)][_0x53c255(0x58e)]=function(){const _0x16c826=_0x53c255;return this['isBattleCoreTargetScope']()&&!this[_0x16c826(0x5b7)]()?this[_0x16c826(0x3d0)]():VisuMZ['BattleCore']['Game_Action_isForRandom']['call'](this);},Game_Action[_0x53c255(0x819)][_0x53c255(0x3d0)]=function(){const _0xa43633=_0x53c255,_0x382cc3=this[_0xa43633(0x4b7)]()[_0xa43633(0x1ee)];return _0x382cc3['match'](/(?:RAND|RANDOM)/i);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x7fb)]=Game_Action[_0x53c255(0x819)][_0x53c255(0x7d0)],Game_Action[_0x53c255(0x819)][_0x53c255(0x7d0)]=function(){const _0x4552d6=_0x53c255;return this[_0x4552d6(0x255)]()&&!this[_0x4552d6(0x5b7)]()?this[_0x4552d6(0x475)]():VisuMZ[_0x4552d6(0x2cc)][_0x4552d6(0x7fb)]['call'](this);},Game_Action[_0x53c255(0x819)]['needsSelectionBattleCore']=function(){const _0x4ac756=_0x53c255,_0x1ffbe1=this[_0x4ac756(0x4b7)]()['scope'];if(_0x1ffbe1[_0x4ac756(0x473)](/RANDOM/i))return![];return VisuMZ['BattleCore'][_0x4ac756(0x7fb)]['call'](this);},VisuMZ['BattleCore'][_0x53c255(0x153)]=Game_Action[_0x53c255(0x819)]['makeTargets'],Game_Action['prototype'][_0x53c255(0x3f1)]=function(){const _0x52221e=_0x53c255;let _0x9b9359=[];return this[_0x52221e(0x255)]()?_0x9b9359=this[_0x52221e(0x4be)]():_0x9b9359=VisuMZ[_0x52221e(0x2cc)][_0x52221e(0x153)][_0x52221e(0x6b9)](this),_0x9b9359=this[_0x52221e(0x3d3)](_0x9b9359),_0x9b9359;},Game_Action[_0x53c255(0x819)][_0x53c255(0x4be)]=function(){const _0x2aebab=_0x53c255;let _0x1c0322=[];const _0x5463b9=String(this[_0x2aebab(0x4b7)]()[_0x2aebab(0x1ee)]),_0x40f11f=VisuMZ[_0x2aebab(0x2cc)]['createKeyJS'](this['item'](),_0x2aebab(0x489));if(VisuMZ[_0x2aebab(0x2cc)]['JS'][_0x40f11f]){const _0x270759=VisuMZ[_0x2aebab(0x2cc)][_0x2aebab(0x172)](this[_0x2aebab(0x4b7)](),_0x2aebab(0x489));return _0x1c0322=VisuMZ[_0x2aebab(0x2cc)]['JS'][_0x270759][_0x2aebab(0x6b9)](this,this[_0x2aebab(0x179)](),_0x1c0322),this[_0x2aebab(0x146)](_0x1c0322);}if(_0x5463b9[_0x2aebab(0x473)](/(\d+) RANDOM ANY/i)){let _0x3595ed=Number(RegExp['$1']);while(_0x3595ed--){const _0x247f0e=Math[_0x2aebab(0x5d9)](0x2)===0x0?this[_0x2aebab(0x110)]():this[_0x2aebab(0x230)]();_0x1c0322[_0x2aebab(0x2bf)](_0x247f0e[_0x2aebab(0x341)]());}return this['repeatTargets'](_0x1c0322);}if(_0x5463b9[_0x2aebab(0x473)](/(\d+) RANDOM (?:ENEMY|ENEMIES|FOE|FOES)/i)){let _0x235ba4=Number(RegExp['$1']);while(_0x235ba4--){_0x1c0322[_0x2aebab(0x2bf)](this['opponentsUnit']()[_0x2aebab(0x341)]());}return this['repeatTargets'](_0x1c0322);}if(_0x5463b9['match'](/(\d+) RANDOM (?:ALLY|ALLIES|FRIEND|FRIENDS)/i)){let _0x23fa10=Number(RegExp['$1']);while(_0x23fa10--){_0x1c0322[_0x2aebab(0x2bf)](this[_0x2aebab(0x230)]()[_0x2aebab(0x341)]());}return this['repeatTargets'](_0x1c0322);}if(_0x5463b9['match'](/ALL (?:ALLY|ALLIES|FRIEND|FRIENDS) (?:BUT|EXCEPT) (?:USER|SELF)/i))return _0x1c0322[_0x2aebab(0x2bf)](...this['friendsUnit']()[_0x2aebab(0x4d7)]()['filter'](_0x53329a=>_0x53329a!==this[_0x2aebab(0x179)]())),this[_0x2aebab(0x146)](_0x1c0322);return VisuMZ[_0x2aebab(0x2cc)]['Game_Action_makeTargets'][_0x2aebab(0x6b9)](this);},Game_Action[_0x53c255(0x819)][_0x53c255(0x785)]=function(_0x3a6d59){const _0xa45613=_0x53c255,_0x2c3952=[];for(let _0x4b9eec=0x0;_0x4b9eec<this[_0xa45613(0x351)]();_0x4b9eec++){_0x2c3952[_0xa45613(0x2bf)](_0x3a6d59[_0xa45613(0x341)]());}return _0x2c3952;},Game_Action[_0x53c255(0x819)][_0x53c255(0x3d3)]=function(_0x21fcf1){const _0x5b22e6=_0x53c255;if(!this[_0x5b22e6(0x4b7)]())return _0x21fcf1;const _0x475893=this[_0x5b22e6(0x4b7)]()[_0x5b22e6(0x236)];return _0x21fcf1;},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x31c)]=Game_Action[_0x53c255(0x819)][_0x53c255(0x218)],Game_Action[_0x53c255(0x819)][_0x53c255(0x218)]=function(_0x519871,_0x31e973){const _0x4f8324=_0x53c255,_0xc24604=_0x519871['isImmortal']();this[_0x4f8324(0x179)]()[_0x4f8324(0x52d)]()[_0x4f8324(0x6a6)](_0x519871[_0x4f8324(0x804)]())&&_0x519871[_0x4f8324(0x3e2)](![]),VisuMZ['BattleCore'][_0x4f8324(0x31c)][_0x4f8324(0x6b9)](this,_0x519871,_0x31e973),_0x519871[_0x4f8324(0x3e2)](_0xc24604);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x514)]=Game_Action[_0x53c255(0x819)]['itemEffectAddNormalState'],Game_Action[_0x53c255(0x819)][_0x53c255(0x482)]=function(_0xe24d22,_0x93a1fe){const _0x1e7dcf=_0x53c255,_0x265e2a=_0xe24d22[_0x1e7dcf(0x334)]();_0x93a1fe[_0x1e7dcf(0x133)]===_0xe24d22[_0x1e7dcf(0x804)]()&&_0xe24d22[_0x1e7dcf(0x3e2)](![]),VisuMZ[_0x1e7dcf(0x2cc)]['Game_Action_itemEffectAddNormalState'][_0x1e7dcf(0x6b9)](this,_0xe24d22,_0x93a1fe),_0xe24d22[_0x1e7dcf(0x3e2)](_0x265e2a);},VisuMZ['BattleCore']['Game_Action_applyGlobal']=Game_Action[_0x53c255(0x819)][_0x53c255(0x501)],Game_Action[_0x53c255(0x819)]['applyGlobal']=function(){const _0x1481c2=_0x53c255;VisuMZ[_0x1481c2(0x2cc)][_0x1481c2(0x225)][_0x1481c2(0x6b9)](this),this[_0x1481c2(0x612)]();},Game_Action['prototype'][_0x53c255(0x612)]=function(){const _0x583017=_0x53c255;if(!SceneManager[_0x583017(0x45f)]())return;const _0x66be2e=/<COMMON (?:EVENT|EVENTS):[ ](.*)>/gi,_0x318aab=this['item']()['note'][_0x583017(0x473)](_0x66be2e);if(_0x318aab)for(const _0x1e3a35 of _0x318aab){if(!_0x1e3a35)continue;_0x1e3a35[_0x583017(0x473)](_0x66be2e);const _0x33c660=String(RegExp['$1'])[_0x583017(0x30d)](',')[_0x583017(0x19c)](_0x28cbec=>String(_0x28cbec)[_0x583017(0x615)]()),_0x2ae55c=_0x33c660[_0x583017(0x19c)](_0x351a41=>DataManager['getCommonEventIdWithName'](_0x351a41));for(const _0x544f39 of _0x2ae55c){const _0x113369=$dataCommonEvents[_0x544f39];_0x113369&&$gameTemp[_0x583017(0x5ad)](_0x544f39);}}},DataManager[_0x53c255(0x7f3)]=function(_0x5d8b17){const _0x31c4bc=_0x53c255;_0x5d8b17=_0x5d8b17[_0x31c4bc(0x61d)]()[_0x31c4bc(0x615)](),this['_commonEventIDs']=this[_0x31c4bc(0x3ef)]||{};if(this[_0x31c4bc(0x3ef)][_0x5d8b17])return this[_0x31c4bc(0x3ef)][_0x5d8b17];for(const _0x580162 of $dataCommonEvents){if(!_0x580162)continue;let _0x1ac878=_0x580162[_0x31c4bc(0x701)];_0x1ac878=_0x1ac878[_0x31c4bc(0x3d6)](/\x1I\[(\d+)\]/gi,''),_0x1ac878=_0x1ac878[_0x31c4bc(0x3d6)](/\\I\[(\d+)\]/gi,''),this[_0x31c4bc(0x3ef)][_0x1ac878[_0x31c4bc(0x61d)]()[_0x31c4bc(0x615)]()]=_0x580162['id'];}return this[_0x31c4bc(0x3ef)][_0x5d8b17]||0x0;},VisuMZ['BattleCore']['Game_BattlerBase_initMembers']=Game_BattlerBase[_0x53c255(0x819)]['initMembers'],Game_BattlerBase['prototype'][_0x53c255(0x1ff)]=function(){const _0x28f115=_0x53c255;VisuMZ['BattleCore'][_0x28f115(0x686)][_0x28f115(0x6b9)](this),this['initMembersBattleCore']();},Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x275)]=function(){const _0x40661d=_0x53c255;this[_0x40661d(0x76a)]=![];},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x39b)]=Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x4fc)],Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x4fc)]=function(){const _0x397bdb=_0x53c255;this[_0x397bdb(0x1fd)]={},VisuMZ[_0x397bdb(0x2cc)][_0x397bdb(0x39b)]['call'](this);},Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x1fb)]=function(_0x10c613){const _0x2a38fd=_0x53c255;return this[_0x2a38fd(0x1fd)]=this[_0x2a38fd(0x1fd)]||{},this[_0x2a38fd(0x1fd)][_0x10c613]!==undefined;},Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x481)]=function(){const _0x75c5ec=_0x53c255;if(this[_0x75c5ec(0x1fd)][_0x75c5ec(0x481)]!==undefined)return this[_0x75c5ec(0x1fd)][_0x75c5ec(0x481)];const _0x4a9195=/<DAMAGE CAP:[ ](\d+)>/i,_0x8521b3=this[_0x75c5ec(0x357)]()[_0x75c5ec(0x19c)](_0x219c76=>_0x219c76&&_0x219c76['note']['match'](_0x4a9195)?Number(RegExp['$1']):0x0);let _0x178cbd=_0x8521b3['length']>0x0?Math[_0x75c5ec(0x776)](..._0x8521b3):0x0;if(_0x178cbd<=0x0)_0x178cbd=VisuMZ[_0x75c5ec(0x2cc)][_0x75c5ec(0x4b3)][_0x75c5ec(0x811)][_0x75c5ec(0xe5)];return this[_0x75c5ec(0x1fd)]['hardDamageCap']=_0x178cbd,this[_0x75c5ec(0x1fd)][_0x75c5ec(0x481)];},Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x3f6)]=function(){const _0x58e1e1=_0x53c255;if(this[_0x58e1e1(0x1fd)][_0x58e1e1(0xde)]!==undefined)return this[_0x58e1e1(0x1fd)][_0x58e1e1(0xde)];let _0x4893c8=VisuMZ[_0x58e1e1(0x2cc)][_0x58e1e1(0x4b3)][_0x58e1e1(0x811)][_0x58e1e1(0x62c)];const _0x5da41e=/<SOFT DAMAGE CAP:[ ]([\+\-]\d+)([%％])>/i,_0x2268f6=this[_0x58e1e1(0x357)]()[_0x58e1e1(0x19c)](_0x1de068=>_0x1de068&&_0x1de068[_0x58e1e1(0x236)][_0x58e1e1(0x473)](_0x5da41e)?Number(RegExp['$1'])/0x64:0x0);return _0x4893c8=_0x2268f6[_0x58e1e1(0x542)]((_0x3817e3,_0x113754)=>_0x3817e3+_0x113754,_0x4893c8),this[_0x58e1e1(0x1fd)][_0x58e1e1(0xde)]=_0x4893c8,this[_0x58e1e1(0x1fd)][_0x58e1e1(0xde)][_0x58e1e1(0x7eb)](0.01,0x1);},VisuMZ[_0x53c255(0x2cc)]['Game_BattlerBase_die']=Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x603)],Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x603)]=function(){const _0x30047f=_0x53c255;VisuMZ[_0x30047f(0x2cc)][_0x30047f(0x5ab)]['call'](this),SceneManager[_0x30047f(0x45f)]()&&this['requestMotion'](_0x30047f(0x4f8));},Game_BattlerBase[_0x53c255(0x819)]['battler']=function(){const _0x4e06ba=_0x53c255;if(!SceneManager[_0x4e06ba(0x45f)]())return null;if(!SceneManager['_scene'][_0x4e06ba(0x3fb)])return null;return SceneManager[_0x4e06ba(0x12f)]['_spriteset'][_0x4e06ba(0x19f)](this);},Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x189)]=function(){const _0x4c660d=_0x53c255;return VisuMZ[_0x4c660d(0x2cc)][_0x4c660d(0x4b3)][_0x4c660d(0x38f)][_0x4c660d(0x646)];},Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x47e)]=function(){const _0x3160c0=_0x53c255;return VisuMZ[_0x3160c0(0x2cc)][_0x3160c0(0x4b3)][_0x3160c0(0x38f)][_0x3160c0(0x779)];},Game_BattlerBase['prototype'][_0x53c255(0x312)]=function(){const _0x6140d8=_0x53c255;return this['isActor']&&this[_0x6140d8(0x807)]()?VisuMZ[_0x6140d8(0x2cc)][_0x6140d8(0x4b3)][_0x6140d8(0x38f)][_0x6140d8(0x7c5)]:VisuMZ[_0x6140d8(0x2cc)][_0x6140d8(0x4b3)]['Enemy'][_0x6140d8(0x7c5)];},Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x588)]=function(){return!![];},Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x25c)]=function(){return 0x0;},Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x235)]=function(){return 0x0;},Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x150)]=function(_0x1d174e){const _0x35349f=_0x53c255;if(!_0x1d174e)return 0x0;let _0x332120=0x0;const _0x16d2bc=_0x1d174e[_0x35349f(0x236)];return _0x16d2bc[_0x35349f(0x473)](/<BATTLE UI OFFSET X:[ ]([\+\-]\d+)>/i)&&(_0x332120+=Number(RegExp['$1'])),_0x16d2bc[_0x35349f(0x473)](/<BATTLE UI OFFSET:[ ]([\+\-]\d+),[ ]([\+\-]\d+)>/i)&&(_0x332120+=Number(RegExp['$1'])),_0x332120;},Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x123)]=function(_0x16885d){const _0x312017=_0x53c255;if(!_0x16885d)return 0x0;let _0x1c8049=0x0;const _0xe46dc3=_0x16885d[_0x312017(0x236)];return _0xe46dc3[_0x312017(0x473)](/<BATTLE UI OFFSET Y:[ ]([\+\-]\d+)>/i)&&(_0x1c8049+=Number(RegExp['$1'])),_0xe46dc3['match'](/<BATTLE UI OFFSET:[ ]([\+\-]\d+),[ ]([\+\-]\d+)>/i)&&(_0x1c8049+=Number(RegExp['$2'])),_0x1c8049;},VisuMZ['BattleCore']['Game_BattlerBase_isStateResist']=Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x6e2)],Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x6e2)]=function(_0x493ab7){const _0x736593=_0x53c255;if(_0x493ab7===this['deathStateId']()&&this[_0x736593(0x334)]())return!![];return VisuMZ[_0x736593(0x2cc)][_0x736593(0x665)][_0x736593(0x6b9)](this,_0x493ab7);},Game_BattlerBase['prototype'][_0x53c255(0x334)]=function(){const _0x221563=_0x53c255;return this[_0x221563(0x76a)];},Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x3e2)]=function(_0x7e646c){const _0xbcb45a=_0x53c255;_0x7e646c?this[_0xbcb45a(0xf8)]():this[_0xbcb45a(0x346)]();},Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0xf8)]=function(){const _0x235c11=_0x53c255;if(this[_0x235c11(0x564)]())return;this[_0x235c11(0x76a)]=!![];},Game_BattlerBase[_0x53c255(0x819)]['removeImmortal']=function(){const _0x58f60=_0x53c255,_0x527fc4=this[_0x58f60(0x72f)]();this['_immortal']=![],this[_0x58f60(0x4fc)](),this['isDead']()&&_0x527fc4&&(this['performCollapse'](),this[_0x58f60(0x6f7)]());},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x7ab)]=Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x6be)],Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x6be)]=function(){const _0x3952ed=_0x53c255;if(!this[_0x3952ed(0x660)]())return![];return VisuMZ[_0x3952ed(0x2cc)][_0x3952ed(0x7ab)][_0x3952ed(0x6b9)](this);},Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x660)]=function(){const _0x113c7e=_0x53c255;for(const _0x1b0a30 of this[_0x113c7e(0x357)]()){if(!_0x1b0a30)continue;if(_0x1b0a30[_0x113c7e(0x236)][_0x113c7e(0x473)](/<(?:ATTACK SEAL|SEAL ATTACK)>/i))return![];}return!![];},VisuMZ[_0x53c255(0x2cc)]['Game_BattlerBase_canGuard']=Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x51c)],Game_BattlerBase[_0x53c255(0x819)][_0x53c255(0x51c)]=function(){const _0x4ceed8=_0x53c255;if(!this[_0x4ceed8(0x63f)]())return![];return VisuMZ[_0x4ceed8(0x2cc)][_0x4ceed8(0xef)][_0x4ceed8(0x6b9)](this);},Game_BattlerBase[_0x53c255(0x819)]['canGuardBattleCore']=function(){const _0x519436=_0x53c255;for(const _0x4e447e of this[_0x519436(0x357)]()){if(!_0x4e447e)continue;if(_0x4e447e['note']['match'](/<(?:GUARD SEAL|SEAL GUARD)>/i))return![];}return!![];},Game_BattlerBase['prototype'][_0x53c255(0x747)]=function(){const _0x3bbad0=_0x53c255;for(const _0x2c302b of this[_0x3bbad0(0x357)]()){if(!_0x2c302b)continue;if(_0x2c302b[_0x3bbad0(0x236)][_0x3bbad0(0x473)](/<(?:ITEM SEAL|SEAL ITEM|SEAL ITEMS)>/i))return![];}return!![];},VisuMZ['BattleCore']['Game_Battler_regenerateAll']=Game_Battler['prototype'][_0x53c255(0x21f)],Game_Battler[_0x53c255(0x819)][_0x53c255(0x21f)]=function(){const _0x3e8071=_0x53c255;if(SceneManager[_0x3e8071(0x45f)]()&&$gameTroop['turnCount']()<=0x0)return;this['processBattleCoreJS'](_0x3e8071(0x3ca)),VisuMZ['BattleCore']['Game_Battler_regenerateAll']['call'](this),this['regenerateAllBattleCore'](),this[_0x3e8071(0x195)](_0x3e8071(0x5a4));},Game_Battler[_0x53c255(0x819)][_0x53c255(0x270)]=function(){const _0x61f8dd=_0x53c255;if(SceneManager[_0x61f8dd(0x45f)]())for(const _0x382e77 of this[_0x61f8dd(0x357)]()){if(!_0x382e77)continue;this[_0x61f8dd(0x208)](_0x382e77);}},Game_Battler[_0x53c255(0x819)]['onRegeneratePlayStateAnimation']=function(_0xd253b){const _0x25f34f=_0x53c255;if(!Imported[_0x25f34f(0x23a)])return;if(!SceneManager[_0x25f34f(0x45f)]())return;if(this[_0x25f34f(0x564)]())return;if(this['isHidden']())return;if(_0xd253b[_0x25f34f(0x236)][_0x25f34f(0x473)](/<(?:REGENERATE|REGEN|DEGEN|DOT|SLIP)[ ]ANIMATION:[ ](\d+)>/i)){const _0x53004f=Number(RegExp['$1']);$gameTemp[_0x25f34f(0x397)]([this],_0x53004f,![],![]);}},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x412)]=Game_Battler[_0x53c255(0x819)][_0x53c255(0x57c)],Game_Battler[_0x53c255(0x819)][_0x53c255(0x57c)]=function(){const _0x14cd4e=_0x53c255;this[_0x14cd4e(0x195)](_0x14cd4e(0x432)),VisuMZ['BattleCore']['Game_Battler_startTpbTurn'][_0x14cd4e(0x6b9)](this),this[_0x14cd4e(0x195)](_0x14cd4e(0x758));},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x1b1)]=Game_Battler['prototype']['onTurnEnd'],Game_Battler[_0x53c255(0x819)][_0x53c255(0x55c)]=function(){const _0x2648c5=_0x53c255;this[_0x2648c5(0x195)]('PreEndTurnJS'),VisuMZ[_0x2648c5(0x2cc)][_0x2648c5(0x1b1)][_0x2648c5(0x6b9)](this),this[_0x2648c5(0x195)](_0x2648c5(0x6d9));},Game_Battler[_0x53c255(0x819)][_0x53c255(0x195)]=function(_0x53c039){const _0x27d194=_0x53c255,_0x12f380=VisuMZ[_0x27d194(0x2cc)]['Settings']['Mechanics'];if(_0x12f380[_0x53c039])_0x12f380[_0x53c039]['call'](this);for(const _0x2cccdf of this[_0x27d194(0x357)]()){if(!_0x2cccdf)continue;key=VisuMZ[_0x27d194(0x2cc)][_0x27d194(0x172)](_0x2cccdf,_0x53c039),VisuMZ['BattleCore']['JS'][key]&&VisuMZ['BattleCore']['JS'][key][_0x27d194(0x6b9)](this,this,this,_0x2cccdf,0x0);}},Game_Battler['prototype'][_0x53c255(0x214)]=function(){const _0x399c20=_0x53c255;return VisuMZ[_0x399c20(0x2cc)]['Settings'][_0x399c20(0x38f)][_0x399c20(0x19a)]||![];},Game_Battler[_0x53c255(0x819)][_0x53c255(0x4dc)]=function(){const _0x1d3ca2=_0x53c255;if(this[_0x1d3ca2(0x7da)]()){if(this['chantStyle']()){if(this['_actions'][_0x1d3ca2(0x73d)](_0xc6b4fa=>_0xc6b4fa[_0x1d3ca2(0x4b7)]()&&_0xc6b4fa[_0x1d3ca2(0x164)]()))return!![];}else{if(this['_actions'][_0x1d3ca2(0x73d)](_0x56511d=>_0x56511d[_0x1d3ca2(0x4b7)]()&&_0x56511d['isMagicSkill']()))return!![];}}if(BattleManager[_0x1d3ca2(0x4ea)]()&&this[_0x1d3ca2(0x3c2)]==='casting')return this['chantStyle']()?this[_0x1d3ca2(0x522)]()&&this[_0x1d3ca2(0x522)]()[_0x1d3ca2(0x4b7)]()&&this[_0x1d3ca2(0x522)]()['isMagical']():this[_0x1d3ca2(0x522)]()&&this[_0x1d3ca2(0x522)]()[_0x1d3ca2(0x4b7)]()&&this[_0x1d3ca2(0x522)]()[_0x1d3ca2(0x29b)]();return![];},Game_Battler[_0x53c255(0x819)]['isCharging']=function(){const _0x4fe086=_0x53c255;if(BattleManager[_0x4fe086(0x4ea)]()&&this[_0x4fe086(0x3c2)]===_0x4fe086(0x141))return this['chantStyle']()?this['currentAction']()&&this[_0x4fe086(0x522)]()[_0x4fe086(0x4b7)]()&&!this[_0x4fe086(0x522)]()[_0x4fe086(0x164)]():this[_0x4fe086(0x522)]()&&this[_0x4fe086(0x522)]()[_0x4fe086(0x4b7)]()&&!this[_0x4fe086(0x522)]()[_0x4fe086(0x29b)]();return![];},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x301)]=Game_Battler['prototype'][_0x53c255(0x2f6)],Game_Battler[_0x53c255(0x819)][_0x53c255(0x2f6)]=function(){const _0x2e95e5=_0x53c255;VisuMZ[_0x2e95e5(0x2cc)][_0x2e95e5(0x301)][_0x2e95e5(0x6b9)](this),this[_0x2e95e5(0x7f7)]=[];},Game_Battler[_0x53c255(0x819)][_0x53c255(0x3e0)]=function(){const _0x5b9f0e=_0x53c255;if(!this[_0x5b9f0e(0x7f7)])this[_0x5b9f0e(0x2f6)]();return this['_damagePopupArray'][_0x5b9f0e(0x343)]>0x0;},Game_Battler[_0x53c255(0x819)][_0x53c255(0x2c8)]=function(){const _0x5f2105=_0x53c255;if(!SceneManager['isSceneBattle']())return;if(!this[_0x5f2105(0x7f7)])this[_0x5f2105(0x2f6)]();this['createSeparateDamagePopups']();const _0x33ee27=this[_0x5f2105(0x5ef)]();if(_0x33ee27)_0x33ee27['setupDamagePopup']();},Game_Battler[_0x53c255(0x819)][_0x53c255(0x2f1)]=function(){const _0x31c96c=_0x53c255,_0x472ebb=this[_0x31c96c(0x284)]();if(_0x472ebb[_0x31c96c(0x7df)]||_0x472ebb[_0x31c96c(0x265)]){const _0x5ab57c=JsonEx[_0x31c96c(0x37a)](_0x472ebb);_0x5ab57c[_0x31c96c(0x3fa)]=![],_0x5ab57c[_0x31c96c(0x6d4)]=0x0,this['_damagePopupArray'][_0x31c96c(0x2bf)](_0x5ab57c);}if(_0x472ebb[_0x31c96c(0x3fa)]){const _0x912431=JsonEx[_0x31c96c(0x37a)](_0x472ebb);_0x912431[_0x31c96c(0x7df)]=![],_0x912431[_0x31c96c(0x265)]=![],_0x912431['mpDamage']=0x0,this[_0x31c96c(0x7f7)][_0x31c96c(0x2bf)](_0x912431);}if(_0x472ebb[_0x31c96c(0x6d4)]!==0x0){const _0x182a90=JsonEx[_0x31c96c(0x37a)](_0x472ebb);_0x182a90['missed']=![],_0x182a90[_0x31c96c(0x265)]=![],_0x182a90[_0x31c96c(0x3fa)]=![],this[_0x31c96c(0x7f7)][_0x31c96c(0x2bf)](_0x182a90);}},Game_Battler[_0x53c255(0x819)][_0x53c255(0x857)]=function(){const _0x165626=_0x53c255;if(!this[_0x165626(0x7f7)])this[_0x165626(0x2f6)]();return VisuMZ[_0x165626(0x2cc)]['Settings'][_0x165626(0x811)][_0x165626(0x464)]?this[_0x165626(0x7f7)][_0x165626(0x68c)]():this[_0x165626(0x7f7)]['pop']();},Game_Battler['prototype'][_0x53c255(0x6e0)]=function(_0x45ffe0,_0x4e2760){const _0x522179=_0x53c255;if(!SceneManager['isSceneBattle']())return;if(!this[_0x522179(0x5ef)]())return;if(_0x45ffe0[_0x522179(0x343)]<=0x0)return;_0x4e2760=_0x4e2760||{},_0x4e2760[_0x522179(0x648)]=_0x4e2760[_0x522179(0x648)]||_0x522179(0x444),_0x4e2760[_0x522179(0x45b)]=_0x4e2760[_0x522179(0x45b)]||[0x0,0x0,0x0,0x0],_0x4e2760[_0x522179(0x6ad)]=_0x4e2760[_0x522179(0x6ad)]||0x0,this[_0x522179(0x5ef)]()[_0x522179(0x6e0)](_0x45ffe0,_0x4e2760);},Game_Battler[_0x53c255(0x819)][_0x53c255(0x5c2)]=function(_0x1cfd56,_0x5cc580,_0x42ad48){const _0x44baed=_0x53c255;if(!SceneManager[_0x44baed(0x45f)]())return;if(!this[_0x44baed(0x5ef)]())return;if(_0x5cc580['length']<=0x0)return;_0x42ad48=_0x42ad48||{},_0x42ad48['textColor']=_0x42ad48[_0x44baed(0x648)]||_0x44baed(0x444),_0x42ad48[_0x44baed(0x45b)]=_0x42ad48[_0x44baed(0x45b)]||[0x0,0x0,0x0,0x0],_0x42ad48[_0x44baed(0x6ad)]=_0x42ad48['flashDuration']||0x0,this[_0x44baed(0x5ef)]()[_0x44baed(0x5c2)](_0x1cfd56,_0x5cc580,_0x42ad48);},Game_Battler[_0x53c255(0x819)][_0x53c255(0x571)]=function(){const _0x277575=_0x53c255;if(this[_0x277575(0x39d)]())return![];if(this[_0x277575(0x72f)]()&&this['isAppeared']())return!![];if(this[_0x277575(0x26c)]()&&this[_0x277575(0x366)]()){if(this[_0x277575(0x564)]()&&this[_0x277575(0x7ba)]())return![];}else{if(this[_0x277575(0x564)]())return![];}return!![];},VisuMZ[_0x53c255(0x2cc)]['Game_Battler_clearMotion']=Game_Battler[_0x53c255(0x819)][_0x53c255(0x6e1)],Game_Battler[_0x53c255(0x819)][_0x53c255(0x6e1)]=function(){const _0x4ab7fb=_0x53c255;VisuMZ['BattleCore']['Game_Battler_clearMotion'][_0x4ab7fb(0x6b9)](this),this[_0x4ab7fb(0x1e5)]();},Game_Battler['prototype'][_0x53c255(0x453)]=function(){return!![];},Game_Battler[_0x53c255(0x819)][_0x53c255(0x839)]=function(){return![];},VisuMZ[_0x53c255(0x2cc)]['Game_Battler_onBattleStart']=Game_Battler[_0x53c255(0x819)]['onBattleStart'],Game_Battler[_0x53c255(0x819)][_0x53c255(0x10a)]=function(_0x9211e0){const _0x1f689a=_0x53c255;VisuMZ[_0x1f689a(0x2cc)][_0x1f689a(0xe3)][_0x1f689a(0x6b9)](this,_0x9211e0),this[_0x1f689a(0x2d2)](_0x9211e0);},Game_Battler['prototype']['onBattleStartBattleCore']=function(_0x42a4e0){const _0x5204f9=_0x53c255;this[_0x5204f9(0x4aa)](![]);},VisuMZ['BattleCore'][_0x53c255(0x30b)]=Game_Battler[_0x53c255(0x819)]['performActionStart'],Game_Battler[_0x53c255(0x819)][_0x53c255(0x5d1)]=function(_0x52167f){const _0x40ed4c=_0x53c255;VisuMZ['BattleCore'][_0x40ed4c(0x30b)][_0x40ed4c(0x6b9)](this,_0x52167f);if(!_0x52167f['isGuard']()){const _0x579b57=this[_0x40ed4c(0x5ef)]();if(_0x579b57)_0x579b57[_0x40ed4c(0x2aa)]();}this['setBattlerFlip'](![]);},Game_Battler[_0x53c255(0x819)][_0x53c255(0x621)]=function(){const _0x3b8a77=_0x53c255,_0x2d8e9f=this[_0x3b8a77(0x858)];this[_0x3b8a77(0x858)]=![];if(BattleManager['isActiveTpb']()&&this['isInputting']()){const _0x4da1b9=this[_0x3b8a77(0x5ef)]();if(_0x4da1b9&&_0x2d8e9f)_0x4da1b9['stepForward']();return;}const _0x58e26c=this[_0x3b8a77(0x5ef)]();if(_0x58e26c)_0x58e26c[_0x3b8a77(0x2ee)]();this[_0x3b8a77(0x4aa)](![]),this[_0x3b8a77(0x6f7)]();},Game_Battler[_0x53c255(0x819)]['performActionMotions']=function(_0x3d7f63){const _0x2c20cc=_0x53c255;if(_0x3d7f63[_0x2c20cc(0x2af)]())this[_0x2c20cc(0x4a0)]();else{if(_0x3d7f63[_0x2c20cc(0x80e)]())this['requestMotion']('guard');else{if(_0x3d7f63[_0x2c20cc(0x164)]())this[_0x2c20cc(0x2a9)](_0x2c20cc(0x69a));else{if(_0x3d7f63[_0x2c20cc(0x5c7)]())_0x3d7f63[_0x2c20cc(0x4b7)]()[_0x2c20cc(0x600)][_0x2c20cc(0x3ce)]>0x0?this[_0x2c20cc(0x4a0)]():this[_0x2c20cc(0x2a9)](_0x2c20cc(0x3b8));else _0x3d7f63[_0x2c20cc(0x595)]()&&this['requestMotion'](_0x2c20cc(0x4b7));}}}},Game_Battler[_0x53c255(0x819)][_0x53c255(0x567)]=function(){const _0x10c5d4=_0x53c255;return $dataSystem[_0x10c5d4(0x7ff)][0x0];},Game_Battler[_0x53c255(0x819)][_0x53c255(0x388)]=function(){const _0x558818=_0x53c255,_0x59f266=this[_0x558818(0x567)]();return _0x59f266?_0x59f266[_0x558818(0x1f6)]:0x0;},Game_Battler[_0x53c255(0x819)]['performSubstitute']=function(_0x334428){const _0x50069f=_0x53c255;if(!$gameSystem[_0x50069f(0xdb)]())return;const _0x14d32c=this['battler'](),_0x400a4a=_0x334428[_0x50069f(0x5ef)]();if(!_0x14d32c||!_0x400a4a)return;const _0x92696f=_0x400a4a['_baseX'],_0x60de96=_0x400a4a[_0x50069f(0x772)];this[_0x50069f(0x234)](_0x92696f,_0x60de96,0x0,![],'Linear',-0x1),_0x14d32c['updatePosition']();const _0x9e67f5=VisuMZ[_0x50069f(0x2cc)]['Settings'][_0x50069f(0x431)];let _0x31ea26=(_0x400a4a[_0x50069f(0x261)]+_0x14d32c[_0x50069f(0x261)])/0x2;_0x31ea26*=this[_0x50069f(0x807)]()?0x1:-0x1;let _0x5d8bbb=_0x9e67f5[_0x50069f(0x61a)]*(this[_0x50069f(0x807)]()?0x1:-0x1);_0x334428[_0x50069f(0x450)](_0x31ea26,_0x5d8bbb,0x0,![],_0x50069f(0x611)),_0x400a4a[_0x50069f(0x824)]();},Game_Battler[_0x53c255(0x819)]['requestMotion']=function(_0x48872){const _0x49e292=_0x53c255;if(SceneManager[_0x49e292(0x45f)]()){const _0x2d78a6=this[_0x49e292(0x5ef)]();_0x2d78a6&&(_0x2d78a6['forceMotion'](_0x48872),[_0x49e292(0x14e),_0x49e292(0x81e),_0x49e292(0x368)][_0x49e292(0x6a6)](_0x48872)&&this[_0x49e292(0x7a4)]());}this[_0x49e292(0x1e5)]();},Game_Battler['prototype']['performWeaponAnimation']=function(){},Game_Battler[_0x53c255(0x819)][_0x53c255(0x820)]=function(_0x86df99){const _0x5dda04=_0x53c255;if(SceneManager[_0x5dda04(0x45f)]()){const _0xb0b03c=this[_0x5dda04(0x5ef)]();if(_0xb0b03c)_0xb0b03c[_0x5dda04(0x3c9)](_0x86df99);}},Game_Battler[_0x53c255(0x819)][_0x53c255(0x2d3)]=function(){const _0x578fc6=_0x53c255;if(SceneManager['isSceneBattle']()){const _0x4bf5bf=this[_0x578fc6(0x388)]();this['startWeaponAnimation'](_0x4bf5bf);}},Game_Battler['prototype'][_0x53c255(0x7d6)]=function(_0x3776a4,_0x134b7a){const _0x5d95b0=_0x53c255;if(!_0x3776a4)return;if(!_0x3776a4[_0x5d95b0(0x4b7)]())return;if(_0x3776a4['isAttack']())return;if(_0x3776a4[_0x5d95b0(0x80e)]())return;if(_0x3776a4['isItem']())return;let _0x563af8=0x0;const _0x225f22=VisuMZ[_0x5d95b0(0x2cc)]['Settings'][_0x5d95b0(0x431)],_0x4892b2=_0x3776a4['item']()[_0x5d95b0(0x236)];if(_0x4892b2[_0x5d95b0(0x473)](/<CAST ANIMATION: (\d+)>/i))_0x563af8=Number(RegExp['$1']);else{if(_0x4892b2[_0x5d95b0(0x473)](/<NO CAST ANIMATION>/i))return;else{if(_0x3776a4[_0x5d95b0(0x76b)]())_0x563af8=_0x225f22['CastCertain'];else{if(_0x3776a4[_0x5d95b0(0x3b5)]())_0x563af8=_0x225f22['CastPhysical'];else _0x3776a4[_0x5d95b0(0x164)]()&&(_0x563af8=_0x225f22[_0x5d95b0(0x17d)]);}}}_0x563af8>0x0&&$gameTemp['requestAnimation']([this],_0x563af8,!!_0x134b7a);},Game_Battler['prototype'][_0x53c255(0x797)]=function(){const _0x342b71=_0x53c255;SoundManager['playReflection']();let _0x4a58c8=VisuMZ[_0x342b71(0x2cc)][_0x342b71(0x4b3)][_0x342b71(0x431)][_0x342b71(0x539)];_0x4a58c8>0x0&&$gameTemp[_0x342b71(0x308)]([this],_0x4a58c8);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x34b)]=Game_Battler[_0x53c255(0x819)][_0x53c255(0xfd)],Game_Battler[_0x53c255(0x819)][_0x53c255(0xfd)]=function(){const _0x2b77a0=_0x53c255;VisuMZ[_0x2b77a0(0x2cc)][_0x2b77a0(0x34b)]['call'](this),this[_0x2b77a0(0x4e4)]();},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x77f)]=Game_Battler[_0x53c255(0x819)]['performMiss'],Game_Battler[_0x53c255(0x819)]['performMiss']=function(){const _0x9af032=_0x53c255;VisuMZ['BattleCore']['Game_Battler_performMiss'][_0x9af032(0x6b9)](this),this[_0x9af032(0x4e4)]();},VisuMZ['BattleCore']['Game_Battler_performEvasion']=Game_Battler[_0x53c255(0x819)][_0x53c255(0x5d4)],Game_Battler['prototype']['performEvasion']=function(){const _0x14cd9c=_0x53c255;VisuMZ[_0x14cd9c(0x2cc)]['Game_Battler_performEvasion']['call'](this),this[_0x14cd9c(0x4e4)]();},Game_Battler[_0x53c255(0x819)]['performFlinch']=function(){const _0xa3ce8=_0x53c255;if(!$gameSystem[_0xa3ce8(0xdb)]())return;if(this[_0xa3ce8(0x858)])return;this[_0xa3ce8(0x858)]=!![];const _0x73255b=this[_0xa3ce8(0x5ef)]();if(_0x73255b)_0x73255b[_0xa3ce8(0x856)]();},Game_Battler['prototype']['requestMotionRefresh']=function(){const _0x19de40=_0x53c255;if(this[_0x19de40(0x564)]()&&this[_0x19de40(0x81f)]!==_0x19de40(0x4f8)){this[_0x19de40(0x2a9)](_0x19de40(0x4f8));return;}if(this['isDead']()&&this[_0x19de40(0x81f)]==='dead')return;if(!!this[_0x19de40(0x80f)])return;if(this['isEnemy']()){if(!this['isDuringNonLoopingMotion']())this[_0x19de40(0x5ef)]()['refreshMotion']();this[_0x19de40(0x1e5)]();return;}if(this[_0x19de40(0x81f)]===_0x19de40(0x63e))return;if(this[_0x19de40(0x81f)]==='escape'&&!BattleManager[_0x19de40(0x6c8)]())return;if(this[_0x19de40(0x81f)]===_0x19de40(0x59a)&&!BattleManager[_0x19de40(0x6c8)]())return;this[_0x19de40(0x6e1)]();if(this[_0x19de40(0x5ef)]()&&BattleManager['isInputting']()){this[_0x19de40(0x5ef)]()[_0x19de40(0x705)](),this[_0x19de40(0x1e5)]();return;}},Game_Enemy['prototype'][_0x53c255(0x433)]=function(){const _0x2580fa=_0x53c255;if(!this[_0x2580fa(0x366)]())return![];const _0x5490d2=this[_0x2580fa(0x5ef)]();if(!_0x5490d2)return![];const _0x13d5e2=_0x5490d2[_0x2580fa(0x4bb)];if(!_0x13d5e2)return![];const _0x247e91=_0x13d5e2[_0x2580fa(0x85d)];return _0x247e91&&!_0x247e91[_0x2580fa(0x2e2)];},Game_Battler[_0x53c255(0x819)][_0x53c255(0x707)]=function(){const _0x49a89f=_0x53c255;return this[_0x49a89f(0x557)];},Game_Battler['prototype'][_0x53c255(0x4aa)]=function(_0x4e8472){const _0xa6c8be=_0x53c255;if(!$gameSystem['isSideView']())return;this[_0xa6c8be(0x557)]=_0x4e8472;const _0x46b2ce=this[_0xa6c8be(0x5ef)]();if(_0x46b2ce)_0x46b2ce[_0xa6c8be(0x40b)]();},Game_Battler['prototype'][_0x53c255(0xfe)]=function(_0x40e754,_0x49953e,_0x39cea2){const _0x397b32=_0x53c255;if(!$gameSystem[_0x397b32(0xdb)]())return;const _0x11344a=this[_0x397b32(0x5ef)]();if(!_0x11344a)return;if(_0x40e754===_0x11344a[_0x397b32(0x396)])return;let _0x87c806=![];if(this[_0x397b32(0x807)]()){if(_0x40e754>_0x11344a[_0x397b32(0x396)])_0x87c806=!![];if(_0x40e754<_0x11344a[_0x397b32(0x396)])_0x87c806=![];}else{if(this[_0x397b32(0x26c)]()){if(_0x40e754>_0x11344a[_0x397b32(0x396)])_0x87c806=![];if(_0x40e754<_0x11344a[_0x397b32(0x396)])_0x87c806=!![];}};this[_0x397b32(0x4aa)](_0x39cea2?!_0x87c806:_0x87c806),_0x11344a[_0x397b32(0x40b)]();},Game_Battler[_0x53c255(0x819)][_0x53c255(0x450)]=function(_0x59c9ce,_0x1e010a,_0x459383,_0x3a41e1,_0x4fc03a){const _0x3e3f11=_0x53c255;if(!$gameSystem['isSideView']())return;const _0xbe37f5=this[_0x3e3f11(0x5ef)]();if(!_0xbe37f5)return;if(_0x3a41e1)this[_0x3e3f11(0xfe)](_0x59c9ce+_0xbe37f5[_0x3e3f11(0x396)],_0x1e010a+_0xbe37f5[_0x3e3f11(0x772)],![]);_0x59c9ce+=_0xbe37f5[_0x3e3f11(0x396)]-_0xbe37f5[_0x3e3f11(0x67c)],_0x1e010a+=_0xbe37f5[_0x3e3f11(0x772)]-_0xbe37f5[_0x3e3f11(0x4a5)],_0xbe37f5[_0x3e3f11(0x64a)](_0x59c9ce,_0x1e010a,_0x459383);if(Imported[_0x3e3f11(0x23a)])_0xbe37f5[_0x3e3f11(0x713)](_0x4fc03a||_0x3e3f11(0x611));},Game_Battler[_0x53c255(0x819)][_0x53c255(0x234)]=function(_0x43fd57,_0x55a5c1,_0x2bd028,_0x28c4ec,_0x4e11d6,_0x256a80){const _0x1ea90d=_0x53c255;if(!$gameSystem[_0x1ea90d(0xdb)]())return;const _0x2a1121=this[_0x1ea90d(0x5ef)]();if(!_0x2a1121)return;_0x256a80=_0x256a80||0x0;if(_0x256a80>0x0){if(_0x2a1121[_0x1ea90d(0x396)]>_0x43fd57)_0x43fd57+=_0x2a1121[_0x1ea90d(0x261)]/0x2+_0x256a80;if(_0x2a1121[_0x1ea90d(0x396)]<_0x43fd57)_0x43fd57-=_0x2a1121[_0x1ea90d(0x261)]/0x2+_0x256a80;}if(_0x28c4ec)this[_0x1ea90d(0xfe)](_0x43fd57,_0x55a5c1,![]);_0x43fd57-=_0x2a1121[_0x1ea90d(0x67c)],_0x55a5c1-=_0x2a1121['_homeY'],_0x2a1121[_0x1ea90d(0x64a)](_0x43fd57,_0x55a5c1,_0x2bd028);if(Imported[_0x1ea90d(0x23a)])_0x2a1121[_0x1ea90d(0x713)](_0x4e11d6||_0x1ea90d(0x611));},Game_Battler[_0x53c255(0x819)][_0x53c255(0x47f)]=function(_0x5ae44f,_0x160f93,_0x398c46){const _0x18516a=_0x53c255;if(!$gameSystem[_0x18516a(0xdb)]())return;const _0x6039ff=this[_0x18516a(0x5ef)]();if(!_0x6039ff)return;_0x6039ff[_0x18516a(0x56a)](_0x5ae44f,_0x160f93,_0x398c46);},Game_Battler['prototype'][_0x53c255(0x1ce)]=function(_0x3ba7ae,_0x5bef47){const _0x21a24c=_0x53c255;if(!$gameSystem[_0x21a24c(0xdb)]())return;const _0x26e241=this['battler']();if(!_0x26e241)return;_0x26e241[_0x21a24c(0x740)](_0x3ba7ae,_0x5bef47);},Game_Battler[_0x53c255(0x819)][_0x53c255(0x3f8)]=function(_0x24dceb,_0x262c63,_0x43d930,_0xfb62a6){const _0x5af5bd=_0x53c255;if(!$gameSystem['isSideView']())return;const _0x37545c=this[_0x5af5bd(0x5ef)]();if(!_0x37545c)return;_0x37545c[_0x5af5bd(0xf7)](_0x24dceb,_0x262c63,_0x43d930,_0xfb62a6);},Game_Battler[_0x53c255(0x819)][_0x53c255(0x40f)]=function(_0x128c67,_0x152b2e,_0x4e7f94,_0x36f03f){const _0xef7a2c=_0x53c255;if(!$gameSystem['isSideView']())return;const _0x2ddb7e=this[_0xef7a2c(0x5ef)]();if(!_0x2ddb7e)return;this[_0xef7a2c(0x807)]()&&(_0x128c67*=-0x1,_0x152b2e*=-0x1),_0x2ddb7e[_0xef7a2c(0x231)](_0x128c67,_0x152b2e,_0x4e7f94,_0x36f03f);},Game_Battler[_0x53c255(0x819)][_0x53c255(0x2c1)]=function(_0x15e5cb,_0x4e7abd,_0x25c55f,_0x456856){const _0x4fd665=_0x53c255;if(!$gameSystem['isSideView']())return;const _0x516b77=this[_0x4fd665(0x5ef)]();if(!_0x516b77)return;_0x516b77[_0x4fd665(0x4da)](_0x15e5cb,_0x4e7abd,_0x25c55f,_0x456856);},Game_Battler[_0x53c255(0x819)][_0x53c255(0x14b)]=function(_0x5a2d70,_0x4a0add,_0x1d0236){const _0x39aeb9=_0x53c255;if(!$gameSystem[_0x39aeb9(0xdb)]())return;const _0xac55e3=this['battler']();if(!_0xac55e3)return;_0xac55e3['startOpacity'](_0x5a2d70,_0x4a0add,_0x1d0236);},Game_Battler[_0x53c255(0x819)][_0x53c255(0x1e5)]=function(){const _0x96ada=_0x53c255,_0xf00645=!!this[_0x96ada(0x80f)];this[_0x96ada(0x80f)]=undefined,_0xf00645&&(this[_0x96ada(0x6f7)](),this[_0x96ada(0x418)]());},Game_Battler[_0x53c255(0x819)]['clearFreezeMotionForWeapons']=function(){const _0x5a8ea8=_0x53c255;if(!SceneManager['isSceneBattle']())return;const _0xdbc391=this[_0x5a8ea8(0x5ef)]();if(!_0xdbc391)return;let _0x6a36a1=this['isActor']()?_0xdbc391[_0x5a8ea8(0x748)]:_0xdbc391[_0x5a8ea8(0x4bb)][_0x5a8ea8(0x748)];_0x6a36a1&&_0x6a36a1[_0x5a8ea8(0x157)](0x0);},Game_Battler[_0x53c255(0x819)]['freezeMotion']=function(_0x51e6ab,_0x9d6bfa,_0x288a93){const _0x15b4b9=_0x53c255;if(this[_0x15b4b9(0x26c)]()&&!this[_0x15b4b9(0x366)]())return;let _0x1e4778=0x0,_0x5b9a25=0x0;_0x51e6ab[_0x15b4b9(0x473)](/ATTACK[ ](\d+)/i)&&(_0x5b9a25=Number(RegExp['$1']),_0x5b9a25--);if(this['isActor']()){const _0x17b254=this[_0x15b4b9(0x851)]();_0x1e4778=_0x17b254[_0x5b9a25]?_0x17b254[_0x5b9a25]['wtypeId']:0x0;}else this[_0x15b4b9(0x26c)]()&&(_0x1e4778=this[_0x15b4b9(0x3f0)]()[_0x15b4b9(0x18b)]||0x0);const _0x3ab2e4=$dataSystem[_0x15b4b9(0x7ff)][_0x1e4778];_0x51e6ab[_0x15b4b9(0x473)](/attack/i)&&(_0x51e6ab=[_0x15b4b9(0x81e),_0x15b4b9(0x14e),_0x15b4b9(0x368)][_0x3ab2e4[_0x15b4b9(0x3ce)]]||_0x15b4b9(0x14e)),this[_0x15b4b9(0x80f)]={'motionType':_0x51e6ab,'weaponImageId':_0x9d6bfa?_0x3ab2e4['weaponImageId']:0x0,'pattern':_0x288a93};},Game_Battler[_0x53c255(0x819)][_0x53c255(0x20e)]=function(_0x5ac91d){const _0x112fdc=_0x53c255;if(!_0x5ac91d)return![];return _0x5ac91d[_0x112fdc(0x230)]()===this[_0x112fdc(0x230)]();},Game_Battler[_0x53c255(0x819)][_0x53c255(0x4e5)]=function(_0x50ac69){const _0x3d3967=_0x53c255;if(!_0x50ac69)return![];return _0x50ac69[_0x3d3967(0x110)]()===this[_0x3d3967(0x230)]();},VisuMZ[_0x53c255(0x2cc)]['Game_Actor_setup']=Game_Actor[_0x53c255(0x819)][_0x53c255(0x157)],Game_Actor[_0x53c255(0x819)]['setup']=function(_0x373f93){const _0x1e268f=_0x53c255;VisuMZ['BattleCore'][_0x1e268f(0x369)]['call'](this,_0x373f93),this[_0x1e268f(0x21b)]();},Game_Actor[_0x53c255(0x819)][_0x53c255(0x21b)]=function(){const _0x1237c0=_0x53c255;this[_0x1237c0(0x424)]='',this[_0x1237c0(0x7de)]()&&this['actor']()['note']['match'](/<BATTLE (?:IMAGE|PORTRAIT):[ ](.*)>/i)&&(this[_0x1237c0(0x424)]=String(RegExp['$1']));},Game_Actor['prototype'][_0x53c255(0x386)]=function(){const _0x307c3c=_0x53c255;if(this[_0x307c3c(0x2a8)]()!=='')return this[_0x307c3c(0x2a8)]();else{if(Imported['VisuMZ_1_MainMenuCore']&&this[_0x307c3c(0x2a4)]()!=='')return this['getMenuImage']();}return'';},Game_Actor['prototype']['getBattlePortrait']=function(){const _0x2275a3=_0x53c255;if(this[_0x2275a3(0x424)]===undefined)this[_0x2275a3(0x21b)]();return this[_0x2275a3(0x424)];},Game_Actor[_0x53c255(0x819)][_0x53c255(0x7d4)]=function(_0x3f9156){const _0x1993ba=_0x53c255;if(this['_battlePortrait']===undefined)this[_0x1993ba(0x21b)]();this[_0x1993ba(0x424)]=_0x3f9156;if(SceneManager['isSceneBattle']()&&$gameParty[_0x1993ba(0x116)]()['includes'](this)){const _0x33fdf7=SceneManager[_0x1993ba(0x12f)]['_statusWindow'];if(_0x33fdf7)_0x33fdf7[_0x1993ba(0x6d5)](this);}},Game_Actor['prototype'][_0x53c255(0x670)]=function(){return!![];},Game_Actor[_0x53c255(0x819)]['isAutoBattle']=function(){const _0x5b6a70=_0x53c255;if(!this[_0x5b6a70(0x83e)]()&&BattleManager[_0x5b6a70(0x6ec)])return!![];return Game_Battler[_0x5b6a70(0x819)][_0x5b6a70(0x390)][_0x5b6a70(0x6b9)](this);},VisuMZ['BattleCore'][_0x53c255(0x5b8)]=Game_Actor['prototype']['makeActionList'],Game_Actor[_0x53c255(0x819)]['makeActionList']=function(){const _0x36d598=_0x53c255;if(BattleManager[_0x36d598(0x6ec)]&&!ConfigManager['autoBattleUseSkills'])return this[_0x36d598(0x11c)]();else{return VisuMZ[_0x36d598(0x2cc)][_0x36d598(0x5b8)][_0x36d598(0x6b9)](this);;}},Game_Actor[_0x53c255(0x819)][_0x53c255(0x11c)]=function(){const _0x232fa6=[],_0x2ae1f4=new Game_Action(this);return _0x2ae1f4['setAttack'](),_0x232fa6['push'](_0x2ae1f4),_0x232fa6;},Game_Actor['prototype'][_0x53c255(0x5fb)]=function(){const _0x2a3111=_0x53c255;return this[_0x2a3111(0x260)]()[_0x2a3111(0x236)][_0x2a3111(0x473)](/<BATTLE COMMANDS>\s*([\s\S]*)\s*<\/BATTLE COMMANDS>/i)?String(RegExp['$1'])[_0x2a3111(0x30d)](/[\r\n]+/):VisuMZ['BattleCore']['Settings']['ActorCmd'][_0x2a3111(0x5d2)];},Game_Actor['prototype'][_0x53c255(0x189)]=function(){const _0xe07ab8=_0x53c255;if(this[_0xe07ab8(0x1fd)][_0xe07ab8(0x72c)]!==undefined)return this[_0xe07ab8(0x1fd)][_0xe07ab8(0x72c)];return this[_0xe07ab8(0x7de)]()[_0xe07ab8(0x236)][_0xe07ab8(0x473)](/<SIDEVIEW ANCHOR: (.*), (.*)>/i)?(this[_0xe07ab8(0x1fd)]['svAnchorX']=eval(RegExp['$1']),this[_0xe07ab8(0x1fd)][_0xe07ab8(0x784)]=eval(RegExp['$2'])):this[_0xe07ab8(0x1fd)][_0xe07ab8(0x72c)]=Game_Battler[_0xe07ab8(0x819)][_0xe07ab8(0x189)][_0xe07ab8(0x6b9)](this),this[_0xe07ab8(0x1fd)][_0xe07ab8(0x72c)];},Game_Actor['prototype']['svBattlerAnchorY']=function(){const _0xd7aa34=_0x53c255;if(this[_0xd7aa34(0x1fd)][_0xd7aa34(0x784)]!==undefined)return this[_0xd7aa34(0x1fd)][_0xd7aa34(0x784)];return this['actor']()[_0xd7aa34(0x236)][_0xd7aa34(0x473)](/<SIDEVIEW ANCHOR: (.*), (.*)>/i)?(this[_0xd7aa34(0x1fd)]['svAnchorX']=eval(RegExp['$1']),this[_0xd7aa34(0x1fd)][_0xd7aa34(0x784)]=eval(RegExp['$2'])):this[_0xd7aa34(0x1fd)][_0xd7aa34(0x784)]=Game_Battler[_0xd7aa34(0x819)][_0xd7aa34(0x47e)]['call'](this),this['_cache'][_0xd7aa34(0x784)];},Game_Actor[_0x53c255(0x819)]['svBattlerShadowVisible']=function(){const _0x2b6d9e=_0x53c255;if(this[_0x2b6d9e(0x1fd)][_0x2b6d9e(0x3c0)]!==undefined)return this[_0x2b6d9e(0x1fd)]['svShadow'];if(this[_0x2b6d9e(0x7de)]()['note']['match'](/<SIDEVIEW SHOW SHADOW>/i))this[_0x2b6d9e(0x1fd)][_0x2b6d9e(0x3c0)]=!![];else this['actor']()[_0x2b6d9e(0x236)]['match'](/<SIDEVIEW HIDE SHADOW>/i)?this[_0x2b6d9e(0x1fd)][_0x2b6d9e(0x3c0)]=![]:this[_0x2b6d9e(0x1fd)][_0x2b6d9e(0x3c0)]=Game_Battler[_0x2b6d9e(0x819)][_0x2b6d9e(0x312)][_0x2b6d9e(0x6b9)](this);return this[_0x2b6d9e(0x1fd)]['svShadow'];},Game_Actor[_0x53c255(0x819)][_0x53c255(0x588)]=function(){const _0x3e8f11=_0x53c255;return VisuMZ[_0x3e8f11(0x2cc)]['Settings'][_0x3e8f11(0x38f)][_0x3e8f11(0x38d)];},Game_Actor[_0x53c255(0x819)][_0x53c255(0x7a4)]=function(){const _0x2977c8=_0x53c255,_0x9f1d31=this[_0x2977c8(0x851)](),_0x305f0b=_0x9f1d31[0x0]?_0x9f1d31[0x0][_0x2977c8(0x18b)]:0x0,_0x1166a8=$dataSystem[_0x2977c8(0x7ff)][_0x305f0b];_0x1166a8&&this[_0x2977c8(0x820)](_0x1166a8[_0x2977c8(0x1f6)]);},Game_Actor[_0x53c255(0x819)][_0x53c255(0x361)]=function(_0x2538ab){const _0x38bea2=_0x53c255;Game_Battler[_0x38bea2(0x819)]['performAction']['call'](this,_0x2538ab),this[_0x38bea2(0x627)](_0x2538ab);},Game_Actor[_0x53c255(0x819)][_0x53c255(0x567)]=function(){const _0x377ad5=_0x53c255,_0x1479fb=this[_0x377ad5(0x851)](),_0x5d442c=_0x1479fb[0x0]?_0x1479fb[0x0][_0x377ad5(0x18b)]:0x0;return $dataSystem[_0x377ad5(0x7ff)][_0x5d442c];},Game_Actor[_0x53c255(0x819)][_0x53c255(0x156)]=function(_0x42e2ba){const _0x1c3b6c=_0x53c255;_0x42e2ba=_0x42e2ba||0x1,_0x42e2ba--;const _0x218915=this['weapons']();return _0x218915[_0x42e2ba]?_0x218915[_0x42e2ba][_0x1c3b6c(0x3b3)]:0x0;},Game_Actor[_0x53c255(0x819)][_0x53c255(0x33e)]=function(_0x2e615a){const _0x15c7ae=_0x53c255;_0x2e615a=_0x2e615a||0x1,_0x2e615a--;const _0xb851f0=this['weapons'](),_0x498ce7=_0xb851f0[_0x2e615a]?_0xb851f0[_0x2e615a][_0x15c7ae(0x18b)]:0x0;return $dataSystem['attackMotions'][_0x498ce7];},Game_Actor[_0x53c255(0x819)][_0x53c255(0x596)]=function(_0x55bf5c){const _0x2935d4=_0x53c255;_0x55bf5c=_0x55bf5c||0x1,_0x55bf5c--;const _0x3efa50=this[_0x2935d4(0x851)](),_0x1d969c=_0x3efa50[_0x55bf5c]?_0x3efa50[_0x55bf5c]['wtypeId']:0x0,_0x26d4c5=$dataSystem['attackMotions'][_0x1d969c];if(_0x26d4c5){if(_0x26d4c5[_0x2935d4(0x3ce)]===0x0)this[_0x2935d4(0x2a9)](_0x2935d4(0x81e));else{if(_0x26d4c5[_0x2935d4(0x3ce)]===0x1)this[_0x2935d4(0x2a9)](_0x2935d4(0x14e));else _0x26d4c5['type']===0x2&&this['requestMotion'](_0x2935d4(0x368));}this['startWeaponAnimation'](_0x26d4c5[_0x2935d4(0x1f6)]);}},Game_Battler['prototype'][_0x53c255(0x11f)]=function(_0x4f8aaa){const _0xbeb6cc=_0x53c255;this[_0xbeb6cc(0x84b)]=_0x4f8aaa||0x0;},Game_Battler['prototype']['nextActiveWeaponSlot']=function(){this['_activeWeaponSlot']=this['_activeWeaponSlot']||0x0,this['_activeWeaponSlot']++;},Game_Battler[_0x53c255(0x819)][_0x53c255(0x187)]=function(){const _0x1725bd=_0x53c255;this[_0x1725bd(0x84b)]=undefined;},VisuMZ['BattleCore']['Game_Actor_equips']=Game_Actor['prototype'][_0x53c255(0x636)],Game_Actor[_0x53c255(0x819)][_0x53c255(0x636)]=function(){const _0x5d61c5=_0x53c255;let _0xf254c7=VisuMZ['BattleCore'][_0x5d61c5(0x2cd)][_0x5d61c5(0x6b9)](this);if(this[_0x5d61c5(0x5c8)])return _0xf254c7;if(this['_activeWeaponSlot']!==undefined){this[_0x5d61c5(0x5c8)]=!![];const _0x3ba45c=this['equipSlots']();for(let _0x239012=0x0;_0x239012<_0x3ba45c[_0x5d61c5(0x343)];_0x239012++){_0x3ba45c[_0x239012]===0x1&&this[_0x5d61c5(0x84b)]!==_0x239012&&(_0xf254c7[_0x239012]=null);}this[_0x5d61c5(0x5c8)]=undefined;}return _0xf254c7;},Window_BattleLog['prototype'][_0x53c255(0x39e)]=function(_0x4533b5){const _0x5b5c79=_0x53c255;return _0x4533b5[_0x5b5c79(0x807)]()?_0x4533b5[_0x5b5c79(0x851)]()[_0x5b5c79(0x343)]||0x1:0x1;},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x394)]=function(_0x233922,_0x252edf){const _0x56e5e9=_0x53c255;_0x233922&&_0x233922[_0x56e5e9(0x807)]()&&_0x233922['setActiveWeaponSlot'](_0x252edf),this[_0x56e5e9(0x1a3)]();},Window_BattleLog[_0x53c255(0x819)]['clearActiveWeaponSet']=function(_0x2e588a){const _0x533fae=_0x53c255;_0x2e588a&&_0x2e588a[_0x533fae(0x807)]()&&_0x2e588a[_0x533fae(0x187)](),this['callNextMethod']();},Game_Actor['prototype'][_0x53c255(0x25c)]=function(){const _0x2628e8=_0x53c255;let _0x29c491=_0x2628e8(0x25c);if(this['checkCacheKey'](_0x29c491))return this[_0x2628e8(0x1fd)][_0x29c491];return this['_cache'][_0x29c491]=this[_0x2628e8(0x150)](this['actor']()),this[_0x2628e8(0x1fd)][_0x29c491];},Game_Actor['prototype'][_0x53c255(0x235)]=function(){const _0x69e14d=_0x53c255;let _0x45ab6f='battleUIOffsetY';if(this[_0x69e14d(0x1fb)](_0x45ab6f))return this[_0x69e14d(0x1fd)][_0x45ab6f];return this[_0x69e14d(0x1fd)][_0x45ab6f]=this[_0x69e14d(0x123)](this[_0x69e14d(0x7de)]()),this[_0x69e14d(0x1fd)][_0x45ab6f];},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x5ce)]=Game_Enemy[_0x53c255(0x819)][_0x53c255(0x157)],Game_Enemy['prototype'][_0x53c255(0x157)]=function(_0x650d6e,_0x548502,_0x6f3f32){const _0x251259=_0x53c255;_0x650d6e=DataManager[_0x251259(0x130)](_0x650d6e),VisuMZ[_0x251259(0x2cc)][_0x251259(0x5ce)][_0x251259(0x6b9)](this,_0x650d6e,_0x548502,_0x6f3f32),Imported[_0x251259(0x56b)]&&this['initElementStatusCore'](),this[_0x251259(0x5bb)](),this['setupBattleCoreData'](),Imported[_0x251259(0x56b)]&&this[_0x251259(0x31f)]();},Game_Enemy[_0x53c255(0x819)]['clearBattleCoreData']=function(){const _0x32ac16=_0x53c255,_0x50ab61=VisuMZ[_0x32ac16(0x2cc)][_0x32ac16(0x4b3)][_0x32ac16(0x1c7)];this[_0x32ac16(0x68a)]=_0x50ab61[_0x32ac16(0x3bd)],this[_0x32ac16(0x722)]={};},Game_Enemy[_0x53c255(0x819)]['setupBattleCoreData']=function(){const _0x123804=_0x53c255,_0x572e8d=VisuMZ['BattleCore']['Settings'][_0x123804(0x1c7)],_0x5c7c21=this[_0x123804(0x221)]()[_0x123804(0x236)];this[_0x123804(0x722)]={'name':'','wtypeId':_0x572e8d['WtypeId'],'collapse':_0x572e8d['AllowCollapse'],'motionIdle':_0x572e8d[_0x123804(0x4b9)],'width':_0x572e8d[_0x123804(0x4a6)]||0x40,'height':_0x572e8d['Height']||0x40,'anchorX':_0x572e8d[_0x123804(0x646)]||0x0,'anchorY':_0x572e8d[_0x123804(0x779)]||0x0,'shadow':_0x572e8d['Shadow']};_0x5c7c21[_0x123804(0x473)](/<ATTACK ANIMATION:[ ](\d+)>/i)&&(this[_0x123804(0x68a)]=Number(RegExp['$1']));const _0x14dc15=this[_0x123804(0x722)];if(_0x5c7c21[_0x123804(0x473)](/<SIDEVIEW BATTLER: (.*)>/i))_0x14dc15[_0x123804(0x701)]=String(RegExp['$1']);else{if(_0x5c7c21[_0x123804(0x473)](/<SIDEVIEW BATTLERS>\s*([\s\S]*)\s*<\/SIDEVIEW BATTLERS>/i)){const _0x466220=String(RegExp['$1'])[_0x123804(0x30d)](/[\r\n]+/)[_0x123804(0x725)]('');_0x14dc15['name']=DataManager['processRandomizedData'](_0x466220);}}_0x5c7c21[_0x123804(0x473)](/<SIDEVIEW ANCHOR: (.*), (.*)>/i)&&(_0x14dc15[_0x123804(0x6e3)]=eval(RegExp['$1']),_0x14dc15['anchorY']=eval(RegExp['$2']));if(_0x5c7c21[_0x123804(0x473)](/<SIDEVIEW COLLAPSE>/i))_0x14dc15['collapse']=!![];else _0x5c7c21[_0x123804(0x473)](/<SIDEVIEW NO COLLAPSE>/i)&&(_0x14dc15['collapse']=![]);if(_0x5c7c21[_0x123804(0x473)](/<SIDEVIEW SHOW SHADOW>/i))_0x14dc15[_0x123804(0x601)]=!![];else _0x5c7c21['match'](/<SIDEVIEW HIDE SHADOW>/i)&&(_0x14dc15[_0x123804(0x601)]=![]);if(_0x5c7c21['match'](/<SIDEVIEW IDLE MOTION: (.*)>/i))_0x14dc15['motionIdle']=String(RegExp['$1'])[_0x123804(0x534)]()[_0x123804(0x615)]();else{if(_0x5c7c21[_0x123804(0x473)](/<SIDEVIEW IDLE MOTIONS>\s*([\s\S]*)\s*<\/SIDEVIEW IDLE MOTIONS>/i)){const _0x428cc2=String(RegExp['$1'])[_0x123804(0x30d)](/[\r\n]+/)['remove']('');_0x14dc15[_0x123804(0x759)]=DataManager[_0x123804(0x18e)](_0x428cc2);}}_0x5c7c21[_0x123804(0x473)](/<SIDEVIEW SIZE: (\d+), (\d+)>/i)&&(_0x14dc15['width']=Number(RegExp['$1']),_0x14dc15[_0x123804(0x6ab)]=Number(RegExp['$2']));if(_0x5c7c21[_0x123804(0x473)](/<SIDEVIEW WEAPON: (.*)>/i))_0x14dc15['wtypeId']=DataManager[_0x123804(0x16b)](RegExp['$1']);else{if(_0x5c7c21['match'](/<SIDEVIEW WEAPONS>\s*([\s\S]*)\s*<\/SIDEVIEW WEAPONS>/i)){const _0x75c10=String(RegExp['$1'])[_0x123804(0x30d)](/[\r\n]+/)[_0x123804(0x725)](''),_0x38387f=DataManager[_0x123804(0x18e)](_0x75c10);_0x14dc15[_0x123804(0x18b)]=DataManager[_0x123804(0x16b)](_0x38387f);}}if(Imported['VisuMZ_1_ElementStatusCore']){const _0x4bd578=this[_0x123804(0x12b)]();for(const _0x5f20a9 of _0x4bd578){const _0x3f3427=this[_0x123804(0x408)](_0x5f20a9)[_0x123804(0x509)][_0x123804(0x61d)]()[_0x123804(0x615)](),_0x3deb2d=_0x5f20a9['toUpperCase']()[_0x123804(0x615)]();if(_0x5c7c21[_0x123804(0x473)](VisuMZ['ElementStatusCore'][_0x123804(0x4bc)][_0x123804(0x3d8)[_0x123804(0x107)](_0x3deb2d,_0x3f3427)]))_0x14dc15[_0x123804(0x701)]=String(RegExp['$1']);else{if(_0x5c7c21[_0x123804(0x473)](VisuMZ[_0x123804(0x83d)][_0x123804(0x4bc)][_0x123804(0x1bf)[_0x123804(0x107)](_0x3deb2d,_0x3f3427)])){const _0x2ef90a=String(RegExp['$1'])['split'](/[\r\n]+/)[_0x123804(0x725)]('');_0x14dc15[_0x123804(0x701)]=DataManager[_0x123804(0x18e)](_0x2ef90a);}}if(_0x5c7c21[_0x123804(0x473)](VisuMZ[_0x123804(0x83d)][_0x123804(0x4bc)][_0x123804(0x6f6)[_0x123804(0x107)](_0x3deb2d,_0x3f3427)]))_0x14dc15['wtypeId']=DataManager[_0x123804(0x16b)](RegExp['$1']);else{if(_0x5c7c21[_0x123804(0x473)](VisuMZ[_0x123804(0x83d)]['RegExp'][_0x123804(0x716)[_0x123804(0x107)](_0x3deb2d,_0x3f3427)])){const _0x1656ee=String(RegExp['$1'])[_0x123804(0x30d)](/[\r\n]+/)[_0x123804(0x725)](''),_0x3f8e72=DataManager[_0x123804(0x18e)](_0x1656ee);_0x14dc15[_0x123804(0x18b)]=DataManager[_0x123804(0x16b)](_0x3f8e72);}}if(_0x5c7c21[_0x123804(0x473)](VisuMZ[_0x123804(0x83d)][_0x123804(0x4bc)]['SvMotionIdleSolo-%1-%2'[_0x123804(0x107)](_0x3deb2d,_0x3f3427)]))_0x14dc15[_0x123804(0x759)]=String(RegExp['$1'])[_0x123804(0x534)]()[_0x123804(0x615)]();else{if(_0x5c7c21['match'](VisuMZ[_0x123804(0x83d)][_0x123804(0x4bc)]['SvMotionIdleMass-%1-%2'[_0x123804(0x107)](_0x3deb2d,_0x3f3427)])){const _0x17aeec=String(RegExp['$1'])['split'](/[\r\n]+/)[_0x123804(0x725)]('');_0x14dc15[_0x123804(0x759)]=DataManager[_0x123804(0x18e)](_0x17aeec);}}}}},Game_Enemy[_0x53c255(0x819)][_0x53c255(0x1cc)]=function(){const _0x5170d7=_0x53c255;return this[_0x5170d7(0x68a)]||0x0;},Game_Enemy[_0x53c255(0x819)][_0x53c255(0x6c3)]=function(){const _0x47d73f=_0x53c255;return this[_0x47d73f(0x1cc)]();},Game_Enemy[_0x53c255(0x819)]['attackAnimationIdSlot']=function(_0x106868){const _0x430ae9=_0x53c255;return this[_0x430ae9(0x1cc)]();},Game_Enemy['prototype'][_0x53c255(0x453)]=function(){const _0x2d8018=_0x53c255;if(this[_0x2d8018(0x221)]()[_0x2d8018(0x236)][_0x2d8018(0x473)](/<BATTLER SPRITE CANNOT MOVE>/i))return![];return Game_Battler[_0x2d8018(0x819)][_0x2d8018(0x453)][_0x2d8018(0x6b9)](this);},Game_Enemy[_0x53c255(0x819)][_0x53c255(0x839)]=function(){const _0x14201d=_0x53c255;if(this['enemy']()['note'][_0x14201d(0x473)](/<BATTLER SPRITE GROUNDED>/i))return!![];return![];},Game_Enemy[_0x53c255(0x819)][_0x53c255(0x736)]=function(){const _0xbbe408=_0x53c255,_0x2e084d=[];for(const _0x2024c9 of this[_0xbbe408(0x221)]()[_0xbbe408(0x22e)]){const _0x5cd179=$dataSkills[_0x2024c9[_0xbbe408(0x809)]];if(_0x5cd179&&!_0x2e084d[_0xbbe408(0x6a6)](_0x5cd179))_0x2e084d[_0xbbe408(0x2bf)](_0x5cd179);}return _0x2e084d;},Game_Enemy[_0x53c255(0x819)][_0x53c255(0x25c)]=function(){const _0x3ba54d=_0x53c255;let _0x419fb8=_0x3ba54d(0x25c);if(this['checkCacheKey'](_0x419fb8))return this[_0x3ba54d(0x1fd)][_0x419fb8];return this[_0x3ba54d(0x1fd)][_0x419fb8]=this[_0x3ba54d(0x150)](this[_0x3ba54d(0x221)]()),this[_0x3ba54d(0x1fd)][_0x419fb8];},Game_Enemy[_0x53c255(0x819)][_0x53c255(0x235)]=function(){const _0x3857d6=_0x53c255;let _0x24c15c=_0x3857d6(0x235);if(this[_0x3857d6(0x1fb)](_0x24c15c))return this[_0x3857d6(0x1fd)][_0x24c15c];return this['_cache'][_0x24c15c]=this[_0x3857d6(0x123)](this[_0x3857d6(0x221)]()),this[_0x3857d6(0x1fd)][_0x24c15c];},Game_Enemy[_0x53c255(0x819)]['svBattlerData']=function(){const _0x4d5086=_0x53c255;if(this[_0x4d5086(0x722)]!==undefined)return this[_0x4d5086(0x722)];return this[_0x4d5086(0x4a7)](),this[_0x4d5086(0x722)];},Game_Enemy[_0x53c255(0x819)]['hasSvBattler']=function(){const _0x343749=_0x53c255;return this[_0x343749(0x3f0)]()['name']!=='';},Game_Enemy[_0x53c255(0x819)]['svBattlerName']=function(){const _0x48f1c0=_0x53c255;return this[_0x48f1c0(0x3f0)]()[_0x48f1c0(0x701)];},Game_Enemy[_0x53c255(0x819)]['battlerSmoothImage']=function(){const _0x162ed9=_0x53c255;return this[_0x162ed9(0x366)]()?VisuMZ[_0x162ed9(0x2cc)][_0x162ed9(0x4b3)][_0x162ed9(0x38f)][_0x162ed9(0x38d)]:VisuMZ[_0x162ed9(0x2cc)][_0x162ed9(0x4b3)][_0x162ed9(0x1c7)][_0x162ed9(0x38d)];},Game_Enemy[_0x53c255(0x819)][_0x53c255(0x361)]=function(_0x3cf6e9){const _0x525b2c=_0x53c255;Game_Battler[_0x525b2c(0x819)][_0x525b2c(0x361)][_0x525b2c(0x6b9)](this,_0x3cf6e9);if(this[_0x525b2c(0x366)]())this[_0x525b2c(0x627)](_0x3cf6e9);},Game_Enemy[_0x53c255(0x819)][_0x53c255(0x4a0)]=function(){const _0x37e48c=_0x53c255,_0xe73fd6=this[_0x37e48c(0x3f0)]()['wtypeId']||0x0,_0x299cbd=$dataSystem[_0x37e48c(0x7ff)][_0xe73fd6];if(_0x299cbd){if(_0x299cbd['type']===0x0)this[_0x37e48c(0x2a9)](_0x37e48c(0x81e));else{if(_0x299cbd[_0x37e48c(0x3ce)]===0x1)this['requestMotion'](_0x37e48c(0x14e));else _0x299cbd[_0x37e48c(0x3ce)]===0x2&&this[_0x37e48c(0x2a9)]('missile');}}},Game_Enemy[_0x53c255(0x819)][_0x53c255(0x7a4)]=function(){const _0x16131d=_0x53c255,_0x5f0fc3=this[_0x16131d(0x3f0)]()[_0x16131d(0x18b)]||0x0,_0x253f39=$dataSystem[_0x16131d(0x7ff)][_0x5f0fc3];_0x253f39&&this[_0x16131d(0x820)](_0x253f39[_0x16131d(0x1f6)]);},Game_Enemy['prototype']['getAttackMotion']=function(){const _0x4e4a1c=_0x53c255,_0x317185=this[_0x4e4a1c(0x3f0)]()['wtypeId']||0x0;return $dataSystem['attackMotions'][_0x317185];},Game_Enemy[_0x53c255(0x819)][_0x53c255(0x33e)]=function(_0x1a702a){const _0x1c786c=_0x53c255;return this[_0x1c786c(0x567)]();},Game_Enemy[_0x53c255(0x819)][_0x53c255(0xfd)]=function(){const _0x3745b7=_0x53c255;Game_Battler[_0x3745b7(0x819)][_0x3745b7(0xfd)][_0x3745b7(0x6b9)](this),this[_0x3745b7(0x670)]()&&this['hasSvBattler']()&&this[_0x3745b7(0x2a9)]('damage'),SoundManager[_0x3745b7(0x4c6)]();},Game_Enemy['prototype'][_0x53c255(0x5d4)]=function(){const _0x1923a7=_0x53c255;Game_Battler[_0x1923a7(0x819)][_0x1923a7(0x5d4)]['call'](this),this[_0x1923a7(0x2a9)](_0x1923a7(0x801));},Game_Enemy[_0x53c255(0x819)][_0x53c255(0x53e)]=function(){const _0xdf289=_0x53c255;Game_Battler[_0xdf289(0x819)]['performMagicEvasion'][_0xdf289(0x6b9)](this),this[_0xdf289(0x2a9)](_0xdf289(0x801));},Game_Enemy[_0x53c255(0x819)][_0x53c255(0x6c1)]=function(){const _0x4378b1=_0x53c255;Game_Battler[_0x4378b1(0x819)][_0x4378b1(0x6c1)]['call'](this),this[_0x4378b1(0x4a0)]();},Game_Enemy['prototype'][_0x53c255(0x7ba)]=function(){const _0x3c3db1=_0x53c255;if(this[_0x3c3db1(0x366)]()){if(this[_0x3c3db1(0x29a)]()>=0x1)return!![];return this[_0x3c3db1(0x3f0)]()[_0x3c3db1(0x271)];}else return!![];},Game_Enemy[_0x53c255(0x819)][_0x53c255(0x189)]=function(){const _0xca66d1=_0x53c255;return this['svBattlerData']()[_0xca66d1(0x6e3)];},Game_Enemy['prototype']['svBattlerAnchorY']=function(){const _0x3ae536=_0x53c255;return this[_0x3ae536(0x3f0)]()['anchorY'];},Game_Enemy[_0x53c255(0x819)][_0x53c255(0x312)]=function(){const _0x1ca735=_0x53c255;return this[_0x1ca735(0x3f0)]()['shadow'];},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x4b4)]=Game_Enemy['prototype']['transform'],Game_Enemy[_0x53c255(0x819)][_0x53c255(0x812)]=function(_0x289780){const _0x59f016=_0x53c255;VisuMZ['BattleCore'][_0x59f016(0x4b4)]['call'](this,_0x289780),this[_0x59f016(0x5bb)](),this['setupBattleCoreData']();const _0x197423=this[_0x59f016(0x5ef)]();if(_0x197423)_0x197423[_0x59f016(0x6de)](this);},Game_Unit['prototype'][_0x53c255(0x195)]=function(_0xa73a94){const _0x30366c=_0x53c255;for(const _0x27eec9 of this[_0x30366c(0x4e3)]()){if(_0x27eec9)_0x27eec9[_0x30366c(0x195)](_0xa73a94);}},Game_Unit[_0x53c255(0x819)][_0x53c255(0x341)]=function(){const _0x456483=_0x53c255,_0x195fef=this[_0x456483(0x4d7)]();return _0x195fef[Math[_0x456483(0x5d9)](_0x195fef[_0x456483(0x343)])];},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x269)]=Game_Party[_0x53c255(0x819)]['addActor'],Game_Party[_0x53c255(0x819)][_0x53c255(0x762)]=function(_0x25258e){const _0x362a98=_0x53c255;VisuMZ[_0x362a98(0x2cc)]['Game_Party_addActor'][_0x362a98(0x6b9)](this,_0x25258e),BattleManager[_0x362a98(0x512)]();},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x49a)]=Game_Party[_0x53c255(0x819)][_0x53c255(0x5cb)],Game_Party[_0x53c255(0x819)][_0x53c255(0x5cb)]=function(_0x5d26a0){const _0x38e7ba=_0x53c255;VisuMZ['BattleCore'][_0x38e7ba(0x49a)][_0x38e7ba(0x6b9)](this,_0x5d26a0),BattleManager[_0x38e7ba(0x512)]();},VisuMZ['BattleCore']['Game_Troop_setup']=Game_Troop[_0x53c255(0x819)][_0x53c255(0x157)],Game_Troop[_0x53c255(0x819)][_0x53c255(0x157)]=function(_0x3ee9c0){const _0x5d770d=_0x53c255;$gameTemp[_0x5d770d(0x7f4)](),$gameTemp['applyForcedGameTroopSettingsBattleCore'](_0x3ee9c0),VisuMZ['BattleCore']['Game_Troop_setup'][_0x5d770d(0x6b9)](this,_0x3ee9c0);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x435)]=Game_Map['prototype'][_0x53c255(0x526)],Game_Map[_0x53c255(0x819)][_0x53c255(0x526)]=function(){const _0x3ada89=_0x53c255;VisuMZ[_0x3ada89(0x2cc)]['Game_Map_setupBattleback'][_0x3ada89(0x6b9)](this),this[_0x3ada89(0x690)]();},Game_Map[_0x53c255(0x819)][_0x53c255(0x690)]=function(){const _0xe1eadb=_0x53c255;this[_0xe1eadb(0x1ba)]={},this[_0xe1eadb(0x6f5)]={};if(!$dataMap)return;const _0x2c756f=$dataMap[_0xe1eadb(0x236)];if(!_0x2c756f)return;const _0x45c0b6=_0x2c756f['match'](/<REGION (\d+) BATTLEBACK(\d+): (.*)>/gi);if(_0x45c0b6)for(const _0x1a3eb6 of _0x45c0b6){_0x1a3eb6[_0xe1eadb(0x473)](/<REGION (\d+) BATTLEBACK(\d+): (.*)>/i);const _0x265045=Number(RegExp['$1']),_0x4844b6=Number(RegExp['$2']),_0x4a3ee8=_0x4844b6===0x1?this[_0xe1eadb(0x1ba)]:this[_0xe1eadb(0x6f5)],_0x1f5354=String(RegExp['$3']);_0x4a3ee8[_0x265045]=_0x1f5354;}},VisuMZ['BattleCore'][_0x53c255(0x448)]=Game_Map[_0x53c255(0x819)][_0x53c255(0xe8)],Game_Map[_0x53c255(0x819)][_0x53c255(0xe8)]=function(){const _0x4986d9=_0x53c255;if(!BattleManager[_0x4986d9(0x649)]()){const _0x454a86=$gamePlayer[_0x4986d9(0x4b1)]($gamePlayer['x'],$gamePlayer['y']);if(this[_0x4986d9(0x1ba)]&&this[_0x4986d9(0x1ba)][_0x454a86])return this['_regionBattleback1'][_0x454a86];}return VisuMZ[_0x4986d9(0x2cc)]['Game_Map_battleback1Name'][_0x4986d9(0x6b9)](this);},VisuMZ['BattleCore']['Game_Map_battleback2Name']=Game_Map[_0x53c255(0x819)]['battleback2Name'],Game_Map[_0x53c255(0x819)][_0x53c255(0x335)]=function(){const _0xc0d84d=_0x53c255;if(!BattleManager[_0xc0d84d(0x649)]()){const _0x5bb28f=$gamePlayer[_0xc0d84d(0x4b1)]($gamePlayer['x'],$gamePlayer['y']);if(this[_0xc0d84d(0x1ba)]&&this[_0xc0d84d(0x6f5)][_0x5bb28f])return this[_0xc0d84d(0x6f5)][_0x5bb28f];}return VisuMZ[_0xc0d84d(0x2cc)][_0xc0d84d(0x43a)]['call'](this);},VisuMZ['BattleCore'][_0x53c255(0x7cd)]=Game_Interpreter['prototype'][_0x53c255(0x7ae)],Game_Interpreter[_0x53c255(0x819)][_0x53c255(0x7ae)]=function(_0x2dcff3){const _0x156368=_0x53c255;return $gameTemp['setLastPluginCommandInterpreter'](this),VisuMZ[_0x156368(0x2cc)][_0x156368(0x7cd)]['call'](this,_0x2dcff3);},VisuMZ['BattleCore'][_0x53c255(0x268)]=Game_Interpreter[_0x53c255(0x819)][_0x53c255(0x34e)],Game_Interpreter[_0x53c255(0x819)][_0x53c255(0x34e)]=function(){const _0xceb9c6=_0x53c255;if(SceneManager['isSceneBattle']())switch(this[_0xceb9c6(0x154)]){case _0xceb9c6(0x342):if(Imported[_0xceb9c6(0x78c)]){if($gameScreen['battleCameraData']()[_0xceb9c6(0x38b)]>0x0)return!![];this['_waitMode']='';}break;case'battleAnimation':if(BattleManager[_0xceb9c6(0x3fb)][_0xceb9c6(0x525)]())return!![];this['_waitMode']='';break;case'battleCamera':if(Imported['VisuMZ_3_ActSeqCamera']){if($gameScreen[_0xceb9c6(0x350)]()[_0xceb9c6(0x325)]>0x0)return!![];if($gameScreen['battleCameraData']()['cameraOffsetDuration']>0x0)return!![];this['_waitMode']='';}break;case'battleEffect':if(BattleManager['_spriteset'][_0xceb9c6(0x521)]())return!![];this['_waitMode']='';break;case _0xceb9c6(0x687):if(BattleManager['_spriteset']['isAnyoneFloating']())return!![];this['_waitMode']='';break;case'battleJump':if(BattleManager[_0xceb9c6(0x3fb)][_0xceb9c6(0x3cd)]())return!![];this[_0xceb9c6(0x154)]='';break;case _0xceb9c6(0x79c):if(BattleManager[_0xceb9c6(0x47a)][_0xceb9c6(0x456)]())return!![];this['_waitMode']='';break;case _0xceb9c6(0x285):if(BattleManager['_spriteset'][_0xceb9c6(0x34c)]())return!![];this[_0xceb9c6(0x154)]='';break;case'battleOpacity':if(BattleManager[_0xceb9c6(0x3fb)][_0xceb9c6(0x1c1)]())return!![];this['_waitMode']='';break;case'battleGrow':if(BattleManager['_spriteset'][_0xceb9c6(0x186)]())return!![];this['_waitMode']='';break;case _0xceb9c6(0x7fc):if(BattleManager['_spriteset']['isAnyoneSkewing']())return!![];this['_waitMode']='';break;case _0xceb9c6(0x461):if(Imported[_0xceb9c6(0x771)]){if(BattleManager[_0xceb9c6(0x3fb)][_0xceb9c6(0x163)]())return!![];this[_0xceb9c6(0x154)]='';}break;case'battleSkew':if(Imported[_0xceb9c6(0x78c)]){if($gameScreen[_0xceb9c6(0x350)]()['skewDuration']>0x0)return!![];this[_0xceb9c6(0x154)]='';}break;case _0xceb9c6(0xda):if(BattleManager[_0xceb9c6(0x3fb)][_0xceb9c6(0x6cf)]())return!![];this['_waitMode']='';break;case'battleZoom':if(Imported['VisuMZ_3_ActSeqCamera']){if($gameScreen['battleCameraData']()[_0xceb9c6(0x735)]>0x0)return!![];this[_0xceb9c6(0x154)]='';}break;}return VisuMZ[_0xceb9c6(0x2cc)][_0xceb9c6(0x268)][_0xceb9c6(0x6b9)](this);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0xfb)]=Game_Interpreter[_0x53c255(0x819)][_0x53c255(0x2c7)],Game_Interpreter[_0x53c255(0x819)][_0x53c255(0x2c7)]=function(_0x17b6ec){const _0x552d13=_0x53c255;return!$gameParty['inBattle']()?this[_0x552d13(0xdd)](_0x17b6ec):VisuMZ[_0x552d13(0x2cc)][_0x552d13(0xfb)][_0x552d13(0x6b9)](this,_0x17b6ec);},Game_Interpreter['prototype'][_0x53c255(0x365)]=function(_0x3d4aaf){const _0xf81ee4=_0x53c255;return VisuMZ[_0xf81ee4(0x2cc)][_0xf81ee4(0xfb)][_0xf81ee4(0x6b9)](this,_0x3d4aaf),BattleManager[_0xf81ee4(0x243)](_0x1ad257=>{const _0x483c7a=_0xf81ee4;this[_0x483c7a(0x14a)][this[_0x483c7a(0x1ea)]]=_0x1ad257;}),!![];},VisuMZ[_0x53c255(0x2cc)]['CheckMapBattleEventValid']=function(_0x4086b1){const _0x216fc0=_0x53c255,_0x3bca1e=$dataCommonEvents[_0x4086b1];if(!_0x3bca1e)return![];if(_0x3bca1e[_0x216fc0(0x4a9)][_0x216fc0(0x343)]<=0x1)return![];return!![];},Game_Interpreter[_0x53c255(0x819)][_0x53c255(0xdd)]=function(_0x3bb232){const _0x2ba2c2=_0x53c255,_0x1e5c1e=VisuMZ[_0x2ba2c2(0x2cc)][_0x2ba2c2(0x4b3)][_0x2ba2c2(0x2ff)],_0x4f6fd9=_0x1e5c1e[_0x2ba2c2(0x529)],_0x355fe7=$dataCommonEvents[_0x4f6fd9];if(_0x355fe7&&VisuMZ[_0x2ba2c2(0x2cc)][_0x2ba2c2(0x2cf)](_0x4f6fd9)){const _0x4854bc=this[_0x2ba2c2(0x7bb)]()?this['_eventId']:0x0,_0x300fc4=_0x355fe7[_0x2ba2c2(0x4a9)];this['setupChild'](_0x300fc4,_0x4854bc),this['_list']=JsonEx[_0x2ba2c2(0x37a)](this['_list']);const _0x2d0573={'code':0xbc3,'indent':this[_0x2ba2c2(0x1ea)],'parameters':JsonEx[_0x2ba2c2(0x37a)](_0x3bb232)};return this[_0x2ba2c2(0x73b)][_0x2ba2c2(0x5e4)](this[_0x2ba2c2(0x805)]+0x1,0x0,_0x2d0573),!![];}else return VisuMZ[_0x2ba2c2(0x2cc)][_0x2ba2c2(0xfb)]['call'](this,_0x3bb232);},VisuMZ[_0x53c255(0x2cc)]['BattleManager_onEncounter']=BattleManager['onEncounter'],BattleManager['onEncounter']=function(){const _0x375c35=_0x53c255;VisuMZ[_0x375c35(0x2cc)][_0x375c35(0x829)][_0x375c35(0x6b9)](this),this['onEncounterBattleCore']();},BattleManager[_0x53c255(0x20b)]=function(){const _0x2e888f=_0x53c255,_0x563bfe=VisuMZ[_0x2e888f(0x2cc)][_0x2e888f(0x4b3)][_0x2e888f(0x2ff)],_0x5112f8=_0x563bfe[_0x2e888f(0x529)];_0x5112f8&&VisuMZ[_0x2e888f(0x2cc)][_0x2e888f(0x2cf)](_0x5112f8)&&(this[_0x2e888f(0x641)]=!![],$gameTemp[_0x2e888f(0x5ad)](_0x563bfe[_0x2e888f(0x529)]),$gameMap['updateInterpreter'](),$gameMap[_0x2e888f(0x738)][_0x2e888f(0x34d)]=!![]),_0x563bfe[_0x2e888f(0xd5)]>0x0&&(this[_0x2e888f(0x6c5)]=!![]);},VisuMZ['BattleCore']['Scene_Map_launchBattle']=Scene_Map[_0x53c255(0x819)][_0x53c255(0x614)],Scene_Map[_0x53c255(0x819)]['launchBattle']=function(){const _0x43f083=_0x53c255;BattleManager['_battleCoreBattleStartEvent']?this[_0x43f083(0x458)]():VisuMZ[_0x43f083(0x2cc)][_0x43f083(0xfa)][_0x43f083(0x6b9)](this);},Scene_Map[_0x53c255(0x819)]['battleCorePreBattleCommonEvent']=function(){const _0x1edcf9=_0x53c255;this[_0x1edcf9(0x25f)]=!![];},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x541)]=SceneManager['isSceneChanging'],SceneManager[_0x53c255(0x50f)]=function(){const _0x5c0a5b=_0x53c255;if(BattleManager[_0x5c0a5b(0x641)])return![];return VisuMZ[_0x5c0a5b(0x2cc)][_0x5c0a5b(0x541)][_0x5c0a5b(0x6b9)](this);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x774)]=Game_Interpreter[_0x53c255(0x819)]['terminate'],Game_Interpreter[_0x53c255(0x819)][_0x53c255(0x72d)]=function(){const _0x46f469=_0x53c255;VisuMZ['BattleCore'][_0x46f469(0x774)][_0x46f469(0x6b9)](this),this['_preBattleCommonEvent']&&(this[_0x46f469(0x34d)]=undefined,SceneManager[_0x46f469(0x12f)][_0x46f469(0x814)]());},Scene_Map[_0x53c255(0x819)][_0x53c255(0x814)]=function(){const _0x133616=_0x53c255;BattleManager[_0x133616(0x641)]=undefined,this[_0x133616(0x2b2)]();},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x4c1)]=Scene_Map[_0x53c255(0x819)]['initialize'],Scene_Map['prototype']['initialize']=function(){const _0xd694f3=_0x53c255;VisuMZ[_0xd694f3(0x2cc)][_0xd694f3(0x4c1)]['call'](this),$gameTemp[_0xd694f3(0x7f4)]();},VisuMZ['BattleCore'][_0x53c255(0x2a0)]=Scene_ItemBase['prototype'][_0x53c255(0x115)],Scene_ItemBase[_0x53c255(0x819)][_0x53c255(0x115)]=function(){const _0x2881c1=_0x53c255;VisuMZ[_0x2881c1(0x2cc)][_0x2881c1(0x2a0)][_0x2881c1(0x6b9)](this),this[_0x2881c1(0x4b7)]()[_0x2881c1(0x236)][_0x2881c1(0x473)](/<CUSTOM ACTION SEQUENCE>/i)&&($gameTemp['_commonEventQueue']=[]),DataManager[_0x2881c1(0x3ff)](this[_0x2881c1(0x4b7)]())&&($gameTemp[_0x2881c1(0x166)]=[]);},VisuMZ[_0x53c255(0x2cc)]['Scene_Options_maxCommands']=Scene_Options[_0x53c255(0x819)]['maxCommands'],Scene_Options[_0x53c255(0x819)][_0x53c255(0x242)]=function(){const _0x5e9916=_0x53c255;let _0x308736=VisuMZ[_0x5e9916(0x2cc)][_0x5e9916(0x1aa)][_0x5e9916(0x6b9)](this);const _0x39a5b4=VisuMZ[_0x5e9916(0x2cc)][_0x5e9916(0x4b3)];if(_0x39a5b4[_0x5e9916(0x216)][_0x5e9916(0x380)]&&_0x39a5b4[_0x5e9916(0x216)][_0x5e9916(0x767)])_0x308736+=0x2;if(_0x39a5b4['HpGauge'][_0x5e9916(0x380)]&&_0x39a5b4[_0x5e9916(0x18f)][_0x5e9916(0x767)])_0x308736+=0x1;return _0x308736;},VisuMZ['BattleCore'][_0x53c255(0x30e)]=Scene_Battle[_0x53c255(0x819)][_0x53c255(0x7e8)],Scene_Battle[_0x53c255(0x819)][_0x53c255(0x7e8)]=function(){const _0x132f6f=_0x53c255;SceneManager[_0x132f6f(0x791)]()?(Scene_Message[_0x132f6f(0x819)][_0x132f6f(0x7e8)][_0x132f6f(0x6b9)](this),this[_0x132f6f(0x3fb)]&&this[_0x132f6f(0x3fb)][_0x132f6f(0x3ed)](),BattleManager['_tpbSceneChangeCacheActor']&&BattleManager['revertTpbCachedActor']()):VisuMZ[_0x132f6f(0x2cc)][_0x132f6f(0x30e)][_0x132f6f(0x6b9)](this);},VisuMZ[_0x53c255(0x2cc)]['Scene_Battle_stop']=Scene_Battle[_0x53c255(0x819)][_0x53c255(0x2b2)],Scene_Battle['prototype'][_0x53c255(0x2b2)]=function(){const _0x2a09ca=_0x53c255;SceneManager['isNextSceneBattleTransitionable']()?Scene_Message[_0x2a09ca(0x819)][_0x2a09ca(0x2b2)]['call'](this):VisuMZ[_0x2a09ca(0x2cc)]['Scene_Battle_stop'][_0x2a09ca(0x6b9)](this);},VisuMZ[_0x53c255(0x2cc)]['Scene_Battle_terminate']=Scene_Battle['prototype'][_0x53c255(0x72d)],Scene_Battle['prototype'][_0x53c255(0x72d)]=function(){const _0x1eeb4c=_0x53c255;SceneManager[_0x1eeb4c(0x28c)]()?Scene_Message['prototype'][_0x1eeb4c(0x72d)]['call'](this):VisuMZ['BattleCore']['Scene_Battle_terminate']['call'](this);},Scene_Battle['prototype'][_0x53c255(0x64f)]=function(){const _0x5edc74=_0x53c255;if(ConfigManager['uiMenuStyle']&&ConfigManager[_0x5edc74(0x177)]!==undefined)return ConfigManager[_0x5edc74(0x177)];else{if(this[_0x5edc74(0x139)]()===_0x5edc74(0x7a0))return![];else{return Scene_Message[_0x5edc74(0x819)][_0x5edc74(0x64f)][_0x5edc74(0x6b9)](this);;}}},VisuMZ['BattleCore'][_0x53c255(0x532)]=Scene_Battle['prototype']['createAllWindows'],Scene_Battle['prototype'][_0x53c255(0x55b)]=function(){const _0x2595ab=_0x53c255;this[_0x2595ab(0x62e)](),VisuMZ[_0x2595ab(0x2cc)]['Scene_Battle_createAllWindows'][_0x2595ab(0x6b9)](this),this[_0x2595ab(0x2e5)]();},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x445)]=Scene_Battle[_0x53c255(0x819)][_0x53c255(0x129)],Scene_Battle['prototype'][_0x53c255(0x129)]=function(){const _0x5ec854=_0x53c255;VisuMZ[_0x5ec854(0x2cc)][_0x5ec854(0x445)][_0x5ec854(0x6b9)](this),this[_0x5ec854(0x139)]()===_0x5ec854(0x7a0)&&this[_0x5ec854(0x46e)]();},Scene_Battle['prototype'][_0x53c255(0xea)]=function(_0x576dd4){const _0x133eb0=_0x53c255;_0x576dd4?(this[_0x133eb0(0x3a3)]['x']=(Graphics[_0x133eb0(0x261)]-Graphics[_0x133eb0(0x32c)])/0x2,this[_0x133eb0(0x3a3)]['y']=(Graphics[_0x133eb0(0x6ab)]-Graphics[_0x133eb0(0x75a)])/0x2):(this[_0x133eb0(0x3a3)]['x']=Graphics[_0x133eb0(0x261)]*0xa,this['_windowLayer']['y']=Graphics[_0x133eb0(0x6ab)]*0xa);},VisuMZ['BattleCore'][_0x53c255(0x640)]=Scene_Battle[_0x53c255(0x819)]['selectNextCommand'],Scene_Battle[_0x53c255(0x819)]['selectNextCommand']=function(){const _0x1669d5=_0x53c255,_0xc30894=BattleManager[_0x1669d5(0x7de)]();VisuMZ[_0x1669d5(0x2cc)][_0x1669d5(0x640)]['call'](this);if(_0xc30894){if(_0xc30894===BattleManager[_0x1669d5(0x7de)]())return;if(_0xc30894===BattleManager[_0x1669d5(0x650)])return;if(_0xc30894[_0x1669d5(0x5ef)]())_0xc30894[_0x1669d5(0x5ef)]()[_0x1669d5(0x2ee)]();}},VisuMZ['BattleCore']['Scene_Battle_selectPreviousCommand']=Scene_Battle[_0x53c255(0x819)][_0x53c255(0x68d)],Scene_Battle[_0x53c255(0x819)][_0x53c255(0x68d)]=function(){const _0x20b856=_0x53c255,_0x1d0b85=BattleManager[_0x20b856(0x7de)]();if(_0x1d0b85&&_0x1d0b85[_0x20b856(0x5ef)])_0x1d0b85['battler']()[_0x20b856(0x2ee)]();VisuMZ[_0x20b856(0x2cc)][_0x20b856(0x42d)][_0x20b856(0x6b9)](this);},VisuMZ['BattleCore'][_0x53c255(0x584)]=Scene_Battle[_0x53c255(0x819)][_0x53c255(0x714)],Scene_Battle[_0x53c255(0x819)][_0x53c255(0x714)]=function(){const _0x4e394a=_0x53c255;if(VisuMZ[_0x4e394a(0x2cc)][_0x4e394a(0x4b3)][_0x4e394a(0x563)]['BattleLogRectJS'])return VisuMZ[_0x4e394a(0x2cc)]['Settings'][_0x4e394a(0x563)][_0x4e394a(0x28f)][_0x4e394a(0x6b9)](this);return VisuMZ[_0x4e394a(0x2cc)][_0x4e394a(0x584)]['call'](this);},VisuMZ[_0x53c255(0x2cc)]['Scene_Battle_createPartyCommandWindow']=Scene_Battle['prototype'][_0x53c255(0x753)],Scene_Battle[_0x53c255(0x819)]['createPartyCommandWindow']=function(){const _0x4dea93=_0x53c255;VisuMZ[_0x4dea93(0x2cc)][_0x4dea93(0x1f9)][_0x4dea93(0x6b9)](this),this[_0x4dea93(0x190)]();},Scene_Battle[_0x53c255(0x819)]['createPartyCommandWindowBattleCore']=function(){const _0x3558e6=_0x53c255,_0xf8c85=this['_partyCommandWindow'];_0xf8c85[_0x3558e6(0x65d)](_0x3558e6(0x484),this[_0x3558e6(0x237)][_0x3558e6(0x4c7)](this)),_0xf8c85['setHandler'](_0x3558e6(0x549),this[_0x3558e6(0x210)][_0x3558e6(0x4c7)](this));const _0x2cd943=this[_0x3558e6(0x139)]();switch(_0x2cd943){case'xp':case _0x3558e6(0x6bf):return this[_0x3558e6(0x64e)]['setBackgroundType'](0x1);break;}},Scene_Battle['prototype'][_0x53c255(0x237)]=function(){const _0x4d986c=_0x53c255;BattleManager[_0x4d986c(0x6ec)]=!![],$gameParty[_0x4d986c(0x60e)](),this['selectNextCommand'](),BattleManager[_0x4d986c(0x4ea)]()&&(BattleManager[_0x4d986c(0x297)]=![]);},Scene_Battle[_0x53c255(0x819)][_0x53c255(0x210)]=function(){const _0x25d2f8=_0x53c255;this[_0x25d2f8(0x40e)]()?(this['_callSceneOptions']=!![],this[_0x25d2f8(0x47a)]['push']('addText',VisuMZ[_0x25d2f8(0x2cc)][_0x25d2f8(0x4b3)]['PartyCmd'][_0x25d2f8(0x1a6)])):this[_0x25d2f8(0x66d)]();},Scene_Battle[_0x53c255(0x819)][_0x53c255(0x40e)]=function(){const _0x82c32c=_0x53c255;return BattleManager[_0x82c32c(0x503)]();},Scene_Battle['prototype'][_0x53c255(0x66d)]=function(){const _0x3b6c42=_0x53c255;this['_callSceneOptions']=![],this[_0x3b6c42(0x3fb)][_0x3b6c42(0x3ed)](),this['_windowLayer'][_0x3b6c42(0x3f2)]=![];if(BattleManager[_0x3b6c42(0x649)]())($dataSystem[_0x3b6c42(0xe8)]||$dataSystem[_0x3b6c42(0x335)])&&SceneManager[_0x3b6c42(0x246)]();else($gameMap['battleback1Name']()||$gameMap[_0x3b6c42(0x335)]())&&SceneManager[_0x3b6c42(0x246)]();SceneManager[_0x3b6c42(0x2bf)](Scene_Options),BattleManager[_0x3b6c42(0x4ea)]()&&(BattleManager[_0x3b6c42(0x5e5)]=BattleManager['actor']());},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x7a3)]=Scene_Battle[_0x53c255(0x819)][_0x53c255(0x679)],Scene_Battle[_0x53c255(0x819)][_0x53c255(0x679)]=function(){const _0x2d3e0e=_0x53c255;VisuMZ[_0x2d3e0e(0x2cc)][_0x2d3e0e(0x7a3)][_0x2d3e0e(0x6b9)](this);if(this[_0x2d3e0e(0x795)]&&!BattleManager[_0x2d3e0e(0x650)])this[_0x2d3e0e(0x66d)]();},Scene_Battle[_0x53c255(0x819)][_0x53c255(0x2e5)]=function(){const _0x3d84b7=_0x53c255,_0x4941ca=this[_0x3d84b7(0x778)]();this['_autoBattleWindow']=new Window_AutoBattleCancel(_0x4941ca),this['_autoBattleWindow'][_0x3d84b7(0xdf)](),this[_0x3d84b7(0x62f)](this[_0x3d84b7(0x60c)]);},Scene_Battle[_0x53c255(0x819)][_0x53c255(0x778)]=function(){const _0x15ed10=_0x53c255;return VisuMZ['BattleCore'][_0x15ed10(0x4b3)][_0x15ed10(0x216)]['AutoBattleRect']['call'](this);},Scene_Battle[_0x53c255(0x819)][_0x53c255(0x515)]=function(){const _0x132289=_0x53c255;return VisuMZ['BattleCore'][_0x132289(0x4b3)][_0x132289(0x3d7)][_0x132289(0x619)];},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x84d)]=Scene_Battle['prototype'][_0x53c255(0x4de)],Scene_Battle[_0x53c255(0x819)][_0x53c255(0x4de)]=function(){const _0x8b7740=_0x53c255;this['isPartyCommandWindowDisabled']()?this[_0x8b7740(0x27c)]():VisuMZ['BattleCore'][_0x8b7740(0x84d)][_0x8b7740(0x6b9)](this);},Scene_Battle[_0x53c255(0x819)][_0x53c255(0x27c)]=function(){const _0x4a488d=_0x53c255;if(BattleManager[_0x4a488d(0x105)]())this[_0x4a488d(0x4d1)]();else BattleManager[_0x4a488d(0x4ea)]()&&VisuMZ[_0x4a488d(0x2cc)]['Scene_Battle_startPartyCommandSelection'][_0x4a488d(0x6b9)](this);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x69b)]=Scene_Battle[_0x53c255(0x819)][_0x53c255(0x543)],Scene_Battle['prototype'][_0x53c255(0x543)]=function(){const _0x552c66=_0x53c255;BattleManager[_0x552c66(0x4ea)]()?this[_0x552c66(0x81a)]():VisuMZ[_0x552c66(0x2cc)][_0x552c66(0x69b)]['call'](this);},VisuMZ[_0x53c255(0x2cc)]['Scene_Battle_createActorCommandWindow']=Scene_Battle['prototype'][_0x53c255(0x4ff)],Scene_Battle[_0x53c255(0x819)][_0x53c255(0x4ff)]=function(){const _0x1c8876=_0x53c255;VisuMZ[_0x1c8876(0x2cc)][_0x1c8876(0x317)][_0x1c8876(0x6b9)](this),this[_0x1c8876(0x82e)]();},Scene_Battle[_0x53c255(0x819)]['createActorCommandWindowBattleCore']=function(){const _0xedfb92=_0x53c255,_0x3220d4=this['_actorCommandWindow'];_0x3220d4[_0xedfb92(0x65d)]('escape',this[_0xedfb92(0x581)][_0xedfb92(0x4c7)](this)),_0x3220d4[_0xedfb92(0x65d)](_0xedfb92(0x484),this[_0xedfb92(0x5ba)][_0xedfb92(0x4c7)](this)),_0x3220d4[_0xedfb92(0x65d)]('singleSkill',this[_0xedfb92(0x30f)][_0xedfb92(0x4c7)](this)),BattleManager[_0xedfb92(0x4ea)]()&&(this[_0xedfb92(0x515)]()?delete _0x3220d4['_handlers'][_0xedfb92(0x1c5)]:_0x3220d4[_0xedfb92(0x65d)](_0xedfb92(0x1c5),this[_0xedfb92(0x536)]['bind'](this)));},Scene_Battle[_0x53c255(0x819)]['actorCommandEscape']=function(){const _0x2269c4=_0x53c255;this[_0x2269c4(0x847)]();},Scene_Battle['prototype'][_0x53c255(0x5ba)]=function(){const _0x3126b8=_0x53c255;BattleManager[_0x3126b8(0x7de)]()[_0x3126b8(0x4c3)](),BattleManager[_0x3126b8(0x85a)](),BattleManager[_0x3126b8(0x84f)](),this[_0x3126b8(0x2ef)]();},Scene_Battle[_0x53c255(0x819)][_0x53c255(0x30f)]=function(){const _0x2149a4=_0x53c255,_0x2dd69c=BattleManager[_0x2149a4(0x48d)]();_0x2dd69c[_0x2149a4(0x19b)](this['_actorCommandWindow'][_0x2149a4(0x723)]()),this[_0x2149a4(0x80c)]();},Scene_Battle[_0x53c255(0x819)][_0x53c255(0x536)]=function(){const _0x254c3e=_0x53c255;this[_0x254c3e(0x64e)][_0x254c3e(0x157)](),this[_0x254c3e(0x176)][_0x254c3e(0x250)]();},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x666)]=Scene_Battle[_0x53c255(0x819)][_0x53c255(0x574)],Scene_Battle[_0x53c255(0x819)]['createHelpWindow']=function(){const _0x7026ed=_0x53c255;VisuMZ[_0x7026ed(0x2cc)][_0x7026ed(0x666)][_0x7026ed(0x6b9)](this),this[_0x7026ed(0x223)]();},Scene_Battle[_0x53c255(0x819)][_0x53c255(0x223)]=function(){const _0xb8213e=_0x53c255;this[_0xb8213e(0x176)][_0xb8213e(0x3d9)](this[_0xb8213e(0x5eb)]),this[_0xb8213e(0x64e)]['setHelpWindow'](this[_0xb8213e(0x5eb)]);},Scene_Battle[_0x53c255(0x819)][_0x53c255(0x139)]=function(){const _0xb8a15=_0x53c255;if($gameTemp[_0xb8a15(0x519)]!==undefined)return $gameTemp[_0xb8a15(0x519)];if(this[_0xb8a15(0x647)])return this['_battleLayoutStyle'];return this[_0xb8a15(0x647)]=VisuMZ['BattleCore'][_0xb8a15(0x4b3)][_0xb8a15(0xf1)][_0xb8a15(0x683)][_0xb8a15(0x534)]()['trim'](),this[_0xb8a15(0x647)];},VisuMZ['BattleCore'][_0x53c255(0x69e)]=Scene_Battle['prototype'][_0x53c255(0x292)],Scene_Battle['prototype'][_0x53c255(0x292)]=function(){const _0x590005=_0x53c255,_0x798844=this[_0x590005(0x139)]();switch(_0x798844){case _0x590005(0x4a9):return this[_0x590005(0x5aa)](Math['max'](0x1,$gameParty[_0x590005(0x35c)]()),!![]);break;default:return VisuMZ[_0x590005(0x2cc)][_0x590005(0x69e)][_0x590005(0x6b9)](this);break;}},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x131)]=Scene_Battle[_0x53c255(0x819)][_0x53c255(0x7ea)],Scene_Battle['prototype'][_0x53c255(0x7ea)]=function(){const _0x319876=_0x53c255,_0x298170=this[_0x319876(0x139)]();switch(_0x298170){case _0x319876(0x7a0):return this[_0x319876(0x339)]();break;case _0x319876(0x1cf):case _0x319876(0x4a9):case'xp':case _0x319876(0x6bf):default:return VisuMZ[_0x319876(0x2cc)][_0x319876(0x131)][_0x319876(0x6b9)](this);break;}},Scene_Battle[_0x53c255(0x819)]['statusWindowRect']=function(){const _0x1688ba=_0x53c255,_0x4a9e9f=this['battleLayoutStyle']();switch(_0x4a9e9f){case'xp':case _0x1688ba(0x6bf):return this[_0x1688ba(0x711)]();break;case'border':return this[_0x1688ba(0x756)]();break;case _0x1688ba(0x1cf):case'list':default:return this[_0x1688ba(0x7b4)]();break;}},VisuMZ['BattleCore'][_0x53c255(0x6a0)]=Scene_Battle[_0x53c255(0x819)][_0x53c255(0x2a3)],Scene_Battle[_0x53c255(0x819)][_0x53c255(0x2a3)]=function(){const _0x152218=_0x53c255,_0x1b1868=this[_0x152218(0x139)]();switch(_0x1b1868){case'xp':case _0x152218(0x6bf):return this[_0x152218(0x5ea)]();break;case'border':return this[_0x152218(0x136)]();case _0x152218(0x1cf):case'list':default:return this['partyCommandWindowRectDefaultStyle']();break;}},Scene_Battle[_0x53c255(0x819)][_0x53c255(0x6ac)]=function(){const _0x4b3989=_0x53c255,_0x200be7=VisuMZ['BattleCore'][_0x4b3989(0x4b3)][_0x4b3989(0xf1)],_0x966a68=_0x200be7[_0x4b3989(0x372)]||0xc0,_0x135253=this[_0x4b3989(0x292)](),_0x1035d6=this[_0x4b3989(0x64f)]()?Graphics['boxWidth']-_0x966a68:0x0,_0x549c50=Graphics[_0x4b3989(0x75a)]-_0x135253;return new Rectangle(_0x1035d6,_0x549c50,_0x966a68,_0x135253);},Scene_Battle['prototype'][_0x53c255(0xff)]=function(){const _0x2c1714=_0x53c255;return this[_0x2c1714(0x2a3)]();},VisuMZ['BattleCore'][_0x53c255(0x17e)]=Scene_Battle[_0x53c255(0x819)]['updateStatusWindowPosition'],Scene_Battle['prototype']['updateStatusWindowPosition']=function(){const _0x3f273b=_0x53c255,_0x3c8f5b=this[_0x3f273b(0x139)]();switch(_0x3c8f5b){case'xp':case _0x3f273b(0x6bf):case _0x3f273b(0x7a0):break;case _0x3f273b(0x1cf):case _0x3f273b(0x4a9):default:VisuMZ[_0x3f273b(0x2cc)]['Scene_Battle_updateStatusWindowPosition'][_0x3f273b(0x6b9)](this);break;}},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x6f0)]=Scene_Battle[_0x53c255(0x819)][_0x53c255(0x199)],Scene_Battle[_0x53c255(0x819)][_0x53c255(0x199)]=function(){const _0x432ae0=_0x53c255;VisuMZ['BattleCore'][_0x432ae0(0x6f0)]['call'](this),this[_0x432ae0(0x300)]();},VisuMZ[_0x53c255(0x2cc)]['Scene_Battle_startEnemySelection']=Scene_Battle[_0x53c255(0x819)]['startEnemySelection'],Scene_Battle[_0x53c255(0x819)][_0x53c255(0x10b)]=function(){const _0x499e50=_0x53c255;VisuMZ[_0x499e50(0x2cc)]['Scene_Battle_startEnemySelection'][_0x499e50(0x6b9)](this),this[_0x499e50(0x6d6)][_0x499e50(0x3eb)](),this[_0x499e50(0x300)]();},Scene_Battle[_0x53c255(0x819)][_0x53c255(0x300)]=function(){const _0x2644a9=_0x53c255,_0x1939d6=this[_0x2644a9(0x139)]();['xp',_0x2644a9(0x6bf),_0x2644a9(0x7a0)][_0x2644a9(0x6a6)](_0x1939d6)&&this['_actorCommandWindow'][_0x2644a9(0x250)](),(_0x1939d6===_0x2644a9(0x7a0)||this[_0x2644a9(0x85c)]())&&(this['_skillWindow'][_0x2644a9(0x250)](),this[_0x2644a9(0x24a)]['close']());},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x5bd)]=Scene_Battle['prototype'][_0x53c255(0x7ec)],Scene_Battle['prototype'][_0x53c255(0x7ec)]=function(){const _0x121944=_0x53c255;VisuMZ['BattleCore']['Scene_Battle_onActorOk']['call'](this),this[_0x121944(0x42e)]();},Scene_Battle[_0x53c255(0x819)][_0x53c255(0x46b)]=function(){const _0x1ef3b5=_0x53c255;return[_0x1ef3b5(0x48f),'guard',_0x1ef3b5(0x6cb)][_0x1ef3b5(0x6a6)](this[_0x1ef3b5(0x176)][_0x1ef3b5(0x232)]());},VisuMZ[_0x53c255(0x2cc)]['Scene_Battle_onActorCancel']=Scene_Battle[_0x53c255(0x819)][_0x53c255(0x78a)],Scene_Battle[_0x53c255(0x819)][_0x53c255(0x78a)]=function(){const _0x1caea4=_0x53c255;this[_0x1caea4(0x46b)]()?(this[_0x1caea4(0x42f)][_0x1caea4(0x359)](),this[_0x1caea4(0x4b8)][_0x1caea4(0xdf)](),this[_0x1caea4(0x176)][_0x1caea4(0x5cd)]()):VisuMZ[_0x1caea4(0x2cc)]['Scene_Battle_onActorCancel'][_0x1caea4(0x6b9)](this),this[_0x1caea4(0x855)]();},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x379)]=Scene_Battle[_0x53c255(0x819)][_0x53c255(0x766)],Scene_Battle[_0x53c255(0x819)][_0x53c255(0x766)]=function(){const _0x51229b=_0x53c255;VisuMZ[_0x51229b(0x2cc)][_0x51229b(0x379)][_0x51229b(0x6b9)](this),this['okTargetSelectionVisibility']();},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x36e)]=Scene_Battle[_0x53c255(0x819)][_0x53c255(0x598)],Scene_Battle[_0x53c255(0x819)][_0x53c255(0x598)]=function(){const _0x9d4743=_0x53c255;this['isNonSubmenuCancel']()?(this[_0x9d4743(0x42f)][_0x9d4743(0x359)](),this[_0x9d4743(0x6d6)][_0x9d4743(0xdf)](),this[_0x9d4743(0x176)][_0x9d4743(0x5cd)]()):VisuMZ[_0x9d4743(0x2cc)][_0x9d4743(0x36e)][_0x9d4743(0x6b9)](this),this[_0x9d4743(0x855)]();},Scene_Battle[_0x53c255(0x819)][_0x53c255(0x42e)]=function(){const _0x247069=_0x53c255,_0xec1b70=this[_0x247069(0x139)]();(_0xec1b70==='border'||this[_0x247069(0x85c)]())&&(this['_skillWindow'][_0x247069(0x678)](),this[_0x247069(0x7be)][_0x247069(0x27d)]&&this[_0x247069(0x7be)][_0x247069(0x359)](),this['_itemWindow'][_0x247069(0x678)](),this['_itemWindow'][_0x247069(0x27d)]&&this[_0x247069(0x24a)][_0x247069(0x359)]());},Scene_Battle[_0x53c255(0x819)][_0x53c255(0x855)]=function(){const _0x543db5=_0x53c255,_0x291183=this[_0x543db5(0x139)]();['xp',_0x543db5(0x6bf),_0x543db5(0x7a0)][_0x543db5(0x6a6)](_0x291183)&&this['_actorCommandWindow'][_0x543db5(0x678)](),this['okTargetSelectionVisibility']();},Scene_Battle[_0x53c255(0x819)][_0x53c255(0x7b4)]=function(){const _0xcc37ff=_0x53c255,_0x155d49=VisuMZ[_0xcc37ff(0x2cc)]['Settings'][_0xcc37ff(0xf1)],_0x3ff584=Window_BattleStatus[_0xcc37ff(0x819)][_0xcc37ff(0x637)](),_0x1464a2=Graphics['boxWidth']-(_0x155d49['CommandWidth']||0xc0),_0xaf26e4=this[_0xcc37ff(0x292)]()+_0x3ff584,_0x526ce3=this['isRightInputMode']()?0x0:Graphics[_0xcc37ff(0x32c)]-_0x1464a2,_0xfa88b8=Graphics['boxHeight']-_0xaf26e4+_0x3ff584;return new Rectangle(_0x526ce3,_0xfa88b8,_0x1464a2,_0xaf26e4);},Scene_Battle[_0x53c255(0x819)][_0x53c255(0x711)]=function(){const _0x3a503c=_0x53c255,_0x20490f=Window_BattleStatus[_0x3a503c(0x819)][_0x3a503c(0x637)](),_0x938b9d=Graphics[_0x3a503c(0x32c)],_0x32b101=this[_0x3a503c(0x292)]()+_0x20490f,_0x59aead=0x0,_0x594de6=Graphics[_0x3a503c(0x75a)]-_0x32b101+_0x20490f;return new Rectangle(_0x59aead,_0x594de6,_0x938b9d,_0x32b101);},Scene_Battle['prototype'][_0x53c255(0x5ea)]=function(){const _0x2ebeea=_0x53c255,_0x59ab3f=Graphics[_0x2ebeea(0x32c)]/0x2,_0x114ee2=this[_0x2ebeea(0x5aa)](VisuMZ['BattleCore'][_0x2ebeea(0x4b3)][_0x2ebeea(0xf1)][_0x2ebeea(0x1e4)],!![]),_0x50288e=Math['round']((Graphics['boxWidth']-_0x59ab3f)/0x2),_0x4a7ee1=Graphics['boxHeight']-_0x114ee2-this[_0x2ebeea(0x711)]()[_0x2ebeea(0x6ab)];return new Rectangle(_0x50288e,_0x4a7ee1,_0x59ab3f,_0x114ee2);},Scene_Battle[_0x53c255(0x819)]['helpWindowRectBorderStyle']=function(){const _0x2698ba=_0x53c255,_0x2698de=Graphics['width'],_0x25ac65=Math[_0x2698ba(0x6c4)]((Graphics['boxWidth']-_0x2698de)/0x2),_0x50b505=this[_0x2698ba(0x629)](),_0xf9d057=(Graphics[_0x2698ba(0x6ab)]-Graphics[_0x2698ba(0x75a)])/-0x2;return new Rectangle(_0x25ac65,_0xf9d057,_0x2698de,_0x50b505);},Scene_Battle['prototype']['statusWindowRectBorderStyle']=function(){const _0x262436=_0x53c255,_0x350022=Graphics[_0x262436(0x261)],_0x1ad56d=Math[_0x262436(0x6c4)]((Graphics[_0x262436(0x32c)]-_0x350022)/0x2),_0x587709=this['calcWindowHeight'](0x4,!![]),_0x5a51fa=Graphics[_0x262436(0x75a)]-_0x587709+(Graphics[_0x262436(0x6ab)]-Graphics[_0x262436(0x75a)])/0x2;return new Rectangle(_0x1ad56d,_0x5a51fa,_0x350022,_0x587709);},Scene_Battle['prototype'][_0x53c255(0x136)]=function(){const _0x475958=_0x53c255,_0x4e5837=Math[_0x475958(0x7f5)](Graphics[_0x475958(0x261)]/0x3),_0x29f507=this['isRightInputMode']()?(Graphics[_0x475958(0x261)]+Graphics[_0x475958(0x32c)])/0x2-_0x4e5837:(Graphics[_0x475958(0x261)]-Graphics[_0x475958(0x32c)])/-0x2,_0x3fa948=this[_0x475958(0x339)](),_0x349c7a=_0x3fa948['y']+_0x3fa948[_0x475958(0x6ab)],_0x3ef362=this[_0x475958(0x756)](),_0x424982=_0x3ef362['y']-_0x349c7a;return new Rectangle(_0x29f507,_0x349c7a,_0x4e5837,_0x424982);},Scene_Battle[_0x53c255(0x819)][_0x53c255(0x692)]=function(){const _0xfae98=_0x53c255,_0x3de8cf=Math[_0xfae98(0x305)](Graphics[_0xfae98(0x261)]/0x3),_0x47212e=Math[_0xfae98(0x6c4)]((Graphics[_0xfae98(0x32c)]-_0x3de8cf)/0x2),_0x35a136=this[_0xfae98(0x136)](),_0x31cf7d=_0x35a136['y'],_0x2c5372=_0x35a136[_0xfae98(0x6ab)];return new Rectangle(_0x47212e,_0x31cf7d,_0x3de8cf,_0x2c5372);},Scene_Battle['prototype'][_0x53c255(0x46e)]=function(){const _0x1bf0f5=_0x53c255;this[_0x1bf0f5(0x111)]['y']=this[_0x1bf0f5(0x5eb)]['y']+this[_0x1bf0f5(0x5eb)][_0x1bf0f5(0x6ab)],this[_0x1bf0f5(0x64f)]()?this[_0x1bf0f5(0x139)]()===_0x1bf0f5(0x7a0)?this['_cancelButton']['x']=0x8:this[_0x1bf0f5(0x111)]['x']=-this[_0x1bf0f5(0x111)][_0x1bf0f5(0x261)]-0x4:this[_0x1bf0f5(0x111)]['x']=Graphics[_0x1bf0f5(0x261)]-(Graphics[_0x1bf0f5(0x261)]-Graphics[_0x1bf0f5(0x32c)])/0x2-this[_0x1bf0f5(0x111)][_0x1bf0f5(0x261)]-0x4;},VisuMZ['BattleCore']['Scene_Battle_skillWindowRect']=Scene_Battle[_0x53c255(0x819)]['skillWindowRect'],Scene_Battle[_0x53c255(0x819)][_0x53c255(0x5fc)]=function(){const _0x5e2d8b=_0x53c255;if(this[_0x5e2d8b(0x139)]()===_0x5e2d8b(0x7a0))return this['skillItemWindowRectBorderStyle']();else return this[_0x5e2d8b(0x85c)]()?this['skillItemWindowRectMiddle']():VisuMZ['BattleCore'][_0x5e2d8b(0x80d)][_0x5e2d8b(0x6b9)](this);},VisuMZ['BattleCore'][_0x53c255(0x2a5)]=Scene_Battle[_0x53c255(0x819)][_0x53c255(0x4ba)],Scene_Battle[_0x53c255(0x819)][_0x53c255(0x4ba)]=function(){const _0x1f5f17=_0x53c255;if(this['battleLayoutStyle']()===_0x1f5f17(0x7a0))return this[_0x1f5f17(0x692)]();else return this[_0x1f5f17(0x85c)]()?this['skillItemWindowRectMiddle']():VisuMZ[_0x1f5f17(0x2cc)]['Scene_Battle_itemWindowRect'][_0x1f5f17(0x6b9)](this);},Scene_Battle[_0x53c255(0x819)][_0x53c255(0x85c)]=function(){const _0x59c9b2=_0x53c255;return VisuMZ[_0x59c9b2(0x2cc)]['Settings'][_0x59c9b2(0xf1)][_0x59c9b2(0xf6)];},Scene_Battle[_0x53c255(0x819)][_0x53c255(0x3d1)]=function(){const _0x513ce5=_0x53c255,_0x55bbef=Sprite_Button[_0x513ce5(0x819)][_0x513ce5(0x7f6)]()*0x2+0x4;let _0x4c4b9f=Graphics[_0x513ce5(0x32c)]-_0x55bbef;Imported['VisuMZ_0_CoreEngine']&&SceneManager['isSideButtonLayout']()&&(_0x4c4b9f+=_0x55bbef);const _0x4a040c=this[_0x513ce5(0x22a)](),_0x5ad02c=Graphics[_0x513ce5(0x75a)]-_0x4a040c-this[_0x513ce5(0x6bc)]()[_0x513ce5(0x6ab)]+Window_BattleStatus[_0x513ce5(0x819)][_0x513ce5(0x637)](),_0x327fd0=0x0;return new Rectangle(_0x327fd0,_0x4a040c,_0x4c4b9f,_0x5ad02c);},Scene_Battle[_0x53c255(0x819)][_0x53c255(0x62e)]=function(){const _0x4ac6e6=_0x53c255;this['_enemyNameContainer']=new Sprite(),this[_0x4ac6e6(0x6ea)]['x']=this[_0x4ac6e6(0x3a3)]['x'],this['_enemyNameContainer']['y']=this[_0x4ac6e6(0x3a3)]['y'];const _0x25114b=this['children'][_0x4ac6e6(0x1d0)](this[_0x4ac6e6(0x3a3)]);this['addChildAt'](this[_0x4ac6e6(0x6ea)],_0x25114b);for(let _0x41d26f=0x0;_0x41d26f<0x8;_0x41d26f++){const _0x44442b=new Window_EnemyName(_0x41d26f);this[_0x4ac6e6(0x6ea)][_0x4ac6e6(0x62f)](_0x44442b);}},Sprite_Battler[_0x53c255(0x6fb)]=VisuMZ[_0x53c255(0x2cc)]['Settings']['Actor'][_0x53c255(0x54f)],VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x7e7)]=Sprite_Battler[_0x53c255(0x819)]['initMembers'],Sprite_Battler['prototype'][_0x53c255(0x1ff)]=function(){const _0x2f54e3=_0x53c255;VisuMZ['BattleCore'][_0x2f54e3(0x7e7)][_0x2f54e3(0x6b9)](this),this[_0x2f54e3(0x275)]();if(this[_0x2f54e3(0x70f)]===Sprite_Enemy)this['createShadowSprite']();this['createDistortionSprite']();},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x275)]=function(){const _0x59ca93=_0x53c255;this[_0x59ca93(0x396)]=0x0,this[_0x59ca93(0x772)]=0x0,this[_0x59ca93(0x4a3)]=0x0,this[_0x59ca93(0x4ef)]=0x0,this[_0x59ca93(0x16e)]=0x0,this[_0x59ca93(0x7b7)]=0x0,this[_0x59ca93(0x6f2)]=_0x59ca93(0x611),this['_jumpHeight']=0x0,this[_0x59ca93(0x420)]=0x0,this['_jumpDuration']=0x0,this[_0x59ca93(0x3dc)]=0x0,this['_targetOpacity']=0xff,this[_0x59ca93(0x6d3)]=0x0,this[_0x59ca93(0x47c)]=0x0,this[_0x59ca93(0x827)]=_0x59ca93(0x611),this[_0x59ca93(0x726)]=0x0,this[_0x59ca93(0x2d4)]=0x0,this['_angleDuration']=0x0,this[_0x59ca93(0x7b5)]=0x0,this['_angleEasing']=_0x59ca93(0x611),this[_0x59ca93(0x731)]=!![],this['_skewX']=0x0,this[_0x59ca93(0x490)]=0x0,this[_0x59ca93(0x85b)]=0x0,this[_0x59ca93(0x407)]=0x0,this[_0x59ca93(0x544)]=0x0,this[_0x59ca93(0x7f1)]=0x0,this['_skewEasing']=_0x59ca93(0x611),this[_0x59ca93(0x83b)]=0x1,this[_0x59ca93(0x7d9)]=0x1,this['_targetGrowX']=0x1,this[_0x59ca93(0x52b)]=0x1,this[_0x59ca93(0xec)]=0x0,this[_0x59ca93(0x181)]=0x0,this['_growEasing']=_0x59ca93(0x611),this[_0x59ca93(0x3a4)]=0x1;},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x296)]=function(){const _0x390175=_0x53c255;this[_0x390175(0x76f)]=new Sprite(),this[_0x390175(0x76f)]['bitmap']=ImageManager[_0x390175(0x518)](_0x390175(0x75e)),this[_0x390175(0x76f)][_0x390175(0x657)][_0x390175(0x7b6)]=VisuMZ[_0x390175(0x2cc)][_0x390175(0x4b3)][_0x390175(0x38f)][_0x390175(0x38d)],this['_shadowSprite']['anchor']['x']=0.5,this[_0x390175(0x76f)]['anchor']['y']=0.5,this['_shadowSprite']['y']=-0x2,this[_0x390175(0x76f)][_0x390175(0x3f2)]=![],this[_0x390175(0x62f)](this[_0x390175(0x76f)]);},Sprite_Battler[_0x53c255(0x819)]['createDistortionSprite']=function(){const _0x5208e3=_0x53c255;this[_0x5208e3(0x4d2)]=new Sprite(),this[_0x5208e3(0x4d2)][_0x5208e3(0x770)]['x']=0.5,this['_distortionSprite'][_0x5208e3(0x770)]['y']=0.5,this[_0x5208e3(0x62f)](this[_0x5208e3(0x4d2)]);},Sprite_Battler[_0x53c255(0x819)]['attachSpritesToDistortionSprite']=function(){const _0x2d5d4c=_0x53c255;if(!this[_0x2d5d4c(0x4d2)])return;if(this[_0x2d5d4c(0x76f)]){const _0x39294a=this[_0x2d5d4c(0x1b8)](this['_distortionSprite']);this[_0x2d5d4c(0x3f4)](this['_shadowSprite'],_0x39294a),this[_0x2d5d4c(0x764)]();}this[_0x2d5d4c(0x4bb)]&&this[_0x2d5d4c(0x4d2)][_0x2d5d4c(0x62f)](this[_0x2d5d4c(0x4bb)]),this[_0x2d5d4c(0x748)]&&this[_0x2d5d4c(0x4d2)][_0x2d5d4c(0x62f)](this[_0x2d5d4c(0x748)]),this[_0x2d5d4c(0x79b)]&&this[_0x2d5d4c(0x4d2)][_0x2d5d4c(0x62f)](this[_0x2d5d4c(0x79b)]),this[_0x2d5d4c(0x850)]&&this[_0x2d5d4c(0x4d2)][_0x2d5d4c(0x62f)](this[_0x2d5d4c(0x850)]);},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x764)]=function(){const _0x3f171f=_0x53c255;if(!this[_0x3f171f(0x76f)])return;if(this[_0x3f171f(0x1d5)]&&this[_0x3f171f(0x1d5)][_0x3f171f(0x312)]()){const _0x4547c0=this[_0x3f171f(0x76f)]['bitmap'];this[_0x3f171f(0x76f)][_0x3f171f(0x411)](0x0,0x0,_0x4547c0[_0x3f171f(0x261)],_0x4547c0[_0x3f171f(0x6ab)]);}else this[_0x3f171f(0x76f)][_0x3f171f(0x411)](0x0,0x0,0x0,0x0);},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x33f)]=function(){const _0x4d8049=_0x53c255;return SceneManager[_0x4d8049(0x45f)]()?SceneManager[_0x4d8049(0x12f)][_0x4d8049(0x3fb)][_0x4d8049(0x5dd)]:this[_0x4d8049(0x7ce)];},Sprite_Battler[_0x53c255(0x819)]['setupTextPopup']=function(_0x573730,_0x381759){const _0x2d3c0c=_0x53c255;if(!this[_0x2d3c0c(0x1d5)][_0x2d3c0c(0x670)]())return;const _0x5b94d3=VisuMZ[_0x2d3c0c(0x2cc)][_0x2d3c0c(0x4b3)][_0x2d3c0c(0x811)],_0x5e9c5e=new Sprite_Damage();_0x5e9c5e[_0x2d3c0c(0x2cb)]=_0x5b94d3[_0x2d3c0c(0x844)],this[_0x2d3c0c(0x19e)](_0x5e9c5e),_0x5e9c5e[_0x2d3c0c(0x6e0)](_0x573730,_0x381759),this['addDamageSprite'](_0x5e9c5e);},Sprite_Battler['prototype'][_0x53c255(0x5c2)]=function(_0x236962,_0x3b2396,_0x4f65a8){const _0x495097=_0x53c255;if(!this[_0x495097(0x1d5)][_0x495097(0x670)]())return;const _0x5b104c=VisuMZ['BattleCore'][_0x495097(0x4b3)][_0x495097(0x811)],_0x57fd8a=new Sprite_Damage();_0x57fd8a[_0x495097(0x2cb)]=_0x5b104c[_0x495097(0x844)],this['sortDamageSprites'](_0x57fd8a),_0x57fd8a['setupIconTextPopup'](_0x236962,_0x3b2396,_0x4f65a8),this['addDamageSprite'](_0x57fd8a);},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x508)]=function(){const _0x1a9c76=_0x53c255;if(!this['_battler'][_0x1a9c76(0x3e0)]())return;while(this[_0x1a9c76(0x1d5)][_0x1a9c76(0x3e0)]()){this['_battler']['isSpriteVisible']()&&this[_0x1a9c76(0x859)]();}this[_0x1a9c76(0x1d5)][_0x1a9c76(0x2f6)](),this[_0x1a9c76(0x1d5)][_0x1a9c76(0x78e)]();},Sprite_Battler[_0x53c255(0x819)]['createDamageSprite']=function(){const _0x1bb5f0=_0x53c255,_0x1788b4=VisuMZ['BattleCore'][_0x1bb5f0(0x4b3)][_0x1bb5f0(0x811)],_0x3f66aa=new Sprite_Damage();_0x3f66aa[_0x1bb5f0(0x2cb)]=_0x1788b4[_0x1bb5f0(0x844)],this[_0x1bb5f0(0x19e)](_0x3f66aa),_0x3f66aa[_0x1bb5f0(0x157)](this['_battler']),_0x3f66aa[_0x1bb5f0(0x768)](this[_0x1bb5f0(0x1d5)]),this['addDamageSprite'](_0x3f66aa);},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x609)]=function(_0x39037c){const _0x21a9d0=_0x53c255;this[_0x21a9d0(0x528)][_0x21a9d0(0x2bf)](_0x39037c);if(this['isShownOnBattlePortrait']())SceneManager[_0x21a9d0(0x12f)][_0x21a9d0(0x42f)][_0x21a9d0(0x609)](_0x39037c,this[_0x21a9d0(0x1d5)]);else{this['damageContainer']()[_0x21a9d0(0x62f)](_0x39037c);if(SceneManager[_0x21a9d0(0x32a)]())_0x39037c[_0x21a9d0(0x2dd)]['x']=-0x1;}},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x798)]=function(){const _0x41db00=_0x53c255;return!$gameSystem[_0x41db00(0xdb)]()&&this['_battler']&&this[_0x41db00(0x1d5)][_0x41db00(0x807)]();},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x19e)]=function(_0x2d215d){const _0x57aa09=_0x53c255,_0x5160fe=VisuMZ['BattleCore'][_0x57aa09(0x4b3)][_0x57aa09(0x811)],_0x58958e=SceneManager[_0x57aa09(0x32a)]()?-0x1:0x1;let _0x20c296=this['x'],_0x66c80=this['y'];const _0x58ca11=SceneManager['_scene'][_0x57aa09(0x42f)];if(_0x58ca11&&this[_0x57aa09(0x7ce)]===_0x58ca11){_0x20c296+=_0x58ca11['x']-this[_0x57aa09(0x167)]();const _0x234c87=_0x58ca11[_0x57aa09(0x585)]()*0x3/0x4;_0x66c80=_0x58ca11['y']+_0x234c87,_0x66c80=Math[_0x57aa09(0x413)](_0x66c80,_0x58ca11['y']+this['y']-this[_0x57aa09(0x6ab)]+_0x234c87);}_0x2d215d['x']=Math[_0x57aa09(0x6c4)](_0x20c296+this[_0x57aa09(0x167)]()*_0x58958e),_0x2d215d['y']=Math[_0x57aa09(0x6c4)](_0x66c80+this[_0x57aa09(0x147)]());if(_0x5160fe[_0x57aa09(0x464)])for(const _0x5732ff of this['_damages']){_0x5732ff['x']+=_0x5160fe[_0x57aa09(0x69c)]*_0x58958e,_0x5732ff['y']+=_0x5160fe[_0x57aa09(0x5ec)];}else{const _0x47f4e0=this[_0x57aa09(0x528)][this[_0x57aa09(0x528)]['length']-0x1];_0x47f4e0&&(_0x2d215d['x']=_0x47f4e0['x']+_0x5160fe[_0x57aa09(0x69c)]*_0x58958e,_0x2d215d['y']=_0x47f4e0['y']+_0x5160fe[_0x57aa09(0x5ec)]);}},VisuMZ['BattleCore']['Sprite_Battler_damageOffsetX']=Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x167)],Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x167)]=function(){const _0x977635=_0x53c255;let _0x20594b=VisuMZ[_0x977635(0x2cc)][_0x977635(0x4ac)][_0x977635(0x6b9)](this),_0x5f46ab=VisuMZ[_0x977635(0x2cc)]['Settings'][_0x977635(0x811)][_0x977635(0x126)]||0x0;return Math[_0x977635(0x6c4)](_0x20594b+_0x5f46ab);},VisuMZ[_0x53c255(0x2cc)]['Sprite_Battler_damageOffsetY']=Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x147)],Sprite_Battler[_0x53c255(0x819)]['damageOffsetY']=function(){const _0x10c0ac=_0x53c255;let _0x4360cd=VisuMZ[_0x10c0ac(0x2cc)][_0x10c0ac(0x634)][_0x10c0ac(0x6b9)](this);switch(VisuMZ['BattleCore']['Settings']['Damage'][_0x10c0ac(0x74b)]){case _0x10c0ac(0x3fd):_0x4360cd-=this['height']*this[_0x10c0ac(0x2dd)]['y'];break;case'center':_0x4360cd-=this[_0x10c0ac(0x6ab)]*this[_0x10c0ac(0x2dd)]['y']*0.5;break;}let _0x5a5933=VisuMZ[_0x10c0ac(0x2cc)][_0x10c0ac(0x4b3)]['Damage']['PopupOffsetY']||0x0;return Math[_0x10c0ac(0x6c4)](_0x4360cd+_0x5a5933);},Sprite_Actor[_0x53c255(0x819)][_0x53c255(0x167)]=function(){const _0x38428b=_0x53c255;return Sprite_Battler[_0x38428b(0x819)][_0x38428b(0x167)][_0x38428b(0x6b9)](this);},Sprite_Actor[_0x53c255(0x819)][_0x53c255(0x147)]=function(){const _0x1d6a7a=_0x53c255;return Sprite_Battler[_0x1d6a7a(0x819)][_0x1d6a7a(0x147)][_0x1d6a7a(0x6b9)](this);},Sprite_Battler[_0x53c255(0x819)]['destroyDamageSprite']=function(_0x274227){const _0x2fc253=_0x53c255;this[_0x2fc253(0x798)]()?SceneManager[_0x2fc253(0x12f)][_0x2fc253(0x42f)][_0x2fc253(0x4b2)](_0x274227):(this[_0x2fc253(0x33f)]()[_0x2fc253(0x4d6)](_0x274227),this[_0x2fc253(0x528)][_0x2fc253(0x725)](_0x274227),_0x274227[_0x2fc253(0x3ea)]());},VisuMZ[_0x53c255(0x2cc)]['Sprite_Battler_setHome']=Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x119)],Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x119)]=function(_0x22ae96,_0x4f15a5){const _0x43bf11=_0x53c255,_0x53af05=VisuMZ[_0x43bf11(0x2cc)][_0x43bf11(0x4b3)];if(this[_0x43bf11(0x70f)]===Sprite_Actor)_0x22ae96+=_0x53af05['Actor'][_0x43bf11(0x77b)]||0x0,_0x4f15a5+=_0x53af05[_0x43bf11(0x38f)][_0x43bf11(0x6cc)]||0x0;else this['constructor']===Sprite_Enemy&&(_0x22ae96+=_0x53af05[_0x43bf11(0x1c7)][_0x43bf11(0x77b)]||0x0,_0x4f15a5+=_0x53af05[_0x43bf11(0x1c7)][_0x43bf11(0x6cc)]||0x0);VisuMZ[_0x43bf11(0x2cc)][_0x43bf11(0x37e)]['call'](this,_0x22ae96,_0x4f15a5);},VisuMZ['BattleCore'][_0x53c255(0x7f9)]=Sprite_Battler[_0x53c255(0x819)]['update'],Sprite_Battler['prototype'][_0x53c255(0x3ed)]=function(){const _0x175181=_0x53c255;VisuMZ[_0x175181(0x2cc)][_0x175181(0x7f9)][_0x175181(0x6b9)](this),!this[_0x175181(0x1d5)]&&this[_0x175181(0x117)]&&(this['_hpGaugeSprite'][_0x175181(0x3f2)]=![]);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x415)]=Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x44e)],Sprite_Battler['prototype'][_0x53c255(0x44e)]=function(){const _0x27379b=_0x53c255;this['updateScale'](),this[_0x27379b(0x288)](),this[_0x27379b(0x82b)](),this['updateFlip'](),this[_0x27379b(0x5e0)](),VisuMZ[_0x27379b(0x2cc)][_0x27379b(0x415)][_0x27379b(0x6b9)](this);if(this[_0x27379b(0x70f)]===Sprite_Enemy)this[_0x27379b(0x3ab)]();},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x417)]=Sprite_Battler['prototype'][_0x53c255(0x824)],Sprite_Battler['prototype']['updatePosition']=function(){const _0x27690a=_0x53c255;VisuMZ['BattleCore'][_0x27690a(0x417)][_0x27690a(0x6b9)](this),this[_0x27690a(0x78f)](),this['updateOpacity']();},Sprite_Battler[_0x53c255(0x819)]['updatePositionBattleCore']=function(){const _0x409512=_0x53c255;this[_0x409512(0x396)]=this['x'],this['_baseY']=this['y'],this[_0x409512(0xed)](),this['updateJump'](),this['x']+=this['extraPositionX'](),this['y']+=this[_0x409512(0x34a)](),this['x']=Math[_0x409512(0x6c4)](this['x']),this['y']=Math[_0x409512(0x6c4)](this['y']);},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x68b)]=function(){let _0x5e1103=0x0;return _0x5e1103;},Sprite_Battler['prototype'][_0x53c255(0x34a)]=function(){const _0x11b2a1=_0x53c255;let _0x8e280a=0x0;this[_0x11b2a1(0x1d5)]&&!this[_0x11b2a1(0x1d5)]['isBattlerGrounded']()&&(_0x8e280a-=this[_0x11b2a1(0x4a3)],_0x8e280a-=this[_0x11b2a1(0x33b)]);if(this[_0x11b2a1(0x4d2)]&&this[_0x11b2a1(0x70f)]!==Sprite_SvEnemy){const _0x281e99=this[_0x11b2a1(0x4d2)][_0x11b2a1(0x2dd)]['y'];_0x8e280a-=(_0x281e99-0x1)*this[_0x11b2a1(0x6ab)];}return _0x8e280a;},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x40b)]=function(){const _0xfd3544=_0x53c255,_0x1a0868=this[_0xfd3544(0x1d5)]&&this[_0xfd3544(0x1d5)]['isBattlerFlipped']();this[_0xfd3544(0x3a4)]=(_0x1a0868?-0x1:0x1)*Math[_0xfd3544(0x1f4)](this[_0xfd3544(0x2dd)]['x']);},Sprite_Battler[_0x53c255(0x819)]['startFloat']=function(_0x40b789,_0x1ae8c9,_0x2f8b8a){const _0x191131=_0x53c255;if(!this[_0x191131(0x4f5)]())return;if(this[_0x191131(0x4ef)]===_0x40b789)return;this['_targetFloatHeight']=_0x40b789,this[_0x191131(0x16e)]=_0x1ae8c9,this[_0x191131(0x7b7)]=_0x1ae8c9,this[_0x191131(0x6f2)]=_0x2f8b8a||_0x191131(0x611);if(_0x1ae8c9<=0x0)this['_floatHeight']=_0x40b789;},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0xed)]=function(){const _0x3f1425=_0x53c255;if(this[_0x3f1425(0x16e)]<=0x0)return;const _0x2fba07=this[_0x3f1425(0x16e)],_0x144f2f=this[_0x3f1425(0x7b7)],_0x18d49d=this[_0x3f1425(0x6f2)];Imported['VisuMZ_0_CoreEngine']?this[_0x3f1425(0x4a3)]=this[_0x3f1425(0x389)](this['_floatHeight'],this['_targetFloatHeight'],_0x2fba07,_0x144f2f,_0x18d49d):this[_0x3f1425(0x4a3)]=(this[_0x3f1425(0x4a3)]*(_0x2fba07-0x1)+this[_0x3f1425(0x4ef)])/_0x2fba07;this['_floatDuration']--;if(this[_0x3f1425(0x16e)]<=0x0)this[_0x3f1425(0x17a)]();},Sprite_Battler[_0x53c255(0x819)]['onFloatEnd']=function(){const _0x43a688=_0x53c255;this[_0x43a688(0x4a3)]=this[_0x43a688(0x4ef)];},Sprite_Battler[_0x53c255(0x819)]['isFloating']=function(){const _0x31dc94=_0x53c255;return this[_0x31dc94(0x16e)]>0x0;},Sprite_Battler[_0x53c255(0x819)]['startJump']=function(_0x20a878,_0xaf0673){const _0x38dc06=_0x53c255;if(!this['canMove']())return;if(_0xaf0673<=0x0)return;this[_0x38dc06(0x420)]=_0x20a878,this[_0x38dc06(0x46f)]=_0xaf0673,this['_jumpWholeDuration']=_0xaf0673;},Sprite_Battler[_0x53c255(0x819)]['updateJump']=function(){const _0x5ba66a=_0x53c255;if(this[_0x5ba66a(0x46f)]<=0x0)return;const _0xec52c0=this['_jumpWholeDuration']-this[_0x5ba66a(0x46f)],_0x344b7f=this['_jumpWholeDuration']/0x2,_0x105201=this[_0x5ba66a(0x420)],_0x35a0ea=-_0x105201/Math[_0x5ba66a(0x531)](_0x344b7f,0x2);this[_0x5ba66a(0x33b)]=_0x35a0ea*Math['pow'](_0xec52c0-_0x344b7f,0x2)+_0x105201,this[_0x5ba66a(0x46f)]--;if(this[_0x5ba66a(0x46f)]<=0x0)return this[_0x5ba66a(0x721)]();},Sprite_Battler['prototype'][_0x53c255(0x721)]=function(){this['_jumpHeight']=0x0;},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x729)]=function(){const _0x6574aa=_0x53c255;return this[_0x6574aa(0x46f)]>0x0;},Sprite_Battler[_0x53c255(0x819)]['startOpacity']=function(_0x283c84,_0x53b4a8,_0x476b89){const _0x3affdf=_0x53c255;if(this[_0x3affdf(0x26a)]===_0x283c84)return;this[_0x3affdf(0x26a)]=_0x283c84,this[_0x3affdf(0x6d3)]=_0x53b4a8,this['_opacityWholeDuration']=_0x53b4a8,this[_0x3affdf(0x827)]=_0x476b89||'Linear';if(_0x53b4a8<=0x0)this['opacity']=_0x283c84;},Sprite_Battler[_0x53c255(0x819)]['updateOpacity']=function(){const _0x8be07c=_0x53c255;if(this[_0x8be07c(0x6d3)]<=0x0)return;const _0x395c99=this[_0x8be07c(0x6d3)],_0x2cf402=this[_0x8be07c(0x47c)],_0x337a7a=this['_opacityEasing'];Imported[_0x8be07c(0x23a)]?this['opacity']=this[_0x8be07c(0x389)](this[_0x8be07c(0x398)],this[_0x8be07c(0x26a)],_0x395c99,_0x2cf402,_0x337a7a):this[_0x8be07c(0x398)]=(this[_0x8be07c(0x398)]*(_0x395c99-0x1)+this[_0x8be07c(0x26a)])/_0x395c99;this[_0x8be07c(0x6d3)]--;if(this[_0x8be07c(0x6d3)]<=0x0)this['onOpacityEnd']();},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x12e)]=function(){const _0x181943=_0x53c255;this[_0x181943(0x398)]=this[_0x181943(0x26a)];},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x5df)]=function(){const _0x299588=_0x53c255;return this[_0x299588(0x6d3)]>0x0;},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x3ab)]=function(){const _0x52dff1=_0x53c255;this['_shadowSprite'][_0x52dff1(0x3f2)]=this[_0x52dff1(0x1d5)][_0x52dff1(0x366)](),this['updateShadowPosition']();},Sprite_Battler[_0x53c255(0x819)]['updateShadowPosition']=function(){const _0x28dd5d=_0x53c255;if(!this['_shadowSprite'])return;this[_0x28dd5d(0x76f)]['y']=Math[_0x28dd5d(0x6c4)](-this[_0x28dd5d(0x34a)]()-0x2);},Sprite_Battler['prototype'][_0x53c255(0x54b)]=function(){const _0x33b6d9=_0x53c255;if(this[_0x33b6d9(0x70f)]===Sprite_SvEnemy)return;this[_0x33b6d9(0x320)](),this[_0x33b6d9(0x556)]();},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x556)]=function(){const _0x784e1f=_0x53c255,_0x1637ce=this[_0x784e1f(0x4d2)];_0x1637ce&&(_0x1637ce['scale']['x']=this['mainSpriteScaleX'](),_0x1637ce[_0x784e1f(0x2dd)]['y']=this['mainSpriteScaleY']());},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x1b4)]=function(){const _0x41e092=_0x53c255;let _0x5e3acd=0x1;return _0x5e3acd*=this['_flipScaleX'],_0x5e3acd*=this[_0x41e092(0x83b)],_0x5e3acd;},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x283)]=function(){return 0x1*this['_growY'];},Sprite_Battler[_0x53c255(0x819)]['mainSpriteWidth']=function(){const _0x5e13e4=_0x53c255;return this[_0x5e13e4(0x261)]*this['mainSpriteScaleX']();},Sprite_Battler['prototype'][_0x53c255(0x848)]=function(){const _0x173cbc=_0x53c255;return this[_0x173cbc(0x6ab)]*this['mainSpriteScaleY']();},Sprite_Battler['prototype'][_0x53c255(0x4da)]=function(_0x285816,_0x3e5767,_0x52b375,_0x4c50ec){const _0x3c7d8d=_0x53c255;if(!this[_0x3c7d8d(0x4f5)]())return;if(!this[_0x3c7d8d(0x4d2)])return;if(this[_0x3c7d8d(0x6ef)]===_0x285816&&this[_0x3c7d8d(0x52b)]===_0x3e5767)return;this[_0x3c7d8d(0x6ef)]=_0x285816,this[_0x3c7d8d(0x52b)]=_0x3e5767,this['_growDuration']=_0x52b375,this[_0x3c7d8d(0x181)]=_0x52b375,this[_0x3c7d8d(0x228)]=_0x4c50ec||_0x3c7d8d(0x611),_0x52b375<=0x0&&(this['_growX']=this[_0x3c7d8d(0x6ef)],this[_0x3c7d8d(0x7d9)]=this[_0x3c7d8d(0x52b)]);},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x320)]=function(){const _0x450839=_0x53c255;if(this[_0x450839(0xec)]<=0x0)return;if(!this['_distortionSprite'])return;const _0x33fa14=this[_0x450839(0xec)],_0x337361=this[_0x450839(0x181)],_0x5e473b=this[_0x450839(0x228)];Imported[_0x450839(0x23a)]?(this[_0x450839(0x83b)]=this[_0x450839(0x389)](this['_growX'],this[_0x450839(0x6ef)],_0x33fa14,_0x337361,_0x5e473b),this['_growY']=this[_0x450839(0x389)](this[_0x450839(0x7d9)],this[_0x450839(0x52b)],_0x33fa14,_0x337361,_0x5e473b)):(this[_0x450839(0x83b)]=(this[_0x450839(0x83b)]*(_0x33fa14-0x1)+this['_targetGrowX'])/_0x33fa14,this[_0x450839(0x7d9)]=(this[_0x450839(0x7d9)]*(_0x33fa14-0x1)+this[_0x450839(0x52b)])/_0x33fa14);this[_0x450839(0xec)]--;if(this[_0x450839(0xec)]<=0x0)this[_0x450839(0x30a)]();},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x30a)]=function(){const _0x3025e2=_0x53c255;this['_growX']=this[_0x3025e2(0x6ef)],this['_growY']=this[_0x3025e2(0x52b)];},Sprite_Battler['prototype'][_0x53c255(0x494)]=function(){const _0x538dbf=_0x53c255;return this[_0x538dbf(0xec)]>0x0;},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x231)]=function(_0x497c40,_0x272c6e,_0x4948d8,_0x39f429){const _0x431a25=_0x53c255;if(!this[_0x431a25(0x4f5)]())return;if(!this[_0x431a25(0x4d2)])return;if(this[_0x431a25(0x85b)]===_0x497c40&&this['_targetSkewY']===_0x272c6e)return;this['_targetSkewX']=_0x497c40,this[_0x431a25(0x407)]=_0x272c6e,this[_0x431a25(0x544)]=_0x4948d8,this['_skewWholeDuration']=_0x4948d8,this['_skewEasing']=_0x39f429||'Linear',_0x4948d8<=0x0&&(this['_distortionSprite'][_0x431a25(0x13d)]['x']=this[_0x431a25(0x85b)],this[_0x431a25(0x4d2)][_0x431a25(0x13d)]['y']=this['_targetSkewY']);},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x288)]=function(){const _0x2172b9=_0x53c255;if(this[_0x2172b9(0x544)]<=0x0)return;if(!this['_distortionSprite'])return;const _0x2b5f71=this[_0x2172b9(0x544)],_0x451e28=this['_skewWholeDuration'],_0x426ed7=this[_0x2172b9(0x65b)],_0x2eabc2=this[_0x2172b9(0x4d2)];Imported[_0x2172b9(0x23a)]?(_0x2eabc2['skew']['x']=this[_0x2172b9(0x389)](_0x2eabc2[_0x2172b9(0x13d)]['x'],this[_0x2172b9(0x85b)],_0x2b5f71,_0x451e28,_0x426ed7),_0x2eabc2[_0x2172b9(0x13d)]['y']=this[_0x2172b9(0x389)](_0x2eabc2[_0x2172b9(0x13d)]['y'],this[_0x2172b9(0x407)],_0x2b5f71,_0x451e28,_0x426ed7)):(_0x2eabc2[_0x2172b9(0x13d)]['x']=(_0x2eabc2['skew']['x']*(_0x2b5f71-0x1)+this[_0x2172b9(0x85b)])/_0x2b5f71,_0x2eabc2[_0x2172b9(0x13d)]['y']=(_0x2eabc2['skew']['y']*(_0x2b5f71-0x1)+this[_0x2172b9(0x407)])/_0x2b5f71);this[_0x2172b9(0x544)]--;if(this[_0x2172b9(0x544)]<=0x0)this[_0x2172b9(0x3a5)]();},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x3a5)]=function(){const _0x11ccc3=_0x53c255;this[_0x11ccc3(0x4d2)][_0x11ccc3(0x13d)]['x']=this[_0x11ccc3(0x85b)],this[_0x11ccc3(0x4d2)]['skew']['y']=this[_0x11ccc3(0x407)];},Sprite_Battler[_0x53c255(0x819)]['isSkewing']=function(){const _0xf7cd65=_0x53c255;return this[_0xf7cd65(0x544)]>0x0;},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0xf7)]=function(_0x1e9466,_0x2520f2,_0x5140c3,_0x48143b){const _0x50ee4c=_0x53c255;if(!this[_0x50ee4c(0x4f5)]())return;if(!this[_0x50ee4c(0x4d2)])return;if(this[_0x50ee4c(0x2d4)]===_0x1e9466)return;this[_0x50ee4c(0x2d4)]=_0x1e9466,this[_0x50ee4c(0x128)]=_0x2520f2,this[_0x50ee4c(0x7b5)]=_0x2520f2,this['_angleEasing']=_0x5140c3||_0x50ee4c(0x611),this[_0x50ee4c(0x731)]=_0x48143b,this['_angleRevertOnFinish']===undefined&&(this[_0x50ee4c(0x731)]=!![]),_0x2520f2<=0x0&&(this[_0x50ee4c(0x726)]=_0x1e9466,this[_0x50ee4c(0x731)]&&(this[_0x50ee4c(0x2d4)]=0x0,this[_0x50ee4c(0x726)]=0x0));},Sprite_Battler['prototype']['updateSpin']=function(){const _0x1cb725=_0x53c255;this[_0x1cb725(0x7ca)](),this[_0x1cb725(0x822)]();},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x7ca)]=function(){const _0x139dcb=_0x53c255;if(this['_angleDuration']<=0x0)return;const _0x38ec5d=this[_0x139dcb(0x128)],_0x73cefb=this[_0x139dcb(0x7b5)],_0x454128=this[_0x139dcb(0x3dd)];Imported[_0x139dcb(0x23a)]?this[_0x139dcb(0x726)]=this[_0x139dcb(0x389)](this[_0x139dcb(0x726)],this[_0x139dcb(0x2d4)],_0x38ec5d,_0x73cefb,_0x454128):this[_0x139dcb(0x726)]=(this['_currentAngle']*(_0x38ec5d-0x1)+this[_0x139dcb(0x2d4)])/_0x38ec5d;this[_0x139dcb(0x128)]--;if(this[_0x139dcb(0x128)]<=0x0)this['onAngleEnd']();},Sprite_Battler['prototype'][_0x53c255(0x5c1)]=function(){const _0x49da42=_0x53c255;this[_0x49da42(0x726)]=this['_targetAngle'],this[_0x49da42(0x731)]&&(this[_0x49da42(0x2d4)]=0x0,this[_0x49da42(0x726)]=0x0);},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x138)]=function(){return this['_angleDuration']>0x0;},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x822)]=function(){const _0x40525c=_0x53c255;if(!this[_0x40525c(0x4d2)])return;const _0x15bb73=this[_0x40525c(0x726)],_0x3f3fd1=this['scale']['x'],_0xab7796=this[_0x40525c(0x1d5)][_0x40525c(0x807)]()?-0x1:0x1;this['_distortionSprite']['angle']=_0x15bb73*_0x3f3fd1*_0xab7796;const _0x167455=this[_0x40525c(0x4d2)][_0x40525c(0x2dd)]['y'];this['_distortionSprite']['y']=this['height']*-0.5*(0x2-_0x167455);const _0x5133ab=[this[_0x40525c(0x79b)],this[_0x40525c(0x4bb)],this[_0x40525c(0x850)]];for(const _0x336fa2 of _0x5133ab){if(!_0x336fa2)continue;_0x336fa2['y']=this[_0x40525c(0x6ab)]*0.5;}this[_0x40525c(0x76f)]&&(this[_0x40525c(0x76f)][_0x40525c(0x2dd)]['x']=this[_0x40525c(0x4d2)][_0x40525c(0x2dd)]['x'],this[_0x40525c(0x76f)][_0x40525c(0x2dd)]['y']=this[_0x40525c(0x4d2)][_0x40525c(0x2dd)]['y']);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x63b)]=Sprite_Actor[_0x53c255(0x819)]['createStateSprite'],Sprite_Actor['prototype'][_0x53c255(0x65c)]=function(){const _0x37d5af=_0x53c255;VisuMZ[_0x37d5af(0x2cc)][_0x37d5af(0x63b)][_0x37d5af(0x6b9)](this),VisuMZ['BattleCore'][_0x37d5af(0x4b3)][_0x37d5af(0x18f)][_0x37d5af(0x6e7)]&&this[_0x37d5af(0x59b)]();},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x1eb)]=Sprite_Enemy[_0x53c255(0x819)]['createStateIconSprite'],Sprite_Enemy[_0x53c255(0x819)]['createStateIconSprite']=function(){const _0x276475=_0x53c255;VisuMZ['BattleCore'][_0x276475(0x4b3)]['HpGauge']['ShowEnemyGauge']&&this[_0x276475(0x59b)](),VisuMZ[_0x276475(0x2cc)][_0x276475(0x1eb)][_0x276475(0x6b9)](this);},Sprite_Battler['prototype'][_0x53c255(0x59b)]=function(){const _0x3504af=_0x53c255;if(!ConfigManager[_0x3504af(0x7dd)])return;if(this[_0x3504af(0x70f)]===Sprite_SvEnemy)return;const _0x2ef88b=VisuMZ[_0x3504af(0x2cc)][_0x3504af(0x4b3)][_0x3504af(0x18f)],_0x34e864=new Sprite_HpGauge();_0x34e864[_0x3504af(0x770)]['x']=_0x2ef88b['AnchorX'],_0x34e864['anchor']['y']=_0x2ef88b[_0x3504af(0x779)],_0x34e864[_0x3504af(0x2dd)]['x']=_0x34e864[_0x3504af(0x2dd)]['y']=_0x2ef88b[_0x3504af(0x38e)],this['_hpGaugeSprite']=_0x34e864,this['addChild'](this[_0x3504af(0x117)]);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x57b)]=Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x6de)],Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x6de)]=function(_0x4e7e88){const _0x19f53c=_0x53c255;VisuMZ['BattleCore'][_0x19f53c(0x57b)]['call'](this,_0x4e7e88),this[_0x19f53c(0x6da)](_0x4e7e88);},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x6da)]=function(_0x27196d){const _0x4b79c7=_0x53c255;if(!_0x27196d)return;if(!this[_0x4b79c7(0x117)])return;if(_0x27196d[_0x4b79c7(0x807)]()){}else{if(_0x27196d[_0x4b79c7(0x26c)]()){if(this[_0x4b79c7(0x70f)]===Sprite_SvEnemy&&!_0x27196d['hasSvBattler']())return;}}this['_hpGaugeSprite'][_0x4b79c7(0x157)](_0x27196d,'hp');},Sprite_Battler['prototype']['updateHpGaugePosition']=function(){const _0x42c635=_0x53c255;if(!this[_0x42c635(0x1d5)])return;if(!this[_0x42c635(0x117)])return;const _0xe90b4d=VisuMZ['BattleCore']['Settings'][_0x42c635(0x18f)],_0x51f073=this[_0x42c635(0x117)];_0x51f073[_0x42c635(0x3f2)]=this['isVisualHpGaugeDisplayed']();const _0x59fc04=_0xe90b4d[_0x42c635(0x77b)],_0x2734aa=_0xe90b4d[_0x42c635(0x6cc)];_0x51f073['x']=_0x59fc04,_0x51f073['x']+=this[_0x42c635(0x1d5)][_0x42c635(0x25c)](),_0x51f073['y']=-this[_0x42c635(0x6ab)]+_0x2734aa,_0x51f073['y']+=this[_0x42c635(0x1d5)][_0x42c635(0x235)]();},Sprite_Battler[_0x53c255(0x819)]['isVisualHpGaugeDisplayed']=function(){const _0x3f36f3=_0x53c255;if(!this[_0x3f36f3(0x1d5)])return![];if(this[_0x3f36f3(0x1d5)][_0x3f36f3(0x807)]())return!![];const _0x4ebabb=this[_0x3f36f3(0x1d5)][_0x3f36f3(0x221)]()[_0x3f36f3(0x236)];if(_0x4ebabb['match'](/<SHOW HP GAUGE>/i))return!![];if(_0x4ebabb[_0x3f36f3(0x473)](/<HIDE HP GAUGE>/i))return![];const _0x136690=VisuMZ['BattleCore'][_0x3f36f3(0x4b3)]['HpGauge'];if(_0x136690[_0x3f36f3(0x373)]){if(_0x136690['BTestBypass']&&BattleManager['isBattleTest']())return!![];if(this[_0x3f36f3(0x1d5)]['_visualHpGauge_JustDied'])return![];return this[_0x3f36f3(0x1d5)][_0x3f36f3(0x835)]();}return!![];},VisuMZ['BattleCore']['Sprite_Battler_isMoving']=Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x594)],Sprite_Battler[_0x53c255(0x819)]['isMoving']=function(){const _0x2c6f59=_0x53c255;if(!this['_battler'])return![];return VisuMZ[_0x2c6f59(0x2cc)][_0x2c6f59(0x645)][_0x2c6f59(0x6b9)](this);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x845)]=Sprite_Battler['prototype'][_0x53c255(0x64a)],Sprite_Battler['prototype']['startMove']=function(_0x51e88e,_0x2fd2d6,_0x7674c2){const _0xe64d41=_0x53c255;this[_0xe64d41(0x4f5)]()&&VisuMZ[_0xe64d41(0x2cc)][_0xe64d41(0x845)][_0xe64d41(0x6b9)](this,_0x51e88e,_0x2fd2d6,_0x7674c2);},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x4f5)]=function(){const _0x3edfa2=_0x53c255;if(this[_0x3edfa2(0x1d5)]&&this['_battler'][_0x3edfa2(0x564)]())return![];if(this[_0x3edfa2(0x1d5)]&&!this[_0x3edfa2(0x1d5)]['canBattlerMove']())return![];return $gameSystem[_0x3edfa2(0xdb)]();},Sprite_Battler['prototype'][_0x53c255(0x2aa)]=function(){},Sprite_Battler[_0x53c255(0x819)]['stepBack']=function(){const _0x235117=_0x53c255;this[_0x235117(0x64a)](0x0,0x0,0xc);},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x7e6)]=function(){},Sprite_Battler[_0x53c255(0x819)][_0x53c255(0x856)]=function(){const _0x376b01=_0x53c255,_0x5963ba=VisuMZ['BattleCore'][_0x376b01(0x4b3)][_0x376b01(0x38f)],_0x3e2b08=this[_0x376b01(0x1d5)]&&this[_0x376b01(0x1d5)][_0x376b01(0x807)]()?0x1:-0x1,_0x52a2a0=this[_0x376b01(0x396)]-this[_0x376b01(0x67c)]+_0x3e2b08*_0x5963ba[_0x376b01(0x4fe)],_0x59a9b6=this[_0x376b01(0x772)]-this[_0x376b01(0x4a5)]+_0x3e2b08*_0x5963ba[_0x376b01(0x64c)],_0x57f12c=_0x5963ba[_0x376b01(0x348)];this[_0x376b01(0x64a)](_0x52a2a0,_0x59a9b6,_0x57f12c);},VisuMZ[_0x53c255(0x2cc)]['Sprite_Actor_initMembers']=Sprite_Actor[_0x53c255(0x819)]['initMembers'],Sprite_Actor[_0x53c255(0x819)][_0x53c255(0x1ff)]=function(){const _0x5a4cf8=_0x53c255;VisuMZ[_0x5a4cf8(0x2cc)]['Sprite_Actor_initMembers'][_0x5a4cf8(0x6b9)](this),this[_0x5a4cf8(0x37f)]();},Sprite_Actor[_0x53c255(0x819)][_0x53c255(0x4c0)]=function(){const _0x2b4f0b=_0x53c255;return this[_0x2b4f0b(0x4d2)]||this[_0x2b4f0b(0x79b)]||this;},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x2a1)]=Sprite_Actor[_0x53c255(0x819)][_0x53c255(0x4fd)],Sprite_Actor['prototype']['moveToStartPosition']=function(){},Sprite_Actor[_0x53c255(0x819)]['moveToStartPositionBattleCore']=function(_0x499b29){const _0x38d3cf=_0x53c255;if(SceneManager['isPreviousSceneBattleTransitionable']())return;if(!_0x499b29)return;if(!_0x499b29['canMove']())return;VisuMZ[_0x38d3cf(0x2cc)][_0x38d3cf(0x2a1)][_0x38d3cf(0x6b9)](this);},VisuMZ['BattleCore']['Sprite_Actor_setActorHome']=Sprite_Actor[_0x53c255(0x819)][_0x53c255(0x15e)],Sprite_Actor['prototype'][_0x53c255(0x15e)]=function(_0x1786e1){const _0x358a0c=_0x53c255;VisuMZ[_0x358a0c(0x2cc)][_0x358a0c(0x4b3)]['Actor'][_0x358a0c(0x410)]?VisuMZ['BattleCore']['Settings'][_0x358a0c(0x38f)]['HomePosJS'][_0x358a0c(0x6b9)](this,_0x1786e1):VisuMZ[_0x358a0c(0x2cc)]['Sprite_Actor_setActorHome'][_0x358a0c(0x6b9)](this,_0x1786e1);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x1bc)]=Sprite_Actor[_0x53c255(0x819)][_0x53c255(0x6de)],Sprite_Actor[_0x53c255(0x819)]['setBattler']=function(_0x475be2){const _0x45b0b0=_0x53c255;VisuMZ['BattleCore'][_0x45b0b0(0x1bc)][_0x45b0b0(0x6b9)](this,_0x475be2),this[_0x45b0b0(0x59f)](_0x475be2);},Sprite_Actor['prototype'][_0x53c255(0x59f)]=function(_0xf8138e){const _0x3c212f=_0x53c255;if(!_0xf8138e)return;if(!this[_0x3c212f(0x79b)])return;this[_0x3c212f(0x79b)]['anchor']['x']=this[_0x3c212f(0x56e)][_0x3c212f(0x189)](),this[_0x3c212f(0x79b)][_0x3c212f(0x770)]['y']=this['_actor']['svBattlerAnchorY'](),this[_0x3c212f(0x764)]();},VisuMZ['BattleCore'][_0x53c255(0x15f)]=Sprite_Actor[_0x53c255(0x819)][_0x53c255(0x3ed)],Sprite_Actor['prototype']['update']=function(){const _0x25518a=_0x53c255;VisuMZ[_0x25518a(0x2cc)][_0x25518a(0x15f)][_0x25518a(0x6b9)](this),this[_0x25518a(0x56e)]&&(this[_0x25518a(0x42c)](),this[_0x25518a(0x14f)]());},VisuMZ['BattleCore'][_0x53c255(0x151)]=Sprite_Actor[_0x53c255(0x819)][_0x53c255(0x81c)],Sprite_Actor[_0x53c255(0x819)][_0x53c255(0x81c)]=function(){const _0x6e9b2c=_0x53c255;VisuMZ['BattleCore'][_0x6e9b2c(0x151)][_0x6e9b2c(0x6b9)](this),this['_mainSprite']&&this['_mainSprite']['bitmap']&&this[_0x6e9b2c(0x1d5)]&&(this[_0x6e9b2c(0x79b)]['bitmap'][_0x6e9b2c(0x7b6)]!==this[_0x6e9b2c(0x1d5)][_0x6e9b2c(0x588)]()&&(this[_0x6e9b2c(0x79b)][_0x6e9b2c(0x657)][_0x6e9b2c(0x7b6)]=this['_battler'][_0x6e9b2c(0x588)]()));},VisuMZ['BattleCore'][_0x53c255(0x6b5)]=Sprite_Actor[_0x53c255(0x819)][_0x53c255(0x3ab)],Sprite_Actor[_0x53c255(0x819)][_0x53c255(0x3ab)]=function(){const _0x4c7560=_0x53c255;VisuMZ['BattleCore'][_0x4c7560(0x6b5)][_0x4c7560(0x6b9)](this),this[_0x4c7560(0x7e1)]();},Sprite_Actor['prototype']['updateShadowBattleCore']=function(){const _0x213cf0=_0x53c255;if(!this[_0x213cf0(0x79b)])return;if(!this[_0x213cf0(0x76f)])return;this[_0x213cf0(0x764)](),this['updateShadowPosition']();},Sprite_Actor['prototype'][_0x53c255(0x42c)]=function(){const _0x2aadb1=_0x53c255;this[_0x2aadb1(0x1bd)][_0x2aadb1(0x2dd)]['x']=0x1/(this[_0x2aadb1(0x2dd)]['x']||0.001),this[_0x2aadb1(0x1bd)][_0x2aadb1(0x2dd)]['y']=0x1/(this['scale']['y']||0.001);},Sprite_Actor['prototype'][_0x53c255(0x14f)]=function(){const _0xcfe62b=_0x53c255;if(!$gameSystem[_0xcfe62b(0xdb)]()&&this[_0xcfe62b(0x70f)]===Sprite_Actor){const _0x298546=Scene_Battle['prototype'][_0xcfe62b(0x139)]();['default',_0xcfe62b(0x4a9),'portrait',_0xcfe62b(0x7a0)][_0xcfe62b(0x6a6)](_0x298546)&&(this[_0xcfe62b(0x398)]=0x0);}},Sprite_Actor['prototype']['refreshMotion']=function(){const _0x3302e7=_0x53c255,_0x4fcf37=this[_0x3302e7(0x56e)];if(_0x4fcf37){const _0x3def74=_0x4fcf37[_0x3302e7(0x4f1)]();if(_0x4fcf37[_0x3302e7(0x6c8)]()||_0x4fcf37['isActing']())this['startMotion'](_0x3302e7(0x718));else{if(_0x3def74===0x3)this['startMotion'](_0x3302e7(0x4f8));else{if(_0x3def74===0x2)this[_0x3302e7(0x326)](_0x3302e7(0x291));else{if(this[_0x3302e7(0x2da)])this[_0x3302e7(0x326)]('escape');else{if(_0x4fcf37[_0x3302e7(0x1db)]())this[_0x3302e7(0x326)](_0x3302e7(0x55f));else{if(_0x4fcf37[_0x3302e7(0x4dc)]())this[_0x3302e7(0x326)]('chant');else{if(_0x4fcf37[_0x3302e7(0x80e)]()||_0x4fcf37[_0x3302e7(0x3b0)]())this[_0x3302e7(0x326)](_0x3302e7(0x59a));else{if(_0x3def74===0x1)this[_0x3302e7(0x326)]('abnormal');else{if(_0x4fcf37[_0x3302e7(0x1f1)]())this[_0x3302e7(0x326)](_0x3302e7(0x3cb));else{if(_0x4fcf37[_0x3302e7(0x677)]())this[_0x3302e7(0x326)](_0x3302e7(0x718));else _0x4fcf37['currentAction']()?this['startMotion']('wait'):this[_0x3302e7(0x326)](_0x3302e7(0x718));}}}}}}}}}}},Sprite_Actor['prototype'][_0x53c255(0x7e6)]=function(){const _0x163028=_0x53c255,_0x43c2c4=0xa,_0x1620a5=0x12c*_0x43c2c4,_0x2cd450=0x1e*_0x43c2c4;this[_0x163028(0x64a)](_0x1620a5,0x0,_0x2cd450);},Sprite_Actor['prototype'][_0x53c255(0x5fd)]=function(){const _0x3bc74a=_0x53c255;Sprite_Battler[_0x3bc74a(0x819)][_0x3bc74a(0x5fd)]['call'](this);},Sprite_Actor[_0x53c255(0x819)][_0x53c255(0xdc)]=function(){const _0x139eb9=_0x53c255;return Sprite_Battler[_0x139eb9(0x6fb)];},Sprite_Weapon[_0x53c255(0x819)][_0x53c255(0xf0)]=function(){const _0x1e6440=_0x53c255;return Sprite_Battler[_0x1e6440(0x6fb)];},Sprite_Actor['prototype'][_0x53c255(0x769)]=function(){},Sprite_Actor[_0x53c255(0x819)][_0x53c255(0x1e2)]=function(){},Sprite_Actor[_0x53c255(0x819)]['updateMotionCount']=function(){const _0x42017=_0x53c255;if(this[_0x42017(0x85d)]&&++this[_0x42017(0x478)]>=this['motionSpeed']()){if(this['_motion']['loop'])this[_0x42017(0x51f)]=(this[_0x42017(0x51f)]+0x1)%0x4;else this['_pattern']<0x2?this[_0x42017(0x51f)]++:this[_0x42017(0x705)]();this[_0x42017(0x478)]=0x0;}},Sprite_Actor[_0x53c255(0x819)][_0x53c255(0x4bd)]=function(_0x1007b8){const _0x5714dc=_0x53c255;if(_0x1007b8===_0x5714dc(0x63e))this[_0x5714dc(0x44f)]=!![];if(this[_0x5714dc(0x1d5)]&&this[_0x5714dc(0x1d5)]['isDead']()){this[_0x5714dc(0x85d)]=Sprite_Actor['MOTIONS'][_0x5714dc(0x4f8)];return;}const _0x1aabc=Sprite_Actor['MOTIONS'][_0x1007b8];this[_0x5714dc(0x85d)]=_0x1aabc,this[_0x5714dc(0x478)]=0x0,this[_0x5714dc(0x51f)]=0x0;},Sprite_Actor[_0x53c255(0x819)]['forceWeaponAnimation']=function(_0x1e9ec5){const _0x54f772=_0x53c255;this[_0x54f772(0x1a0)](),this[_0x54f772(0x748)]['setup'](_0x1e9ec5),this[_0x54f772(0x56e)][_0x54f772(0x780)]();},Sprite_Actor[_0x53c255(0x819)][_0x53c255(0x1a0)]=function(){const _0x157f34=_0x53c255;let _0x562f9d=-0x10,_0x439387=this[_0x157f34(0x6ab)]*0.5;const _0x9fcb18=/<SIDEVIEW WEAPON OFFSET:[ ]([\+\-]\d+),[ ]([\+\-]\d+)>/i,_0x55f752=this[_0x157f34(0x1d5)][_0x157f34(0x357)]()[_0x157f34(0x19c)](_0x67a916=>_0x67a916&&_0x67a916[_0x157f34(0x236)][_0x157f34(0x473)](_0x9fcb18)?Number(RegExp['$1']):0x0),_0x575e8a=this[_0x157f34(0x1d5)][_0x157f34(0x357)]()[_0x157f34(0x19c)](_0x1c5d79=>_0x1c5d79&&_0x1c5d79[_0x157f34(0x236)][_0x157f34(0x473)](_0x9fcb18)?Number(RegExp['$2']):0x0);_0x562f9d=_0x55f752[_0x157f34(0x542)]((_0x394607,_0x3076ff)=>_0x394607+_0x3076ff,_0x562f9d),_0x439387=_0x575e8a[_0x157f34(0x542)]((_0x3a1a32,_0x4d4095)=>_0x3a1a32+_0x4d4095,_0x439387),this[_0x157f34(0x748)]['x']=_0x562f9d,this[_0x157f34(0x748)]['y']=_0x439387,this['_weaponSprite']['update']();},Sprite_Weapon[_0x53c255(0x819)][_0x53c255(0x157)]=function(_0x51d3a9){const _0x1dddb0=_0x53c255;this[_0x1dddb0(0x37b)]=_0x51d3a9,this[_0x1dddb0(0x41e)]=-0x1,this[_0x1dddb0(0x51f)]=0x0,this[_0x1dddb0(0x712)](),this['updateFrame']();},Sprite_Actor['prototype'][_0x53c255(0x6f4)]=function(){},Sprite_Actor[_0x53c255(0x819)][_0x53c255(0x2aa)]=function(){const _0x1da18c=_0x53c255,_0xe3f082=VisuMZ[_0x1da18c(0x2cc)][_0x1da18c(0x4b3)][_0x1da18c(0x431)],_0x960206=_0xe3f082['StepDistanceX'],_0x419e59=_0xe3f082[_0x1da18c(0x61a)],_0x4f77b2=_0xe3f082['StepDuration'];this[_0x1da18c(0x64a)](-_0x960206,-_0x419e59,_0x4f77b2);},VisuMZ[_0x53c255(0x2cc)]['Sprite_Actor_updateFrame']=Sprite_Actor[_0x53c255(0x819)]['updateFrame'],Sprite_Actor[_0x53c255(0x819)]['updateFrame']=function(){const _0x45ed00=_0x53c255;this[_0x45ed00(0x703)](),VisuMZ[_0x45ed00(0x2cc)][_0x45ed00(0x699)][_0x45ed00(0x6b9)](this);},Sprite_Actor[_0x53c255(0x819)][_0x53c255(0x703)]=function(){const _0x8e680=_0x53c255;if(this[_0x8e680(0x1d5)]&&this['_battler'][_0x8e680(0x80f)]){const _0x24f97b=this[_0x8e680(0x1d5)][_0x8e680(0x80f)];this[_0x8e680(0x85d)]=Sprite_Actor[_0x8e680(0x103)][_0x24f97b[_0x8e680(0x29d)]],this['_pattern']=_0x24f97b[_0x8e680(0x371)];const _0x26d685=this[_0x8e680(0x748)];_0x26d685['freezeFrame'](_0x24f97b['weaponImageId'],_0x24f97b[_0x8e680(0x371)]),this[_0x8e680(0x1a0)]();}},Sprite_Weapon[_0x53c255(0x819)][_0x53c255(0x1e0)]=function(_0x188e4e,_0x2c33c2){const _0x5c91ec=_0x53c255;this['_weaponImageId']=_0x188e4e,this['_animationCount']=-Infinity,this['_pattern']=_0x2c33c2,this[_0x5c91ec(0x712)](),this[_0x5c91ec(0x401)]();},Sprite_Enemy['prototype'][_0x53c255(0x1ff)]=function(){const _0x2915ed=_0x53c255;Sprite_Battler[_0x2915ed(0x819)][_0x2915ed(0x1ff)][_0x2915ed(0x6b9)](this),this['_enemy']=null,this[_0x2915ed(0x148)]=![],this[_0x2915ed(0x7cf)]='',this[_0x2915ed(0x1ec)]=0x0,this['_effectType']=null,this[_0x2915ed(0x248)]=0x0,this[_0x2915ed(0x313)]=0x0,this[_0x2915ed(0x363)](),this[_0x2915ed(0x449)]();},VisuMZ['BattleCore'][_0x53c255(0x4d4)]=Sprite_Enemy[_0x53c255(0x819)][_0x53c255(0x3ed)],Sprite_Enemy[_0x53c255(0x819)][_0x53c255(0x3ed)]=function(){const _0x489bc5=_0x53c255;VisuMZ[_0x489bc5(0x2cc)][_0x489bc5(0x4d4)][_0x489bc5(0x6b9)](this),this[_0x489bc5(0x764)]();},Sprite_Enemy[_0x53c255(0x819)]['createMainSprite']=function(){const _0x325c12=_0x53c255;this[_0x325c12(0x79b)]=new Sprite(),this[_0x325c12(0x79b)][_0x325c12(0x770)]['x']=0.5,this[_0x325c12(0x79b)]['anchor']['y']=0x1,this['addChild'](this[_0x325c12(0x79b)]),this['attachSpritesToDistortionSprite']();},Sprite_Enemy[_0x53c255(0x819)][_0x53c255(0x4c0)]=function(){const _0xf1e392=_0x53c255;return this[_0xf1e392(0x4d2)]||this['_mainSprite']||this;},Sprite_Enemy[_0x53c255(0x819)]['loadBitmap']=function(_0x2bd6fa){const _0x469478=_0x53c255;this[_0x469478(0x657)]=new Bitmap(0x1,0x1),$gameSystem['isSideView']()?this[_0x469478(0x79b)][_0x469478(0x657)]=ImageManager[_0x469478(0x76d)](_0x2bd6fa):this[_0x469478(0x79b)]['bitmap']=ImageManager[_0x469478(0x622)](_0x2bd6fa),this['_mainSprite'][_0x469478(0x657)][_0x469478(0x50a)](this[_0x469478(0x4bf)][_0x469478(0x4c7)](this));},Sprite_Enemy[_0x53c255(0x819)]['createEmptyBitmap']=function(){const _0x1a22b1=_0x53c255,_0x2b77df=this[_0x1a22b1(0x79b)]['bitmap'];_0x2b77df&&(this[_0x1a22b1(0x657)]=new Bitmap(_0x2b77df[_0x1a22b1(0x261)],_0x2b77df['height']));},VisuMZ['BattleCore'][_0x53c255(0x45c)]=Sprite_Enemy[_0x53c255(0x819)][_0x53c255(0x810)],Sprite_Enemy[_0x53c255(0x819)][_0x53c255(0x810)]=function(_0x1c60ae){const _0x41d6fe=_0x53c255;this[_0x41d6fe(0x79b)]&&this['_mainSprite'][_0x41d6fe(0x810)](_0x1c60ae);},VisuMZ[_0x53c255(0x2cc)]['Sprite_Enemy_initVisibility']=Sprite_Enemy[_0x53c255(0x819)][_0x53c255(0x462)],Sprite_Enemy[_0x53c255(0x819)][_0x53c255(0x462)]=function(){const _0x57e515=_0x53c255;this[_0x57e515(0x7ba)]()?VisuMZ[_0x57e515(0x2cc)][_0x57e515(0x7c7)][_0x57e515(0x6b9)](this):(this[_0x57e515(0x148)]=!this[_0x57e515(0x47d)][_0x57e515(0x39d)](),!this['_appeared']&&(this[_0x57e515(0x398)]=0x0));},VisuMZ[_0x53c255(0x2cc)]['Sprite_Enemy_updateCollapse']=Sprite_Enemy[_0x53c255(0x819)][_0x53c255(0x842)],Sprite_Enemy['prototype'][_0x53c255(0x842)]=function(){const _0x5ada4e=_0x53c255;if(this[_0x5ada4e(0x7ba)]())VisuMZ['BattleCore'][_0x5ada4e(0x71d)][_0x5ada4e(0x6b9)](this);},Sprite_Enemy[_0x53c255(0x819)][_0x53c255(0x401)]=function(){const _0x5ac7ef=_0x53c255;Sprite_Battler[_0x5ac7ef(0x819)][_0x5ac7ef(0x401)][_0x5ac7ef(0x6b9)](this);const _0x5623b7=this['mainSprite']()||this;if(!_0x5623b7)return;!_0x5623b7[_0x5ac7ef(0x657)]&&(_0x5623b7[_0x5ac7ef(0x657)]=new Bitmap(this[_0x5ac7ef(0x261)],this[_0x5ac7ef(0x6ab)])),this[_0x5ac7ef(0x559)]===_0x5ac7ef(0x48a)?this[_0x5ac7ef(0x79b)]['setFrame'](0x0,0x0,this[_0x5ac7ef(0x79b)]['width'],this[_0x5ac7ef(0x248)]):_0x5623b7[_0x5ac7ef(0x411)](0x0,0x0,_0x5623b7[_0x5ac7ef(0x657)][_0x5ac7ef(0x261)],this[_0x5ac7ef(0x657)][_0x5ac7ef(0x6ab)]);},VisuMZ['BattleCore']['Sprite_Enemy_updateBossCollapse']=Sprite_Enemy[_0x53c255(0x819)]['updateBossCollapse'],Sprite_Enemy[_0x53c255(0x819)]['updateBossCollapse']=function(){const _0x1897c9=_0x53c255;if(this[_0x1897c9(0x7ba)]())VisuMZ[_0x1897c9(0x2cc)][_0x1897c9(0x2eb)][_0x1897c9(0x6b9)](this);},Sprite_Enemy[_0x53c255(0x819)][_0x53c255(0x594)]=function(){const _0x41c94d=_0x53c255;return Sprite_Battler[_0x41c94d(0x819)][_0x41c94d(0x594)][_0x41c94d(0x6b9)](this);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x4eb)]=Sprite_Enemy[_0x53c255(0x819)][_0x53c255(0x42c)],Sprite_Enemy[_0x53c255(0x819)]['updateStateSprite']=function(){const _0x2606d8=_0x53c255;VisuMZ['BattleCore'][_0x2606d8(0x4eb)][_0x2606d8(0x6b9)](this),this['updateStateSpriteBattleCore']();},Sprite_Enemy['prototype'][_0x53c255(0x173)]=function(){const _0x25f3d3=_0x53c255;this[_0x25f3d3(0x273)]['x']=0x0,this[_0x25f3d3(0x273)]['x']+=this[_0x25f3d3(0x1d5)][_0x25f3d3(0x25c)](),this[_0x25f3d3(0x273)]['y']=-this[_0x25f3d3(0x657)][_0x25f3d3(0x6ab)]-this[_0x25f3d3(0x273)]['height'],this[_0x25f3d3(0x273)]['y']+=this[_0x25f3d3(0x1d5)]['battleUIOffsetY'](),this['_stateIconSprite'][_0x25f3d3(0x2dd)]['x']=0x1/(this[_0x25f3d3(0x2dd)]['x']||0.001),this[_0x25f3d3(0x273)][_0x25f3d3(0x2dd)]['y']=0x1/(this[_0x25f3d3(0x2dd)]['y']||0.001),this['hasSvBattler']()&&(this[_0x25f3d3(0x4bb)][_0x25f3d3(0x1bd)][_0x25f3d3(0x2dd)]['x']=-0x1/(this[_0x25f3d3(0x2dd)]['x']||0.001),this[_0x25f3d3(0x4bb)][_0x25f3d3(0x1bd)][_0x25f3d3(0x2dd)]['y']=0x1/(this[_0x25f3d3(0x2dd)]['y']||0.001));},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x53f)]=Sprite_Enemy[_0x53c255(0x819)][_0x53c255(0x6de)],Sprite_Enemy[_0x53c255(0x819)]['setBattler']=function(_0x455552){const _0x370419=_0x53c255;VisuMZ[_0x370419(0x2cc)][_0x370419(0x53f)][_0x370419(0x6b9)](this,_0x455552),this[_0x370419(0x455)](_0x455552);},Sprite_Enemy[_0x53c255(0x819)][_0x53c255(0x455)]=function(_0x5bc762){const _0x3b2070=_0x53c255;!this[_0x3b2070(0x4bb)]&&(this[_0x3b2070(0x4bb)]=new Sprite_SvEnemy(_0x5bc762),this['attachSpritesToDistortionSprite']()),this[_0x3b2070(0x4bb)][_0x3b2070(0x6de)](_0x5bc762);},Sprite_Enemy[_0x53c255(0x819)][_0x53c255(0x366)]=function(){const _0x2cb0b6=_0x53c255;return this[_0x2cb0b6(0x47d)]&&this['_enemy'][_0x2cb0b6(0x366)]();},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x3c1)]=Sprite_Enemy['prototype'][_0x53c255(0x712)],Sprite_Enemy[_0x53c255(0x819)][_0x53c255(0x712)]=function(_0x5788d4){const _0x3d7e31=_0x53c255;if(this[_0x3d7e31(0x366)]()){const _0x42cf64=this[_0x3d7e31(0x47d)]['svBattlerData']();this[_0x3d7e31(0x657)]=new Bitmap(_0x42cf64[_0x3d7e31(0x261)],_0x42cf64[_0x3d7e31(0x6ab)]);}else VisuMZ['BattleCore'][_0x3d7e31(0x3c1)]['call'](this,_0x5788d4);},Sprite_Enemy[_0x53c255(0x819)][_0x53c255(0x7ba)]=function(){const _0x4e482c=_0x53c255;return this[_0x4e482c(0x366)]()?this[_0x4e482c(0x47d)][_0x4e482c(0x7ba)]():!![];},Sprite_Enemy[_0x53c255(0x819)]['refreshMotion']=function(){const _0x14df35=_0x53c255;this[_0x14df35(0x366)]()&&this['_svBattlerSprite']['refreshMotion']();},Sprite_Enemy[_0x53c255(0x819)]['forceMotion']=function(_0x16a63e){const _0x5b98d9=_0x53c255;if(this['hasSvBattler']())this[_0x5b98d9(0x4bb)][_0x5b98d9(0x4bd)](_0x16a63e);},Sprite_Enemy['prototype'][_0x53c255(0x3c9)]=function(_0x475cc7){const _0x3f00c4=_0x53c255;if(this[_0x3f00c4(0x366)]())this['_svBattlerSprite'][_0x3f00c4(0x3c9)](_0x475cc7);},Sprite_Enemy[_0x53c255(0x819)]['stepForward']=function(){const _0x11c0b8=_0x53c255,_0x591041=VisuMZ[_0x11c0b8(0x2cc)]['Settings'][_0x11c0b8(0x431)],_0x2ebf78=_0x591041['StepDistanceX'],_0x5bdf6a=_0x591041[_0x11c0b8(0x61a)],_0x3a4341=_0x591041[_0x11c0b8(0x57a)];this['startMove'](_0x2ebf78,_0x5bdf6a,_0x3a4341);};function Sprite_SvEnemy(){const _0xb48a97=_0x53c255;this[_0xb48a97(0x854)](...arguments);}Sprite_SvEnemy[_0x53c255(0x819)]=Object['create'](Sprite_Actor[_0x53c255(0x819)]),Sprite_SvEnemy[_0x53c255(0x819)][_0x53c255(0x70f)]=Sprite_SvEnemy,Sprite_SvEnemy[_0x53c255(0x819)]['initialize']=function(_0x569962){const _0x503610=_0x53c255;Sprite_Actor['prototype'][_0x503610(0x854)][_0x503610(0x6b9)](this,_0x569962),this[_0x503610(0x2dd)]['x']=-0x1,this[_0x503610(0x1bd)][_0x503610(0x2dd)]['x']=-0x1;},Sprite_SvEnemy[_0x53c255(0x819)][_0x53c255(0x296)]=function(){},Sprite_SvEnemy[_0x53c255(0x819)]['moveToStartPosition']=function(){},Sprite_SvEnemy[_0x53c255(0x819)][_0x53c255(0x15e)]=function(_0x54406b){},Sprite_SvEnemy[_0x53c255(0x819)][_0x53c255(0x3ab)]=function(){},Sprite_SvEnemy[_0x53c255(0x819)][_0x53c255(0x51d)]=function(){},Sprite_SvEnemy['prototype'][_0x53c255(0x42c)]=function(){const _0x4d9173=_0x53c255;this[_0x4d9173(0x1bd)]['visible']=![];},Sprite_SvEnemy['prototype'][_0x53c255(0x81c)]=function(){const _0x3686ec=_0x53c255;Sprite_Battler[_0x3686ec(0x819)][_0x3686ec(0x81c)]['call'](this);const _0x451d41=this[_0x3686ec(0x56e)]['svBattlerName']();this[_0x3686ec(0x7cf)]!==_0x451d41&&(this['_battlerName']=_0x451d41,this[_0x3686ec(0x79b)]['bitmap']=ImageManager['loadSvActor'](_0x451d41)),this['_mainSprite']&&this[_0x3686ec(0x79b)]['bitmap']&&this[_0x3686ec(0x1d5)]&&(this['_mainSprite']['bitmap'][_0x3686ec(0x7b6)]!==this[_0x3686ec(0x1d5)][_0x3686ec(0x588)]()&&(this[_0x3686ec(0x79b)]['bitmap'][_0x3686ec(0x7b6)]=this['_battler']['battlerSmoothImage']()));},Sprite_SvEnemy[_0x53c255(0x819)][_0x53c255(0x7e6)]=function(){},Sprite_SvEnemy['prototype'][_0x53c255(0x64a)]=function(_0x5c6041,_0x587d6a,_0x38c4c9){const _0x4c6268=_0x53c255;if(this[_0x4c6268(0x7ce)])this[_0x4c6268(0x7ce)][_0x4c6268(0x64a)](_0x5c6041,_0x587d6a,_0x38c4c9);},Sprite_SvEnemy[_0x53c255(0x819)][_0x53c255(0x705)]=function(){const _0x291326=_0x53c255,_0x33e87e=this[_0x291326(0x56e)];if(_0x33e87e){const _0x32cb3e=_0x33e87e['stateMotionIndex']();if(_0x33e87e[_0x291326(0x6c8)]()||_0x33e87e[_0x291326(0xd9)]())this[_0x291326(0x326)]('walk');else{if(_0x32cb3e===0x3)this[_0x291326(0x326)](_0x291326(0x4f8));else{if(_0x32cb3e===0x2)this[_0x291326(0x326)](_0x291326(0x291));else{if(_0x33e87e[_0x291326(0x4dc)]())this[_0x291326(0x326)](_0x291326(0x43b));else{if(_0x33e87e[_0x291326(0x80e)]()||_0x33e87e['isGuardWaiting']())this[_0x291326(0x326)](_0x291326(0x59a));else{if(_0x32cb3e===0x1)this[_0x291326(0x326)]('abnormal');else{if(_0x33e87e[_0x291326(0x1f1)]())this['startMotion']('dying');else _0x33e87e[_0x291326(0x677)]()?this['startMotion']('walk'):this['startMotion'](_0x33e87e['svBattlerData']()[_0x291326(0x759)]||_0x291326(0x718));}}}}}}}},Sprite_SvEnemy[_0x53c255(0x819)][_0x53c255(0x175)]=function(){const _0x2a95d2=_0x53c255;return this[_0x2a95d2(0x7ce)]?this['parent'][_0x2a95d2(0x3b4)]===0x0&&this['parent'][_0x2a95d2(0x4dd)]===0x0:!![];},Sprite_SvEnemy['prototype'][_0x53c255(0x40b)]=function(){},Sprite_Damage['prototype'][_0x53c255(0x768)]=function(_0x51379b){const _0x19c2a3=_0x53c255,_0x1b5220=_0x51379b['getNextDamagePopup']()||_0x51379b[_0x19c2a3(0x284)]();if(_0x1b5220[_0x19c2a3(0x7df)]||_0x1b5220[_0x19c2a3(0x265)])this[_0x19c2a3(0x1c3)]=0x0,this[_0x19c2a3(0x60f)]();else{if(_0x1b5220[_0x19c2a3(0x3fa)])this[_0x19c2a3(0x1c3)]=_0x1b5220[_0x19c2a3(0x333)]>=0x0?0x0:0x1,this[_0x19c2a3(0x144)](_0x1b5220['hpDamage']);else _0x51379b[_0x19c2a3(0x72f)]()&&_0x1b5220[_0x19c2a3(0x6d4)]!==0x0&&(this[_0x19c2a3(0x1c3)]=_0x1b5220[_0x19c2a3(0x6d4)]>=0x0?0x2:0x3,this[_0x19c2a3(0x144)](_0x1b5220[_0x19c2a3(0x6d4)]));}_0x1b5220[_0x19c2a3(0x1d8)]&&this['setupCriticalEffect']();},Sprite_Damage['prototype'][_0x53c255(0x157)]=function(_0xdc4d9b){},Sprite_Damage[_0x53c255(0x819)]['createDigits']=function(_0x6a7e9f){const _0x2ca59c=_0x53c255;let _0x3fe6b1=this[_0x2ca59c(0x3be)](_0x6a7e9f);const _0x158383=this[_0x2ca59c(0x5a2)](),_0x33784a=Math['floor'](_0x158383*0.75);for(let _0x3e4635=0x0;_0x3e4635<_0x3fe6b1[_0x2ca59c(0x343)];_0x3e4635++){const _0x223b46=this[_0x2ca59c(0x4a4)](_0x33784a,_0x158383);_0x223b46[_0x2ca59c(0x657)][_0x2ca59c(0x375)](_0x3fe6b1[_0x3e4635],0x0,0x0,_0x33784a,_0x158383,'center'),_0x223b46['x']=(_0x3e4635-(_0x3fe6b1[_0x2ca59c(0x343)]-0x1)/0x2)*_0x33784a,_0x223b46['dy']=-_0x3e4635;}},Sprite_Damage['prototype'][_0x53c255(0x3be)]=function(_0x4cce67){const _0x2687a3=_0x53c255;let _0x444343=Math[_0x2687a3(0x1f4)](_0x4cce67)['toString']();this[_0x2687a3(0x23d)]()&&(_0x444343=VisuMZ[_0x2687a3(0x378)](_0x444343));const _0x3f7d52=VisuMZ[_0x2687a3(0x2cc)][_0x2687a3(0x4b3)][_0x2687a3(0x811)];let _0x3c73e6='',_0x9e782b='';switch(this[_0x2687a3(0x1c3)]){case 0x0:_0x3c73e6=_0x3f7d52['hpDamageFmt']||_0x2687a3(0x113),_0x9e782b=TextManager['hp'];if(_0x4cce67===0x0)_0x3c73e6='%1';break;case 0x1:_0x3c73e6=_0x3f7d52[_0x2687a3(0x3d4)]||_0x2687a3(0x6f3),_0x9e782b=TextManager['hp'];break;case 0x2:_0x3c73e6=_0x3f7d52[_0x2687a3(0x82a)]||_0x2687a3(0x7a9),_0x9e782b=TextManager['mp'];break;case 0x3:_0x3c73e6=_0x3f7d52[_0x2687a3(0x6db)]||'+%1\x20MP',_0x9e782b=TextManager['mp'];break;}return _0x3c73e6[_0x2687a3(0x107)](_0x444343,_0x9e782b)[_0x2687a3(0x615)]();},Sprite_Damage[_0x53c255(0x819)][_0x53c255(0x23d)]=function(){const _0x910200=_0x53c255;return Imported[_0x910200(0x23a)]?VisuMZ[_0x910200(0x485)][_0x910200(0x4b3)][_0x910200(0x506)][_0x910200(0x3a6)]:![];},Sprite_Damage['prototype'][_0x53c255(0x204)]=function(){const _0x3f9ae6=_0x53c255,_0x49af02=VisuMZ[_0x3f9ae6(0x2cc)][_0x3f9ae6(0x4b3)][_0x3f9ae6(0x811)];this[_0x3f9ae6(0x2b6)]=_0x49af02[_0x3f9ae6(0x672)][_0x3f9ae6(0x599)](0x0),this[_0x3f9ae6(0x751)]=_0x49af02[_0x3f9ae6(0x1e3)];},Sprite_Damage[_0x53c255(0x819)][_0x53c255(0x6e0)]=function(_0x41effa,_0x8114da){const _0xb873e6=_0x53c255;this[_0xb873e6(0x2b6)]=_0x8114da['flashColor']||[0x0,0x0,0x0,0x0],this[_0xb873e6(0x2b6)]=JsonEx['makeDeepCopy'](this[_0xb873e6(0x2b6)]),this[_0xb873e6(0x751)]=_0x8114da[_0xb873e6(0x6ad)]||0x0;const _0x1cf612=this[_0xb873e6(0x5a2)](),_0x20014f=Math['floor'](_0x1cf612*0x1e),_0x1cf6ea=this['createChildSprite'](_0x20014f,_0x1cf612);_0x1cf6ea[_0xb873e6(0x657)]['textColor']=ColorManager[_0xb873e6(0x7fd)](_0x8114da['textColor']),_0x1cf6ea[_0xb873e6(0x657)]['drawText'](_0x41effa,0x0,0x0,_0x20014f,_0x1cf612,_0xb873e6(0x211)),_0x1cf6ea['dy']=0x0;},Sprite_Damage[_0x53c255(0x819)][_0x53c255(0x5c2)]=function(_0x294bbb,_0x5164fe,_0x43a311){const _0xbaa810=_0x53c255,_0x58d9fd=Math['max'](this[_0xbaa810(0x5a2)](),ImageManager[_0xbaa810(0x183)]),_0x1c946e=Math['floor'](_0x58d9fd*0x1e),_0x37f3b5=this[_0xbaa810(0x4a4)](_0x1c946e,_0x58d9fd),_0x44c544=ImageManager[_0xbaa810(0x142)]/0x2,_0x3567a2=_0x37f3b5[_0xbaa810(0x657)]['measureTextWidth'](_0x5164fe+'\x20');_0x37f3b5[_0xbaa810(0x657)]['textColor']=ColorManager['getColor'](_0x43a311[_0xbaa810(0x648)]),_0x37f3b5[_0xbaa810(0x657)][_0xbaa810(0x375)](_0x5164fe,_0x44c544,0x0,_0x1c946e-_0x44c544,_0x58d9fd,_0xbaa810(0x211));const _0x5b169f=Math['round']((_0x58d9fd-ImageManager[_0xbaa810(0x183)])/0x2),_0x501de9=_0x1c946e/0x2-ImageManager['iconWidth']-_0x3567a2/0x2+_0x44c544/0x2,_0x26f199=ImageManager[_0xbaa810(0x518)](_0xbaa810(0x4ca)),_0x1ea038=ImageManager['iconWidth'],_0x2dc152=ImageManager[_0xbaa810(0x183)],_0x30853e=_0x294bbb%0x10*_0x1ea038,_0x336b73=Math[_0xbaa810(0x7f5)](_0x294bbb/0x10)*_0x2dc152;_0x37f3b5[_0xbaa810(0x657)]['blt'](_0x26f199,_0x30853e,_0x336b73,_0x1ea038,_0x2dc152,_0x501de9,_0x5b169f),this[_0xbaa810(0x2b6)]=_0x43a311['flashColor']||[0x0,0x0,0x0,0x0],this[_0xbaa810(0x2b6)]=JsonEx[_0xbaa810(0x37a)](this[_0xbaa810(0x2b6)]),this[_0xbaa810(0x751)]=_0x43a311[_0xbaa810(0x6ad)]||0x0,_0x37f3b5['dy']=0x0;},VisuMZ['BattleCore']['Sprite_StateIcon_updateFrame']=Sprite_StateIcon[_0x53c255(0x819)]['updateFrame'],Sprite_StateIcon['prototype'][_0x53c255(0x401)]=function(){const _0xd184fb=_0x53c255;VisuMZ[_0xd184fb(0x2cc)]['Sprite_StateIcon_updateFrame'][_0xd184fb(0x6b9)](this),this[_0xd184fb(0x3f2)]=this[_0xd184fb(0x3bb)]>0x0?!![]:![];},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x355)]=Sprite_Weapon['prototype']['loadBitmap'],Sprite_Weapon['prototype'][_0x53c255(0x712)]=function(){const _0x2f5aeb=_0x53c255;VisuMZ[_0x2f5aeb(0x2cc)][_0x2f5aeb(0x355)][_0x2f5aeb(0x6b9)](this),this[_0x2f5aeb(0x657)]&&(this[_0x2f5aeb(0x657)][_0x2f5aeb(0x7b6)]=VisuMZ[_0x2f5aeb(0x2cc)][_0x2f5aeb(0x4b3)]['Actor'][_0x2f5aeb(0x38d)]);};function Sprite_HpGauge(){const _0x4c0901=_0x53c255;this[_0x4c0901(0x854)](...arguments);}Sprite_HpGauge['prototype']=Object[_0x53c255(0x21a)](Sprite_Gauge['prototype']),Sprite_HpGauge[_0x53c255(0x819)]['constructor']=Sprite_HpGauge,Sprite_HpGauge['prototype'][_0x53c255(0x854)]=function(){const _0x27b1dd=_0x53c255;Sprite_Gauge[_0x27b1dd(0x819)][_0x27b1dd(0x854)][_0x27b1dd(0x6b9)](this);},Sprite_HpGauge['prototype'][_0x53c255(0x739)]=function(){return 0x0;},Sprite_HpGauge[_0x53c255(0x819)][_0x53c255(0x3db)]=function(){const _0x29b8d4=_0x53c255;this[_0x29b8d4(0x657)][_0x29b8d4(0x6fe)]();const _0x10a830=this[_0x29b8d4(0x750)]();!isNaN(_0x10a830)&&this[_0x29b8d4(0x143)]();},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0xf9)]=Sprite_Battleback[_0x53c255(0x819)][_0x53c255(0x618)],Sprite_Battleback[_0x53c255(0x819)][_0x53c255(0x618)]=function(){const _0x434493=_0x53c255,_0x5967da=VisuMZ['BattleCore'][_0x434493(0x4b3)][_0x434493(0x77c)];if(!_0x5967da)return VisuMZ[_0x434493(0x2cc)][_0x434493(0xf9)][_0x434493(0x6b9)](this);const _0x1960f7=String(_0x5967da[_0x434493(0x5ac)])||'MZ';switch(_0x1960f7){case'MZ':VisuMZ[_0x434493(0x2cc)][_0x434493(0xf9)]['call'](this);break;case _0x434493(0x1de):this['adjustPosition_1for1']();break;case _0x434493(0x381):this[_0x434493(0x447)]();break;case'ScaleDown':this[_0x434493(0x3da)]();break;case _0x434493(0x524):this['adjustPosition_ScaleUp']();break;}},Sprite_Battleback[_0x53c255(0x819)]['adjustPosition_1for1']=function(){const _0x466d07=_0x53c255;this['width']=Graphics[_0x466d07(0x261)],this[_0x466d07(0x6ab)]=Graphics['height'];const _0x1d853b=0x1;this[_0x466d07(0x2dd)]['x']=_0x1d853b,this[_0x466d07(0x2dd)]['y']=_0x1d853b,this['x']=0x0,this['y']=0x0;},Sprite_Battleback[_0x53c255(0x819)][_0x53c255(0x447)]=function(){const _0x1d740a=_0x53c255;this[_0x1d740a(0x261)]=Graphics[_0x1d740a(0x261)],this[_0x1d740a(0x6ab)]=Graphics[_0x1d740a(0x6ab)];const _0x2273fc=this['width']/this[_0x1d740a(0x657)]['width'],_0x29fb0e=this['height']/this[_0x1d740a(0x657)][_0x1d740a(0x6ab)],_0x1aa910=Math[_0x1d740a(0x776)](_0x2273fc,_0x29fb0e);this[_0x1d740a(0x2dd)]['x']=_0x1aa910,this[_0x1d740a(0x2dd)]['y']=_0x1aa910,this['x']=(Graphics[_0x1d740a(0x261)]-this['width'])/0x2,this['y']=Graphics[_0x1d740a(0x6ab)]-this['height'];},Sprite_Battleback[_0x53c255(0x819)][_0x53c255(0x3da)]=function(){const _0x3895dd=_0x53c255;this[_0x3895dd(0x261)]=Graphics[_0x3895dd(0x261)],this[_0x3895dd(0x6ab)]=Graphics[_0x3895dd(0x6ab)];const _0x590ae4=Math[_0x3895dd(0x413)](0x1,this[_0x3895dd(0x261)]/this['bitmap']['width']),_0x1e2a8b=Math['min'](0x1,this[_0x3895dd(0x6ab)]/this[_0x3895dd(0x657)][_0x3895dd(0x6ab)]),_0x8fde4b=Math[_0x3895dd(0x776)](_0x590ae4,_0x1e2a8b);this[_0x3895dd(0x2dd)]['x']=_0x8fde4b,this[_0x3895dd(0x2dd)]['y']=_0x8fde4b,this['x']=(Graphics[_0x3895dd(0x261)]-this[_0x3895dd(0x261)])/0x2,this['y']=Graphics[_0x3895dd(0x6ab)]-this[_0x3895dd(0x6ab)];},Sprite_Battleback[_0x53c255(0x819)][_0x53c255(0x6b7)]=function(){const _0x8ea922=_0x53c255;this['width']=Graphics[_0x8ea922(0x261)],this['height']=Graphics[_0x8ea922(0x6ab)];const _0x22860a=Math[_0x8ea922(0x776)](0x1,this['width']/this[_0x8ea922(0x657)][_0x8ea922(0x261)]),_0x4ffca6=Math[_0x8ea922(0x776)](0x1,this[_0x8ea922(0x6ab)]/this[_0x8ea922(0x657)]['height']),_0x376e5d=Math['max'](_0x22860a,_0x4ffca6);this[_0x8ea922(0x2dd)]['x']=_0x376e5d,this['scale']['y']=_0x376e5d,this['x']=(Graphics[_0x8ea922(0x261)]-this[_0x8ea922(0x261)])/0x2,this['y']=Graphics[_0x8ea922(0x6ab)]-this[_0x8ea922(0x6ab)];},Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x803)]=function(){const _0x1c97b1=_0x53c255;if(!$gameSystem[_0x1c97b1(0xdb)]())return![];return![];},Spriteset_Battle['prototype'][_0x53c255(0x174)]=function(){return 0x0;},Spriteset_Battle[_0x53c255(0x819)]['animationNextDelay']=function(){return 0x0;},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x566)]=Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x7bd)],Spriteset_Battle['prototype'][_0x53c255(0x7bd)]=function(){const _0x321377=_0x53c255;VisuMZ['BattleCore'][_0x321377(0x566)][_0x321377(0x6b9)](this),this['createWeather']();},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x247)]=Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x3ed)],Spriteset_Battle[_0x53c255(0x819)]['update']=function(){const _0x38267b=_0x53c255;VisuMZ[_0x38267b(0x2cc)]['Spriteset_Battle_update'][_0x38267b(0x6b9)](this),this['updateWeather']();},Spriteset_Battle['prototype'][_0x53c255(0x124)]=function(){const _0x5c7a9b=_0x53c255;this['_weather']=new Weather(),this[_0x5c7a9b(0x7d1)][_0x5c7a9b(0x62f)](this[_0x5c7a9b(0x1e8)]);},Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x2e6)]=function(){const _0x580ddf=_0x53c255;this[_0x580ddf(0x1e8)][_0x580ddf(0x3ce)]=$gameScreen['weatherType'](),this[_0x580ddf(0x1e8)][_0x580ddf(0x6e6)]=$gameScreen['weatherPower']();},Game_Interpreter[_0x53c255(0x819)]['command236']=function(_0x2fe940){const _0x213157=_0x53c255;$gameScreen[_0x213157(0x3a9)](_0x2fe940[0x0],_0x2fe940[0x1],_0x2fe940[0x2]);if(_0x2fe940[0x3])this[_0x213157(0x55f)](_0x2fe940[0x2]);return!![];},VisuMZ['BattleCore'][_0x53c255(0x681)]=Game_Interpreter[_0x53c255(0x819)]['command283'],Game_Interpreter[_0x53c255(0x819)]['command283']=function(_0x2c2e9c){const _0x5d1674=_0x53c255;return SceneManager['isSceneBattle']()?(SceneManager[_0x5d1674(0x12f)][_0x5d1674(0x3fb)][_0x5d1674(0x55a)](_0x2c2e9c[0x0],_0x2c2e9c[0x1]),!![]):VisuMZ[_0x5d1674(0x2cc)]['Game_Interpreter_command283'][_0x5d1674(0x6b9)](this,_0x2c2e9c);},Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x48b)]=function(_0x2ee330,_0x365b4f){const _0x2bab55=_0x53c255;_0x2ee330[_0x2bab55(0x657)]=_0x365b4f;},Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x55a)]=function(_0x3c61c7,_0x502ac1){const _0x4d0a54=_0x53c255;_0x3c61c7=_0x3c61c7||'',_0x502ac1=_0x502ac1||'';_0x3c61c7===''&&_0x502ac1===''&&(_0x3c61c7=this[_0x4d0a54(0x36d)][_0x4d0a54(0xe8)](),_0x502ac1=this[_0x4d0a54(0xfc)]['battleback2Name']());const _0x19f690=ImageManager['loadBattleback1'](_0x3c61c7),_0x16f629=ImageManager['loadBattleback2'](_0x502ac1);_0x19f690[_0x4d0a54(0x50a)](this[_0x4d0a54(0x70a)][_0x4d0a54(0x4c7)](this,this[_0x4d0a54(0x36d)],this[_0x4d0a54(0xfc)],_0x19f690,_0x16f629));},Spriteset_Battle['prototype']['updateBattlebackBitmap1']=function(_0x39c2d3,_0x26746d,_0x1b8ba8,_0x54dea3){const _0x122ed5=_0x53c255;_0x54dea3[_0x122ed5(0x50a)](this['updateBattlebackBitmap2'][_0x122ed5(0x4c7)](this,_0x39c2d3,_0x26746d,_0x1b8ba8,_0x54dea3));},Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x551)]=function(_0x4a9d82,_0x2167d0,_0x5710a0,_0x375cc2){const _0x271345=_0x53c255;_0x4a9d82['bitmap']=_0x5710a0,_0x2167d0['bitmap']=_0x375cc2,_0x4a9d82[_0x271345(0x618)](),_0x2167d0[_0x271345(0x618)]();},VisuMZ[_0x53c255(0x2cc)]['Spriteset_Battle_createBattleField']=Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x41f)],Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x41f)]=function(){const _0x2c6a3d=_0x53c255;VisuMZ[_0x2c6a3d(0x2cc)][_0x2c6a3d(0x586)][_0x2c6a3d(0x6b9)](this),this[_0x2c6a3d(0x4a8)]();},Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x4a8)]=function(){const _0x22ee00=_0x53c255;this[_0x22ee00(0x53d)](),this[_0x22ee00(0x2d1)](),this[_0x22ee00(0x1e7)](),this[_0x22ee00(0x7fe)]();},Spriteset_Battle['prototype']['createBattleFieldContainer']=function(){const _0x5864d3=_0x53c255;this[_0x5864d3(0x5b5)]=new Sprite(),this['_battleField'][_0x5864d3(0x62f)](this[_0x5864d3(0x5b5)]);},Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x2d1)]=function(){const _0x4ea0e5=_0x53c255;this['_animationContainer']=new Sprite(),this[_0x4ea0e5(0x7d1)][_0x4ea0e5(0x62f)](this['_animationContainer']);},Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x1e7)]=function(){const _0x1ade3f=_0x53c255;this[_0x1ade3f(0x5dd)]=new Sprite(),this[_0x1ade3f(0x5dd)]['x']=this[_0x1ade3f(0x7d1)]['x'],this[_0x1ade3f(0x5dd)]['y']=this[_0x1ade3f(0x7d1)]['y'],this[_0x1ade3f(0x62f)](this[_0x1ade3f(0x5dd)]);},Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x7fe)]=function(){const _0x3a14fa=_0x53c255;if(!this[_0x3a14fa(0x803)]())return;this['_battlerContainer'][_0x3a14fa(0x2dd)]['x']=-0x1,this['_battlerContainer']['x']=this['_battleField'][_0x3a14fa(0x261)],this[_0x3a14fa(0x4c8)][_0x3a14fa(0x2dd)]['x']=-0x1,this[_0x3a14fa(0x4c8)]['x']=this[_0x3a14fa(0x7d1)]['width'],this[_0x3a14fa(0x5dd)][_0x3a14fa(0x2dd)]['x']=-0x1,this[_0x3a14fa(0x5dd)]['x']=this[_0x3a14fa(0x7d1)]['x']+this[_0x3a14fa(0x7d1)][_0x3a14fa(0x261)];},Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x35f)]=function(){const _0x32f653=_0x53c255;Imported[_0x32f653(0x23a)]&&VisuMZ[_0x32f653(0x485)][_0x32f653(0x4b3)]['UI'][_0x32f653(0x698)]&&this[_0x32f653(0x6fa)]();const _0x109b91=$gameTroop[_0x32f653(0x4e3)](),_0x253253=[];for(const _0x21bb26 of _0x109b91){_0x253253[_0x32f653(0x2bf)](new Sprite_Enemy(_0x21bb26));}_0x253253[_0x32f653(0x4e1)](this[_0x32f653(0x2de)]['bind'](this));for(const _0x590def of _0x253253){this[_0x32f653(0x5b5)][_0x32f653(0x62f)](_0x590def);}this[_0x32f653(0x535)]=_0x253253;},Spriteset_Battle['prototype']['createActors']=function(){const _0x5bbb9b=_0x53c255;this[_0x5bbb9b(0x7f0)]=[];for(let _0x53ff8a=0x0;_0x53ff8a<$gameParty[_0x5bbb9b(0x35c)]();_0x53ff8a++){const _0x3e67b7=$gameParty['battleMembers']()[_0x53ff8a],_0x2105ed=new Sprite_Actor();_0x2105ed['moveToStartPositionBattleCore'](_0x3e67b7),_0x2105ed[_0x5bbb9b(0x6de)](_0x3e67b7),_0x2105ed['update'](),this[_0x5bbb9b(0x7f0)][_0x5bbb9b(0x2bf)](_0x2105ed),this[_0x5bbb9b(0x5b5)]['addChild'](_0x2105ed);}},Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x451)]=function(_0x5d94a9,_0x5b7d75,_0x55588d,_0x5e597a){const _0x1e9a14=_0x53c255,_0x2b1d5f=this[_0x1e9a14(0x4ad)](_0x5b7d75),_0x1a82c3=new(_0x2b1d5f?Sprite_AnimationMV:Sprite_Animation)(),_0x2749fb=this[_0x1e9a14(0x152)](_0x5d94a9);this[_0x1e9a14(0x7bf)](_0x5d94a9[0x0])&&(_0x55588d=!_0x55588d),_0x1a82c3[_0x1e9a14(0x316)]=_0x5d94a9,_0x1a82c3[_0x1e9a14(0x157)](_0x2749fb,_0x5b7d75,_0x55588d,_0x5e597a),this[_0x1e9a14(0x554)](_0x1a82c3);},Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x554)]=function(_0x86703a){const _0x489c82=_0x53c255;this[_0x489c82(0x354)](_0x86703a)?this[_0x489c82(0x469)]()[_0x489c82(0x62f)](_0x86703a):this[_0x489c82(0x4c8)][_0x489c82(0x62f)](_0x86703a),this[_0x489c82(0x7cb)]['push'](_0x86703a);},Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x354)]=function(_0x525144){const _0x2d3cda=_0x53c255;if(!_0x525144)return![];if(!_0x525144[_0x2d3cda(0x104)])return![];if(_0x525144[_0x2d3cda(0x104)][_0x2d3cda(0x198)]!==0x0)return![];if(!_0x525144[_0x2d3cda(0x316)][0x0])return![];if(!_0x525144['targetObjects'][0x0][_0x2d3cda(0x807)]())return![];if($gameSystem[_0x2d3cda(0xdb)]())return![];if(!this['battleStatusWindowAnimationContainer']())return![];return Window_BattleStatus[_0x2d3cda(0x819)][_0x2d3cda(0x139)]()==='portrait';},Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x469)]=function(){const _0x1bc7a1=_0x53c255;if(!SceneManager[_0x1bc7a1(0x12f)])return;if(!SceneManager[_0x1bc7a1(0x12f)][_0x1bc7a1(0x42f)])return;if(!SceneManager[_0x1bc7a1(0x12f)][_0x1bc7a1(0x42f)][_0x1bc7a1(0x377)])return;return SceneManager[_0x1bc7a1(0x12f)][_0x1bc7a1(0x42f)]['_effectsContainer'];},Spriteset_Battle['prototype'][_0x53c255(0x1a1)]=function(_0x21002a){const _0x526cbc=_0x53c255;this[_0x526cbc(0x730)](_0x21002a);for(const _0x5dffe8 of _0x21002a[_0x526cbc(0x316)]){_0x5dffe8[_0x526cbc(0x1c0)]&&_0x5dffe8[_0x526cbc(0x1c0)]();}_0x21002a[_0x526cbc(0x3ea)]();},Spriteset_Battle[_0x53c255(0x819)]['removeAnimationFromContainer']=function(_0x5e17d7){const _0x44c360=_0x53c255;this[_0x44c360(0x7cb)][_0x44c360(0x725)](_0x5e17d7),this[_0x44c360(0x354)](_0x5e17d7)?this[_0x44c360(0x469)]()[_0x44c360(0x4d6)](_0x5e17d7):this['_animationContainer']['removeChild'](_0x5e17d7);},VisuMZ[_0x53c255(0x2cc)]['Spriteset_Battle_updateActors']=Spriteset_Battle['prototype']['updateActors'],Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x69d)]=function(){const _0x4dc65d=_0x53c255;VisuMZ[_0x4dc65d(0x2cc)][_0x4dc65d(0xd8)][_0x4dc65d(0x6b9)](this),this[_0x4dc65d(0x226)]();},Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x226)]=function(){const _0x37d29e=_0x53c255;this[_0x37d29e(0x5b5)][_0x37d29e(0x26d)][_0x37d29e(0x4e1)](this[_0x37d29e(0x849)][_0x37d29e(0x4c7)](this)),this[_0x37d29e(0x54a)]();},Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x849)]=function(_0x221cb0,_0x31fab8){const _0x87c632=_0x53c255;if(VisuMZ[_0x87c632(0x2cc)][_0x87c632(0x4b3)][_0x87c632(0x38f)][_0x87c632(0x2e9)]){if(_0x221cb0['_battler']&&_0x31fab8[_0x87c632(0x1d5)]){if(_0x221cb0[_0x87c632(0x1d5)][_0x87c632(0x807)]()&&_0x31fab8[_0x87c632(0x1d5)]['isEnemy']())return 0x1;else{if(_0x31fab8[_0x87c632(0x1d5)][_0x87c632(0x807)]()&&_0x221cb0[_0x87c632(0x1d5)][_0x87c632(0x26c)]())return-0x1;}}}return _0x221cb0['_baseY']!==_0x31fab8['_baseY']?_0x221cb0['_baseY']-_0x31fab8['_baseY']:_0x31fab8['spriteId']-_0x221cb0[_0x87c632(0x5da)];},Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x54a)]=function(){const _0x2f71b8=_0x53c255;if(!VisuMZ[_0x2f71b8(0x2cc)][_0x2f71b8(0x4b3)][_0x2f71b8(0x38f)][_0x2f71b8(0x59d)])return;const _0x509dc9=BattleManager[_0x2f71b8(0x650)];if(_0x509dc9){if(_0x509dc9['isActor']()&&!$gameSystem[_0x2f71b8(0xdb)]())return;const _0x5a7863=_0x509dc9[_0x2f71b8(0x5ef)]();if(_0x5a7863&&_0x509dc9['isActor']())this[_0x2f71b8(0x5b5)][_0x2f71b8(0x62f)](_0x5a7863);}},Spriteset_Battle['prototype'][_0x53c255(0x336)]=function(){const _0x33e6a0=_0x53c255;for(const _0x15816a of $gameParty['aliveMembers']()){if(!_0x15816a)continue;if(!_0x15816a[_0x33e6a0(0x5ef)]())continue;_0x15816a[_0x33e6a0(0x5ef)]()[_0x33e6a0(0x2da)]=!![],_0x15816a[_0x33e6a0(0x5ef)]()[_0x33e6a0(0x7e6)]();}},Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x456)]=function(){return![];},Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x314)]=function(){const _0x567727=_0x53c255;return this[_0x567727(0x253)]()[_0x567727(0x73d)](_0x1b0de9=>_0x1b0de9[_0x567727(0x6a1)]());},Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x3cd)]=function(){return this['battlerSprites']()['some'](_0x36e759=>_0x36e759['isJumping']());},Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x186)]=function(){const _0x435838=_0x53c255;return this[_0x435838(0x253)]()[_0x435838(0x73d)](_0x5d0c4b=>_0x5d0c4b[_0x435838(0x494)]());},Spriteset_Battle[_0x53c255(0x819)][_0x53c255(0x24e)]=function(){const _0x570a14=_0x53c255;return this[_0x570a14(0x253)]()[_0x570a14(0x73d)](_0x59f076=>_0x59f076['isSkewing']());},Spriteset_Battle['prototype']['isAnyoneSpinning']=function(){const _0x5baed2=_0x53c255;return this[_0x5baed2(0x253)]()[_0x5baed2(0x73d)](_0x541b22=>_0x541b22['isSpinning']());},Spriteset_Battle['prototype'][_0x53c255(0x1c1)]=function(){const _0x333fe9=_0x53c255;return this[_0x333fe9(0x253)]()[_0x333fe9(0x73d)](_0x3ab8b4=>_0x3ab8b4[_0x333fe9(0x5df)]());},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x2fb)]=Window_ItemList[_0x53c255(0x819)][_0x53c255(0x15a)],Window_ItemList[_0x53c255(0x819)][_0x53c255(0x15a)]=function(){const _0x4ff47f=_0x53c255;return SceneManager[_0x4ff47f(0x45f)]()?SceneManager[_0x4ff47f(0x12f)]['battleLayoutStyle']()===_0x4ff47f(0x7a0)?VisuMZ[_0x4ff47f(0x2cc)]['Settings'][_0x4ff47f(0xf1)][_0x4ff47f(0x3e6)]:VisuMZ[_0x4ff47f(0x2cc)][_0x4ff47f(0x4b3)]['BattleLayout'][_0x4ff47f(0x3af)]:VisuMZ['BattleCore'][_0x4ff47f(0x2fb)][_0x4ff47f(0x6b9)](this);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x16d)]=Window_SkillList[_0x53c255(0x819)]['maxCols'],Window_SkillList[_0x53c255(0x819)][_0x53c255(0x15a)]=function(){const _0x307d87=_0x53c255;return SceneManager[_0x307d87(0x45f)]()?SceneManager[_0x307d87(0x12f)]['battleLayoutStyle']()==='border'?VisuMZ[_0x307d87(0x2cc)]['Settings'][_0x307d87(0xf1)][_0x307d87(0x3e6)]:VisuMZ[_0x307d87(0x2cc)][_0x307d87(0x4b3)][_0x307d87(0xf1)][_0x307d87(0x3af)]:VisuMZ['BattleCore'][_0x307d87(0x16d)]['call'](this);},VisuMZ[_0x53c255(0x2cc)]['Window_Options_addGeneralOptions']=Window_Options[_0x53c255(0x819)]['addGeneralOptions'],Window_Options[_0x53c255(0x819)]['addGeneralOptions']=function(){const _0x358d4c=_0x53c255;VisuMZ[_0x358d4c(0x2cc)][_0x358d4c(0x409)][_0x358d4c(0x6b9)](this),this[_0x358d4c(0x356)](),this[_0x358d4c(0x11b)]();},Window_Options['prototype'][_0x53c255(0x356)]=function(){const _0x4845af=_0x53c255;VisuMZ[_0x4845af(0x2cc)]['Settings']['AutoBattle'][_0x4845af(0x380)]&&(this['addBattleCoreAutoBattleStartupCommand'](),this[_0x4845af(0x6cd)]());},Window_Options['prototype'][_0x53c255(0x11b)]=function(){const _0x2a92a5=_0x53c255;if(!VisuMZ[_0x2a92a5(0x2cc)][_0x2a92a5(0x4b3)][_0x2a92a5(0x18f)]['AddHpGaugeOption'])return;const _0x59441c=TextManager[_0x2a92a5(0x7dd)],_0x2ca857=_0x2a92a5(0x7dd);this[_0x2a92a5(0x5cf)](_0x59441c,_0x2ca857);},Window_Options['prototype']['addBattleCoreAutoBattleStartupCommand']=function(){const _0x59c813=_0x53c255,_0x5d4867=TextManager[_0x59c813(0x37c)],_0x3ddba8=_0x59c813(0x674);this[_0x59c813(0x5cf)](_0x5d4867,_0x3ddba8);},Window_Options[_0x53c255(0x819)]['addBattleCoreAutoBattleStyleCommand']=function(){const _0x2f9737=_0x53c255,_0x4f634d=TextManager[_0x2f9737(0x575)],_0x518e0c=_0x2f9737(0x67d);this[_0x2f9737(0x5cf)](_0x4f634d,_0x518e0c);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x310)]=Window_Options['prototype']['statusText'],Window_Options['prototype']['statusText']=function(_0x3ca1ef){const _0x51f351=_0x53c255,_0x25fd04=this[_0x51f351(0x2b8)](_0x3ca1ef);return _0x25fd04===_0x51f351(0x67d)?this['statusTextAutoBattleStyle']():VisuMZ[_0x51f351(0x2cc)][_0x51f351(0x310)][_0x51f351(0x6b9)](this,_0x3ca1ef);},Window_Options[_0x53c255(0x819)][_0x53c255(0x852)]=function(){const _0x5001c9=_0x53c255,_0x3e7c43=VisuMZ['BattleCore'][_0x5001c9(0x4b3)][_0x5001c9(0x216)],_0x193144=this[_0x5001c9(0x215)]('autoBattleUseSkills');return _0x193144?_0x3e7c43[_0x5001c9(0x2b0)]:_0x3e7c43['StyleOFF'];},Window_ShopStatus[_0x53c255(0x819)][_0x53c255(0x6a3)]=function(){const _0x2db24b=_0x53c255,_0x47186d=DataManager[_0x2db24b(0x287)](this[_0x2db24b(0x4ec)]),_0x137b74=VisuMZ[_0x2db24b(0x560)][_0x47186d];if(!_0x137b74)return this[_0x2db24b(0x495)]();const _0x322738=_0x2db24b(0x3e5)[_0x2db24b(0x107)](this['_item'][_0x2db24b(0x600)]['type']),_0x2958e0=[null,TextManager['hp'],TextManager['mp'],TextManager['hp'],TextManager['mp'],TextManager['hp'],TextManager['mp']][this[_0x2db24b(0x4ec)][_0x2db24b(0x600)][_0x2db24b(0x3ce)]];return _0x137b74[_0x322738][_0x2db24b(0x107)](_0x2958e0);},Window_ShopStatus[_0x53c255(0x819)][_0x53c255(0x818)]=function(){const _0x4c0522=_0x53c255,_0x3b11d6=DataManager[_0x4c0522(0x287)](this[_0x4c0522(0x4ec)]),_0x4ce41a=VisuMZ[_0x4c0522(0x560)][_0x3b11d6];if(!_0x4ce41a)return this['getItemDamageAmountTextOriginal']();return _0x4ce41a['DamageDisplay'][_0x4c0522(0x6b9)](this);},VisuMZ['BattleCore'][_0x53c255(0x838)]=Window_PartyCommand[_0x53c255(0x819)][_0x53c255(0x854)],Window_PartyCommand[_0x53c255(0x819)][_0x53c255(0x854)]=function(_0x97b9f5){const _0x5f93b3=_0x53c255;VisuMZ['BattleCore']['Window_PartyCommand_initialize'][_0x5f93b3(0x6b9)](this,_0x97b9f5),this[_0x5f93b3(0x32b)](_0x97b9f5);},Window_PartyCommand[_0x53c255(0x819)][_0x53c255(0x32b)]=function(_0x36e526){const _0x50076a=_0x53c255,_0x26be09=new Rectangle(0x0,0x0,_0x36e526[_0x50076a(0x261)],_0x36e526[_0x50076a(0x6ab)]);this[_0x50076a(0xee)]=new Window_Base(_0x26be09),this[_0x50076a(0xee)][_0x50076a(0x398)]=0x0,this['addChild'](this[_0x50076a(0xee)]),this[_0x50076a(0x193)]();},Window_PartyCommand[_0x53c255(0x819)][_0x53c255(0x5b4)]=function(){const _0x1efff6=_0x53c255;Window_Command[_0x1efff6(0x819)][_0x1efff6(0x5b4)][_0x1efff6(0x6b9)](this);if(this['_commandNameWindow'])this['updateCommandNameWindow']();},Window_PartyCommand[_0x53c255(0x819)][_0x53c255(0x193)]=function(){const _0x2509d8=_0x53c255,_0x338c30=this[_0x2509d8(0xee)];_0x338c30[_0x2509d8(0x14d)]['clear']();const _0x3ad3b6=this[_0x2509d8(0x5f6)](this[_0x2509d8(0x568)]());if(_0x3ad3b6===_0x2509d8(0x191)&&this[_0x2509d8(0x71e)]()>0x0){const _0x46e6b2=this[_0x2509d8(0x3d2)](this['index']());let _0x5426ce=this[_0x2509d8(0x392)](this[_0x2509d8(0x568)]());_0x5426ce=_0x5426ce[_0x2509d8(0x3d6)](/\\I\[(\d+)\]/gi,''),_0x338c30[_0x2509d8(0x201)](),this[_0x2509d8(0x121)](_0x5426ce,_0x46e6b2),this[_0x2509d8(0x360)](_0x5426ce,_0x46e6b2),this[_0x2509d8(0x224)](_0x5426ce,_0x46e6b2);}},Window_PartyCommand[_0x53c255(0x819)][_0x53c255(0x121)]=function(_0x58ea70,_0x22e1a8){},Window_PartyCommand[_0x53c255(0x819)][_0x53c255(0x360)]=function(_0x39a6d1,_0x5ab170){const _0x3514a0=_0x53c255,_0x5d6676=this['_commandNameWindow'];_0x5d6676[_0x3514a0(0x375)](_0x39a6d1,0x0,_0x5ab170['y'],_0x5d6676['innerWidth'],'center');},Window_PartyCommand[_0x53c255(0x819)][_0x53c255(0x224)]=function(_0x1bce54,_0x19fa89){const _0x1b3909=_0x53c255,_0x19dfa1=this[_0x1b3909(0xee)],_0xd3a61d=$gameSystem['windowPadding'](),_0x333706=_0x19fa89['x']+Math[_0x1b3909(0x7f5)](_0x19fa89[_0x1b3909(0x261)]/0x2)+_0xd3a61d;_0x19dfa1['x']=_0x19dfa1[_0x1b3909(0x261)]/-0x2+_0x333706,_0x19dfa1['y']=Math[_0x1b3909(0x7f5)](_0x19fa89['height']/0x2);},Window_PartyCommand['prototype'][_0x53c255(0x6aa)]=function(){const _0x686f8e=_0x53c255;this[_0x686f8e(0x620)](),this['addAutoBattleCommand'](),this[_0x686f8e(0x14c)](),this['addOptionsCommand'](),this['addEscapeCommand']();},Window_PartyCommand['prototype'][_0x53c255(0x620)]=function(){const _0x211ac8=_0x53c255,_0x5f3a2b=this[_0x211ac8(0x28d)](),_0x379c2a=VisuMZ[_0x211ac8(0x2cc)]['Settings'][_0x211ac8(0x3d7)][_0x211ac8(0x3cf)],_0x29aba8=_0x5f3a2b===_0x211ac8(0x788)?TextManager[_0x211ac8(0x31a)]:_0x211ac8(0x7e3)[_0x211ac8(0x107)](_0x379c2a,TextManager[_0x211ac8(0x31a)]),_0x522a60=this[_0x211ac8(0x227)]();this[_0x211ac8(0x5cf)](_0x29aba8,'fight',_0x522a60);},Window_PartyCommand['prototype']['isFightCommandEnabled']=function(){return!![];},Window_PartyCommand[_0x53c255(0x819)][_0x53c255(0x155)]=function(){const _0x250811=_0x53c255;if(!this['isAutoBattleCommandAdded']())return;const _0x156312=this[_0x250811(0x28d)](),_0x2b9f8e=VisuMZ[_0x250811(0x2cc)][_0x250811(0x4b3)][_0x250811(0x3d7)][_0x250811(0x7c9)],_0x42f6d4=_0x156312===_0x250811(0x788)?TextManager[_0x250811(0x484)]:_0x250811(0x7e3)[_0x250811(0x107)](_0x2b9f8e,TextManager[_0x250811(0x484)]),_0x4544f5=this[_0x250811(0x4a1)]();this[_0x250811(0x5cf)](_0x42f6d4,_0x250811(0x484),_0x4544f5);},Window_PartyCommand[_0x53c255(0x819)][_0x53c255(0x7a8)]=function(){const _0x21c5d5=_0x53c255;return VisuMZ[_0x21c5d5(0x2cc)][_0x21c5d5(0x4b3)][_0x21c5d5(0x3d7)][_0x21c5d5(0x502)];},Window_PartyCommand[_0x53c255(0x819)][_0x53c255(0x4a1)]=function(){return!![];},Window_PartyCommand['prototype'][_0x53c255(0x14c)]=function(){},Window_PartyCommand[_0x53c255(0x819)][_0x53c255(0x2fa)]=function(){const _0x1ac96a=_0x53c255;if(!this[_0x1ac96a(0x281)]())return;const _0x196e08=this[_0x1ac96a(0x28d)](),_0x4e98ad=VisuMZ['BattleCore'][_0x1ac96a(0x4b3)][_0x1ac96a(0x3d7)]['CmdIconOptions'],_0x44916a=_0x196e08===_0x1ac96a(0x788)?TextManager['options']:_0x1ac96a(0x7e3)[_0x1ac96a(0x107)](_0x4e98ad,TextManager[_0x1ac96a(0x549)]),_0x1c2abe=this[_0x1ac96a(0x499)]();this[_0x1ac96a(0x5cf)](_0x44916a,_0x1ac96a(0x549),_0x1c2abe);},Window_PartyCommand[_0x53c255(0x819)][_0x53c255(0x281)]=function(){const _0x19abfd=_0x53c255;return VisuMZ[_0x19abfd(0x2cc)]['Settings'][_0x19abfd(0x3d7)][_0x19abfd(0x802)];},Window_PartyCommand[_0x53c255(0x819)][_0x53c255(0x499)]=function(){return!![];},Window_PartyCommand[_0x53c255(0x819)][_0x53c255(0x26b)]=function(){const _0x5d246b=_0x53c255,_0x45dc9e=this[_0x5d246b(0x28d)](),_0x17d865=VisuMZ[_0x5d246b(0x2cc)]['Settings']['PartyCmd']['CmdIconEscape'],_0x2536c9=_0x45dc9e==='text'?TextManager[_0x5d246b(0x2ba)]:_0x5d246b(0x7e3)[_0x5d246b(0x107)](_0x17d865,TextManager[_0x5d246b(0x2ba)]),_0x34bd7d=this[_0x5d246b(0x754)]();this[_0x5d246b(0x5cf)](_0x2536c9,_0x5d246b(0x2ba),_0x34bd7d);},Window_PartyCommand['prototype'][_0x53c255(0x754)]=function(){return BattleManager['canEscape']();},Window_PartyCommand[_0x53c255(0x819)][_0x53c255(0x830)]=function(){const _0x325b51=_0x53c255;return VisuMZ[_0x325b51(0x2cc)][_0x325b51(0x4b3)]['PartyCmd']['CmdTextAlign'];},Window_PartyCommand['prototype'][_0x53c255(0x5f1)]=function(_0x58167a){const _0x4d4d78=_0x53c255,_0x191c89=this['commandStyleCheck'](_0x58167a);if(_0x191c89===_0x4d4d78(0x7a5))this[_0x4d4d78(0x15d)](_0x58167a);else _0x191c89===_0x4d4d78(0x191)?this[_0x4d4d78(0x272)](_0x58167a):Window_Command['prototype'][_0x4d4d78(0x5f1)]['call'](this,_0x58167a);},Window_PartyCommand['prototype'][_0x53c255(0x28d)]=function(){const _0x2c7574=_0x53c255;return VisuMZ['BattleCore']['Settings']['PartyCmd'][_0x2c7574(0x374)];},Window_PartyCommand[_0x53c255(0x819)][_0x53c255(0x5f6)]=function(_0x1a3bdc){const _0x43365c=_0x53c255;if(_0x1a3bdc<0x0)return _0x43365c(0x788);const _0x1d62af=this[_0x43365c(0x28d)]();if(_0x1d62af!==_0x43365c(0x64d))return _0x1d62af;else{if(this[_0x43365c(0x71e)]()>0x0){const _0x2fbdf0=this[_0x43365c(0x392)](_0x1a3bdc);if(_0x2fbdf0['match'](/\\I\[(\d+)\]/i)){const _0x57554f=this['itemLineRect'](_0x1a3bdc),_0x363e9c=this[_0x43365c(0x6a5)](_0x2fbdf0)[_0x43365c(0x261)];return _0x363e9c<=_0x57554f[_0x43365c(0x261)]?'iconText':'icon';}}}return _0x43365c(0x788);},Window_PartyCommand[_0x53c255(0x819)][_0x53c255(0x15d)]=function(_0xb15441){const _0x22d8b3=_0x53c255,_0x51d9c6=this['itemLineRect'](_0xb15441),_0x440b25=this['commandName'](_0xb15441),_0x4c0d66=this['textSizeEx'](_0x440b25)[_0x22d8b3(0x261)];this[_0x22d8b3(0x10f)](this[_0x22d8b3(0x5c6)](_0xb15441));const _0x3d54e0=this[_0x22d8b3(0x830)]();if(_0x3d54e0===_0x22d8b3(0x513))this[_0x22d8b3(0x259)](_0x440b25,_0x51d9c6['x']+_0x51d9c6[_0x22d8b3(0x261)]-_0x4c0d66,_0x51d9c6['y'],_0x4c0d66);else{if(_0x3d54e0===_0x22d8b3(0x211)){const _0x5e6409=_0x51d9c6['x']+Math[_0x22d8b3(0x7f5)]((_0x51d9c6[_0x22d8b3(0x261)]-_0x4c0d66)/0x2);this['drawTextEx'](_0x440b25,_0x5e6409,_0x51d9c6['y'],_0x4c0d66);}else this[_0x22d8b3(0x259)](_0x440b25,_0x51d9c6['x'],_0x51d9c6['y'],_0x4c0d66);}},Window_PartyCommand[_0x53c255(0x819)][_0x53c255(0x272)]=function(_0x2f50e2){const _0x134dcf=_0x53c255;this[_0x134dcf(0x392)](_0x2f50e2)[_0x134dcf(0x473)](/\\I\[(\d+)\]/i);const _0x200326=Number(RegExp['$1'])||0x0,_0x1ff01d=this[_0x134dcf(0x3d2)](_0x2f50e2),_0x45e8eb=_0x1ff01d['x']+Math['floor']((_0x1ff01d['width']-ImageManager[_0x134dcf(0x142)])/0x2),_0x15b6b2=_0x1ff01d['y']+(_0x1ff01d[_0x134dcf(0x6ab)]-ImageManager[_0x134dcf(0x183)])/0x2;this['drawIcon'](_0x200326,_0x45e8eb,_0x15b6b2);},Window_PartyCommand[_0x53c255(0x819)][_0x53c255(0xdf)]=function(){},Window_PartyCommand[_0x53c255(0x819)][_0x53c255(0x5cd)]=function(){const _0x484ac0=_0x53c255;Window_Command['prototype'][_0x484ac0(0x5cd)][_0x484ac0(0x6b9)](this);const _0x4519ac=this[_0x484ac0(0x139)]();_0x4519ac===_0x484ac0(0x7a0)&&this[_0x484ac0(0x1af)]();},Window_PartyCommand['prototype'][_0x53c255(0x139)]=function(){const _0x73fd2a=_0x53c255;if(this[_0x73fd2a(0x647)])return this[_0x73fd2a(0x647)];return this[_0x73fd2a(0x647)]=SceneManager[_0x73fd2a(0x12f)][_0x73fd2a(0x139)](),this['_battleLayoutStyle'];},Window_PartyCommand[_0x53c255(0x819)][_0x53c255(0x84e)]=function(){const _0x465011=_0x53c255,_0x476c27=VisuMZ[_0x465011(0x2cc)][_0x465011(0x4b3)][_0x465011(0x3d7)],_0x4e096d=this[_0x465011(0x232)]();switch(_0x4e096d){case _0x465011(0x31a):this['_helpWindow'][_0x465011(0x4e7)](_0x476c27[_0x465011(0x610)]);break;case _0x465011(0x484):this['_helpWindow'][_0x465011(0x4e7)](_0x476c27[_0x465011(0x3f3)]);break;case _0x465011(0x549):this['_helpWindow'][_0x465011(0x4e7)](_0x476c27['HelpOptions']);break;case _0x465011(0x2ba):this[_0x465011(0x5eb)][_0x465011(0x4e7)](_0x476c27[_0x465011(0x385)]);break;default:this[_0x465011(0x5eb)][_0x465011(0x4e7)]('');break;}},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x6c9)]=Window_ActorCommand['prototype'][_0x53c255(0x854)],Window_ActorCommand[_0x53c255(0x819)][_0x53c255(0x854)]=function(_0x1f2bbd){const _0x2f3b54=_0x53c255;VisuMZ[_0x2f3b54(0x2cc)][_0x2f3b54(0x6c9)][_0x2f3b54(0x6b9)](this,_0x1f2bbd),this[_0x2f3b54(0x32b)](_0x1f2bbd);},Window_ActorCommand[_0x53c255(0x819)]['createCommandNameWindow']=function(_0x3662d6){const _0x24f514=_0x53c255,_0x4cea7d=new Rectangle(0x0,0x0,_0x3662d6['width'],_0x3662d6[_0x24f514(0x6ab)]);this[_0x24f514(0xee)]=new Window_Base(_0x4cea7d),this['_commandNameWindow'][_0x24f514(0x398)]=0x0,this[_0x24f514(0x62f)](this['_commandNameWindow']),this['updateCommandNameWindow']();},Window_ActorCommand['prototype'][_0x53c255(0x5b4)]=function(){const _0x32e7da=_0x53c255;Window_Command[_0x32e7da(0x819)][_0x32e7da(0x5b4)][_0x32e7da(0x6b9)](this);if(this[_0x32e7da(0xee)])this['updateCommandNameWindow']();},Window_ActorCommand[_0x53c255(0x819)][_0x53c255(0x193)]=function(){const _0x359e5a=_0x53c255,_0x5a7b9d=this[_0x359e5a(0xee)];_0x5a7b9d['contents']['clear']();const _0x451dac=this['commandStyleCheck'](this[_0x359e5a(0x568)]());if(_0x451dac===_0x359e5a(0x191)&&this[_0x359e5a(0x71e)]()>0x0){const _0x40d498=this[_0x359e5a(0x3d2)](this[_0x359e5a(0x568)]());let _0x417f52=this[_0x359e5a(0x392)](this[_0x359e5a(0x568)]());_0x417f52=_0x417f52[_0x359e5a(0x3d6)](/\\I\[(\d+)\]/gi,''),_0x5a7b9d[_0x359e5a(0x201)](),this[_0x359e5a(0x121)](_0x417f52,_0x40d498),this[_0x359e5a(0x360)](_0x417f52,_0x40d498),this[_0x359e5a(0x224)](_0x417f52,_0x40d498);}},Window_ActorCommand['prototype']['commandNameWindowDrawBackground']=function(_0x1d619d,_0x1c3b13){},Window_ActorCommand[_0x53c255(0x819)][_0x53c255(0x360)]=function(_0x3b8f4c,_0x12916e){const _0x2419b5=_0x53c255,_0x2d996d=this['_commandNameWindow'];_0x2d996d[_0x2419b5(0x375)](_0x3b8f4c,0x0,_0x12916e['y'],_0x2d996d[_0x2419b5(0x6d2)],_0x2419b5(0x211));},Window_ActorCommand[_0x53c255(0x819)]['commandNameWindowCenter']=function(_0x7e9443,_0x53a7a9){const _0x4b07e6=_0x53c255,_0x1c75a8=this[_0x4b07e6(0xee)],_0x1293ff=$gameSystem[_0x4b07e6(0x382)](),_0xccf49=_0x53a7a9['x']+Math[_0x4b07e6(0x7f5)](_0x53a7a9[_0x4b07e6(0x261)]/0x2)+_0x1293ff;_0x1c75a8['x']=_0x1c75a8[_0x4b07e6(0x261)]/-0x2+_0xccf49,_0x1c75a8['y']=Math[_0x4b07e6(0x7f5)](_0x53a7a9[_0x4b07e6(0x6ab)]/0x2);},Window_ActorCommand[_0x53c255(0x819)]['makeCommandList']=function(){const _0x17a6b1=_0x53c255;if(!this[_0x17a6b1(0x56e)])return;const _0x2caf32=this[_0x17a6b1(0x56e)][_0x17a6b1(0x5fb)]();for(const _0x41b6e5 of _0x2caf32){this[_0x17a6b1(0x831)](_0x41b6e5[_0x17a6b1(0x61d)]()[_0x17a6b1(0x615)]());}},Window_ActorCommand[_0x53c255(0x819)][_0x53c255(0x831)]=function(_0x1138bf){const _0x274433=_0x53c255;_0x1138bf==='ATTACK'&&this[_0x274433(0x826)]();['STYPES',_0x274433(0x1f5)][_0x274433(0x6a6)](_0x1138bf)&&this[_0x274433(0x1ad)]();_0x1138bf==='GUARD'&&this[_0x274433(0x5e8)]();_0x1138bf===_0x274433(0x2c5)&&this[_0x274433(0x58a)]();_0x1138bf==='ESCAPE'&&this[_0x274433(0x26b)]();_0x1138bf===_0x274433(0x2a7)&&this[_0x274433(0x155)]();if(_0x1138bf['match'](/STYPE: (\d+)/i)){const _0x50bb79=Number(RegExp['$1']);this['addSkillTypeCommand'](_0x50bb79);}else{if(_0x1138bf[_0x274433(0x473)](/STYPE: (.*)/i)){const _0x4fceb4=DataManager[_0x274433(0x6b3)](RegExp['$1']);this[_0x274433(0xe1)](_0x4fceb4);}}_0x1138bf===_0x274433(0x3ac)&&this[_0x274433(0x472)]();if(_0x1138bf['match'](/SKILL: (\d+)/i)){const _0x4d9fa1=Number(RegExp['$1']);this['addSingleSkillCommand']($dataSkills[_0x4d9fa1]);}else{if(_0x1138bf[_0x274433(0x473)](/SKILL: (.*)/i)){const _0x4843db=DataManager['getSkillIdWithName'](RegExp['$1']);this[_0x274433(0x775)]($dataSkills[_0x4843db]);}}_0x1138bf===_0x274433(0x7c3)&&Imported[_0x274433(0x834)]&&this['addPartyCommand'](),[_0x274433(0x27f),_0x274433(0x70d)]['includes'](_0x1138bf)&&Imported[_0x274433(0x555)]&&this['addCombatLogCommand']();},Window_ActorCommand[_0x53c255(0x819)][_0x53c255(0x826)]=function(){const _0x836f17=_0x53c255,_0x3aaeee=$dataSkills[this[_0x836f17(0x56e)]['attackSkillId']()];if(!_0x3aaeee)return;if(!this[_0x836f17(0x7c4)](_0x3aaeee))return;const _0x20173a=this[_0x836f17(0x28d)](),_0x3971ef=DataManager[_0x836f17(0x101)](_0x3aaeee),_0x320319=DataManager['battleCommandIcon'](_0x3aaeee),_0x90af64=_0x20173a===_0x836f17(0x788)?_0x3971ef:_0x836f17(0x7e3)[_0x836f17(0x107)](_0x320319,_0x3971ef);this[_0x836f17(0x5cf)](_0x90af64,_0x836f17(0x48f),this['_actor'][_0x836f17(0x6be)]());},Window_ActorCommand[_0x53c255(0x819)][_0x53c255(0x5e8)]=function(){const _0x4d2071=_0x53c255,_0x53037a=$dataSkills[this[_0x4d2071(0x56e)]['guardSkillId']()];if(!_0x53037a)return;if(!this[_0x4d2071(0x7c4)](_0x53037a))return;const _0x30afc3=this[_0x4d2071(0x28d)](),_0x286697=DataManager[_0x4d2071(0x101)](_0x53037a),_0x19b3d2=DataManager[_0x4d2071(0x1c2)](_0x53037a),_0x230360=_0x30afc3===_0x4d2071(0x788)?_0x286697:_0x4d2071(0x7e3)[_0x4d2071(0x107)](_0x19b3d2,_0x286697);this[_0x4d2071(0x5cf)](_0x230360,'guard',this[_0x4d2071(0x56e)][_0x4d2071(0x51c)]());},Window_ActorCommand[_0x53c255(0x819)][_0x53c255(0x58a)]=function(){const _0x2cf527=_0x53c255,_0x2f4d2c=this[_0x2cf527(0x28d)](),_0x5c62be=VisuMZ['BattleCore'][_0x2cf527(0x4b3)][_0x2cf527(0x31b)]['CmdIconItem'],_0xadbb5d=_0x2f4d2c===_0x2cf527(0x788)?TextManager[_0x2cf527(0x4b7)]:'\x5cI[%1]%2'['format'](_0x5c62be,TextManager[_0x2cf527(0x4b7)]),_0x1eaec5=this[_0x2cf527(0x5ae)]();this[_0x2cf527(0x5cf)](_0xadbb5d,_0x2cf527(0x4b7),_0x1eaec5);},Window_ActorCommand[_0x53c255(0x819)][_0x53c255(0x5ae)]=function(){const _0x3a4f55=_0x53c255;return this[_0x3a4f55(0x56e)]&&this[_0x3a4f55(0x56e)][_0x3a4f55(0x747)]();},Window_ActorCommand[_0x53c255(0x819)][_0x53c255(0x1ad)]=function(){const _0x2f7bf3=_0x53c255,_0x5f3aba=this[_0x2f7bf3(0x56e)][_0x2f7bf3(0x761)]();for(const _0x51a5e2 of _0x5f3aba){this['addSkillTypeCommand'](_0x51a5e2);}},Window_ActorCommand['prototype']['addSkillTypeCommand']=function(_0x645c8c){const _0x39acd8=_0x53c255;let _0x124ad9=$dataSystem['skillTypes'][_0x645c8c];if(!_0x124ad9)return;let _0x544989=_0x124ad9;const _0x2030c9=this['commandStyle']();if(_0x2030c9===_0x39acd8(0x788))_0x544989=_0x544989[_0x39acd8(0x3d6)](/\x1I\[(\d+)\]/gi,''),_0x544989=_0x544989[_0x39acd8(0x3d6)](/\\I\[(\d+)\]/gi,'');else{if(!_0x124ad9['match'](/\\I\[(\d+)\]/i)){const _0x38a507=Imported[_0x39acd8(0x58c)]?VisuMZ['SkillsStatesCore'][_0x39acd8(0x4b3)][_0x39acd8(0x744)]:VisuMZ[_0x39acd8(0x2cc)][_0x39acd8(0x4b3)][_0x39acd8(0x31b)],_0x529799=$dataSystem[_0x39acd8(0x465)][_0x39acd8(0x6a6)](_0x645c8c),_0x2aeec5=_0x529799?_0x38a507[_0x39acd8(0x240)]:_0x38a507[_0x39acd8(0x4ae)];_0x544989=_0x39acd8(0x7e3)[_0x39acd8(0x107)](_0x2aeec5,_0x124ad9);}}this[_0x39acd8(0x5cf)](_0x544989,_0x39acd8(0x3b8),!![],_0x645c8c);},Window_ActorCommand[_0x53c255(0x819)]['addSingleSkillCommands']=function(){const _0x505c8e=_0x53c255,_0x518c00=this[_0x505c8e(0x56e)][_0x505c8e(0x761)](),_0x30e32d=this[_0x505c8e(0x56e)][_0x505c8e(0x736)]();for(const _0x224500 of _0x30e32d){if(!_0x224500)continue;if(Imported[_0x505c8e(0x58c)]){if(this[_0x505c8e(0x263)](_0x224500))continue;if(this[_0x505c8e(0x1c6)](_0x224500))continue;}else{if(!_0x518c00[_0x505c8e(0x6a6)](_0x224500[_0x505c8e(0x6b1)]))continue;}this[_0x505c8e(0x775)](_0x224500);}},Window_ActorCommand[_0x53c255(0x819)]['getSimilarSTypes']=function(_0x5652b4){const _0x494c4e=_0x53c255,_0x5577a8=this[_0x494c4e(0x56e)][_0x494c4e(0x761)](),_0x46883c=_0x5577a8['filter'](_0x2d618b=>DataManager[_0x494c4e(0x322)](_0x5652b4)[_0x494c4e(0x6a6)](_0x2d618b));return _0x46883c[_0x494c4e(0x343)]<=0x0;},Window_ActorCommand[_0x53c255(0x819)][_0x53c255(0x1c6)]=function(_0x16f61f){const _0x2ffc12=_0x53c255;if(!Window_SkillList[_0x2ffc12(0x819)][_0x2ffc12(0x3b6)][_0x2ffc12(0x6b9)](this,_0x16f61f))return!![];if(!Window_SkillList[_0x2ffc12(0x819)][_0x2ffc12(0x642)][_0x2ffc12(0x6b9)](this,_0x16f61f))return!![];if(!Window_SkillList[_0x2ffc12(0x819)][_0x2ffc12(0x511)]['call'](this,_0x16f61f))return!![];return![];},Window_ActorCommand[_0x53c255(0x819)][_0x53c255(0x775)]=function(_0x268043){const _0x1bc09d=_0x53c255;if(!_0x268043)return;if(!this['canAddSkillCommand'](_0x268043))return;const _0x21d416=this[_0x1bc09d(0x28d)](),_0x44f8a5=DataManager[_0x1bc09d(0x101)](_0x268043),_0x140630=DataManager['battleCommandIcon'](_0x268043),_0x333598=_0x21d416==='text'?_0x44f8a5:'\x5cI[%1]%2'[_0x1bc09d(0x107)](_0x140630,_0x44f8a5),_0x224ff4=this[_0x1bc09d(0x56e)]['canUse'](_0x268043);this['addCommand'](_0x333598,'singleSkill',_0x224ff4,_0x268043['id']);},Window_ActorCommand['prototype']['canAddSkillCommand']=function(_0x35b44f){const _0x359fb2=_0x53c255,_0x38c635=_0x35b44f[_0x359fb2(0x236)];if(_0x38c635[_0x359fb2(0x473)](/<COMMAND REQUIRE LEARN>/i)){if(!this[_0x359fb2(0x56e)][_0x359fb2(0x45d)](_0x35b44f['id']))return![];}if(_0x38c635[_0x359fb2(0x473)](/<COMMAND REQUIRE ACCESS>/i)){if(!this[_0x359fb2(0x56e)][_0x359fb2(0x651)](_0x35b44f['id']))return![];}const _0x3e04d9=VisuMZ[_0x359fb2(0x2cc)]['createKeyJS'](_0x35b44f,_0x359fb2(0x414));if(VisuMZ[_0x359fb2(0x2cc)]['JS'][_0x3e04d9]){if(!VisuMZ['BattleCore']['JS'][_0x3e04d9][_0x359fb2(0x6b9)](this,this[_0x359fb2(0x56e)],_0x35b44f))return![];}return VisuMZ[_0x359fb2(0x2cc)][_0x359fb2(0x488)](_0x35b44f);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x488)]=function(_0x264bff){const _0x279f54=_0x53c255,_0x475ad8=_0x264bff['note'];if(_0x475ad8['match'](/<COMMAND SHOW[ ](?:SW|SWITCH|SWITCHES):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x1ebc4b=JSON[_0x279f54(0x202)]('['+RegExp['$1'][_0x279f54(0x473)](/\d+/g)+']');for(const _0x1d3a88 of _0x1ebc4b){if(!$gameSwitches[_0x279f54(0x24b)](_0x1d3a88))return![];}return!![];}if(_0x475ad8[_0x279f54(0x473)](/<COMMAND SHOW ALL[ ](?:SW|SWITCH|SWITCHES):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0xf648b7=JSON[_0x279f54(0x202)]('['+RegExp['$1'][_0x279f54(0x473)](/\d+/g)+']');for(const _0x1bb2af of _0xf648b7){if(!$gameSwitches[_0x279f54(0x24b)](_0x1bb2af))return![];}return!![];}if(_0x475ad8[_0x279f54(0x473)](/<COMMAND SHOW ANY[ ](?:SW|SWITCH|SWITCHES):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x2327f8=JSON[_0x279f54(0x202)]('['+RegExp['$1'][_0x279f54(0x473)](/\d+/g)+']');for(const _0x724766 of _0x2327f8){if($gameSwitches[_0x279f54(0x24b)](_0x724766))return!![];}return![];}if(_0x475ad8[_0x279f54(0x473)](/<COMMAND HIDE[ ](?:SW|SWITCH|SWITCHES):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x5b8cd7=JSON[_0x279f54(0x202)]('['+RegExp['$1'][_0x279f54(0x473)](/\d+/g)+']');for(const _0x77df19 of _0x5b8cd7){if(!$gameSwitches[_0x279f54(0x24b)](_0x77df19))return!![];}return![];}if(_0x475ad8[_0x279f54(0x473)](/<COMMAND HIDE ALL[ ](?:SW|SWITCH|SWITCHES):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0xf79e49=JSON[_0x279f54(0x202)]('['+RegExp['$1']['match'](/\d+/g)+']');for(const _0x495372 of _0xf79e49){if(!$gameSwitches[_0x279f54(0x24b)](_0x495372))return!![];}return![];}if(_0x475ad8['match'](/<COMMAND HIDE ANY[ ](?:SW|SWITCH|SWITCHES):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x2f093b=JSON[_0x279f54(0x202)]('['+RegExp['$1'][_0x279f54(0x473)](/\d+/g)+']');for(const _0x47edc7 of _0x2f093b){if($gameSwitches[_0x279f54(0x24b)](_0x47edc7))return![];}return!![];}return!![];},Window_ActorCommand[_0x53c255(0x819)]['addEscapeCommand']=function(){const _0x257ddb=_0x53c255,_0x36c33c=this[_0x257ddb(0x28d)](),_0x125769=VisuMZ['BattleCore'][_0x257ddb(0x4b3)]['PartyCmd'][_0x257ddb(0x60b)],_0x288014=_0x36c33c==='text'?TextManager[_0x257ddb(0x2ba)]:_0x257ddb(0x7e3)[_0x257ddb(0x107)](_0x125769,TextManager[_0x257ddb(0x2ba)]),_0x5d0151=this['isEscapeCommandEnabled']();this['addCommand'](_0x288014,_0x257ddb(0x2ba),_0x5d0151);},Window_ActorCommand[_0x53c255(0x819)]['isEscapeCommandEnabled']=function(){const _0x1a73e7=_0x53c255;return BattleManager[_0x1a73e7(0x471)]();},Window_ActorCommand[_0x53c255(0x819)][_0x53c255(0x155)]=function(){const _0x14626f=_0x53c255,_0x3918ff=this[_0x14626f(0x28d)](),_0x14c2d8=VisuMZ['BattleCore'][_0x14626f(0x4b3)][_0x14626f(0x3d7)][_0x14626f(0x7c9)],_0x5098ca=_0x3918ff==='text'?TextManager['autoBattle']:_0x14626f(0x7e3)[_0x14626f(0x107)](_0x14c2d8,TextManager['autoBattle']),_0xe121d0=this[_0x14626f(0x4a1)]();this['addCommand'](_0x5098ca,_0x14626f(0x484),_0xe121d0);},Window_ActorCommand[_0x53c255(0x819)]['isAutoBattleCommandEnabled']=function(){return!![];},Window_ActorCommand['prototype']['itemTextAlign']=function(){const _0x15c6f9=_0x53c255;return VisuMZ['BattleCore'][_0x15c6f9(0x4b3)][_0x15c6f9(0x31b)][_0x15c6f9(0x35a)];},Window_ActorCommand['prototype'][_0x53c255(0x5f1)]=function(_0x28ec82){const _0x53c49d=_0x53c255,_0x2c6b8b=this[_0x53c49d(0x5f6)](_0x28ec82);if(_0x2c6b8b===_0x53c49d(0x7a5))this[_0x53c49d(0x15d)](_0x28ec82);else _0x2c6b8b==='icon'?this[_0x53c49d(0x272)](_0x28ec82):Window_Command[_0x53c49d(0x819)][_0x53c49d(0x5f1)][_0x53c49d(0x6b9)](this,_0x28ec82);this[_0x53c49d(0x62d)](_0x28ec82);},Window_ActorCommand[_0x53c255(0x819)]['commandStyle']=function(){const _0x68ea0a=_0x53c255;return VisuMZ[_0x68ea0a(0x2cc)][_0x68ea0a(0x4b3)][_0x68ea0a(0x31b)]['CmdStyle'];},Window_ActorCommand[_0x53c255(0x819)][_0x53c255(0x5f6)]=function(_0x25e9d0){const _0x3e8ce7=_0x53c255;if(_0x25e9d0<0x0)return _0x3e8ce7(0x788);const _0x5df738=this['commandStyle']();if(_0x5df738!==_0x3e8ce7(0x64d))return _0x5df738;else{if(this['maxItems']()>0x0){const _0x209fef=this[_0x3e8ce7(0x392)](_0x25e9d0);if(_0x209fef[_0x3e8ce7(0x473)](/\\I\[(\d+)\]/i)){const _0x7a65f5=this[_0x3e8ce7(0x3d2)](_0x25e9d0),_0x5c466f=this['textSizeEx'](_0x209fef)['width'];return _0x5c466f<=_0x7a65f5[_0x3e8ce7(0x261)]?'iconText':_0x3e8ce7(0x191);}}}return _0x3e8ce7(0x788);},Window_ActorCommand[_0x53c255(0x819)][_0x53c255(0x15d)]=function(_0xe81431){const _0x403e3a=_0x53c255,_0x28787d=this[_0x403e3a(0x3d2)](_0xe81431),_0x573a0f=this['commandName'](_0xe81431),_0x5d9d88=this['textSizeEx'](_0x573a0f)[_0x403e3a(0x261)];this[_0x403e3a(0x10f)](this['isCommandEnabled'](_0xe81431));const _0x524d8e=this['itemTextAlign']();if(_0x524d8e===_0x403e3a(0x513))this[_0x403e3a(0x259)](_0x573a0f,_0x28787d['x']+_0x28787d[_0x403e3a(0x261)]-_0x5d9d88,_0x28787d['y'],_0x5d9d88);else{if(_0x524d8e===_0x403e3a(0x211)){const _0x1e9797=_0x28787d['x']+Math[_0x403e3a(0x7f5)]((_0x28787d['width']-_0x5d9d88)/0x2);this['drawTextEx'](_0x573a0f,_0x1e9797,_0x28787d['y'],_0x5d9d88);}else this['drawTextEx'](_0x573a0f,_0x28787d['x'],_0x28787d['y'],_0x5d9d88);}},Window_ActorCommand[_0x53c255(0x819)][_0x53c255(0x272)]=function(_0x1175f3){const _0x1f63fd=_0x53c255;this['commandName'](_0x1175f3)[_0x1f63fd(0x473)](/\\I\[(\d+)\]/i);const _0x3e075b=Number(RegExp['$1'])||0x0,_0x32a75f=this[_0x1f63fd(0x3d2)](_0x1175f3),_0x231f91=_0x32a75f['x']+Math[_0x1f63fd(0x7f5)]((_0x32a75f[_0x1f63fd(0x261)]-ImageManager[_0x1f63fd(0x142)])/0x2),_0x207529=_0x32a75f['y']+(_0x32a75f[_0x1f63fd(0x6ab)]-ImageManager['iconHeight'])/0x2;this[_0x1f63fd(0x4e2)](_0x3e075b,_0x231f91,_0x207529);},Window_ActorCommand['prototype']['drawSingleSkillCost']=function(_0x73db8e){const _0x2c2ccb=_0x53c255,_0x801936=this[_0x2c2ccb(0x2b8)](_0x73db8e);if(![_0x2c2ccb(0x48f),_0x2c2ccb(0x59a),_0x2c2ccb(0x6cb)][_0x2c2ccb(0x6a6)](_0x801936))return;const _0x313ece=this[_0x2c2ccb(0x3d2)](_0x73db8e);let _0x1bf582=null;if(_0x801936===_0x2c2ccb(0x48f))_0x1bf582=$dataSkills[this['_actor'][_0x2c2ccb(0x74c)]()];else _0x801936===_0x2c2ccb(0x59a)?_0x1bf582=$dataSkills[this[_0x2c2ccb(0x56e)][_0x2c2ccb(0x7dc)]()]:_0x1bf582=$dataSkills[this[_0x2c2ccb(0x73b)][_0x73db8e][_0x2c2ccb(0x428)]];this[_0x2c2ccb(0x7b0)](this[_0x2c2ccb(0x56e)],_0x1bf582,_0x313ece['x'],_0x313ece['y'],_0x313ece[_0x2c2ccb(0x261)]);},Window_ActorCommand['prototype'][_0x53c255(0x7b0)]=function(_0x311c8d,_0x4c7ab4,_0x304012,_0x500afe,_0x5f47e2){const _0x407513=_0x53c255;if(!_0x4c7ab4)return;Imported['VisuMZ_1_SkillsStatesCore']?Window_Command[_0x407513(0x819)][_0x407513(0x7b0)][_0x407513(0x6b9)](this,_0x311c8d,_0x4c7ab4,_0x304012,_0x500afe,_0x5f47e2):Window_SkillList['prototype'][_0x407513(0x7b0)]['call'](this,_0x4c7ab4,_0x304012,_0x500afe,_0x5f47e2);},Window_ActorCommand['prototype'][_0x53c255(0xdf)]=function(){},Window_ActorCommand[_0x53c255(0x819)]['activate']=function(){const _0x68fbf3=_0x53c255;Window_Command[_0x68fbf3(0x819)][_0x68fbf3(0x5cd)][_0x68fbf3(0x6b9)](this);const _0x96dddd=this[_0x68fbf3(0x139)]();_0x96dddd===_0x68fbf3(0x7a0)&&this['showHelpWindow']();},Window_ActorCommand['prototype'][_0x53c255(0x139)]=function(){const _0x336c37=_0x53c255;if(this[_0x336c37(0x647)])return this['_battleLayoutStyle'];return this[_0x336c37(0x647)]=SceneManager[_0x336c37(0x12f)][_0x336c37(0x139)](),this[_0x336c37(0x647)];},VisuMZ['BattleCore']['Window_ActorCommand_setup']=Window_ActorCommand['prototype'][_0x53c255(0x157)],Window_ActorCommand[_0x53c255(0x819)][_0x53c255(0x157)]=function(_0xc2031b){const _0x529e59=_0x53c255,_0x1fe665=this[_0x529e59(0x139)]();if(_0xc2031b&&['xp',_0x529e59(0x6bf)][_0x529e59(0x6a6)](_0x1fe665))this[_0x529e59(0x251)](_0xc2031b);else _0xc2031b&&[_0x529e59(0x7a0)][_0x529e59(0x6a6)](_0x1fe665)&&(this[_0x529e59(0x5ff)](_0xc2031b),this[_0x529e59(0x1af)]());VisuMZ['BattleCore'][_0x529e59(0x252)][_0x529e59(0x6b9)](this,_0xc2031b),_0xc2031b&&$gameTroop['aliveMembers']()[_0x529e59(0x343)]>0x0&&_0xc2031b[_0x529e59(0x5ef)]()&&_0xc2031b[_0x529e59(0x5ef)]()[_0x529e59(0x2aa)]();},Window_ActorCommand[_0x53c255(0x819)][_0x53c255(0x251)]=function(_0x1759d1){const _0x1d4f1e=_0x53c255,_0x2113a3=Math[_0x1d4f1e(0x6c4)](Graphics[_0x1d4f1e(0x32c)]/0x3),_0x1b3120=Math[_0x1d4f1e(0x6c4)](Graphics[_0x1d4f1e(0x32c)]/$gameParty[_0x1d4f1e(0x116)]()['length']),_0xb0a511=Math[_0x1d4f1e(0x413)](_0x2113a3,_0x1b3120),_0x6ab132=this[_0x1d4f1e(0x10d)](VisuMZ[_0x1d4f1e(0x2cc)]['Settings']['BattleLayout'][_0x1d4f1e(0x1e4)]),_0x52cbf0=_0x1b3120*_0x1759d1['index']()+(_0x1b3120-_0xb0a511)/0x2,_0x509f1a=SceneManager[_0x1d4f1e(0x12f)]['_statusWindow']['y']-_0x6ab132;this[_0x1d4f1e(0x13f)](_0x52cbf0,_0x509f1a,_0xb0a511,_0x6ab132),this[_0x1d4f1e(0x668)](),this['setBackgroundType'](0x1);},Window_ActorCommand[_0x53c255(0x819)][_0x53c255(0x5ff)]=function(_0x43c1a5){const _0x30e2ce=_0x53c255,_0x5af95a=SceneManager[_0x30e2ce(0x12f)][_0x30e2ce(0x136)]();this['move'](_0x5af95a['x'],_0x5af95a['y'],_0x5af95a[_0x30e2ce(0x261)],_0x5af95a[_0x30e2ce(0x6ab)]),this['createContents'](),this[_0x30e2ce(0x1bb)](0x0);},Window_ActorCommand[_0x53c255(0x819)][_0x53c255(0x1b0)]=function(){const _0x5e1f2b=_0x53c255;if(this[_0x5e1f2b(0x684)]){const _0x3e3751=this[_0x5e1f2b(0x684)][_0x5e1f2b(0x657)],_0x13846e=this[_0x5e1f2b(0x261)]-0x8,_0x3a20ac=this[_0x5e1f2b(0x6ab)],_0x27385a=this['padding'],_0x538e2e=ColorManager[_0x5e1f2b(0x73e)](),_0x5a621f=ColorManager[_0x5e1f2b(0x659)]();this[_0x5e1f2b(0x684)]['x']=0x4,_0x3e3751[_0x5e1f2b(0x6ba)](_0x13846e,_0x3a20ac),_0x3e3751[_0x5e1f2b(0x602)](0x0,0x0,_0x13846e,_0x27385a,_0x5a621f,_0x538e2e,!![]),_0x3e3751[_0x5e1f2b(0x441)](0x0,_0x27385a,_0x13846e,_0x3a20ac-_0x27385a*0x2,_0x538e2e),_0x3e3751[_0x5e1f2b(0x602)](0x0,_0x3a20ac-_0x27385a,_0x13846e,_0x27385a,_0x538e2e,_0x5a621f,!![]),this[_0x5e1f2b(0x684)][_0x5e1f2b(0x411)](0x0,0x0,_0x13846e,_0x3a20ac);}},Window_ActorCommand[_0x53c255(0x819)][_0x53c255(0x84e)]=function(){const _0x25a490=_0x53c255;if(!this['_actor'])return;const _0x27f897=VisuMZ[_0x25a490(0x2cc)][_0x25a490(0x4b3)][_0x25a490(0x31b)],_0x2dc206=this['currentSymbol']();switch(_0x2dc206){case _0x25a490(0x48f):this[_0x25a490(0x743)]($dataSkills[this[_0x25a490(0x56e)][_0x25a490(0x74c)]()]);break;case'guard':this['setHelpWindowItem']($dataSkills[this[_0x25a490(0x56e)][_0x25a490(0x7dc)]()]);break;case _0x25a490(0x3b8):const _0x12ce67=_0x27f897['HelpSkillType'],_0x4ebb8f=_0x12ce67[_0x25a490(0x107)]($dataSystem[_0x25a490(0x761)][this[_0x25a490(0x723)]()]);this[_0x25a490(0x5eb)][_0x25a490(0x4e7)](_0x4ebb8f);break;case _0x25a490(0x6cb):this['setHelpWindowItem']($dataSkills[this[_0x25a490(0x723)]()]);break;case _0x25a490(0x4b7):this['_helpWindow'][_0x25a490(0x4e7)](_0x27f897[_0x25a490(0x185)]);break;case'escape':this['_helpWindow'][_0x25a490(0x4e7)](_0x27f897[_0x25a490(0x385)]);break;case'autoBattle':this[_0x25a490(0x5eb)][_0x25a490(0x4e7)](_0x27f897[_0x25a490(0x3f3)]);break;default:this[_0x25a490(0x5eb)][_0x25a490(0x4e7)]('');break;}},VisuMZ['BattleCore'][_0x53c255(0x324)]=Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x854)],Window_BattleStatus['prototype'][_0x53c255(0x854)]=function(_0x185e21){const _0x21fcdc=_0x53c255;VisuMZ[_0x21fcdc(0x2cc)]['Window_BattleStatus_initialize']['call'](this,_0x185e21),this['initBattleCore']();},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x311)]=function(){const _0x184447=_0x53c255;this[_0x184447(0x162)]=this[_0x184447(0x724)]();},Window_BattleStatus[_0x53c255(0x819)]['battleLayoutStyle']=function(){const _0x425ab9=_0x53c255;if(this[_0x425ab9(0x647)])return this[_0x425ab9(0x647)];return this['_battleLayoutStyle']=SceneManager[_0x425ab9(0x12f)][_0x425ab9(0x139)](),this[_0x425ab9(0x647)];},Window_BattleStatus[_0x53c255(0x819)]['isFrameVisible']=function(){const _0x23732a=_0x53c255,_0x176236=this[_0x23732a(0x139)]();switch(_0x176236){case'list':case'border':return!![];break;case _0x23732a(0x1cf):case'xp':case _0x23732a(0x6bf):default:return![];break;}},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x637)]=function(){const _0x508059=_0x53c255;return this[_0x508059(0x724)]()?0x0:0xa;},Window_BattleStatus['prototype'][_0x53c255(0x15a)]=function(){const _0x2e36cd=_0x53c255,_0x2998e1=this[_0x2e36cd(0x139)]();switch(_0x2998e1){case _0x2e36cd(0x4a9):return 0x1;break;case'xp':case _0x2e36cd(0x6bf):return $gameParty[_0x2e36cd(0x116)]()[_0x2e36cd(0x343)];break;case _0x2e36cd(0x1cf):default:return $gameParty[_0x2e36cd(0x35c)]();break;}},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x2dc)]=function(){const _0x269f3e=_0x53c255,_0x22673a=this[_0x269f3e(0x139)]();switch(_0x22673a){case _0x269f3e(0x4a9):return Window_StatusBase[_0x269f3e(0x819)][_0x269f3e(0x2dc)][_0x269f3e(0x6b9)](this);break;case'default':case'xp':case'portrait':default:return this[_0x269f3e(0x77e)];break;}},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x244)]=function(){const _0x5709eb=_0x53c255,_0x1473f4=this['battleLayoutStyle']();switch(_0x1473f4){case _0x5709eb(0x4a9):return Window_StatusBase[_0x5709eb(0x819)][_0x5709eb(0x244)][_0x5709eb(0x6b9)](this);break;case _0x5709eb(0x1cf):case'xp':case'portrait':default:return 0x0;break;}},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x2ae)]=function(){const _0x272ee9=_0x53c255;this[_0x272ee9(0x724)]()?Window_StatusBase[_0x272ee9(0x819)][_0x272ee9(0x2ae)][_0x272ee9(0x6b9)](this):this[_0x272ee9(0x6b6)]=0x8;},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x773)]=function(){this['_requestRefresh']=!![];},Window_BattleStatus[_0x53c255(0x819)]['update']=function(){const _0x4ec3eb=_0x53c255;Window_StatusBase[_0x4ec3eb(0x819)][_0x4ec3eb(0x3ed)][_0x4ec3eb(0x6b9)](this),this['updateRefresh'](),this[_0x4ec3eb(0x5b0)]();if(this[_0x4ec3eb(0x139)]()===_0x4ec3eb(0x7a0))this[_0x4ec3eb(0x836)]();},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x4ce)]=function(){const _0x539d61=_0x53c255;if($gameTemp[_0x539d61(0x62a)]())this['preparePartyRefresh'](),this[_0x539d61(0x28b)]=![];else this[_0x539d61(0x28b)]&&(this[_0x539d61(0x28b)]=![],this[_0x539d61(0x4fc)]());},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x359)]=function(){const _0x2129d8=_0x53c255;Window_StatusBase['prototype'][_0x2129d8(0x359)][_0x2129d8(0x6b9)](this);if(!$gameSystem[_0x2129d8(0xdb)]())this[_0x2129d8(0x4fc)]();},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0xdf)]=function(){const _0x4c7083=_0x53c255;if(this[_0x4c7083(0x70f)]===Window_BattleStatus)return;Window_StatusBase[_0x4c7083(0x819)][_0x4c7083(0xdf)][_0x4c7083(0x6b9)](this);},Window_BattleStatus['prototype'][_0x53c255(0x1fe)]=function(_0x137f9d){const _0x69e7f1=_0x53c255,_0x175a5d=this[_0x69e7f1(0x139)]();switch(_0x175a5d){case'xp':case _0x69e7f1(0x6bf):break;case _0x69e7f1(0x1cf):case _0x69e7f1(0x4a9):case _0x69e7f1(0x7a0):default:return Window_StatusBase[_0x69e7f1(0x819)][_0x69e7f1(0x1fe)][_0x69e7f1(0x6b9)](this,_0x137f9d);break;}},VisuMZ[_0x53c255(0x2cc)]['Window_BattleStatus_drawItemImage']=Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x70b)],Window_BattleStatus['prototype'][_0x53c255(0x70b)]=function(_0x557315){const _0x22511c=_0x53c255,_0xcbd33b=this['battleLayoutStyle']();switch(_0xcbd33b){case'list':this[_0x22511c(0x1f2)](_0x557315);break;case'xp':this['drawItemImageXPStyle'](_0x557315);break;case _0x22511c(0x6bf):this[_0x22511c(0x51a)](_0x557315);break;case _0x22511c(0x1cf):case'border':default:VisuMZ['BattleCore'][_0x22511c(0x5fe)]['call'](this,_0x557315);break;}},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x43c)]=function(_0x31c4b3){const _0x178d46=_0x53c255,_0x379bd8=this[_0x178d46(0x139)]();if(!$gameSystem[_0x178d46(0xdb)]())this[_0x178d46(0x12a)](_0x31c4b3);switch(_0x379bd8){case _0x178d46(0x4a9):this[_0x178d46(0x82d)](_0x31c4b3);break;case'xp':case _0x178d46(0x6bf):case _0x178d46(0x1cf):case _0x178d46(0x7a0):default:this['drawItemStatusXPStyle'](_0x31c4b3);break;}},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x715)]=function(){const _0xfc7ecc=_0x53c255,_0x1ddefe=this['battleLayoutStyle']();if(['xp'][_0xfc7ecc(0x6a6)](_0x1ddefe)&&!$gameSystem['isSideView']()){this[_0xfc7ecc(0x7e4)](0x0,0x0,0x0,0x0);return;}Window_StatusBase[_0xfc7ecc(0x819)][_0xfc7ecc(0x715)]['call'](this);},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x12a)]=function(_0x492b20){const _0x5b81df=_0x53c255,_0x22da77=this['actor'](_0x492b20)[_0x5b81df(0x5ef)]();if(!_0x22da77)return;const _0x533863=this[_0x5b81df(0x139)](),_0x4eb9b2=this['itemRect'](_0x492b20);let _0x234c4f=Math[_0x5b81df(0x6c4)](_0x4eb9b2['x']+_0x4eb9b2['width']/0x2);['list']['includes'](_0x533863)&&(_0x234c4f=_0x4eb9b2['width']/$gameParty['battleMembers']()[_0x5b81df(0x343)],_0x234c4f*=_0x492b20,_0x234c4f+=_0x4eb9b2[_0x5b81df(0x261)]/$gameParty[_0x5b81df(0x116)]()['length']/0x2);let _0x5277cd=Math[_0x5b81df(0x6c4)](this[_0x5b81df(0x1b2)](_0x492b20,_0x22da77,_0x4eb9b2));_0x22da77[_0x5b81df(0x119)](_0x234c4f,_0x5277cd),this[_0x5b81df(0x3f4)](_0x22da77,0x1),_0x22da77[_0x5b81df(0x359)]();},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x1b2)]=function(_0x5a602d,_0x3f54d4,_0x322b5f){const _0x5efa5b=_0x53c255,_0x843353=VisuMZ['BattleCore'][_0x5efa5b(0x4b3)][_0x5efa5b(0xf1)],_0x48e166=this['battleLayoutStyle']();if(_0x48e166==='xp'){const _0x188ffa=_0x843353[_0x5efa5b(0x3c6)];switch(_0x188ffa[_0x5efa5b(0x534)]()[_0x5efa5b(0x615)]()){case'bottom':return _0x322b5f['height']-_0x3f54d4[_0x5efa5b(0x76f)][_0x5efa5b(0x6ab)]/0x4;break;case _0x5efa5b(0x211):const _0x50fa34=_0x843353[_0x5efa5b(0x671)];return(_0x322b5f['height']+(_0x3f54d4[_0x5efa5b(0x6ab)]||_0x50fa34))/0x2;break;case _0x5efa5b(0x307):return 0x0;case _0x5efa5b(0x701):default:return this[_0x5efa5b(0x184)](_0x322b5f);break;}}else{if(_0x48e166==='portrait'){}}return _0x3f54d4[_0x5efa5b(0x6ab)];},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x1f2)]=function(_0x2547e0){const _0x220f00=_0x53c255;if(!VisuMZ[_0x220f00(0x2cc)][_0x220f00(0x4b3)]['BattleLayout']['ShowFacesListStyle'])return;const _0x162a8c=this['actor'](_0x2547e0),_0x61026d=this[_0x220f00(0x293)](_0x2547e0);_0x61026d[_0x220f00(0x261)]=ImageManager[_0x220f00(0x656)],_0x61026d['height']-=0x2,this[_0x220f00(0x590)](_0x162a8c,_0x61026d['x']+0x1,_0x61026d['y']+0x1,_0x61026d[_0x220f00(0x261)],_0x61026d['height']);},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x82d)]=function(_0x434c17){const _0xac3608=_0x53c255,_0x59339e=$dataSystem[_0xac3608(0x2d9)]?0x4:0x3,_0x13bf7c=_0x59339e*0x80+(_0x59339e-0x1)*0x8+0x4,_0x424cd4=this[_0xac3608(0x7de)](_0x434c17),_0x33d816=this[_0xac3608(0x293)](_0x434c17);let _0xa882f5=_0x33d816['x']+this['padding'];VisuMZ[_0xac3608(0x2cc)]['Settings'][_0xac3608(0xf1)][_0xac3608(0x7a7)]?_0xa882f5=_0x33d816['x']+ImageManager[_0xac3608(0x656)]+0x8:_0xa882f5+=ImageManager['iconWidth'];const _0x376443=Math['round'](Math[_0xac3608(0x413)](_0x33d816['x']+_0x33d816['width']-_0x13bf7c,_0xa882f5)),_0x3431d3=Math['round'](_0x33d816['y']+(_0x33d816[_0xac3608(0x6ab)]-Sprite_Name['prototype'][_0xac3608(0x318)]())/0x2),_0x3e2ce0=Math[_0xac3608(0x6c4)](_0x376443-ImageManager[_0xac3608(0x142)]/0x2-0x4),_0x50b63d=Math[_0xac3608(0x6c4)](_0x33d816['y']+(_0x33d816[_0xac3608(0x6ab)]-ImageManager[_0xac3608(0x183)])/0x2+ImageManager[_0xac3608(0x183)]/0x2);let _0x1384d7=_0x376443+0x88;const _0x3eb238=_0x3431d3;this['placeTimeGauge'](_0x424cd4,_0x376443-0x4,_0x3431d3),this[_0xac3608(0x19d)](_0x424cd4,_0x376443,_0x3431d3),this[_0xac3608(0x337)](_0x424cd4,_0x3e2ce0,_0x50b63d),this['placeGauge'](_0x424cd4,'hp',_0x1384d7+0x88*0x0,_0x3eb238),this[_0xac3608(0x446)](_0x424cd4,'mp',_0x1384d7+0x88*0x1,_0x3eb238),$dataSystem['optDisplayTp']&&this[_0xac3608(0x446)](_0x424cd4,'tp',_0x1384d7+0x88*0x2,_0x3eb238);},Window_BattleStatus['prototype']['drawItemImageXPStyle']=function(_0x56ec86){const _0x36be9e=_0x53c255;if(!$gameSystem[_0x36be9e(0xdb)]())return;VisuMZ['BattleCore']['Window_BattleStatus_drawItemImage'][_0x36be9e(0x6b9)](this,_0x56ec86);},Window_BattleStatus['prototype']['drawItemStatusXPStyle']=function(_0xeb8bc5){const _0x37a975=_0x53c255,_0x56fb8f=this[_0x37a975(0x7de)](_0xeb8bc5),_0x543620=this[_0x37a975(0x293)](_0xeb8bc5),_0x34053b=Math[_0x37a975(0x6c4)](_0x543620['x']+(_0x543620[_0x37a975(0x261)]-0x80)/0x2),_0x30ea33=this[_0x37a975(0x184)](_0x543620);let _0x510f3b=_0x34053b-ImageManager['iconWidth']/0x2-0x4,_0x4e059a=_0x30ea33+ImageManager[_0x37a975(0x183)]/0x2;_0x510f3b-ImageManager[_0x37a975(0x142)]/0x2<_0x543620['x']&&(_0x510f3b=_0x34053b+ImageManager['iconWidth']/0x2-0x4,_0x4e059a=_0x30ea33-ImageManager[_0x37a975(0x183)]/0x2);const _0x2dbbcb=_0x34053b,_0x36a68d=this[_0x37a975(0x7d5)](_0x543620);this[_0x37a975(0x607)](_0x56fb8f,_0x34053b,_0x30ea33),this[_0x37a975(0x19d)](_0x56fb8f,_0x34053b,_0x30ea33),this[_0x37a975(0x337)](_0x56fb8f,_0x510f3b,_0x4e059a),this[_0x37a975(0x3c8)](_0x56fb8f,_0x2dbbcb,_0x36a68d);},Window_BattleStatus['prototype'][_0x53c255(0x4c5)]=function(_0x10cb9b){const _0x40f248=_0x53c255;if(!VisuMZ[_0x40f248(0x2cc)][_0x40f248(0x4b3)][_0x40f248(0xf1)][_0x40f248(0x837)])return![];if(_0x10cb9b[_0x40f248(0x2a8)]())return!![];return Imported['VisuMZ_1_MainMenuCore']&&_0x10cb9b[_0x40f248(0x2a4)]();},Game_Actor['prototype'][_0x53c255(0x7cc)]=function(){const _0x578d1c=_0x53c255;if(this[_0x578d1c(0x7de)]()[_0x578d1c(0x236)][_0x578d1c(0x473)](/<BATTLE (?:IMAGE|PORTRAIT) OFFSET X:[ ]([\+\-]\d+)>/i))return Number(RegExp['$1']);else{if(this[_0x578d1c(0x7de)]()[_0x578d1c(0x236)][_0x578d1c(0x473)](/<BATTLE (?:IMAGE|PORTRAIT) OFFSET:[ ]([\+\-]\d+),[ ]([\+\-]\d+)>/i))return Number(RegExp['$1']);}return 0x0;},Game_Actor[_0x53c255(0x819)]['getBattlePortraitOffsetY']=function(){const _0x2a8d5c=_0x53c255;if(this['actor']()[_0x2a8d5c(0x236)][_0x2a8d5c(0x473)](/<BATTLE (?:IMAGE|PORTRAIT) OFFSET Y:[ ]([\+\-]\d+)>/i))return Number(RegExp['$1']);else{if(this['actor']()[_0x2a8d5c(0x236)]['match'](/<BATTLE (?:IMAGE|PORTRAIT) OFFSET:[ ]([\+\-]\d+),[ ]([\+\-]\d+)>/i))return Number(RegExp['$2']);}return 0x0;},Window_BattleStatus[_0x53c255(0x819)]['drawItemImagePortraitStyle']=function(_0x3b3e24){const _0x15d1de=_0x53c255,_0x39875f=this['actor'](_0x3b3e24);if(this['showPortraits'](_0x39875f)){const _0xac97d4=_0x15d1de(0x5d7)['format'](_0x39875f[_0x15d1de(0x664)]()),_0x319120=this[_0x15d1de(0x1b3)](_0xac97d4,Sprite),_0x4f0dbb=_0x39875f[_0x15d1de(0x386)]();_0x4f0dbb!==''?_0x319120[_0x15d1de(0x657)]=ImageManager[_0x15d1de(0x7a2)](_0x4f0dbb):_0x319120[_0x15d1de(0x657)]=ImageManager['_emptyBitmap'];const _0x3cdbdb=this['itemRect'](_0x3b3e24);_0x319120[_0x15d1de(0x770)]['x']=0.5,_0x319120['anchor']['y']=0x1;let _0x3f38ac=Math[_0x15d1de(0x6c4)](_0x3cdbdb['x']+_0x3cdbdb[_0x15d1de(0x261)]/0x2)+this[_0x15d1de(0x6b6)];_0x3f38ac+=_0x39875f[_0x15d1de(0x7cc)]();let _0x11b655=Math[_0x15d1de(0x6c4)](this[_0x15d1de(0x6ab)]);_0x11b655+=_0x39875f['getBattlePortraitOffsetY'](),_0x319120[_0x15d1de(0x13f)](_0x3f38ac,_0x11b655);const _0x1a70c4=VisuMZ['BattleCore'][_0x15d1de(0x4b3)][_0x15d1de(0xf1)][_0x15d1de(0x49d)];_0x319120[_0x15d1de(0x2dd)]['x']=_0x1a70c4,_0x319120[_0x15d1de(0x2dd)]['y']=_0x1a70c4,_0x319120[_0x15d1de(0x359)]();}else{const _0x3ad397=this[_0x15d1de(0x34f)](_0x3b3e24);this[_0x15d1de(0x590)](_0x39875f,_0x3ad397['x'],_0x3ad397['y'],_0x3ad397[_0x15d1de(0x261)],_0x3ad397[_0x15d1de(0x6ab)]);}},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x1b3)]=function(_0x507886,_0x2ba689){const _0x42631f=_0x53c255,_0x44475f=this[_0x42631f(0x5a6)];if(_0x44475f[_0x507886])return _0x44475f[_0x507886];else{const _0x17973c=new _0x2ba689();return _0x44475f[_0x507886]=_0x17973c,this['addChildToBack'](_0x17973c),this[_0x42631f(0x631)](this['_cursorArea']),_0x17973c;}},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x540)]=function(){const _0x57e743=_0x53c255;this[_0x57e743(0x5a5)](),this[_0x57e743(0x3f7)](),Window_StatusBase['prototype'][_0x57e743(0x540)]['call'](this),this[_0x57e743(0x3fe)]();},Window_BattleStatus[_0x53c255(0x819)]['_createCursorArea']=function(){const _0x397d22=_0x53c255;this[_0x397d22(0x5a9)]=new Sprite(),this[_0x397d22(0x5a9)][_0x397d22(0x5f0)]=[new PIXI['filters'][(_0x397d22(0x546))]()],this[_0x397d22(0x5a9)][_0x397d22(0x3a2)]=new Rectangle(),this[_0x397d22(0x5a9)][_0x397d22(0x13f)](this[_0x397d22(0x145)],this[_0x397d22(0x145)]),this[_0x397d22(0x62f)](this[_0x397d22(0x5a9)]);},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x3f7)]=function(){const _0x17dcf5=_0x53c255;this['_effectsContainer']=new Sprite(),this[_0x17dcf5(0x62f)](this[_0x17dcf5(0x377)]);},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x3fe)]=function(){const _0x381be9=_0x53c255;this[_0x381be9(0x5dd)]=new Sprite(),this[_0x381be9(0x62f)](this[_0x381be9(0x5dd)]);},Window_BattleStatus['prototype']['_createCursorSprite']=function(){const _0x1e8884=_0x53c255;this[_0x1e8884(0x406)]=new Sprite();for(let _0x34dc04=0x0;_0x34dc04<0x9;_0x34dc04++){this[_0x1e8884(0x406)]['addChild'](new Sprite());}this[_0x1e8884(0x5a9)][_0x1e8884(0x62f)](this[_0x1e8884(0x406)]);},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x73a)]=function(){const _0x483147=_0x53c255;Window_StatusBase['prototype'][_0x483147(0x73a)][_0x483147(0x6b9)](this),this[_0x483147(0x276)]();},Window_BattleStatus[_0x53c255(0x819)]['_updateCursorArea']=function(){const _0xd57eab=_0x53c255,_0x899358=this[_0xd57eab(0x145)];this['_cursorArea']['move'](_0x899358,_0x899358),this[_0xd57eab(0x5a9)]['x']=_0x899358-this[_0xd57eab(0x439)]['x'],this[_0xd57eab(0x5a9)]['y']=_0x899358-this[_0xd57eab(0x439)]['y'],this[_0xd57eab(0x6d2)]>0x0&&this[_0xd57eab(0x77e)]>0x0?this[_0xd57eab(0x5a9)][_0xd57eab(0x3f2)]=this[_0xd57eab(0x1f3)]():this[_0xd57eab(0x5a9)]['visible']=![];},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x63a)]=function(){const _0xb4edf4=_0x53c255;Window_StatusBase[_0xb4edf4(0x819)][_0xb4edf4(0x63a)]['call'](this),this['_updateCursorFilterArea']();},Window_BattleStatus[_0x53c255(0x819)]['_updateCursorFilterArea']=function(){const _0x1a2869=_0x53c255,_0x28f5c3=this['_cursorArea'][_0x1a2869(0x429)]['apply'](new Point(0x0,0x0)),_0x145262=this['_cursorArea'][_0x1a2869(0x3a2)];_0x145262['x']=_0x28f5c3['x']+this['origin']['x'],_0x145262['y']=_0x28f5c3['y']+this[_0x1a2869(0x439)]['y'],_0x145262[_0x1a2869(0x261)]=this[_0x1a2869(0x6d2)],_0x145262[_0x1a2869(0x6ab)]=this[_0x1a2869(0x77e)];},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x6d5)]=function(_0x55bca4){const _0x51f56b=_0x53c255;if(this[_0x51f56b(0x139)]()!==_0x51f56b(0x6bf))return;this['drawItemImagePortraitStyle'](_0x55bca4['index']());},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x609)]=function(_0x434e79,_0x548931){const _0x4c7b83=_0x53c255;if(!this[_0x4c7b83(0x5dd)])return;if(!_0x434e79)return;if(!_0x548931)return;const _0x463b2d=this['itemRect'](_0x548931[_0x4c7b83(0x568)]());_0x463b2d['x']+=_0x463b2d[_0x4c7b83(0x261)]/0x2+this[_0x4c7b83(0x6b6)],_0x434e79['x']=_0x463b2d['x'],_0x434e79['y']=_0x463b2d['y'],this[_0x4c7b83(0x5dd)][_0x4c7b83(0x62f)](_0x434e79);},Window_BattleStatus['prototype'][_0x53c255(0x4b2)]=function(_0xc6ab24){const _0x29035b=_0x53c255;if(!this[_0x29035b(0x5dd)])return;if(!_0xc6ab24)return;this['_damageContainer'][_0x29035b(0x4d6)](_0xc6ab24);},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x836)]=function(){const _0x2d418f=_0x53c255;if(!this[_0x2d418f(0x49c)]())return;if(!this[_0x2d418f(0x122)])this['createBorderStylePortraitSprite']();this[_0x2d418f(0x267)](),this[_0x2d418f(0x4cc)]();},Window_BattleStatus[_0x53c255(0x819)]['isBorderStylePortraitShown']=function(){const _0x449cd2=_0x53c255;if(this[_0x449cd2(0x70f)]!==Window_BattleStatus)return![];if(!SceneManager['isSceneBattle']())return![];return VisuMZ[_0x449cd2(0x2cc)][_0x449cd2(0x4b3)][_0x449cd2(0xf1)][_0x449cd2(0x29f)];},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x537)]=function(){const _0x5761ad=_0x53c255;this[_0x5761ad(0x122)]=new Sprite();const _0x2050eb=SceneManager[_0x5761ad(0x12f)],_0x128b67=_0x2050eb[_0x5761ad(0x26d)][_0x5761ad(0x1d0)](_0x2050eb[_0x5761ad(0x3a3)]);_0x2050eb[_0x5761ad(0x3f4)](this['_borderPortraitSprite'],_0x128b67),this[_0x5761ad(0x122)][_0x5761ad(0x770)]['x']=0.5,this[_0x5761ad(0x122)][_0x5761ad(0x770)]['y']=0x1;const _0x3e50ba=VisuMZ['BattleCore'][_0x5761ad(0x4b3)]['BattleLayout'][_0x5761ad(0x74e)];this['_borderPortraitSprite'][_0x5761ad(0x2dd)]['x']=_0x3e50ba,this[_0x5761ad(0x122)][_0x5761ad(0x2dd)]['y']=_0x3e50ba,this['_borderPortraitSprite']['y']=this['y']+this[_0x5761ad(0x6ab)],this['_borderPortraitDuration']=0x0;},Window_BattleStatus['prototype'][_0x53c255(0x267)]=function(){const _0x352a56=_0x53c255;this[_0x352a56(0x122)][_0x352a56(0x3f2)]=BattleManager[_0x352a56(0x6c8)]();const _0x99d8af=BattleManager['actor']();if(_0x99d8af===this[_0x352a56(0x122)]['actor'])return;this[_0x352a56(0x122)][_0x352a56(0x7de)]=_0x99d8af||this['_borderPortraitSprite'][_0x352a56(0x7de)];if(!_0x99d8af)return;else{if(_0x99d8af[_0x352a56(0x386)]()===''){this[_0x352a56(0x122)][_0x352a56(0x657)]=ImageManager[_0x352a56(0x67a)];return;}else{const _0x5aae84=ImageManager['loadPicture'](_0x99d8af[_0x352a56(0x386)]());_0x5aae84['addLoadListener'](this['processBorderActor'][_0x352a56(0x4c7)](this,_0x5aae84));}}},Window_BattleStatus[_0x53c255(0x819)]['processBorderActor']=function(_0x3c9adb){const _0x4f7f58=_0x53c255;this[_0x4f7f58(0x82f)]=0x14,this['_borderPortraitSprite'][_0x4f7f58(0x657)]=_0x3c9adb;SceneManager[_0x4f7f58(0x12f)][_0x4f7f58(0x64f)]()?(this[_0x4f7f58(0x122)]['x']=0x0,this['_borderPortraitTargetX']=Math[_0x4f7f58(0x305)](_0x3c9adb[_0x4f7f58(0x261)]/0x2)):(this[_0x4f7f58(0x122)]['x']=this[_0x4f7f58(0x261)],this[_0x4f7f58(0x52a)]=this[_0x4f7f58(0x261)]*0x3/0x4);this[_0x4f7f58(0x122)]['opacity']=0x0,this[_0x4f7f58(0x122)]['y']=this['y']+this['height'];const _0x33ce80=BattleManager[_0x4f7f58(0x7de)]();_0x33ce80&&(this[_0x4f7f58(0x52a)]+=_0x33ce80[_0x4f7f58(0x7cc)](),this['_borderPortraitSprite']['y']+=_0x33ce80[_0x4f7f58(0x30c)]());},Window_BattleStatus[_0x53c255(0x819)]['updateBorderSprite']=function(){const _0x1183f4=_0x53c255;if(this['_borderPortraitDuration']>0x0){const _0x27b8e6=this[_0x1183f4(0x82f)],_0x479b2a=this[_0x1183f4(0x122)];_0x479b2a['x']=(_0x479b2a['x']*(_0x27b8e6-0x1)+this[_0x1183f4(0x52a)])/_0x27b8e6,_0x479b2a['opacity']=(_0x479b2a['opacity']*(_0x27b8e6-0x1)+0xff)/_0x27b8e6,this[_0x1183f4(0x82f)]--;}},Window_BattleStatus[_0x53c255(0x819)][_0x53c255(0x5b0)]=function(){const _0xdc0fe=_0x53c255;return;this[_0xdc0fe(0x377)]&&(this[_0xdc0fe(0x377)]['x']=this['x'],this[_0xdc0fe(0x377)]['y']=this['y']),this[_0xdc0fe(0x5dd)]&&(this[_0xdc0fe(0x5dd)]['x']=this['x'],this['_damageContainer']['y']=this['y']);},Window_BattleActor[_0x53c255(0x819)]['isOkEnabled']=function(){const _0x3715ff=_0x53c255;return Window_BattleStatus[_0x3715ff(0x819)]['isOkEnabled'][_0x3715ff(0x6b9)](this)&&this['isActionSelectionValid']();},Window_BattleActor[_0x53c255(0x819)][_0x53c255(0x6f1)]=function(){const _0x16e9fe=_0x53c255,_0x3e70df=BattleManager[_0x16e9fe(0x48d)](),_0x601b38=this[_0x16e9fe(0x7de)](this['index']());if(!_0x3e70df)return!![];if(!_0x3e70df[_0x16e9fe(0x4b7)]())return!![];const _0x4b10b7=_0x3e70df[_0x16e9fe(0x4b7)]()[_0x16e9fe(0x236)];if(_0x4b10b7[_0x16e9fe(0x473)](/<CANNOT TARGET (?:USER|SELF)>/i)){if(_0x601b38===BattleManager['actor']())return![];}return!![];},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x5f3)]=Window_BattleEnemy['prototype']['initialize'],Window_BattleEnemy[_0x53c255(0x819)][_0x53c255(0x854)]=function(_0x561f20){const _0x162558=_0x53c255;this['_lastEnemy']=null,VisuMZ[_0x162558(0x2cc)]['Window_BattleEnemy_initialize']['call'](this,_0x561f20);},Window_BattleEnemy['prototype'][_0x53c255(0x15a)]=function(){const _0x37bbf9=_0x53c255;return this[_0x37bbf9(0x71e)]();},VisuMZ[_0x53c255(0x2cc)]['Window_BattleEnemy_show']=Window_BattleEnemy[_0x53c255(0x819)][_0x53c255(0x359)],Window_BattleEnemy[_0x53c255(0x819)][_0x53c255(0x359)]=function(){const _0x8d8bd5=_0x53c255;VisuMZ[_0x8d8bd5(0x2cc)][_0x8d8bd5(0x3e1)][_0x8d8bd5(0x6b9)](this),this['y']=Graphics['height']*0xa;},Window_BattleEnemy[_0x53c255(0x819)]['validTargets']=function(){const _0x54d18b=_0x53c255;return $gameTroop[_0x54d18b(0x4d7)]()['slice'](0x0);},Window_BattleEnemy['prototype'][_0x53c255(0x4fc)]=function(){const _0x187558=_0x53c255;this[_0x187558(0x41b)]=this['validTargets'](),this['sortEnemies'](),Window_Selectable['prototype'][_0x187558(0x4fc)][_0x187558(0x6b9)](this);},Window_BattleEnemy[_0x53c255(0x819)][_0x53c255(0x67b)]=function(){const _0x2291d5=_0x53c255;this[_0x2291d5(0x41b)]['sort']((_0x364eb9,_0x28bb4e)=>{const _0x497eb7=_0x2291d5;return _0x364eb9[_0x497eb7(0x5ef)]()[_0x497eb7(0x396)]===_0x28bb4e[_0x497eb7(0x5ef)]()[_0x497eb7(0x396)]?_0x364eb9[_0x497eb7(0x5ef)]()[_0x497eb7(0x772)]-_0x28bb4e['battler']()[_0x497eb7(0x772)]:_0x364eb9[_0x497eb7(0x5ef)]()[_0x497eb7(0x396)]-_0x28bb4e[_0x497eb7(0x5ef)]()['_baseX'];}),SceneManager[_0x2291d5(0x32a)]()&&this['_enemies'][_0x2291d5(0x70e)]();},Window_BattleEnemy[_0x53c255(0x819)]['autoSelect']=function(){const _0xe6192a=_0x53c255,_0x3bb9a7=VisuMZ['BattleCore'][_0xe6192a(0x4b3)][_0xe6192a(0x1c7)];_0x3bb9a7[_0xe6192a(0x3ad)]?this['autoSelectLastSelected']():this[_0xe6192a(0x623)]();},Window_BattleEnemy['prototype']['autoSelectLastSelected']=function(){const _0x5d64b8=_0x53c255;if(this[_0x5d64b8(0x570)]&&this['_enemies'][_0x5d64b8(0x6a6)](this[_0x5d64b8(0x570)])){const _0x5ee930=this[_0x5d64b8(0x41b)][_0x5d64b8(0x1d0)](this[_0x5d64b8(0x570)]);this['forceSelect'](_0x5ee930);}else this[_0x5d64b8(0x623)]();},Window_BattleEnemy[_0x53c255(0x819)][_0x53c255(0x623)]=function(){const _0x3c7ae9=_0x53c255,_0x55f262=VisuMZ['BattleCore'][_0x3c7ae9(0x4b3)]['Enemy'];let _0x40b928=![];$gameSystem['isSideView']()?_0x40b928=_0x55f262[_0x3c7ae9(0x27e)]:_0x40b928=_0x55f262['FrontViewSelect'],this[_0x3c7ae9(0x10e)](_0x40b928?this[_0x3c7ae9(0x71e)]()-0x1:0x0);},Window_BattleEnemy['prototype']['callOkHandler']=function(){const _0x20e6a4=_0x53c255;Window_Selectable['prototype'][_0x20e6a4(0x72e)][_0x20e6a4(0x6b9)](this),this[_0x20e6a4(0x570)]=this[_0x20e6a4(0x221)]();},Window_BattleItem[_0x53c255(0x819)][_0x53c255(0x6a6)]=function(_0x39bc7c){const _0x57aa20=_0x53c255;if(!_0x39bc7c)return![];return _0x39bc7c[_0x57aa20(0x507)]===0x0||_0x39bc7c[_0x57aa20(0x507)]===0x1;};function Window_AutoBattleCancel(){this['initialize'](...arguments);}Window_AutoBattleCancel[_0x53c255(0x819)]=Object[_0x53c255(0x21a)](Window_Base[_0x53c255(0x819)]),Window_AutoBattleCancel['prototype'][_0x53c255(0x70f)]=Window_AutoBattleCancel,Window_AutoBattleCancel[_0x53c255(0x819)]['initialize']=function(_0x522232){const _0x852d9a=_0x53c255;Window_Base[_0x852d9a(0x819)]['initialize']['call'](this,_0x522232),this[_0x852d9a(0x1bb)](this[_0x852d9a(0x306)]()),this[_0x852d9a(0x4fc)]();},Window_AutoBattleCancel[_0x53c255(0x819)][_0x53c255(0x306)]=function(){const _0x22b393=_0x53c255;return VisuMZ[_0x22b393(0x2cc)][_0x22b393(0x4b3)][_0x22b393(0x216)][_0x22b393(0x685)];},Window_AutoBattleCancel[_0x53c255(0x819)][_0x53c255(0x4fc)]=function(){const _0x1fe1b0=_0x53c255;this['contents']['clear']();const _0xf429c8=VisuMZ[_0x1fe1b0(0x2cc)][_0x1fe1b0(0x4b3)][_0x1fe1b0(0x216)][_0x1fe1b0(0x426)],_0x5d24a0=_0xf429c8[_0x1fe1b0(0x107)](this[_0x1fe1b0(0x6b8)](),this[_0x1fe1b0(0x12d)]()),_0x100dd3=this[_0x1fe1b0(0x6a5)](_0x5d24a0)[_0x1fe1b0(0x261)],_0x17c800=Math[_0x1fe1b0(0x7f5)]((this[_0x1fe1b0(0x6d2)]-_0x100dd3)/0x2);this[_0x1fe1b0(0x259)](_0x5d24a0,_0x17c800,0x0,_0x100dd3);},Window_AutoBattleCancel[_0x53c255(0x819)][_0x53c255(0x6b8)]=function(){const _0x35e9d6=_0x53c255;return Imported[_0x35e9d6(0x23a)]?TextManager['getInputButtonString']('ok'):VisuMZ[_0x35e9d6(0x2cc)]['Settings']['AutoBattle'][_0x35e9d6(0x591)];},Window_AutoBattleCancel['prototype']['cancelButtonText']=function(){const _0x552056=_0x53c255;return Imported[_0x552056(0x23a)]?TextManager[_0x552056(0x349)]('cancel'):VisuMZ[_0x552056(0x2cc)]['Settings'][_0x552056(0x216)][_0x552056(0x1a7)];},Window_AutoBattleCancel[_0x53c255(0x819)]['update']=function(){const _0x878f10=_0x53c255;Window_Base[_0x878f10(0x819)][_0x878f10(0x3ed)][_0x878f10(0x6b9)](this),this['updateVisibility'](),this[_0x878f10(0x66b)]();},Window_AutoBattleCancel[_0x53c255(0x819)][_0x53c255(0x1d6)]=function(){const _0x3048f6=_0x53c255;this['visible']=BattleManager[_0x3048f6(0x6ec)];},Window_AutoBattleCancel[_0x53c255(0x819)][_0x53c255(0x66b)]=function(){const _0x3d60b7=_0x53c255;if(!BattleManager[_0x3d60b7(0x6ec)])return;(Input[_0x3d60b7(0x6b0)]('ok')||Input[_0x3d60b7(0x6b0)](_0x3d60b7(0x1c5))||TouchInput[_0x3d60b7(0x22c)]()||TouchInput[_0x3d60b7(0x658)]())&&(SoundManager['playCancel'](),BattleManager['_autoBattle']=![],Input[_0x3d60b7(0x6fe)](),TouchInput[_0x3d60b7(0x6fe)]());};function Window_EnemyName(){this['initialize'](...arguments);}Window_EnemyName[_0x53c255(0x819)]=Object[_0x53c255(0x21a)](Window_Base['prototype']),Window_EnemyName[_0x53c255(0x819)][_0x53c255(0x70f)]=Window_EnemyName,Window_EnemyName[_0x53c255(0x819)][_0x53c255(0x854)]=function(_0x15d24c){const _0x189994=_0x53c255;this[_0x189994(0xe7)]=_0x15d24c,this[_0x189994(0x4c2)]='';const _0x28614f=new Rectangle(0x0,0x0,Graphics[_0x189994(0x32c)],this[_0x189994(0x585)]()*0x4);Window_Base['prototype'][_0x189994(0x854)][_0x189994(0x6b9)](this,_0x28614f),this[_0x189994(0x1bb)](0x2),this[_0x189994(0x3c5)]=0x0;},Window_EnemyName[_0x53c255(0x819)]['updatePadding']=function(){const _0x300a1a=_0x53c255;this[_0x300a1a(0x6b6)]=0x0;},Window_EnemyName[_0x53c255(0x819)][_0x53c255(0x221)]=function(){const _0x19a6a8=_0x53c255;return $gameTroop[_0x19a6a8(0x4e3)]()[this[_0x19a6a8(0xe7)]];},Window_EnemyName[_0x53c255(0x819)]['update']=function(){const _0x32aefd=_0x53c255;Window_Base['prototype'][_0x32aefd(0x3ed)][_0x32aefd(0x6b9)](this);if(this[_0x32aefd(0x221)]()&&this['enemy']()['name']()!==this[_0x32aefd(0x4c2)])this[_0x32aefd(0x4fc)]();this['updateOpacity'](),this['updatePosition']();},Window_EnemyName[_0x53c255(0x819)][_0x53c255(0x69f)]=function(){const _0x188646=_0x53c255;if(!this[_0x188646(0x221)]()){if(this['contentsOpacity']>0x0)this[_0x188646(0x3c5)]-=0x10;}else{if(this[_0x188646(0x221)]()['isDead']()){if(this[_0x188646(0x3c5)]>0x0)this[_0x188646(0x3c5)]-=0x10;}else{if(SceneManager[_0x188646(0x12f)][_0x188646(0x6d6)]&&SceneManager[_0x188646(0x12f)][_0x188646(0x6d6)][_0x188646(0x27d)]&&SceneManager['_scene'][_0x188646(0x6d6)][_0x188646(0x41b)][_0x188646(0x6a6)](this['enemy']())){if(this[_0x188646(0x3c5)]<0xff)this[_0x188646(0x3c5)]+=0x10;}else this[_0x188646(0x3c5)]>0x0&&(this[_0x188646(0x3c5)]-=0x10);}}},Window_EnemyName[_0x53c255(0x819)]['updatePosition']=function(){const _0x3b4bb5=_0x53c255;if(!this['enemy']())return;SceneManager['isBattleFlipped']()?this['x']=Graphics[_0x3b4bb5(0x32c)]-this[_0x3b4bb5(0x221)]()['battler']()[_0x3b4bb5(0x396)]:this['x']=this['enemy']()[_0x3b4bb5(0x5ef)]()['_baseX'];this['x']-=Math[_0x3b4bb5(0x6c4)](this[_0x3b4bb5(0x261)]/0x2),this['y']=this[_0x3b4bb5(0x221)]()[_0x3b4bb5(0x5ef)]()[_0x3b4bb5(0x772)]-Math[_0x3b4bb5(0x6c4)](this[_0x3b4bb5(0x585)]()*1.5);const _0x325e58=VisuMZ[_0x3b4bb5(0x2cc)]['Settings']['Enemy'];this['x']+=_0x325e58[_0x3b4bb5(0x233)]||0x0,this['y']+=_0x325e58[_0x3b4bb5(0x26e)]||0x0;},Window_EnemyName['prototype'][_0x53c255(0x201)]=function(){const _0x777474=_0x53c255;Window_Base[_0x777474(0x819)][_0x777474(0x201)]['call'](this),this['contents'][_0x777474(0x5a2)]=VisuMZ[_0x777474(0x2cc)][_0x777474(0x4b3)][_0x777474(0x1c7)][_0x777474(0x79e)];},Window_EnemyName['prototype'][_0x53c255(0x4fc)]=function(){const _0x4e2bb3=_0x53c255;this['contents'][_0x4e2bb3(0x6fe)]();if(!this[_0x4e2bb3(0x221)]())return;this[_0x4e2bb3(0x4c2)]=this[_0x4e2bb3(0x221)]()[_0x4e2bb3(0x701)]();const _0x504893=this['textSizeEx'](this[_0x4e2bb3(0x4c2)])[_0x4e2bb3(0x261)],_0x38cfbb=Math[_0x4e2bb3(0x6c4)]((this['innerWidth']-_0x504893)/0x2);this['drawTextEx'](this[_0x4e2bb3(0x4c2)],_0x38cfbb,0x0,_0x504893+0x8);},Window_BattleLog[_0x53c255(0x819)]['maxLines']=function(){const _0x1d9516=_0x53c255;return VisuMZ[_0x1d9516(0x2cc)][_0x1d9516(0x4b3)][_0x1d9516(0x563)]['MaxLines'];},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x5d8)]=function(){const _0x116915=_0x53c255;return VisuMZ[_0x116915(0x2cc)][_0x116915(0x4b3)][_0x116915(0x563)][_0x116915(0x663)];},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x12c)]=function(){const _0x2a9906=_0x53c255;return VisuMZ[_0x2a9906(0x2cc)][_0x2a9906(0x4b3)]['BattleLog'][_0x2a9906(0x5cc)];},Window_BattleLog[_0x53c255(0x819)]['isFastForward']=function(){return![];},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x22f)]=function(_0x3d2ead,_0x305276){const _0xc8f691=_0x53c255;this[_0xc8f691(0x319)]('actionSplicePoint'),BattleManager[_0xc8f691(0x262)](_0x3d2ead,_0x305276),this['callNextMethod']();},Window_BattleLog[_0x53c255(0x819)]['actionSplicePoint']=function(){this['callNextMethod']();},Window_BattleLog[_0x53c255(0x819)]['push']=function(_0x3955d7){const _0x23ecf6=_0x53c255,_0x5d4fb0=Array[_0x23ecf6(0x819)]['slice'][_0x23ecf6(0x6b9)](arguments,0x1),_0x6f5567={'name':_0x3955d7,'params':_0x5d4fb0},_0x460e08=this[_0x23ecf6(0x120)][_0x23ecf6(0x19c)](_0x467133=>_0x467133[_0x23ecf6(0x701)])[_0x23ecf6(0x1d0)](_0x23ecf6(0x83a));_0x460e08>=0x0?this[_0x23ecf6(0x120)][_0x23ecf6(0x5e4)](_0x460e08,0x0,_0x6f5567):this['_methods'][_0x23ecf6(0x2bf)](_0x6f5567);},Window_BattleLog['prototype'][_0x53c255(0x319)]=function(_0x5e459b){const _0xe32a7=_0x53c255,_0x48e85e=Array['prototype'][_0xe32a7(0x599)][_0xe32a7(0x6b9)](arguments,0x1);this[_0xe32a7(0x120)]['unshift']({'name':_0x5e459b,'params':_0x48e85e});},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x289)]=function(){const _0x3cf61d=_0x53c255;if(!$gameTemp[_0x3cf61d(0xf4)]())return;console['log'](this[_0x3cf61d(0x120)][_0x3cf61d(0x19c)](_0x3572e7=>_0x3572e7[_0x3cf61d(0x701)])['join']('\x0a'));},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x2b7)]=Window_BattleLog['prototype'][_0x53c255(0x4fc)],Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x4fc)]=function(){const _0x1568c7=_0x53c255;this[_0x1568c7(0x28b)]=!![];},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x399)]=Window_BattleLog['prototype'][_0x53c255(0x3ed)],Window_BattleLog[_0x53c255(0x819)]['update']=function(){const _0x4c49b0=_0x53c255;VisuMZ['BattleCore'][_0x4c49b0(0x399)][_0x4c49b0(0x6b9)](this);if(this[_0x4c49b0(0x28b)])this[_0x4c49b0(0x217)]();},Window_BattleLog['prototype'][_0x53c255(0x217)]=function(){const _0x1e93e9=_0x53c255;this[_0x1e93e9(0x28b)]=![],VisuMZ[_0x1e93e9(0x2cc)][_0x1e93e9(0x2b7)][_0x1e93e9(0x6b9)](this);},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x7ac)]=function(_0x4c5099){const _0x5e44a6=_0x53c255;let _0xaf4069=VisuMZ[_0x5e44a6(0x2cc)][_0x5e44a6(0x4b3)][_0x5e44a6(0x563)][_0x5e44a6(0x78b)][_0x5e44a6(0x534)]()['trim'](),_0x27ceb4=this['_lines'][_0x4c5099];if(_0x27ceb4[_0x5e44a6(0x473)](/<LEFT>/i))_0xaf4069=_0x5e44a6(0x2b3);else{if(_0x27ceb4[_0x5e44a6(0x473)](/<CENTER>/i))_0xaf4069=_0x5e44a6(0x211);else _0x27ceb4[_0x5e44a6(0x473)](/<RIGHT>/i)&&(_0xaf4069='right');}_0x27ceb4=_0x27ceb4['replace'](/<(?:LEFT|CENTER|RIGHT)>/gi,''),_0x27ceb4=_0x27ceb4[_0x5e44a6(0x3d6)](/\\I\[0\]/gi,'');const _0x428825=this[_0x5e44a6(0x3a8)](_0x4c5099);this[_0x5e44a6(0x14d)][_0x5e44a6(0x219)](_0x428825['x'],_0x428825['y'],_0x428825[_0x5e44a6(0x261)],_0x428825[_0x5e44a6(0x6ab)]);const _0x3a18fa=this['textSizeEx'](_0x27ceb4)[_0x5e44a6(0x261)];let _0x29b116=_0x428825['x'];if(_0xaf4069==='center')_0x29b116+=(_0x428825[_0x5e44a6(0x261)]-_0x3a18fa)/0x2;else _0xaf4069==='right'&&(_0x29b116+=_0x428825[_0x5e44a6(0x261)]-_0x3a18fa);this[_0x5e44a6(0x259)](_0x27ceb4,_0x29b116,_0x428825['y'],_0x3a18fa+0x8);},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x5d3)]=function(_0x401d0f){const _0x44908e=_0x53c255;this['_lines']['push'](_0x401d0f),this[_0x44908e(0x4fc)](),this[_0x44908e(0x1a3)]();},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x34e)]=function(){const _0x2c85fa=_0x53c255;let _0x3fcb38=![];switch(this[_0x2c85fa(0x154)]){case _0x2c85fa(0x580):_0x3fcb38=this[_0x2c85fa(0x3fb)][_0x2c85fa(0x521)]();break;case _0x2c85fa(0x1b9):_0x3fcb38=this[_0x2c85fa(0x3fb)][_0x2c85fa(0x34c)]();break;case'animation':_0x3fcb38=this[_0x2c85fa(0x3fb)][_0x2c85fa(0x525)]();break;case _0x2c85fa(0x4af):_0x3fcb38=this[_0x2c85fa(0x3fb)][_0x2c85fa(0x314)]();break;case _0x2c85fa(0x5b9):_0x3fcb38=this[_0x2c85fa(0x3fb)][_0x2c85fa(0x3cd)]();break;case _0x2c85fa(0x398):_0x3fcb38=this[_0x2c85fa(0x3fb)]['isAnyoneChangingOpacity']();break;}return!_0x3fcb38&&(this[_0x2c85fa(0x154)]=''),_0x3fcb38;},Window_BattleLog[_0x53c255(0x819)]['waitForAnimation']=function(){const _0x1767ec=_0x53c255;this[_0x1767ec(0x727)]('animation');},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x638)]=function(){const _0x231ca6=_0x53c255;this[_0x231ca6(0x727)](_0x231ca6(0x4af));},Window_BattleLog[_0x53c255(0x819)]['waitForJump']=function(){const _0x1d4ae0=_0x53c255;this[_0x1d4ae0(0x727)](_0x1d4ae0(0x5b9));},Window_BattleLog['prototype']['waitForOpacity']=function(){const _0xf3b136=_0x53c255;this[_0xf3b136(0x727)](_0xf3b136(0x398));},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x821)]=function(){const _0x3fb44e=_0x53c255,_0x243feb=VisuMZ[_0x3fb44e(0x2cc)][_0x3fb44e(0x4b3)][_0x3fb44e(0x563)];if(!_0x243feb[_0x3fb44e(0x680)])return;this['push'](_0x3fb44e(0x5d3),_0x243feb[_0x3fb44e(0x5fa)][_0x3fb44e(0x107)]($gameTroop[_0x3fb44e(0x474)]())),this[_0x3fb44e(0x2bf)](_0x3fb44e(0x5c0),_0x243feb[_0x3fb44e(0x783)]),this['push'](_0x3fb44e(0x6fe));},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x3e9)]=function(_0x5ab027,_0x591b11,_0x522565){const _0x4820b1=_0x53c255;this[_0x4820b1(0x4d5)](_0x591b11)?BattleManager['prepareCustomActionSequence']():this[_0x4820b1(0x31e)](_0x5ab027,_0x591b11,_0x522565);},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x4d5)]=function(_0x2ab040){const _0x1ac416=_0x53c255;if(!SceneManager[_0x1ac416(0x45f)]())return![];if(!_0x2ab040)return![];if(!_0x2ab040[_0x1ac416(0x4b7)]())return![];if(_0x2ab040[_0x1ac416(0x4b7)]()['note'][_0x1ac416(0x473)](/<CUSTOM ACTION SEQUENCE>/i))return!![];if(DataManager['checkAutoCustomActionSequenceNotetagEffect'](_0x2ab040[_0x1ac416(0x4b7)]()))return!![];return![];},Window_BattleLog['prototype'][_0x53c255(0x31e)]=function(_0xd47ee6,_0x55a83c,_0x47662b){const _0x2962ad=_0x53c255,_0x47aca4=_0x55a83c[_0x2962ad(0x4b7)]();this[_0x2962ad(0x36b)](_0xd47ee6,_0x55a83c,_0x47662b),this[_0x2962ad(0x68f)](_0xd47ee6,_0x55a83c,_0x47662b),this[_0x2962ad(0x2f2)](_0xd47ee6,_0x55a83c,_0x47662b);},Window_BattleLog['prototype'][_0x53c255(0x362)]=function(_0x53ec46,_0x428c08){const _0x3e25d5=_0x53c255,_0x22165e=VisuMZ[_0x3e25d5(0x2cc)]['Settings'][_0x3e25d5(0x563)];_0x22165e[_0x3e25d5(0x467)]&&this[_0x3e25d5(0x2bf)]('addText','<CENTER>%1'[_0x3e25d5(0x107)](DataManager[_0x3e25d5(0x4c9)](_0x428c08)));if(DataManager[_0x3e25d5(0x5c7)](_0x428c08)){if(_0x22165e[_0x3e25d5(0x2bd)])this[_0x3e25d5(0x2e4)](_0x428c08[_0x3e25d5(0x7d2)],_0x53ec46,_0x428c08);if(_0x22165e[_0x3e25d5(0x6df)])this[_0x3e25d5(0x2e4)](_0x428c08[_0x3e25d5(0x229)],_0x53ec46,_0x428c08);}else{if(_0x22165e[_0x3e25d5(0x222)])this[_0x3e25d5(0x2e4)](TextManager['useItem'],_0x53ec46,_0x428c08);}},Window_BattleLog['prototype'][_0x53c255(0x36b)]=function(_0x137898,_0x48ce94,_0x3be843){const _0x22849e=_0x53c255,_0x26e2fa=_0x48ce94[_0x22849e(0x4b7)]();this['displayAction'](_0x137898,_0x26e2fa),this[_0x22849e(0x2bf)](_0x22849e(0x652),_0x137898,_0x3be843,!![]),this[_0x22849e(0x2bf)](_0x22849e(0x5d1),_0x137898,_0x48ce94),this[_0x22849e(0x2bf)](_0x22849e(0x5b2)),this[_0x22849e(0x2bf)](_0x22849e(0x7d6),_0x137898,_0x48ce94),this[_0x22849e(0x2bf)](_0x22849e(0x579));},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x68f)]=function(_0x2a4ad2,_0x3dcef1,_0x199cb9){const _0x41f9bb=_0x53c255;if(this[_0x41f9bb(0x793)](_0x3dcef1))this['autoMeleeSingleTargetActionSet'](_0x2a4ad2,_0x3dcef1,_0x199cb9);else{if(this['isMeleeMultiTargetAction'](_0x3dcef1))this[_0x41f9bb(0x789)](_0x2a4ad2,_0x3dcef1,_0x199cb9);else _0x3dcef1['isForRandom']()?this[_0x41f9bb(0x67e)](_0x2a4ad2,_0x3dcef1,_0x199cb9):this[_0x41f9bb(0x3bc)](_0x2a4ad2,_0x3dcef1,_0x199cb9);}},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x793)]=function(_0x35405e){const _0x2fc735=_0x53c255;if(!_0x35405e[_0x2fc735(0x3b5)]())return![];if(!_0x35405e[_0x2fc735(0x46a)]())return![];if(!_0x35405e[_0x2fc735(0x38a)]())return![];return VisuMZ[_0x2fc735(0x2cc)][_0x2fc735(0x4b3)][_0x2fc735(0x431)][_0x2fc735(0x617)];},Window_BattleLog[_0x53c255(0x819)]['autoMeleeSingleTargetActionSet']=function(_0x26dc55,_0x158e37,_0x4b9b82){const _0x33a304=_0x53c255,_0x4c6bdb=_0x26dc55[_0x33a304(0x567)]()[_0x33a304(0x3ce)]<0x2,_0x5af63f=0x14,_0x13885b=0x30;_0x4c6bdb&&(this[_0x33a304(0x2bf)]('performJump',[_0x26dc55],_0x13885b,_0x5af63f),this[_0x33a304(0x2bf)](_0x33a304(0xe0),_0x26dc55,_0x4b9b82,_0x33a304(0x530),_0x5af63f,!![],_0x33a304(0x611),!![]),this[_0x33a304(0x2bf)]('requestMotion',[_0x26dc55],_0x33a304(0x718)),this[_0x33a304(0x2bf)](_0x33a304(0x5b2)));let _0x234d7e=_0x158e37[_0x33a304(0x2af)]()?this[_0x33a304(0x39e)](_0x26dc55):0x1;for(let _0x376eac=0x0;_0x376eac<_0x234d7e;_0x376eac++){_0x158e37[_0x33a304(0x2af)]()&&_0x26dc55[_0x33a304(0x807)]()&&this[_0x33a304(0x2bf)]('setActiveWeaponSet',_0x26dc55,_0x376eac),_0x158e37[_0x33a304(0x4b7)]()['animationId']<0x0?this[_0x33a304(0x67e)](_0x26dc55,_0x158e37,_0x4b9b82):this[_0x33a304(0x3bc)](_0x26dc55,_0x158e37,_0x4b9b82);}_0x158e37[_0x33a304(0x2af)]()&&_0x26dc55[_0x33a304(0x807)]()&&this['push'](_0x33a304(0x682),_0x26dc55);this[_0x33a304(0x2bf)](_0x33a304(0x652),_0x26dc55,_0x4b9b82,![]);if(_0x4c6bdb){const _0x3b4303=_0x26dc55['battler']();this[_0x33a304(0x2bf)](_0x33a304(0x694),[_0x26dc55],_0x13885b,_0x5af63f),this[_0x33a304(0x2bf)](_0x33a304(0x6e8),_0x26dc55,_0x3b4303[_0x33a304(0x67c)],_0x3b4303[_0x33a304(0x4a5)],_0x5af63f,![],_0x33a304(0x611)),this[_0x33a304(0x2bf)](_0x33a304(0x2a9),[_0x26dc55],_0x33a304(0x801)),this[_0x33a304(0x2bf)](_0x33a304(0x5b2)),this[_0x33a304(0x2bf)]('requestMotion',[_0x26dc55],_0x33a304(0x718));}},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x65f)]=function(_0x135b0a){const _0x317939=_0x53c255;if(!_0x135b0a[_0x317939(0x3b5)]())return![];if(!_0x135b0a[_0x317939(0x80a)]())return![];if(!_0x135b0a[_0x317939(0x38a)]())return![];return VisuMZ[_0x317939(0x2cc)][_0x317939(0x4b3)][_0x317939(0x431)][_0x317939(0x7a6)];},Window_BattleLog[_0x53c255(0x819)]['autoMeleeMultiTargetActionSet']=function(_0x57c156,_0x12bfaa,_0xb6ff6e){const _0x27761c=_0x53c255,_0x556726=_0x57c156['getAttackMotion']()[_0x27761c(0x3ce)]<0x2,_0x2eab01=0x14,_0x1e1861=0x30;_0x556726&&(this['push'](_0x27761c(0x694),[_0x57c156],_0x1e1861,_0x2eab01),this[_0x27761c(0x2bf)]('performMoveToTargets',_0x57c156,_0xb6ff6e,_0x27761c(0x39a),_0x2eab01,!![],_0x27761c(0x611),!![]),this[_0x27761c(0x2bf)](_0x27761c(0x2a9),[_0x57c156],'walk'),this[_0x27761c(0x2bf)](_0x27761c(0x5b2)));let _0x3ab00d=_0x12bfaa[_0x27761c(0x2af)]()?this[_0x27761c(0x39e)](_0x57c156):0x1;for(let _0x95fcbf=0x0;_0x95fcbf<_0x3ab00d;_0x95fcbf++){_0x12bfaa[_0x27761c(0x2af)]()&&_0x57c156[_0x27761c(0x807)]()&&this[_0x27761c(0x2bf)](_0x27761c(0x394),_0x57c156,_0x95fcbf),this['wholeActionSet'](_0x57c156,_0x12bfaa,_0xb6ff6e);}_0x12bfaa[_0x27761c(0x2af)]()&&_0x57c156[_0x27761c(0x807)]()&&this[_0x27761c(0x2bf)](_0x27761c(0x682),_0x57c156);this['push']('applyImmortal',_0x57c156,_0xb6ff6e,![]);if(_0x556726){const _0x34530e=_0x57c156[_0x27761c(0x5ef)]();this['push'](_0x27761c(0x694),[_0x57c156],_0x1e1861,_0x2eab01),this[_0x27761c(0x2bf)](_0x27761c(0x6e8),_0x57c156,_0x34530e[_0x27761c(0x67c)],_0x34530e[_0x27761c(0x4a5)],_0x2eab01,![],_0x27761c(0x611)),this[_0x27761c(0x2bf)](_0x27761c(0x2a9),[_0x57c156],'evade'),this[_0x27761c(0x2bf)](_0x27761c(0x5b2)),this['push']('requestMotion',[_0x57c156],_0x27761c(0x718));}},Window_BattleLog['prototype']['targetActionSet']=function(_0x2f8f03,_0x412123,_0xfdc30c){const _0x27dc57=_0x53c255,_0x227d08=_0x412123['item']();for(const _0x4d2052 of _0xfdc30c){if(!_0x4d2052)continue;this[_0x27dc57(0x2bf)]('performAction',_0x2f8f03,_0x412123),this[_0x27dc57(0x2bf)](_0x27dc57(0x5c0),Sprite_Battler[_0x27dc57(0x6fb)]),this['push']('showAnimation',_0x2f8f03,[_0x4d2052],_0x227d08[_0x27dc57(0x3b3)]),this[_0x27dc57(0x2bf)](_0x27dc57(0x5c0),0x18),this['push']('actionEffect',_0x2f8f03,_0x4d2052);}},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x3bc)]=function(_0x38527d,_0x48db0e,_0x1bfe7f){const _0x26bf77=_0x53c255,_0x5ceaba=_0x48db0e[_0x26bf77(0x4b7)]();this[_0x26bf77(0x2bf)](_0x26bf77(0x361),_0x38527d,_0x48db0e),this['push'](_0x26bf77(0x5c0),Sprite_Battler[_0x26bf77(0x6fb)]),this[_0x26bf77(0x2bf)](_0x26bf77(0x13a),_0x38527d,_0x1bfe7f[_0x26bf77(0x72a)](),_0x5ceaba[_0x26bf77(0x3b3)]),this[_0x26bf77(0x2bf)](_0x26bf77(0x579));for(const _0x34541c of _0x1bfe7f){if(!_0x34541c)continue;this['push'](_0x26bf77(0x22f),_0x38527d,_0x34541c);}},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x2f2)]=function(_0x165cb4,_0xd630a9,_0x568863){const _0x31750f=_0x53c255,_0x55524d=_0xd630a9[_0x31750f(0x4b7)]();this[_0x31750f(0x2bf)]('applyImmortal',_0x165cb4,_0x568863,![]),this[_0x31750f(0x2bf)]('waitForNewLine'),this[_0x31750f(0x2bf)](_0x31750f(0x61c)),this[_0x31750f(0x2bf)](_0x31750f(0x6fe)),this[_0x31750f(0x2bf)](_0x31750f(0x5a8),_0x165cb4),this[_0x31750f(0x2bf)](_0x31750f(0x5b2));},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x82c)]=function(_0x32906c){},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x36c)]=Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x497)],Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x497)]=function(_0x33894c){const _0x48ec3c=_0x53c255;if(!VisuMZ[_0x48ec3c(0x2cc)]['Settings'][_0x48ec3c(0x563)][_0x48ec3c(0x46c)])return;VisuMZ[_0x48ec3c(0x2cc)][_0x48ec3c(0x36c)][_0x48ec3c(0x6b9)](this,_0x33894c);},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x6f8)]=function(_0x59deff){const _0x534266=_0x53c255;this[_0x534266(0x2bf)](_0x534266(0x6c1),_0x59deff);VisuMZ[_0x534266(0x2cc)][_0x534266(0x4b3)]['ActionSequence']['CounterPlayback']&&this[_0x534266(0x2bf)]('showAnimation',_0x59deff,[BattleManager['_subject']],-0x1);if(!VisuMZ[_0x534266(0x2cc)][_0x534266(0x4b3)][_0x534266(0x563)][_0x534266(0x404)])return;this[_0x534266(0x2bf)]('addText',TextManager[_0x534266(0x669)]['format'](_0x59deff['name']()));},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x73f)]=function(_0x140620){const _0x3cf888=_0x53c255;this[_0x3cf888(0x2bf)](_0x3cf888(0x797),_0x140620);if(!VisuMZ['BattleCore'][_0x3cf888(0x4b3)]['BattleLog']['ShowReflect'])return;this[_0x3cf888(0x2bf)](_0x3cf888(0x5d3),TextManager[_0x3cf888(0x662)]['format'](_0x140620[_0x3cf888(0x701)]()));},Window_BattleLog['prototype'][_0x53c255(0x2f3)]=function(_0x3a6fd8,_0x355278){const _0x3c858e=_0x53c255;if(VisuMZ[_0x3c858e(0x2cc)][_0x3c858e(0x4b3)][_0x3c858e(0x431)][_0x3c858e(0x416)]){const _0x5ee01a=_0x355278[_0x3c858e(0x4b7)]();this[_0x3c858e(0x2bf)](_0x3c858e(0x13a),_0x3a6fd8,[_0x3a6fd8],_0x5ee01a[_0x3c858e(0x3b3)]);}},Window_BattleLog[_0x53c255(0x819)]['displaySubstitute']=function(_0x23ce11,_0x42cf15){const _0x37d014=_0x53c255;this[_0x37d014(0x2bf)](_0x37d014(0x358),_0x23ce11,_0x42cf15);if(!VisuMZ['BattleCore'][_0x37d014(0x4b3)][_0x37d014(0x563)][_0x37d014(0x53c)])return;const _0x44dcf4=_0x23ce11[_0x37d014(0x701)](),_0x3c7cd1=TextManager['substitute'][_0x37d014(0x107)](_0x44dcf4,_0x42cf15[_0x37d014(0x701)]());this[_0x37d014(0x2bf)](_0x37d014(0x5d3),_0x3c7cd1);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x437)]=Window_BattleLog[_0x53c255(0x819)]['displayFailure'],Window_BattleLog['prototype'][_0x53c255(0x3c4)]=function(_0x343d3d){const _0x212293=_0x53c255;if(!VisuMZ[_0x212293(0x2cc)][_0x212293(0x4b3)][_0x212293(0x563)][_0x212293(0x4b5)])return;VisuMZ['BattleCore'][_0x212293(0x437)][_0x212293(0x6b9)](this,_0x343d3d);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x332)]=Window_BattleLog['prototype']['displayCritical'],Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x732)]=function(_0xf1414c){const _0x5d7f58=_0x53c255;if(!VisuMZ[_0x5d7f58(0x2cc)]['Settings'][_0x5d7f58(0x563)][_0x5d7f58(0x400)])return;VisuMZ[_0x5d7f58(0x2cc)][_0x5d7f58(0x332)]['call'](this,_0xf1414c);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x39f)]=Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x5ca)],Window_BattleLog['prototype']['displayMiss']=function(_0x256725){const _0x2459eb=_0x53c255;!VisuMZ[_0x2459eb(0x2cc)]['Settings']['BattleLog'][_0x2459eb(0x3b9)]?this[_0x2459eb(0x2bf)]('performMiss',_0x256725):VisuMZ[_0x2459eb(0x2cc)]['Window_BattleLog_displayMiss']['call'](this,_0x256725);},VisuMZ[_0x53c255(0x2cc)]['Window_BattleLog_displayEvasion']=Window_BattleLog[_0x53c255(0x819)]['displayEvasion'],Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x505)]=function(_0x2717dd){const _0x4760fb=_0x53c255;!VisuMZ['BattleCore'][_0x4760fb(0x4b3)]['BattleLog'][_0x4760fb(0x3b9)]?_0x2717dd[_0x4760fb(0x284)]()['physical']?this[_0x4760fb(0x2bf)]('performEvasion',_0x2717dd):this['push'](_0x4760fb(0x53e),_0x2717dd):VisuMZ[_0x4760fb(0x2cc)]['Window_BattleLog_displayEvasion'][_0x4760fb(0x6b9)](this,_0x2717dd);},Window_BattleLog['prototype'][_0x53c255(0x298)]=function(_0x4be677){const _0x3c49e3=_0x53c255;_0x4be677[_0x3c49e3(0x284)]()[_0x3c49e3(0x3fa)]&&(_0x4be677[_0x3c49e3(0x284)]()[_0x3c49e3(0x333)]>0x0&&!_0x4be677[_0x3c49e3(0x284)]()[_0x3c49e3(0x4f3)]&&this[_0x3c49e3(0x2bf)](_0x3c49e3(0xfd),_0x4be677),_0x4be677[_0x3c49e3(0x284)]()[_0x3c49e3(0x333)]<0x0&&this[_0x3c49e3(0x2bf)](_0x3c49e3(0x463),_0x4be677),VisuMZ['BattleCore'][_0x3c49e3(0x4b3)][_0x3c49e3(0x563)][_0x3c49e3(0x3e7)]&&this[_0x3c49e3(0x2bf)]('addText',this[_0x3c49e3(0x708)](_0x4be677)));},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x127)]=Window_BattleLog['prototype'][_0x53c255(0x16f)],Window_BattleLog['prototype'][_0x53c255(0x16f)]=function(_0x4a6816){const _0x5b8230=_0x53c255;if(!VisuMZ[_0x5b8230(0x2cc)][_0x5b8230(0x4b3)]['BattleLog'][_0x5b8230(0x2c4)])return;VisuMZ['BattleCore'][_0x5b8230(0x127)][_0x5b8230(0x6b9)](this,_0x4a6816);},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x833)]=Window_BattleLog['prototype'][_0x53c255(0x39c)],Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x39c)]=function(_0x2d7f95){const _0x1038d6=_0x53c255;if(!VisuMZ[_0x1038d6(0x2cc)][_0x1038d6(0x4b3)]['BattleLog'][_0x1038d6(0x675)])return;VisuMZ[_0x1038d6(0x2cc)][_0x1038d6(0x833)]['call'](this,_0x2d7f95);},Window_BattleLog[_0x53c255(0x819)]['displayAddedStates']=function(_0x49aaef){const _0x51e2e3=_0x53c255,_0x3368da=_0x49aaef[_0x51e2e3(0x284)](),_0x4f74ef=_0x3368da[_0x51e2e3(0x345)]();for(const _0x27f3af of _0x4f74ef){const _0x481d14=_0x49aaef[_0x51e2e3(0x807)]()?_0x27f3af['message1']:_0x27f3af[_0x51e2e3(0x229)];_0x481d14&&VisuMZ[_0x51e2e3(0x2cc)][_0x51e2e3(0x4b3)]['BattleLog'][_0x51e2e3(0x592)]&&(this[_0x51e2e3(0x2bf)](_0x51e2e3(0x383)),this[_0x51e2e3(0x2bf)](_0x51e2e3(0x65a)),this[_0x51e2e3(0x2bf)]('addText',_0x481d14[_0x51e2e3(0x107)](_0x49aaef[_0x51e2e3(0x701)]())),this['push'](_0x51e2e3(0x55f))),_0x27f3af['id']===_0x49aaef[_0x51e2e3(0x804)]()&&this[_0x51e2e3(0x2bf)](_0x51e2e3(0x74d),_0x49aaef);}},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x11d)]=function(_0x4bcf12){const _0x3b04c0=_0x53c255;if(!VisuMZ[_0x3b04c0(0x2cc)][_0x3b04c0(0x4b3)][_0x3b04c0(0x563)]['ShowRemovedState'])return;const _0x36b3d6=_0x4bcf12[_0x3b04c0(0x284)](),_0x1d2bb9=_0x36b3d6[_0x3b04c0(0x706)]();for(const _0x5ea23f of _0x1d2bb9){_0x5ea23f[_0x3b04c0(0x44b)]&&(this['push'](_0x3b04c0(0x383)),this['push'](_0x3b04c0(0x65a)),this[_0x3b04c0(0x2bf)](_0x3b04c0(0x5d3),_0x5ea23f[_0x3b04c0(0x44b)]['format'](_0x4bcf12[_0x3b04c0(0x701)]())),this[_0x3b04c0(0x2bf)]('wait'));}},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x1f0)]=function(_0x392b17){const _0x266863=_0x53c255,_0x4ceed1=VisuMZ['BattleCore'][_0x266863(0x4b3)][_0x266863(0x563)],_0x3961b6=_0x392b17[_0x266863(0x284)]();if(_0x4ceed1['ShowAddedBuff'])this[_0x266863(0x258)](_0x392b17,_0x3961b6[_0x266863(0xe2)],TextManager[_0x266863(0x565)]);if(_0x4ceed1[_0x266863(0x493)])this['displayBuffs'](_0x392b17,_0x3961b6[_0x266863(0x4fa)],TextManager[_0x266863(0x50c)]);if(_0x4ceed1[_0x266863(0x23f)])this[_0x266863(0x258)](_0x392b17,_0x3961b6[_0x266863(0x4f4)],TextManager['buffRemove']);},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x258)]=function(_0x5db590,_0x119a53,_0x20cde7){const _0x366e20=_0x53c255;for(const _0xc7259a of _0x119a53){const _0x376c9b=_0x20cde7[_0x366e20(0x107)](_0x5db590[_0x366e20(0x701)](),TextManager['param'](_0xc7259a));this['push'](_0x366e20(0x383)),this['push']('pushBaseLine'),this['push']('addText',_0x376c9b),this['push']('wait');}},VisuMZ['BattleCore'][_0x53c255(0x256)]=Window_BattleLog[_0x53c255(0x819)]['clear'],Window_BattleLog[_0x53c255(0x819)]['clear']=function(){const _0x5dd1c5=_0x53c255;VisuMZ[_0x5dd1c5(0x2cc)][_0x5dd1c5(0x256)][_0x5dd1c5(0x6b9)](this),this[_0x5dd1c5(0x1a3)]();},VisuMZ['BattleCore'][_0x53c255(0x22d)]=Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x65a)],Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x65a)]=function(){const _0x223afc=_0x53c255;VisuMZ['BattleCore'][_0x223afc(0x22d)][_0x223afc(0x6b9)](this),this[_0x223afc(0x1a3)]();},VisuMZ[_0x53c255(0x2cc)]['Window_BattleLog_popBaseLine']=Window_BattleLog['prototype']['popBaseLine'],Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x383)]=function(){const _0x595bf6=_0x53c255;VisuMZ[_0x595bf6(0x2cc)][_0x595bf6(0x4cb)]['call'](this),this[_0x595bf6(0x4fc)](),this[_0x595bf6(0x1a3)]();},VisuMZ['BattleCore']['Window_BattleLog_popupDamage']=Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x500)],Window_BattleLog['prototype'][_0x53c255(0x500)]=function(_0x27307d){const _0x1ce51e=_0x53c255;VisuMZ[_0x1ce51e(0x2cc)]['Window_BattleLog_popupDamage'][_0x1ce51e(0x6b9)](this,_0x27307d),this[_0x1ce51e(0x1a3)]();},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x18d)]=function(){const _0xff4792=_0x53c255;let _0x245786=0x0;this[_0xff4792(0x75c)][_0xff4792(0x343)]>0x0&&(_0x245786=this[_0xff4792(0x75c)][this[_0xff4792(0x75c)][_0xff4792(0x343)]-0x1]),this['_lines'][_0xff4792(0x343)]>_0x245786?this[_0xff4792(0x55f)]():this[_0xff4792(0x1a3)]();},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x2ca)]=Window_BattleLog[_0x53c255(0x819)]['performActionStart'],Window_BattleLog['prototype'][_0x53c255(0x5d1)]=function(_0x5ea44d,_0x951c61){const _0x1b0dc2=_0x53c255;VisuMZ[_0x1b0dc2(0x2cc)][_0x1b0dc2(0x2ca)][_0x1b0dc2(0x6b9)](this,_0x5ea44d,_0x951c61),this[_0x1b0dc2(0x1a3)]();},VisuMZ['BattleCore'][_0x53c255(0x1a5)]=Window_BattleLog['prototype']['performAction'],Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x361)]=function(_0x3b476b,_0x4e8e64){const _0x7b78a1=_0x53c255;VisuMZ[_0x7b78a1(0x2cc)]['Window_BattleLog_performAction'][_0x7b78a1(0x6b9)](this,_0x3b476b,_0x4e8e64),this[_0x7b78a1(0x1a3)]();},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x3ba)]=Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x5a8)],Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x5a8)]=function(_0x3bd920){const _0x48837e=_0x53c255;VisuMZ[_0x48837e(0x2cc)][_0x48837e(0x3ba)]['call'](this,_0x3bd920);for(const _0x2a2ab1 of BattleManager[_0x48837e(0x36a)]()){if(!_0x2a2ab1)continue;if(_0x2a2ab1[_0x48837e(0x564)]())continue;_0x2a2ab1['performActionEndMembers']();}this['callNextMethod']();},VisuMZ[_0x53c255(0x2cc)]['Window_BattleLog_performDamage']=Window_BattleLog[_0x53c255(0x819)][_0x53c255(0xfd)],Window_BattleLog[_0x53c255(0x819)][_0x53c255(0xfd)]=function(_0x3b2518){const _0x13ce31=_0x53c255;VisuMZ[_0x13ce31(0x2cc)][_0x13ce31(0x42a)][_0x13ce31(0x6b9)](this,_0x3b2518),this['callNextMethod']();},VisuMZ[_0x53c255(0x2cc)]['Window_BattleLog_performMiss']=Window_BattleLog['prototype']['performMiss'],Window_BattleLog['prototype'][_0x53c255(0x547)]=function(_0x51dbfd){const _0x13a7ec=_0x53c255;VisuMZ[_0x13a7ec(0x2cc)]['Window_BattleLog_performMiss']['call'](this,_0x51dbfd),this['callNextMethod']();},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x825)]=Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x463)],Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x463)]=function(_0x32616b){const _0x24ae7e=_0x53c255;VisuMZ[_0x24ae7e(0x2cc)][_0x24ae7e(0x825)][_0x24ae7e(0x6b9)](this,_0x32616b),this['callNextMethod']();},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x329)]=Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x5d4)],Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x5d4)]=function(_0x45c743){const _0xdf74e0=_0x53c255;VisuMZ[_0xdf74e0(0x2cc)][_0xdf74e0(0x329)]['call'](this,_0x45c743),this[_0xdf74e0(0x1a3)]();},VisuMZ['BattleCore'][_0x53c255(0x2ad)]=Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x53e)],Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x53e)]=function(_0x2e8a82){const _0x23636e=_0x53c255;VisuMZ[_0x23636e(0x2cc)][_0x23636e(0x2ad)][_0x23636e(0x6b9)](this,_0x2e8a82),this[_0x23636e(0x1a3)]();},VisuMZ[_0x53c255(0x2cc)]['Window_BattleLog_performCounter']=Window_BattleLog['prototype'][_0x53c255(0x6c1)],Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x6c1)]=function(_0x46b9e3){const _0x1f6e8b=_0x53c255;VisuMZ[_0x1f6e8b(0x2cc)][_0x1f6e8b(0x25e)][_0x1f6e8b(0x6b9)](this,_0x46b9e3),this[_0x1f6e8b(0x1a3)]();},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x6c6)]=Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x797)],Window_BattleLog['prototype'][_0x53c255(0x797)]=function(_0x1a33f4){const _0x28b48f=_0x53c255;VisuMZ[_0x28b48f(0x2cc)][_0x28b48f(0x6c6)]['call'](this,_0x1a33f4),this[_0x28b48f(0x1a3)]();},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x2c2)]=Window_BattleLog[_0x53c255(0x819)]['performSubstitute'],Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x358)]=function(_0x4d1471,_0x2b2d73){const _0x282393=_0x53c255;VisuMZ[_0x282393(0x2cc)][_0x282393(0x2c2)]['call'](this,_0x4d1471,_0x2b2d73),this['callNextMethod']();},VisuMZ[_0x53c255(0x2cc)][_0x53c255(0x249)]=Window_BattleLog[_0x53c255(0x819)]['performCollapse'],Window_BattleLog[_0x53c255(0x819)]['performCollapse']=function(_0x4539e9){const _0x26b5d3=_0x53c255;VisuMZ[_0x26b5d3(0x2cc)][_0x26b5d3(0x249)]['call'](this,_0x4539e9),this['callNextMethod']();},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x7d6)]=function(_0x40802b,_0x3ddb92){const _0x315b99=_0x53c255;_0x40802b[_0x315b99(0x7d6)](_0x3ddb92),this['callNextMethod']();},Window_BattleLog[_0x53c255(0x819)][_0x53c255(0x20d)]=function(_0x5d142e,_0x1e6d3a){const _0x36c9b8=_0x53c255,_0x55b95b=_0x5d142e[_0x36c9b8(0x1cc)]();_0x55b95b<=0x0?SoundManager['playEnemyAttack']():this[_0x36c9b8(0x487)](_0x1e6d3a,_0x55b95b);},Window_BattleLog[_0x53c255(0x819)]['applyImmortal']=function(_0x2706b4,_0xd6deef,_0x390b03){const _0x2735d4=_0x53c255,_0x246055=[_0x2706b4][_0x2735d4(0x733)](_0xd6deef);for(const _0x2c2f54 of _0x246055){if(!_0x2c2f54)continue;_0x2c2f54[_0x2735d4(0x3e2)](_0x390b03);}this[_0x2735d4(0x1a3)]();},Window_BattleLog['prototype'][_0x53c255(0x5c0)]=function(_0x5c2347){const _0xbdbde9=_0x53c255;this[_0xbdbde9(0x239)]=_0x5c2347;},Window_BattleLog['prototype'][_0x53c255(0x2a9)]=function(_0x335435,_0x5378b2){const _0x204e98=_0x53c255;for(const _0x128572 of _0x335435){if(!_0x128572)continue;_0x128572[_0x204e98(0x2a9)](_0x5378b2);}this[_0x204e98(0x1a3)]();},Window_BattleLog[_0x53c255(0x819)]['performMoveToPoint']=function(_0x37176a,_0x2b3146,_0x453aa0,_0x224ca7,_0x17a2f6,_0x26ba4b){_0x37176a['moveBattlerToPoint'](_0x2b3146,_0x453aa0,_0x224ca7,_0x17a2f6,_0x26ba4b,-0x1),this['callNextMethod']();},Window_BattleLog['prototype'][_0x53c255(0xe0)]=function(_0x11b881,_0x4b40fc,_0x2f9acf,_0x14437d,_0x595716,_0x170c04,_0x4ac4a8){const _0x3f89f6=_0x53c255,_0xd61aa1=Math['min'](..._0x4b40fc['map'](_0x18eef4=>_0x18eef4['battler']()[_0x3f89f6(0x396)]-_0x18eef4[_0x3f89f6(0x5ef)]()[_0x3f89f6(0x491)]()/0x2)),_0x2cdf58=Math[_0x3f89f6(0x776)](..._0x4b40fc[_0x3f89f6(0x19c)](_0x178148=>_0x178148[_0x3f89f6(0x5ef)]()[_0x3f89f6(0x396)]+_0x178148[_0x3f89f6(0x5ef)]()[_0x3f89f6(0x491)]()/0x2)),_0x2da2fc=Math[_0x3f89f6(0x413)](..._0x4b40fc['map'](_0x85cd95=>_0x85cd95[_0x3f89f6(0x5ef)]()['_baseY']-_0x85cd95[_0x3f89f6(0x5ef)]()[_0x3f89f6(0x848)]())),_0x84549a=Math[_0x3f89f6(0x776)](..._0x4b40fc['map'](_0x2fa2bd=>_0x2fa2bd[_0x3f89f6(0x5ef)]()[_0x3f89f6(0x772)])),_0x613345=_0x4b40fc[_0x3f89f6(0x527)](_0x3f2543=>_0x3f2543['isActor']())[_0x3f89f6(0x343)],_0x37b4a4=_0x4b40fc[_0x3f89f6(0x527)](_0x478da3=>_0x478da3['isEnemy']())[_0x3f89f6(0x343)];let _0x587c8d=0x0,_0x8a1b9e=0x0;if(_0x2f9acf[_0x3f89f6(0x473)](/front/i))_0x587c8d=_0x613345>=_0x37b4a4?_0xd61aa1:_0x2cdf58;else{if(_0x2f9acf['match'](/middle/i))_0x587c8d=(_0xd61aa1+_0x2cdf58)/0x2,_0x4ac4a8=-0x1;else _0x2f9acf[_0x3f89f6(0x473)](/back/i)&&(_0x587c8d=_0x613345>=_0x37b4a4?_0x2cdf58:_0xd61aa1);}if(_0x2f9acf['match'](/head/i))_0x8a1b9e=_0x2da2fc;else{if(_0x2f9acf[_0x3f89f6(0x473)](/center/i))_0x8a1b9e=(_0x2da2fc+_0x84549a)/0x2;else _0x2f9acf['match'](/base/i)&&(_0x8a1b9e=_0x84549a);}_0x11b881[_0x3f89f6(0x234)](_0x587c8d,_0x8a1b9e,_0x14437d,_0x595716,_0x170c04,_0x4ac4a8),this['callNextMethod']();},Window_BattleLog['prototype'][_0x53c255(0x694)]=function(_0x324eba,_0x7ac81c,_0x7b2407){const _0xb5d4df=_0x53c255;for(const _0x2a6d17 of _0x324eba){if(!_0x2a6d17)continue;_0x2a6d17[_0xb5d4df(0x1ce)](_0x7ac81c,_0x7b2407);}this[_0xb5d4df(0x1a3)]();};