/*:
 * @plugindesc v.0.5.0 Allows you to save party's inventory for loading in future.
 * @author Foobarz076
 * @url https://gitlab.com/foobarz076
 * @target MZ
 * @help
 * It allows you to save party's inventory for loading in future.
 * You may adjust how slots available in plugin settings.
 * 
 * # Usage
 * Here is three plugin commands available.
 * 
 * SaveInventory(int slot,
 *               boolean clear_current=false);
 * 
 *      Save you current inventory to the slot which you specified.
 *      If clear_current is set, your current inventory will be removed.
 * 
 * LoadInventory(int slot,
 *               boolean overwrite_current=false);
 * 
 *      Load your saved inventory from the slot which you
 *      specified to your current.
 *      If overwrite is set, your current inventory will be
 *      overwrited with saved, Or they will be added to your inventory.
 * 
 * ClearInventory();
 * 
 *      A standalone function for clearing your inventory manually.
 * 
 * # License
 *     Copyright (C) 2021 Foobarz076
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * @param max_slots
 * @text Max Slots
 * @type number
 * @min 0
 * @default 4
 * @desc How many slots are available?
 * 
 * @command SaveInventory
 * @desc Save inventory to slot.
 * @arg slot
 * @text Slot
 * @desc Which Slot ?.
 * @type number
 * @min 1
 * @default 1
 * 
 * @arg clear_current
 * @text Clear Current
 * @desc Emptying the existing inventory after saved?
 * @type boolean
 * @default false
 *
 * @command LoadInventory
 * @desc Load inventory from one slot.
 * @arg slot
 * @text Slot
 * @desc Which Slot you want to load?
 * @type number
 * @min 1
 * @default 1
 * 
 * @arg overwrite_current
 * @text Overwrite Current
 * @desc Overwrite the existing inventory with saved?
 * @type boolean
 * @default false
 * 
 * @command ClearInventory
 * @desc Clear inventory manually.
 * 
*/

var Imported = Imported || {};
var Foo = {};
Foo.MaxSlots = Number(PluginManager.parameters('Foo_InventorySave')["Max Slots"]);

function slotData(gold = 0, items = null) {
    if (!items) {
        items = {}
    };
    return { 'gold': gold, 'items': items }
};

function initSlots(maxSlots) {
    var slots = [];
    slots[0] = {};
    for (var i = 0; i < maxSlots; i++) {
        slots.push(slotData());
    }
    return slots;
}

function clearInventory() {
    $gameParty._gold = 0;
    $gameParty._items = {};
}

function mergeInventory(source, add) {
    var result = { ...source };
    for (key in add) {
        if (source[key]) {
            result[key] += add[key];
        } else {
            result[key] = add[key];
        }
    }
    return result;
}



Game_System.prototype.FooInventorySlots = function () {
    if (!this._FooInventorySlots) {
        this._FooInventorySlots = initSlots(Foo.MaxSlots);
    }
    return this._FooInventorySlots;
};

Game_System.prototype.setFooInventorySlots = function (slot, value) {
    this._FooInventorySlots = Game_System.prototype.FooInventorySlots()
    this._FooInventorySlots[slot] = value;
};


PluginManager.registerCommand('Foo_InventorySave', 'ClearInventory', function (args) {
    clearInventory();
});

PluginManager.registerCommand('Foo_InventorySave', 'SaveInventory', function (args) {
    const clear_current = eval(args.clear_current);
    const slot = Number(eval(args.slot));
    var currentSlot = slotData(gold = $gameParty._gold, items = $gameParty._items);
    $gameSystem.setFooInventorySlots(slot, currentSlot);
    if (clear_current) {
        clearInventory();
    }
});

PluginManager.registerCommand('Foo_InventorySave', 'LoadInventory', function (args) {
    const overwrite_current = eval(args.overwrite_current);
    const slot = Number(eval(args.slot));
    var loadedSlot = $gameSystem.FooInventorySlots()[slot];
    if (overwrite_current) {
        clearInventory();
        $gameParty._gold = loadedSlot['gold'];
        $gameParty._items = loadedSlot['items'];
    } else {
        $gameParty._gold += loadedSlot['gold'];
        $gameParty._items = mergeInventory($gameParty._items,loadedSlot['items']);
    }
});
