//=============================================================================
// VisuStella MZ - Skills & States Core
// VisuMZ_1_SkillsStatesCore.js
//=============================================================================

var Imported = Imported || {};
Imported.VisuMZ_1_SkillsStatesCore = true;

var VisuMZ = VisuMZ || {};
VisuMZ.SkillsStatesCore = VisuMZ.SkillsStatesCore || {};
VisuMZ.SkillsStatesCore.version = 1.12;

//=============================================================================
 /*:
 * @target MZ
 * @plugindesc [RPG Maker MZ] [Tier 1] [Version 1.12] [SkillsStatesCore]
 * @author VisuStella
 * @url http://www.yanfly.moe/wiki/Skills_and_States_Core_VisuStella_MZ
 * @orderAfter VisuMZ_0_CoreEngine
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 *
 * The Skills & States Core plugin extends and builds upon the functionality of
 * RPG Maker MZ's inherent skill, state, and buff functionalities and allows
 * game devs to customize its various aspects.
 *
 * Features include all (but not limited to) the following:
 * 
 * * Assigning multiple Skill Types to Skills.
 * * Making custom Skill Cost Types (such as HP, Gold, and Items).
 * * Allowing Skill Costs to become percentile-based or dynamic either directly
 *   through the Skills themselves or through trait-like notetags.
 * * Replacing gauges for different classes to display different types of
 *   Skill Cost Type resources.
 * * Hiding/Showing and enabling/disabling skills based on switches, learned
 *   skills, and code.
 * * Setting rulings for states, including if they're cleared upon death, how
 *   reapplying the state affects their turn count, and more.
 * * Allowing states to be categorized and affected by categories, too.
 * * Displaying turn counts on states drawn in the window or on sprites.
 * * Manipulation of state, buff, and debuff turns through skill and item
 *   effect notetags.
 * * Create custom damage over time state calculations through notetags.
 * * Allow database objects to apply passive states to its user.
 * * Passive states can have conditions before they become active as well.
 * * Updated Skill Menu Scene layout to fit more modern appearances.
 * * Added bonus if Items & Equips Core is installed to utilize the Shop Status
 *   Window to display skill data inside the Skill Menu.
 * * Control over various aspects of the Skill Menu Scene.
 *
 * ============================================================================
 * Requirements
 * ============================================================================
 *
 * This plugin is made for RPG Maker MZ. This will not work in other iterations
 * of RPG Maker.
 *
 * ------ Tier 1 ------
 *
 * This plugin is a Tier 1 plugin. Place it under other plugins of lower tier
 * value on your Plugin Manager list (ie: 0, 1, 2, 3, 4, 5). This is to ensure
 * that your plugins will have the best compatibility with the rest of the
 * VisuStella MZ library.
 *
 * ============================================================================
 * Major Changes
 * ============================================================================
 *
 * This plugin adds some new hard-coded features to RPG Maker MZ's functions.
 * The following is a list of them.
 *
 * ---
 *
 * Buff & Debuff Level Management
 *
 * - In RPG Maker MZ, buffs and debuffs when applied to one another will shift
 * the buff modifier level up or down. This plugin will add an extra change to
 * the mechanic by making it so that once the buff modifier level reaches a
 * neutral point, the buff or debuff is removed altogether and resets the buff
 * and debuff turn counter for better accuracy.
 *
 * ---
 *
 * Skill Costs
 *
 * - In RPG Maker MZ, skill costs used to be hard-coded. Now, all Skill Cost
 * Types are now moved to the Plugin Parameters, including MP and TP. This
 * means that from payment to checking for them, it's all done through the
 * options available.
 *
 * - By default in RPG Maker MZ, displayed skill costs would only display only
 * one type: TP if available, then MP. If a skill costs both TP and MP, then
 * only TP was displayed. This plugin changes that aspect by displaying all the
 * cost types available in order of the Plugin Parameter Skill Cost Types.
 *
 * - By default in RPG Maker MZ, displayed skill costs were only color-coded.
 * This plugin changes that aspect by displaying the Skill Cost Type's name
 * alongside the cost. This is to help color-blind players distinguish what
 * costs a skill has.
 *
 * ---
 *
 * Sprite Gauges
 *
 * - Sprite Gauges in RPG Maker MZ by default are hard-coded and only work for
 * HP, MP, TP, and Time (used for ATB). This plugin makes it possible for them
 * to be customized through the use of Plugin Parameters under the Skill Cost
 * Types and their related-JavaScript entries.
 *
 * ---
 * 
 * State Displays
 * 
 * - To put values onto states and display them separately from the state turns
 * you can use the following script calls.
 * 
 *   battler.getStateDisplay(stateId)
 *   - This returns whatever value is stored for the specified battler under
 *     that specific state value.
 *   - If there is no value to be returned it will return an empty string.
 * 
 *   battler.setStateDisplay(stateId, value)
 *   - This sets the display for the battler's specific state to whatever you
 *     declared as the value.
 *   - The value is best used as a number or a string.
 * 
 *   battler.clearStateDisplay(stateId)
 *   - This clears the display for the battler's specific state.
 *   - In short, this sets the stored display value to an empty string.
 * 
 * ---
 *
 * Window Functions Moved
 *
 * - Some functions found in RPG Maker MZ's default code for Window_StatusBase
 * and Window_SkillList are now moved to Window_Base to make the functions
 * available throughout all windows for usage.
 *
 * ---
 *
 * ============================================================================
 * Notetags
 * ============================================================================
 *
 * The following are notetags that have been added through this plugin. These
 * notetags will not work with your game if this plugin is OFF or not present.
 *
 * === General Skill Notetags ===
 *
 * The following are general notetags that are skill-related.
 *
 * ---
 *
 * <Skill Type: x>
 * <Skill Types: x,x,x>
 *
 * <Skill Type: name>
 * <Skill Types: name, name, name>
 *
 * - Used for: Skill Notetags
 * - Marks the skill to have multiple Skill Types, meaning they would appear
 *   under different skill types without needing to create duplicate skills.
 * - Replace 'x' with a number value representing the Skill Type's ID.
 * - If using 'name' notetag variant, replace 'name' with the Skill Type(s)
 *   name desired to be added.
 *
 * ---
 *
 * === Skill Cost Notetags ===
 *
 * The following are notetags that can be used to adjust skill costs. Some of
 * these notetags are added through the Plugin Parameter: Skill Cost Types and
 * can be altered there. This also means that some of these notetags can have
 * their functionality altered and/or removed.
 *
 * ---
 *
 * <type Cost: x>
 * <type Cost: x%>
 *
 * - Used for: Skill Notetags
 * - These notetags are used to designate costs of custom or already existing
 *   types that cannot be made by the Database Editor.
 * - Replace 'type' with a resource type. Existing ones found in the Plugin
 *   Parameters are 'HP', 'MP', 'TP', 'Gold', and 'Potion'. More can be added.
 * - Replace 'x' with a number value to determine the exact type cost value.
 *   This lets you bypass the Database Editor's limit of 9,999 MP and 100 TP.
 * - The 'x%' version is replaced with a percentile value to determine a cost
 *   equal to a % of the type's maximum quantity limit.
 * - Functionality for these notetags can be altered in the Plugin Parameters.
 *
 * Examples:
 *   <HP Cost: 500>
 *   <MP Cost: 25%>
 *   <Gold Cost: 3000>
 *   <Potion Cost: 5>
 *
 * ---
 *
 * <type Cost Max: x>
 * <type Cost Min: x>
 *
 * - Used for: Skill Notetags
 * - These notetags are used to ensure conditional and % costs don't become too
 *   large or too small.
 * - Replace 'type' with a resource type. Existing ones found in the Plugin
 *   Parameters are 'HP', 'MP', 'TP', 'Gold', and 'Potion'. More can be added.
 * - Replace 'x' with a number value to determine the maximum or minimum values
 *   that the cost can be.
 * - Functionality for these notetags can be altered in the Plugin Parameters.
 *
 * Examples:
 *   <HP Cost Max: 1500>
 *   <MP Cost Min: 5>
 *   <Gold Cost Max: 10000>
 *   <Potion Cost Min: 3>
 *
 * ---
 *
 * <type Cost: +x>
 * <type Cost: -x>
 *
 * <type Cost: x%>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Replace 'type' with a resource type. Existing ones found in the Plugin
 *   Parameters are 'HP', 'MP', 'TP', 'Gold', and 'Potion'. More can be added.
 * - For % notetag variant: Replace 'x' with a number value to determine the
 *   rate to adjust the Skill Cost Type by as a flat value. This is applied
 *   before <type Cost: +x> and <type Cost: -x> notetags.
 * - For + and - notetag variants: Replace 'x' with a number value to determine
 *   how much to adjust the Skill Cost Type by as a flat value. This is applied
 *   after <type Cost: x%> notetags.
 * - Functionality for these notetags can be altered in the Plugin Parameters.
 *
 * Examples:
 *   <HP Cost: +20>
 *   <MP Cost: -10>
 *   <Gold Cost: 50%>
 *   <Potion Cost: 200%>
 *
 * ---
 *
 * <Custom Cost Text>
 *  text
 * </Custom Cost Text>
 *
 * - Used for: Skill Notetags
 * - Allows you to insert custom text into the skill's cost area towards the
 *   end of the costs.
 * - Replace 'text' with the text you wish to display.
 * - Text codes may be used.
 *
 * ---
 *
 * === JavaScript Notetags: Skill Costs ===
 *
 * The following are notetags made for users with JavaScript knowledge to
 * determine any dynamic Skill Cost Types used for particular skills.
 *
 * ---
 *
 * <JS type Cost>
 *  code
 *  code
 *  cost = code;
 * </JS type Cost>
 *
 * - Used for: Skill Notetags
 * - Replace 'type' with a resource type. Existing ones found in the Plugin
 *   Parameters are 'HP', 'MP', 'TP', 'Gold', and 'Potion'. More can be added.
 * - Replace 'code' to determine the type 'cost' of the skill.
 * - Insert the final type cost into the 'cost' variable.
 * - The 'user' variable refers to the user about to perform the skill.
 * - The 'skill' variable refers to the skill being used.
 * - Functionality for the notetag can be altered in the Plugin Parameters.
 *
 * ---
 *
 * === Gauge Replacement Notetags ===
 *
 * Certain classes can have their gauges swapped out for other Skill Cost
 * Types. This is especially helpful for the classes that don't utilize those
 * Skill Cost Types. You can mix and match them however you want.
 *
 * ---
 *
 * <Replace HP Gauge: type>
 * <Replace MP Gauge: type>
 * <Replace TP Gauge: type>
 *
 * - Used for: Class Notetags
 * - Replaces the HP (1st), MP (2nd), or TP (3rd) gauge with a different Skill
 *   Cost Type.
 * - Replace 'type' with a resource type. Existing ones found in the Plugin
 *   Parameters are 'HP', 'MP', 'TP', 'Gold', and 'Potion'. More can be added.
 * - Replace 'type' with 'none' to not display any gauges there.
 * - The <Replace TP Gauge: type> will require 'Display TP in Window' setting
 *   to be on in the Database > System 1 tab.
 * - Functionality for the notetags can be altered by changes made to the
 *   Skill & States Core Plugin Parameters.
 *
 * ---
 *
 * === Skill Accessibility Notetags ===
 *
 * Sometimes, you don't want all skills to be visible whether it be to hide
 * menu-only skills during battle, until certain switches are turned ON/OFF, or
 * until certain skills have been learned.
 *
 * ---
 *
 * <Hide in Battle>
 * <Hide outside Battle>
 *
 * - Used for: Skill Notetags
 * - Makes the specific skill visible or hidden depending on whether or not the
 *   player is currently in battle.
 *
 * ---
 *
 * <Show Switch: x>
 *
 * <Show All Switches: x,x,x>
 * <Show Any Switches: x,x,x>
 *
 * - Used for: Skill Notetags
 * - Determines the visibility of the skill based on switches.
 * - Replace 'x' with the switch ID to determine the skill's visibility.
 * - If 'All' notetag variant is used, skill will be hidden until all switches
 *   are ON. Then, it would be shown.
 * - If 'Any' notetag variant is used, skill will be shown if any of the
 *   switches are ON. Otherwise, it would be hidden.
 *
 * ---
 *
 * <Hide Switch: x>
 *
 * <Hide All Switches: x,x,x>
 * <Hide Any Switches: x,x,x>
 *
 * - Used for: Skill Notetags
 * - Determines the visibility of the skill based on switches.
 * - Replace 'x' with the switch ID to determine the skill's visibility.
 * - If 'All' notetag variant is used, skill will be shown until all switches
 *   are ON. Then, it would be hidden.
 * - If 'Any' notetag variant is used, skill will be hidden if any of the
 *   switches are ON. Otherwise, it would be shown.
 *
 * ---
 *
 * <Show if learned Skill: x>
 *
 * <Show if learned All Skills: x,x,x>
 * <Show if learned Any Skills: x,x,x>
 *
 * <Show if learned Skill: name>
 *
 * <Show if learned All Skills: name, name, name>
 * <Show if learned Any Skills: name, name, name>
 *
 * - Used for: Skill Notetags
 * - Determines the visibility of the skill based on skills learned.
 * - This does not apply to skills added by traits on actors, classes, any
 *   equipment, or states. These are not considered learned skills. They are
 *   considered temporary skills.
 * - Replace 'x' with the skill ID to determine the skill's visibility.
 * - If 'name' notetag viarant is used, replace 'name' with the skill's name to
 *   be checked for the notetag.
 * - If 'All' notetag variant is used, skill will be hidden until all skills
 *   are learned. Then, it would be shown.
 * - If 'Any' notetag variant is used, skill will be shown if any of the skills
 *   are learned. Otherwise, it would be hidden.
 *
 * ---
 *
 * <Hide if learned Skill: x>
 *
 * <Hide if learned All Skills: x,x,x>
 * <Hide if learned Any Skills: x,x,x>
 *
 * <Hide if learned Skill: name>
 *
 * <Hide if learned All Skills: name, name, name>
 * <Hide if learned Any Skills: name, name, name>
 *
 * - Used for: Skill Notetags
 * - Determines the visibility of the skill based on skills learned.
 * - This does not apply to skills added by traits on actors, classes, any
 *   equipment, or states. These are not considered learned skills. They are
 *   considered temporary skills.
 * - Replace 'x' with the skill ID to determine the skill's visibility.
 * - If 'name' notetag viarant is used, replace 'name' with the skill's name to
 *   be checked for the notetag.
 * - If 'All' notetag variant is used, skill will be shown until all skills
 *   are learned. Then, it would be hidden.
 * - If 'Any' notetag variant is used, skill will be hidden if any of the
 *   skills are learned. Otherwise, it would be shown.
 *
 * ---
 *
 * <Show if has Skill: x>
 *
 * <Show if have All Skills: x,x,x>
 * <Show if have Any Skills: x,x,x>
 *
 * <Show if has Skill: name>
 *
 * <Show if have All Skills: name, name, name>
 * <Show if have Any Skills: name, name, name>
 *
 * - Used for: Skill Notetags
 * - Determines the visibility of the skill based on skills available.
 * - This applies to both skills that have been learned and/or temporarily
 *   added through traits on actors, classes, equipment, or states.
 * - Replace 'x' with the skill ID to determine the skill's visibility.
 * - If 'name' notetag viarant is used, replace 'name' with the skill's name to
 *   be checked for the notetag.
 * - If 'All' notetag variant is used, skill will be hidden until all skills
 *   are learned. Then, it would be shown.
 * - If 'Any' notetag variant is used, skill will be shown if any of the skills
 *   are learned. Otherwise, it would be hidden.
 *
 * ---
 *
 * <Hide if has Skill: x>
 *
 * <Hide if have All Skills: x,x,x>
 * <Hide if have Any Skills: x,x,x>
 *
 * <Hide if has Skill: name>
 *
 * <Hide if have All Skills: name, name, name>
 * <Hide if have Any Skills: name, name, name>
 *
 * - Used for: Skill Notetags
 * - Determines the visibility of the skill based on skills available.
 * - This applies to both skills that have been learned and/or temporarily
 *   added through traits on actors, classes, equipment, or states.
 * - Replace 'x' with the skill ID to determine the skill's visibility.
 * - If 'name' notetag viarant is used, replace 'name' with the skill's name to
 *   be checked for the notetag.
 * - If 'All' notetag variant is used, skill will be shown until all skills
 *   are learned. Then, it would be hidden.
 * - If 'Any' notetag variant is used, skill will be hidden if any of the
 *   skills are learned. Otherwise, it would be shown.
 *
 * ---
 *
 * <Enable Switch: x>
 *
 * <Enable All Switches: x,x,x>
 * <Enable Any Switches: x,x,x>
 *
 * - Used for: Skill Notetags
 * - Determines the enabled status of the skill based on switches.
 * - Replace 'x' with the switch ID to determine the skill's enabled status.
 * - If 'All' notetag variant is used, skill will be disabled until all
 *   switches are ON. Then, it would be enabled.
 * - If 'Any' notetag variant is used, skill will be enabled if any of the
 *   switches are ON. Otherwise, it would be disabled.
 *
 * ---
 *
 * <Disable Switch: x>
 *
 * <Disable All Switches: x,x,x>
 * <Disable Any Switches: x,x,x>
 *
 * - Used for: Skill Notetags
 * - Determines the enabled status of the skill based on switches.
 * - Replace 'x' with the switch ID to determine the skill's enabled status.
 * - If 'All' notetag variant is used, skill will be enabled until all switches
 *   are ON. Then, it would be disabled.
 * - If 'Any' notetag variant is used, skill will be disabled if any of the
 *   switches are ON. Otherwise, it would be enabled.
 *
 * ---
 *
 * === JavaScript Notetags: Skill Accessibility ===
 *
 * The following are notetags made for users with JavaScript knowledge to
 * determine if a skill can be accessible visibly or through usage.
 *
 * ---
 *
 * <JS Skill Visible>
 *  code
 *  code
 *  visible = code;
 * </JS Skill Visible>
 *
 * - Used for: Skill Notetags
 * - Determines the visibility of the skill based on JavaScript code.
 * - Replace 'code' to determine the type visibility of the skill.
 * - The 'visible' variable returns a boolean (true/false) to determine if the
 *   skill will be visible or not.
 * - The 'user' variable refers to the user with the skill.
 * - The 'skill' variable refers to the skill being checked.
 * - All other visibility conditions must be met for this code to count.
 *
 * ---
 *
 * <JS Skill Enable>
 *  code
 *  code
 *  enabled = code;
 * </JS Skill Enable>
 *
 * - Used for: Skill Notetags
 * - Determines the enabled status of the skill based on JavaScript code.
 * - Replace 'code' to determine the type enabled status of the skill.
 * - The 'enabled' variable returns a boolean (true/false) to determine if the
 *   skill will be enabled or not.
 * - The 'user' variable refers to the user with the skill.
 * - The 'skill' variable refers to the skill being checked.
 * - All other skill conditions must be met in order for this to code to count.
 *
 * ---
 *
 * === General State-Related Notetags ===
 *
 * The following notetags are centered around states, such as how their turn
 * counts are displayed, items and skills that affect state turns, if the state
 * can avoid removal by death state, etc.
 *
 * ---
 *
 * <No Death Clear>
 *
 * - Used for: State Notetags
 * - Prevents this state from being cleared upon death.
 * - This allows this state to be added to an already dead battler, too.
 *
 * ---
 *
 * <No Recover All Clear>
 *
 * - Used for: State Notetags
 * - Prevents this state from being cleared upon using the Recover All command.
 *
 * ---
 *
 * <Group Defeat>
 *
 * - Used for: State Notetags
 * - If an entire party is affected by states with the <Group Defeat> notetag,
 *   they are considered defeated.
 * - Usage for this includes party-wide petrification, frozen, etc.
 *
 * ---
 *
 * <Reapply Rules: Ignore>
 * <Reapply Rules: Reset>
 * <Reapply Rules: Greater>
 * <Reapply Rules: Add>
 *
 * - Used for: State Notetags
 * - Choose what kind of rules this state follows if the state is being applied
 *   to a target that already has the state. This affects turns specifically.
 * - 'Ignore' will bypass any turn changes.
 * - 'Reset' will recalculate the state's turns.
 * - 'Greater' will choose to either keep the current turn count if it's higher
 *   than the reset amount or reset it if the current turn count is lower.
 * - 'Add' will add the state's turn count to the applied amount.
 * - If this notetag isn't used, it will use the rules set in the States >
 *   Plugin Parameters.
 *
 * ---
 *
 * <Positive State>
 * <Negative State>
 *
 * - Used for: State Notetags
 * - Marks the state as a positive state or negative state, also altering the
 *   state's turn count color to match the Plugin Parameter settings.
 * - This also puts the state into either the 'Positive' category or
 *   'Negative' category.
 *
 * ---
 *
 * <Category: name>
 * <Category: name, name, name>
 *
 * - Used for: State Notetags
 * - Arranges states into certain/multiple categories.
 * - Replace 'name' with a category name to mark this state as.
 * - Insert multiples of this to mark the state with  multiple categories.
 *
 * ---
 *
 * <Categories>
 *  name
 *  name
 * </Categories>
 *
 * - Used for: State Notetags
 * - Arranges states into certain/multiple categories.
 * - Replace each 'name' with a category name to mark this state as.
 *
 * ---
 *
 * <State x Category Remove: y>
 * 
 * <State x Category Remove: All>
 *
 * - Used for: Skill, Item Notetags
 * - Allows the skill/item to remove 'y' states from specific category 'x'.
 * - Replace 'x' with a category name to remove from.
 * - Replace 'y' with the number of times to remove from that category.
 * - Use the 'All' variant to remove all of the states of that category.
 * - Insert multiples of this to remove different types of categories.
 *
 * ---
 *
 * <Hide State Turns>
 *
 * - Used for: State Notetags
 * - Hides the state turns from being shown at all.
 * - This will by pass any Plugin Parameter settings.
 *
 * ---
 *
 * <Turn Color: x>
 * <Turn Color: #rrggbb>
 *
 * - Used for: State Notetags
 * - Hides the state turns from being shown at all.
 * - Determines the color of the state's turn count.
 * - Replace 'x' with a number value depicting a window text color.
 * - Replace 'rrggbb' with a hex color code for a more custom color.
 *
 * ---
 *
 * <State id Turns: +x>
 * <State id Turns: -x>
 *
 * <Set State id Turns: x>
 *
 * <State name Turns: +x>
 * <State name Turns: -x>
 *
 * <Set State name Turns: x>
 *
 * - Used for: Skill, Item Notetags
 * - If the target is affected by state 'id' or state 'name', change the state
 *   turn duration for target.
 * - For 'id' variant, replace 'id' with the ID of the state to modify.
 * - For 'name' variant, replace 'name' with the name of the state to modify.
 * - Replace 'x' with the value you wish to increase, decrease, or set to.
 * - Insert multiples of this notetag to affect multiple states at once.
 *
 * ---
 *
 * <param Buff Turns: +x>
 * <param Buff Turns: -x>
 *
 * <Set param Buff Turns: x>
 *
 * - Used for: Skill, Item Notetags
 * - If the target is affected by a 'param' buff, change that buff's turn
 *   duration for target.
 * - Replace 'param' with 'MaxHP', 'MaxMP', 'ATK', 'DEF', 'MAT', 'MDF', 'AGI',
 *   or 'LUK' to determine which parameter buff to modify.
 * - Replace 'x' with the value you wish to increase, decrease, or set to.
 * - Insert multiples of this notetag to affect multiple parameters at once.
 *
 * ---
 *
 * <param Debuff Turns: +x>
 * <param Debuff Turns: -x>
 *
 * <Set param Debuff Turns: x>
 *
 * - Used for: Skill, Item Notetags
 * - If the target is affected by a 'param' debuff, change that debuff's turn
 *   duration for target.
 * - Replace 'param' with 'MaxHP', 'MaxMP', 'ATK', 'DEF', 'MAT', 'MDF', 'AGI',
 *   or 'LUK' to determine which parameter debuff to modify.
 * - Replace 'x' with the value you wish to increase, decrease, or set to.
 * - Insert multiples of this notetag to affect multiple parameters at once.
 *
 * ---
 *
 * === JavaScript Notetags: On Add/Erase/Expire ===
 *
 * Using JavaScript code, you can use create custom effects that occur when a
 * state has bee added, erased, or expired.
 * 
 * ---
 *
 * <JS On Add State>
 *  code
 *  code
 * </JS On Add State>
 *
 * - Used for: State Notetags
 * - When a state is added, run the code added by this notetag.
 * - The 'user' variable refers to the current active battler.
 * - The 'target' variable refers to the battler affected by this state.
 * - The 'origin' variable refers to the one who applied this state.
 * - The 'state' variable refers to the current state being affected.
 *
 * ---
 *
 * <JS On Erase State>
 *  code
 *  code
 * </JS On Erase State>
 *
 * - Used for: State Notetags
 * - When a state is erased, run the code added by this notetag.
 * - The 'user' variable refers to the current active battler.
 * - The 'target' variable refers to the battler affected by this state.
 * - The 'origin' variable refers to the one who applied this state.
 * - The 'state' variable refers to the current state being affected.
 *
 * ---
 *
 * <JS On Expire State>
 *  code
 *  code
 * </JS On Expire State>
 *
 * - Used for: State Notetags
 * - When a state has expired, run the code added by this notetag.
 * - The 'user' variable refers to the current active battler.
 * - The 'target' variable refers to the battler affected by this state.
 * - The 'origin' variable refers to the one who applied this state.
 * - The 'state' variable refers to the current state being affected.
 *
 * ---
 *
 * === JavaScript Notetags: Slip Damage/Healing ===
 *
 * Slip Damage, in RPG Maker vocabulary, refers to damage over time. The
 * following notetags allow you to perform custom slip damage/healing.
 *
 * ---
 *
 * <JS type Slip Damage>
 *  code
 *  code
 *  damage = code;
 * </JS type Slip Damage>
 *
 * - Used for: State Notetags
 * - Code used to determine how much slip damage is dealt to the affected unit
 *   during each regeneration phase.
 * - Replace 'type' with 'HP', 'MP', or 'TP'.
 * - Replace 'code' with the calculations on what to determine slip damage.
 * - The 'user' variable refers to the origin of the state.
 * - The 'target' variable refers to the affected unit receiving the damage.
 * - The 'state' variable refers to the current state being affected.
 * - The 'damage' variable is the finalized slip damage to be dealt.
 *
 * ---
 *
 * <JS type Slip Heal>
 *  code
 *  code
 *  heal = code;
 * </JS type Slip Heal>
 *
 * - Used for: State Notetags
 * - Code used to determine how much slip healing is dealt to the affected unit
 *   during each regeneration phase.
 * - Replace 'type' with 'HP', 'MP', or 'TP'.
 * - Replace 'code' with the calculations on what to determine slip healing.
 * - The 'user' variable refers to the origin of the state.
 * - The 'target' variable refers to the affected unit receiving the healing.
 * - The 'state' variable refers to the current state being affected.
 * - The 'heal' variable is the finalized slip healing to be recovered.
 *
 * ---
 *
 * === Passive State Notetags ===
 *
 * Passive States are states that are always applied to actors and enemies
 * provided that their conditions have been met. These can be granted through
 * database objects or through the Passive States Plugin Parameters.
 * 
 * ---
 * 
 * For those using the code "a.isStateAffected(10)" to check if a target is
 * affected by a state or not, this does NOT check passive states. This only
 * checks for states that were directly applied to the target.
 * 
 * Instead, use "a.states().includes($dataStates[10])" to check for them. This
 * code will search for both directly applied states and passive states alike.
 *
 * ---
 *
 * <Passive State: x>
 * <Passive States: x,x,x>
 *
 * <Passive State: name>
 * <Passive States: name, name, name>
 *
 * - Used for: Actor, Class, Skill, Item, Weapon, Armor, Enemy Notetags
 * - Adds passive state(s) x to trait object, applying it to related actor or
 *   enemy unit(s).
 * - Replace 'x' with a number to determine which state to add as a passive.
 * - If using 'name' notetag variant, replace 'name' with the name of the
 *   state(s) to add as a passive.
 * - Note: If you plan on applying a passive state through a skill, it must be
 *   through a skill that has been learned by the target and not a skill that
 *   is given through a trait.
 *
 * ---
 *
 * <Passive Stackable>
 *
 * - Used for: State Notetags
 * - Makes it possible for this passive state to be added multiple times.
 * - Otherwise, only one instance of the passive state can be available.
 *
 * ---
 *
 * <Passive Condition Class: id>
 * <Passive Condition Classes: id, id, id>
 *
 * <Passive Condition Class: name>
 * <Passive Condition Classes: name, name, name>
 *
 * - Used for: State Notetags
 * - Determines the passive condition of the passive state based on the actor's
 *   current class. As long as the actor's current class matches one of the
 *   data entries, the passive condition is considered passed.
 * - For 'id' variant, replace 'id' with a number representing class's ID.
 * - For 'name' variant, replace 'name' with the class's name.
 *
 * ---
 *
 * <Passive Condition Multiclass: id>
 * <Passive Condition Multiclass: id, id, id>
 *
 * <Passive Condition Multiclass: name>
 * <Passive Condition Multiclass: name, name, name>
 *
 * - Used for: State Notetags
 * - Requires VisuMZ_2_ClassChangeSystem!
 * - Determines the passive condition of the passive state based on the actor's
 *   multiclasses. As long as the actor has any of the matching classes
 *   assigned as a multiclass, the passive condition is considered passed.
 * - For 'id' variant, replace 'id' with a number representing class's ID.
 * - For 'name' variant, replace 'name' with the class's name.
 *
 * ---
 *
 * <Passive Condition Switch ON: x>
 *
 * <Passive Condition All Switches ON: x,x,x>
 * <Passive Condition Any Switch ON: x,x,x>
 *
 * - Used for: State Notetags
 * - Determines the passive condition of the passive state based on switches.
 * - Replace 'x' with the switch ID to determine the state's passive condition.
 * - If 'All' notetag variant is used, conditions will not be met until all
 *   switches are ON. Then, it would be met.
 * - If 'Any' notetag variant is used, conditions will be met if any of the
 *   switches are ON. Otherwise, it would not be met.
 *
 * ---
 *
 * <Passive Condition Switch OFF: x>
 *
 * <Passive Condition All Switches OFF: x,x,x>
 * <Passive Condition Any Switch OFF: x,x,x>
 *
 * - Used for: State Notetags
 * - Determines the passive condition of the passive state based on switches.
 * - Replace 'x' with the switch ID to determine the state's passive condition.
 * - If 'All' notetag variant is used, conditions will not be met until all
 *   switches are OFF. Then, it would be met.
 * - If 'Any' notetag variant is used, conditions will be met if any of the
 *   switches are OFF. Otherwise, it would not be met.
 *
 * ---
 *
 * === JavaScript Notetags: Passive State ===
 *
 * The following is a notetag made for users with JavaScript knowledge to
 * determine if a passive state's condition can be met.
 *
 * ---
 *
 * <JS Passive Condition>
 *  code
 *  code
 *  condition = code;
 * </JS Passive Condition>
 *
 * - Used for: State Notetags
 * - Determines the passive condition of the state based on JavaScript code.
 * - Replace 'code' to determine if a passive state's condition has been met.
 * - The 'condition' variable returns a boolean (true/false) to determine if
 *   the passive state's condition is met or not.
 * - The 'user' variable refers to the user affected by the passive state.
 * - The 'state' variable refers to the passive state being checked.
 * - All other passive conditions must be met for this code to count.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: General Skill Settings
 * ============================================================================
 *
 * These Plugin Parameters adjust various aspects of the game regarding skills
 * from the custom Skill Menu Layout to global custom effects made in code.
 *
 * ---
 *
 * General
 * 
 *   Use Updated Layout:
 *   - Use the Updated Skill Menu Layout provided by this plugin?
 *   - This will automatically enable the Status Window.
 *   - This will override the Core Engine windows settings.
 *
 *   Layout Style:
 *   - If using an updated layout, how do you want to style the menu scene?
 *     - Upper Help, Left Input
 *     - Upper Help, Right Input
 *     - Lower Help, Left Input
 *     - Lower Help, Right Input
 *
 * ---
 *
 * Skill Type Window
 * 
 *   Style:
 *   - How do you wish to draw commands in the Skill Type Window?
 *   - Text Only: Display only the text.
 *   - Icon Only: Display only the icon.
 *   - Icon + Text: Display the icon first, then the text.
 *   - Auto: Determine which is better to use based on the size of the cell.
 * 
 *   Text Align:
 *   - Text alignment for the Skill Type Window.
 *
 * ---
 *
 * List Window
 * 
 *   Columns:
 *   - Number of maximum columns.
 *
 * ---
 *
 * Shop Status Window
 * 
 *   Show in Skill Menu?:
 *   - Show the Shop Status Window in the Skill Menu?
 *   - This is enabled if the Updated Layout is on.
 * 
 *   Adjust List Window?:
 *   - Automatically adjust the Skill List Window in the Skill Menu if using
 *     the Shop Status Window?
 * 
 *   Background Type:
 *   - Select background type for this window.
 *     - 0 - Window
 *     - 1 - Dim
 *     - 2 - Transparent
 * 
 *   JS: X, Y, W, H:
 *   - Code used to determine the dimensions for this Shop Status Window in the
 *     Skill Menu.
 *
 * ---
 *
 * Skill Types
 * 
 *   Hidden Skill Types:
 *   - Insert the ID's of the Skill Types you want hidden from view ingame.
 * 
 *   Hidden During Battle:
 *   - Insert the ID's of the Skill Types you want hidden during battle only.
 * 
 *   Icon: Normal Type:
 *   - Icon used for normal skill types that aren't assigned any icons.
 *   - To assign icons to skill types, simply insert \I[x] into the
 *     skill type's name in the Database > Types tab.
 * 
 *   Icon: Magic Type:
 *   - Icon used for magic skill types that aren't assigned any icons.
 *   - To assign icons to skill types, simply insert \I[x] into the
 *     skill type's name in the Database > Types tab.
 *
 * ---
 *
 * Global JS Effects
 * 
 *   JS: Skill Conditions:
 *   - JavaScript code for a global-wide skill condition check.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Skill Cost Types
 * ============================================================================
 *
 * Skill Cost Types are the resources that are used for your skills. These can
 * range from the default MP and TP resources to the newly added HP, Gold, and
 * Potion resources.
 *
 * ---
 *
 * Settings
 * 
 *   Name:
 *   - A name for this Skill Cost Type.
 * 
 *   Icon:
 *   - Icon used for this Skill Cost Type.
 *   - Use 0 for no icon.
 * 
 *   Font Color:
 *   - Text Color used to display this cost.
 *   - For a hex color, use #rrggbb with VisuMZ_1_MessageCore
 * 
 *   Font Size:
 *   - Font size used to display this cost.
 *
 * ---
 *
 * Cost Processing
 * 
 *   JS: Cost Calculation:
 *   - Code on how to calculate this resource cost for the skill.
 * 
 *   JS: Can Pay Cost?:
 *   - Code on calculating whether or not the user is able to pay the cost.
 * 
 *   JS: Paying Cost:
 *   - Code for if met, this is the actual process of paying of the cost.
 *
 * ---
 *
 * Window Display
 * 
 *   JS: Show Cost?:
 *   - Code for determining if the cost is shown or not.
 * 
 *   JS: Cost Text:
 *   - Code to determine the text (with Text Code support) used for the
 *     displayed cost.
 *
 * ---
 *
 * Gauge Display
 * 
 *   JS: Maximum Value:
 *   - Code to determine the maximum value used for this Skill Cost resource
 *     for gauges.
 * 
 *   JS: Current Value:
 *   - Code to determine the current value used for this Skill Cost resource
 *     for gauges.
 * 
 *   JS: Draw Gauge:
 *   - Code to determine how to draw the Skill Cost resource for this 
 *     gauge type.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: General State Settings
 * ============================================================================
 *
 * These are general settings regarding RPG Maker MZ's state-related aspects
 * from how turns are reapplied to custom code that's ran whenever states are
 * added, erased, or expired.
 *
 * ---
 *
 * General
 * 
 *   Reapply Rules:
 *   - These are the rules when reapplying states.
 *   - Ignore: State doesn't get added.
 *   - Reset: Turns get reset.
 *   - Greater: Turns take greater value (current vs reset).
 *   - Add: Turns add upon existing turns.
 * 
 *   Maximum Turns:
 *   - Maximum number of turns to let states go up to.
 *   - This can be changed with the <Max Turns: x> notetag.
 *
 * ---
 *
 * Turn Display
 * 
 *   Show Turns?:
 *   - Display state turns on top of window icons and sprites?
 * 
 *   Turn Font Size:
 *   - Font size used for displaying turns.
 * 
 *   Offset X:
 *   - Offset the X position of the turn display.
 * 
 *   Offset Y:
 *   - Offset the Y position of the turn display.
 * 
 *   Turn Font Size:
 *   - Font size used for displaying turns.
 * 
 *   Turn Color: Neutral:
 *   - Use #rrggbb for custom colors or regular numbers for text colors from
 *     the Window Skin.
 * 
 *   Turn Color: Positive:
 *   - Use #rrggbb for custom colors or regular numbers for text colors from
 *     the Window Skin.
 * 
 *   Turn Color: Negative:
 *   - Use #rrggbb for custom colors or regular numbers for text colors from
 *     the Window Skin.
 *
 * ---
 *
 * Data Display
 * 
 *   Show Data?:
 *   - Display state data on top of window icons and sprites?
 * 
 *   Data Font Size:
 *   - Font size used for displaying state data.
 * 
 *   Offset X:
 *   - Offset the X position of the state data display.
 * 
 *   Offset Y:
 *   - Offset the Y position of the state data display.
 *
 * ---
 *
 * Global JS Effects
 * 
 *   JS: On Add State:
 *   - JavaScript code for a global-wide custom effect whenever a state
 *     is added.
 * 
 *   JS: On Erase State:
 *   - JavaScript code for a global-wide custom effect whenever a state
 *     is erased.
 * 
 *   JS: On Expire State:
 *   - JavaScript code for a global-wide custom effect whenever a state
 *     has expired.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: General Buff/Debuff Settings
 * ============================================================================
 *
 * Buffs and debuffs don't count as states by RPG Maker MZ's mechanics, but
 * they do function close enough for them to be added to this plugin for
 * adjusting. Change these settings to make buffs and debuffs work to your
 * game's needs.
 *
 * ---
 *
 * General
 * 
 *   Reapply Rules:
 *   - These are the rules when reapplying buffs/debuffs.
 *   - Ignore: Buff/Debuff doesn't get added.
 *   - Reset: Turns get reset.
 *   - Greater: Turns take greater value (current vs reset).
 *   - Add: Turns add upon existing turns.
 * 
 *   Maximum Turns:
 *   - Maximum number of turns to let buffs and debuffs go up to.
 *
 * ---
 *
 * Stacking
 * 
 *   Max Stacks: Buff:
 *   - Maximum number of stacks for buffs.
 * 
 *   Max Stacks: Debuff:
 *   - Maximum number of stacks for debuffs.
 * 
 *   JS: Buff/Debuff Rate:
 *   - Code to determine how much buffs and debuffs affect parameters.
 *
 * ---
 *
 * Turn Display
 * 
 *   Show Turns?:
 *   - Display buff and debuff turns on top of window icons and sprites?
 * 
 *   Turn Font Size:
 *   - Font size used for displaying turns.
 * 
 *   Offset X:
 *   - Offset the X position of the turn display.
 * 
 *   Offset Y:
 *   - Offset the Y position of the turn display.
 * 
 *   Turn Color: Buffs:
 *   - Use #rrggbb for custom colors or regular numbers for text colors from
 *     the Window Skin.
 * 
 *   Turn Color: Debuffs:
 *   - Use #rrggbb for custom colors or regular numbers for text colors from
 *     the Window Skin.
 *
 * ---
 *
 * Rate Display
 * 
 *   Show Rate?:
 *   - Display buff and debuff rate on top of window icons and sprites?
 * 
 *   Rate Font Size:
 *   - Font size used for displaying rate.
 * 
 *   Offset X:
 *   - Offset the X position of the rate display.
 * 
 *   Offset Y:
 *   - Offset the Y position of the rate display.
 *
 * ---
 *
 * Global JS Effects
 * 
 *   JS: On Add Buff:
 *   - JavaScript code for a global-wide custom effect whenever a
 *     buff is added.
 * 
 *   JS: On Add Debuff:
 *   - JavaScript code for a global-wide custom effect whenever a
 *     debuff is added.
 * 
 *   JS: On Erase Buff:
 *   - JavaScript code for a global-wide custom effect whenever a
 *     buff is added.
 * 
 *   JS: On Erase Debuff:
 *   - JavaScript code for a global-wide custom effect whenever a
 *     debuff is added.
 * 
 *   JS: On Expire Buff:
 *   - JavaScript code for a global-wide custom effect whenever a
 *     buff is added.
 * 
 *   JS: On Expire Debuff:
 *   - JavaScript code for a global-wide custom effect whenever a
 *     debuff is added.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Passive State Settings
 * ============================================================================
 *
 * These Plugin Parameters adjust passive states that can affect all actors and
 * enemies as well as have global conditions.
 * 
 * ---
 * 
 * For those using the code "a.isStateAffected(10)" to check if a target is
 * affected by a state or not, this does NOT check passive states. This only
 * checks for states that were directly applied to the target.
 * 
 * Instead, use "a.states().includes($dataStates[10])" to check for them. This
 * code will search for both directly applied states and passive states alike.
 *
 * ---
 *
 * ---
 *
 * List
 * 
 *   Global Passives:
 *   - A list of passive states to affect actors and enemies.
 * 
 *   Actor-Only Passives:
 *   - A list of passive states to affect actors only.
 * 
 *   Enemy Passives:
 *   - A list of passive states to affect enemies only.
 *
 * ---
 *
 * Global JS Effects
 * 
 *   JS: Condition Check:
 *   - JavaScript code for a global-wide passive condition check.
 *
 * ---
 *
 * ============================================================================
 * Terms of Use
 * ============================================================================
 *
 * 1. These plugins may be used in free or commercial games provided that they
 * have been acquired through legitimate means at VisuStella.com and/or any
 * other official approved VisuStella sources. Exceptions and special
 * circumstances that may prohibit usage will be listed on VisuStella.com.
 * 
 * 2. All of the listed coders found in the Credits section of this plugin must
 * be given credit in your games or credited as a collective under the name:
 * "VisuStella".
 * 
 * 3. You may edit the source code to suit your needs, so long as you do not
 * claim the source code belongs to you. VisuStella also does not take
 * responsibility for the plugin if any changes have been made to the plugin's
 * code, nor does VisuStella take responsibility for user-provided custom code
 * used for custom control effects including advanced JavaScript notetags
 * and/or plugin parameters that allow custom JavaScript code.
 * 
 * 4. You may NOT redistribute these plugins nor take code from this plugin to
 * use as your own. These plugins and their code are only to be downloaded from
 * VisuStella.com and other official/approved VisuStella sources. A list of
 * official/approved sources can also be found on VisuStella.com.
 *
 * 5. VisuStella is not responsible for problems found in your game due to
 * unintended usage, incompatibility problems with plugins outside of the
 * VisuStella MZ library, plugin versions that aren't up to date, nor
 * responsible for the proper working of compatibility patches made by any
 * third parties. VisuStella is not responsible for errors caused by any
 * user-provided custom code used for custom control effects including advanced
 * JavaScript notetags and/or plugin parameters that allow JavaScript code.
 *
 * 6. If a compatibility patch needs to be made through a third party that is
 * unaffiliated with VisuStella that involves using code from the VisuStella MZ
 * library, contact must be made with a member from VisuStella and have it
 * approved. The patch would be placed on VisuStella.com as a free download
 * to the public. Such patches cannot be sold for monetary gain, including
 * commissions, crowdfunding, and/or donations.
 *
 * ============================================================================
 * Credits
 * ============================================================================
 * 
 * If you are using this plugin, credit the following people in your game:
 * 
 * Team VisuStella
 * - Yanfly
 * - Arisu
 * - Olivia
 * - Irina
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 * 
 * Version 1.12: February 19, 2021
 * * Feature Update
 * ** Changed the way passive state infinite stacking as a blanket coverage.
 *    Update made by Olivia.
 * 
 * Version 1.11: February 12, 2021
 * * Bug Fixes!
 * ** Added a check to prevent passive states from infinitely stacking. Fix
 *    made by Olivia.
 * 
 * Version 1.10: January 15, 2021
 * * Documentation Update!
 * ** Help file updated for new features.
 * * New Feature!
 * ** New Plugin Parameters added
 * *** Plugin Parameters > Skill Settings > Background Type
 * 
 * Version 1.09: January 1, 2021
 * * Bug Fixes!
 * ** Custom JS TP slip damage and healing should now work properly.
 *    Fix made by Yanfly.
 * 
 * Version 1.08: December 25, 2020
 * * Bug Fixes!
 * ** <JS On Add State> should no longer trigger multiple times for the death
 *    state. Fix made by Yanfly.
 * * Documentation Update!
 * ** Added documentation for updated feature(s)!
 * * Feature Update!
 * ** <No Death Clear> can now allow the affected state to be added to an
 *    already dead battler. Update made by Yanfly.
 * 
 * Version 1.07: December 18, 2020
 * * Documentation Update!
 * ** Added documentation for new feature(s)!
 * * New Features!
 * ** New notetags added by Yanfly:
 * *** <Passive Condition Multiclass: id>
 * *** <Passive Condition Multiclass: id, id, id>
 * *** <Passive Condition Multiclass: name>
 * *** <Passive Condition Multiclass: name, name, name>
 * ** New Plugin Parameter added by Yanfly.
 * *** Plugin Parameters > States > General > Action End Update
 * **** States with "Action End" auto-removal will also update turns at the end
 *      of each action instead of all actions.
 * ***** Turn this off if you wish for state turn updates to function like they
 *       do by default for "Action End".
 * 
 * Version 1.06: December 4, 2020
 * * Optimization Update!
 * ** Plugin should run more optimized.
 * 
 * Version 1.05: November 15, 2020
 * * Bug Fixes!
 * ** The alignment of the Skill Type Window is now fixed and will reflect upon
 *    the default settings. Fix made by Yanfly.
 * * Documentation Update!
 * ** Added documentation for new feature(s)!
 * * New Features!
 * ** <State x Category Remove: All> notetag added by Yanfly.
 * * Optimization Update!
 * ** Plugin should run more optimized.
 * 
 * Version 1.04: September 27, 2020
 * * Documentation Update
 * ** "Use Updated Layout" plugin parameters now have the added clause:
 *    "This will override the Core Engine windows settings." to reduce
 *    confusion. Added by Irina.
 * 
 * Version 1.03: September 13, 2020
 * * Bug Fixes!
 * ** <JS type Slip Damage> custom notetags now work for passive states. Fix
 *    made by Olivia.
 * ** Setting the Command Window style to "Text Only" will no longer add in
 *    the icon text codes. Bug fixed by Yanfly.
 * 
 * Version 1.02: August 30, 2020
 * * Bug Fixes!
 * ** The JS Notetags for Add, Erase, and Expire states are now fixed. Fix made
 *    by Yanfly.
 * * Documentation Update!
 * ** <Show if learned Skill: x> and <Hide if learned Skill: x> notetags have
 *    the following added to their descriptions:
 * *** This does not apply to skills added by traits on actors, classes, any
 *     equipment, or states. These are not considered learned skills. They are
 *     considered temporary skills.
 * * New Features!
 * ** Notetags added by Yanfly:
 * *** <Show if has Skill: x>
 * *** <Show if have All Skills: x,x,x>
 * *** <Show if have Any Skills: x,x,x>
 * *** <Show if has Skill: name>
 * *** <Show if have All Skills: name, name, name>
 * *** <Show if have Any Skills: name, name, name>
 * *** <Hide if has Skill: x>
 * *** <Hide if have All Skills: x,x,x>
 * *** <Hide if have Any Skills: x,x,x>
 * *** <Hide if has Skill: name>
 * *** <Hide if have All Skills: name, name, name>
 * *** <Hide if have Any Skills: name, name, name>
 * *** These have been added to remove the confusion regarding learned skills
 *     as skills added through trait effects are not considered learned skills
 *     by RPG Maker MZ.
 * 
 * Version 1.01: August 23, 2020
 * * Bug Fixes!
 * ** Passive states from Elements & Status Menu Core are now functional.
 *    Fix made by Olivia.
 * * Compatibility Update
 * ** Extended functions to allow for better compatibility.
 * * Updated documentation
 * ** Explains that passive states are not directly applied and are therefore
 *    not affected by code such as "a.isStateAffected(10)".
 * ** Instead, use "a.states().includes($dataStates[10])"
 * ** "Use #rrggbb for a hex color." lines now replaced with
 *    "For a hex color, use #rrggbb with VisuMZ_1_MessageCore"
 *
 * Version 1.00: August 20, 2020
 * * Finished Plugin!
 *
 * ============================================================================
 * End of Helpfile
 * ============================================================================
 *
 * @ ==========================================================================
 * @ Plugin Parameters
 * @ ==========================================================================
 *
 * @param BreakHead
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param SkillsStatesCore
 * @default Plugin Parameters
 *
 * @param ATTENTION
 * @default READ THE HELP FILE
 *
 * @param BreakSettings
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param Skills:struct
 * @text Skill Settings
 * @type struct<Skills>
 * @desc Adjust general skill settings here.
 * @default {"General":"","EnableLayout:eval":"true","LayoutStyle:str":"upper/left","SkillTypeWindow":"","CmdStyle:str":"auto","CmdTextAlign:str":"left","ListWindow":"","ListWindowCols:num":"1","ShopStatusWindow":"","ShowShopStatus:eval":"true","SkillSceneAdjustSkillList:eval":"true","SkillMenuStatusRect:func":"\"const ww = this.shopStatusWidth();\\nconst wh = this._itemWindow.height;\\nconst wx = Graphics.boxWidth - this.shopStatusWidth();\\nconst wy = this._itemWindow.y;\\nreturn new Rectangle(wx, wy, ww, wh);\"","SkillTypes":"","HiddenSkillTypes:arraynum":"[]","BattleHiddenSkillTypes:arraynum":"[]","IconStypeNorm:num":"78","IconStypeMagic:num":"79","CustomJS":"","SkillConditionJS:func":"\"// Declare Variables\\nconst skill = arguments[0];\\nconst user = this;\\nconst target = this;\\nconst a = this;\\nconst b = this;\\nlet enabled = true;\\n\\n// Perform Checks\\n\\n\\n// Return boolean\\nreturn enabled;\""}
 *
 * @param Costs:arraystruct
 * @text Skill Cost Types
 * @parent Skills:struct
 * @type struct<Cost>[]
 * @desc A list of all the skill cost types added by this plugin
 * and the code that controls them in-game.
 * @default ["{\"Name:str\":\"HP\",\"Settings\":\"\",\"Icon:num\":\"0\",\"FontColor:str\":\"20\",\"FontSize:num\":\"22\",\"Cost\":\"\",\"CalcJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nlet cost = 0;\\\\n\\\\n// Calculations\\\\nconst note = skill.note;\\\\nif (note.match(/<HP COST:[ ](\\\\\\\\d+)>/i)) {\\\\n    cost += Number(RegExp.$1);\\\\n}\\\\nif (note.match(/<HP COST:[ ](\\\\\\\\d+)([%％])>/i)) {\\\\n    cost += Math.ceil(Number(RegExp.$1) * user.mhp / 100);\\\\n}\\\\nif (note.match(/<JS HP COST>\\\\\\\\s*([\\\\\\\\s\\\\\\\\S]*)\\\\\\\\s*<\\\\\\\\/JS HP COST>/i)) {\\\\n    const code = String(RegExp.$1);\\\\n    eval(code);\\\\n}\\\\n\\\\n// Apply Trait Cost Alterations\\\\nif (cost > 0) {\\\\n    const rateNote = /<HP COST:[ ](\\\\\\\\d+\\\\\\\\.?\\\\\\\\d*)([%％])>/i;\\\\n    const rates = user.traitObjects().map((obj) => (obj && obj.note.match(rateNote) ? Number(RegExp.$1) / 100 : 1));\\\\n    const flatNote = /<HP COST:[ ]([\\\\\\\\+\\\\\\\\-]\\\\\\\\d+)>/i;\\\\n    const flats = user.traitObjects().map((obj) => (obj && obj.note.match(flatNote) ? Number(RegExp.$1) : 0));\\\\n    cost = rates.reduce((r, rate) => r * rate, cost);\\\\n    cost = flats.reduce((r, flat) => r + flat, cost);\\\\n    cost = Math.max(1, cost);\\\\n}\\\\n\\\\n// Set Cost Limits\\\\nif (note.match(/<HP COST MAX:[ ](\\\\\\\\d+)>/i)) {\\\\n    cost = Math.min(cost, Number(RegExp.$1));\\\\n}\\\\nif (note.match(/<HP COST MIN:[ ](\\\\\\\\d+)>/i)) {\\\\n    cost = Math.max(cost, Number(RegExp.$1));\\\\n}\\\\n\\\\n// Return cost value\\\\nreturn Math.round(Math.max(0, cost));\\\"\",\"CanPayJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nconst cost = arguments[1];\\\\n\\\\n// Return Boolean\\\\nif (cost <= 0) {\\\\n    return true;\\\\n} else {\\\\n    return user._hp > cost;\\\\n}\\\"\",\"PayJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nconst cost = arguments[1];\\\\n\\\\n// Process Payment\\\\nuser._hp -= cost;\\\"\",\"Windows\":\"\",\"ShowJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nconst cost = arguments[1];\\\\n\\\\n// Return Boolean\\\\nreturn cost > 0;\\\"\",\"TextJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nconst cost = arguments[1];\\\\nconst settings = arguments[2];\\\\nconst fontSize = settings.FontSize;\\\\nconst color = settings.FontColor;\\\\nconst name = TextManager.hp;\\\\nconst icon = settings.Icon;\\\\nlet text = '';\\\\n\\\\n// Text: Change Font Size\\\\ntext += '\\\\\\\\\\\\\\\\FS[%1]'.format(fontSize);\\\\n\\\\n// Text: Add Color\\\\nif (color.match(/#(.*)/i) && Imported.VisuMZ_1_MessageCore) {\\\\n    text += '\\\\\\\\\\\\\\\\HexColor<%1>'.format(String(RegExp.$1));\\\\n} else {\\\\n    text += '\\\\\\\\\\\\\\\\C[%1]'.format(color);\\\\n}\\\\n\\\\n// Text: Add Cost\\\\ntext += '%1 %2'.format(cost, name);\\\\n\\\\n// Text: Add Icon\\\\nif (icon  > 0) {\\\\n    text += '\\\\\\\\\\\\\\\\I[%1]'.format(icon);\\\\n}\\\\n\\\\n// Return text\\\\nreturn text;\\\"\",\"Gauges\":\"\",\"GaugeMaxJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\n\\\\n// Return value\\\\nreturn user.mhp;\\\"\",\"GaugeCurrentJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\n\\\\n// Return value\\\\nreturn user.hp;\\\"\",\"GaugeDrawJS:func\":\"\\\"// Declare Variables\\\\nconst sprite = this;\\\\nconst settings = sprite._costSettings;\\\\nconst bitmap = sprite.bitmap;\\\\nconst user = sprite._battler;\\\\nconst currentValue = sprite.currentDisplayedValue();\\\\n\\\\n// Draw Gauge\\\\nconst color1 = ColorManager.hpGaugeColor1();\\\\nconst color2 = ColorManager.hpGaugeColor2();\\\\nconst gx = 0;\\\\nconst gy = sprite.bitmapHeight() - sprite.gaugeHeight();\\\\nconst gw = sprite.bitmapWidth() - gx;\\\\nconst gh = sprite.gaugeHeight();\\\\nthis.drawFullGauge(color1, color2, gx, gy, gw, gh);\\\\n\\\\n// Draw Label\\\\nconst label = TextManager.hpA;\\\\nconst lx = 4;\\\\nconst ly = 0;\\\\nconst lw = sprite.bitmapWidth();\\\\nconst lh = sprite.bitmapHeight();\\\\nsprite.setupLabelFont();\\\\nbitmap.paintOpacity = 255;\\\\nbitmap.drawText(label, lx, ly, lw, lh, \\\\\\\"left\\\\\\\");\\\\n\\\\n// Draw Value\\\\nconst vw = sprite.bitmapWidth() - 2;\\\\nconst vh = sprite.bitmapHeight();\\\\nsprite.setupValueFont();\\\\nbitmap.textColor = ColorManager.hpColor(user);\\\\nbitmap.drawText(currentValue, 0, 0, vw, vh, \\\\\\\"right\\\\\\\");\\\"\"}","{\"Name:str\":\"MP\",\"Settings\":\"\",\"Icon:num\":\"0\",\"FontColor:str\":\"23\",\"FontSize:num\":\"22\",\"Cost\":\"\",\"CalcJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nlet cost = 0;\\\\n\\\\n// Calculations\\\\nconst note = skill.note;\\\\ncost = Math.floor(skill.mpCost * user.mcr);\\\\nif (note.match(/<MP COST:[ ](\\\\\\\\d+)([%％])>/i)) {\\\\n    cost += Math.ceil(Number(RegExp.$1) * user.mmp / 100);\\\\n}\\\\nif (note.match(/<JS MP COST>\\\\\\\\s*([\\\\\\\\s\\\\\\\\S]*)\\\\\\\\s*<\\\\\\\\/JS MP COST>/i)) {\\\\n    const code = String(RegExp.$1);\\\\n    eval(code);\\\\n}\\\\n\\\\n// Apply Trait Cost Alterations\\\\nif (cost > 0) {\\\\n    const rateNote = /<MP COST:[ ](\\\\\\\\d+\\\\\\\\.?\\\\\\\\d*)([%％])>/i;\\\\n    const rates = user.traitObjects().map((obj) => (obj && obj.note.match(rateNote) ? Number(RegExp.$1) / 100 : 1));\\\\n    const flatNote = /<MP COST:[ ]([\\\\\\\\+\\\\\\\\-]\\\\\\\\d+)>/i;\\\\n    const flats = user.traitObjects().map((obj) => (obj && obj.note.match(flatNote) ? Number(RegExp.$1) : 0));\\\\n    cost = rates.reduce((r, rate) => r * rate, cost);\\\\n    cost = flats.reduce((r, flat) => r + flat, cost);\\\\n    cost = Math.max(1, cost);\\\\n}\\\\n\\\\n// Set Cost Limits\\\\nif (note.match(/<MP COST MAX:[ ](\\\\\\\\d+)>/i)) {\\\\n    cost = Math.min(cost, Number(RegExp.$1));\\\\n}\\\\nif (note.match(/<MP COST MIN:[ ](\\\\\\\\d+)>/i)) {\\\\n    cost = Math.max(cost, Number(RegExp.$1));\\\\n}\\\\n\\\\n// Return cost value\\\\nreturn Math.round(Math.max(0, cost));\\\"\",\"CanPayJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nconst cost = arguments[1];\\\\n\\\\n// Return Boolean\\\\nreturn user._mp >= cost;\\\"\",\"PayJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nconst cost = arguments[1];\\\\n\\\\n// Process Payment\\\\nuser._mp -= cost;\\\"\",\"Windows\":\"\",\"ShowJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nconst cost = arguments[1];\\\\n\\\\n// Return Boolean\\\\nreturn cost > 0;\\\"\",\"TextJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nconst cost = arguments[1];\\\\nconst settings = arguments[2];\\\\nconst fontSize = settings.FontSize;\\\\nconst color = settings.FontColor;\\\\nconst name = TextManager.mp;\\\\nconst icon = settings.Icon;\\\\nlet text = '';\\\\n\\\\n// Text: Change Font Size\\\\ntext += '\\\\\\\\\\\\\\\\FS[%1]'.format(fontSize);\\\\n\\\\n// Text: Add Color\\\\nif (color.match(/#(.*)/i) && Imported.VisuMZ_1_MessageCore) {\\\\n    text += '\\\\\\\\\\\\\\\\HexColor<#%1>'.format(String(RegExp.$1));\\\\n} else {\\\\n    text += '\\\\\\\\\\\\\\\\C[%1]'.format(color);\\\\n}\\\\n\\\\n// Text: Add Cost\\\\ntext += '%1 %2'.format(cost, name);\\\\n\\\\n// Text: Add Icon\\\\nif (icon  > 0) {\\\\n    text += '\\\\\\\\\\\\\\\\I[%1]'.format(icon);\\\\n}\\\\n\\\\n// Return text\\\\nreturn text;\\\"\",\"Gauges\":\"\",\"GaugeMaxJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\n\\\\n// Return value\\\\nreturn user.mmp;\\\"\",\"GaugeCurrentJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\n\\\\n// Return value\\\\nreturn user.mp;\\\"\",\"GaugeDrawJS:func\":\"\\\"// Declare Variables\\\\nconst sprite = this;\\\\nconst settings = sprite._costSettings;\\\\nconst bitmap = sprite.bitmap;\\\\nconst user = sprite._battler;\\\\nconst currentValue = sprite.currentDisplayedValue();\\\\n\\\\n// Draw Gauge\\\\nconst color1 = ColorManager.mpGaugeColor1();\\\\nconst color2 = ColorManager.mpGaugeColor2();\\\\nconst gx = 0;\\\\nconst gy = sprite.bitmapHeight() - sprite.gaugeHeight();\\\\nconst gw = sprite.bitmapWidth() - gx;\\\\nconst gh = sprite.gaugeHeight();\\\\nthis.drawFullGauge(color1, color2, gx, gy, gw, gh);\\\\n\\\\n// Draw Label\\\\nconst label = TextManager.mpA;\\\\nconst lx = 4;\\\\nconst ly = 0;\\\\nconst lw = sprite.bitmapWidth();\\\\nconst lh = sprite.bitmapHeight();\\\\nsprite.setupLabelFont();\\\\nbitmap.paintOpacity = 255;\\\\nbitmap.drawText(label, lx, ly, lw, lh, \\\\\\\"left\\\\\\\");\\\\n\\\\n// Draw Value\\\\nconst vw = sprite.bitmapWidth() - 2;\\\\nconst vh = sprite.bitmapHeight();\\\\nsprite.setupValueFont();\\\\nbitmap.textColor = ColorManager.mpColor(user);\\\\nbitmap.drawText(currentValue, 0, 0, vw, vh, \\\\\\\"right\\\\\\\");\\\"\"}","{\"Name:str\":\"TP\",\"Settings\":\"\",\"Icon:num\":\"0\",\"FontColor:str\":\"29\",\"FontSize:num\":\"22\",\"Cost\":\"\",\"CalcJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nlet cost = 0;\\\\n\\\\n// Calculations\\\\nconst note = skill.note;\\\\ncost = skill.tpCost;\\\\nif (note.match(/<TP COST:[ ](\\\\\\\\d+)([%％])>/i)) {\\\\n    cost += Math.ceil(Number(RegExp.$1) * user.maxTp() / 100);\\\\n}\\\\nif (note.match(/<JS TP COST>\\\\\\\\s*([\\\\\\\\s\\\\\\\\S]*)\\\\\\\\s*<\\\\\\\\/JS TP COST>/i)) {\\\\n    const code = String(RegExp.$1);\\\\n    eval(code);\\\\n}\\\\n\\\\n// Apply Trait Cost Alterations\\\\nif (cost > 0) {\\\\n    const rateNote = /<TP COST:[ ](\\\\\\\\d+\\\\\\\\.?\\\\\\\\d*)([%％])>/i;\\\\n    const rates = user.traitObjects().map((obj) => (obj && obj.note.match(rateNote) ? Number(RegExp.$1) / 100 : 1));\\\\n    const flatNote = /<TP COST:[ ]([\\\\\\\\+\\\\\\\\-]\\\\\\\\d+)>/i;\\\\n    const flats = user.traitObjects().map((obj) => (obj && obj.note.match(flatNote) ? Number(RegExp.$1) : 0));\\\\n    cost = rates.reduce((r, rate) => r * rate, cost);\\\\n    cost = flats.reduce((r, flat) => r + flat, cost);\\\\n    cost = Math.max(1, cost);\\\\n}\\\\n\\\\n// Set Cost Limits\\\\nif (note.match(/<TP COST MAX:[ ](\\\\\\\\d+)>/i)) {\\\\n    cost = Math.min(cost, Number(RegExp.$1));\\\\n}\\\\nif (note.match(/<TP COST MIN:[ ](\\\\\\\\d+)>/i)) {\\\\n    cost = Math.max(cost, Number(RegExp.$1));\\\\n}\\\\n\\\\n// Return cost value\\\\nreturn Math.round(Math.max(0, cost));\\\"\",\"CanPayJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nconst cost = arguments[1];\\\\n\\\\n// Return Boolean\\\\nreturn user._tp >= cost;\\\"\",\"PayJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nconst cost = arguments[1];\\\\n\\\\n// Process Payment\\\\nuser._tp -= cost;\\\"\",\"Windows\":\"\",\"ShowJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nconst cost = arguments[1];\\\\n\\\\n// Return Boolean\\\\nreturn cost > 0;\\\"\",\"TextJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nconst cost = arguments[1];\\\\nconst settings = arguments[2];\\\\nconst fontSize = settings.FontSize;\\\\nconst color = settings.FontColor;\\\\nconst name = TextManager.tp;\\\\nconst icon = settings.Icon;\\\\nlet text = '';\\\\n\\\\n// Text: Change Font Size\\\\ntext += '\\\\\\\\\\\\\\\\FS[%1]'.format(fontSize);\\\\n\\\\n// Text: Add Color\\\\nif (color.match(/#(.*)/i) && Imported.VisuMZ_1_MessageCore) {\\\\n    text += '\\\\\\\\\\\\\\\\HexColor<#%1>'.format(String(RegExp.$1));\\\\n} else {\\\\n    text += '\\\\\\\\\\\\\\\\C[%1]'.format(color);\\\\n}\\\\n\\\\n// Text: Add Cost\\\\ntext += '%1 %2'.format(cost, name);\\\\n\\\\n// Text: Add Icon\\\\nif (icon  > 0) {\\\\n    text += '\\\\\\\\\\\\\\\\I[%1]'.format(icon);\\\\n}\\\\n\\\\n// Return text\\\\nreturn text;\\\"\",\"Gauges\":\"\",\"GaugeMaxJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\n\\\\n// Return value\\\\nreturn user.maxTp();\\\"\",\"GaugeCurrentJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\n\\\\n// Return value\\\\nreturn user.tp;\\\"\",\"GaugeDrawJS:func\":\"\\\"// Declare Variables\\\\nconst sprite = this;\\\\nconst settings = sprite._costSettings;\\\\nconst bitmap = sprite.bitmap;\\\\nconst user = sprite._battler;\\\\nconst currentValue = sprite.currentDisplayedValue();\\\\n\\\\n// Draw Gauge\\\\nconst color1 = ColorManager.tpGaugeColor1();\\\\nconst color2 = ColorManager.tpGaugeColor2();\\\\nconst gx = 0;\\\\nconst gy = sprite.bitmapHeight() - sprite.gaugeHeight();\\\\nconst gw = sprite.bitmapWidth() - gx;\\\\nconst gh = sprite.gaugeHeight();\\\\nthis.drawFullGauge(color1, color2, gx, gy, gw, gh);\\\\n\\\\n// Draw Label\\\\nconst label = TextManager.tpA;\\\\nconst lx = 4;\\\\nconst ly = 0;\\\\nconst lw = sprite.bitmapWidth();\\\\nconst lh = sprite.bitmapHeight();\\\\nsprite.setupLabelFont();\\\\nbitmap.paintOpacity = 255;\\\\nbitmap.drawText(label, lx, ly, lw, lh, \\\\\\\"left\\\\\\\");\\\\n\\\\n// Draw Value\\\\nconst vw = sprite.bitmapWidth() - 2;\\\\nconst vh = sprite.bitmapHeight();\\\\nsprite.setupValueFont();\\\\nbitmap.textColor = ColorManager.tpColor(user);\\\\nbitmap.drawText(currentValue, 0, 0, vw, vh, \\\\\\\"right\\\\\\\");\\\"\"}","{\"Name:str\":\"Gold\",\"Settings\":\"\",\"Icon:num\":\"0\",\"FontColor:str\":\"17\",\"FontSize:num\":\"22\",\"Cost\":\"\",\"CalcJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nlet cost = 0;\\\\n\\\\n// Calculations\\\\nconst note = skill.note;\\\\nif (note.match(/<GOLD COST:[ ](\\\\\\\\d+)>/i)) {\\\\n    cost += Number(RegExp.$1);\\\\n}\\\\nif (note.match(/<GOLD COST:[ ](\\\\\\\\d+)([%％])>/i)) {\\\\n    cost += Math.ceil(Number(RegExp.$1) * $gameParty.gold() / 100);\\\\n}\\\\nif (note.match(/<JS GOLD COST>\\\\\\\\s*([\\\\\\\\s\\\\\\\\S]*)\\\\\\\\s*<\\\\\\\\/JS GOLD COST>/i)) {\\\\n    const code = String(RegExp.$1);\\\\n    eval(code);\\\\n}\\\\n\\\\n// Apply Trait Cost Alterations\\\\nif (cost > 0) {\\\\n    const rateNote = /<GOLD COST:[ ](\\\\\\\\d+\\\\\\\\.?\\\\\\\\d*)([%％])>/i;\\\\n    const rates = user.traitObjects().map((obj) => (obj && obj.note.match(rateNote) ? Number(RegExp.$1) / 100 : 1));\\\\n    const flatNote = /<GOLD COST:[ ]([\\\\\\\\+\\\\\\\\-]\\\\\\\\d+)>/i;\\\\n    const flats = user.traitObjects().map((obj) => (obj && obj.note.match(flatNote) ? Number(RegExp.$1) : 0));\\\\n    cost = rates.reduce((r, rate) => r * rate, cost);\\\\n    cost = flats.reduce((r, flat) => r + flat, cost);\\\\n    cost = Math.max(1, cost);\\\\n}\\\\n\\\\n// Set Cost Limits\\\\nif (note.match(/<GOLD COST MAX:[ ](\\\\\\\\d+)>/i)) {\\\\n    cost = Math.min(cost, Number(RegExp.$1));\\\\n}\\\\nif (note.match(/<GOLD COST MIN:[ ](\\\\\\\\d+)>/i)) {\\\\n    cost = Math.max(cost, Number(RegExp.$1));\\\\n}\\\\n\\\\n// Return cost value\\\\nreturn Math.round(Math.max(0, cost));\\\"\",\"CanPayJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nconst cost = arguments[1];\\\\n\\\\n// Return Boolean\\\\nreturn $gameParty.gold() >= cost;\\\"\",\"PayJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nconst cost = arguments[1];\\\\n\\\\n// Process Payment\\\\n$gameParty.loseGold(cost);\\\"\",\"Windows\":\"\",\"ShowJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nconst cost = arguments[1];\\\\n\\\\n// Return Boolean\\\\nreturn cost > 0;\\\"\",\"TextJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nconst cost = arguments[1];\\\\nconst settings = arguments[2];\\\\nconst fontSize = settings.FontSize;\\\\nconst color = settings.FontColor;\\\\nconst name = TextManager.currencyUnit;\\\\nconst icon = settings.Icon;\\\\nlet text = '';\\\\n\\\\n// Text: Change Font Size\\\\ntext += '\\\\\\\\\\\\\\\\FS[%1]'.format(fontSize);\\\\n\\\\n// Text: Add Color\\\\nif (color.match(/#(.*)/i) && Imported.VisuMZ_1_MessageCore) {\\\\n    text += '\\\\\\\\\\\\\\\\HexColor<#%1>'.format(String(RegExp.$1));\\\\n} else {\\\\n    text += '\\\\\\\\\\\\\\\\C[%1]'.format(color);\\\\n}\\\\n\\\\n// Text: Add Cost\\\\ntext += '%1 %2'.format(cost, name);\\\\n\\\\n// Text: Add Icon\\\\nif (icon  > 0) {\\\\n    text += '\\\\\\\\\\\\\\\\I[%1]'.format(icon);\\\\n}\\\\n\\\\n// Return text\\\\nreturn text;\\\"\",\"Gauges\":\"\",\"GaugeMaxJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\n\\\\n// Return value\\\\nreturn $gameParty.maxGold();\\\"\",\"GaugeCurrentJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\n\\\\n// Return value\\\\nreturn $gameParty.gold();\\\"\",\"GaugeDrawJS:func\":\"\\\"// Declare Variables\\\\nconst sprite = this;\\\\nconst settings = sprite._costSettings;\\\\nconst bitmap = sprite.bitmap;\\\\nconst user = sprite._battler;\\\\nconst currentValue = sprite.currentDisplayedValue();\\\\n\\\\n// Draw Label\\\\nconst label = TextManager.currencyUnit;\\\\nconst lx = 4;\\\\nconst ly = 0;\\\\nconst lw = sprite.bitmapWidth();\\\\nconst lh = sprite.bitmapHeight();\\\\nsprite.setupLabelFont();\\\\nbitmap.paintOpacity = 255;\\\\nbitmap.drawText(label, lx, ly, lw, lh, \\\\\\\"left\\\\\\\");\\\\n\\\\n// Draw Value\\\\nconst vw = sprite.bitmapWidth() - 2;\\\\nconst vh = sprite.bitmapHeight();\\\\nsprite.setupValueFont();\\\\nbitmap.textColor = ColorManager.normalColor();\\\\nbitmap.drawText(currentValue, 0, 0, vw, vh, \\\\\\\"right\\\\\\\");\\\"\"}","{\"Name:str\":\"Potion\",\"Settings\":\"\",\"Icon:num\":\"176\",\"FontColor:str\":\"0\",\"FontSize:num\":\"22\",\"Cost\":\"\",\"CalcJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nlet cost = 0;\\\\n\\\\n// Calculations\\\\nconst note = skill.note;\\\\nif (note.match(/<POTION COST:[ ](\\\\\\\\d+)>/i)) {\\\\n    cost += Number(RegExp.$1);\\\\n}\\\\nif (note.match(/<JS POTION COST>\\\\\\\\s*([\\\\\\\\s\\\\\\\\S]*)\\\\\\\\s*<\\\\\\\\/JS POTION COST>/i)) {\\\\n    const code = String(RegExp.$1);\\\\n    eval(code);\\\\n}\\\\n\\\\n// Apply Trait Cost Alterations\\\\nif (cost > 0) {\\\\n    const rateNote = /<POTION COST:[ ](\\\\\\\\d+\\\\\\\\.?\\\\\\\\d*)([%％])>/i;\\\\n    const rates = user.traitObjects().map((obj) => (obj && obj.note.match(rateNote) ? Number(RegExp.$1) / 100 : 1));\\\\n    const flatNote = /<POTION COST:[ ]([\\\\\\\\+\\\\\\\\-]\\\\\\\\d+)>/i;\\\\n    const flats = user.traitObjects().map((obj) => (obj && obj.note.match(flatNote) ? Number(RegExp.$1) : 0));\\\\n    cost = rates.reduce((r, rate) => r * rate, cost);\\\\n    cost = flats.reduce((r, flat) => r + flat, cost);\\\\n    cost = Math.max(1, cost);\\\\n}\\\\n\\\\n// Set Cost Limits\\\\nif (note.match(/<POTION COST MAX:[ ](\\\\\\\\d+)>/i)) {\\\\n    cost = Math.min(cost, Number(RegExp.$1));\\\\n}\\\\nif (note.match(/<POTION COST MIN:[ ](\\\\\\\\d+)>/i)) {\\\\n    cost = Math.max(cost, Number(RegExp.$1));\\\\n}\\\\n\\\\n// Return cost value\\\\nreturn Math.round(Math.max(0, cost));\\\"\",\"CanPayJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nconst cost = arguments[1];\\\\nconst item = $dataItems[7];\\\\n\\\\n// Return Boolean\\\\nif (user.isActor() && cost > 0) {\\\\n    return $gameParty.numItems(item) >= cost;\\\\n} else {\\\\n    return true;\\\\n}\\\"\",\"PayJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nconst cost = arguments[1];\\\\nconst item = $dataItems[7];\\\\n\\\\n// Process Payment\\\\nif (user.isActor()) {\\\\n    $gameParty.loseItem(item, cost);\\\\n}\\\"\",\"Windows\":\"\",\"ShowJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst skill = arguments[0];\\\\nconst cost = arguments[1];\\\\n\\\\n// Return Boolean\\\\nreturn cost > 0;\\\"\",\"TextJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst item = $dataItems[7];\\\\nconst skill = arguments[0];\\\\nconst cost = arguments[1];\\\\nconst settings = arguments[2];\\\\nconst fontSize = settings.FontSize;\\\\nconst color = settings.FontColor;\\\\nconst name = settings.Name;\\\\nconst icon = settings.Icon;\\\\nlet text = '';\\\\n\\\\n// Text: Change Font Size\\\\ntext += '\\\\\\\\\\\\\\\\FS[%1]'.format(fontSize);\\\\n\\\\n// Text: Add Color\\\\nif (color.match(/#(.*)/i) && Imported.VisuMZ_1_MessageCore) {\\\\n    text += '\\\\\\\\\\\\\\\\HexColor<#%1>'.format(String(RegExp.$1));\\\\n} else {\\\\n    text += '\\\\\\\\\\\\\\\\C[%1]'.format(color);\\\\n}\\\\n\\\\n// Text: Add Cost\\\\ntext += '×%1'.format(cost);\\\\n\\\\n// Text: Add Icon\\\\ntext += '\\\\\\\\\\\\\\\\I[%1]'.format(item.iconIndex);\\\\n\\\\n// Return text\\\\nreturn text;\\\"\",\"Gauges\":\"\",\"GaugeMaxJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst item = $dataItems[7];\\\\n\\\\n// Return value\\\\nreturn $gameParty.maxItems(item);\\\"\",\"GaugeCurrentJS:func\":\"\\\"// Declare Variables\\\\nconst user = this;\\\\nconst item = $dataItems[7];\\\\n\\\\n// Return value\\\\nreturn $gameParty.numItems(item);\\\"\",\"GaugeDrawJS:func\":\"\\\"// Declare Variables\\\\nconst sprite = this;\\\\nconst settings = sprite._costSettings;\\\\nconst bitmap = sprite.bitmap;\\\\nconst user = sprite._battler;\\\\nconst item = $dataItems[7];\\\\nconst currentValue = sprite.currentDisplayedValue();\\\\n\\\\n// Draw Gauge\\\\nconst color1 = ColorManager.textColor(30);\\\\nconst color2 = ColorManager.textColor(31);\\\\nconst gx = 0;\\\\nconst gy = sprite.bitmapHeight() - sprite.gaugeHeight();\\\\nconst gw = sprite.bitmapWidth() - gx;\\\\nconst gh = sprite.gaugeHeight();\\\\nthis.drawFullGauge(color1, color2, gx, gy, gw, gh);\\\\n\\\\n// Draw Icon\\\\nconst iconIndex = item.iconIndex;\\\\nconst iconBitmap = ImageManager.loadSystem(\\\\\\\"IconSet\\\\\\\");\\\\nconst pw = ImageManager.iconWidth;\\\\nconst ph = ImageManager.iconHeight;\\\\nconst sx = (iconIndex % 16) * pw;\\\\nconst sy = Math.floor(iconIndex / 16) * ph;\\\\nbitmap.blt(iconBitmap, sx, sy, pw, ph, 0, 0, 24, 24);\\\\n\\\\n// Draw Value\\\\nconst vw = sprite.bitmapWidth() - 2;\\\\nconst vh = sprite.bitmapHeight();\\\\nsprite.setupValueFont();\\\\nbitmap.textColor = ColorManager.normalColor();\\\\nbitmap.drawText(currentValue, 0, 0, vw, vh, \\\\\\\"right\\\\\\\");\\\"\"}"]
 *
 * @param BreakSkills
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param States:struct
 * @text State Settings
 * @type struct<States>
 * @desc Adjust general state settings here.
 * @default {"General":"","ReapplyRules:str":"greater","MaxTurns:num":"99","ActionEndUpdate:eval":"true","Turns":"","ShowTurns:eval":"true","TurnFontSize:num":"16","TurnOffsetX:num":"-4","TurnOffsetY:num":"-6","ColorNeutral:str":"0","ColorPositive:str":"24","ColorNegative:str":"27","Data":"","ShowData:eval":"true","DataFontSize:num":"12","DataOffsetX:num":"0","DataOffsetY:num":"8","CustomJS":"","onAddStateJS:func":"\"// Declare Variables\\nconst stateId = arguments[0];\\nconst origin = this.getStateOrigin(stateId);\\nconst state = $dataStates[stateId];\\nconst user = this.getCurrentStateActiveUser();\\nconst target = this;\\nconst a = origin;\\nconst b = this;\\n\\n// Perform Actions\"","onEraseStateJS:func":"\"// Declare Variables\\nconst stateId = arguments[0];\\nconst origin = this.getStateOrigin(stateId);\\nconst state = $dataStates[stateId];\\nconst user = this.getCurrentStateActiveUser();\\nconst target = this;\\nconst a = origin;\\nconst b = this;\\n\\n// Perform Actions\\n\"","onExpireStateJS:func":"\"// Declare Variables\\nconst stateId = arguments[0];\\nconst origin = this.getStateOrigin(stateId);\\nconst state = $dataStates[stateId];\\nconst user = this.getCurrentStateActiveUser();\\nconst target = this;\\nconst a = origin;\\nconst b = this;\\n\\n// Perform Actions\\n\""}
 *
 * @param Buffs:struct
 * @text Buff/Debuff Settings
 * @parent States:struct
 * @type struct<Buffs>
 * @desc Adjust general buff/debuff settings here.
 * @default {"General":"","ReapplyRules:str":"greater","MaxTurns:num":"99","Stacking":"","StackBuffMax:num":"2","StackDebuffMax:num":"2","MultiplierJS:func":"\"// Declare Variables\\nconst user = this;\\nconst paramId = arguments[0];\\nconst buffLevel = arguments[1];\\nlet rate = 1;\\n\\n// Perform Calculations\\nrate += buffLevel * 0.25;\\n\\n// Return Rate\\nreturn Math.max(0, rate);\"","Turns":"","ShowTurns:eval":"true","TurnFontSize:num":"16","TurnOffsetX:num":"-4","TurnOffsetY:num":"-6","ColorBuff:str":"24","ColorDebuff:str":"27","Data":"","ShowData:eval":"false","DataFontSize:num":"12","DataOffsetX:num":"0","DataOffsetY:num":"8","CustomJS":"","onAddBuffJS:func":"\"// Declare Variables\\nconst paramId = arguments[0];\\nconst modifier = this._buffs[paramId];\\nconst origin = this.getCurrentStateActiveUser();\\nconst user = this.getCurrentStateActiveUser();\\nconst target = this;\\nconst a = origin;\\nconst b = this;\\n\\n// Perform Actions\\n\"","onAddDebuffJS:func":"\"// Declare Variables\\nconst paramId = arguments[0];\\nconst modifier = this._buffs[paramId];\\nconst origin = this.getCurrentStateActiveUser();\\nconst user = this.getCurrentStateActiveUser();\\nconst target = this;\\nconst a = origin;\\nconst b = this;\\n\\n// Perform Actions\\n\"","onEraseBuffJS:func":"\"// Declare Variables\\nconst paramId = arguments[0];\\nconst modifier = this._buffs[paramId];\\nconst origin = this.getCurrentStateActiveUser();\\nconst user = this.getCurrentStateActiveUser();\\nconst target = this;\\nconst a = origin;\\nconst b = this;\\n\\n// Perform Actions\\n\"","onEraseDebuffJS:func":"\"// Declare Variables\\nconst paramId = arguments[0];\\nconst modifier = this._buffs[paramId];\\nconst origin = this.getCurrentStateActiveUser();\\nconst user = this.getCurrentStateActiveUser();\\nconst target = this;\\nconst a = origin;\\nconst b = this;\\n\\n// Perform Actions\\n\"","onExpireBuffJS:func":"\"// Declare Variables\\nconst paramId = arguments[0];\\nconst modifier = this._buffs[paramId];\\nconst origin = this.getCurrentStateActiveUser();\\nconst user = this.getCurrentStateActiveUser();\\nconst target = this;\\nconst a = origin;\\nconst b = this;\\n\\n// Perform Actions\\n\"","onExpireDebuffJS:func":"\"// Declare Variables\\nconst paramId = arguments[0];\\nconst modifier = this._buffs[paramId];\\nconst origin = this.getCurrentStateActiveUser();\\nconst user = this.getCurrentStateActiveUser();\\nconst target = this;\\nconst a = origin;\\nconst b = this;\\n\\n// Perform Actions\\n\""}
 *
 * @param PassiveStates:struct
 * @text Passive States
 * @parent States:struct
 * @type struct<PassiveStates>
 * @desc Adjust passive state settings here.
 * @default {"List":"","Global:arraynum":"[]","Actor:arraynum":"[]","Enemy:arraynum":"[]","CustomJS":"","PassiveConditionJS:func":"\"// Declare Variables\\nconst state = arguments[0];\\nconst stateId = state.id;\\nconst user = this;\\nconst target = this;\\nconst a = this;\\nconst b = this;\\nlet condition = true;\\n\\n// Perform Checks\\n\\n\\n// Return boolean\\nreturn condition;\""}
 *
 * @param BreakEnd1
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param End Of
 * @default Plugin Parameters
 *
 * @param BreakEnd2
 * @text --------------------------
 * @default ----------------------------------
 *
 */
/* ----------------------------------------------------------------------------
 * General Skill Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Skills:
 *
 * @param General
 *
 * @param EnableLayout:eval
 * @text Use Updated Layout
 * @parent General
 * @type boolean
 * @on Use
 * @off Don't Use
 * @desc Use the Updated Skill Menu Layout provided by this plugin?
 * This will override the Core Engine windows settings.
 * @default true
 *
 * @param LayoutStyle:str
 * @text Layout Style
 * @parent General
 * @type select
 * @option Upper Help, Left Input
 * @value upper/left
 * @option Upper Help, Right Input
 * @value upper/right
 * @option Lower Help, Left Input
 * @value lower/left
 * @option Lower Help, Right Input
 * @value lower/right
 * @desc If using an updated layout, how do you want to style
 * the menu scene layout?
 * @default upper/left
 *
 * @param SkillTypeWindow
 * @text Skill Type Window
 *
 * @param CmdStyle:str
 * @text Style
 * @parent SkillTypeWindow
 * @type select
 * @option Text Only
 * @value text
 * @option Icon Only
 * @value icon
 * @option Icon + Text
 * @value iconText
 * @option Automatic
 * @value auto
 * @desc How do you wish to draw commands in the Skill Type Window?
 * @default auto
 *
 * @param CmdTextAlign:str
 * @text Text Align
 * @parent SkillTypeWindow
 * @type combo
 * @option left
 * @option center
 * @option right
 * @desc Text alignment for the Skill Type Window.
 * @default left
 *
 * @param ListWindow
 * @text List Window
 *
 * @param ListWindowCols:num
 * @text Columns
 * @parent ListWindow
 * @type number
 * @min 1
 * @desc Number of maximum columns.
 * @default 1
 *
 * @param ShopStatusWindow
 * @text Shop Status Window
 *
 * @param ShowShopStatus:eval
 * @text Show in Skill Menu?
 * @parent ShopStatusWindow
 * @type boolean
 * @on Show
 * @off Don't Show
 * @desc Show the Shop Status Window in the Skill Menu?
 * This is enabled if the Updated Layout is on.
 * @default true
 *
 * @param SkillSceneAdjustSkillList:eval
 * @text Adjust List Window?
 * @parent ShopStatusWindow
 * @type boolean
 * @on Adjust
 * @off Don't
 * @desc Automatically adjust the Skill List Window in the Skill Menu if using the Shop Status Window?
 * @default true
 *
 * @param SkillSceneStatusBgType:num
 * @text Background Type
 * @parent ShopStatusWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param SkillMenuStatusRect:func
 * @text JS: X, Y, W, H
 * @parent ShopStatusWindow
 * @type note
 * @desc Code used to determine the dimensions for this Shop Status Window in the Skill Menu.
 * @default "const ww = this.shopStatusWidth();\nconst wh = this._itemWindow.height;\nconst wx = Graphics.boxWidth - this.shopStatusWidth();\nconst wy = this._itemWindow.y;\nreturn new Rectangle(wx, wy, ww, wh);"
 *
 * @param SkillTypes
 * @text Skill Types
 *
 * @param HiddenSkillTypes:arraynum
 * @text Hidden Skill Types
 * @parent SkillTypes
 * @type number[]
 * @min 1
 * @max 99
 * @desc Insert the ID's of the Skill Types you want hidden from view ingame.
 * @default []
 *
 * @param BattleHiddenSkillTypes:arraynum
 * @text Hidden During Battle
 * @parent SkillTypes
 * @type number[]
 * @min 1
 * @max 99
 * @desc Insert the ID's of the Skill Types you want hidden during battle only.
 * @default []
 *
 * @param IconStypeNorm:num
 * @text Icon: Normal Type
 * @parent SkillTypes
 * @desc Icon used for normal skill types that aren't assigned any icons.
 * @default 78
 *
 * @param IconStypeMagic:num
 * @text Icon: Magic Type
 * @parent SkillTypes
 * @desc Icon used for magic skill types that aren't assigned any icons.
 * @default 79
 *
 * @param CustomJS
 * @text Global JS Effects
 *
 * @param SkillConditionJS:func
 * @text JS: Skill Conditions
 * @parent CustomJS
 * @type note
 * @desc JavaScript code for a global-wide skill condition check.
 * @default "// Declare Variables\nconst skill = arguments[0];\nconst user = this;\nconst target = this;\nconst a = this;\nconst b = this;\nlet enabled = true;\n\n// Perform Checks\n\n\n// Return boolean\nreturn enabled;"
 *
 */
/* ----------------------------------------------------------------------------
 * Skill Cost Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Cost:
 *
 * @param Name:str
 * @text Name
 * @desc A name for this Skill Cost Type.
 * @default Untitled
 *
 * @param Settings
 *
 * @param Icon:num
 * @text Icon
 * @parent Settings
 * @desc Icon used for this Skill Cost Type.
 * Use 0 for no icon.
 * @default 0
 *
 * @param FontColor:str
 * @text Font Color
 * @parent Settings
 * @desc Text Color used to display this cost.
 * For a hex color, use #rrggbb with VisuMZ_1_MessageCore
 * @default 0
 *
 * @param FontSize:num
 * @text Font Size
 * @parent Settings
 * @type number
 * @min 1
 * @desc Font size used to display this cost.
 * @default 22
 *
 * @param Cost
 * @text Cost Processing
 *
 * @param CalcJS:func
 * @text JS: Cost Calculation
 * @parent Cost
 * @type note
 * @desc Code on how to calculate this resource cost for the skill.
 * @default "// Declare Variables\nconst user = this;\nconst skill = arguments[0];\nlet cost = 0;\n\n// Return cost value\nreturn Math.round(Math.max(0, cost));"
 *
 * @param CanPayJS:func
 * @text JS: Can Pay Cost?
 * @parent Cost
 * @type note
 * @desc Code on calculating whether or not the user is able to pay the cost.
 * @default "// Declare Variables\nconst user = this;\nconst skill = arguments[0];\nconst cost = arguments[1];\n\n// Return Boolean\nreturn true;"
 *
 * @param PayJS:func
 * @text JS: Paying Cost
 * @parent Cost
 * @type note
 * @desc Code for if met, this is the actual process of paying of the cost.
 * @default "// Declare Variables\nconst user = this;\nconst skill = arguments[0];\nconst cost = arguments[1];\n\n// Process Payment\n"
 *
 * @param Windows
 * @text Window Display
 *
 * @param ShowJS:func
 * @text JS: Show Cost?
 * @parent  Windows
 * @type note
 * @desc Code for determining if the cost is shown or not.
 * @default "// Declare Variables\nconst user = this;\nconst skill = arguments[0];\nconst cost = arguments[1];\n\n// Return Boolean\nreturn cost > 0;"
 *
 * @param TextJS:func
 * @text JS: Cost Text
 * @parent  Windows
 * @type note
 * @desc Code to determine the text (with Text Code support) used for the displayed cost.
 * @default "// Declare Variables\nconst user = this;\nconst skill = arguments[0];\nconst cost = arguments[1];\nconst settings = arguments[2];\nconst fontSize = settings.FontSize;\nconst color = settings.FontColor;\nconst name = settings.Name;\nconst icon = settings.Icon;\nlet text = '';\n\n// Text: Change Font Size\ntext += '\\\\FS[%1]'.format(fontSize);\n\n// Text: Add Color\nif (color.match(/#(.*)/i) && Imported.VisuMZ_1_MessageCore) {\n    text += '\\\\HexColor<#%1>'.format(String(RegExp.$1));\n} else {\n    text += '\\\\C[%1]'.format(color);\n}\n\n// Text: Add Cost\ntext += '%1 %2'.format(cost, name);\n\n// Text: Add Icon\nif (icon  > 0) {\n    text += '\\\\I[%1]'.format(icon);\n}\n\n// Return text\nreturn text;"
 *
 * @param Gauges
 * @text Gauge Display
 *
 * @param GaugeMaxJS:func
 * @text JS: Maximum Value
 * @parent  Gauges
 * @type note
 * @desc Code to determine the maximum value used for this Skill Cost resource for gauges.
 * @default "// Declare Variables\nconst user = this;\n\n// Return value\nreturn 0;"
 *
 * @param GaugeCurrentJS:func
 * @text JS: Current Value
 * @parent  Gauges
 * @type note
 * @desc Code to determine the current value used for this Skill Cost resource for gauges.
 * @default "// Declare Variables\nconst user = this;\n\n// Return value\nreturn 0;"
 *
 * @param GaugeDrawJS:func
 * @text JS: Draw Gauge
 * @parent  Gauges
 * @type note
 * @desc Code to determine how to draw the Skill Cost resource for this gauge type.
 * @default "// Declare Variables\nconst sprite = this;\nconst settings = sprite._costSettings;\nconst bitmap = sprite.bitmap;\nconst user = sprite._battler;\nconst currentValue = sprite.currentDisplayedValue();\n\n// Draw Gauge\nconst color1 = ColorManager.textColor(30);\nconst color2 = ColorManager.textColor(31);\nconst gx = 0;\nconst gy = sprite.bitmapHeight() - sprite.gaugeHeight();\nconst gw = sprite.bitmapWidth() - gx;\nconst gh = sprite.gaugeHeight();\nthis.drawFullGauge(color1, color2, gx, gy, gw, gh);\n\n// Draw Label\nconst label = settings.Name;\nconst lx = 4;\nconst ly = 0;\nconst lw = sprite.bitmapWidth();\nconst lh = sprite.bitmapHeight();\nsprite.setupLabelFont();\nbitmap.paintOpacity = 255;\nbitmap.drawText(label, lx, ly, lw, lh, \"left\");\n\n// Draw Value\nconst vw = sprite.bitmapWidth() - 2;\nconst vh = sprite.bitmapHeight();\nsprite.setupValueFont();\nbitmap.textColor = ColorManager.normalColor();\nbitmap.drawText(currentValue, 0, 0, vw, vh, \"right\");"
 *
 */
/* ----------------------------------------------------------------------------
 * General State Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~States:
 *
 * @param General
 *
 * @param ReapplyRules:str
 * @text Reapply Rules
 * @parent General
 * @type select
 * @option Ignore: State doesn't get added.
 * @value ignore
 * @option Reset: Turns get reset.
 * @value reset
 * @option Greater: Turns take greater value (current vs reset).
 * @value greater
 * @option Add: Turns add upon existing turns.
 * @value add
 * @desc These are the rules when reapplying states.
 * @default greater
 *
 * @param MaxTurns:num
 * @text Maximum Turns
 * @parent General
 * @type number
 * @min 1
 * @desc Maximum number of turns to let states go up to.
 * This can be changed with the <Max Turns: x> notetag.
 * @default 9999
 *
 * @param ActionEndUpdate:eval
 * @text Action End Update
 * @parent General
 * @type boolean
 * @on Update Each Action
 * @off Don't Change
 * @desc States with "Action End" auto-removal will also update
 * turns at the end of each action instead of all actions.
 * @default true
 *
 * @param Turns
 * @text Turn Display
 *
 * @param ShowTurns:eval
 * @text Show Turns?
 * @parent Turns
 * @type boolean
 * @on Display
 * @off Hide
 * @desc Display state turns on top of window icons and sprites?
 * @default true
 *
 * @param TurnFontSize:num
 * @text Turn Font Size
 * @parent Turns
 * @type number
 * @min 1
 * @desc Font size used for displaying turns.
 * @default 16
 *
 * @param TurnOffsetX:num
 * @text Offset X
 * @parent Turns
 * @desc Offset the X position of the turn display.
 * @default -4
 *
 * @param TurnOffsetY:num
 * @text Offset Y
 * @parent Turns
 * @desc Offset the Y position of the turn display.
 * @default -6
 *
 * @param TurnFontSize:num
 * @text Turn Font Size
 * @parent Turns
 * @desc Font size used for displaying turns.
 * @default 16
 *
 * @param ColorNeutral:str
 * @text Turn Color: Neutral
 * @parent Turns
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 0
 *
 * @param ColorPositive:str
 * @text Turn Color: Positive
 * @parent Turns
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 24
 *
 * @param ColorNegative:str
 * @text Turn Color: Negative
 * @parent Turns
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 27
 *
 * @param Data
 * @text Data Display
 *
 * @param ShowData:eval
 * @text Show Data?
 * @parent Data
 * @type boolean
 * @on Display
 * @off Hide
 * @desc Display state data on top of window icons and sprites?
 * @default true
 *
 * @param DataFontSize:num
 * @text Data Font Size
 * @parent Data
 * @type number
 * @min 1
 * @desc Font size used for displaying state data.
 * @default 12
 *
 * @param DataOffsetX:num
 * @text Offset X
 * @parent Data
 * @desc Offset the X position of the state data display.
 * @default 0
 *
 * @param DataOffsetY:num
 * @text Offset Y
 * @parent Data
 * @desc Offset the Y position of the state data display.
 * @default 8
 *
 * @param CustomJS
 * @text Global JS Effects
 *
 * @param onAddStateJS:func
 * @text JS: On Add State
 * @parent CustomJS
 * @type note
 * @desc JavaScript code for a global-wide custom effect whenever a
 * state is added.
 * @default "// Declare Variables\nconst stateId = arguments[0];\nconst origin = this.getStateOrigin(stateId);\nconst state = $dataStates[stateId];\nconst user = this.getCurrentStateActiveUser();\nconst target = this;\nconst a = origin;\nconst b = this;\n\n// Perform Actions\n"
 *
 * @param onEraseStateJS:func
 * @text JS: On Erase State
 * @parent CustomJS
 * @type note
 * @desc JavaScript code for a global-wide custom effect whenever a
 * state is erased.
 * @default "// Declare Variables\nconst stateId = arguments[0];\nconst origin = this.getStateOrigin(stateId);\nconst state = $dataStates[stateId];\nconst user = this.getCurrentStateActiveUser();\nconst target = this;\nconst a = origin;\nconst b = this;\n\n// Perform Actions\n"
 *
 * @param onExpireStateJS:func
 * @text JS: On Expire State
 * @parent CustomJS
 * @type note
 * @desc JavaScript code for a global-wide custom effect whenever a
 * state has expired.
 * @default "// Declare Variables\nconst stateId = arguments[0];\nconst origin = this.getStateOrigin(stateId);\nconst state = $dataStates[stateId];\nconst user = this.getCurrentStateActiveUser();\nconst target = this;\nconst a = origin;\nconst b = this;\n\n// Perform Actions\n"
 *
 */
/* ----------------------------------------------------------------------------
 * General Buff/Debuff Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Buffs:
 *
 * @param General
 *
 * @param ReapplyRules:str
 * @text Reapply Rules
 * @parent General
 * @type select
 * @option Ignore: Buff/Debuff doesn't get added.
 * @value ignore
 * @option Reset: Turns get reset.
 * @value reset
 * @option Greater: Turns take greater value (current vs reset).
 * @value greater
 * @option Add: Turns add upon existing turns.
 * @value add
 * @desc These are the rules when reapplying buffs/debuffs.
 * @default greater
 *
 * @param MaxTurns:num
 * @text Maximum Turns
 * @parent General
 * @type number
 * @min 1
 * @desc Maximum number of turns to let buffs and debuffs go up to.
 * @default 9999
 *
 * @param Stacking
 *
 * @param StackBuffMax:num
 * @text Max Stacks: Buff
 * @parent Stacking
 * @type number
 * @min 1
 * @desc Maximum number of stacks for buffs.
 * @default 2
 *
 * @param StackDebuffMax:num
 * @text Max Stacks: Debuff
 * @parent Stacking
 * @type number
 * @min 1
 * @desc Maximum number of stacks for debuffs.
 * @default 2
 *
 * @param MultiplierJS:func
 * @text JS: Buff/Debuff Rate
 * @parent Stacking
 * @type note
 * @desc Code to determine how much buffs and debuffs affect parameters.
 * @default "// Declare Variables\nconst user = this;\nconst paramId = arguments[0];\nconst buffLevel = arguments[1];\nlet rate = 1;\n\n// Perform Calculations\nrate += buffLevel * 0.25;\n\n// Return Rate\nreturn Math.max(0, rate);"
 *
 * @param Turns
 * @text Turns Display
 *
 * @param ShowTurns:eval
 * @text Show Turns?
 * @parent Turns
 * @type boolean
 * @on Display
 * @off Hide
 * @desc Display buff and debuff turns on top of window icons and sprites?
 * @default true
 *
 * @param TurnFontSize:num
 * @text Turn Font Size
 * @parent Turns
 * @type number
 * @min 1
 * @desc Font size used for displaying turns.
 * @default 16
 *
 * @param TurnOffsetX:num
 * @text Offset X
 * @parent Turns
 * @desc Offset the X position of the turn display.
 * @default -4
 *
 * @param TurnOffsetY:num
 * @text Offset Y
 * @parent Turns
 * @desc Offset the Y position of the turn display.
 * @default -6
 *
 * @param ColorBuff:str
 * @text Turn Color: Buffs
 * @parent Turns
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 24
 *
 * @param ColorDebuff:str
 * @text Turn Color: Debuffs
 * @parent Turns
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 27
 *
 * @param Data
 * @text Rate Display
 *
 * @param ShowData:eval
 * @text Show Rate?
 * @parent Data
 * @type boolean
 * @on Display
 * @off Hide
 * @desc Display buff and debuff rate on top of window icons and sprites?
 * @default false
 *
 * @param DataFontSize:num
 * @text Rate Font Size
 * @parent Data
 * @type number
 * @min 1
 * @desc Font size used for displaying rate.
 * @default 12
 *
 * @param DataOffsetX:num
 * @text Offset X
 * @parent Data
 * @desc Offset the X position of the rate display.
 * @default 0
 *
 * @param DataOffsetY:num
 * @text Offset Y
 * @parent Data
 * @desc Offset the Y position of the rate display.
 * @default 8
 *
 * @param CustomJS
 * @text Global JS Effects
 *
 * @param onAddBuffJS:func
 * @text JS: On Add Buff
 * @parent CustomJS
 * @type note
 * @desc JavaScript code for a global-wide custom effect whenever a
 * buff is added.
 * @default "// Declare Variables\nconst paramId = arguments[0];\nconst modifier = this._buffs[paramId];\nconst origin = this.getCurrentStateActiveUser();\nconst user = this.getCurrentStateActiveUser();\nconst target = this;\nconst a = origin;\nconst b = this;\n\n// Perform Actions\n"
 *
 * @param onAddDebuffJS:func
 * @text JS: On Add Debuff
 * @parent CustomJS
 * @type note
 * @desc JavaScript code for a global-wide custom effect whenever a
 * debuff is added.
 * @default "// Declare Variables\nconst paramId = arguments[0];\nconst modifier = this._buffs[paramId];\nconst origin = this.getCurrentStateActiveUser();\nconst user = this.getCurrentStateActiveUser();\nconst target = this;\nconst a = origin;\nconst b = this;\n\n// Perform Actions\n"
 *
 * @param onEraseBuffJS:func
 * @text JS: On Erase Buff
 * @parent CustomJS
 * @type note
 * @desc JavaScript code for a global-wide custom effect whenever a
 * buff is erased.
 * @default "// Declare Variables\nconst paramId = arguments[0];\nconst modifier = this._buffs[paramId];\nconst origin = this.getCurrentStateActiveUser();\nconst user = this.getCurrentStateActiveUser();\nconst target = this;\nconst a = origin;\nconst b = this;\n\n// Perform Actions\n"
 *
 * @param onEraseDebuffJS:func
 * @text JS: On Erase Debuff
 * @parent CustomJS
 * @type note
 * @desc JavaScript code for a global-wide custom effect whenever a
 * debuff is erased.
 * @default "// Declare Variables\nconst paramId = arguments[0];\nconst modifier = this._buffs[paramId];\nconst origin = this.getCurrentStateActiveUser();\nconst user = this.getCurrentStateActiveUser();\nconst target = this;\nconst a = origin;\nconst b = this;\n\n// Perform Actions\n"
 *
 * @param onExpireBuffJS:func
 * @text JS: On Expire Buff
 * @parent CustomJS
 * @type note
 * @desc JavaScript code for a global-wide custom effect whenever a
 * buff has expired.
 * @default "// Declare Variables\nconst paramId = arguments[0];\nconst modifier = this._buffs[paramId];\nconst origin = this.getCurrentStateActiveUser();\nconst user = this.getCurrentStateActiveUser();\nconst target = this;\nconst a = origin;\nconst b = this;\n\n// Perform Actions\n"
 *
 * @param onExpireDebuffJS:func
 * @text JS: On Expire Debuff
 * @parent CustomJS
 * @type note
 * @desc JavaScript code for a global-wide custom effect whenever a
 * debuff has expired.
 * @default "// Declare Variables\nconst paramId = arguments[0];\nconst modifier = this._buffs[paramId];\nconst origin = this.getCurrentStateActiveUser();\nconst user = this.getCurrentStateActiveUser();\nconst target = this;\nconst a = origin;\nconst b = this;\n\n// Perform Actions\n"
 *
 */
/* ----------------------------------------------------------------------------
 * Passive State Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~PassiveStates:
 *
 * @param List
 *
 * @param Global:arraynum
 * @text Global Passives
 * @parent List
 * @type state[]
 * @desc A list of passive states to affect actors and enemies.
 * @default []
 *
 * @param Actor:arraynum
 * @text Actor-Only Passives
 * @parent List
 * @type state[]
 * @desc A list of passive states to affect actors only.
 * @default []
 *
 * @param Enemy:arraynum
 * @text Enemy Passives
 * @parent List
 * @type state[]
 * @desc A list of passive states to affect enemies only.
 * @default []
 *
 * @param CustomJS
 * @text Global JS Effects
 *
 * @param PassiveConditionJS:func
 * @text JS: Condition Check
 * @parent CustomJS
 * @type note
 * @desc JavaScript code for a global-wide passive condition check.
 * @default "// Declare Variables\nconst state = arguments[0];\nconst stateId = state.id;\nconst user = this;\nconst target = this;\nconst a = this;\nconst b = this;\nlet condition = true;\n\n// Perform Checks\n\n\n// Return boolean\nreturn condition;"
 *
 */
//=============================================================================

const _0x3f58=['stateData','isActor','shift','redraw','Actor','createItemWindow','Sprite_Gauge_gaugeRate','_stateRetainType','makeCurrentTroopUniqueID','learnSkill','indexOf','Window_SkillStatus_refresh','checkShowHideSwitchNotetags','changePaintOpacity','Game_BattlerBase_refresh','rgba(0,\x200,\x200,\x201)','NEGATIVE','onExpireBuffGlobalJS','getSkillIdWithName','checkSkillTypeMatch','equips','removeBuffsAuto','CalcJS','anchor','drawActorIconsAllTurnCounters','normalColor','GaugeCurrentJS','replace','priority','onEraseDebuff','createCommandNameWindow','_stateTurns','gaugeLineHeight','_skills','DataOffsetY','text','_scene','changeOutlineColor','isPassiveStateStackable','bitmap','Sprite_Gauge_currentValue','_skillTypeWindow','isAllDead','scrollTo','createTurnDisplaySprite','_animationIndex','user','keys','isStateAddable','description','SkillsStatesCore','stateMaximumTurns','addCommand','onAddDebuff','onAddDebuffJS','\x0a\x20\x20\x20\x20\x20\x20\x20\x20const\x20origin\x20=\x20this.getStateOrigin(stateId);\x0a\x20\x20\x20\x20\x20\x20\x20\x20const\x20state\x20=\x20$dataStates[stateId];\x0a\x20\x20\x20\x20\x20\x20\x20\x20const\x20user\x20=\x20this.getCurrentStateActiveUser();\x0a\x20\x20\x20\x20\x20\x20\x20\x20const\x20target\x20=\x20this;\x0a\x20\x20\x20\x20\x20\x20\x20\x20const\x20a\x20=\x20origin;\x0a\x20\x20\x20\x20\x20\x20\x20\x20const\x20b\x20=\x20this;\x0a\x20\x20\x20\x20\x20\x20\x20\x20try\x20{\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20%1\x0a\x20\x20\x20\x20\x20\x20\x20\x20}\x20catch\x20(e)\x20{\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20if\x20($gameTemp.isPlaytest())\x20console.log(e);\x0a\x20\x20\x20\x20\x20\x20\x20\x20}\x0a\x20\x20\x20\x20','slipMp','drawText','makeCommandList','states','decreaseBuff','commandStyle','drawItemStyleIconText','frameCount','initialize','parameters','recover\x20all','forgetSkill','skills','getCurrentStateOriginKey','lineHeight','magicSkills','DisplayedParams','Costs','Game_Actor_learnSkill','textColor','drawExtendedParameter','6TLIrTO','itemWindowRectSkillsStatesCore','STRUCT','isUseModernControls','_shopStatusWindow','Sprite_Gauge_redraw','addStateTurns','checkShowHideSkillNotetags','fillRect','gaugeBackColor','tpCost','isPlaytest','drawActorBuffRates','Sprite_StateIcon_updateFrame','%1\x20is\x20incorrectly\x20placed\x20on\x20the\x20plugin\x20list.\x0aIt\x20is\x20a\x20Tier\x20%2\x20plugin\x20placed\x20over\x20other\x20Tier\x20%3\x20plugins.\x0aPlease\x20reorder\x20the\x20plugin\x20list\x20from\x20smallest\x20to\x20largest\x20tier\x20numbers.','FUNC','stateMpSlipDamageJS','ALL','%1\x27s\x20version\x20does\x20not\x20match\x20plugin\x27s.\x20Please\x20update\x20it\x20in\x20the\x20Plugin\x20Manager.','map','MDF','Game_BattlerBase_decreaseBuff','isStateRemoved','log','_colorCache','passiveStateObjects','ignore','stateHpSlipDamageJS','eraseBuff','meetsPassiveStateConditionJS','colSpacing','_checkingPassiveStates','hasSkill','setDebuffTurns','commandStyleCheck','onEraseStateJS','currentClass','checkCacheKey','death','ARRAYSTRUCT','gainSilentTp','fontSize','Game_Battler_addBuff','debuffTurns','Sprite_Gauge_initMembers','applyBuffTurnManipulationEffects','checkShowHideNotetags','getStateRetainType','initMembersSkillsStatesCore','ARRAYEVAL','\x5cI[%1]%2','clear','stateTpSlipHealJS','Window_SkillList_setActor','addPassiveStatesByNotetag','_stateDisplay','stateColor','Game_Battler_addDebuff','enemy','name','ARRAYSTR','isStateResist','convertGaugeTypeSkillsStatesCore','addWindow','mainFontSize','skillTpCost','opacity','Buffs','ShowTurns','_turnDisplaySprite','totalStateCategoryAffected','Scene_Skill_helpWindowRect','83244LWXHsB','members','addChild','_costSettings','_stateData','gainHp','Game_Actor_forgetSkill','stateId','getStateOrigin','LUK','meetsSkillConditionsEnableJS','slipTp','skill','setup','multiclasses','Parse_Notetags_State_PassiveJS','Game_BattlerBase_buffIconIndex','autoRemovalTiming','addBuff','getStateIdWithName','Game_Battler_isStateAddable','callUpdateHelp','removeStatesByCategory','windowPadding','_categoryWindow','mainFontFace','\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20let\x20visible\x20=\x20true;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20user\x20=\x20this._actor;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20target\x20=\x20this._actor;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20a\x20=\x20this._actor;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20b\x20=\x20this._actor;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20try\x20{\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20%1\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20}\x20catch\x20(e)\x20{\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20if\x20($gameTemp.isPlaytest())\x20console.log(e);\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20}\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20return\x20visible;\x0a\x20\x20\x20\x20\x20\x20\x20\x20','onRemoveState','clearStateRetainType','helpWindowRectSkillsStatesCore','_itemWindow','right','MultiplierJS','isLearnedSkill','uiMenuStyle','skillTypes','currentDisplayedValue','Game_Unit_isAllDead','Game_BattlerBase_clearStates','categories','process_VisuMZ_SkillsStatesCore_Notetags','constructor','shopStatusWidth','isRightInputMode','onAddStateGlobalJS','ParseSkillNotetags','Game_BattlerBase_initMembers','onExpireState','isAlive','Game_BattlerBase_skillTpCost','NUM','skillVisibleJS','Game_BattlerBase_overwriteBuffTurns','1pDMiAP','uiInputPosition','onAddState','addPassiveStates','setItem','active','isBuffAffected','greater','resetTextColor','onEraseStateCustomJS','match','success','<actor-%1>','makeAdditionalSkillCostText','onExpireStateCustomJS','Game_Actor_skillTypes','584591lrytBi','itemWindowRect','isMaxDebuffAffected','updatedLayoutStyle','getSkillTypes','setupSkillsStatesCore','Window_SkillList_includes','includesSkillsStatesCore','remove','SkillConditionJS','setStateData','setStatusWindow','applySkillsStatesCoreEffects','ColorPositive','_subject','createShopStatusWindow','_stateIDs','checkSkillConditionsNotetags','concat','Game_BattlerBase_resetStateCounts','_stypeIDs','isSkillUsableForAutoBattle','refresh','applyStateCategoryRemovalEffects','push','onEraseDebuffGlobalJS','skillTypeWindowRectSkillsStatesCore','aliveMembers','getStateReapplyRulings','clamp','makeCommandName','getStypeIdWithName','commandNameWindowDrawBackground','helpAreaHeight','BattleManager_endAction','updateCommandNameWindow','makeSuccess','clearStatesWithStateRetain','paramBuffRate','onExpireStateGlobalJS','Enemy','buffTurns','Skills','mainCommandWidth','value','drawExtendedSkillsStatesCoreStatus','setBackgroundType','paySkillCost','CanPayJS','IconStypeMagic','enemyId','States','itemTextAlign','length','applyItemUserEffect','state','fontFace','heal','currentValueSkillsStatesCore','buff','addPassiveStatesTraitSets','skillCostSeparator','stateAddJS','innerWidth','isUseSkillsStatesCoreUpdatedLayout','updateStateTurns','uiHelpPosition','ARRAYJSON','includes','_commandNameWindow','statesByCategory','Scene_Skill_skillTypeWindowRect','hasState','isStateAffected','usableSkills','Sprite_Gauge_setup','142QBlbEL','commandName','onAddStateJS','initMembers','removeState','commandNameWindowDrawText','#%1','buffColor','ReapplyRules','_classIDs','calcWindowHeight','clearStateDisplay','clearStates','Parse_Notetags_State_ApplyRemoveLeaveJS','isGroupDefeatStateAffected','2003coPuoP','isStateCategoryAffected','_stateOrigin','reset','status','_buffs','Scene_Skill_statusWindowRect','Parse_Notetags_State_SlipEffectJS','Scene_Boot_onDatabaseLoaded','TurnOffsetY','stateTpSlipDamageJS','height','_stateMaxTurns','parse','VisuMZ_1_ItemsEquipsCore','Game_BattlerBase_die','sort','boxWidth','onEraseBuffJS','buffIconIndex','_actor','contents','Game_Battler_addState','currentMaxValueSkillsStatesCore','4023RBYYYV','24467alvNsw','_currentTroopUniqueID','%1%','drawActorStateData','Scene_Skill_createItemWindow','Settings','meetsPassiveStateGlobalConditionJS','ConvertParams','ATK','Game_BattlerBase_recoverAll','maxSlipDamage','meetsPassiveStateConditionSwitches','clearStateData','onExpireDebuffJS','setStypeId','%1\x20is\x20missing\x20a\x20required\x20plugin.\x0aPlease\x20install\x20%2\x20into\x20the\x20Plugin\x20Manager.','Parse_Notetags_Skill_Cost','groupDefeat','currentMaxValue','TurnFontSize','updateTurnDisplaySprite','isDebuffAffected','stateMpSlipHealJS','increaseBuff','actions','shopStatusWindowRect','ColorDebuff','onAddBuffGlobalJS','max','slipHp','buttonAssistText1','addDebuff','onAddStateCustomJS','_skillIDs','setStateDisplay','applyStateTurnManipulationEffects','resetStateCounts','filter','removeStatesAuto','Name','Game_BattlerBase_states','overwriteBuffTurns','iconIndex','damage','DEF','totalStateCategory','ShowShopStatus','retrieveStateColor','drawTextEx','7pdQqEC','setStateTurns','shopStatusWindowRectSkillsStatesCore','Sprite_StateIcon_loadBitmap','toUpperCase','DataOffsetX','checkShowHideJS','hasStateCategory','iconHeight','die','_checkingVisuMzPassiveStateObjects','meetsStateCondition','round','commandNameWindowCenter','ParseStateNotetags','skillId','4049bKfjfL','convertTargetToStateOriginKey','iconText','stateEraseJS','removeBuff','prototype','Window_StatusBase_drawActorIcons','getClassIdWithName','ANY','_cache','SkillSceneStatusBgType','checkShowHideBattleNotetags','isSkillCostShown','drawItem','_currentActor','_battler','format','auto','statePassiveConditionJS','ShowJS','Game_BattlerBase_meetsSkillConditions','getColor','helpWindowRect','setPassiveStateSlipDamageJS','addBuffTurns','inBattle','passiveStates','drawActorStateTurns','loadBitmap','onExpireDebuffGlobalJS','return\x200','skillMpCost','CmdStyle','floor','regenerateAllSkillsStatesCore','onAddBuff','_stypeId','resetFontSettings','MAT','textSizeEx','Parse_Notetags_State_Category','MAXMP','MaxTurns','drawActorBuffTurns','ShowData','split','center','getColorDataFromPluginParameters','_states','drawParamText','adjustItemWidthByShopStatus','none','number','recoverAll','onAddStateMakeCustomSlipValues','setStateRetainType','Game_Action_applyItemUserEffect','meetsSkillConditionsGlobalJS','trim','PassiveStates','onEraseDebuffJS','Window_SkillType_initialize','getStateData','110485CuzFuH','eraseState','redrawSkillsStatesCore','width','onAddBuffJS','ceil','<troop-%1>','Parse_Notetags_Skill_JS','toLowerCase','Game_BattlerBase_eraseBuff','getCurrentTroopUniqueID','note','Sprite_Gauge_currentMaxValue','actor','gaugeRate','process_VisuMZ_SkillsStatesCore_Skill_Notetags','convertPassiveStates','updateStatesActionEnd','BattleHiddenSkillTypes','regenerateAll','index','statusWindowRect','drawActorIcons','addState','\x0a\x20\x20\x20\x20\x20\x20\x20\x20let\x20%2\x20=\x200;\x0a\x20\x20\x20\x20\x20\x20\x20\x20const\x20origin\x20=\x20this.getStateOrigin(stateId);\x0a\x20\x20\x20\x20\x20\x20\x20\x20const\x20state\x20=\x20$dataStates[stateId];\x0a\x20\x20\x20\x20\x20\x20\x20\x20const\x20user\x20=\x20origin;\x0a\x20\x20\x20\x20\x20\x20\x20\x20const\x20target\x20=\x20this;\x0a\x20\x20\x20\x20\x20\x20\x20\x20const\x20a\x20=\x20origin;\x0a\x20\x20\x20\x20\x20\x20\x20\x20const\x20b\x20=\x20this;\x0a\x20\x20\x20\x20\x20\x20\x20\x20try\x20{\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20%1\x0a\x20\x20\x20\x20\x20\x20\x20\x20}\x20catch\x20(e)\x20{\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20if\x20($gameTemp.isPlaytest())\x20console.log(e);\x0a\x20\x20\x20\x20\x20\x20\x20\x20}\x0a\x20\x20\x20\x20\x20\x20\x20\x20%2\x20=\x20Math.round(Math.max(0,\x20%2)\x20*\x20%3);\x0a\x20\x20\x20\x20\x20\x20\x20\x20this.setStateData(stateId,\x20\x27%4\x27,\x20%2);\x0a\x20\x20\x20\x20','isBuffOrDebuffAffected','createAllSkillCostText','TurnOffsetX','addPassiveStatesFromOtherPlugins','ColorBuff','call','Game_BattlerBase_skillMpCost','Game_Troop_setup','VisuMZ_0_CoreEngine','Window_SkillList_updateHelp','mainAreaHeight','applyDebuffTurnManipulationEffects','onAddDebuffGlobalJS','isPartyAllAffectedByGroupDefeatStates','itemAt','DataFontSize','process_VisuMZ_SkillsStatesCore_State_Notetags','setStateOrigin','EnableLayout','setActor','SkillSceneAdjustSkillList','checkSkillConditionsSwitchNotetags','onRegenerateCustomStateDamageOverTime','onEraseBuff','isBuffExpired','onDatabaseLoaded','getStateDisplay','ARRAYFUNC','onExpireDebuff','innerHeight','updateFrame','iconWidth','buttonAssistSwitch','debuffColor','setBuffTurns','_result','allowCreateShopStatusWindow','maxCols','outlineColor','Game_BattlerBase_increaseBuff','stateExpireJS','meetsSkillConditions','_buffTurns','_stored_state-%1-color','skillEnableJS','changeTextColor','drawFullGauge','ActionEndUpdate','TextJS','mainAreaTop','Game_BattlerBase_eraseState','useDigitGrouping','PassiveConditionJS','updateHelp','mpCost','getCurrentStateActiveUser','meetsPassiveStateConditionClasses','currentValue','onEraseBuffGlobalJS','Game_Battler_regenerateAll','item','GaugeDrawJS','ParseClassIDs','drawSkillCost','stateHpSlipHealJS','removeStatesByCategoryAll','\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20let\x20condition\x20=\x20true;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20user\x20=\x20this;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20target\x20=\x20this;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20a\x20=\x20this;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20b\x20=\x20this;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20try\x20{\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20%1\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20}\x20catch\x20(e)\x20{\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20if\x20($gameTemp.isPlaytest())\x20console.log(e);\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20}\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20return\x20condition;\x0a\x20\x20\x20\x20\x20\x20\x20\x20','fontBold','Global','icon','MAXHP','221VAhCFj','LayoutStyle','itemLineRect','exit','_statusWindow','createSkillCostText','onEraseStateGlobalJS','stateTurns','addPassiveStatesByPluginParameters','onExpireBuff','drawItemStyleIcon','getStateOriginByKey','clearStateOrigin','VisuMZ_1_MainMenuCore'];const _0x230f=function(_0x111cf5,_0x3607ba){_0x111cf5=_0x111cf5-0x69;let _0x3f588e=_0x3f58[_0x111cf5];return _0x3f588e;};const _0x1bfe85=_0x230f;(function(_0x5504ae,_0xa03ff2){const _0x2dccd3=_0x230f;while(!![]){try{const _0x31c5ea=parseInt(_0x2dccd3(0xae))*parseInt(_0x2dccd3(0xd5))+-parseInt(_0x2dccd3(0x28e))*-parseInt(_0x2dccd3(0x117))+-parseInt(_0x2dccd3(0x29e))+parseInt(_0x2dccd3(0x107))*parseInt(_0x2dccd3(0x259))+parseInt(_0x2dccd3(0xd6))*parseInt(_0x2dccd3(0x211))+-parseInt(_0x2dccd3(0x1b6))*parseInt(_0x2dccd3(0xbd))+parseInt(_0x2dccd3(0x156));if(_0x31c5ea===_0xa03ff2)break;else _0x5504ae['push'](_0x5504ae['shift']());}catch(_0xca4934){_0x5504ae['push'](_0x5504ae['shift']());}}}(_0x3f58,0x5ebd8));var label=_0x1bfe85(0x1f6),tier=tier||0x0,dependencies=[],pluginData=$plugins['filter'](function(_0xa6f5e6){const _0x212a0f=_0x1bfe85;return _0xa6f5e6[_0x212a0f(0xc1)]&&_0xa6f5e6[_0x212a0f(0x1f5)][_0x212a0f(0xa6)]('['+label+']');})[0x0];VisuMZ[label][_0x1bfe85(0xdb)]=VisuMZ[label][_0x1bfe85(0xdb)]||{},VisuMZ[_0x1bfe85(0xdd)]=function(_0x1d0fc7,_0x204deb){const _0x4a9c95=_0x1bfe85;for(const _0x1fa256 in _0x204deb){if(_0x1fa256[_0x4a9c95(0x298)](/(.*):(.*)/i)){const _0x1ad4de=String(RegExp['$1']),_0x4a7de8=String(RegExp['$2'])['toUpperCase']()[_0x4a9c95(0x151)]();let _0x4eb5b7,_0x2b41e0,_0x8bc941;switch(_0x4a7de8){case _0x4a9c95(0x28b):_0x4eb5b7=_0x204deb[_0x1fa256]!==''?Number(_0x204deb[_0x1fa256]):0x0;break;case'ARRAYNUM':_0x2b41e0=_0x204deb[_0x1fa256]!==''?JSON[_0x4a9c95(0xca)](_0x204deb[_0x1fa256]):[],_0x4eb5b7=_0x2b41e0['map'](_0x47f08b=>Number(_0x47f08b));break;case'EVAL':_0x4eb5b7=_0x204deb[_0x1fa256]!==''?eval(_0x204deb[_0x1fa256]):null;break;case _0x4a9c95(0x242):_0x2b41e0=_0x204deb[_0x1fa256]!==''?JSON[_0x4a9c95(0xca)](_0x204deb[_0x1fa256]):[],_0x4eb5b7=_0x2b41e0[_0x4a9c95(0x224)](_0x3f206e=>eval(_0x3f206e));break;case'JSON':_0x4eb5b7=_0x204deb[_0x1fa256]!==''?JSON[_0x4a9c95(0xca)](_0x204deb[_0x1fa256]):'';break;case _0x4a9c95(0xa5):_0x2b41e0=_0x204deb[_0x1fa256]!==''?JSON[_0x4a9c95(0xca)](_0x204deb[_0x1fa256]):[],_0x4eb5b7=_0x2b41e0[_0x4a9c95(0x224)](_0x5ad7f5=>JSON[_0x4a9c95(0xca)](_0x5ad7f5));break;case _0x4a9c95(0x220):_0x4eb5b7=_0x204deb[_0x1fa256]!==''?new Function(JSON[_0x4a9c95(0xca)](_0x204deb[_0x1fa256])):new Function(_0x4a9c95(0x135));break;case _0x4a9c95(0x18a):_0x2b41e0=_0x204deb[_0x1fa256]!==''?JSON['parse'](_0x204deb[_0x1fa256]):[],_0x4eb5b7=_0x2b41e0[_0x4a9c95(0x224)](_0x4a860d=>new Function(JSON[_0x4a9c95(0xca)](_0x4a860d)));break;case'STR':_0x4eb5b7=_0x204deb[_0x1fa256]!==''?String(_0x204deb[_0x1fa256]):'';break;case _0x4a9c95(0x24d):_0x2b41e0=_0x204deb[_0x1fa256]!==''?JSON[_0x4a9c95(0xca)](_0x204deb[_0x1fa256]):[],_0x4eb5b7=_0x2b41e0[_0x4a9c95(0x224)](_0x35a47f=>String(_0x35a47f));break;case _0x4a9c95(0x213):_0x8bc941=_0x204deb[_0x1fa256]!==''?JSON[_0x4a9c95(0xca)](_0x204deb[_0x1fa256]):{},_0x1d0fc7[_0x1ad4de]={},VisuMZ[_0x4a9c95(0xdd)](_0x1d0fc7[_0x1ad4de],_0x8bc941);continue;case _0x4a9c95(0x238):_0x2b41e0=_0x204deb[_0x1fa256]!==''?JSON[_0x4a9c95(0xca)](_0x204deb[_0x1fa256]):[],_0x4eb5b7=_0x2b41e0[_0x4a9c95(0x224)](_0x3d32fd=>VisuMZ[_0x4a9c95(0xdd)]({},JSON[_0x4a9c95(0xca)](_0x3d32fd)));break;default:continue;}_0x1d0fc7[_0x1ad4de]=_0x4eb5b7;}}return _0x1d0fc7;},(_0x3858c6=>{const _0x337910=_0x1bfe85,_0x2929e6=_0x3858c6['name'];for(const _0x1b0530 of dependencies){if(!Imported[_0x1b0530]){alert(_0x337910(0xe5)[_0x337910(0x127)](_0x2929e6,_0x1b0530)),SceneManager['exit']();break;}}const _0x5a63e2=_0x3858c6[_0x337910(0x1f5)];if(_0x5a63e2['match'](/\[Version[ ](.*?)\]/i)){const _0x43f08b=Number(RegExp['$1']);_0x43f08b!==VisuMZ[label]['version']&&(alert(_0x337910(0x223)[_0x337910(0x127)](_0x2929e6,_0x43f08b)),SceneManager[_0x337910(0x1b9)]());}if(_0x5a63e2[_0x337910(0x298)](/\[Tier[ ](\d+)\]/i)){const _0x2624c8=Number(RegExp['$1']);_0x2624c8<tier?(alert(_0x337910(0x21f)[_0x337910(0x127)](_0x2929e6,_0x2624c8,tier)),SceneManager[_0x337910(0x1b9)]()):tier=Math['max'](_0x2624c8,tier);}VisuMZ[_0x337910(0xdd)](VisuMZ[label][_0x337910(0xdb)],_0x3858c6[_0x337910(0x205)]);})(pluginData),VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0xc5)]=Scene_Boot[_0x1bfe85(0x11c)][_0x1bfe85(0x188)],Scene_Boot['prototype'][_0x1bfe85(0x188)]=function(){const _0x168e58=_0x1bfe85;VisuMZ['SkillsStatesCore']['Scene_Boot_onDatabaseLoaded'][_0x168e58(0x174)](this),this[_0x168e58(0x281)]();},Scene_Boot[_0x1bfe85(0x11c)][_0x1bfe85(0x281)]=function(){const _0x2aa798=_0x1bfe85;if(VisuMZ['ParseAllNotetags'])return;this[_0x2aa798(0x165)](),this['process_VisuMZ_SkillsStatesCore_State_Notetags']();},Scene_Boot['prototype'][_0x1bfe85(0x165)]=function(){const _0xdc708d=_0x1bfe85;for(const _0x3e81c2 of $dataSkills){if(!_0x3e81c2)continue;VisuMZ['SkillsStatesCore'][_0xdc708d(0xe6)](_0x3e81c2),VisuMZ[_0xdc708d(0x1f6)][_0xdc708d(0x15d)](_0x3e81c2);}},Scene_Boot[_0x1bfe85(0x11c)][_0x1bfe85(0x17f)]=function(){const _0x154637=_0x1bfe85;for(const _0x537e99 of $dataStates){if(!_0x537e99)continue;VisuMZ[_0x154637(0x1f6)][_0x154637(0x13f)](_0x537e99),VisuMZ['SkillsStatesCore']['Parse_Notetags_State_PassiveJS'](_0x537e99),VisuMZ[_0x154637(0x1f6)][_0x154637(0xc4)](_0x537e99),VisuMZ[_0x154637(0x1f6)][_0x154637(0xbb)](_0x537e99);}},VisuMZ['SkillsStatesCore'][_0x1bfe85(0x286)]=VisuMZ[_0x1bfe85(0x286)],VisuMZ[_0x1bfe85(0x286)]=function(_0x2ba7d0){const _0x237238=_0x1bfe85;VisuMZ[_0x237238(0x1f6)][_0x237238(0x286)][_0x237238(0x174)](this,_0x2ba7d0),VisuMZ[_0x237238(0x1f6)]['Parse_Notetags_Skill_Cost'](_0x2ba7d0),VisuMZ['SkillsStatesCore'][_0x237238(0x15d)](_0x2ba7d0);},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x115)]=VisuMZ[_0x1bfe85(0x115)],VisuMZ[_0x1bfe85(0x115)]=function(_0x3bf216){const _0x2985cb=_0x1bfe85;VisuMZ[_0x2985cb(0x1f6)][_0x2985cb(0x115)]['call'](this,_0x3bf216),VisuMZ[_0x2985cb(0x1f6)]['Parse_Notetags_State_Category'](_0x3bf216),VisuMZ[_0x2985cb(0x1f6)][_0x2985cb(0x268)](_0x3bf216),VisuMZ[_0x2985cb(0x1f6)][_0x2985cb(0xc4)](_0x3bf216),VisuMZ[_0x2985cb(0x1f6)]['Parse_Notetags_State_ApplyRemoveLeaveJS'](_0x3bf216);},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0xe6)]=function(_0x6c68b6){const _0x4aa0af=_0x1bfe85,_0x24c065=_0x6c68b6[_0x4aa0af(0x161)];_0x24c065[_0x4aa0af(0x298)](/<MP COST:[ ](\d+)>/i)&&(_0x6c68b6[_0x4aa0af(0x1a5)]=Number(RegExp['$1'])),_0x24c065[_0x4aa0af(0x298)](/<TP COST:[ ](\d+)>/i)&&(_0x6c68b6[_0x4aa0af(0x21b)]=Number(RegExp['$1']));},VisuMZ[_0x1bfe85(0x1f6)]['skillEnableJS']={},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x28c)]={},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x15d)]=function(_0x347595){const _0x5e5a80=_0x1bfe85,_0xb88ebb=_0x347595[_0x5e5a80(0x161)];if(_0xb88ebb[_0x5e5a80(0x298)](/<JS SKILL ENABLE>\s*([\s\S]*)\s*<\/JS SKILL ENABLE>/i)){const _0x2871c0=String(RegExp['$1']),_0x1d2f22='\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20let\x20enabled\x20=\x20true;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20user\x20=\x20this;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20target\x20=\x20this;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20a\x20=\x20this;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20const\x20b\x20=\x20this;\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20try\x20{\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20%1\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20}\x20catch\x20(e)\x20{\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20if\x20($gameTemp.isPlaytest())\x20console.log(e);\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20}\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20return\x20enabled;\x0a\x20\x20\x20\x20\x20\x20\x20\x20'[_0x5e5a80(0x127)](_0x2871c0);VisuMZ[_0x5e5a80(0x1f6)][_0x5e5a80(0x19b)][_0x347595['id']]=new Function(_0x5e5a80(0x265),_0x1d2f22);}if(_0xb88ebb['match'](/<JS SKILL VISIBLE>\s*([\s\S]*)\s*<\/JS SKILL VISIBLE>/i)){const _0xe56171=String(RegExp['$1']),_0x179422=_0x5e5a80(0x273)[_0x5e5a80(0x127)](_0xe56171);VisuMZ[_0x5e5a80(0x1f6)][_0x5e5a80(0x28c)][_0x347595['id']]=new Function('skill',_0x179422);}},VisuMZ[_0x1bfe85(0x1f6)]['Parse_Notetags_State_Category']=function(_0x18a195){const _0x4b4a2a=_0x1bfe85;_0x18a195[_0x4b4a2a(0x280)]=[_0x4b4a2a(0x222),_0x4b4a2a(0x11f)];const _0x30597f=_0x18a195['note'],_0x40f097=_0x30597f[_0x4b4a2a(0x298)](/<(?:CATEGORY|CATEGORIES):[ ](.*)>/gi);if(_0x40f097)for(const _0x309521 of _0x40f097){_0x309521[_0x4b4a2a(0x298)](/<(?:CATEGORY|CATEGORIES):[ ](.*)>/gi);const _0x271418=String(RegExp['$1'])[_0x4b4a2a(0x10b)]()[_0x4b4a2a(0x151)]()[_0x4b4a2a(0x144)](',');for(const _0x4d9368 of _0x271418){_0x18a195[_0x4b4a2a(0x280)][_0x4b4a2a(0x7a)](_0x4d9368[_0x4b4a2a(0x151)]());}}if(_0x30597f[_0x4b4a2a(0x298)](/<(?:CATEGORY|CATEGORIES)>\s*([\s\S]*)\s*<\/(?:CATEGORY|CATEGORIES)>/i)){const _0x1f14e4=RegExp['$1'][_0x4b4a2a(0x144)](/[\r\n]+/);for(const _0xeca1ca of _0x1f14e4){_0x18a195[_0x4b4a2a(0x280)][_0x4b4a2a(0x7a)](_0xeca1ca[_0x4b4a2a(0x10b)]()[_0x4b4a2a(0x151)]());}}_0x30597f[_0x4b4a2a(0x298)](/<POSITIVE STATE>/i)&&_0x18a195[_0x4b4a2a(0x280)][_0x4b4a2a(0x7a)]('POSITIVE'),_0x30597f[_0x4b4a2a(0x298)](/<NEGATIVE STATE>/i)&&_0x18a195['categories'][_0x4b4a2a(0x7a)](_0x4b4a2a(0x1d4));},VisuMZ['SkillsStatesCore'][_0x1bfe85(0x129)]={},VisuMZ['SkillsStatesCore'][_0x1bfe85(0x268)]=function(_0x35db67){const _0x3b1184=_0x1bfe85,_0x16b24b=_0x35db67[_0x3b1184(0x161)];if(_0x16b24b[_0x3b1184(0x298)](/<JS PASSIVE CONDITION>\s*([\s\S]*)\s*<\/JS PASSIVE CONDITION>/i)){const _0xb8f104=String(RegExp['$1']),_0x476a52=_0x3b1184(0x1b1)[_0x3b1184(0x127)](_0xb8f104);VisuMZ[_0x3b1184(0x1f6)][_0x3b1184(0x129)][_0x35db67['id']]=new Function(_0x3b1184(0x99),_0x476a52);}},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x22c)]={},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x1af)]={},VisuMZ['SkillsStatesCore'][_0x1bfe85(0x221)]={},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0xec)]={},VisuMZ['SkillsStatesCore'][_0x1bfe85(0xc7)]={},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x245)]={},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0xc4)]=function(_0xb5a47){const _0x4ced6a=_0x1bfe85,_0x46ce18=_0xb5a47[_0x4ced6a(0x161)],_0x3c02fc=_0x4ced6a(0x16e);if(_0x46ce18[_0x4ced6a(0x298)](/<JS HP SLIP DAMAGE>\s*([\s\S]*)\s*<\/JS HP SLIP DAMAGE>/i)){const _0x1ba9eb=String(RegExp['$1']),_0x37666e=_0x3c02fc['format'](_0x1ba9eb,_0x4ced6a(0x101),-0x1,_0x4ced6a(0xf3));VisuMZ['SkillsStatesCore'][_0x4ced6a(0x22c)][_0xb5a47['id']]=new Function(_0x4ced6a(0x260),_0x37666e);}else{if(_0x46ce18[_0x4ced6a(0x298)](/<JS HP SLIP HEAL>\s*([\s\S]*)\s*<\/JS HP SLIP HEAL>/i)){const _0x5f3b88=String(RegExp['$1']),_0x6907c=_0x3c02fc[_0x4ced6a(0x127)](_0x5f3b88,_0x4ced6a(0x9b),0x1,_0x4ced6a(0xf3));VisuMZ[_0x4ced6a(0x1f6)]['stateHpSlipHealJS'][_0xb5a47['id']]=new Function(_0x4ced6a(0x260),_0x6907c);}}if(_0x46ce18[_0x4ced6a(0x298)](/<JS MP SLIP DAMAGE>\s*([\s\S]*)\s*<\/JS MP SLIP DAMAGE>/i)){const _0x2cf0df=String(RegExp['$1']),_0x328f3e=_0x3c02fc[_0x4ced6a(0x127)](_0x2cf0df,_0x4ced6a(0x101),-0x1,_0x4ced6a(0x1fc));VisuMZ['SkillsStatesCore']['stateMpSlipDamageJS'][_0xb5a47['id']]=new Function(_0x4ced6a(0x260),_0x328f3e);}else{if(_0x46ce18[_0x4ced6a(0x298)](/<JS MP SLIP HEAL>\s*([\s\S]*)\s*<\/JS MP SLIP HEAL>/i)){const _0x9b4869=String(RegExp['$1']),_0xd18557=_0x3c02fc[_0x4ced6a(0x127)](_0x9b4869,_0x4ced6a(0x9b),0x1,_0x4ced6a(0x1fc));VisuMZ[_0x4ced6a(0x1f6)][_0x4ced6a(0xec)][_0xb5a47['id']]=new Function(_0x4ced6a(0x260),_0xd18557);}}if(_0x46ce18[_0x4ced6a(0x298)](/<JS TP SLIP DAMAGE>\s*([\s\S]*)\s*<\/JS TP SLIP DAMAGE>/i)){const _0x388861=String(RegExp['$1']),_0x44eac6=_0x3c02fc[_0x4ced6a(0x127)](_0x388861,_0x4ced6a(0x101),-0x1,'slipTp');VisuMZ[_0x4ced6a(0x1f6)][_0x4ced6a(0xc7)][_0xb5a47['id']]=new Function(_0x4ced6a(0x260),_0x44eac6);}else{if(_0x46ce18[_0x4ced6a(0x298)](/<JS TP SLIP HEAL>\s*([\s\S]*)\s*<\/JS TP SLIP HEAL>/i)){const _0x9616c=String(RegExp['$1']),_0x369f1a=_0x3c02fc[_0x4ced6a(0x127)](_0x9616c,_0x4ced6a(0x9b),0x1,_0x4ced6a(0x264));VisuMZ[_0x4ced6a(0x1f6)][_0x4ced6a(0x245)][_0xb5a47['id']]=new Function('stateId',_0x369f1a);}}},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0xa0)]={},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x11a)]={},VisuMZ[_0x1bfe85(0x1f6)]['stateExpireJS']={},VisuMZ['SkillsStatesCore']['Parse_Notetags_State_ApplyRemoveLeaveJS']=function(_0x1cfb96){const _0x3d1d0d=_0x1bfe85,_0xc12579=_0x1cfb96[_0x3d1d0d(0x161)],_0x5727d8=_0x3d1d0d(0x1fb);if(_0xc12579[_0x3d1d0d(0x298)](/<JS ON ADD STATE>\s*([\s\S]*)\s*<\/JS ON ADD STATE>/i)){const _0x1126df=String(RegExp['$1']),_0x588e34=_0x5727d8[_0x3d1d0d(0x127)](_0x1126df);VisuMZ[_0x3d1d0d(0x1f6)]['stateAddJS'][_0x1cfb96['id']]=new Function('stateId',_0x588e34);}if(_0xc12579['match'](/<JS ON ERASE STATE>\s*([\s\S]*)\s*<\/JS ON ERASE STATE>/i)){const _0x4e07d3=String(RegExp['$1']),_0xc79398=_0x5727d8['format'](_0x4e07d3);VisuMZ['SkillsStatesCore'][_0x3d1d0d(0x11a)][_0x1cfb96['id']]=new Function(_0x3d1d0d(0x260),_0xc79398);}if(_0xc12579[_0x3d1d0d(0x298)](/<JS ON EXPIRE STATE>\s*([\s\S]*)\s*<\/JS ON EXPIRE STATE>/i)){const _0x5a627f=String(RegExp['$1']),_0x44d3b2=_0x5727d8[_0x3d1d0d(0x127)](_0x5a627f);VisuMZ['SkillsStatesCore'][_0x3d1d0d(0x197)][_0x1cfb96['id']]=new Function(_0x3d1d0d(0x260),_0x44d3b2);}},DataManager[_0x1bfe85(0x11e)]=function(_0x2c28ee){const _0x539f01=_0x1bfe85;_0x2c28ee=_0x2c28ee[_0x539f01(0x10b)]()[_0x539f01(0x151)](),this['_classIDs']=this['_classIDs']||{};if(this[_0x539f01(0xb7)][_0x2c28ee])return this['_classIDs'][_0x2c28ee];for(const _0x1de7d7 of $dataClasses){if(!_0x1de7d7)continue;let _0xfd2d79=_0x1de7d7[_0x539f01(0x24c)];_0xfd2d79=_0xfd2d79['replace'](/\x1I\[(\d+)\]/gi,''),_0xfd2d79=_0xfd2d79[_0x539f01(0x1df)](/\\I\[(\d+)\]/gi,''),this[_0x539f01(0xb7)][_0xfd2d79['toUpperCase']()[_0x539f01(0x151)]()]=_0x1de7d7['id'];}return this['_classIDs'][_0x2c28ee]||0x0;},DataManager['getSkillTypes']=function(_0x5be734){const _0x3d8bf1=_0x1bfe85;this[_0x3d8bf1(0x76)]=this['_stypeIDs']||{};if(this[_0x3d8bf1(0x76)][_0x5be734['id']])return this[_0x3d8bf1(0x76)][_0x5be734['id']];this[_0x3d8bf1(0x76)][_0x5be734['id']]=[_0x5be734['stypeId']];if(_0x5be734[_0x3d8bf1(0x161)]['match'](/<SKILL[ ](?:TYPE|TYPES):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x2c3410=JSON[_0x3d8bf1(0xca)]('['+RegExp['$1']['match'](/\d+/g)+']');this['_stypeIDs'][_0x5be734['id']]=this[_0x3d8bf1(0x76)][_0x5be734['id']][_0x3d8bf1(0x74)](_0x2c3410);}else{if(_0x5be734[_0x3d8bf1(0x161)][_0x3d8bf1(0x298)](/<SKILL[ ](?:TYPE|TYPES):[ ](.*)>/i)){const _0x5339e1=RegExp['$1'][_0x3d8bf1(0x144)](',');for(const _0x33407c of _0x5339e1){const _0x59820a=DataManager[_0x3d8bf1(0x81)](_0x33407c);if(_0x59820a)this[_0x3d8bf1(0x76)][_0x5be734['id']]['push'](_0x59820a);}}}return this[_0x3d8bf1(0x76)][_0x5be734['id']];},DataManager['getStypeIdWithName']=function(_0xca9fe1){const _0x41233d=_0x1bfe85;_0xca9fe1=_0xca9fe1[_0x41233d(0x10b)]()['trim'](),this[_0x41233d(0x76)]=this[_0x41233d(0x76)]||{};if(this[_0x41233d(0x76)][_0xca9fe1])return this[_0x41233d(0x76)][_0xca9fe1];for(let _0x59476f=0x1;_0x59476f<0x64;_0x59476f++){if(!$dataSystem[_0x41233d(0x27c)][_0x59476f])continue;let _0x3950da=$dataSystem[_0x41233d(0x27c)][_0x59476f][_0x41233d(0x10b)]()[_0x41233d(0x151)]();_0x3950da=_0x3950da[_0x41233d(0x1df)](/\x1I\[(\d+)\]/gi,''),_0x3950da=_0x3950da[_0x41233d(0x1df)](/\\I\[(\d+)\]/gi,''),this['_stypeIDs'][_0x3950da]=_0x59476f;}return this[_0x41233d(0x76)][_0xca9fe1]||0x0;},DataManager[_0x1bfe85(0x1d6)]=function(_0x462481){const _0x4f6036=_0x1bfe85;_0x462481=_0x462481['toUpperCase']()[_0x4f6036(0x151)](),this[_0x4f6036(0xf7)]=this[_0x4f6036(0xf7)]||{};if(this['_skillIDs'][_0x462481])return this['_skillIDs'][_0x462481];for(const _0xb7627f of $dataSkills){if(!_0xb7627f)continue;this[_0x4f6036(0xf7)][_0xb7627f['name'][_0x4f6036(0x10b)]()[_0x4f6036(0x151)]()]=_0xb7627f['id'];}return this[_0x4f6036(0xf7)][_0x462481]||0x0;},DataManager['getStateIdWithName']=function(_0x2a3c7f){const _0x3e3d7a=_0x1bfe85;_0x2a3c7f=_0x2a3c7f['toUpperCase']()[_0x3e3d7a(0x151)](),this[_0x3e3d7a(0x72)]=this[_0x3e3d7a(0x72)]||{};if(this[_0x3e3d7a(0x72)][_0x2a3c7f])return this[_0x3e3d7a(0x72)][_0x2a3c7f];for(const _0x2f11f3 of $dataStates){if(!_0x2f11f3)continue;this['_stateIDs'][_0x2f11f3[_0x3e3d7a(0x24c)][_0x3e3d7a(0x10b)]()[_0x3e3d7a(0x151)]()]=_0x2f11f3['id'];}return this[_0x3e3d7a(0x72)][_0x2a3c7f]||0x0;},DataManager[_0x1bfe85(0x1f7)]=function(_0x144fdd){const _0x187f7a=_0x1bfe85;this[_0x187f7a(0xc9)]=this['_stateMaxTurns']||{};if(this[_0x187f7a(0xc9)][_0x144fdd])return this['_stateMaxTurns'][_0x144fdd];return $dataStates[_0x144fdd][_0x187f7a(0x161)]['match'](/<MAX TURNS:[ ](\d+)>/i)?this['_stateMaxTurns'][_0x144fdd]=Number(RegExp['$1']):this['_stateMaxTurns'][_0x144fdd]=VisuMZ[_0x187f7a(0x1f6)][_0x187f7a(0xdb)][_0x187f7a(0x95)][_0x187f7a(0x141)],this[_0x187f7a(0xc9)][_0x144fdd];},ColorManager[_0x1bfe85(0x146)]=function(_0xd5e55f,_0x362482){const _0x3c0f1a=_0x1bfe85;return _0x362482=String(_0x362482),this['_colorCache']=this[_0x3c0f1a(0x229)]||{},_0x362482['match'](/#(.*)/i)?this[_0x3c0f1a(0x229)][_0xd5e55f]='#%1'['format'](String(RegExp['$1'])):this[_0x3c0f1a(0x229)][_0xd5e55f]=this['textColor'](Number(_0x362482)),this[_0x3c0f1a(0x229)][_0xd5e55f];},ColorManager[_0x1bfe85(0x12c)]=function(_0x1a92cf){const _0x153687=_0x1bfe85;return _0x1a92cf=String(_0x1a92cf),_0x1a92cf[_0x153687(0x298)](/#(.*)/i)?_0x153687(0xb4)['format'](String(RegExp['$1'])):this[_0x153687(0x20f)](Number(_0x1a92cf));},ColorManager['stateColor']=function(_0x274c5b){const _0x4d079a=_0x1bfe85;if(typeof _0x274c5b===_0x4d079a(0x14b))_0x274c5b=$dataStates[_0x274c5b];const _0x24e743=_0x4d079a(0x19a)[_0x4d079a(0x127)](_0x274c5b['id']);this[_0x4d079a(0x229)]=this[_0x4d079a(0x229)]||{};if(this[_0x4d079a(0x229)][_0x24e743])return this['_colorCache'][_0x24e743];const _0xdd11ff=this[_0x4d079a(0x105)](_0x274c5b);return this['getColorDataFromPluginParameters'](_0x24e743,_0xdd11ff);},ColorManager[_0x1bfe85(0x105)]=function(_0x583524){const _0x10bbea=_0x1bfe85,_0x360231=_0x583524['note'];if(_0x360231[_0x10bbea(0x298)](/<TURN COLOR:[ ](.*)>/i))return String(RegExp['$1']);else{if(_0x360231['match'](/<POSITIVE STATE>/i))return VisuMZ[_0x10bbea(0x1f6)][_0x10bbea(0xdb)][_0x10bbea(0x95)][_0x10bbea(0x6f)];else return _0x360231['match'](/<NEGATIVE STATE>/i)?VisuMZ['SkillsStatesCore'][_0x10bbea(0xdb)][_0x10bbea(0x95)]['ColorNegative']:VisuMZ[_0x10bbea(0x1f6)][_0x10bbea(0xdb)][_0x10bbea(0x95)]['ColorNeutral'];}},ColorManager[_0x1bfe85(0xb5)]=function(){const _0x50e051=_0x1bfe85,_0x58c7bb='_stored_buffColor';this[_0x50e051(0x229)]=this[_0x50e051(0x229)]||{};if(this[_0x50e051(0x229)][_0x58c7bb])return this[_0x50e051(0x229)][_0x58c7bb];const _0x2a5c96=VisuMZ[_0x50e051(0x1f6)]['Settings'][_0x50e051(0x254)][_0x50e051(0x173)];return this[_0x50e051(0x146)](_0x58c7bb,_0x2a5c96);},ColorManager[_0x1bfe85(0x190)]=function(){const _0x30f9a8=_0x1bfe85,_0x4de980='_stored_debuffColor';this[_0x30f9a8(0x229)]=this[_0x30f9a8(0x229)]||{};if(this[_0x30f9a8(0x229)][_0x4de980])return this[_0x30f9a8(0x229)][_0x4de980];const _0x229518=VisuMZ[_0x30f9a8(0x1f6)][_0x30f9a8(0xdb)][_0x30f9a8(0x254)][_0x30f9a8(0xf0)];return this[_0x30f9a8(0x146)](_0x4de980,_0x229518);},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x84)]=BattleManager['endAction'],BattleManager['endAction']=function(){const _0x4f0e26=_0x1bfe85;this['updateStatesActionEnd'](),VisuMZ[_0x4f0e26(0x1f6)][_0x4f0e26(0x84)][_0x4f0e26(0x174)](this);},BattleManager[_0x1bfe85(0x167)]=function(){const _0x381f84=_0x1bfe85,_0x4685da=VisuMZ[_0x381f84(0x1f6)][_0x381f84(0xdb)]['States'];if(!_0x4685da)return;if(_0x4685da[_0x381f84(0x19e)]===![])return;if(!this[_0x381f84(0x70)])return;this['_subject'][_0x381f84(0x167)]();},Game_Battler[_0x1bfe85(0x11c)][_0x1bfe85(0x167)]=function(){const _0x38737c=_0x1bfe85;for(const _0x49c763 of this[_0x38737c(0x147)]){const _0x1a424a=$dataStates[_0x49c763];if(!_0x1a424a)continue;if(_0x1a424a[_0x38737c(0x26a)]!==0x1)continue;this[_0x38737c(0x1e3)][_0x49c763]>0x0&&this[_0x38737c(0x1e3)][_0x49c763]--;}this['removeStatesAuto'](0x1);},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0xa3)]=function(){const _0x15d377=_0x1bfe85,_0x189d51=VisuMZ['SkillsStatesCore'][_0x15d377(0xdb)][_0x15d377(0x95)];for(const _0x5170c6 of this[_0x15d377(0x147)]){const _0x53ec1b=$dataStates[_0x5170c6];if(_0x189d51&&_0x189d51[_0x15d377(0x19e)]!==![]){if(_0x53ec1b&&_0x53ec1b[_0x15d377(0x26a)]===0x1)continue;}this['_stateTurns'][_0x5170c6]>0x0&&this[_0x15d377(0x1e3)][_0x5170c6]--;}},VisuMZ[_0x1bfe85(0x1f6)]['Game_Action_applyItemUserEffect']=Game_Action[_0x1bfe85(0x11c)][_0x1bfe85(0x98)],Game_Action[_0x1bfe85(0x11c)][_0x1bfe85(0x98)]=function(_0x13163d){const _0x5c180f=_0x1bfe85;VisuMZ[_0x5c180f(0x1f6)][_0x5c180f(0x14f)][_0x5c180f(0x174)](this,_0x13163d),this['applySkillsStatesCoreEffects'](_0x13163d);},Game_Action[_0x1bfe85(0x11c)][_0x1bfe85(0x6e)]=function(_0xe512bf){const _0x2283f6=_0x1bfe85;this['applyStateCategoryRemovalEffects'](_0xe512bf),this[_0x2283f6(0xf9)](_0xe512bf),this['applyBuffTurnManipulationEffects'](_0xe512bf),this[_0x2283f6(0x17a)](_0xe512bf);},Game_Action['prototype'][_0x1bfe85(0x79)]=function(_0x3e0429){const _0x240535=_0x1bfe85;if(_0x3e0429[_0x240535(0x1ff)]()['length']<=0x0)return;const _0x1836f9=this[_0x240535(0x1ab)]()['note'];if(_0x1836f9[_0x240535(0x298)](/<STATE[ ](.*)[ ]CATEGORY REMOVE:[ ]ALL>/i)){const _0x32e172=String(RegExp['$1']);_0x3e0429[_0x240535(0x1b0)](_0x32e172);}const _0x6017b4=_0x1836f9[_0x240535(0x298)](/<STATE[ ](.*)[ ]CATEGORY REMOVE:[ ](\d+)>/gi);if(_0x6017b4)for(const _0xc1d9de of _0x6017b4){_0xc1d9de[_0x240535(0x298)](/<STATE[ ](.*)[ ]CATEGORY REMOVE:[ ](\d+)>/i);const _0x5cf154=String(RegExp['$1']),_0x1e618e=Number(RegExp['$2']);_0x3e0429[_0x240535(0x26f)](_0x5cf154,_0x1e618e);}},Game_Action['prototype']['applyStateTurnManipulationEffects']=function(_0x55d8d6){const _0x4139a6=_0x1bfe85,_0x3bed4d=this['item']()['note'],_0x2f4ebc=_0x3bed4d['match'](/<SET STATE[ ](.*)[ ]TURNS:[ ](\d+)>/gi);if(_0x2f4ebc)for(const _0x4df772 of _0x2f4ebc){let _0x5d527a=0x0,_0x5a50e8=0x0;if(_0x4df772[_0x4139a6(0x298)](/<SET STATE[ ](\d+)[ ]TURNS:[ ](\d+)>/i))_0x5d527a=Number(RegExp['$1']),_0x5a50e8=Number(RegExp['$2']);else _0x4df772['match'](/<SET STATE[ ](.*)[ ]TURNS:[ ](\d+)>/i)&&(_0x5d527a=DataManager['getStateIdWithName'](RegExp['$1']),_0x5a50e8=Number(RegExp['$2']));_0x55d8d6[_0x4139a6(0x108)](_0x5d527a,_0x5a50e8),this[_0x4139a6(0x86)](_0x55d8d6);}const _0x11f491=_0x3bed4d[_0x4139a6(0x298)](/<STATE[ ](.*)[ ]TURNS:[ ]([\+\-]\d+)>/gi);if(_0x11f491)for(const _0x254cfa of _0x11f491){let _0x28574f=0x0,_0x467195=0x0;if(_0x254cfa[_0x4139a6(0x298)](/<STATE[ ](\d+)[ ]TURNS:[ ]([\+\-]\d+)>/i))_0x28574f=Number(RegExp['$1']),_0x467195=Number(RegExp['$2']);else _0x254cfa[_0x4139a6(0x298)](/<STATE[ ](.*)[ ]TURNS:[ ]([\+\-]\d+)>/i)&&(_0x28574f=DataManager[_0x4139a6(0x26c)](RegExp['$1']),_0x467195=Number(RegExp['$2']));_0x55d8d6['addStateTurns'](_0x28574f,_0x467195),this['makeSuccess'](_0x55d8d6);}},Game_Action[_0x1bfe85(0x11c)][_0x1bfe85(0x23e)]=function(_0x483399){const _0x407718=_0x1bfe85,_0x251b22=[_0x407718(0x1b5),'MAXMP',_0x407718(0xde),'DEF',_0x407718(0x13d),_0x407718(0x225),'AGI',_0x407718(0x262)],_0x32980d=this[_0x407718(0x1ab)]()['note'],_0x40451d=_0x32980d[_0x407718(0x298)](/<SET[ ](.*)[ ]BUFF TURNS:[ ](\d+)>/gi);if(_0x40451d)for(const _0x148292 of _0x40451d){_0x148292[_0x407718(0x298)](/<SET[ ](.*)[ ]BUFF TURNS:[ ](\d+)>/i);const _0x53627e=_0x251b22['indexOf'](String(RegExp['$1'])[_0x407718(0x10b)]()),_0xf42554=Number(RegExp['$2']);_0x53627e>=0x0&&(_0x483399[_0x407718(0x191)](_0x53627e,_0xf42554),this[_0x407718(0x86)](_0x483399));}const _0x585406=_0x32980d[_0x407718(0x298)](/<(.*)[ ]BUFF TURNS:[ ]([\+\-]\d+)>/gi);if(_0x585406)for(const _0x4db194 of _0x40451d){_0x4db194[_0x407718(0x298)](/<(.*)[ ]BUFF TURNS:[ ]([\+\-]\d+)>/i);const _0xdcef5a=_0x251b22[_0x407718(0x1ce)](String(RegExp['$1'])[_0x407718(0x10b)]()),_0x19d8ff=Number(RegExp['$2']);_0xdcef5a>=0x0&&(_0x483399['addBuffTurns'](_0xdcef5a,_0x19d8ff),this[_0x407718(0x86)](_0x483399));}},Game_Action[_0x1bfe85(0x11c)][_0x1bfe85(0x17a)]=function(_0x273234){const _0x2ac90b=_0x1bfe85,_0x12b9ec=[_0x2ac90b(0x1b5),_0x2ac90b(0x140),_0x2ac90b(0xde),_0x2ac90b(0x102),_0x2ac90b(0x13d),_0x2ac90b(0x225),'AGI','LUK'],_0x3d79b2=this[_0x2ac90b(0x1ab)]()[_0x2ac90b(0x161)],_0x435b36=_0x3d79b2[_0x2ac90b(0x298)](/<SET[ ](.*)[ ]DEBUFF TURNS:[ ](\d+)>/gi);if(_0x435b36)for(const _0x4efbb5 of _0x435b36){_0x4efbb5[_0x2ac90b(0x298)](/<SET[ ](.*)[ ]DEBUFF TURNS:[ ](\d+)>/i);const _0x27f1c5=_0x12b9ec[_0x2ac90b(0x1ce)](String(RegExp['$1'])[_0x2ac90b(0x10b)]()),_0x5b2fa4=Number(RegExp['$2']);_0x27f1c5>=0x0&&(_0x273234[_0x2ac90b(0x232)](_0x27f1c5,_0x5b2fa4),this[_0x2ac90b(0x86)](_0x273234));}const _0x1e9a5f=_0x3d79b2['match'](/<(.*)[ ]DEBUFF TURNS:[ ]([\+\-]\d+)>/gi);if(_0x1e9a5f)for(const _0x1ead54 of _0x435b36){_0x1ead54[_0x2ac90b(0x298)](/<(.*)[ ]DEBUFF TURNS:[ ]([\+\-]\d+)>/i);const _0x350954=_0x12b9ec['indexOf'](String(RegExp['$1'])['toUpperCase']()),_0x993b92=Number(RegExp['$2']);_0x350954>=0x0&&(_0x273234['addDebuffTurns'](_0x350954,_0x993b92),this[_0x2ac90b(0x86)](_0x273234));}},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x287)]=Game_BattlerBase[_0x1bfe85(0x11c)]['initMembers'],Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0xb1)]=function(){const _0xc13062=_0x1bfe85;this[_0xc13062(0x120)]={},this[_0xc13062(0x241)](),VisuMZ[_0xc13062(0x1f6)][_0xc13062(0x287)]['call'](this);},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x241)]=function(){const _0x1ae9f4=_0x1bfe85;this[_0x1ae9f4(0x1cb)]='',this[_0x1ae9f4(0x25d)]={},this[_0x1ae9f4(0x248)]={},this[_0x1ae9f4(0xbf)]={};},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x236)]=function(_0xffd7f8){const _0x351993=_0x1bfe85;return this[_0x351993(0x120)]=this[_0x351993(0x120)]||{},this[_0x351993(0x120)][_0xffd7f8]!==undefined;},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x1d2)]=Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x78)],Game_BattlerBase['prototype']['refresh']=function(){const _0x458a50=_0x1bfe85;this['_cache']={},VisuMZ[_0x458a50(0x1f6)][_0x458a50(0x1d2)][_0x458a50(0x174)](this);},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x1a1)]=Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x157)],Game_BattlerBase[_0x1bfe85(0x11c)]['eraseState']=function(_0x962bae){const _0x509924=_0x1bfe85;let _0x15775b=this[_0x509924(0xab)](_0x962bae);VisuMZ[_0x509924(0x1f6)][_0x509924(0x1a1)][_0x509924(0x174)](this,_0x962bae);if(_0x15775b&&!this[_0x509924(0xab)](_0x962bae))this[_0x509924(0x274)](_0x962bae);},Game_BattlerBase['prototype'][_0x1bfe85(0x274)]=function(_0x43ec81){const _0x26aa96=_0x1bfe85;this['clearStateData'](_0x43ec81),this[_0x26aa96(0xb9)](_0x43ec81),this[_0x26aa96(0x1c2)](_0x43ec81);},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x75)]=Game_BattlerBase['prototype'][_0x1bfe85(0xfa)],Game_BattlerBase['prototype'][_0x1bfe85(0xfa)]=function(_0x346344){const _0xcd69d4=_0x1bfe85,_0x150f6c=$dataStates[_0x346344],_0x43c610=this['stateTurns'](_0x346344),_0x4345e9=this[_0xcd69d4(0x7e)](_0x150f6c)[_0xcd69d4(0x15e)]()[_0xcd69d4(0x151)]();switch(_0x4345e9){case _0xcd69d4(0x22b):if(_0x43c610<=0x0)VisuMZ[_0xcd69d4(0x1f6)][_0xcd69d4(0x75)][_0xcd69d4(0x174)](this,_0x346344);break;case _0xcd69d4(0xc0):VisuMZ[_0xcd69d4(0x1f6)][_0xcd69d4(0x75)][_0xcd69d4(0x174)](this,_0x346344);break;case _0xcd69d4(0x295):VisuMZ[_0xcd69d4(0x1f6)][_0xcd69d4(0x75)][_0xcd69d4(0x174)](this,_0x346344),this[_0xcd69d4(0x1e3)][_0x346344]=Math[_0xcd69d4(0xf2)](this[_0xcd69d4(0x1e3)][_0x346344],_0x43c610);break;case'add':VisuMZ[_0xcd69d4(0x1f6)][_0xcd69d4(0x75)][_0xcd69d4(0x174)](this,_0x346344),this[_0xcd69d4(0x1e3)][_0x346344]+=_0x43c610;break;default:VisuMZ[_0xcd69d4(0x1f6)][_0xcd69d4(0x75)][_0xcd69d4(0x174)](this,_0x346344);break;}},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x7e)]=function(_0x2cba0b){const _0x396d2b=_0x1bfe85,_0x123ed0=_0x2cba0b[_0x396d2b(0x161)];return _0x123ed0[_0x396d2b(0x298)](/<REAPPLY RULES:[ ](.*)>/i)?String(RegExp['$1']):VisuMZ[_0x396d2b(0x1f6)]['Settings']['States']['ReapplyRules'];},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x28d)]=Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0xff)],Game_BattlerBase['prototype'][_0x1bfe85(0xff)]=function(_0x22b839,_0x4e5160){const _0xefb459=_0x1bfe85,_0x471e76=VisuMZ[_0xefb459(0x1f6)]['Settings'][_0xefb459(0x254)][_0xefb459(0xb6)],_0x4c5f3c=this['buffTurns'](_0x22b839);switch(_0x471e76){case _0xefb459(0x22b):if(_0x4c5f3c<=0x0)this[_0xefb459(0x199)][_0x22b839]=_0x4e5160;break;case _0xefb459(0xc0):this[_0xefb459(0x199)][_0x22b839]=_0x4e5160;break;case _0xefb459(0x295):this[_0xefb459(0x199)][_0x22b839]=Math[_0xefb459(0xf2)](_0x4c5f3c,_0x4e5160);break;case'add':this['_buffTurns'][_0x22b839]+=_0x4e5160;break;default:VisuMZ[_0xefb459(0x1f6)][_0xefb459(0x28d)]['call'](this,_0x22b839,_0x4e5160);break;}const _0x1885cc=VisuMZ['SkillsStatesCore'][_0xefb459(0xdb)][_0xefb459(0x254)]['MaxTurns'];this[_0xefb459(0x199)][_0x22b839]=this[_0xefb459(0x199)][_0x22b839]['clamp'](0x0,_0x1885cc);},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0xbc)]=function(){const _0x3e77b3=_0x1bfe85;if(this[_0x3e77b3(0x120)][_0x3e77b3(0xe7)]!==undefined)return this[_0x3e77b3(0x120)]['groupDefeat'];this[_0x3e77b3(0x120)][_0x3e77b3(0xe7)]=![];const _0x226913=this[_0x3e77b3(0x1ff)]();for(const _0x179ab9 of _0x226913){if(!_0x179ab9)continue;if(_0x179ab9['note']['match'](/<GROUP DEFEAT>/i)){this[_0x3e77b3(0x120)][_0x3e77b3(0xe7)]=!![];break;}}return this['_cache'][_0x3e77b3(0xe7)];},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x27f)]=Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0xba)],Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0xba)]=function(){const _0x4e9178=_0x1bfe85;this['getStateRetainType']()!==''?this[_0x4e9178(0x87)]():(VisuMZ[_0x4e9178(0x1f6)][_0x4e9178(0x27f)][_0x4e9178(0x174)](this),this['initMembersSkillsStatesCore']());},Game_BattlerBase[_0x1bfe85(0x11c)]['clearStatesWithStateRetain']=function(){const _0x5f440e=_0x1bfe85,_0x4a3450=this[_0x5f440e(0x1ff)]();for(const _0x1e757d of _0x4a3450){if(_0x1e757d&&this['canClearState'](_0x1e757d))this[_0x5f440e(0x157)](_0x1e757d['id']);}this[_0x5f440e(0x120)]={};},Game_BattlerBase[_0x1bfe85(0x11c)]['canClearState']=function(_0x50bc4c){const _0x31bce8=_0x1bfe85,_0x505257=this[_0x31bce8(0x240)]();if(_0x505257!==''){const _0x1d8ae4=_0x50bc4c[_0x31bce8(0x161)];if(_0x505257===_0x31bce8(0x237)&&_0x1d8ae4[_0x31bce8(0x298)](/<NO DEATH CLEAR>/i))return![];if(_0x505257===_0x31bce8(0x206)&&_0x1d8ae4[_0x31bce8(0x298)](/<NO RECOVER ALL CLEAR>/i))return![];}return this[_0x31bce8(0xab)](_0x50bc4c['id']);},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x240)]=function(){const _0x23cb30=_0x1bfe85;return this[_0x23cb30(0x1cb)];},Game_BattlerBase['prototype']['setStateRetainType']=function(_0x376929){const _0x34bc42=_0x1bfe85;this[_0x34bc42(0x1cb)]=_0x376929;},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x275)]=function(){this['_stateRetainType']='';},VisuMZ['SkillsStatesCore']['Game_BattlerBase_die']=Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x110)],Game_BattlerBase[_0x1bfe85(0x11c)]['die']=function(){const _0x2a2e28=_0x1bfe85;this[_0x2a2e28(0x14e)](_0x2a2e28(0x237)),VisuMZ[_0x2a2e28(0x1f6)][_0x2a2e28(0xcc)]['call'](this),this[_0x2a2e28(0x275)]();},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0xdf)]=Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x14c)],Game_BattlerBase[_0x1bfe85(0x11c)]['recoverAll']=function(){const _0x4273e0=_0x1bfe85;this[_0x4273e0(0x14e)](_0x4273e0(0x206)),VisuMZ[_0x4273e0(0x1f6)][_0x4273e0(0xdf)][_0x4273e0(0x174)](this),this[_0x4273e0(0x275)]();},Game_BattlerBase[_0x1bfe85(0x11c)]['canPaySkillCost']=function(_0x3aee74){const _0x16bd8d=_0x1bfe85;for(settings of VisuMZ[_0x16bd8d(0x1f6)][_0x16bd8d(0xdb)][_0x16bd8d(0x20d)]){const _0x510363=settings['CalcJS']['call'](this,_0x3aee74);if(!settings[_0x16bd8d(0x92)][_0x16bd8d(0x174)](this,_0x3aee74,_0x510363))return![];}return!![];},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x91)]=function(_0x1f2f6c){const _0x114cd1=_0x1bfe85;for(settings of VisuMZ[_0x114cd1(0x1f6)][_0x114cd1(0xdb)]['Costs']){const _0x1a240f=settings[_0x114cd1(0x1da)][_0x114cd1(0x174)](this,_0x1f2f6c);settings['PayJS']['call'](this,_0x1f2f6c,_0x1a240f);}},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x12b)]=Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x198)],Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x198)]=function(_0x3ce12b){const _0x1947b8=_0x1bfe85;if(!_0x3ce12b)return![];if(!VisuMZ[_0x1947b8(0x1f6)][_0x1947b8(0x12b)][_0x1947b8(0x174)](this,_0x3ce12b))return![];if(!this[_0x1947b8(0x73)](_0x3ce12b))return![];if(!this['meetsSkillConditionsEnableJS'](_0x3ce12b))return![];if(!this[_0x1947b8(0x150)](_0x3ce12b))return![];return!![];},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x73)]=function(_0x1d2f2b){const _0x4b591f=_0x1bfe85;if(!this[_0x4b591f(0x184)](_0x1d2f2b))return![];return!![];},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x184)]=function(_0xb38fd0){const _0x26be14=_0x1bfe85,_0x1cd6e4=_0xb38fd0[_0x26be14(0x161)];if(_0x1cd6e4[_0x26be14(0x298)](/<ENABLE[ ](?:SW|SWITCH|SWITCHES):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x3bf6ae=JSON['parse']('['+RegExp['$1'][_0x26be14(0x298)](/\d+/g)+']');for(const _0x344129 of _0x3bf6ae){if(!$gameSwitches[_0x26be14(0x8e)](_0x344129))return![];}return!![];}if(_0x1cd6e4['match'](/<ENABLE ALL[ ](?:SW|SWITCH|SWITCHES):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x61c4b6=JSON[_0x26be14(0xca)]('['+RegExp['$1'][_0x26be14(0x298)](/\d+/g)+']');for(const _0x10a0a4 of _0x61c4b6){if(!$gameSwitches[_0x26be14(0x8e)](_0x10a0a4))return![];}return!![];}if(_0x1cd6e4[_0x26be14(0x298)](/<ENABLE ANY[ ](?:SW|SWITCH|SWITCHES):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x525357=JSON[_0x26be14(0xca)]('['+RegExp['$1'][_0x26be14(0x298)](/\d+/g)+']');for(const _0x52878e of _0x525357){if($gameSwitches[_0x26be14(0x8e)](_0x52878e))return!![];}return![];}if(_0x1cd6e4[_0x26be14(0x298)](/<DISABLE[ ](?:SW|SWITCH|SWITCHES):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x41d7b4=JSON[_0x26be14(0xca)]('['+RegExp['$1']['match'](/\d+/g)+']');for(const _0x392d8f of _0x41d7b4){if(!$gameSwitches[_0x26be14(0x8e)](_0x392d8f))return!![];}return![];}if(_0x1cd6e4[_0x26be14(0x298)](/<DISABLE ALL[ ](?:SW|SWITCH|SWITCHES):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x35767e=JSON[_0x26be14(0xca)]('['+RegExp['$1'][_0x26be14(0x298)](/\d+/g)+']');for(const _0x18d50e of _0x35767e){if(!$gameSwitches['value'](_0x18d50e))return!![];}return![];}if(_0x1cd6e4[_0x26be14(0x298)](/<DISABLE ANY[ ](?:SW|SWITCH|SWITCHES):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x30c045=JSON[_0x26be14(0xca)]('['+RegExp['$1']['match'](/\d+/g)+']');for(const _0x3aa56d of _0x30c045){if($gameSwitches[_0x26be14(0x8e)](_0x3aa56d))return![];}return!![];}return!![];},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x263)]=function(_0x4dca8e){const _0x25ecbd=_0x1bfe85,_0x407f0a=_0x4dca8e['note'],_0x194931=VisuMZ['SkillsStatesCore'][_0x25ecbd(0x19b)];return _0x194931[_0x4dca8e['id']]?_0x194931[_0x4dca8e['id']][_0x25ecbd(0x174)](this,_0x4dca8e):!![];},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x150)]=function(_0x65135a){const _0x59556d=_0x1bfe85;return VisuMZ[_0x59556d(0x1f6)][_0x59556d(0xdb)][_0x59556d(0x8c)][_0x59556d(0x6b)][_0x59556d(0x174)](this,_0x65135a);},VisuMZ['SkillsStatesCore']['Game_BattlerBase_skillMpCost']=Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x136)],Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x136)]=function(_0x438321){const _0x5d0d60=_0x1bfe85;for(settings of VisuMZ[_0x5d0d60(0x1f6)][_0x5d0d60(0xdb)]['Costs']){if(settings[_0x5d0d60(0xfd)]['toUpperCase']()==='MP')return settings[_0x5d0d60(0x1da)]['call'](this,_0x438321);}return VisuMZ[_0x5d0d60(0x1f6)][_0x5d0d60(0x175)][_0x5d0d60(0x174)](this,_0x438321);},VisuMZ[_0x1bfe85(0x1f6)]['Game_BattlerBase_skillTpCost']=Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x252)],Game_BattlerBase['prototype'][_0x1bfe85(0x252)]=function(_0x386ecd){const _0x170538=_0x1bfe85;for(settings of VisuMZ[_0x170538(0x1f6)][_0x170538(0xdb)][_0x170538(0x20d)]){if(settings[_0x170538(0xfd)][_0x170538(0x10b)]()==='TP')return settings[_0x170538(0x1da)]['call'](this,_0x386ecd);}return VisuMZ[_0x170538(0x1f6)][_0x170538(0x28a)][_0x170538(0x174)](this,_0x386ecd);},Game_BattlerBase['prototype'][_0x1bfe85(0xaa)]=function(_0x11a66f){const _0x524fb8=_0x1bfe85;if(typeof _0x11a66f===_0x524fb8(0x14b))_0x11a66f=$dataStates[_0x11a66f];return this['states']()[_0x524fb8(0xa6)](_0x11a66f);},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0xfe)]=Game_BattlerBase[_0x1bfe85(0x11c)]['states'],Game_BattlerBase[_0x1bfe85(0x11c)]['states']=function(){const _0x276513=_0x1bfe85;let _0x5d6945=VisuMZ[_0x276513(0x1f6)]['Game_BattlerBase_states']['call'](this);if(this[_0x276513(0x230)])return _0x5d6945;return this[_0x276513(0x230)]=!![],this[_0x276513(0x291)](_0x5d6945),this['_checkingPassiveStates']=undefined,_0x5d6945;},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x291)]=function(_0x5df232){const _0x12ceed=_0x1bfe85,_0x12eb6b=this[_0x12ceed(0x131)]();for(state of _0x12eb6b){if(!state)continue;if(!this['isPassiveStateStackable'](state)&&_0x5df232[_0x12ceed(0xa6)](state))continue;_0x5df232[_0x12ceed(0x7a)](state);}_0x12eb6b[_0x12ceed(0x97)]>0x0&&_0x5df232[_0x12ceed(0xcd)]((_0x4d7650,_0x3069b7)=>{const _0x20fdcd=_0x12ceed,_0x4c4889=_0x4d7650['priority'],_0x480ca5=_0x3069b7[_0x20fdcd(0x1e0)];if(_0x4c4889!==_0x480ca5)return _0x480ca5-_0x4c4889;return _0x4d7650-_0x3069b7;});},Game_BattlerBase['prototype'][_0x1bfe85(0x1ea)]=function(_0x530d94){const _0x5813ef=_0x1bfe85;return _0x530d94[_0x5813ef(0x161)]['match'](/<PASSIVE STACKABLE>/i);},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x166)]=function(){const _0x503ad3=_0x1bfe85,_0x36bc7b=[];for(const _0x2af2e8 of this[_0x503ad3(0x120)][_0x503ad3(0x131)]){const _0x32208e=$dataStates[_0x2af2e8];if(!_0x32208e)continue;if(!this['meetsPassiveStateConditions'](_0x32208e))continue;_0x36bc7b[_0x503ad3(0x7a)](_0x32208e);}return _0x36bc7b;},Game_BattlerBase[_0x1bfe85(0x11c)]['meetsPassiveStateConditions']=function(_0xe3ae2b){const _0x533a9a=_0x1bfe85;if(!this[_0x533a9a(0x1a7)](_0xe3ae2b))return![];if(!this[_0x533a9a(0xe1)](_0xe3ae2b))return![];if(!this[_0x533a9a(0x22e)](_0xe3ae2b))return![];if(!this[_0x533a9a(0xdc)](_0xe3ae2b))return![];return!![];},Game_BattlerBase['prototype'][_0x1bfe85(0x1a7)]=function(_0x326b47){return!![];},Game_Actor[_0x1bfe85(0x11c)][_0x1bfe85(0x1a7)]=function(_0x965d6d){const _0x1154f5=_0x1bfe85,_0x44f1d7=_0x965d6d[_0x1154f5(0x161)];if(_0x44f1d7[_0x1154f5(0x298)](/<PASSIVE CONDITION[ ](?:CLASS|CLASSES):[ ](.*)>/i)){const _0x56bf0f=String(RegExp['$1'])[_0x1154f5(0x144)](',')[_0x1154f5(0x224)](_0x231f95=>_0x231f95['trim']()),_0x148728=VisuMZ[_0x1154f5(0x1f6)]['ParseClassIDs'](_0x56bf0f);return _0x148728[_0x1154f5(0xa6)](this[_0x1154f5(0x235)]());}if(_0x44f1d7[_0x1154f5(0x298)](/<PASSIVE CONDITION[ ](?:MULTICLASS|MULTICLASSES):[ ](.*)>/i)){const _0x8a5346=String(RegExp['$1'])['split'](',')[_0x1154f5(0x224)](_0x263994=>_0x263994['trim']()),_0xf20e86=VisuMZ[_0x1154f5(0x1f6)]['ParseClassIDs'](_0x8a5346);let _0x5aebcb=[this['currentClass']()];return Imported['VisuMZ_2_ClassChangeSystem']&&this[_0x1154f5(0x267)]&&(_0x5aebcb=this[_0x1154f5(0x267)]()),_0xf20e86[_0x1154f5(0xfb)](_0x4d5462=>_0x5aebcb[_0x1154f5(0xa6)](_0x4d5462))[_0x1154f5(0x97)]>0x0;}return Game_BattlerBase[_0x1154f5(0x11c)][_0x1154f5(0x1a7)][_0x1154f5(0x174)](this,_0x965d6d);},VisuMZ['SkillsStatesCore'][_0x1bfe85(0x1ad)]=function(_0x3cd214){const _0x5bcac6=_0x1bfe85,_0x6c61e2=[];for(let _0x596856 of _0x3cd214){_0x596856=(String(_0x596856)||'')[_0x5bcac6(0x151)]();const _0x38a6ab=/^\d+$/['test'](_0x596856);_0x38a6ab?_0x6c61e2['push'](Number(_0x596856)):_0x6c61e2['push'](DataManager[_0x5bcac6(0x11e)](_0x596856));}return _0x6c61e2['map'](_0x3dfe37=>$dataClasses[Number(_0x3dfe37)])[_0x5bcac6(0x6a)](null);},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0xe1)]=function(_0x994175){const _0x3ea006=_0x1bfe85,_0x46f1d2=_0x994175[_0x3ea006(0x161)];if(_0x46f1d2[_0x3ea006(0x298)](/<PASSIVE CONDITION[ ](?:SWITCH|SWITCHES)[ ]ON:[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0xd0c53=JSON[_0x3ea006(0xca)]('['+RegExp['$1']['match'](/\d+/g)+']');for(const _0x199643 of _0xd0c53){if(!$gameSwitches[_0x3ea006(0x8e)](_0x199643))return![];}return!![];}if(_0x46f1d2[_0x3ea006(0x298)](/<PASSIVE CONDITION ALL[ ](?:SWITCH|SWITCHES)[ ]ON:[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x507e53=JSON[_0x3ea006(0xca)]('['+RegExp['$1'][_0x3ea006(0x298)](/\d+/g)+']');for(const _0x1e0736 of _0x507e53){if(!$gameSwitches[_0x3ea006(0x8e)](_0x1e0736))return![];}return!![];}if(_0x46f1d2[_0x3ea006(0x298)](/<PASSIVE CONDITION ANY[ ](?:SWITCH|SWITCHES)[ ]ON:[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x456e44=JSON[_0x3ea006(0xca)]('['+RegExp['$1'][_0x3ea006(0x298)](/\d+/g)+']');for(const _0x6665e4 of _0x456e44){if($gameSwitches[_0x3ea006(0x8e)](_0x6665e4))return!![];}return![];}if(_0x46f1d2[_0x3ea006(0x298)](/<PASSIVE CONDITION[ ](?:SWITCH|SWITCHES)[ ]OFF:[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x12dfe2=JSON['parse']('['+RegExp['$1'][_0x3ea006(0x298)](/\d+/g)+']');for(const _0x47901f of _0x12dfe2){if(!$gameSwitches['value'](_0x47901f))return!![];}return![];}if(_0x46f1d2[_0x3ea006(0x298)](/<PASSIVE CONDITION ALL[ ](?:SWITCH|SWITCHES)[ ]OFF:[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0xf2b203=JSON[_0x3ea006(0xca)]('['+RegExp['$1'][_0x3ea006(0x298)](/\d+/g)+']');for(const _0x4d779d of _0xf2b203){if(!$gameSwitches['value'](_0x4d779d))return!![];}return![];}if(_0x46f1d2[_0x3ea006(0x298)](/<PASSIVE CONDITION ANY[ ](?:SWITCH|SWITCHES)[ ]OFF:[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x354251=JSON[_0x3ea006(0xca)]('['+RegExp['$1'][_0x3ea006(0x298)](/\d+/g)+']');for(const _0x2101af of _0x354251){if($gameSwitches[_0x3ea006(0x8e)](_0x2101af))return![];}return!![];}return!![];},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x22e)]=function(_0x459dd6){const _0x4f6eef=_0x1bfe85,_0x229201=VisuMZ[_0x4f6eef(0x1f6)][_0x4f6eef(0x129)];if(_0x229201[_0x459dd6['id']]&&!_0x229201[_0x459dd6['id']][_0x4f6eef(0x174)](this,_0x459dd6))return![];return!![];},Game_BattlerBase['prototype'][_0x1bfe85(0xdc)]=function(_0x21099f){const _0x4e385d=_0x1bfe85;return VisuMZ[_0x4e385d(0x1f6)][_0x4e385d(0xdb)][_0x4e385d(0x152)][_0x4e385d(0x1a3)]['call'](this,_0x21099f);},Game_BattlerBase[_0x1bfe85(0x11c)]['passiveStates']=function(){const _0x2fee19=_0x1bfe85;if(this[_0x2fee19(0x236)](_0x2fee19(0x131)))return this[_0x2fee19(0x166)]();if(this[_0x2fee19(0x111)])return[];return this[_0x2fee19(0x111)]=!![],this[_0x2fee19(0x120)][_0x2fee19(0x131)]=[],this[_0x2fee19(0x172)](),this[_0x2fee19(0x247)](),this[_0x2fee19(0x1be)](),this[_0x2fee19(0x111)]=undefined,this[_0x2fee19(0x166)]();},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x172)]=function(){const _0x18749f=_0x1bfe85;if(Imported['VisuMZ_1_ElementStatusCore'])this[_0x18749f(0x9e)]();},Game_BattlerBase[_0x1bfe85(0x11c)]['passiveStateObjects']=function(){return[];},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x247)]=function(){const _0x2b6501=_0x1bfe85,_0x1c4ff8=this[_0x2b6501(0x22a)]();for(const _0x1532e3 of _0x1c4ff8){if(!_0x1532e3)continue;const _0x563145=_0x1532e3['note'][_0x2b6501(0x298)](/<PASSIVE (?:STATE|STATES):[ ](.*)>/gi);if(_0x563145)for(const _0x3a31bc of _0x563145){_0x3a31bc[_0x2b6501(0x298)](/<PASSIVE (?:STATE|STATES):[ ](.*)>/i);const _0x118d58=RegExp['$1'];if(_0x118d58[_0x2b6501(0x298)](/(\d+(?:\s*,\s*\d+)*)/i)){const _0x49b447=JSON[_0x2b6501(0xca)]('['+RegExp['$1']['match'](/\d+/g)+']');this[_0x2b6501(0x120)]['passiveStates']=this[_0x2b6501(0x120)][_0x2b6501(0x131)][_0x2b6501(0x74)](_0x49b447);}else{const _0x319d40=_0x118d58[_0x2b6501(0x144)](',');for(const _0x368713 of _0x319d40){const _0x8873f9=DataManager[_0x2b6501(0x26c)](_0x368713);if(_0x8873f9)this['_cache'][_0x2b6501(0x131)][_0x2b6501(0x7a)](_0x8873f9);}}}}},Game_BattlerBase['prototype'][_0x1bfe85(0x1be)]=function(){const _0x1920bd=_0x1bfe85,_0x2ae225=VisuMZ[_0x1920bd(0x1f6)][_0x1920bd(0xdb)][_0x1920bd(0x152)][_0x1920bd(0x1b3)];this['_cache'][_0x1920bd(0x131)]=this[_0x1920bd(0x120)][_0x1920bd(0x131)][_0x1920bd(0x74)](_0x2ae225);},Game_BattlerBase['prototype'][_0x1bfe85(0x1bd)]=function(_0x53d1a0){const _0x47c8dc=_0x1bfe85;if(typeof _0x53d1a0!==_0x47c8dc(0x14b))_0x53d1a0=_0x53d1a0['id'];return this[_0x47c8dc(0x1e3)][_0x53d1a0]||0x0;},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x108)]=function(_0x126efc,_0x3212c9){const _0x31a221=_0x1bfe85;if(typeof _0x126efc!==_0x31a221(0x14b))_0x126efc=_0x126efc['id'];if(this[_0x31a221(0xab)](_0x126efc)){const _0x333a44=DataManager[_0x31a221(0x1f7)](_0x126efc);this['_stateTurns'][_0x126efc]=_0x3212c9[_0x31a221(0x7f)](0x0,_0x333a44);if(this[_0x31a221(0x1e3)][_0x126efc]<=0x0)this[_0x31a221(0xb2)](_0x126efc);}},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x217)]=function(_0x411086,_0x58b514){const _0xc7a1bc=_0x1bfe85;if(typeof _0x411086!=='number')_0x411086=_0x411086['id'];this[_0xc7a1bc(0xab)](_0x411086)&&(_0x58b514+=this[_0xc7a1bc(0x1bd)](_0x411086),this[_0xc7a1bc(0x108)](_0x411086,_0x58b514));},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x15f)]=Game_BattlerBase[_0x1bfe85(0x11c)]['eraseBuff'],Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x22d)]=function(_0x3fd5db){const _0x322ca3=_0x1bfe85,_0x319687=this[_0x322ca3(0xc2)][_0x3fd5db];VisuMZ[_0x322ca3(0x1f6)][_0x322ca3(0x15f)][_0x322ca3(0x174)](this,_0x3fd5db);if(_0x319687>0x0)this[_0x322ca3(0x186)](_0x3fd5db);if(_0x319687<0x0)this[_0x322ca3(0x1e1)](_0x3fd5db);},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x196)]=Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0xed)],Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0xed)]=function(_0x469dde){const _0x351978=_0x1bfe85;VisuMZ['SkillsStatesCore'][_0x351978(0x196)]['call'](this,_0x469dde);if(!this[_0x351978(0x16f)](_0x469dde))this[_0x351978(0x22d)](_0x469dde);},VisuMZ[_0x1bfe85(0x1f6)]['Game_BattlerBase_decreaseBuff']=Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x200)],Game_BattlerBase[_0x1bfe85(0x11c)]['decreaseBuff']=function(_0xce8d9c){const _0x32d5ff=_0x1bfe85;VisuMZ[_0x32d5ff(0x1f6)][_0x32d5ff(0x226)][_0x32d5ff(0x174)](this,_0xce8d9c);if(!this[_0x32d5ff(0x16f)](_0xce8d9c))this[_0x32d5ff(0x22d)](_0xce8d9c);},Game_BattlerBase['prototype']['onEraseBuff']=function(_0x29d692){},Game_BattlerBase['prototype'][_0x1bfe85(0x1e1)]=function(_0xb641b7){},Game_BattlerBase['prototype']['isMaxBuffAffected']=function(_0x4ce189){const _0xde12f1=_0x1bfe85;return this[_0xde12f1(0xc2)][_0x4ce189]===VisuMZ[_0xde12f1(0x1f6)][_0xde12f1(0xdb)][_0xde12f1(0x254)]['StackBuffMax'];},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x2a0)]=function(_0x2f902c){const _0x28ca91=_0x1bfe85;return this['_buffs'][_0x2f902c]===-VisuMZ['SkillsStatesCore'][_0x28ca91(0xdb)][_0x28ca91(0x254)]['StackDebuffMax'];},VisuMZ['SkillsStatesCore'][_0x1bfe85(0x269)]=Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0xd0)],Game_BattlerBase['prototype'][_0x1bfe85(0xd0)]=function(_0x308859,_0x30e42d){const _0x578d93=_0x1bfe85;return _0x308859=_0x308859[_0x578d93(0x7f)](-0x2,0x2),VisuMZ[_0x578d93(0x1f6)][_0x578d93(0x269)]['call'](this,_0x308859,_0x30e42d);},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x88)]=function(_0x287587){const _0x2400aa=_0x1bfe85,_0x575cd1=this[_0x2400aa(0xc2)][_0x287587];return VisuMZ[_0x2400aa(0x1f6)][_0x2400aa(0xdb)]['Buffs'][_0x2400aa(0x279)][_0x2400aa(0x174)](this,_0x287587,_0x575cd1);},Game_BattlerBase[_0x1bfe85(0x11c)]['buffTurns']=function(_0xea4d6c){const _0x2a6d95=_0x1bfe85;return this[_0x2a6d95(0x199)][_0xea4d6c]||0x0;},Game_BattlerBase['prototype'][_0x1bfe85(0x23c)]=function(_0x2e5a71){return this['buffTurns'](_0x2e5a71);},Game_BattlerBase['prototype']['setBuffTurns']=function(_0x2e78f8,_0x2e0260){const _0x558458=_0x1bfe85;if(this[_0x558458(0x294)](_0x2e78f8)){const _0x85c703=VisuMZ[_0x558458(0x1f6)][_0x558458(0xdb)][_0x558458(0x254)]['MaxTurns'];this[_0x558458(0x199)][_0x2e78f8]=_0x2e0260[_0x558458(0x7f)](0x0,_0x85c703);}},Game_BattlerBase['prototype'][_0x1bfe85(0x12f)]=function(_0x416ba9,_0x169910){const _0x1d68af=_0x1bfe85;this[_0x1d68af(0x294)](_0x416ba9)&&(_0x169910+=this[_0x1d68af(0x8b)](stateId),this[_0x1d68af(0x108)](_0x416ba9,_0x169910));},Game_BattlerBase[_0x1bfe85(0x11c)]['setDebuffTurns']=function(_0x610772,_0x253873){const _0x23d41a=_0x1bfe85;if(this[_0x23d41a(0xeb)](_0x610772)){const _0x51e37a=VisuMZ[_0x23d41a(0x1f6)]['Settings'][_0x23d41a(0x254)][_0x23d41a(0x141)];this[_0x23d41a(0x199)][_0x610772]=_0x253873[_0x23d41a(0x7f)](0x0,_0x51e37a);}},Game_BattlerBase[_0x1bfe85(0x11c)]['addDebuffTurns']=function(_0x4caff2,_0x24de78){const _0x2d27b2=_0x1bfe85;this[_0x2d27b2(0xeb)](_0x4caff2)&&(_0x24de78+=this[_0x2d27b2(0x8b)](stateId),this[_0x2d27b2(0x108)](_0x4caff2,_0x24de78));},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x1c4)]=function(_0x200dd6){const _0x230fa1=_0x1bfe85;if(typeof _0x200dd6!==_0x230fa1(0x14b))_0x200dd6=_0x200dd6['id'];return this[_0x230fa1(0x25d)]=this[_0x230fa1(0x25d)]||{},this['_stateData'][_0x200dd6]=this[_0x230fa1(0x25d)][_0x200dd6]||{},this['_stateData'][_0x200dd6];},Game_BattlerBase[_0x1bfe85(0x11c)]['getStateData']=function(_0x4f2b6f,_0x29288b){const _0x3858d8=_0x1bfe85;if(typeof _0x4f2b6f!==_0x3858d8(0x14b))_0x4f2b6f=_0x4f2b6f['id'];const _0x4a0360=this[_0x3858d8(0x1c4)](_0x4f2b6f);return _0x4a0360[_0x29288b];},Game_BattlerBase['prototype'][_0x1bfe85(0x6c)]=function(_0x1f532a,_0x2cdf54,_0x531c28){const _0x4a536d=_0x1bfe85;if(typeof _0x1f532a!==_0x4a536d(0x14b))_0x1f532a=_0x1f532a['id'];const _0x1ebe7e=this['stateData'](_0x1f532a);_0x1ebe7e[_0x2cdf54]=_0x531c28;},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0xe2)]=function(_0x45278c){const _0x27750d=_0x1bfe85;if(typeof _0x45278c!==_0x27750d(0x14b))_0x45278c=_0x45278c['id'];this['_stateData']=this[_0x27750d(0x25d)]||{},this[_0x27750d(0x25d)][_0x45278c]={};},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x189)]=function(_0xafbfe1){const _0x301016=_0x1bfe85;if(typeof _0xafbfe1!==_0x301016(0x14b))_0xafbfe1=_0xafbfe1['id'];return this['_stateDisplay']=this[_0x301016(0x248)]||{},this[_0x301016(0x248)][_0xafbfe1]===undefined&&(this['_stateDisplay'][_0xafbfe1]=''),this[_0x301016(0x248)][_0xafbfe1];},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0xf8)]=function(_0x39a43f,_0x51b407){const _0x516c43=_0x1bfe85;if(typeof _0x39a43f!==_0x516c43(0x14b))_0x39a43f=_0x39a43f['id'];this[_0x516c43(0x248)]=this['_stateDisplay']||{},this['_stateDisplay'][_0x39a43f]=_0x51b407;},Game_BattlerBase[_0x1bfe85(0x11c)]['clearStateDisplay']=function(_0x4a4e84){const _0x1b2eb0=_0x1bfe85;if(typeof _0x4a4e84!==_0x1b2eb0(0x14b))_0x4a4e84=_0x4a4e84['id'];this[_0x1b2eb0(0x248)]=this['_stateDisplay']||{},this[_0x1b2eb0(0x248)][_0x4a4e84]='';},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x261)]=function(_0x3c97bf){const _0x1bbceb=_0x1bfe85;if(typeof _0x3c97bf!=='number')_0x3c97bf=_0x3c97bf['id'];this[_0x1bbceb(0xbf)]=this[_0x1bbceb(0xbf)]||{},this[_0x1bbceb(0xbf)][_0x3c97bf]=this[_0x1bbceb(0xbf)][_0x3c97bf]||_0x1bbceb(0x1f2);const _0x3279f8=this[_0x1bbceb(0xbf)][_0x3c97bf];return this[_0x1bbceb(0x1c1)](_0x3279f8);},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x180)]=function(_0x1a6ea5,_0x2d7040){const _0x423693=_0x1bfe85;this[_0x423693(0xbf)]=this[_0x423693(0xbf)]||{};const _0x28ec6d=_0x2d7040?this[_0x423693(0x118)](_0x2d7040):this['getCurrentStateOriginKey']();this[_0x423693(0xbf)][_0x1a6ea5]=_0x28ec6d;},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x1c2)]=function(_0x3a07a2){const _0x4e0c9c=_0x1bfe85;this[_0x4e0c9c(0xbf)]=this[_0x4e0c9c(0xbf)]||{},delete this[_0x4e0c9c(0xbf)][_0x3a07a2];},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x209)]=function(){const _0x1e89a8=_0x1bfe85,_0x4a201d=this[_0x1e89a8(0x1a6)]();return this['convertTargetToStateOriginKey'](_0x4a201d);},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x1a6)]=function(){const _0x2c6ae2=_0x1bfe85;if($gameParty[_0x2c6ae2(0x130)]()){if(BattleManager[_0x2c6ae2(0x70)])return BattleManager[_0x2c6ae2(0x70)];else{if(BattleManager[_0x2c6ae2(0x125)])return BattleManager[_0x2c6ae2(0x125)];}}else{const _0x497c8e=SceneManager[_0x2c6ae2(0x1e8)];if(![Scene_Map,Scene_Item][_0x2c6ae2(0xa6)](_0x497c8e[_0x2c6ae2(0x282)]))return $gameParty['menuActor']();}return this;},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x118)]=function(_0x2c1666){const _0x6a247=_0x1bfe85;if(!_0x2c1666)return _0x6a247(0x1f2);if(_0x2c1666[_0x6a247(0x1c5)]())return _0x6a247(0x29a)[_0x6a247(0x127)](_0x2c1666['actorId']());else{const _0x4615a2='<enemy-%1>'['format'](_0x2c1666[_0x6a247(0x94)]()),_0x59d58c='<member-%1>'[_0x6a247(0x127)](_0x2c1666[_0x6a247(0x16a)]()),_0x5cae8a=_0x6a247(0x15c)['format']($gameTroop[_0x6a247(0x160)]());return'%1\x20%2\x20%3'['format'](_0x4615a2,_0x59d58c,_0x5cae8a);}return _0x6a247(0x1f2);},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x1c1)]=function(_0xe25883){const _0x15664c=_0x1bfe85;if(_0xe25883===_0x15664c(0x1f2))return this;else{if(_0xe25883[_0x15664c(0x298)](/<actor-(\d+)>/i))return $gameActors[_0x15664c(0x163)](Number(RegExp['$1']));else{if($gameParty[_0x15664c(0x130)]()&&_0xe25883['match'](/<troop-(\d+)>/i)){const _0x52b4c8=Number(RegExp['$1']);if(_0x52b4c8===$gameTroop[_0x15664c(0x160)]()){if(_0xe25883[_0x15664c(0x298)](/<member-(\d+)>/i))return $gameTroop[_0x15664c(0x25a)]()[Number(RegExp['$1'])];}}if(_0xe25883[_0x15664c(0x298)](/<enemy-(\d+)>/i))return new Game_Enemy(Number(RegExp['$1']),-0x1f4,-0x1f4);}}return this;},VisuMZ['SkillsStatesCore'][_0x1bfe85(0xd3)]=Game_Battler[_0x1bfe85(0x11c)]['addState'],Game_Battler['prototype'][_0x1bfe85(0x16d)]=function(_0x41c1e3){const _0x24426a=_0x1bfe85,_0x558cb7=this[_0x24426a(0x1f4)](_0x41c1e3);VisuMZ['SkillsStatesCore']['Game_Battler_addState'][_0x24426a(0x174)](this,_0x41c1e3);if(_0x558cb7&&this[_0x24426a(0xaa)]($dataStates[_0x41c1e3])){this[_0x24426a(0x290)](_0x41c1e3);;}},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x26d)]=Game_Battler[_0x1bfe85(0x11c)][_0x1bfe85(0x1f4)],Game_Battler[_0x1bfe85(0x11c)]['isStateAddable']=function(_0x19f9bb){const _0x21c8d4=_0x1bfe85,_0x5e453a=$dataStates[_0x19f9bb];if(_0x5e453a&&_0x5e453a[_0x21c8d4(0x161)][_0x21c8d4(0x298)](/<NO DEATH CLEAR>/i))return!this[_0x21c8d4(0x24e)](_0x19f9bb)&&!this['isStateRestrict'](_0x19f9bb)&&!this[_0x21c8d4(0x192)][_0x21c8d4(0x227)](_0x19f9bb);return VisuMZ[_0x21c8d4(0x1f6)]['Game_Battler_isStateAddable']['call'](this,_0x19f9bb);},Game_Battler['prototype']['onAddState']=function(_0x56e70c){const _0x3e72be=_0x1bfe85;this[_0x3e72be(0x180)](_0x56e70c),this[_0x3e72be(0x14d)](_0x56e70c),this['onAddStateCustomJS'](_0x56e70c),this[_0x3e72be(0x285)](_0x56e70c);},Game_Battler[_0x1bfe85(0x11c)][_0x1bfe85(0x274)]=function(_0x5550ea){const _0xe556e=_0x1bfe85;Game_BattlerBase[_0xe556e(0x11c)][_0xe556e(0x274)][_0xe556e(0x174)](this,_0x5550ea),this[_0xe556e(0x297)](_0x5550ea),this['onEraseStateGlobalJS'](_0x5550ea);},Game_Battler[_0x1bfe85(0x11c)][_0x1bfe85(0xfc)]=function(_0x1443e2){const _0x1d3a32=_0x1bfe85;for(const _0x35cdc2 of this[_0x1d3a32(0x1ff)]()){this['isStateExpired'](_0x35cdc2['id'])&&_0x35cdc2['autoRemovalTiming']===_0x1443e2&&(this[_0x1d3a32(0xb2)](_0x35cdc2['id']),this[_0x1d3a32(0x288)](_0x35cdc2['id']),this[_0x1d3a32(0x89)](_0x35cdc2['id']));}},Game_Battler[_0x1bfe85(0x11c)][_0x1bfe85(0x288)]=function(_0x28ee60){const _0x4191f6=_0x1bfe85;this[_0x4191f6(0x29c)](_0x28ee60);},Game_Battler['prototype'][_0x1bfe85(0xf6)]=function(_0xbc110b){const _0x1e63d9=_0x1bfe85,_0x2fe31b=VisuMZ[_0x1e63d9(0x1f6)][_0x1e63d9(0xa0)];if(_0x2fe31b[_0xbc110b])_0x2fe31b[_0xbc110b][_0x1e63d9(0x174)](this,_0xbc110b);},Game_Battler['prototype'][_0x1bfe85(0x297)]=function(_0x594d8e){const _0x27b6d4=_0x1bfe85,_0xb62cc0=VisuMZ[_0x27b6d4(0x1f6)][_0x27b6d4(0x11a)];if(_0xb62cc0[_0x594d8e])_0xb62cc0[_0x594d8e][_0x27b6d4(0x174)](this,_0x594d8e);},Game_Battler['prototype'][_0x1bfe85(0x29c)]=function(_0x1921fb){const _0x582ad7=_0x1bfe85,_0x35e008=VisuMZ[_0x582ad7(0x1f6)][_0x582ad7(0x197)];if(_0x35e008[_0x1921fb])_0x35e008[_0x1921fb][_0x582ad7(0x174)](this,_0x1921fb);},Game_Battler['prototype'][_0x1bfe85(0x285)]=function(_0x303d6e){const _0x5823c1=_0x1bfe85;try{VisuMZ[_0x5823c1(0x1f6)]['Settings'][_0x5823c1(0x95)][_0x5823c1(0xb0)][_0x5823c1(0x174)](this,_0x303d6e);}catch(_0x503305){if($gameTemp[_0x5823c1(0x21c)]())console[_0x5823c1(0x228)](_0x503305);}},Game_Battler['prototype'][_0x1bfe85(0x1bc)]=function(_0x29ad87){const _0x124ced=_0x1bfe85;try{VisuMZ['SkillsStatesCore'][_0x124ced(0xdb)][_0x124ced(0x95)][_0x124ced(0x234)][_0x124ced(0x174)](this,_0x29ad87);}catch(_0x44f1e6){if($gameTemp[_0x124ced(0x21c)]())console[_0x124ced(0x228)](_0x44f1e6);}},Game_Battler['prototype'][_0x1bfe85(0x89)]=function(_0xc09ab1){const _0x4e89c4=_0x1bfe85;try{VisuMZ[_0x4e89c4(0x1f6)][_0x4e89c4(0xdb)]['States']['onExpireStateJS'][_0x4e89c4(0x174)](this,_0xc09ab1);}catch(_0x270411){if($gameTemp[_0x4e89c4(0x21c)]())console[_0x4e89c4(0x228)](_0x270411);}},Game_Battler[_0x1bfe85(0x11c)]['statesByCategory']=function(_0x48d3eb){const _0x319ba6=_0x1bfe85;return _0x48d3eb=_0x48d3eb[_0x319ba6(0x10b)]()['trim'](),this[_0x319ba6(0x1ff)]()[_0x319ba6(0xfb)](_0x33f522=>_0x33f522[_0x319ba6(0x280)][_0x319ba6(0xa6)](_0x48d3eb));},Game_Battler[_0x1bfe85(0x11c)][_0x1bfe85(0x26f)]=function(_0x298816,_0x3db243){const _0x24c200=_0x1bfe85;_0x298816=_0x298816[_0x24c200(0x10b)]()[_0x24c200(0x151)](),_0x3db243=_0x3db243||0x0;const _0x50cfa9=this[_0x24c200(0xa8)](_0x298816),_0xf8b053=[];for(const _0x13a4ac of _0x50cfa9){if(!_0x13a4ac)continue;if(_0x3db243<=0x0)return;_0xf8b053['push'](_0x13a4ac['id']),this[_0x24c200(0x192)][_0x24c200(0x299)]=!![],_0x3db243--;}while(_0xf8b053[_0x24c200(0x97)]>0x0){this[_0x24c200(0xb2)](_0xf8b053[_0x24c200(0x1c6)]());}},Game_Battler[_0x1bfe85(0x11c)]['removeStatesByCategoryAll']=function(_0x1c52ba){const _0x5c5ba0=_0x1bfe85;_0x1c52ba=_0x1c52ba[_0x5c5ba0(0x10b)]()[_0x5c5ba0(0x151)]();const _0x3ab118=this[_0x5c5ba0(0xa8)](_0x1c52ba),_0x53383b=[];for(const _0x4e72a8 of _0x3ab118){if(!_0x4e72a8)continue;_0x53383b[_0x5c5ba0(0x7a)](_0x4e72a8['id']),this['_result'][_0x5c5ba0(0x299)]=!![];}while(_0x53383b[_0x5c5ba0(0x97)]>0x0){this[_0x5c5ba0(0xb2)](_0x53383b[_0x5c5ba0(0x1c6)]());}},Game_Battler[_0x1bfe85(0x11c)][_0x1bfe85(0xbe)]=function(_0x348a0d){const _0x577a02=_0x1bfe85;return this[_0x577a02(0x257)](_0x348a0d)>0x0;},Game_Battler[_0x1bfe85(0x11c)][_0x1bfe85(0x10e)]=function(_0x1780e4){const _0x1e433f=_0x1bfe85;return this[_0x1e433f(0x103)](_0x1780e4)>0x0;},Game_Battler[_0x1bfe85(0x11c)][_0x1bfe85(0x257)]=function(_0x44947f){const _0x15a605=_0x1bfe85,_0x407b30=this[_0x15a605(0xa8)](_0x44947f)[_0x15a605(0xfb)](_0x15998e=>this['isStateAffected'](_0x15998e['id']));return _0x407b30[_0x15a605(0x97)];},Game_Battler[_0x1bfe85(0x11c)][_0x1bfe85(0x103)]=function(_0x51e391){const _0x22bb08=_0x1bfe85,_0x12093b=this[_0x22bb08(0xa8)](_0x51e391);return _0x12093b['length'];},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x23b)]=Game_Battler[_0x1bfe85(0x11c)][_0x1bfe85(0x26b)],Game_Battler[_0x1bfe85(0x11c)][_0x1bfe85(0x26b)]=function(_0x27c61c,_0x4bae57){const _0x2b8738=_0x1bfe85;VisuMZ[_0x2b8738(0x1f6)][_0x2b8738(0x23b)][_0x2b8738(0x174)](this,_0x27c61c,_0x4bae57),this[_0x2b8738(0x294)](_0x27c61c)&&this[_0x2b8738(0x13a)](_0x27c61c,_0x4bae57);},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x24a)]=Game_Battler['prototype'][_0x1bfe85(0xf5)],Game_Battler['prototype'][_0x1bfe85(0xf5)]=function(_0x1fb99e,_0x1b84b4){const _0x315e25=_0x1bfe85;VisuMZ['SkillsStatesCore']['Game_Battler_addDebuff'][_0x315e25(0x174)](this,_0x1fb99e,_0x1b84b4),this['isDebuffAffected'](_0x1fb99e)&&this[_0x315e25(0x1f9)](_0x1fb99e,_0x1b84b4);},Game_Battler[_0x1bfe85(0x11c)][_0x1bfe85(0x1d9)]=function(){const _0x4efe20=_0x1bfe85;for(let _0x232f1a=0x0;_0x232f1a<this['buffLength']();_0x232f1a++){if(this[_0x4efe20(0x187)](_0x232f1a)){const _0x43a6e0=this[_0x4efe20(0xc2)][_0x232f1a];this[_0x4efe20(0x11b)](_0x232f1a);if(_0x43a6e0>0x0)this[_0x4efe20(0x1bf)](_0x232f1a);if(_0x43a6e0<0x0)this[_0x4efe20(0x18b)](_0x232f1a);}}},Game_Battler[_0x1bfe85(0x11c)][_0x1bfe85(0x13a)]=function(_0x4b8376,_0x633f7){this['onAddBuffGlobalJS'](_0x4b8376,_0x633f7);},Game_Battler[_0x1bfe85(0x11c)][_0x1bfe85(0x1f9)]=function(_0x3a773a,_0x5e3842){const _0x2e99e4=_0x1bfe85;this[_0x2e99e4(0x17b)](_0x3a773a,_0x5e3842);},Game_Battler[_0x1bfe85(0x11c)]['onEraseBuff']=function(_0x173c82){const _0x5bc791=_0x1bfe85;Game_BattlerBase['prototype'][_0x5bc791(0x186)][_0x5bc791(0x174)](this,_0x173c82),this[_0x5bc791(0x1a9)](_0x173c82);},Game_Battler[_0x1bfe85(0x11c)]['onEraseDebuff']=function(_0x1f3ae4){const _0x52a11e=_0x1bfe85;Game_BattlerBase[_0x52a11e(0x11c)]['onEraseDebuff'][_0x52a11e(0x174)](this,_0x1f3ae4),this[_0x52a11e(0x7b)](_0x1f3ae4);},Game_Battler[_0x1bfe85(0x11c)][_0x1bfe85(0x1bf)]=function(_0x34ff90){const _0x466f51=_0x1bfe85;this[_0x466f51(0x1d5)](_0x34ff90);},Game_Battler['prototype']['onExpireDebuff']=function(_0x30494f){this['onExpireDebuffGlobalJS'](_0x30494f);},Game_Battler[_0x1bfe85(0x11c)][_0x1bfe85(0xf1)]=function(_0x156e7d,_0xe90ffd){const _0x5f360d=_0x1bfe85;VisuMZ[_0x5f360d(0x1f6)][_0x5f360d(0xdb)]['Buffs'][_0x5f360d(0x15a)]['call'](this,_0x156e7d,_0xe90ffd);},Game_Battler[_0x1bfe85(0x11c)][_0x1bfe85(0x17b)]=function(_0x3e6039,_0x31027d){const _0x21e34b=_0x1bfe85;VisuMZ[_0x21e34b(0x1f6)][_0x21e34b(0xdb)]['Buffs'][_0x21e34b(0x1fa)][_0x21e34b(0x174)](this,_0x3e6039,_0x31027d);},Game_BattlerBase[_0x1bfe85(0x11c)]['onEraseBuffGlobalJS']=function(_0xe0ebc7){const _0x41f69a=_0x1bfe85;VisuMZ[_0x41f69a(0x1f6)][_0x41f69a(0xdb)][_0x41f69a(0x254)][_0x41f69a(0xcf)][_0x41f69a(0x174)](this,_0xe0ebc7);},Game_BattlerBase[_0x1bfe85(0x11c)][_0x1bfe85(0x7b)]=function(_0x4f8586){const _0x420c37=_0x1bfe85;VisuMZ['SkillsStatesCore'][_0x420c37(0xdb)][_0x420c37(0x254)][_0x420c37(0x153)]['call'](this,_0x4f8586);},Game_Battler[_0x1bfe85(0x11c)]['onExpireBuffGlobalJS']=function(_0x4f1040){const _0x409349=_0x1bfe85;VisuMZ[_0x409349(0x1f6)][_0x409349(0xdb)]['Buffs']['onExpireBuffJS'][_0x409349(0x174)](this,_0x4f1040);},Game_Battler['prototype'][_0x1bfe85(0x134)]=function(_0x37146d){const _0x3803b3=_0x1bfe85;VisuMZ['SkillsStatesCore']['Settings'][_0x3803b3(0x254)][_0x3803b3(0xe3)][_0x3803b3(0x174)](this,_0x37146d);},Game_Battler[_0x1bfe85(0x11c)][_0x1bfe85(0x14d)]=function(_0x2418a2){const _0x30fc0a=_0x1bfe85,_0x2aaf40=VisuMZ[_0x30fc0a(0x1f6)],_0x4ea035=['stateHpSlipDamageJS','stateHpSlipHealJS','stateMpSlipDamageJS',_0x30fc0a(0xec),_0x30fc0a(0xc7),'stateTpSlipHealJS'];for(const _0x3fa0c7 of _0x4ea035){_0x2aaf40[_0x3fa0c7][_0x2418a2]&&_0x2aaf40[_0x3fa0c7][_0x2418a2][_0x30fc0a(0x174)](this,_0x2418a2);}},VisuMZ['SkillsStatesCore'][_0x1bfe85(0x1aa)]=Game_Battler[_0x1bfe85(0x11c)][_0x1bfe85(0x169)],Game_Battler[_0x1bfe85(0x11c)][_0x1bfe85(0x169)]=function(){const _0x56c813=_0x1bfe85;VisuMZ[_0x56c813(0x1f6)][_0x56c813(0x1aa)][_0x56c813(0x174)](this),this[_0x56c813(0x12e)](),this[_0x56c813(0x139)]();},Game_Battler[_0x1bfe85(0x11c)][_0x1bfe85(0x12e)]=function(){const _0x329029=_0x1bfe85;for(const _0x4fd9c4 of this[_0x329029(0x131)]()){if(!_0x4fd9c4)continue;this['onAddStateMakeCustomSlipValues'](_0x4fd9c4['id']);}},Game_Battler[_0x1bfe85(0x11c)][_0x1bfe85(0x139)]=function(){const _0x3adb3b=_0x1bfe85;if(!this[_0x3adb3b(0x289)]())return;const _0xd0003=this[_0x3adb3b(0x1ff)]();for(const _0x47ebde of _0xd0003){if(!_0x47ebde)continue;this[_0x3adb3b(0x185)](_0x47ebde);}},Game_Battler['prototype'][_0x1bfe85(0x185)]=function(_0x4c436a){const _0x4b8fcd=_0x1bfe85,_0x142b41=this['getStateData'](_0x4c436a['id'],'slipHp')||0x0,_0x4a3cb2=-this[_0x4b8fcd(0xe0)](),_0x44dd5d=Math[_0x4b8fcd(0xf2)](_0x142b41,_0x4a3cb2);if(_0x44dd5d!==0x0)this[_0x4b8fcd(0x25e)](_0x44dd5d);const _0x8631ad=this[_0x4b8fcd(0x155)](_0x4c436a['id'],_0x4b8fcd(0x1fc))||0x0;if(_0x8631ad!==0x0)this['gainMp'](_0x8631ad);const _0x2e6d1b=this[_0x4b8fcd(0x155)](_0x4c436a['id'],_0x4b8fcd(0x264))||0x0;if(_0x2e6d1b!==0x0)this[_0x4b8fcd(0x239)](_0x2e6d1b);},VisuMZ['SkillsStatesCore'][_0x1bfe85(0x29d)]=Game_Actor['prototype']['skillTypes'],Game_Actor['prototype'][_0x1bfe85(0x27c)]=function(){const _0x34d473=_0x1bfe85,_0x5001c9=VisuMZ[_0x34d473(0x1f6)]['Game_Actor_skillTypes'][_0x34d473(0x174)](this),_0x152167=VisuMZ[_0x34d473(0x1f6)]['Settings'][_0x34d473(0x8c)];let _0x17afdd=_0x152167['HiddenSkillTypes'];return $gameParty[_0x34d473(0x130)]()&&(_0x17afdd=_0x17afdd['concat'](_0x152167[_0x34d473(0x168)])),_0x5001c9[_0x34d473(0xfb)](_0x1d24ac=>!_0x17afdd['includes'](_0x1d24ac));},Game_Actor[_0x1bfe85(0x11c)][_0x1bfe85(0xac)]=function(){const _0x587413=_0x1bfe85;return this[_0x587413(0x208)]()['filter'](_0x3e8a9e=>this[_0x587413(0x77)](_0x3e8a9e));},Game_Actor[_0x1bfe85(0x11c)][_0x1bfe85(0x77)]=function(_0x38e3cb){const _0x1fc2b2=_0x1bfe85;if(!this['canUse'](_0x38e3cb))return![];const _0x164d36=this[_0x1fc2b2(0x27c)](),_0x2e09f9=DataManager[_0x1fc2b2(0x2a2)](_0x38e3cb),_0x26a95d=_0x164d36[_0x1fc2b2(0xfb)](_0x1bacc8=>_0x2e09f9[_0x1fc2b2(0xa6)](_0x1bacc8));return _0x26a95d[_0x1fc2b2(0x97)]>0x0;},Game_Actor['prototype'][_0x1bfe85(0x22a)]=function(){const _0x56e5d5=_0x1bfe85;let _0x2c95d5=[this[_0x56e5d5(0x163)](),this[_0x56e5d5(0x235)]()];_0x2c95d5=_0x2c95d5['concat'](this[_0x56e5d5(0x1d8)]()['filter'](_0x4c859f=>_0x4c859f));for(const _0x3bf1e9 of this[_0x56e5d5(0x1e5)]){const _0x272aae=$dataSkills[_0x3bf1e9];if(_0x272aae)_0x2c95d5[_0x56e5d5(0x7a)](_0x272aae);}return _0x2c95d5;},Game_Actor['prototype'][_0x1bfe85(0x1be)]=function(){const _0x511944=_0x1bfe85;Game_Battler[_0x511944(0x11c)]['addPassiveStatesByPluginParameters'][_0x511944(0x174)](this);const _0x4a234f=VisuMZ[_0x511944(0x1f6)][_0x511944(0xdb)]['PassiveStates'][_0x511944(0x1c8)];this[_0x511944(0x120)][_0x511944(0x131)]=this['_cache'][_0x511944(0x131)][_0x511944(0x74)](_0x4a234f);},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x20e)]=Game_Actor[_0x1bfe85(0x11c)][_0x1bfe85(0x1cd)],Game_Actor[_0x1bfe85(0x11c)][_0x1bfe85(0x1cd)]=function(_0x1f2e8a){const _0x5f4eb3=_0x1bfe85;VisuMZ[_0x5f4eb3(0x1f6)][_0x5f4eb3(0x20e)][_0x5f4eb3(0x174)](this,_0x1f2e8a),this['_cache']={};},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x25f)]=Game_Actor['prototype'][_0x1bfe85(0x207)],Game_Actor['prototype'][_0x1bfe85(0x207)]=function(_0x1951d3){const _0x47ec3a=_0x1bfe85;VisuMZ['SkillsStatesCore'][_0x47ec3a(0x25f)][_0x47ec3a(0x174)](this,_0x1951d3),this[_0x47ec3a(0x120)]={};},Game_Enemy['prototype'][_0x1bfe85(0x22a)]=function(){const _0x5196f0=_0x1bfe85;let _0x1580b4=[this[_0x5196f0(0x24b)]()];return _0x1580b4[_0x5196f0(0x74)](this[_0x5196f0(0x208)]());},Game_Enemy['prototype']['addPassiveStatesByPluginParameters']=function(){const _0x35a5dc=_0x1bfe85;Game_Battler['prototype'][_0x35a5dc(0x1be)]['call'](this);const _0x547dea=VisuMZ[_0x35a5dc(0x1f6)][_0x35a5dc(0xdb)]['PassiveStates'][_0x35a5dc(0x8a)];this[_0x35a5dc(0x120)][_0x35a5dc(0x131)]=this[_0x35a5dc(0x120)]['passiveStates']['concat'](_0x547dea);},Game_Enemy['prototype'][_0x1bfe85(0x208)]=function(){const _0x8a6ddd=_0x1bfe85,_0x548e15=[];for(const _0x34e12a of this['enemy']()[_0x8a6ddd(0xee)]){const _0x45899e=$dataSkills[_0x34e12a[_0x8a6ddd(0x116)]];if(_0x45899e&&!_0x548e15[_0x8a6ddd(0xa6)](_0x45899e))_0x548e15[_0x8a6ddd(0x7a)](_0x45899e);}return _0x548e15;},Game_Enemy['prototype'][_0x1bfe85(0x112)]=function(_0x3a6b2d){const _0x192ffe=_0x1bfe85;return this[_0x192ffe(0xaa)]($dataStates[_0x3a6b2d]);},VisuMZ[_0x1bfe85(0x1f6)]['Game_Unit_isAllDead']=Game_Unit[_0x1bfe85(0x11c)][_0x1bfe85(0x1ee)],Game_Unit[_0x1bfe85(0x11c)]['isAllDead']=function(){const _0x40afe1=_0x1bfe85;if(this[_0x40afe1(0x17c)]())return!![];return VisuMZ[_0x40afe1(0x1f6)][_0x40afe1(0x27e)][_0x40afe1(0x174)](this);},Game_Unit[_0x1bfe85(0x11c)][_0x1bfe85(0x17c)]=function(){const _0x5eedf5=_0x1bfe85,_0x53ef7e=this[_0x5eedf5(0x7d)]();for(const _0x1f78e4 of _0x53ef7e){if(!_0x1f78e4[_0x5eedf5(0xbc)]())return![];}return!![];},VisuMZ[_0x1bfe85(0x1f6)]['Game_Troop_setup']=Game_Troop['prototype'][_0x1bfe85(0x266)],Game_Troop[_0x1bfe85(0x11c)][_0x1bfe85(0x266)]=function(_0x230ff4){const _0x2148f6=_0x1bfe85;VisuMZ['SkillsStatesCore'][_0x2148f6(0x176)][_0x2148f6(0x174)](this,_0x230ff4),this[_0x2148f6(0x1cc)]();},Game_Troop[_0x1bfe85(0x11c)][_0x1bfe85(0x1cc)]=function(){const _0x324b12=_0x1bfe85;this[_0x324b12(0xd7)]=Graphics[_0x324b12(0x203)];},Game_Troop[_0x1bfe85(0x11c)][_0x1bfe85(0x160)]=function(){const _0x5396d8=_0x1bfe85;return this[_0x5396d8(0xd7)]=this[_0x5396d8(0xd7)]||Graphics['frameCount'],this[_0x5396d8(0xd7)];},Scene_Skill[_0x1bfe85(0x11c)]['isBottomHelpMode']=function(){const _0xb44479=_0x1bfe85;if(ConfigManager[_0xb44479(0x27b)]&&ConfigManager[_0xb44479(0xa4)]!==undefined)return ConfigManager[_0xb44479(0xa4)];else{if(this['isUseSkillsStatesCoreUpdatedLayout']())return this[_0xb44479(0x2a1)]()['match'](/LOWER/i);else Scene_ItemBase[_0xb44479(0x11c)][_0xb44479(0x284)][_0xb44479(0x174)](this);}},Scene_Skill[_0x1bfe85(0x11c)][_0x1bfe85(0x284)]=function(){const _0x6dfc14=_0x1bfe85;if(ConfigManager[_0x6dfc14(0x27b)]&&ConfigManager[_0x6dfc14(0x28f)]!==undefined)return ConfigManager[_0x6dfc14(0x28f)];else return this[_0x6dfc14(0xa2)]()?this['updatedLayoutStyle']()[_0x6dfc14(0x298)](/RIGHT/i):Scene_ItemBase[_0x6dfc14(0x11c)]['isRightInputMode'][_0x6dfc14(0x174)](this);},Scene_Skill[_0x1bfe85(0x11c)][_0x1bfe85(0x2a1)]=function(){const _0x32077e=_0x1bfe85;return VisuMZ['SkillsStatesCore'][_0x32077e(0xdb)]['Skills'][_0x32077e(0x1b7)];},Scene_Skill['prototype'][_0x1bfe85(0x214)]=function(){const _0x5751c4=_0x1bfe85;return this['_categoryWindow']&&this[_0x5751c4(0x271)][_0x5751c4(0x214)]();},Scene_Skill[_0x1bfe85(0x11c)][_0x1bfe85(0xa2)]=function(){const _0x35fefe=_0x1bfe85;return VisuMZ[_0x35fefe(0x1f6)][_0x35fefe(0xdb)][_0x35fefe(0x8c)][_0x35fefe(0x181)];},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x258)]=Scene_Skill[_0x1bfe85(0x11c)][_0x1bfe85(0x12d)],Scene_Skill[_0x1bfe85(0x11c)][_0x1bfe85(0x12d)]=function(){const _0x2db183=_0x1bfe85;return this[_0x2db183(0xa2)]()?this[_0x2db183(0x276)]():VisuMZ[_0x2db183(0x1f6)][_0x2db183(0x258)][_0x2db183(0x174)](this);},Scene_Skill[_0x1bfe85(0x11c)][_0x1bfe85(0x276)]=function(){const _0x4d7092=_0x1bfe85,_0x4335d0=0x0,_0x46fb95=this['helpAreaTop'](),_0x2fde67=Graphics[_0x4d7092(0xce)],_0x23ad4c=this[_0x4d7092(0x83)]();return new Rectangle(_0x4335d0,_0x46fb95,_0x2fde67,_0x23ad4c);},VisuMZ['SkillsStatesCore'][_0x1bfe85(0xa9)]=Scene_Skill[_0x1bfe85(0x11c)]['skillTypeWindowRect'],Scene_Skill[_0x1bfe85(0x11c)]['skillTypeWindowRect']=function(){const _0x14c20c=_0x1bfe85;return this['isUseSkillsStatesCoreUpdatedLayout']()?this[_0x14c20c(0x7c)]():VisuMZ['SkillsStatesCore']['Scene_Skill_skillTypeWindowRect'][_0x14c20c(0x174)](this);},Scene_Skill[_0x1bfe85(0x11c)]['skillTypeWindowRectSkillsStatesCore']=function(){const _0x54ee89=_0x1bfe85,_0x9a77ec=this[_0x54ee89(0x8d)](),_0x2ee1c1=this[_0x54ee89(0xb8)](0x3,!![]),_0x3e3371=this[_0x54ee89(0x284)]()?Graphics['boxWidth']-_0x9a77ec:0x0,_0x38c030=this[_0x54ee89(0x1a0)]();return new Rectangle(_0x3e3371,_0x38c030,_0x9a77ec,_0x2ee1c1);},VisuMZ['SkillsStatesCore'][_0x1bfe85(0xc3)]=Scene_Skill['prototype'][_0x1bfe85(0x16b)],Scene_Skill[_0x1bfe85(0x11c)][_0x1bfe85(0x16b)]=function(){const _0x380520=_0x1bfe85;return this[_0x380520(0xa2)]()?this['statusWindowRectSkillsStatesCore']():VisuMZ[_0x380520(0x1f6)][_0x380520(0xc3)][_0x380520(0x174)](this);},Scene_Skill[_0x1bfe85(0x11c)]['statusWindowRectSkillsStatesCore']=function(){const _0x5a9c82=_0x1bfe85,_0x31c345=Graphics[_0x5a9c82(0xce)]-this['mainCommandWidth'](),_0x509ab1=this['_skillTypeWindow']['height'],_0x1fe958=this[_0x5a9c82(0x284)]()?0x0:Graphics[_0x5a9c82(0xce)]-_0x31c345,_0x3e59a1=this[_0x5a9c82(0x1a0)]();return new Rectangle(_0x1fe958,_0x3e59a1,_0x31c345,_0x509ab1);},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0xda)]=Scene_Skill[_0x1bfe85(0x11c)][_0x1bfe85(0x1c9)],Scene_Skill[_0x1bfe85(0x11c)][_0x1bfe85(0x1c9)]=function(){const _0x31466f=_0x1bfe85;VisuMZ['SkillsStatesCore'][_0x31466f(0xda)][_0x31466f(0x174)](this),this[_0x31466f(0x193)]()&&this[_0x31466f(0x71)]();},VisuMZ['SkillsStatesCore']['Scene_Skill_itemWindowRect']=Scene_Skill[_0x1bfe85(0x11c)][_0x1bfe85(0x29f)],Scene_Skill[_0x1bfe85(0x11c)][_0x1bfe85(0x29f)]=function(){const _0x4c6786=_0x1bfe85;if(this[_0x4c6786(0xa2)]())return this[_0x4c6786(0x212)]();else{const _0x4ec659=VisuMZ[_0x4c6786(0x1f6)]['Scene_Skill_itemWindowRect'][_0x4c6786(0x174)](this);return this['allowCreateShopStatusWindow']()&&this[_0x4c6786(0x149)]()&&(_0x4ec659[_0x4c6786(0x159)]-=this[_0x4c6786(0x283)]()),_0x4ec659;}},Scene_Skill['prototype']['itemWindowRectSkillsStatesCore']=function(){const _0xb38813=_0x1bfe85,_0x527201=Graphics['boxWidth']-this[_0xb38813(0x283)](),_0x2f00ef=this[_0xb38813(0x179)]()-this[_0xb38813(0x1ba)][_0xb38813(0xc8)],_0x2f464b=this[_0xb38813(0x284)]()?Graphics[_0xb38813(0xce)]-_0x527201:0x0,_0x174474=this['_statusWindow']['y']+this['_statusWindow'][_0xb38813(0xc8)];return new Rectangle(_0x2f464b,_0x174474,_0x527201,_0x2f00ef);},Scene_Skill[_0x1bfe85(0x11c)][_0x1bfe85(0x193)]=function(){const _0x3006b8=_0x1bfe85;if(!Imported['VisuMZ_1_ItemsEquipsCore'])return![];else return this[_0x3006b8(0xa2)]()?!![]:VisuMZ[_0x3006b8(0x1f6)]['Settings'][_0x3006b8(0x8c)][_0x3006b8(0x104)];},Scene_Skill[_0x1bfe85(0x11c)]['adjustItemWidthByShopStatus']=function(){const _0x22c325=_0x1bfe85;return VisuMZ[_0x22c325(0x1f6)][_0x22c325(0xdb)][_0x22c325(0x8c)][_0x22c325(0x183)];},Scene_Skill[_0x1bfe85(0x11c)][_0x1bfe85(0x71)]=function(){const _0xad19b4=_0x1bfe85,_0x3a4fb0=this[_0xad19b4(0xef)]();this[_0xad19b4(0x215)]=new Window_ShopStatus(_0x3a4fb0),this[_0xad19b4(0x250)](this[_0xad19b4(0x215)]),this['_itemWindow'][_0xad19b4(0x6d)](this[_0xad19b4(0x215)]);const _0x21ba7b=VisuMZ[_0xad19b4(0x1f6)]['Settings'][_0xad19b4(0x8c)][_0xad19b4(0x121)];this['_shopStatusWindow'][_0xad19b4(0x90)](_0x21ba7b||0x0);},Scene_Skill[_0x1bfe85(0x11c)][_0x1bfe85(0xef)]=function(){const _0x9a1b90=_0x1bfe85;return this['isUseSkillsStatesCoreUpdatedLayout']()?this[_0x9a1b90(0x109)]():VisuMZ[_0x9a1b90(0x1f6)][_0x9a1b90(0xdb)][_0x9a1b90(0x8c)]['SkillMenuStatusRect'][_0x9a1b90(0x174)](this);},Scene_Skill[_0x1bfe85(0x11c)][_0x1bfe85(0x109)]=function(){const _0x1ba5e7=_0x1bfe85,_0x3483ae=this[_0x1ba5e7(0x283)](),_0x47d51e=this[_0x1ba5e7(0x277)][_0x1ba5e7(0xc8)],_0x58839d=this['isRightInputMode']()?0x0:Graphics['boxWidth']-this[_0x1ba5e7(0x283)](),_0x407479=this[_0x1ba5e7(0x277)]['y'];return new Rectangle(_0x58839d,_0x407479,_0x3483ae,_0x47d51e);},Scene_Skill[_0x1bfe85(0x11c)]['shopStatusWidth']=function(){const _0x5b7354=_0x1bfe85;return Imported[_0x5b7354(0xcb)]?Scene_Shop[_0x5b7354(0x11c)]['statusWidth']():0x0;},Scene_Skill[_0x1bfe85(0x11c)][_0x1bfe85(0xf4)]=function(){const _0x49584e=_0x1bfe85;return this[_0x49584e(0x1ed)]&&this[_0x49584e(0x1ed)][_0x49584e(0x293)]?TextManager[_0x49584e(0x18f)]:'';},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x23d)]=Sprite_Gauge['prototype'][_0x1bfe85(0xb1)],Sprite_Gauge['prototype'][_0x1bfe85(0xb1)]=function(){const _0x30c501=_0x1bfe85;VisuMZ['SkillsStatesCore'][_0x30c501(0x23d)]['call'](this),this[_0x30c501(0x25c)]=null;},VisuMZ['SkillsStatesCore'][_0x1bfe85(0xad)]=Sprite_Gauge[_0x1bfe85(0x11c)][_0x1bfe85(0x266)],Sprite_Gauge[_0x1bfe85(0x11c)]['setup']=function(_0x5ddf5f,_0x3db587){const _0xca1854=_0x1bfe85;this[_0xca1854(0x2a3)](_0x5ddf5f,_0x3db587),_0x3db587=_0x3db587[_0xca1854(0x15e)](),VisuMZ[_0xca1854(0x1f6)][_0xca1854(0xad)][_0xca1854(0x174)](this,_0x5ddf5f,_0x3db587);},Sprite_Gauge[_0x1bfe85(0x11c)][_0x1bfe85(0x2a3)]=function(_0x48e716,_0x7881d0){const _0x27d8fc=_0x1bfe85,_0x271dbf=VisuMZ['SkillsStatesCore'][_0x27d8fc(0xdb)][_0x27d8fc(0x20d)]['filter'](_0x1304b=>_0x1304b[_0x27d8fc(0xfd)][_0x27d8fc(0x10b)]()===_0x7881d0[_0x27d8fc(0x10b)]());_0x271dbf[_0x27d8fc(0x97)]>=0x1?this[_0x27d8fc(0x25c)]=_0x271dbf[0x0]:this['_costSettings']=null;},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x1ec)]=Sprite_Gauge[_0x1bfe85(0x11c)]['currentValue'],Sprite_Gauge[_0x1bfe85(0x11c)][_0x1bfe85(0x1a8)]=function(){const _0xe97f6f=_0x1bfe85;return this[_0xe97f6f(0x126)]&&this[_0xe97f6f(0x25c)]?this[_0xe97f6f(0x9c)]():VisuMZ[_0xe97f6f(0x1f6)][_0xe97f6f(0x1ec)][_0xe97f6f(0x174)](this);},Sprite_Gauge['prototype'][_0x1bfe85(0x9c)]=function(){const _0x568b1e=_0x1bfe85;return this[_0x568b1e(0x25c)][_0x568b1e(0x1de)][_0x568b1e(0x174)](this[_0x568b1e(0x126)]);},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x162)]=Sprite_Gauge[_0x1bfe85(0x11c)]['currentMaxValue'],Sprite_Gauge[_0x1bfe85(0x11c)][_0x1bfe85(0xe8)]=function(){const _0x1723f1=_0x1bfe85;return this[_0x1723f1(0x126)]&&this[_0x1723f1(0x25c)]?this['currentMaxValueSkillsStatesCore']():VisuMZ['SkillsStatesCore'][_0x1723f1(0x162)]['call'](this);},Sprite_Gauge[_0x1bfe85(0x11c)][_0x1bfe85(0xd4)]=function(){const _0x10e83e=_0x1bfe85;return this['_costSettings']['GaugeMaxJS'][_0x10e83e(0x174)](this[_0x10e83e(0x126)]);},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x1ca)]=Sprite_Gauge[_0x1bfe85(0x11c)][_0x1bfe85(0x164)],Sprite_Gauge[_0x1bfe85(0x11c)][_0x1bfe85(0x164)]=function(){const _0x1a10ce=_0x1bfe85,_0x59e849=VisuMZ[_0x1a10ce(0x1f6)][_0x1a10ce(0x1ca)][_0x1a10ce(0x174)](this);return _0x59e849[_0x1a10ce(0x7f)](0x0,0x1);},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x216)]=Sprite_Gauge['prototype'][_0x1bfe85(0x1c7)],Sprite_Gauge[_0x1bfe85(0x11c)]['redraw']=function(){const _0x7007e2=_0x1bfe85;this[_0x7007e2(0x126)]&&this[_0x7007e2(0x25c)]?(this[_0x7007e2(0x1eb)][_0x7007e2(0x244)](),this['redrawSkillsStatesCore']()):VisuMZ[_0x7007e2(0x1f6)]['Sprite_Gauge_redraw'][_0x7007e2(0x174)](this);},Sprite_Gauge['prototype'][_0x1bfe85(0x27d)]=function(){const _0x2d9f24=_0x1bfe85;let _0x2242df=this[_0x2d9f24(0x1a8)]();return Imported[_0x2d9f24(0x177)]&&this[_0x2d9f24(0x1a2)]()&&(_0x2242df=VisuMZ['GroupDigits'](_0x2242df)),_0x2242df;},Sprite_Gauge['prototype'][_0x1bfe85(0x158)]=function(){const _0x391ab7=_0x1bfe85;this[_0x391ab7(0x25c)][_0x391ab7(0x1ac)][_0x391ab7(0x174)](this);},Sprite_Gauge[_0x1bfe85(0x11c)][_0x1bfe85(0x19d)]=function(_0x413260,_0x5ea621,_0xcc9ca4,_0x4ca6e1,_0xd3f9f4,_0x281211){const _0x261544=_0x1bfe85,_0x3fb69d=this[_0x261544(0x164)](),_0x313baa=Math['floor']((_0xd3f9f4-0x2)*_0x3fb69d),_0x2c0ed6=_0x281211-0x2,_0x17dabf=this[_0x261544(0x21a)]();this['bitmap'][_0x261544(0x219)](_0xcc9ca4,_0x4ca6e1,_0xd3f9f4,_0x281211,_0x17dabf),this['bitmap']['gradientFillRect'](_0xcc9ca4+0x1,_0x4ca6e1+0x1,_0x313baa,_0x2c0ed6,_0x413260,_0x5ea621);},VisuMZ['SkillsStatesCore'][_0x1bfe85(0x10a)]=Sprite_StateIcon[_0x1bfe85(0x11c)][_0x1bfe85(0x133)],Sprite_StateIcon['prototype'][_0x1bfe85(0x133)]=function(){const _0x3251a5=_0x1bfe85;VisuMZ['SkillsStatesCore'][_0x3251a5(0x10a)][_0x3251a5(0x174)](this),this[_0x3251a5(0x1f0)]();},Sprite_StateIcon[_0x1bfe85(0x11c)]['createTurnDisplaySprite']=function(){const _0x2cbf9b=_0x1bfe85,_0x25c458=Window_Base[_0x2cbf9b(0x11c)][_0x2cbf9b(0x20a)]();this[_0x2cbf9b(0x256)]=new Sprite(),this[_0x2cbf9b(0x256)][_0x2cbf9b(0x1eb)]=new Bitmap(ImageManager[_0x2cbf9b(0x18e)],_0x25c458),this[_0x2cbf9b(0x256)][_0x2cbf9b(0x1db)]['x']=this[_0x2cbf9b(0x1db)]['x'],this[_0x2cbf9b(0x256)][_0x2cbf9b(0x1db)]['y']=this[_0x2cbf9b(0x1db)]['y'],this[_0x2cbf9b(0x25b)](this[_0x2cbf9b(0x256)]),this['contents']=this[_0x2cbf9b(0x256)][_0x2cbf9b(0x1eb)];},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x21e)]=Sprite_StateIcon['prototype'][_0x1bfe85(0x18d)],Sprite_StateIcon[_0x1bfe85(0x11c)][_0x1bfe85(0x18d)]=function(){const _0x2bd461=_0x1bfe85;VisuMZ[_0x2bd461(0x1f6)][_0x2bd461(0x21e)][_0x2bd461(0x174)](this),this[_0x2bd461(0xea)]();},Sprite_StateIcon[_0x1bfe85(0x11c)][_0x1bfe85(0x1fd)]=function(_0x5a93d0,_0x11c669,_0x1de220,_0x4c9bd9,_0x3d21bb){const _0x570e51=_0x1bfe85;this[_0x570e51(0xd2)][_0x570e51(0x1fd)](_0x5a93d0,_0x11c669,_0x1de220,_0x4c9bd9,this[_0x570e51(0xd2)]['height'],_0x3d21bb);},Sprite_StateIcon[_0x1bfe85(0x11c)][_0x1bfe85(0xea)]=function(){const _0x42a000=_0x1bfe85;this['resetFontSettings'](),this[_0x42a000(0xd2)]['clear']();const _0x53c3ec=this[_0x42a000(0x126)];if(!_0x53c3ec)return;const _0x33a2f0=_0x53c3ec[_0x42a000(0x1ff)]()[_0x42a000(0xfb)](_0x129ff7=>_0x129ff7[_0x42a000(0x100)]>0x0),_0x4c6ab0=[...Array(0x8)[_0x42a000(0x1f3)]()]['filter'](_0x5383a0=>_0x53c3ec['buff'](_0x5383a0)!==0x0),_0x5918e2=this[_0x42a000(0x1f1)],_0x10baea=_0x33a2f0[_0x5918e2];if(_0x10baea)Window_Base[_0x42a000(0x11c)][_0x42a000(0x132)]['call'](this,_0x53c3ec,_0x10baea,0x0,0x0),Window_Base[_0x42a000(0x11c)][_0x42a000(0xd9)][_0x42a000(0x174)](this,_0x53c3ec,_0x10baea,0x0,0x0);else{const _0x5b017e=_0x4c6ab0[_0x5918e2-_0x33a2f0[_0x42a000(0x97)]];if(!_0x5b017e)return;Window_Base[_0x42a000(0x11c)][_0x42a000(0x142)]['call'](this,_0x53c3ec,_0x5b017e,0x0,0x0),Window_Base[_0x42a000(0x11c)]['drawActorBuffRates'][_0x42a000(0x174)](this,_0x53c3ec,_0x5b017e,0x0,0x0);}},Sprite_StateIcon['prototype'][_0x1bfe85(0x13c)]=function(){const _0x6f4fc1=_0x1bfe85;this[_0x6f4fc1(0xd2)][_0x6f4fc1(0x9a)]=$gameSystem[_0x6f4fc1(0x272)](),this[_0x6f4fc1(0xd2)][_0x6f4fc1(0x23a)]=$gameSystem[_0x6f4fc1(0x251)](),this[_0x6f4fc1(0x296)]();},Sprite_StateIcon[_0x1bfe85(0x11c)][_0x1bfe85(0x296)]=function(){const _0xc545d2=_0x1bfe85;this[_0xc545d2(0x19c)](ColorManager[_0xc545d2(0x1dd)]()),this[_0xc545d2(0x1e9)](ColorManager['outlineColor']());},Sprite_StateIcon[_0x1bfe85(0x11c)][_0x1bfe85(0x19c)]=function(_0x10b541){const _0x15a975=_0x1bfe85;this[_0x15a975(0xd2)][_0x15a975(0x20f)]=_0x10b541;},Sprite_StateIcon[_0x1bfe85(0x11c)]['changeOutlineColor']=function(_0x7742ef){const _0x197767=_0x1bfe85;this[_0x197767(0xd2)][_0x197767(0x195)]=_0x7742ef;},Window_Base['prototype']['drawSkillCost']=function(_0x4fe01f,_0x10f0a0,_0x33097a,_0x3e4dae,_0x4e4c61){const _0x29a50c=_0x1bfe85,_0x2d2682=this[_0x29a50c(0x170)](_0x4fe01f,_0x10f0a0),_0x55a374=this[_0x29a50c(0x13e)](_0x2d2682,_0x33097a,_0x3e4dae,_0x4e4c61),_0x4a0a8e=_0x33097a+_0x4e4c61-_0x55a374[_0x29a50c(0x159)];this['drawTextEx'](_0x2d2682,_0x4a0a8e,_0x3e4dae,_0x4e4c61),this[_0x29a50c(0x13c)]();},Window_Base[_0x1bfe85(0x11c)][_0x1bfe85(0x170)]=function(_0x538a26,_0x3b27b6){const _0x79eb5=_0x1bfe85;let _0x39c5fd='';for(settings of VisuMZ[_0x79eb5(0x1f6)][_0x79eb5(0xdb)][_0x79eb5(0x20d)]){if(!this[_0x79eb5(0x123)](_0x538a26,_0x3b27b6,settings))continue;if(_0x39c5fd[_0x79eb5(0x97)]>0x0)_0x39c5fd+=this['skillCostSeparator']();_0x39c5fd+=this[_0x79eb5(0x1bb)](_0x538a26,_0x3b27b6,settings);}_0x39c5fd=this['makeAdditionalSkillCostText'](_0x538a26,_0x3b27b6,_0x39c5fd);if(_0x3b27b6['note']['match'](/<CUSTOM COST TEXT>\s*([\s\S]*)\s*<\/CUSTOM COST TEXT>/i)){if(_0x39c5fd['length']>0x0)_0x39c5fd+=this[_0x79eb5(0x9f)]();_0x39c5fd+=String(RegExp['$1']);}return _0x39c5fd;},Window_Base[_0x1bfe85(0x11c)][_0x1bfe85(0x29b)]=function(_0xc5f489,_0x1bc7a7,_0x4edcdc){return _0x4edcdc;},Window_Base['prototype'][_0x1bfe85(0x123)]=function(_0x423f07,_0x4cfd99,_0x42533e){const _0x5ea624=_0x1bfe85,_0x53bafb=_0x42533e[_0x5ea624(0x1da)][_0x5ea624(0x174)](_0x423f07,_0x4cfd99);return _0x42533e[_0x5ea624(0x12a)]['call'](_0x423f07,_0x4cfd99,_0x53bafb,_0x42533e);},Window_Base[_0x1bfe85(0x11c)][_0x1bfe85(0x1bb)]=function(_0xcb5a80,_0xaaaf0d,_0x3d5e86){const _0x44d814=_0x1bfe85,_0x2de636=_0x3d5e86['CalcJS'][_0x44d814(0x174)](_0xcb5a80,_0xaaaf0d);return _0x3d5e86[_0x44d814(0x19f)][_0x44d814(0x174)](_0xcb5a80,_0xaaaf0d,_0x2de636,_0x3d5e86);},Window_Base['prototype'][_0x1bfe85(0x9f)]=function(){return'\x20';},Window_Base['prototype'][_0x1bfe85(0x16c)]=function(_0x1e8071,_0x5d46ca,_0x566106,_0x55b5e6){const _0x3af05a=_0x1bfe85;if(!_0x1e8071)return;VisuMZ['SkillsStatesCore'][_0x3af05a(0x11d)][_0x3af05a(0x174)](this,_0x1e8071,_0x5d46ca,_0x566106,_0x55b5e6),this[_0x3af05a(0x1dc)](_0x1e8071,_0x5d46ca,_0x566106,_0x55b5e6);},Window_Base[_0x1bfe85(0x11c)][_0x1bfe85(0x1dc)]=function(_0x49634c,_0xff171e,_0x19cc04,_0x1c29b9){const _0x3bab0d=_0x1bfe85;_0x1c29b9=_0x1c29b9||0x90;const _0x233c46=ImageManager[_0x3bab0d(0x18e)],_0x333156=_0x49634c['allIcons']()['slice'](0x0,Math[_0x3bab0d(0x138)](_0x1c29b9/_0x233c46)),_0x25759c=_0x49634c['states']()[_0x3bab0d(0xfb)](_0xc1779b=>_0xc1779b[_0x3bab0d(0x100)]>0x0),_0x582760=[...Array(0x8)[_0x3bab0d(0x1f3)]()][_0x3bab0d(0xfb)](_0x53e30c=>_0x49634c['buff'](_0x53e30c)!==0x0),_0x50dc91=[];let _0x1f934c=_0xff171e;for(let _0x309900=0x0;_0x309900<_0x333156['length'];_0x309900++){this['resetFontSettings']();const _0x33b8ba=_0x25759c[_0x309900];if(_0x33b8ba)!_0x50dc91[_0x3bab0d(0xa6)](_0x33b8ba)&&this[_0x3bab0d(0x132)](_0x49634c,_0x33b8ba,_0x1f934c,_0x19cc04),this['drawActorStateData'](_0x49634c,_0x33b8ba,_0x1f934c,_0x19cc04),_0x50dc91[_0x3bab0d(0x7a)](_0x33b8ba);else{const _0x3afa9c=_0x582760[_0x309900-_0x25759c[_0x3bab0d(0x97)]];this['drawActorBuffTurns'](_0x49634c,_0x3afa9c,_0x1f934c,_0x19cc04),this[_0x3bab0d(0x21d)](_0x49634c,_0x3afa9c,_0x1f934c,_0x19cc04);}_0x1f934c+=_0x233c46;}},Window_Base['prototype']['drawActorStateTurns']=function(_0x1c734b,_0x20222e,_0x51ca0e,_0x24320f){const _0x30ed18=_0x1bfe85;if(!VisuMZ['SkillsStatesCore'][_0x30ed18(0xdb)][_0x30ed18(0x95)]['ShowTurns'])return;if(!_0x1c734b['isStateAffected'](_0x20222e['id']))return;if(_0x20222e['autoRemovalTiming']===0x0)return;if(_0x20222e[_0x30ed18(0x161)]['match'](/<HIDE STATE TURNS>/i))return;const _0x2dfd38=_0x1c734b['stateTurns'](_0x20222e['id']),_0x35174a=ImageManager[_0x30ed18(0x18e)],_0x4578d9=ColorManager[_0x30ed18(0x249)](_0x20222e);this[_0x30ed18(0x19c)](_0x4578d9),this[_0x30ed18(0x1e9)]('rgba(0,\x200,\x200,\x201)'),this[_0x30ed18(0xd2)][_0x30ed18(0x1b2)]=!![],this[_0x30ed18(0xd2)]['fontSize']=VisuMZ[_0x30ed18(0x1f6)]['Settings'][_0x30ed18(0x95)][_0x30ed18(0xe9)],_0x51ca0e+=VisuMZ[_0x30ed18(0x1f6)][_0x30ed18(0xdb)][_0x30ed18(0x95)][_0x30ed18(0x171)],_0x24320f+=VisuMZ['SkillsStatesCore'][_0x30ed18(0xdb)]['States']['TurnOffsetY'],this[_0x30ed18(0x1fd)](_0x2dfd38,_0x51ca0e,_0x24320f,_0x35174a,_0x30ed18(0x278)),this['contents'][_0x30ed18(0x1b2)]=![],this[_0x30ed18(0x13c)]();},Window_Base[_0x1bfe85(0x11c)][_0x1bfe85(0xd9)]=function(_0x35de55,_0x11e20d,_0x4d8317,_0x5550f6){const _0x1b6812=_0x1bfe85;if(!VisuMZ[_0x1b6812(0x1f6)][_0x1b6812(0xdb)][_0x1b6812(0x95)][_0x1b6812(0x143)])return;const _0x5b3dbe=ImageManager[_0x1b6812(0x18e)],_0x2e6ac7=ImageManager[_0x1b6812(0x10f)]/0x2,_0x3a6db8=ColorManager['normalColor']();this[_0x1b6812(0x19c)](_0x3a6db8),this['changeOutlineColor'](_0x1b6812(0x1d3)),this[_0x1b6812(0xd2)][_0x1b6812(0x1b2)]=!![],this['contents'][_0x1b6812(0x23a)]=VisuMZ[_0x1b6812(0x1f6)][_0x1b6812(0xdb)][_0x1b6812(0x95)][_0x1b6812(0x17e)],_0x4d8317+=VisuMZ[_0x1b6812(0x1f6)][_0x1b6812(0xdb)][_0x1b6812(0x95)][_0x1b6812(0x10c)],_0x5550f6+=VisuMZ[_0x1b6812(0x1f6)][_0x1b6812(0xdb)][_0x1b6812(0x95)][_0x1b6812(0x1e6)];const _0x3a2e71=String(_0x35de55['getStateDisplay'](_0x11e20d['id']));this['drawText'](_0x3a2e71,_0x4d8317,_0x5550f6,_0x5b3dbe,_0x1b6812(0x145)),this['contents'][_0x1b6812(0x1b2)]=![],this['resetFontSettings']();},Window_Base[_0x1bfe85(0x11c)][_0x1bfe85(0x142)]=function(_0x2f3be5,_0xeadc0c,_0x564073,_0xc1794b){const _0x314647=_0x1bfe85;if(!VisuMZ[_0x314647(0x1f6)][_0x314647(0xdb)][_0x314647(0x254)][_0x314647(0x255)])return;const _0x6ac29e=_0x2f3be5[_0x314647(0x9d)](_0xeadc0c);if(_0x6ac29e===0x0)return;const _0x3555ee=_0x2f3be5[_0x314647(0x8b)](_0xeadc0c),_0x3fc1a3=ImageManager[_0x314647(0x18e)],_0x452720=_0x6ac29e>0x0?ColorManager[_0x314647(0xb5)]():ColorManager[_0x314647(0x190)]();this['changeTextColor'](_0x452720),this[_0x314647(0x1e9)](_0x314647(0x1d3)),this['contents'][_0x314647(0x1b2)]=!![],this[_0x314647(0xd2)]['fontSize']=VisuMZ[_0x314647(0x1f6)]['Settings'][_0x314647(0x254)]['TurnFontSize'],_0x564073+=VisuMZ[_0x314647(0x1f6)][_0x314647(0xdb)][_0x314647(0x254)][_0x314647(0x171)],_0xc1794b+=VisuMZ[_0x314647(0x1f6)]['Settings'][_0x314647(0x254)][_0x314647(0xc6)],this[_0x314647(0x1fd)](_0x3555ee,_0x564073,_0xc1794b,_0x3fc1a3,_0x314647(0x278)),this[_0x314647(0xd2)][_0x314647(0x1b2)]=![],this[_0x314647(0x13c)]();},Window_Base[_0x1bfe85(0x11c)][_0x1bfe85(0x21d)]=function(_0x3cdd51,_0x5ad84f,_0x375db8,_0x26760c){const _0x2b07bd=_0x1bfe85;if(!VisuMZ[_0x2b07bd(0x1f6)][_0x2b07bd(0xdb)]['Buffs'][_0x2b07bd(0x143)])return;const _0x4e669a=_0x3cdd51[_0x2b07bd(0x88)](_0x5ad84f),_0xc792e8=_0x3cdd51[_0x2b07bd(0x9d)](_0x5ad84f),_0x4128a3=ImageManager['iconWidth'],_0x3566c6=ImageManager[_0x2b07bd(0x10f)]/0x2,_0xcf3db0=_0xc792e8>0x0?ColorManager[_0x2b07bd(0xb5)]():ColorManager[_0x2b07bd(0x190)]();this['changeTextColor'](_0xcf3db0),this[_0x2b07bd(0x1e9)](_0x2b07bd(0x1d3)),this[_0x2b07bd(0xd2)]['fontBold']=!![],this[_0x2b07bd(0xd2)][_0x2b07bd(0x23a)]=VisuMZ[_0x2b07bd(0x1f6)][_0x2b07bd(0xdb)][_0x2b07bd(0x254)][_0x2b07bd(0x17e)],_0x375db8+=VisuMZ[_0x2b07bd(0x1f6)][_0x2b07bd(0xdb)][_0x2b07bd(0x254)]['DataOffsetX'],_0x26760c+=VisuMZ[_0x2b07bd(0x1f6)][_0x2b07bd(0xdb)]['Buffs']['DataOffsetY'];const _0x479f89=_0x2b07bd(0xd8)[_0x2b07bd(0x127)](Math[_0x2b07bd(0x113)](_0x4e669a*0x64));this[_0x2b07bd(0x1fd)](_0x479f89,_0x375db8,_0x26760c,_0x4128a3,_0x2b07bd(0x145)),this[_0x2b07bd(0xd2)]['fontBold']=![],this[_0x2b07bd(0x13c)]();},VisuMZ[_0x1bfe85(0x1f6)]['Window_StatusBase_placeGauge']=Window_StatusBase[_0x1bfe85(0x11c)]['placeGauge'],Window_StatusBase[_0x1bfe85(0x11c)]['placeGauge']=function(_0x4de79f,_0x3ad45d,_0x4a76e8,_0x335efb){const _0x1d581b=_0x1bfe85;if(_0x4de79f[_0x1d581b(0x1c5)]())_0x3ad45d=this['convertGaugeTypeSkillsStatesCore'](_0x4de79f,_0x3ad45d);this['placeExactGauge'](_0x4de79f,_0x3ad45d,_0x4a76e8,_0x335efb);},Window_StatusBase[_0x1bfe85(0x11c)]['placeExactGauge']=function(_0x3e49b7,_0x2ecf95,_0x1c0beb,_0x43a436){const _0x1d20f7=_0x1bfe85;if([_0x1d20f7(0x14a),'untitled']['includes'](_0x2ecf95[_0x1d20f7(0x15e)]()))return;VisuMZ[_0x1d20f7(0x1f6)]['Window_StatusBase_placeGauge'][_0x1d20f7(0x174)](this,_0x3e49b7,_0x2ecf95,_0x1c0beb,_0x43a436);},Window_StatusBase[_0x1bfe85(0x11c)][_0x1bfe85(0x24f)]=function(_0x90c3b,_0x2f8f43){const _0x14a2d9=_0x1bfe85,_0xa7c9d5=_0x90c3b[_0x14a2d9(0x235)]()['note'];if(_0x2f8f43==='hp'&&_0xa7c9d5[_0x14a2d9(0x298)](/<REPLACE HP GAUGE:[ ](.*)>/i))return String(RegExp['$1']);else{if(_0x2f8f43==='mp'&&_0xa7c9d5['match'](/<REPLACE MP GAUGE:[ ](.*)>/i))return String(RegExp['$1']);else return _0x2f8f43==='tp'&&_0xa7c9d5[_0x14a2d9(0x298)](/<REPLACE TP GAUGE:[ ](.*)>/i)?String(RegExp['$1']):_0x2f8f43;}},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x11d)]=Window_StatusBase[_0x1bfe85(0x11c)][_0x1bfe85(0x16c)],Window_StatusBase['prototype']['drawActorIcons']=function(_0x33e864,_0x576f34,_0x9bf148,_0x1d1dc1){const _0x51df4a=_0x1bfe85;if(!_0x33e864)return;Window_Base['prototype'][_0x51df4a(0x16c)][_0x51df4a(0x174)](this,_0x33e864,_0x576f34,_0x9bf148,_0x1d1dc1);},VisuMZ[_0x1bfe85(0x1f6)]['Window_SkillType_initialize']=Window_SkillType[_0x1bfe85(0x11c)][_0x1bfe85(0x204)],Window_SkillType[_0x1bfe85(0x11c)][_0x1bfe85(0x204)]=function(_0x59b537){const _0x5c2a1d=_0x1bfe85;VisuMZ[_0x5c2a1d(0x1f6)][_0x5c2a1d(0x154)][_0x5c2a1d(0x174)](this,_0x59b537),this['createCommandNameWindow'](_0x59b537);},Window_SkillType['prototype'][_0x1bfe85(0x1e2)]=function(_0x1e3355){const _0x28c690=_0x1bfe85,_0x1e822a=new Rectangle(0x0,0x0,_0x1e3355[_0x28c690(0x159)],_0x1e3355[_0x28c690(0xc8)]);this[_0x28c690(0xa7)]=new Window_Base(_0x1e822a),this[_0x28c690(0xa7)][_0x28c690(0x253)]=0x0,this[_0x28c690(0x25b)](this[_0x28c690(0xa7)]),this[_0x28c690(0x85)]();},Window_SkillType[_0x1bfe85(0x11c)][_0x1bfe85(0x26e)]=function(){const _0x13c456=_0x1bfe85;Window_Command[_0x13c456(0x11c)][_0x13c456(0x26e)][_0x13c456(0x174)](this);if(this[_0x13c456(0xa7)])this[_0x13c456(0x85)]();},Window_SkillType[_0x1bfe85(0x11c)]['updateCommandNameWindow']=function(){const _0x3d7e97=_0x1bfe85,_0x4286fc=this['_commandNameWindow'];_0x4286fc['contents'][_0x3d7e97(0x244)]();const _0x14d8fc=this[_0x3d7e97(0x233)](this[_0x3d7e97(0x16a)]());if(_0x14d8fc===_0x3d7e97(0x1b4)&&this['maxItems']()>0x0){const _0x2836b8=this['itemLineRect'](this[_0x3d7e97(0x16a)]());let _0x587502=this['commandName'](this['index']());_0x587502=_0x587502[_0x3d7e97(0x1df)](/\\I\[(\d+)\]/gi,''),_0x4286fc[_0x3d7e97(0x13c)](),this[_0x3d7e97(0x82)](_0x587502,_0x2836b8),this[_0x3d7e97(0xb3)](_0x587502,_0x2836b8),this[_0x3d7e97(0x114)](_0x587502,_0x2836b8);}},Window_SkillType[_0x1bfe85(0x11c)][_0x1bfe85(0x82)]=function(_0x1e32d1,_0x4e5c0c){},Window_SkillType['prototype']['commandNameWindowDrawText']=function(_0x48f936,_0x1dc420){const _0x2b750d=_0x1bfe85,_0x59ceac=this[_0x2b750d(0xa7)];_0x59ceac[_0x2b750d(0x1fd)](_0x48f936,0x0,_0x1dc420['y'],_0x59ceac[_0x2b750d(0xa1)],_0x2b750d(0x145));},Window_SkillType[_0x1bfe85(0x11c)][_0x1bfe85(0x114)]=function(_0x519125,_0x1ba79e){const _0x40a145=_0x1bfe85,_0x8ff00b=this[_0x40a145(0xa7)],_0x130a4a=$gameSystem[_0x40a145(0x270)](),_0x187526=_0x1ba79e['x']+Math[_0x40a145(0x138)](_0x1ba79e[_0x40a145(0x159)]/0x2)+_0x130a4a;_0x8ff00b['x']=_0x8ff00b[_0x40a145(0x159)]/-0x2+_0x187526,_0x8ff00b['y']=Math[_0x40a145(0x138)](_0x1ba79e[_0x40a145(0xc8)]/0x2);},Window_SkillType[_0x1bfe85(0x11c)][_0x1bfe85(0x214)]=function(){const _0x3d4c96=_0x1bfe85;return Imported[_0x3d4c96(0x177)]&&Window_Command[_0x3d4c96(0x11c)]['isUseModernControls']['call'](this);},Window_SkillType[_0x1bfe85(0x11c)][_0x1bfe85(0x1fe)]=function(){const _0x2811bc=_0x1bfe85;if(!this[_0x2811bc(0xd1)])return;const _0x8e95fc=this['_actor'][_0x2811bc(0x27c)]();for(const _0x473b1e of _0x8e95fc){const _0x125254=this[_0x2811bc(0x80)](_0x473b1e);this[_0x2811bc(0x1f8)](_0x125254,'skill',!![],_0x473b1e);}},Window_SkillType[_0x1bfe85(0x11c)]['makeCommandName']=function(_0x4615ed){const _0x3e6c48=_0x1bfe85;let _0x5395a7=$dataSystem['skillTypes'][_0x4615ed];if(_0x5395a7[_0x3e6c48(0x298)](/\\I\[(\d+)\]/i))return _0x5395a7;if(this[_0x3e6c48(0x201)]()===_0x3e6c48(0x1e7))return _0x5395a7;const _0x5a1fe6=VisuMZ[_0x3e6c48(0x1f6)][_0x3e6c48(0xdb)][_0x3e6c48(0x8c)],_0x42d2af=$dataSystem[_0x3e6c48(0x20b)]['includes'](_0x4615ed),_0x5f4a6b=_0x42d2af?_0x5a1fe6[_0x3e6c48(0x93)]:_0x5a1fe6['IconStypeNorm'];return _0x3e6c48(0x243)['format'](_0x5f4a6b,_0x5395a7);},Window_SkillType[_0x1bfe85(0x11c)][_0x1bfe85(0x96)]=function(){const _0x33a2ae=_0x1bfe85;return VisuMZ[_0x33a2ae(0x1f6)][_0x33a2ae(0xdb)][_0x33a2ae(0x8c)]['CmdTextAlign'];},Window_SkillType[_0x1bfe85(0x11c)][_0x1bfe85(0x124)]=function(_0xea6113){const _0x2328fb=_0x1bfe85,_0x295114=this['commandStyleCheck'](_0xea6113);if(_0x295114===_0x2328fb(0x119))this[_0x2328fb(0x202)](_0xea6113);else _0x295114===_0x2328fb(0x1b4)?this['drawItemStyleIcon'](_0xea6113):Window_Command[_0x2328fb(0x11c)][_0x2328fb(0x124)][_0x2328fb(0x174)](this,_0xea6113);},Window_SkillType[_0x1bfe85(0x11c)]['commandStyle']=function(){const _0x5c6fd7=_0x1bfe85;return VisuMZ['SkillsStatesCore']['Settings']['Skills'][_0x5c6fd7(0x137)];},Window_SkillType[_0x1bfe85(0x11c)][_0x1bfe85(0x233)]=function(_0x2b1e08){const _0x37b0bb=_0x1bfe85;if(_0x2b1e08<0x0)return _0x37b0bb(0x1e7);const _0x3d25aa=this[_0x37b0bb(0x201)]();if(_0x3d25aa!==_0x37b0bb(0x128))return _0x3d25aa;else{if(this['maxItems']()>0x0){const _0x51ccf1=this[_0x37b0bb(0xaf)](_0x2b1e08);if(_0x51ccf1[_0x37b0bb(0x298)](/\\I\[(\d+)\]/i)){const _0x4918a8=this[_0x37b0bb(0x1b8)](_0x2b1e08),_0xe865a8=this[_0x37b0bb(0x13e)](_0x51ccf1)[_0x37b0bb(0x159)];return _0xe865a8<=_0x4918a8['width']?_0x37b0bb(0x119):_0x37b0bb(0x1b4);}}}return _0x37b0bb(0x1e7);},Window_SkillType['prototype'][_0x1bfe85(0x202)]=function(_0x378bf2){const _0x1847eb=_0x1bfe85,_0x7e56a3=this['itemLineRect'](_0x378bf2),_0x154261=this[_0x1847eb(0xaf)](_0x378bf2),_0x4f1805=this[_0x1847eb(0x13e)](_0x154261)[_0x1847eb(0x159)];this[_0x1847eb(0x1d1)](this['isCommandEnabled'](_0x378bf2));const _0x35b0e0=this[_0x1847eb(0x96)]();if(_0x35b0e0===_0x1847eb(0x278))this[_0x1847eb(0x106)](_0x154261,_0x7e56a3['x']+_0x7e56a3[_0x1847eb(0x159)]-_0x4f1805,_0x7e56a3['y'],_0x4f1805);else{if(_0x35b0e0==='center'){const _0x50014b=_0x7e56a3['x']+Math['floor']((_0x7e56a3[_0x1847eb(0x159)]-_0x4f1805)/0x2);this[_0x1847eb(0x106)](_0x154261,_0x50014b,_0x7e56a3['y'],_0x4f1805);}else this[_0x1847eb(0x106)](_0x154261,_0x7e56a3['x'],_0x7e56a3['y'],_0x4f1805);}},Window_SkillType[_0x1bfe85(0x11c)][_0x1bfe85(0x1c0)]=function(_0xc1c73){const _0x4481c5=_0x1bfe85;this['commandName'](_0xc1c73)[_0x4481c5(0x298)](/\\I\[(\d+)\]/i);const _0x42251c=Number(RegExp['$1'])||0x0,_0x44ed5e=this['itemLineRect'](_0xc1c73),_0x3a2630=_0x44ed5e['x']+Math[_0x4481c5(0x138)]((_0x44ed5e[_0x4481c5(0x159)]-ImageManager[_0x4481c5(0x18e)])/0x2),_0x561067=_0x44ed5e['y']+(_0x44ed5e[_0x4481c5(0xc8)]-ImageManager[_0x4481c5(0x10f)])/0x2;this['drawIcon'](_0x42251c,_0x3a2630,_0x561067);},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x1cf)]=Window_SkillStatus[_0x1bfe85(0x11c)]['refresh'],Window_SkillStatus['prototype'][_0x1bfe85(0x78)]=function(){const _0xc0eb76=_0x1bfe85;VisuMZ[_0xc0eb76(0x1f6)][_0xc0eb76(0x1cf)]['call'](this);if(this['_actor'])this[_0xc0eb76(0x8f)]();},Window_SkillStatus[_0x1bfe85(0x11c)][_0x1bfe85(0x8f)]=function(){const _0x14273d=_0x1bfe85;if(!Imported[_0x14273d(0x177)])return;if(!Imported[_0x14273d(0x1c3)])return;const _0x8f2acf=this[_0x14273d(0x1e4)]();let _0x8dacf4=this[_0x14273d(0x22f)]()/0x2+0xb4+0xb4+0xb4,_0x52a73e=this[_0x14273d(0xa1)]-_0x8dacf4-0x2;if(_0x52a73e>=0x12c){const _0x52d3f1=VisuMZ['CoreEngine'][_0x14273d(0xdb)]['Param'][_0x14273d(0x20c)],_0x167359=Math[_0x14273d(0x138)](_0x52a73e/0x2)-0x18;let _0x23c06d=_0x8dacf4,_0x48c8e6=Math['floor']((this[_0x14273d(0x18c)]-Math[_0x14273d(0x15b)](_0x52d3f1[_0x14273d(0x97)]/0x2)*_0x8f2acf)/0x2),_0x8f8888=0x0;for(const _0x5a3f1f of _0x52d3f1){this['drawExtendedParameter'](_0x23c06d,_0x48c8e6,_0x167359,_0x5a3f1f),_0x8f8888++,_0x8f8888%0x2===0x0?(_0x23c06d=_0x8dacf4,_0x48c8e6+=_0x8f2acf):_0x23c06d+=_0x167359+0x18;}}this[_0x14273d(0x13c)]();},Window_SkillStatus[_0x1bfe85(0x11c)][_0x1bfe85(0x210)]=function(_0x1f55c5,_0x5d3755,_0x3221ba,_0x42caa4){const _0x29fece=_0x1bfe85,_0x37eaa0=this[_0x29fece(0x1e4)]();this[_0x29fece(0x13c)](),this[_0x29fece(0x148)](_0x1f55c5,_0x5d3755,_0x3221ba,_0x42caa4,!![]),this[_0x29fece(0x296)](),this[_0x29fece(0xd2)][_0x29fece(0x23a)]-=0x8;const _0x54ebaf=this[_0x29fece(0xd1)]['paramValueByName'](_0x42caa4,!![]);this[_0x29fece(0xd2)][_0x29fece(0x1fd)](_0x54ebaf,_0x1f55c5,_0x5d3755,_0x3221ba,_0x37eaa0,'right');},VisuMZ['SkillsStatesCore'][_0x1bfe85(0x2a4)]=Window_SkillList['prototype'][_0x1bfe85(0xa6)],Window_SkillList[_0x1bfe85(0x11c)][_0x1bfe85(0xa6)]=function(_0xd1202f){const _0x16b745=_0x1bfe85;return this[_0x16b745(0x69)](_0xd1202f);},VisuMZ[_0x1bfe85(0x1f6)]['Window_SkillList_maxCols']=Window_SkillList[_0x1bfe85(0x11c)][_0x1bfe85(0x194)],Window_SkillList[_0x1bfe85(0x11c)]['maxCols']=function(){const _0xbb1875=_0x1bfe85;return SceneManager[_0xbb1875(0x1e8)]['constructor']===Scene_Battle?VisuMZ['SkillsStatesCore']['Window_SkillList_maxCols'][_0xbb1875(0x174)](this):VisuMZ[_0xbb1875(0x1f6)][_0xbb1875(0xdb)][_0xbb1875(0x8c)]['ListWindowCols'];},VisuMZ[_0x1bfe85(0x1f6)][_0x1bfe85(0x246)]=Window_SkillList['prototype']['setActor'],Window_SkillList['prototype'][_0x1bfe85(0x182)]=function(_0x2fc74a){const _0x2e7138=_0x1bfe85,_0x4d2f6a=this[_0x2e7138(0xd1)]!==_0x2fc74a;VisuMZ[_0x2e7138(0x1f6)][_0x2e7138(0x246)][_0x2e7138(0x174)](this,_0x2fc74a),_0x4d2f6a&&(this[_0x2e7138(0x1ba)]&&this[_0x2e7138(0x1ba)][_0x2e7138(0x282)]===Window_ShopStatus&&this[_0x2e7138(0x1ba)][_0x2e7138(0x292)](this[_0x2e7138(0x17d)](0x0)));},Window_SkillList['prototype'][_0x1bfe85(0xe4)]=function(_0x342f6d){const _0x187c89=_0x1bfe85;if(this[_0x187c89(0x13b)]===_0x342f6d)return;this[_0x187c89(0x13b)]=_0x342f6d,this[_0x187c89(0x78)](),this[_0x187c89(0x1ef)](0x0,0x0),this['_statusWindow']&&this[_0x187c89(0x1ba)]['constructor']===Window_ShopStatus&&this[_0x187c89(0x1ba)]['setItem'](this[_0x187c89(0x17d)](0x0));},Window_SkillList[_0x1bfe85(0x11c)][_0x1bfe85(0x69)]=function(_0x1a3461){const _0x1e0cd2=_0x1bfe85;if(!_0x1a3461)return VisuMZ[_0x1e0cd2(0x1f6)][_0x1e0cd2(0x2a4)][_0x1e0cd2(0x174)](this,_0x1a3461);if(!this[_0x1e0cd2(0x1d7)](_0x1a3461))return![];if(!this[_0x1e0cd2(0x23f)](_0x1a3461))return![];if(!this[_0x1e0cd2(0x10d)](_0x1a3461))return![];return!![];},Window_SkillList[_0x1bfe85(0x11c)][_0x1bfe85(0x1d7)]=function(_0x4b8a20){const _0x43f0f4=_0x1bfe85;return DataManager['getSkillTypes'](_0x4b8a20)[_0x43f0f4(0xa6)](this[_0x43f0f4(0x13b)]);},Window_SkillList[_0x1bfe85(0x11c)][_0x1bfe85(0x23f)]=function(_0x3c256f){const _0x4a176b=_0x1bfe85;if(!this[_0x4a176b(0x122)](_0x3c256f))return![];if(!this[_0x4a176b(0x1d0)](_0x3c256f))return![];if(!this[_0x4a176b(0x218)](_0x3c256f))return![];return!![];},Window_SkillList[_0x1bfe85(0x11c)][_0x1bfe85(0x122)]=function(_0x3e8f80){const _0x8981f4=_0x1bfe85,_0x44c998=_0x3e8f80[_0x8981f4(0x161)];if(_0x44c998[_0x8981f4(0x298)](/<HIDE IN BATTLE>/i)&&$gameParty[_0x8981f4(0x130)]())return![];else return _0x44c998[_0x8981f4(0x298)](/<HIDE OUTSIDE BATTLE>/i)&&!$gameParty['inBattle']()?![]:!![];},Window_SkillList[_0x1bfe85(0x11c)][_0x1bfe85(0x1d0)]=function(_0x53d58e){const _0x1f679e=_0x1bfe85,_0x4c72fe=_0x53d58e[_0x1f679e(0x161)];if(_0x4c72fe[_0x1f679e(0x298)](/<SHOW[ ](?:SW|SWITCH|SWITCHES):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x391e4f=JSON[_0x1f679e(0xca)]('['+RegExp['$1']['match'](/\d+/g)+']');for(const _0x498320 of _0x391e4f){if(!$gameSwitches[_0x1f679e(0x8e)](_0x498320))return![];}return!![];}if(_0x4c72fe[_0x1f679e(0x298)](/<SHOW ALL[ ](?:SW|SWITCH|SWITCHES):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x4ed78d=JSON[_0x1f679e(0xca)]('['+RegExp['$1']['match'](/\d+/g)+']');for(const _0x3324e6 of _0x4ed78d){if(!$gameSwitches[_0x1f679e(0x8e)](_0x3324e6))return![];}return!![];}if(_0x4c72fe[_0x1f679e(0x298)](/<SHOW ANY[ ](?:SW|SWITCH|SWITCHES):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x41dc3b=JSON[_0x1f679e(0xca)]('['+RegExp['$1']['match'](/\d+/g)+']');for(const _0x3a156e of _0x41dc3b){if($gameSwitches['value'](_0x3a156e))return!![];}return![];}if(_0x4c72fe[_0x1f679e(0x298)](/<HIDE[ ](?:SW|SWITCH|SWITCHES):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x14f104=JSON[_0x1f679e(0xca)]('['+RegExp['$1'][_0x1f679e(0x298)](/\d+/g)+']');for(const _0x381b13 of _0x14f104){if(!$gameSwitches[_0x1f679e(0x8e)](_0x381b13))return!![];}return![];}if(_0x4c72fe[_0x1f679e(0x298)](/<HIDE ALL[ ](?:SW|SWITCH|SWITCHES):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x425d39=JSON[_0x1f679e(0xca)]('['+RegExp['$1'][_0x1f679e(0x298)](/\d+/g)+']');for(const _0xe3edd0 of _0x425d39){if(!$gameSwitches[_0x1f679e(0x8e)](_0xe3edd0))return!![];}return![];}if(_0x4c72fe[_0x1f679e(0x298)](/<HIDE ANY[ ](?:SW|SWITCH|SWITCHES):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x2fa96a=JSON['parse']('['+RegExp['$1'][_0x1f679e(0x298)](/\d+/g)+']');for(const _0x5e5c2b of _0x2fa96a){if($gameSwitches[_0x1f679e(0x8e)](_0x5e5c2b))return![];}return!![];}return!![];},Window_SkillList['prototype'][_0x1bfe85(0x218)]=function(_0x25d68a){const _0x17dbfe=_0x1bfe85,_0x1b5d1c=_0x25d68a[_0x17dbfe(0x161)];if(_0x1b5d1c[_0x17dbfe(0x298)](/<SHOW IF LEARNED[ ](?:SKILL|SKILLS):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x468ecc=JSON[_0x17dbfe(0xca)]('['+RegExp['$1'][_0x17dbfe(0x298)](/\d+/g)+']');for(const _0x206053 of _0x468ecc){if(!this[_0x17dbfe(0xd1)]['isLearnedSkill'](_0x206053))return![];}return!![];}else{if(_0x1b5d1c[_0x17dbfe(0x298)](/<SHOW IF LEARNED[ ](?:SKILL|SKILLS):[ ](.*)>/i)){const _0x5ea2c4=RegExp['$1']['split'](',');for(const _0x24f03e of _0x5ea2c4){const _0x14aab3=DataManager['getSkillIdWithName'](_0x24f03e);if(!_0x14aab3)continue;if(!this[_0x17dbfe(0xd1)][_0x17dbfe(0x27a)](_0x14aab3))return![];}return!![];}}if(_0x1b5d1c[_0x17dbfe(0x298)](/<SHOW IF LEARNED ALL[ ](?:SKILL|SKILLS):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x516c21=JSON[_0x17dbfe(0xca)]('['+RegExp['$1'][_0x17dbfe(0x298)](/\d+/g)+']');for(const _0x13cc07 of _0x516c21){if(!this[_0x17dbfe(0xd1)][_0x17dbfe(0x27a)](_0x13cc07))return![];}return!![];}else{if(_0x1b5d1c[_0x17dbfe(0x298)](/<SHOW IF LEARNED ALL[ ](?:SKILL|SKILLS):[ ](.*)>/i)){const _0xd4e307=RegExp['$1'][_0x17dbfe(0x144)](',');for(const _0x1e0a70 of _0xd4e307){const _0x2f0838=DataManager[_0x17dbfe(0x1d6)](_0x1e0a70);if(!_0x2f0838)continue;if(!this[_0x17dbfe(0xd1)][_0x17dbfe(0x27a)](_0x2f0838))return![];}return!![];}}if(_0x1b5d1c[_0x17dbfe(0x298)](/<SHOW IF LEARNED ANY[ ](?:SKILL|SKILLS):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x5211e9=JSON[_0x17dbfe(0xca)]('['+RegExp['$1'][_0x17dbfe(0x298)](/\d+/g)+']');for(const _0x9e18ce of _0x5211e9){if(this[_0x17dbfe(0xd1)]['isLearnedSkill'](_0x9e18ce))return!![];}return![];}else{if(_0x1b5d1c[_0x17dbfe(0x298)](/<SHOW IF LEARNED ANY[ ](?:SKILL|SKILLS):[ ](.*)>/i)){const _0x5c1a34=RegExp['$1'][_0x17dbfe(0x144)](',');for(const _0x4c013e of _0x5c1a34){const _0x3609ed=DataManager[_0x17dbfe(0x1d6)](_0x4c013e);if(!_0x3609ed)continue;if(this[_0x17dbfe(0xd1)]['isLearnedSkill'](_0x3609ed))return!![];}return![];}}if(_0x1b5d1c[_0x17dbfe(0x298)](/<HIDE IF LEARNED[ ](?:SKILL|SKILLS):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x263c6a=JSON[_0x17dbfe(0xca)]('['+RegExp['$1']['match'](/\d+/g)+']');for(const _0x469baa of _0x263c6a){if(!this[_0x17dbfe(0xd1)][_0x17dbfe(0x27a)](_0x469baa))return!![];}return![];}else{if(_0x1b5d1c[_0x17dbfe(0x298)](/<HIDE IF LEARNED[ ](?:SKILL|SKILLS):[ ](.*)>/i)){const _0x52feab=RegExp['$1'][_0x17dbfe(0x144)](',');for(const _0x3fc7a3 of _0x52feab){const _0x5c3b8f=DataManager[_0x17dbfe(0x1d6)](_0x3fc7a3);if(!_0x5c3b8f)continue;if(!this[_0x17dbfe(0xd1)][_0x17dbfe(0x27a)](_0x5c3b8f))return!![];}return![];}}if(_0x1b5d1c[_0x17dbfe(0x298)](/<HIDE IF LEARNED ALL[ ](?:SKILL|SKILLS):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x4f0009=JSON[_0x17dbfe(0xca)]('['+RegExp['$1'][_0x17dbfe(0x298)](/\d+/g)+']');for(const _0x4886d2 of _0x4f0009){if(!this[_0x17dbfe(0xd1)][_0x17dbfe(0x27a)](_0x4886d2))return!![];}return![];}else{if(_0x1b5d1c['match'](/<HIDE IF LEARNED ALL[ ](?:SKILL|SKILLS):[ ](.*)>/i)){const _0x19e1c9=RegExp['$1'][_0x17dbfe(0x144)](',');for(const _0x2a9888 of _0x19e1c9){const _0x5c47be=DataManager['getSkillIdWithName'](_0x2a9888);if(!_0x5c47be)continue;if(!this[_0x17dbfe(0xd1)][_0x17dbfe(0x27a)](_0x5c47be))return!![];}return![];}}if(_0x1b5d1c['match'](/<HIDE IF LEARNED ANY[ ](?:SKILL|SKILLS):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x52bd87=JSON['parse']('['+RegExp['$1'][_0x17dbfe(0x298)](/\d+/g)+']');for(const _0x3e367e of _0x52bd87){if(this['_actor']['isLearnedSkill'](_0x3e367e))return![];}return!![];}else{if(_0x1b5d1c[_0x17dbfe(0x298)](/<HIDE IF LEARNED ANY[ ](?:SKILL|SKILLS):[ ](.*)>/i)){const _0x1670ea=RegExp['$1'][_0x17dbfe(0x144)](',');for(const _0x1f359f of _0x1670ea){const _0x28a259=DataManager['getSkillIdWithName'](_0x1f359f);if(!_0x28a259)continue;if(this['_actor'][_0x17dbfe(0x27a)](_0x28a259))return![];}return!![];}}if(_0x1b5d1c['match'](/<SHOW IF (?:HAS|HAVE)[ ](?:SKILL|SKILLS):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x38fcb1=JSON[_0x17dbfe(0xca)]('['+RegExp['$1']['match'](/\d+/g)+']');for(const _0x310c05 of _0x38fcb1){if(!this[_0x17dbfe(0xd1)][_0x17dbfe(0x231)](_0x310c05))return![];}return!![];}else{if(_0x1b5d1c[_0x17dbfe(0x298)](/<SHOW IF (?:HAS|HAVE)[ ](?:SKILL|SKILLS):[ ](.*)>/i)){const _0x4b46a4=RegExp['$1'][_0x17dbfe(0x144)](',');for(const _0x132695 of _0x4b46a4){const _0x3c9529=DataManager['getSkillIdWithName'](_0x132695);if(!_0x3c9529)continue;if(!this[_0x17dbfe(0xd1)][_0x17dbfe(0x231)](_0x3c9529))return![];}return!![];}}if(_0x1b5d1c[_0x17dbfe(0x298)](/<SHOW IF (?:HAS|HAVE) ALL[ ](?:SKILL|SKILLS):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x175b69=JSON[_0x17dbfe(0xca)]('['+RegExp['$1'][_0x17dbfe(0x298)](/\d+/g)+']');for(const _0x5c8c30 of _0x175b69){if(!this[_0x17dbfe(0xd1)][_0x17dbfe(0x231)](_0x5c8c30))return![];}return!![];}else{if(_0x1b5d1c[_0x17dbfe(0x298)](/<SHOW IF (?:HAS|HAVE) ALL[ ](?:SKILL|SKILLS):[ ](.*)>/i)){const _0x4db17c=RegExp['$1'][_0x17dbfe(0x144)](',');for(const _0x469110 of _0x4db17c){const _0x30e132=DataManager[_0x17dbfe(0x1d6)](_0x469110);if(!_0x30e132)continue;if(!this[_0x17dbfe(0xd1)][_0x17dbfe(0x231)](_0x30e132))return![];}return!![];}}if(_0x1b5d1c[_0x17dbfe(0x298)](/<SHOW IF (?:HAS|HAVE) ANY[ ](?:SKILL|SKILLS):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x520846=JSON['parse']('['+RegExp['$1']['match'](/\d+/g)+']');for(const _0x2ccaba of _0x520846){if(this[_0x17dbfe(0xd1)][_0x17dbfe(0x231)](_0x2ccaba))return!![];}return![];}else{if(_0x1b5d1c[_0x17dbfe(0x298)](/<SHOW IF (?:HAS|HAVE) ANY[ ](?:SKILL|SKILLS):[ ](.*)>/i)){const _0x281e1a=RegExp['$1']['split'](',');for(const _0x1fd48f of _0x281e1a){const _0x3c4c74=DataManager[_0x17dbfe(0x1d6)](_0x1fd48f);if(!_0x3c4c74)continue;if(this[_0x17dbfe(0xd1)][_0x17dbfe(0x231)](_0x3c4c74))return!![];}return![];}}if(_0x1b5d1c[_0x17dbfe(0x298)](/<HIDE IF (?:HAS|HAVE)[ ](?:SKILL|SKILLS):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x4b6621=JSON[_0x17dbfe(0xca)]('['+RegExp['$1'][_0x17dbfe(0x298)](/\d+/g)+']');for(const _0x2f31d8 of _0x4b6621){if(!this[_0x17dbfe(0xd1)][_0x17dbfe(0x231)](_0x2f31d8))return!![];}return![];}else{if(_0x1b5d1c[_0x17dbfe(0x298)](/<HIDE IF (?:HAS|HAVE)[ ](?:SKILL|SKILLS):[ ](.*)>/i)){const _0x2c3351=RegExp['$1'][_0x17dbfe(0x144)](',');for(const _0x5d0ace of _0x2c3351){const _0x4c96f7=DataManager[_0x17dbfe(0x1d6)](_0x5d0ace);if(!_0x4c96f7)continue;if(!this['_actor'][_0x17dbfe(0x231)](_0x4c96f7))return!![];}return![];}}if(_0x1b5d1c[_0x17dbfe(0x298)](/<HIDE IF (?:HAS|HAVE) ALL[ ](?:SKILL|SKILLS):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x5d12c1=JSON[_0x17dbfe(0xca)]('['+RegExp['$1'][_0x17dbfe(0x298)](/\d+/g)+']');for(const _0x1719b3 of _0x5d12c1){if(!this[_0x17dbfe(0xd1)][_0x17dbfe(0x231)](_0x1719b3))return!![];}return![];}else{if(_0x1b5d1c[_0x17dbfe(0x298)](/<HIDE IF (?:HAS|HAVE) ALL[ ](?:SKILL|SKILLS):[ ](.*)>/i)){const _0x5be819=RegExp['$1'][_0x17dbfe(0x144)](',');for(const _0x48448f of _0x5be819){const _0x300ef4=DataManager['getSkillIdWithName'](_0x48448f);if(!_0x300ef4)continue;if(!this[_0x17dbfe(0xd1)][_0x17dbfe(0x231)](_0x300ef4))return!![];}return![];}}if(_0x1b5d1c[_0x17dbfe(0x298)](/<HIDE IF (?:HAS|HAVE) ANY[ ](?:SKILL|SKILLS):[ ]*(\d+(?:\s*,\s*\d+)*)>/i)){const _0x245cd4=JSON[_0x17dbfe(0xca)]('['+RegExp['$1'][_0x17dbfe(0x298)](/\d+/g)+']');for(const _0x23ce19 of _0x245cd4){if(this[_0x17dbfe(0xd1)]['hasSkill'](_0x23ce19))return![];}return!![];}else{if(_0x1b5d1c['match'](/<HIDE IF (?:HAS|HAVE) ANY[ ](?:SKILL|SKILLS):[ ](.*)>/i)){const _0x35da72=RegExp['$1']['split'](',');for(const _0x549e4f of _0x35da72){const _0xd6a240=DataManager[_0x17dbfe(0x1d6)](_0x549e4f);if(!_0xd6a240)continue;if(this['_actor']['hasSkill'](_0xd6a240))return![];}return!![];}}return!![];},Window_SkillList[_0x1bfe85(0x11c)]['checkShowHideJS']=function(_0x28a433){const _0x5a8644=_0x1bfe85,_0x151a3b=_0x28a433[_0x5a8644(0x161)],_0x529967=VisuMZ[_0x5a8644(0x1f6)]['skillVisibleJS'];return _0x529967[_0x28a433['id']]?_0x529967[_0x28a433['id']][_0x5a8644(0x174)](this,_0x28a433):!![];},Window_SkillList[_0x1bfe85(0x11c)][_0x1bfe85(0x1ae)]=function(_0x48e846,_0x2a5887,_0x8ad6db,_0x613fc5){const _0x29b9fa=_0x1bfe85;Window_Base['prototype']['drawSkillCost'][_0x29b9fa(0x174)](this,this[_0x29b9fa(0xd1)],_0x48e846,_0x2a5887,_0x8ad6db,_0x613fc5);},Window_SkillList[_0x1bfe85(0x11c)]['setStatusWindow']=function(_0xab7b57){const _0x508086=_0x1bfe85;this[_0x508086(0x1ba)]=_0xab7b57,this[_0x508086(0x26e)]();},VisuMZ['SkillsStatesCore'][_0x1bfe85(0x178)]=Window_SkillList['prototype'][_0x1bfe85(0x1a4)],Window_SkillList[_0x1bfe85(0x11c)][_0x1bfe85(0x1a4)]=function(){const _0xe2043c=_0x1bfe85;VisuMZ[_0xe2043c(0x1f6)]['Window_SkillList_updateHelp'][_0xe2043c(0x174)](this),this[_0xe2043c(0x1ba)]&&this[_0xe2043c(0x1ba)][_0xe2043c(0x282)]===Window_ShopStatus&&this[_0xe2043c(0x1ba)][_0xe2043c(0x292)](this[_0xe2043c(0x1ab)]());};