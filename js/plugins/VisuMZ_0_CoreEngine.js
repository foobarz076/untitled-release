//=============================================================================
// VisuStella MZ - Core Engine
// VisuMZ_0_CoreEngine.js
//=============================================================================

var Imported = Imported || {};
Imported.VisuMZ_0_CoreEngine = true;

var VisuMZ = VisuMZ || {};
VisuMZ.CoreEngine = VisuMZ.CoreEngine || {};
VisuMZ.CoreEngine.version = 1.26;

//=============================================================================
 /*:
 * @target MZ
 * @plugindesc [RPG Maker MZ] [Tier 0] [Version 1.26] [CoreEngine]
 * @author VisuStella
 * @url http://www.yanfly.moe/wiki/Core_Engine_VisuStella_MZ
 *
 * @help
 * ============================================================================
 * Introduction
 * ============================================================================
 *
 * The Core Engine plugin is designed to fix any bugs that may have slipped
 * past RPG Maker MZ's source code and to give game devs more control over
 * RPG Maker MZ's various features, ranging from mechanics to aesthetics to
 * quality of life improvements.
 *
 * Features include all (but not limited to) the following:
 *
 * * Bug fixes for the problems existing in the RPG Maker MZ base code.
 * * Failsafes added for Script Call related event commands.
 * * Lots of Quality of Life Settings that can be activated through the
 *   Plugin Parameters.
 * * Control over the various Text Colors used throughout the game.
 * * Change up the maximum amount of gold carried, give it an icon attached to
 *   the label, and include text for overlap specifics.
 * * Preload images as the game boots up.
 * * Add specific background images for menus found throughout the game.
 * * A button assist window will appear at the top or bottom of the screen,
 *   detailing which buttons do what when inside a menu. This feature can be
 *   turned off.
 * * Choose which in-game battler parameters to display inside menus (ie ATK,
 *   DEF, AGI, etc.) and determine their maximum values, along with plenty of
 *   notetags to give more control over parameter, x-parameter, s-parameter
 *   bonuses through equipment, states, and other trait objects.
 * * Control over how the UI objects appear (such as the menu button, cancel
 *   button, left/right actor switch buttons).
 * * Reposition actors and enemies if the battle resolution is larger.
 * * Allow class names and nicknames to support text codes when displayed.
 * * Determine how windows behave in the game, if they will mask other windows,
 *   their line height properties, and more.
 *
 * ============================================================================
 * Requirements
 * ============================================================================
 *
 * This plugin is made for RPG Maker MZ. This will not work in other iterations
 * of RPG Maker.
 *
 * ------ Tier 0 ------
 *
 * This plugin is a Tier 0 plugin. Place it under other plugins of lower tier
 * value on your Plugin Manager list (ie: 0, 1, 2, 3, 4, 5). This is to ensure
 * that your plugins will have the best compatibility with the rest of the
 * VisuStella MZ Plugin library.
 *
 * ============================================================================
 * Important Changes: Bug Fixes
 * ============================================================================
 *
 * This plugin also serves to fix various bugs found in RPG Maker MZ that have
 * been unaddressed or not yet taken care of. The following is a list of bugs
 * that have been fixed by this plugin:
 *
 * ---
 *
 * Attack Skill Trait
 *
 * Enemies are unaffected by the Attack Skill Trait. This means if they have
 * an Attack action, they will always use Attack over and over even if their
 * Attack Skill Trait has been changed. This plugin will change it up so that
 * the Attack skill will comply with whatever their Attack Skill Trait's skill
 * is set to.
 *
 * ---
 *
 * Auto Battle Actor Skill Usage
 *
 * If an actor with Auto Battle has access to a skill but not have any access
 * to that skill's type, that actor will still be able to use the skill during
 * Auto Battle despite the fact that the actor cannot use that skill during
 * manual input.
 *
 * ---
 * 
 * Auto Battle Lock Up
 * 
 * If an auto battle Actor fights against an enemy whose DEF/MDF is too high,
 * they will not use any actions at all. This can cause potential game freezing
 * and softlocks. This plugin will change that and have them default to a
 * regular Attack.
 * 
 * ---
 * 
 * Gamepad Repeat Input
 * 
 * Cleared inputs on gamepads do not have a downtime and will trigger the
 * following input frame. The causes problems with certain RPG Maker MZ menus
 * where the inputs have to be cleared as the next immediate frame will have
 * them inputted again. This plugin changes it so that whenever inputs are
 * cleared, there is a downtime equal to the keyboard clear frames before the
 * gamepad input is registered once more.
 * 
 * ---
 *
 * Move Picture, Origin Differences
 *
 * If a Show Picture event command is made with an Origin setting of
 * "Upper Left" and a Move Picture event command is made afterwards with an
 * Origin setting of "Center", RPG Maker MZ would originally have it instantly
 * jump into the new origin setting without making a clean transition between
 * them. This plugin will create that clean transition between origins.
 *
 * ---
 * 
 * Show Scrolling Text, additional functionality
 * 
 * The event command "Show Scrolling Text" now has additional functionality as
 * long as the VisuStella MZ Core Engine is installed. If the game dev inserts
 * "// Script Call" (without the quotes) inside the scrolling text, then the
 * entirity of the Show Scrolling Text event command will be ran as a giant
 * script call event command.
 * 
 * The reason why this functionality is added is because the "Script..." event
 * command contains only 12 lines maximum. This means for any script call
 * larger than 12 lines of code cannot be done by normal means as each script
 * call is ran as a separate instance.
 * 
 * By repurposing the "Show Scrolling Text" event command to be able to
 * function as an extended "Script..." event command, such a thing is now
 * possible with less hassle and more lines to code with.
 * 
 * This effect does not occur if the Show Scrolling Text event command does not
 * have "// Script Call" in its contents.
 * 
 * ---
 * 
 * Timer Sprite
 * 
 * By default, RPG Maker MZ adds Sprite_Timer into its spriteset, either for
 * maps or for battles. There is one major problem with this: when spritesets
 * are affected by filters, zooms, and/or blurs, this hinders how readable the
 * timer sprite is, making the information perceived by the player to be much
 * harder than it needs to be. The Core Engine adds the sprite to the parent
 * scene instead of the spriteset to ensure it's unobscured by anything else.
 * 
 * ---
 * 
 * Unusable Battle Items
 * 
 * If any party member is able to use an item in battle, then all party members
 * are able to use said item, even if that party member is supposed to be
 * unable to use that item. This is now changed so that battle items are
 * checked on an individual basis and not on a party-wide basis.
 * 
 * ---
 *
 * ============================================================================
 * Major Changes: New Hard-Coded Features
 * ============================================================================
 *
 * This plugin adds some new hard-coded features to RPG Maker MZ's functions.
 * The following is a list of them.
 *
 * ---
 *
 * Scroll-Linked Pictures
 *
 * - If a Parallax has a ! at the start of its filename, it is bound to the map
 * scrolling. The same thing now happens with pictures. If a Picture has a ! at
 * the start of its filename, it is bound to the map's scrolling as well.
 *
 * ---
 *
 * Movement Route Scripts
 *
 * - If code in a Movement Route Script command fails, instead of crashing the
 * game, it will now act as if nothing happened except to display the cause of
 * the error inside the console.
 *
 * ---
 * 
 * Script Call Failsafes
 * 
 * - If code found in Conditional Branches, Control Variables, and/or Script
 * Calls fail to activate, instead of crashing the game, it will now act as if
 * nothing happened except to display the cause of the error inside the
 * console.
 * 
 * ---
 * 
 * Digit Grouping
 * 
 * - There exists an option to change how numbers are displayed and converted
 * in your game. This option can be enabled or disabled by going into the
 * Plugin Manager > VisuMZ_0_OptionsCore > Quality of Life Settings >
 * Digit Grouping and toggling on/off whichever ones you want.
 * 
 * - Digit Grouping will follow the rules of whatever country/locale the Plugin
 * Parameters are set to. If it's to default 'en-US', then 1234567.123456 will
 * become 1,234,567.123456. Set it to 'es-ES' and it becomes 1.234.567,123456
 * instead.
 * 
 * - This uses JavaScript's Number.toLocaleString() function and will therefore
 * follow whatever rules it has. This means if there are trailing zeroes at the
 * end of a decimal, it will cut them off. Numbers like 123.45000 will become
 * 123.45 instead. Excess numbers past 6 decimal places will be rounded. A
 * number like 0.123456789 will become 0.123457 instead.
 * 
 * - Numbers in between [ and ], < and > will be excluded from digit grouping
 * in order for text codes to be preserved accurately. \I[1234] will remain as
 * \I[1234].
 * 
 * - If you would like to enter in a number without digit grouping, surround it
 * with {{ and }}. Typing in {{1234567890}} will yield 1234567890.
 * 
 * ---
 *
 * ============================================================================
 * Notetags
 * ============================================================================
 *
 * The following are notetags that have been added through this plugin. These
 * notetags will not work with your game if this plugin is OFF or not present.
 *
 * === Actors ===
 *
 * Parameter limits can be adjusted in the Plugin Parameters, but this won't
 * lift the ability to change the values of an actor's initial or max level
 * past the editor's limits. Instead, this must be done through the usage of
 * notetags to accomplish the feat.
 *
 * ---
 *
 * <Max Level: x>
 *
 * - Used for: Actor Notetags
 * - Replace 'x' with an integer to determine the actor's max level.
 * - This allows you to go over the database limit of 99.
 * - If this notetag isn't used, default to the actor's database value.
 *
 * ---
 *
 * <Initial Level: x>
 *
 * - Used for: Actor Notetags
 * - Replace 'x' with an integer to determine the actor's initial level.
 * - This allows you to go over the database limit of 99.
 * - If this notetag isn't used, default to the actor's database value.
 *
 * ---
 *
 * === Classes ===
 *
 * As actor levels can now surpass 99 due to the notetag system, there may be
 * some skills you wish certain classes can learn upon reaching higher levels
 * past 99, too.
 *
 * ---
 * 
 * <Learn At Level: x>
 *
 * - Used for: Class Skill Learn Notetags
 * - Replace 'x' with an integer to determine the level this class will learn
 *   the associated skill at.
 * - This allows you to go over the database limit of 99.
 * - If this notetag isn't used, default to the class's database value.
 *
 * ---
 *
 * === Enemies ===
 *
 * Enemies are now given levels. The levels don't do anything except to serve
 * as a container for a number value. This way, levels can be used in damage
 * formulas (ie. a.atk - b.level) without causing any errors. To give enemies
 * levels, use the notetags below. These notetags also allow you to adjust the
 * base parameters, EXP, and Gold past the database limitations.
 *
 * ---
 *
 * <Level: x>
 *
 * - Used for: Enemy Notetags
 * - Replace 'x' with an integer to determine the enemy's level.
 * - If no level is declared, the level will default to 1.
 *
 * ---
 *
 * <param: x>
 *
 * - Used for: Enemy Notetags
 * - Replace 'param' with 'MaxHP', 'MaxMP', 'ATK', 'DEF', 'MAT', 'MDF', 'AGI',
 *   or 'LUK' to determine which parameter to alter.
 * - Replace 'x' with an integer to set an enemy's 'param' base value.
 * - This will overwrite the enemy's database value and can exceed the original
 *   value limitation in the database.
 * - If these notetags aren't used, default to the enemy's database value.
 *
 * ---
 *
 * <EXP: x>
 * <Gold: x>
 *
 * - Used for: Enemy Notetags
 * - Replace 'x' with an integer to determine the enemy's EXP or Gold values.
 * - This will overwrite the enemy's database value and can exceed the original
 *   value limitation in the database.
 * - If these notetags aren't used, default to the enemy's database value.
 *
 * ---
 * 
 * === Animations ===
 * 
 * Animations in RPG Maker MZ are done by Effekseer and the animation system
 * has been revamped. However, the animations are only centered on the targets
 * now, and cannot be attached to the head or foot. Insert these tags into
 * the names of the animations in the database to adjust their positions.
 * 
 * ---
 * 
 * <Head>
 * <Foot>
 * 
 * - Used for: Animation Name Tags
 * - Will set the animation to anchor on top of the sprite (if <Head> is used)
 *   or at the bottom of the sprite (if <Foot> is used).
 * 
 * ---
 * 
 * <Anchor X: x>
 * <Anchor Y: y>
 * 
 * <Anchor: x, y>
 * 
 * - Used for: Animation Name Tags
 * - Will anchor the animation at a specific point within the sprite based on
 *   the 'x' and 'y' values.
 * - Replace 'x' and 'y' with numeric values representing their positions based
 *   on a rate where 0.0 is the furthest left/up (x, y respectively) to 1.0 for
 *   the furthest right/down (x, y respectively).
 * 
 * Examples:
 * 
 * <Anchor X: 0.4>
 * <Anchor Y: 0.8>
 * 
 * <Anchor: 0.2, 0.9>
 * 
 * ---
 * 
 * <Offset X: +x>
 * <Offset X: -x>
 * <Offset Y: +y>
 * <Offset Y: -y>
 * 
 * <Offset: +x, +y>
 * <Offset: -x, -y>
 * 
 * - Used for: Animation Name Tags
 * - Will anchor the animation to be offset by an exact number of pixels.
 * - This does the same the editor does, except it lets you input values
 *   greater than 999 and lower than -999.
 * - Replace 'x' and 'y' with numeric values the exact number of pixels to
 *   offset the animation's x and y coordinates by.
 * 
 * Examples:
 * 
 * <Offset X: +20>
 * <Offset Y: -50>
 * 
 * <Offset: +10, -30>
 * 
 * ---
 *
 * === Quality of Life ===
 *
 * By default, RPG Maker MZ does not offer an encounter step minimum after a
 * random encounter has finished. This means that one step immediately after
 * finishing a battle, the player can immediately enter another battle. The
 * Quality of Life improvement: Minimum Encounter Steps allows you to set a
 * buffer range between battles for the player to have some breathing room.
 *
 * ---
 *
 * <Minimum Encounter Steps: x>
 *
 * - Used for: Map Notetags
 * - Replace 'x' with the minimum number of steps before the player enters a
 *   random encounter on that map.
 * - If this notetag is not used, then the minimum encounter steps for the map
 *   will default to Quality of Life Settings => Encounter Rate Min.
 *
 * ---
 *
 * Tile shadows are automatically added to certain tiles in the map editor.
 * These tile shadows may or may not fit some types of maps. You can turn them
 * on/off with the Quality of Life Plugin Parameters or you can override the
 * settings with the following notetags:
 *
 * ---
 *
 * <Show Tile Shadows>
 * <Hide Tile Shadows>
 *
 * - Used for: Map Notetags
 * - Use the respective notetag for the function you wish to achieve.
 * - If this notetag is not used, then the minimum encounter steps for the map
 *   will default to Quality of Life Settings => No Tile Shadows.
 *
 * ---
 *
 * === Basic, X, and S Parameters ===
 *
 * A battler's parameters, or stats as some devs know them as, are the values
 * that determine how a battler performs. These settings allow you to alter
 * behaviors and give boosts to trait objects in a more controlled manner.
 *
 * ---
 *
 * <param Plus: +x>
 * <param Plus: -x>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Adds or subtracts 'x' to 'param' plus value when calculating totals.
 * - Replace 'param' with 'MaxHP', 'MaxMP', 'ATK', 'DEF', 'MAT', 'MDF', 'AGI',
 *   or 'LUK' to determine which parameter to modify.
 * - Replace 'x' with an integer on how much to adjust the parameter by.
 * - This is used to calculate the 'plus' portion in the Parameter Settings =>
 *   Basic Parameter => Formula.
 *
 * ---
 *
 * <param Rate: x%>
 * <param Rate: x.x>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Changes 'param' rate to 'x' to alter the total 'param' value.
 * - Replace 'param' with 'MaxHP', 'MaxMP', 'ATK', 'DEF', 'MAT', 'MDF', 'AGI',
 *   or 'LUK' to determine which parameter to modify.
 * - Replace 'x' with a percentage (ie. 150%) or a rate (ie. 1.5).
 * - This is used to calculate the 'paramRate' portion in Parameter Settings =>
 *   Basic Parameter => Formula.
 *
 * ---
 *
 * <param Flat: +x>
 * <param Flat: -x>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Adds or subtracts 'x' to 'param' plus value when calculating totals.
 * - Replace 'param' with 'MaxHP', 'MaxMP', 'ATK', 'DEF', 'MAT', 'MDF', 'AGI',
 *   or 'LUK' to determine which parameter to modify.
 * - Replace 'x' with an integer on how much to adjust the parameter by.
 * - This is used to calculate the 'flatBonus' portion in Parameter Settings =>
 *   Basic Parameter => Formula.
 *
 * ---
 *
 * <param Max: x>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Sets max caps for the 'param' to be 'x'. If there are multiple max caps
 *   available to the unit, then the highest will be selected.
 * - Replace 'param' with 'MaxHP', 'MaxMP', 'ATK', 'DEF', 'MAT', 'MDF', 'AGI',
 *   or 'LUK' to determine which parameter to modify.
 * - Replace 'x' with an integer to determine what the max cap should be.
 *
 * ---
 *
 * <xparam Plus: +x%>
 * <xparam Plus: -x%>
 *
 * <xparam Plus: +x.x>
 * <xparam Plus: -x.x>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Adds or subtracts 'x' to 'xparam' plus value when calculating totals.
 * - Replace 'xparam' with 'HIT', 'EVA', 'CRI', 'CEV', 'MEV', 'MRF', 'CNT',
 *   'HRG', 'MRG', 'TRG' to determine which X parameter to modify.
 * - Replace 'x' with a percentage (ie. 150%) or a rate (ie. 1.5).
 * - This is used to calculate the 'plus' portion in the Parameter Settings =>
 *   X Parameter => Formula.
 *
 * ---
 *
 * <xparam Rate: x%>
 * <xparam Rate: x.x>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Changes 'param' rate to 'x' to alter the total 'xparam' value.
 * - Replace 'xparam' with 'HIT', 'EVA', 'CRI', 'CEV', 'MEV', 'MRF', 'CNT',
 *   'HRG', 'MRG', 'TRG' to determine which X parameter to modify.
 * - Replace 'x' with a percentage (ie. 150%) or a rate (ie. 1.5).
 * - This is used to calculate the 'paramRate' portion in Parameter Settings =>
 *   X Parameter => Formula.
 *
 * ---
 *
 * <xparam Flat: +x%>
 * <xparam Flat: -x%>
 *
 * <xparam Flat: +x.x>
 * <xparam Flat: -x.x>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Adds or subtracts 'x' to 'xparam' plus value when calculating totals.
 * - Replace 'xparam' with 'HIT', 'EVA', 'CRI', 'CEV', 'MEV', 'MRF', 'CNT',
 *   'HRG', 'MRG', 'TRG' to determine which X parameter to modify.
 * - Replace 'x' with a percentage (ie. 150%) or a rate (ie. 1.5).
 * - This is used to calculate the 'flatBonus' portion in Parameter Settings =>
 *   X Parameter => Formula.
 *
 * ---
 *
 * <sparam Plus: +x%>
 * <sparam Plus: -x%>
 *
 * <sparam Plus: +x.x>
 * <sparam Plus: -x.x>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Adds or subtracts 'x' to 'sparam' plus value when calculating totals.
 * - Replace 'sparam' with 'TGR', 'GRD', 'REC', 'PHA', 'MCR', 'TCR', 'PDR',
 *   'MDR', 'FDR', 'EXR' to determine which S parameter to modify.
 * - Replace 'x' with a percentage (ie. 150%) or a rate (ie. 1.5).
 * - This is used to calculate the 'plus' portion in the Parameter Settings =>
 *   S Parameter => Formula.
 *
 * ---
 *
 * <sparam Rate: x%>
 * <sparam Rate: x.x>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Changes 'param' rate to 'x' to alter the total 'sparam' value.
 * - Replace 'sparam' with 'TGR', 'GRD', 'REC', 'PHA', 'MCR', 'TCR', 'PDR',
 *   'MDR', 'FDR', 'EXR' to determine which S parameter to modify.
 * - Replace 'x' with a percentage (ie. 150%) or a rate (ie. 1.5).
 * - This is used to calculate the 'paramRate' portion in Parameter Settings =>
 *   S Parameter => Formula.
 *
 * ---
 *
 * <sparam Flat: +x%>
 * <sparam Flat: -x%>
 *
 * <sparam Flat: +x.x>
 * <sparam Flat: -x.x>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Adds or subtracts 'x' to 'sparam' plus value when calculating totals.
 * - Replace 'sparam' with 'TGR', 'GRD', 'REC', 'PHA', 'MCR', 'TCR', 'PDR',
 *   'MDR', 'FDR', 'EXR' to determine which S parameter to modify.
 * - Replace 'x' with a percentage (ie. 150%) or a rate (ie. 1.5).
 * - This is used to calculate the 'flatBonus' portion in Parameter Settings =>
 *   S Parameter => Formula.
 *
 * === JavaScript Notetags: Basic, X, and S Parameters ===
 *
 * The following are notetags made for users with JavaScript knowledge. These
 * notetags are primarily aimed at Basic, X, and S Parameters.
 *
 * ---
 *
 * <JS param Plus: code>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs 'code' to change the 'param' plus value.
 * - Replace 'param' with 'MaxHP', 'MaxMP', 'ATK', 'DEF', 'MAT', 'MDF', 'AGI',
 *   or 'LUK' to determine which parameter to modify.
 * - Replace 'code' with JavaScript code to determine how much to change the
 *   plus amount for the parameter's total calculation.
 * - This is used to calculate the 'plus' portion in the Parameter Settings =>
 *   Basic Parameter => Formula.
 *
 * ---
 *
 * <JS param Rate: code>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs 'code' to change the 'param' rate value.
 * - Replace 'param' with 'MaxHP', 'MaxMP', 'ATK', 'DEF', 'MAT', 'MDF', 'AGI',
 *   or 'LUK' to determine which parameter to modify.
 * - Replace 'code' with JavaScript code to determine how much to change the
 *   param rate amount for the parameter's total calculation.
 * - This is used to calculate the 'paramRate' portion in Parameter Settings =>
 *   Basic Parameter => Formula.
 *
 * ---
 *
 * <JS param Flat: code>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs 'code' to change the 'param' flat value.
 * - Replace 'param' with 'MaxHP', 'MaxMP', 'ATK', 'DEF', 'MAT', 'MDF', 'AGI',
 *   or 'LUK' to determine which parameter to modify.
 * - Replace 'code' with JavaScript code to determine how much to change the
 *   flat bonus amount for the parameter's total calculation.
 * - This is used to calculate the 'flatBonus' portion in Parameter Settings =>
 *   Basic Parameter => Formula.
 *
 * ---
 *
 * <JS param Max: code>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs 'code' to determine what the max cap for 'param' should be. If there
 *   are multiple max caps available to the unit, then the highest is selected.
 * - Replace 'param' with 'MaxHP', 'MaxMP', 'ATK', 'DEF', 'MAT', 'MDF', 'AGI',
 *   or 'LUK' to determine which parameter to modify.
 * - Replace 'code' with JavaScript code to determine the max cap for the
 *   desired parameter.
 *
 * ---
 *
 * <JS xparam Plus: code>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs 'code' to change the 'xparam' plus value.
 * - Replace 'xparam' with 'HIT', 'EVA', 'CRI', 'CEV', 'MEV', 'MRF', 'CNT',
 *   'HRG', 'MRG', 'TRG' to determine which X parameter to modify.
 * - Replace 'code' with JavaScript code to determine how much to change the
 *   plus amount for the X parameter's total calculation.
 * - This is used to calculate the 'plus' portion in the Parameter Settings =>
 *   X Parameter => Formula.
 *
 * ---
 *
 * <JS xparam Rate: code>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs 'code' to change the 'xparam' rate value.
 * - Replace 'xparam' with 'HIT', 'EVA', 'CRI', 'CEV', 'MEV', 'MRF', 'CNT',
 *   'HRG', 'MRG', 'TRG' to determine which X parameter to modify.
 * - Replace 'code' with JavaScript code to determine how much to change the
 *   param rate amount for the X parameter's total calculation.
 * - This is used to calculate the 'paramRate' portion in Parameter Settings =>
 *   X Parameter => Formula.
 *
 * ---
 *
 * <JS xparam Flat: code>
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs 'code' to change the 'xparam' flat value.
 * - Replace 'xparam' with 'HIT', 'EVA', 'CRI', 'CEV', 'MEV', 'MRF', 'CNT',
 *   'HRG', 'MRG', 'TRG' to determine which X parameter to modify.
 * - Replace 'code' with JavaScript code to determine how much to change the
 *   flat bonus amount for the X parameter's total calculation.
 * - This is used to calculate the 'flatBonus' portion in Parameter Settings =>
 *   X Parameter => Formula.
 *
 * ---
 *
 * <JS sparam Plus: code>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs 'code' to change the 'sparam' plus value.
 * - Replace 'sparam' with 'TGR', 'GRD', 'REC', 'PHA', 'MCR', 'TCR', 'PDR',
 *   'MDR', 'FDR', 'EXR' to determine which S parameter to modify.
 * - Replace 'code' with JavaScript code to determine how much to change the
 *   plus amount for the S parameter's total calculation.
 * - This is used to calculate the 'plus' portion in the Parameter Settings =>
 *   S Parameter => Formula.
 *
 * ---
 *
 * <JS sparam Rate: code>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs 'code' to change the 'sparam' rate value.
 * - Replace 'sparam' with 'TGR', 'GRD', 'REC', 'PHA', 'MCR', 'TCR', 'PDR',
 *   'MDR', 'FDR', 'EXR' to determine which S parameter to modify.
 * - Replace 'code' with JavaScript code to determine how much to change the
 *   param rate amount for the S parameter's total calculation.
 * - This is used to calculate the 'paramRate' portion in Parameter Settings =>
 *   S Parameter => Formula.
 *
 * ---
 *
 * <JS sparam Flat: code>
 *
 * - Used for: Actor, Class, Weapon, Armor, Enemy, State Notetags
 * - Runs 'code' to change the 'sparam' flat value.
 * - Replace 'sparam' with 'TGR', 'GRD', 'REC', 'PHA', 'MCR', 'TCR', 'PDR',
 *   'MDR', 'FDR', 'EXR' to determine which S parameter to modify.
 * - Replace 'code' with JavaScript code to determine how much to change the
 *   flat bonus amount for the S parameter's total calculation.
 * - This is used to calculate the 'flatBonus' portion in Parameter Settings =>
 *   S Parameter => Formula.
 *
 * ---
 * 
 * === Battle Setting-Related Notetags ===
 * 
 * These tags will change the settings for battle regardless of how the battle
 * system is set up normally. Insert these tags in either the noteboxes of maps
 * or the names of troops for them to take effect. If both are present for a
 * specific battle, then priority goes to the setting found in the troop name.
 * 
 * ---
 * 
 * <FV>
 * <Front View>
 * <Battle View: FV>
 * <Battle View: Front View>
 * 
 * - Used for: Map Notetags and Troop Name Tags
 * - Changes the perspective of battle to front view for this specific map or
 *   battle.
 * - Make sure you have the enemy image files available in the img/enemies/
 *   folder as they will used instead of the "sv_enemies" graphics.
 * 
 * ---
 * 
 * <SV>
 * <Side View>
 * <Battle View: SV>
 * <Battle View: Side View>
 * 
 * - Used for: Map Notetags and Troop Name Tags
 * - Changes the perspective of battle to side view for this specific map or
 *   battle.
 * - Make sure you have the enemy image files available in the img/sv_enemies/
 *   folder as they will used instead of the "enemies" graphics.
 * - Make sure your actors have "sv_actor" graphics attached to them.
 * 
 * ---
 * 
 * <DTB>
 * <Battle System: DTB>
 * 
 * - Used for: Map Notetags and Troop Name Tags
 * - Changes the battle system to the default battle system (DTB).
 * 
 * ---
 * 
 * <TPB Active>
 * <ATB Active>
 * <Battle System: TPB Active>
 * <Battle System: ATB Active>
 * 
 * <TPB Wait>
 * <ATB Wait>
 * <Battle System: TPB Wait>
 * <Battle System: ATB Wait>
 * 
 * - Used for: Map Notetags and Troop Name Tags
 * - Changes the battle system to the time progress battle system (TPB) or
 *   active turn battle system (ATB) if you have VisuMZ_2_BattleSystemATB
 *   installed for the game project.
 * 
 * ---
 * 
 * <BTB>
 * <Battle System: BTB>
 * 
 * <CTB>
 * <Battle System: CTB>
 * 
 * <FTB>
 * <Battle System: FTB>
 * 
 * <STB>
 * <Battle System: STB>
 * 
 * - Used for: Map Notetags and Troop Name Tags
 * - Changes the battle system to the respective battle system as long as you
 *   have those plugins installed in the current project.
 * 
 * ---
 *
 * ============================================================================
 * Plugin Commands
 * ============================================================================
 *
 * The following are Plugin Commands that come with this plugin. They can be
 * accessed through the Plugin Command event command.
 *
 * ---
 * 
 * === Game Plugin Commands ===
 * 
 * ---
 *
 * Game: Open URL
 * - Opens a website URL from the game.
 *
 *   URL:
 *   - Where do you want to take the player?
 *
 * ---
 * 
 * === Gold Plugin Commands ===
 * 
 * ---
 *
 * Gold: Gain/Lose
 * - Allows you to give/take more gold than the event editor limit.
 *
 *   Value:
 *   - How much gold should the player gain/lose?
 *   - Use negative values to remove gold.
 *
 * ---
 * 
 * === Picture Plugin Commands ===
 * 
 * ---
 *
 * Picture: Easing Type
 * - Changes the easing type to a number of options.
 *
 *   Picture ID:
 *   - Which picture do you wish to apply this easing to?
 *
 *   Easing Type:
 *   - Select which easing type you wish to apply.
 *
 *   Instructions:
 *   - Insert this Plugin Command after a "Move Picture" event command.
 *   - Turn off "Wait for Completion" in the "Move Picture" event.
 *   - You may have to add in your own "Wait" event command after.
 *
 * ---
 * 
 * Picture: Erase All
 * - Erases all pictures on the screen because it's extremely tedious to do it
 *   one by one.
 * 
 * ---
 * 
 * Picture: Erase Range
 * - Erases all pictures within a range of numbers because it's extremely
 *   tedious to do it one by one.
 * 
 *   Starting ID:
 *   - The starting ID of the pictures to erase.
 * 
 *   Ending ID:
 *   - The ending ID of the pictures to erase.
 * 
 * ---
 * 
 * === Screen Shake Plugin Commands ===
 * 
 * ---
 * 
 * Screen Shake: Custom:
 * - Creates a custom screen shake effect and also sets the following uses of
 *   screen shake to this style.
 * 
 *   Shake Style:
 *   - Select shake style type.
 *   - Original
 *   - Random
 *   - Horizontal
 *   - Vertical
 * 
 *   Power:
 *   - Power level for screen shake.
 * 
 *   Speed:
 *   - Speed level for screen shake.
 * 
 *   Duration:
 *   - Duration of screenshake.
 *   - You can use code as well.
 * 
 *   Wait for Completion:
 *   - Wait until completion before moving onto the next event?
 * 
 * ---
 * 
 * === System Plugin Commands ===
 * 
 * ---
 *
 * System: Battle System Change
 * - Switch to a different battle system in-game.
 *
 *   Change To:
 *   - Choose which battle system to switch to.
 *     - Database Default (Use game database setting)
 *     - -
 *     - DTB: Default Turn Battle
 *     - TPB Active: Time Progress Battle (Active)
 *     - TPB Wait: Time Progress Battle (Wait)
 *     - -
 *     - BTB: Brave Turn Battle (Req VisuMZ_2_BattleSystemBTB)
 *     - CTB: Charge Turn Battle (Req VisuMZ_2_BattleSystemCTB)
 *     - OTB: Order Turn Battle (Req VisuMZ_2_BattleSystemOTB)
 *     - STB: Standard Turn Battle (Req VisuMZ_2_BattleSystemSTB)
 *
 * ---
 * 
 * System: Load Images
 * - Allows you to (pre) load up images ahead of time.
 *
 *   img/animations/:
 *   img/battlebacks1/:
 *   img/battlebacks2/:
 *   img/enemies/:
 *   img/faces/:
 *   img/parallaxes/:
 *   img/pictures/:
 *   img/sv_actors/:
 *   img/sv_enemies/:
 *   img/system/:
 *   img/tilesets/:
 *   img/titles1/:
 *   img/titles2/:
 *   - Which files do you wish to load from this directory?
 * 
 * ---
 *
 * System: Main Font Size
 * - Set the game's main font size.
 *
 *   Change To:
 *   - Change the font size to this number.
 *
 * ---
 *
 * System: Side View Battle
 * - Switch between Front View or Side View for battle.
 *
 *   Change To:
 *   - Choose which view type to switch to.
 *
 * ---
 *
 * System: Window Padding
 * - Change the game's window padding amount.
 *
 *   Change To:
 *   - Change the game's standard window padding to this value.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Quality of Life Settings
 * ============================================================================
 *
 * A variety of (optional) settings and changes are added with the Core Engine
 * to improve the quality of life for both the game devs and players alike.
 *
 * ---
 *
 * Play Test
 * 
 *   New Game on Boot:
 *   - Automatically start a new game on Play Test?
 *   - Only enabled during Play Test.
 *
 *   No Play Test Mode:
 *   - Force the game to be out of Play Test mode when play testing.
 * 
 *   Open Console on Boot:
 *   - Open the Debug Console upon booting up your game?
 *   - Only enabled during Play Test.
 *
 *   F6: Toggle Sound:
 *   - F6 Key Function: Turn on all sound to 100% or to 0%, toggling between
 *     the two.
 *   - Only enabled during Play Test.
 *
 *   F7: Toggle Fast Mode:
 *   - F7 Key Function: Toggle fast mode.
 *   - Only enabled during Play Test.
 *
 *   New Game > Common Event:
 *   - Runs a common event each time a new game is started.
 *   - Only enabled during Play Test.
 *
 * ---
 *
 * Digit Grouping
 *
 *   Standard Text:
 *   - Make numbers like 1234567 appear like 1,234,567 for standard text
 *     inside windows?
 *
 *   Ex Text:
 *   - Make numbers like 1234567 appear like 1,234,567 for ex text,
 *     written through drawTextEx (like messages)?
 *
 *   Damage Sprites:
 *   - Make numbers like 1234567 appear like 1,234,567 for in-battle
 *     damage sprites?
 *
 *   Gauge Sprites:
 *   - Make numbers like 1234567 appear like 1,234,567 for visible gauge
 *     sprites such as HP, MP, and TP gauges?
 * 
 *   Country/Locale
 *   - Base the digit grouping on which country/locale?
 *   - This will follow all of the digit grouping rules found here:
 *     https://www.w3schools.com/JSREF/jsref_tolocalestring_number.asp
 *
 * ---
 *
 * Player Benefit
 *
 *   Encounter Rate Min:
 *   - Minimum number of steps the player can take without any
 *     random encounters.
 *
 *   Escape Always:
 *   - If the player wants to escape a battle, let them escape the battle
 *     with 100% chance.
 *
 *   Accuracy Formula:
 *   - Accuracy formula calculation change to
 *     Skill Hit% * (User HIT - Target EVA) for better results.
 *
 *   Accuracy Boost:
 *   - Boost HIT and EVA rates in favor of the player.
 *
 *   Level Up -> Full HP:
 *   Level Up -> Full MP:
 *   - Recovers full HP or MP when an actor levels up.
 *
 * ---
 *
 * Misc
 * 
 *   Anti-Zoom Pictures:
 *   - If on, prevents pictures from being affected by zoom.
 *
 *   Font Shadows:
 *   - If on, text uses shadows instead of outlines.
 *
 *   Font Smoothing:
 *   - If on, smoothes fonts shown in-game.
 *
 *   Key Item Protection:
 *   - If on, prevents Key Items from being able to be sold and from being
 *     able to be consumed.
 *
 *   Modern Controls:
 *   - If on, allows usage of the Home/End buttons.
 *   - Home would scroll to the first item on a list.
 *   - End would scroll to the last item on a list.
 *   - Shift + Up would page up.
 *   - Shift + Down would page down.
 * 
 *   NewGame > CommonEvent:
 *   - Runs a common event each time a new game during any session is started.
 *   - Applies to all types of sessions, play test or not.
 *
 *   No Tile Shadows:
 *   - Removes tile shadows from being displayed in-game.
 *
 *   Pixel Image Rendering:
 *   - If on, pixelates the image rendering (for pixel games).
 *
 *   Require Focus?
 *   - Requires the game to be focused? If the game isn't focused, it will
 *     pause if it's not the active window.
 *
 *   Smart Event Collision:
 *   - Makes events only able to collide with one another if they're
 *    'Same as characters' priority.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Battle System
 * ============================================================================
 * 
 * Choose which battle system to use for your game.
 * 
 * ---
 * 
 *   Database Default (Use game database setting)
 * 
 *   -
 * 
 *   DTB: Default Turn Battle
 *   TPB Active: Time Progress Battle (Active)
 *   TPB Wait: Time Progress Battle (Wait)
 * 
 *   -
 * 
 *   BTB: Brave Turn Battle (Req VisuMZ_2_BattleSystemBTB)
 *   CTB: Charge Turn Battle (Req VisuMZ_2_BattleSystemCTB)
 *   OTB: Order Turn Battle (Req VisuMZ_2_BattleSystemOTB)
 *   STB: Standard Turn Battle (Req VisuMZ_2_BattleSystemSTB)
 * 
 *   -
 * 
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Color Settings
 * ============================================================================
 *
 * These settings allow you, the game dev, to have more control over which
 * colors appear for what conditions found in the game. You can use regular
 * numbers to use the colors predetermined by the game's Window Skin or you
 * can use the #rrggbb format for a hex color code.
 *
 * You can find out what hex codes belong to which color from this website:
 * https://htmlcolorcodes.com/
 *
 * ---
 *
 * Basic Colors
 * - These are colors that almost never change and are used globally throughout
 *   the in-game engine.
 *
 *   Normal:
 *   System:
 *   Crisis:
 *   Death:
 *   Gauge Back:
 *   HP Gauge:
 *   MP Gauge:
 *   MP Cost:
 *   Power Up:
 *   Power Down:
 *   CT Gauge:
 *   TP Gauge:
 *   Pending Color:
 *   EXP Gauge:
 *   MaxLv Gauge:
 *   - Use #rrggbb for custom colors or regular numbers
 *   for text colors from the Window Skin.
 *
 * ---
 *
 * Alpha Colors:
 * - These are colors that have a bit of transparency to them and are specified
 *   by the 'rgba(red, green, blue, alpha)' format.
 * - Replace 'red' with a number between 0-255 (integer).
 * - Replace 'green' with a number between 0-255 (integer).
 * - Replace 'blue' with a number between 0-255 (integer).
 * - Replace 'alpha' with a number between 0 and 1 (decimal).
 * 
 *   Window Font Outline:
 *   Gauge Number Outline:
 *   Dim Color:
 *   Item Back Color:
 *   - Colors with a bit of alpha settings.
 *   - Format rgba(0-255, 0-255, 0-255, 0-1)
 *
 * ---
 *
 * Conditional Colors:
 * - These require a bit of JavaScript knowledge. These determine what colors
 *   to use under which situations and uses such as different values of HP, MP,
 *   TP, for comparing equipment, and determine damage popup colors.
 * 
 *   JS: Actor HP Color:
 *   JS: Actor MP Color:
 *   JS: Actor TP Color:
 *   - Code used for determining what HP, MP, or TP color to use for actors.
 *
 *   JS: Parameter Change:
 *   - Code used for determining whatcolor to use for parameter changes.
 *
 *   JS: Damage Colors:
 *   - Code used for determining what color to use for damage types.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Gold Settings
 * ============================================================================
 *
 * Gold is the main currency in RPG Maker MZ. The settings provided here will
 * determine how Gold appears in the game and certain behaviors Gold has.
 *
 * ---
 *
 * Gold Settings
 *
 *   Gold Max:
 *   - Maximum amount of Gold the party can hold.
 *   - Default 99999999
 *
 *   Gold Font Size:
 *   - Font size used for displaying Gold inside Gold Windows.
 *   - Default: 26
 *
 *   Gold Icon:
 *   - Icon used to represent Gold.
 *   - Use 0 for no icon.
 *
 *   Gold Overlap:
 *   - Text used too much Gold to fit in the window.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Image Loading
 * ============================================================================
 *
 * Not all images are loaded at once in-game. RPG Maker MZ uses asynchronous
 * loading which means images are loaded when needed. This may cause delays in
 * when you want certain images to appear. However, if an image is loaded
 * beforehand, they can be used immediately provided they aren't removed from
 * the image cache.
 *
 * ---
 *
 * Image Loading
 *
 *   img/animations/:
 *   img/battlebacks1/:
 *   img/battlebacks2/:
 *   img/enemies/:
 *   img/faces/:
 *   img/parallaxes/:
 *   img/pictures/:
 *   img/sv_actors/:
 *   img/sv_enemies/:
 *   img/system/:
 *   img/tilesets/:
 *   img/titles1/:
 *   img/titles2/:
 *   - Which files do you wish to load from this directory upon starting
 *     up the game?
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Keyboard Input Settings
 * ============================================================================
 *
 * Settings for the game that utilize keyboard input. These are primarily for
 * the name input scene (Scene_Name) and the number input event command. These
 * settings have only been tested on English keyboards and may or may not be
 * compatible with other languages, so please disable these features if they do
 * not fit in with your game.
 *
 * ---
 * 
 * Controls
 * 
 *   WASD Movement:
 *   - Enables or disables WASD movement for your game project.
 *   - Moves the W page down button to E.
 * 
 *   R Button: Dash Toggle:
 *   - Enables or disables R button as an Always Dash option toggle.
 * 
 * ---
 *
 * Name Input
 * 
 *   Enable?:
 *   - Enables keyboard input for name entry.
 *   - Only tested with English keyboards.
 * 
 *   Default Mode:
 *   - Select default mode when entering the scene.
 *     - Default - Uses Arrow Keys to select letters.
 *     - Keyboard - Uses Keyboard to type in letters.
 * 
 *   QWERTY Layout:
 *   - Uses the QWERTY layout for manual entry.
 * 
 *   Keyboard Message:
 *   - The message displayed when allowing keyboard entry.
 *   - You may use text codes here.
 *
 * ---
 *
 * Number Input
 * 
 *   Enable?:
 *   - Enables keyboard input for number entry.
 *   - Only tested with English keyboards.
 *
 * ---
 * 
 * Button Assist
 * 
 *   Switch to Keyboard:
 *   - Text used to describe the keyboard switch.
 * 
 *   Switch To Manual:
 *   - Text used to describe the manual entry switch.
 * 
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Menu Background Settings
 * ============================================================================
 *
 * These settings in the Plugin Parameters allow you to adjust the background
 * images used for each of the scenes. The images will be taken from the game
 * project folders img/titles1/ and img/titles2/ to load into the game.
 *
 * These settings are only available to scenes found within the Main Menu, the
 * Shop scene, and the Actor Naming scene.
 *
 * ---
 *
 * Menu Background Settings:
 *
 *   Scene_Menu:
 *   Scene_Item:
 *   Scene_Skill:
 *   Scene_Equip:
 *   Scene_Status:
 *   Scene_Options:
 *   Scene_Save:
 *   Scene_Load:
 *   Scene_GameEnd:
 *   Scene_Shop:
 *   Scene_Name:
 *   - Individual background settings for the scene.
 *
 *   Scene_Unlisted
 *   - Individual background settings for any scenes that aren't listed above.
 *
 * ---
 *
 * Background Settings
 *
 *   Snapshop Opacity:
 *   - Snapshot opacity for the scene.
 *
 *   Background 1:
 *   - Filename used for the bottom background image.
 *   - Leave empty if you don't wish to use one.
 *
 *   Background 2:
 *   - Filename used for the upper background image.
 *   - Leave empty if you don't wish to use one.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Menu Button Assist Window
 * ============================================================================
 *
 * In most modern RPG's, there exist small windows on the screen which tell the
 * player what the control schemes are for that scene. This plugin gives you
 * the option to add that window to the menu scenes in the form of a Button
 * Assist Window.
 *
 * ---
 *
 * General
 * 
 *   Enable:
 *   - Enable the Menu Button Assist Window.
 * 
 *   Location:
 *   - Determine the location of the Button Assist Window.
 *   - Requires Plugin Parameters => UI => Side Buttons ON.
 *
 *   Background Type:
 *   - Select background type for this window.
 *
 * ---
 *
 * Text
 * 
 *   Text Format:
 *   - Format on how the buttons are displayed.
 *   - Text codes allowed. %1 - Key, %2 - Text
 * 
 *   Multi-Key Format:
 *   - Format for actions with multiple keys.
 *   - Text codes allowed. %1 - Key 1, %2 - Key 2
 * 
 *   OK Text:
 *   Cancel Text:
 *   Switch Actor Text:
 *   - Default text used to display these various actions.
 *
 * ---
 *
 * Keys
 * 
 *   Key: Unlisted Format:
 *   - If a key is not listed below, use this format.
 *   - Text codes allowed. %1 - Key
 * 
 *   Key: Up:
 *   Key: Down:
 *   Key: Left:
 *   Key: Right:
 *   Key: Shift:
 *   Key: Tab:
 *   Key: A through Z:
 *   - How this key is shown in-game.
 *   - Text codes allowed.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Menu Layout Settings
 * ============================================================================
 *
 * These settings allow you to rearrange the positions of the scenes accessible
 * from the Main Menu, the Shop scene, and the Actor Naming scene. This will
 * require you to have some JavaScript knowledge to make the windows work the
 * way you would like.
 *
 * ---
 *
 * Menu Layout Settings
 *
 *   Scene_Title:
 *   Scene_Menu:
 *   Scene_Item:
 *   Scene_Skill:
 *   Scene_Equip:
 *   Scene_Status:
 *   Scene_Options:
 *   Scene_Save:
 *   Scene_Load:
 *   Scene_GameEnd:
 *   Scene_Shop:
 *   Scene_Name:
 *   - Various options on adjusting the selected scene.
 *
 * ---
 *
 * Scene Window Settings
 *
 *   Background Type:
 *   - Selects the background type for the selected window.
 *   - Window
 *   - Dim
 *   - Transparent
 *
 *   JS: X, Y, W, H
 *   - Code used to determine the dimensions for the selected window.
 *
 * ---
 *
 * Scene_Title Settings
 * - The following are settings unique to Scene_Title.
 *
 * Title Screen
 *
 *   Document Title Format:
 *   - Format to display text in document title.
 *   - %1 - Main Title, %2 - Subtitle, %3 - Version
 *
 *   Subtitle:
 *   - Subtitle to be displayed under the title name.
 *   
 *   Version:
 *   - Version to be display in the title screen corner.
 *   
 *   JS: Draw Title:
 *   - Code used to draw the game title.
 *   
 *   JS: Draw Subtitle:
 *   - Code used to draw the game subtitle.
 *   
 *   JS: Draw Version:
 *   - Code used to draw the game version.
 *   
 *   Button Fade Speed:
 *   - Speed at which the buttons fade in at (1-255).
 *
 * ---
 *
 * Scene_GameEnd Settings
 * - The following are settings unique to Scene_GameEnd.
 *   
 *   Command Window List:
 *   - Window commands used by the title screen.
 *   - Add new commands here.
 *
 * ---
 *
 * Command Window List
 * - This is found under Scene_Title and Scene_GameEnd settings.
 *
 *   Symbol:
 *   - The symbol used for this command.
 * 
 *   STR: Text:
 *   - Displayed text used for this title command.
 *   - If this has a value, ignore the JS: Text version.
 * 
 *   JS: Text:
 *   - JavaScript code used to determine string used for the displayed name.
 * 
 *   JS: Show:
 *   - JavaScript code used to determine if the item is shown or not.
 * 
 *   JS: Enable:
 *   - JavaScript code used to determine if the item is enabled or not.
 * 
 *   JS: Ext:
 *   - JavaScript code used to determine any ext data that should be added.
 * 
 *   JS: Run Code:
 *   - JavaScript code that runs once this command is selected.
 * 
 * ---
 *
 * Title Picture Buttons:
 * - This is found under Scene_Title settings.
 * 
 *   Picture's Filename:
 *   - Filename used for the picture.
 *
 *   Button URL:
 *   - URL for the button to go to upon being clicked.
 *
 *   JS: Position:
 *   - JavaScript code that helps determine the button's Position.
 *
 *   JS: On Load:
 *   - JavaScript code that runs once this button bitmap is loaded.
 *
 *   JS: Run Code:
 *   - JavaScript code that runs once this button is pressed.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Parameter Settings
 * ============================================================================
 *
 * A battler's parameters, or stats as some devs know them as, are the values
 * that determine how a battler performs. These settings allow you to alter
 * their behaviors and give boosts to trait objects in a controlled manner.
 *
 * ---
 *
 * Parameter Settings
 *
 *   Displayed Parameters
 *   - A list of the parameters that will be displayed in-game.
 *   - Shown in the Equip Menu.
 *   - Shown in the Status Menu.
 *
 *   Extended Parameters
 *   - The list shown in extended scenes (for other VisuStella plugins).
 *
 * ---
 *
 * === Basic Parameters ===
 *
 * MHP - MaxHP
 * - This is the maximum health points value. The amount of health points (HP)
 * a battler has determines whether or not the battler is in a living state or
 * a dead state. If the HP value is above 0, then the battler is living. If it
 * is 0 or below, the battler is in a dead state unless the battler has a way
 * to counteract death (usually through immortality). When the battler takes
 * damage, it is usually dealt to the HP value and reduces it. If the battler
 * is healed, then the HP value is increased. The MaxHP value determines what's
 * the maximum amount the HP value can be held at, meaning the battler cannot
 * be healed past that point.
 *
 * MMP - MaxMP
 * - This is the maximum magic points value. Magic points (MP) are typically
 * used for the cost of skills and spells in battle. If the battler has enough
 * MP to fit the cost of the said skill, the battler is able to use the said
 * skill provided that all of the skill's other conditions are met. If not, the
 * battler is then unable to use the skill. Upon using a skill that costs MP,
 * the battler's MP is reduced. However, the battler's MP can be recovered and
 * results in a gain of MP. The MaxMP value determines what is the maximum
 * amount the MP value can be held at, meaning the battler cannot recover MP
 * past the MaxMP value.
 *
 * ATK - Attack
 * - This is the attack value of the battler. By default, this stat is used for
 * the purpose of damage calculations only, and is typically used to represent
 * the battler's physical attack power. Given normal damage formulas, higher
 * values mean higher damage output for physical attacks.
 *
 * DEF - Defense
 * - This is the defense value of the battler. By default, this stat is used
 * for the purpose of damage calculations only, and is typically used to
 * represent the battler's physical defense. Given normal damage formulas,
 * higher values mean less damage received from physical attacks.
 *
 * MAT - Magic Attack
 * - This is the magic attack value of the battler. By default, this stat is
 * used for the purpose of damage calculations only, and is typically used to
 * represent the battler's magical attack power. Given normal damage formulas,
 * higher values mean higher damage output for magical attacks.
 *
 * MDF - Magic Defense
 * - This is the magic defense value of the battler. By default, this stat is
 * used for the purpose of damage calculations only, and is typically used to
 * represent the battler's magical defense. Given normal damage formulas,
 * higher values mean less damage received from magical attacks.
 *
 * AGI - Agility
 * - This is the agility value of the battler. By default, this stat is used to
 * determine battler's position in the battle turn's order. Given a normal turn
 * calculation formula, the higher the value, the faster the battler is, and
 * the more likely the battler will have its turn earlier in a turn.
 *
 * LUK - Luck
 * - This is the luck value of the battler. By default, this stat is used to
 * affect the success rate of states, buffs, and debuffs applied by the battler
 * and received by the battler. If the user has a higher LUK value, the state,
 * buff, or debuff is more likely to succeed. If the target has a higher LUK
 * value, then the state, buff, or debuff is less likely to succeed.
 *
 * ---
 *
 * Basic Parameters
 *
 *   HP Crisis Rate:
 *   - HP Ratio at which a battler can be considered in crisis mode.
 *
 *   JS: Formula:
 *   - Formula used to determine the total value all 8 basic parameters:
 *   - MaxHP, MaxMP, ATK, DEF, MAT, MDF, AGI, LUK.
 *
 * Parameter Caps:
 *
 *   MaxHP Cap:
 *   MaxMP Cap:
 *   ATK Cap:
 *   DEF Cap:
 *   MAT Cap:
 *   MDF Cap:
 *   AGI Cap:
 *   LUK Cap:
 *   - Formula used to determine the selected parameter's cap.
 *
 * ---
 *
 * === X Parameters ===
 *
 * HIT - Hit Rate%
 * - This determines the physical hit success rate of the any physical action.
 * All physical attacks make a check through the HIT rate to see if the attack
 * will connect. If the HIT value passes the randomizer check, the attack will
 * connect. If the HIT value fails to pass the randomizer check, the attack
 * will be considered a MISS.
 *
 * EVA - Evasion Rate%
 * - This determines the physical evasion rate against any incoming physical
 * actions. If the HIT value passes, the action is then passed to the EVA check
 * through a randomizer check. If the randomizer check passes, the physical
 * attack is evaded and will fail to connect. If the randomizer check passes,
 * the attempt to evade the action will fail and the action connects.
 *
 * CRI - Critical Hit Rate%
 * - Any actions that enable Critical Hits will make a randomizer check with
 * this number. If the randomizer check passes, extra damage will be carried
 * out by the initiated action. If the randomizer check fails, no extra damage
 * will be added upon the action.
 *
 * CEV - Critical Evasion Rate%
 * - This value is put against the Critical Hit Rate% in a multiplicative rate.
 * If the Critical Hit Rate is 90% and the Critical Evasion Rate is
 * 20%, then the randomizer check will make a check against 72% as the values
 * are calculated by the source code as CRI * (1 - CEV), therefore, with values
 * as 0.90 * (1 - 0.20) === 0.72.
 *
 * MEV - Magic Evasion Rate%
 * - Where EVA is the evasion rate against physical actions, MEV is the evasion
 * rate against magical actions. As there is not magical version of HIT, the
 * MEV value will always be bit against when a magical action is initiated. If
 * the randomizer check passes for MEV, the magical action will not connect. If
 * the randomizer check fails for MEV, the magical action will connect.
 *
 * MRF - Magic Reflect Rate%
 * - If a magical action connects and passes, there is a chance the magical
 * action can be bounced back to the caster. That chance is the Magic Reflect
 * Rate. If the randomizer check for the Magic Reflect Rate passes, then the
 * magical action is bounced back to the caster, ignoring the caster's Magic
 * Evasion Rate. If the randomizer check for the Magic Reflect Rate fails, then
 * the magical action will connect with its target.
 *
 * CNT - Counter Attack Rate%
 * - If a physical action connects and passes, there is a chance the physical
 * action can be avoided and a counter attack made by the user will land on the
 * attacking unit. This is the Counter Attack Rate. If the randomizer check for
 * the Counter Attack Rate passes, the physical action is evaded and the target
 * will counter attack the user. If the randomizer check fails, the physical
 * action will connect to the target.
 *
 * HRG - HP% Regeneration
 * - During a battler's regeneration phase, the battler will regenerate this
 * percentage of its MaxHP as gained HP with a 100% success rate.
 *
 * MRG - MP% Regeneration
 * - During a battler's regeneration phase, the battler will regenerate this
 * percentage of its MaxMP as gained MP with a 100% success rate.
 *
 * TRG - TP% Regeneration
 * - During a battler's regeneration phase, the battler will regenerate this
 * percentage of its MaxTP as gained TP with a 100% success rate.
 *
 * ---
 *
 * X Parameters
 *
 *   JS: Formula:
 *   - Formula used to determine the total value all 10 X parameters:
 *   - HIT, EVA, CRI, CEV, MEV, MRF, CNT, HRG, MRG, TRG.
 *
 * Vocabulary
 *
 *   HIT:
 *   EVA:
 *   CRI:
 *   CEV:
 *   MEV:
 *   MRF:
 *   CNT:
 *   HRG:
 *   MRG:
 *   TRG:
 *   - In-game vocabulary used for the selected X Parameter.
 *
 * ---
 *
 * === S Parameters ===
 *
 * TGR - Target Rate
 * - Against the standard enemy, the Target Rate value determines the odds of
 * an enemy specifically targeting the user for a single target attack. At 0%,
 * the enemy will almost never target the user. At 100%, it will have normal
 * targeting opportunity. At 100%+, the user will have an increased chance of
 * being targeted.
 * *NOTE: For those using the Battle A.I. Core, any actions that have specific
 * target conditions will bypass the TGR rate.
 *
 * GRD - Guard Effect
 * - This is the effectiveness of guarding. This affects the guard divisor
 * value of 2. At 100% GRD, damage will become 'damage / (2 * 1.00)'. At 50%
 * GRD, damage will become 'damage / (2 * 0.50)'. At 200% GRD, damage will
 * become 'damage / (2 * 2.00)' and so forth.
 *
 * REC - Recovery Effect
 * - This is how effective heals are towards the user. The higher the REC rate,
 * the more the user is healed. If a spell were to heal for 100 and the user
 * has 300% REC, then the user is healed for 300 instead.
 *
 * PHA - Pharmacology
 * - This is how effective items are when used by the user. The higher the PHA
 * rate, the more effective the item effect. If the user is using a Potion that
 * recovers 100% on a target ally and the user has 300% PHA, then the target
 * ally will receive healing for 300 instead.
 *
 * MCR - MP Cost Rate
 * - This rate affects how much MP skills with an MP Cost will require to use.
 * If the user has 100% MCR, then the MP Cost will be standard. If the user has
 * 50% MCR, then all skills that cost MP will cost only half the required MP.
 * If the user has 200% MCR, then all skills will cost 200% their MP cost.
 *
 * TCR - TP Charge Rate
 * - This rate affects how much TP skills with an TP will charge when gaining
 * TP through various actions. At 100%, TP will charge normally. At 50%, TP
 * will charge at half speed. At 200%, TP will charge twice as fast.
 *
 * PDR - Physical Damage Rate
 * - This rate affects how much damage the user will take from physical damage.
 * If the user has 100% PDR, then the user takes the normal amount. If the user
 * has 50% PDR, then all physical damage dealt to the user is halved. If the
 * user has 200% PDR, then all physical damage dealt to the user is doubled.
 *
 * MDR - Magical Damage Rate
 * - This rate affects how much damage the user will take from magical damage.
 * If the user has 100% MDR, then the user takes the normal amount. If the user
 * has 50% MDR, then all magical damage dealt to the user is halved. If the
 * user has 200% MDR, then all magical damage dealt to the user is doubled.
 *
 * FDR - Floor Damage Rate
 * - On the field map, this alters how much damage the user will take when the
 * player walks over a tile that damages the party. The FDR value only affects
 * the damage dealt to the particular actor and not the whole party. If FDR is
 * at 100%, then the user takes the full damage. If FDR is at 50%, then only
 * half of the damage goes through. If FDR is at 200%, then floor damage is
 * doubled for that actor.
 *
 * EXR - Experience Rate
 * - This determines the amount of experience gain the user whenever the user
 * gains any kind of EXP. At 100% EXR, the rate of experience gain is normal.
 * At 50%, the experience gain is halved. At 200%, the experience gain for the
 * user is doubled.
 *
 * ---
 *
 * S Parameters
 *
 *   JS: Formula
 *   - Formula used to determine the total value all 10 S parameters:
 *   - TGR, GRD, REC, PHA, MCR, TCR, PDR, MDR, FDR, EXR.
 *
 * Vocabulary
 *
 *   TGR:
 *   GRD:
 *   REC:
 *   PHA:
 *   MCR:
 *   TCR:
 *   PDR:
 *   MDR:
 *   FDR:
 *   EXR:
 *   - In-game vocabulary used for the selected S Parameter.
 *
 * ---
 *
 * Icons
 * 
 *   Draw Icons?
 *   - Draw icons next to parameter names?
 *
 *   MaxHP, MaxMP, ATK, DEF, MAT, MDF, AGI, LUK:
 *   HIT, EVA, CRI, CEV, MEV, MRF, CNT, HRG, MRG, TRG:
 *   TGR, GRD, REC, PHA, MCR, TCR, PDR, MDR, FDR, EXR:
 *   - Icon used for the selected parameter.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Custom Parameters Settings
 * ============================================================================
 *
 * As of version 1.07, you can add Custom Parameters to your game if RPG Maker
 * MZ's default set of parameters isn't enough for you. These parameters can
 * have variable functionality depending on how you code it. More importantly,
 * these are compatible with the VisuStella MZ menus and the VisuStella Core
 * Engine's Parameters settings.
 * 
 * For clarification, these settings do NOT create brand-new parameters for you
 * to use and add to your game nor are the bonuses supported by other plugins
 * in the VisuStella MZ library. These settings exist to function as a bridge
 * for non-VisuStella MZ plugins that have created their own parameter values
 * and to show them inside VisuStella menus.
 *
 * ---
 *
 * Custom Parameter
 * 
 *   Parameter Name:
 *   - What's the parameter's name?
 *   - Used for VisuStella MZ menus.
 * 
 *   Abbreviation:
 *   - What abbreviation do you want to use for the parameter?
 *   - Do not use special characters. Avoid numbers if possible.
 * 
 *   Icon:
 *   - What icon do you want to use to represent this parameter?
 *   - Used for VisuStella MZ menus.
 * 
 *   Type:
 *   - What kind of number value will be returned with this parameter?
 *     - Integer (Whole Numbers Only)
 *     - Float (Decimals are Allowed)
 * 
 *   JS: Value:
 *   - Run this code when this parameter is to be returned.
 *
 * ---
 * 
 * Instructions on Adding Custom Parameters to VisuStella Menus
 * 
 * In the Core Engine and Elements and Status Menu Core plugins, there are
 * plugin parameter fields for you to insert the parameters you want displayed
 * and visible to the player.
 * 
 * Insert in those the abbreviation of the custom parameter. For example, if
 * you want to add the "Strength" custom parameter and the abbreviation is
 * "str", then add "str" to the Core Engine/Elements and Status Menu Core's
 * plugin parameter field for "Strength" to appear in-game. Case does not
 * matter here so you can insert "str" or "STR" and it will register all the
 * same to make them appear in-game.
 * 
 * ---
 * 
 * Instructions on Using Custom Parameters as Mechanics
 * 
 * If you want to use a custom parameter in, say, a damage formula, refer to
 * the abbreviation you have set for the custom parameter. For example, if you
 * want to call upon the "Strength" custom parameter's value and its set
 * abbreviation is "str", then refer to it as such. This is case sensitive.
 * 
 * An example damage formula would be something like the following if using
 * "str" for "Strength" and "con" for "Constitution":
 * 
 *   a.str - b.con
 * 
 * These values are attached to the Game_Battlerbase prototype class.
 * 
 * ---
 * 
 * Instructions on Setting Custom Parameter Values
 * 
 * This requires JavaScript knowledge. There is no way around it. Whatever code
 * you insert into the "JS: Value" field will return the value desired. The
 * 'user' variable will refer to the Game_Battlerbase prototype object in which
 * the information is to be drawn from.
 * 
 * Depending on the "type" you've set for the Custom Parameter, the returned
 * value will be rounded using Math.round for integers and left alone if set as
 * a float number.
 * 
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Screen Shake Settings
 * ============================================================================
 *
 * Get more screen shake effects into your game!
 * 
 * These effects have been added by Aries of Sheratan!
 *
 * ---
 *
 * Settings
 * 
 *   Default Style:
 *   - The default style used for screen shakes.
 *   - Original
 *   - Random
 *   - Horizontal
 *   - Vertical
 * 
 *   JS: Original Style:
 *   JS: Random Style
 *   JS: Horizontal Style
 *   JS: Vertical Style
 *   - This code gives you control over screen shake for this screen
 *     shake style.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Title Command List Settings
 * ============================================================================
 *
 * This plugin parameter allows you to adjust the commands that appear on the
 * title screen. Some JavaScript knowledge is needed.
 *
 * ---
 *
 * Title Command
 * 
 *   Symbol:
 *   - The symbol used for this command.
 * 
 *   STR: Text:
 *   - Displayed text used for this title command.
 *   - If this has a value, ignore the JS: Text version.
 * 
 *   JS: Text:
 *   - JavaScript code used to determine string used for the displayed name.
 * 
 *   JS: Show:
 *   - JavaScript code used to determine if the item is shown or not.
 * 
 *   JS: Enable:
 *   - JavaScript code used to determine if the item is enabled or not.
 * 
 *   JS: Ext:
 *   - JavaScript code used to determine any ext data that should be added.
 * 
 *   JS: Run Code:
 *   - JavaScript code that runs once this command is selected.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Title Picture Buttons Settings
 * ============================================================================
 *
 * These allow you to insert picture buttons on your title screen that can
 * send users to various links on the internet when clicked.
 *
 * ---
 *
 * Settings
 * 
 *   Picture's Filename:
 *   - Filename used for the picture.
 * 
 *   Button URL:
 *   - URL for the button to go to upon being clicked.
 * 
 *   JS: Position:
 *   - JavaScript code that helps determine the button's Position.
 * 
 *   JS: On Load:
 *   - JavaScript code that runs once this button bitmap is loaded.
 * 
 *   JS: Run Code:
 *   - JavaScript code that runs once this button is pressed.
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: UI Settings
 * ============================================================================
 *
 * In previous iterations of RPG Maker, the Core Engine would allow you to
 * change the screen resolution. In MZ, that functionality is provided by
 * default but a number of UI settings still remain. These settings allow you
 * adjust how certain in-game objects and menus are displayed.
 *
 * ---
 *
 * UI Area
 *
 *   Fade Speed:
 *   - Default fade speed for transitions.
 *
 *   Box Margin:
 *   - Set the margin in pixels for the screen borders.
 *
 *   Command Window Width:
 *   - Sets the width for standard Command Windows.
 *
 *   Bottom Help Window:
 *   - Put the Help Window at the bottom of the screen?
 *
 *   Right Aligned Menus:
 *   - Put most command windows to the right side of the screen.
 *
 *   Show Buttons:
 *   - Show clickable buttons in your game?
 * 
 *     Show Cancel Button:
 *     Show Menu Button:
 *     Show Page Up/Down:
 *     Show Number Buttons:
 *     - Show/hide these respective buttons if the above is enabled.
 *     - If 'Show Buttons' is false, these will be hidden no matter what.
 *
 *   Button Area Height:
 *   - Sets the height for the button area.
 *
 *   Bottom Buttons:
 *   - Put the buttons at the bottom of the screen?
 *
 *   Side Buttons:
 *   - Push buttons to the side of the UI if there is room.
 *
 * ---
 *
 * Larger Resolutions
 * 
 *   Reposition Actors:
 *   - Update the position of actors in battle if the screen resolution
 *     has changed to become larger than 816x624.
 *   - Ignore if using the VisuStella MZ Battle Core.
 *   - When using the VisuStella MZ Battle Core, adjust the position through
 *     Battle Core > Parameters > Actor Battler Settings > JS: Home Position
 *
 *   Reposition Enemies:
 *   - Update the position of enemies in battle if the screen resolution
 *     has changed to become larger than 816x624.
 *
 * ---
 *
 * Menu Objects
 *
 *   Level -> EXP Gauge:
 *   - Draw an EXP Gauge under the drawn level.
 *
 *   Parameter Arrow:
 *   - The arrow used to show changes in the parameter values.
 *
 * ---
 *
 * Text Code Support
 *
 *   Class Names:
 *   - Make class names support text codes?
 *
 *   Nicknames:
 *   - Make nicknames support text codes?
 *
 * ---
 *
 * ============================================================================
 * Plugin Parameters: Window Settings
 * ============================================================================
 *
 * Adjust the default settings of the windows in-game. This ranges from things
 * such as the line height (to better fit your font size) to the opacity level
 * (to fit your window skins).
 *
 * ---
 *
 * Window Defaults
 * 
 *   Line Height:
 *   - Default line height used for standard windows.
 * 
 *   Item Height Padding:
 *   - Default padding for selectable items.
 * 
 *   Item Padding:
 *   - Default line padding used for standard windows.
 * 
 *   Back Opacity:
 *   - Default back opacity used for standard windows.
 * 
 *   Translucent Opacity:
 *   - Default translucent opacity used for standard windows.
 * 
 *   Window Opening Speed:
 *   - Default open speed used for standard windows.
 *   - Default: 32 (Use a number between 0-255)
 * 
 *   Column Spacing:
 *   - Default column spacing for selectable windows.
 *   - Default: 8
 * 
 *   Row Spacing:
 *   - Default row spacing for selectable windows.
 *   - Default: 4
 *
 * ---
 * 
 * Selectable Items:
 * 
 *   Show Background?:
 *   - Selectable menu items have dark boxes behind them. Show them?
 * 
 *   Item Height Padding:
 *   - Default padding for selectable items.
 * 
 *   JS: Draw Background:
 *   - Code used to draw the background rectangle behind clickable menu objects
 * 
 * ---
 *
 * ============================================================================
 * Plugin Parameters: JS: Quick Functions
 * ============================================================================
 * 
 * WARNING: This feature is highly experimental! Use it at your own risk!
 * 
 * JavaScript Quick Functions allow you to quickly declare functions in the
 * global namespace for ease of access. It's so that these functions can be
 * used in Script Calls, Control Variable Script Inputs, Conditional Branch
 * Script Inputs, Damage Formulas, and more.
 * 
 * ---
 * 
 * JS: Quick Function
 * 
 *   Function Name:
 *   - The function's name in the global namespace.
 *   - Will not overwrite functions/variables of the same name.
 * 
 *   JS: Code:
 *   - Run this code when using the function.
 * 
 * ---
 * 
 * If you have a Function Name of "Example", then typing "Example()" in a
 * Script Call, Conditional Branch Script Input, or similar field will yield
 * whatever the code is instructed to return.
 * 
 * If a function or variable of a similar name already exists in the global
 * namespace, then the quick function will be ignored and not created.
 * 
 * If a quick function contains bad code that would otherwise crash the game,
 * a fail safe has been implemented to prevent it from doing so, display an
 * error log, and then return a 0 value.
 * 
 * ---
 *
 * ============================================================================
 * Terms of Use
 * ============================================================================
 *
 * 1. These plugins may be used in free or commercial games provided that they
 * have been acquired through legitimate means at VisuStella.com and/or any
 * other official approved VisuStella sources. Exceptions and special
 * circumstances that may prohibit usage will be listed on VisuStella.com.
 * 
 * 2. All of the listed coders found in the Credits section of this plugin must
 * be given credit in your games or credited as a collective under the name:
 * "VisuStella".
 * 
 * 3. You may edit the source code to suit your needs, so long as you do not
 * claim the source code belongs to you. VisuStella also does not take
 * responsibility for the plugin if any changes have been made to the plugin's
 * code, nor does VisuStella take responsibility for user-provided custom code
 * used for custom control effects including advanced JavaScript notetags
 * and/or plugin parameters that allow custom JavaScript code.
 * 
 * 4. You may NOT redistribute these plugins nor take code from this plugin to
 * use as your own. These plugins and their code are only to be downloaded from
 * VisuStella.com and other official/approved VisuStella sources. A list of
 * official/approved sources can also be found on VisuStella.com.
 *
 * 5. VisuStella is not responsible for problems found in your game due to
 * unintended usage, incompatibility problems with plugins outside of the
 * VisuStella MZ library, plugin versions that aren't up to date, nor
 * responsible for the proper working of compatibility patches made by any
 * third parties. VisuStella is not responsible for errors caused by any
 * user-provided custom code used for custom control effects including advanced
 * JavaScript notetags and/or plugin parameters that allow JavaScript code.
 *
 * 6. If a compatibility patch needs to be made through a third party that is
 * unaffiliated with VisuStella that involves using code from the VisuStella MZ
 * library, contact must be made with a member from VisuStella and have it
 * approved. The patch would be placed on VisuStella.com as a free download
 * to the public. Such patches cannot be sold for monetary gain, including
 * commissions, crowdfunding, and/or donations.
 *
 * ============================================================================
 * Credits
 * ============================================================================
 * 
 * If you are using this plugin, credit the following people in your game:
 *
 * Team VisuStella
 * * Yanfly
 * * Arisu
 * * Olivia
 * * Irina
 *
 * ============================================================================
 * Changelog
 * ============================================================================
 * 
 * Version 1.26: February 19, 2021
 * * Bug Fixes!
 * ** Certain Plugin Parameters no longer have settings that restrict them to
 *    a maximum of 1. Fix made by Arisu.
 * * Feature Update!
 * ** Changed the default value for a New Game > Common Event upon Play Testing
 *    to 0 to prevent confusion. Update made by Arisu.
 * 
 * Version 1.25: February 5, 2021
 * * Documentation Update!
 * ** Help file updated for new features.
 * * New Features!
 * ** Show Scrolling Text, additional functionality added by Arisu
 * *** The event command "Show Scrolling Text" now has additional functionality
 *     as long as the VisuStella MZ Core Engine is installed. If the game dev
 *     inserts "// Script Call" (without the quotes) inside the scrolling text,
 *     then the entirity of the Show Scrolling Text event command will be ran
 *     as a giant script call event command.
 * *** The reason why this functionality is added is because the "Script..."
 *     event command contains only 12 lines maximum. This means for any script
 *     call larger than 12 lines of code cannot be done by normal means as each
 *     script call is ran as a separate instance.
 * *** By repurposing the "Show Scrolling Text" event command to be able to
 *     function as an extended "Script..." event command, such a thing is now
 *     possible with less hassle and more lines to code with.
 * *** This effect does not occur if the Show Scrolling Text event command does
 *     not have "// Script Call" in its contents.
 * 
 * Version 1.24: January 29, 2021
 * * Documentation Update!
 * ** Plugin Parameters: Custom Parameters Settings added the following note:
 * *** For clarification, these settings do NOT create brand-new parameters for
 *     you to use and add to your game nor are the bonuses supported by other
 *     plugins in the VisuStella MZ library. These settings exist to function
 *     as a bridge for non-VisuStella MZ plugins that have created their own
 *     parameter values and to show them inside VisuStella menus.
 * * Feature Update!
 * ** Default JS Plugin Parameter for the Title Command: "Shutdown" now has a
 *    note in it that reads: "Do NOT use this command with mobile devices or
 *    browser games. All it does is cause the game to display a blank, black
 *    canvas which the player is unable to do anything with. It does NOT force
 *    close the browser tab nor the app."
 * *** This is also why this command is disabled by default for any non-NodeJS
 *     client deployed game versions.
 * ** Disabled some bug fixes made by the Core Engine for the default RMMZ code
 *    base since the 1.1.1 version now contains those very same fixes.
 * 
 * Version 1.23: January 22, 2021
 * * Optimization Update!
 * ** Plugin should run more optimized.
 * 
 * Version 1.22: January 15, 2021
 * * Documentation Update!
 * ** Added documentation for new RPG Maker MZ bug fixes!
 * * Bug Fixes!
 * ** RPG Maker MZ Bug: Sprite_Timer is added to the spriteset for the parent
 *    scene, making it affected by any filers, zooms, and/or blurs, hindering
 *    its readability.
 * 
 * Version 1.21: January 8, 2021
 * * Documentation Update!
 * ** Added documentation for new feature(s)!
 * * New Features!
 * ** New Plugin Parameters added by Arisu:
 * *** Plugin Parameters > Keyboard Input > Controls > WASD Movement
 * *** Plugin Parameters > Keyboard Input > Controls > R Button: Dash Toggle
 * 
 * Version 1.20: January 1, 2021
 * * Compatibility Update!
 * ** Added compatibility functionality for future plugins.
 * 
 * Version 1.19: December 25, 2020
 * * Documentation Update!
 * ** Added documentation for new feature(s) and feature updates!
 * * Bug Fixes!
 * ** Fixed typo inside of the comments inside the JS: Quick Functions.
 * * Feature Update!
 * ** Plugin Parameters > Color Settings > Outline Color is now renamed to
 *    Font Outline.
 * * New Features!
 * ** New Plugin Parameters added by Shaz!
 * *** Plugin Parameters > Color Settings > Gauge Number Outline
 * 
 * Version 1.18: December 18, 2020
 * * Bug Fixes!
 * ** Compatible string text from the Items and Equips Core will no longer
 *    register MaxHP and MaxMP as percentile values for the info window.
 * ** RPG Maker MZ Bug: Gamepads no longer go rapidfire after a cleared input.
 *    There is now a period of delay for gamepads after an input clear.
 * ** RPG Maker MZ Bug: Unusable items on an individual-actor basis will no
 *    longer be overwritten by party-based usability for battle. Fix by Yanfly.
 * ** RPG Maker MV animations will no longer crash for unplayable sound
 *    effects. Fix made by Yanfly.
 * * Compatibility Update!
 * ** Plugins should be more compatible with one another.
 * * Documentation Update!
 * ** Added documentation for new feature(s)!
 * ** Added documentation for new RPG Maker MZ bug fixes!
 * * New Features!
 * ** New Plugin Parameters added by Yanfly!
 * *** Plugin Parameters > Button Assist > Key: Shift
 * *** Plugin Parameters > Button Assist > Key: Tab
 * **** These let you assign text codes to the Shift and Tab buttons for the
 *      Button Assist windows.
 * *** Plugin Parameters > QoL Settings > Misc > NewGame > CommonEvent
 * **** For an all version (including non-play test) common event to start new
 *      games with.
 * 
 * Version 1.17: December 11, 2020
 * * Compatibility Update!
 * ** Added compatibility functionality for future plugins.
 * 
 * Version 1.16: December 4, 2020
 * * Compatibility Update!
 * ** Plugins should be more compatible with one another.
 * * Documentation Update!
 * ** Added documentation for new feature(s)!
 * * Feature Update!
 * ** Button Assist Window for the change name scene will now default to "Tab"
 *    for switching between both modes. Update made by Yanfly.
 * * New Features!
 * ** New Plugin Parameter added by Yanfly:
 * *** Plugin Parameters > Keyboard Input > Default Mode
 * **** Select default mode when entering the scene.
 * 
 * Version 1.15: November 29, 2020
 * * Bug Fixes!
 * ** Pressing "Enter" in the change name scene while the actor's name is
 *    completely empty will no longer result in endless buzzer sounds. Fix made
 *    by Arisu.
 * * Documentation Update!
 * ** Added documentation for new feature(s)!
 * * Feature Update!
 * ** For the name change scene, the "Tab" key now also lets the user switch
 *    between the two modes. Update made by Yanfly.
 * * New Features!
 * ** Two new plugin parameters added to Keyboard Input:
 * *** "Switch To Keyboard" and "Switch To Manual"
 * **** These determine the text used for the button assist window when
 *      switching between the two modes. Update made by Yanfly.
 * **** Button Assist window now takes into consideration for these texts.
 * * Optimization Update!
 * ** Plugin should run more optimized.
 * 
 * Version 1.14: November 22, 2020
 * * Documentation Update!
 * ** Added documentation for new feature(s)!
 * * New Features!
 * ** New Plugin Command added by Yanfly!
 * *** System: Load Images
 * **** Allows you to (pre) load up images ahead of time.
 * 
 * Version 1.13: November 15, 2020
 * * Optimization Update!
 * ** Plugin should run more optimized.
 * 
 * Version 1.12: November 8, 2020
 * * Compatibility Update!
 * ** Plugins should be more compatible with one another.
 * * Documentation Update!
 * ** Added documentation for new feature(s)!
 * * Feature Update!
 * ** Screen Shake Plugin Parameters and JS: Quick Function Plugin Parameters
 *    have been taken off experimental status.
 * * New Features!
 * ** New plugin parameters added by Arisu.
 * *** Plugin Parameters > Keyboard Input
 * **** Settings for the game that utilize keyboard input. These are primarily
 *      for the name input scene (Scene_Name) and the number input event
 *      command. These settings have only been tested on English keyboards and
 *      may or may not be compatible with other languages, so please disable
 *      these features if they do not fit in with your game.
 * 
 * Version 1.11: November 1, 2020
 * * Compatibility Update!
 * ** Plugins should be more compatible with one another.
 * * Feature Update!
 * ** Bitmap smoothing now takes into consideration for rounding coordinates.
 *    Update made by Irina.
 * 
 * Version 1.10: October 25, 2020
 * * Feature Update!
 * ** Sprite animation location now adjusts position relative to the sprite's
 *    scale, too. Update made by Arisu.
 *
 * Version 1.09: October 18, 2020
 * * Bug Fixes!
 * ** RPG Maker MZ Bug: Auto Battle Lock Up. Fixed by Yanfly.
 * *** If an auto battle Actor fights against an enemy whose DEF/MDF is too
 *     high, they will not use any actions at all. This can cause potential
 *     game freezing and softlocks. This plugin will change that and have them
 *     default to a regular Attack.
 * * Compatibility Update!
 * ** Plugins should be more compatible with one another.
 * 
 * Version 1.08: October 11, 2020
 * * Feature Update!
 * ** Altered sprite bitmaps via the various draw functions will now be marked
 *    as modified and will automatically purge themselves from graphical memory
 *    upon a sprite's removal to free up more resources. Change made by Yanfly.
 * ** Picture Sprite Origin anchors are now tied to the Game_Picture show and
 *    move commands instead of the Game_Interpretter commands. Change by Arisu.
 * 
 * Version 1.07: October 4, 2020
 * * Documentation Update!
 * ** New documentation added for the new Plugin Parameter category:
 *    "Custom Parameters".
 * * New Features!
 * ** New Plugin Parameter "Custom Parameters" added by Yanfly.
 * *** Create custom parameters for your game! These will appear in
 *     VisuStella MZ menus.
 * 
 * Version 1.06: September 27, 2020
 * * Bug Fixes!
 * ** Battler evasion pose can now occur if there is a miss. These were made
 *    separate in RPG Maker MZ and misses didn't enable the evasion pose. Fix
 *    made by Olivia.
 * * New Features!
 * ** New notetags for Maps and name tags for Troops added by Yanfly!
 * *** <Frontview>, <Sideview> to change the battle view for that specific map,
 *     or troop regardless of what other settings are.
 * *** <DTB>, <TPB Active>, <TPB Wait> to change the battle system for that
 *     specific map or troop regardless of what other settings are.
 * 
 * Version 1.05: September 20, 2020
 * * Bug Fixes!
 * ** <Level: x> notetag for enemies is now fixed! Fix made by Arisu.
 * * Documentation Update!
 * ** Documentation added for the new "System: Battle System Change" Plugin
 *    Command and removed the old "System: Set Time Progress Battle".
 * * Feature Update!
 * ** The Plugin Command "System: Set Time Progress Battle" has been replaced
 *    with "System: Battle System Change" instead. This is to accommodate
 *    future plugins that allow for different battle systems. Added by Yanfly.
 * *** If you have previously used "System: Set Time Progress Battle", please
 *     replace them. We apologize for the inconvenience.
 * * New Features!
 * ** In the Core Engine's plugin parameters, you can now set the Battle System
 *    used. This will default to whatever is the game database's setting. This
 *    feature is used for the future when new battle systems are made. Feature
 *    added by Yanfly.
 * 
 * Version 1.04: September 13, 2020
 * * Documentation Update!
 * ** Added new documentation for the "Title Command List" and Title Picture
 *    Buttons" plugin parameters. They now have a dedicated section each.
 * * Feature Updates!
 * ** Moved the "Title Command List" and "Title Picture Buttons" parameters
 *    from the Menu Layout > Title settings. They were far too hidden away and
 *    users had a hard time finding them. Update made by Yanfly.
 * *** Users who have customized these settings before will need to readjust
 *     them again. We apologize for the inconvenience.
 * 
 * Version 1.03: September 6, 2020
 * * Bug Fixes!
 * ** Having QoL > Modern Controls disabled (why would you) used to prevent the
 *    down button from working. It works again. Fix made by Yanfly.
 * * New Feature!
 * ** Plugin default settings now come with a "Game End" option on the title
 *    screen. For those updating from version 1.02 or order, you can add this
 *    in by opening the Core Engine > Plugin Parameters > Menu Layout Settings
 *    > press "delete" on Scene_Title > open it up, then the new settings will
 *    fill in automatically.
 * * New Experimental Feature Added:
 * ** Screen Shake Settings added to the Plugin Parameters.
 * *** Screen Shake: Custom Plugin Command added!
 * *** Credit to Aries of Sheratan, who gave us permission to use her formula.
 * *** We'll be expanding on more screen shaking options in the future.
 * * Optimization Update
 * ** Digit Grouping now works more efficiently.
 * 
 * Version 1.02: August 30, 2020
 * * New Feature!
 * ** New Plugin Command: "Picture: Erase All". Added by Olivia.
 * *** Erases all pictures on the screen because it's extremely tedious to do
 *     it one by one.
 * ** New Plugin Command: "Picture: Erase Range"
 * *** Erases all pictures within a range of numbers because it's extremely
 *     tedious to do it one by one.
 * * Optimization Update
 * ** Added a more accurate means of parsing numbers for Digit Grouping.
 * ** Window_Base.prototype.textSizeEx now stores data to a cache.
 * * Documentation Update
 * ** Added a section to Major Changes: New Hard-Coded Features on
 *    Digit Grouping and explaining its intricacies.
 * ** Added a note to Plugin Parameters > UI > Reposition Actors to ignore the
 *    setting if using the Battle Core.
 * 
 * Version 1.01: August 23, 2020
 * * Bug Fixes!
 * ** Digit grouping fixed to allow text codes to detect values larger than
 *    1000. Fix made by Olivia and Yanfly.
 * ** Param Plus, Rate, Flat notetags fixed. Fix made by Yanfly.
 * * New Experimental Feature Added:
 * ** JS: Quick Functions found in the Plugin Parameters
 *
 * Version 1.00: August 20, 2020
 * * Finished Plugin!
 *
 * ============================================================================
 * End of Helpfile
 * ============================================================================
 *
 * @ --------------------------------------------------------------------------
 *
 * @command OpenURL
 * @text Game: Open URL
 * @desc Opens a website URL from the game.
 *
 * @arg URL:str
 * @text URL
 * @desc Where do you want to take the player?
 * @default https://www.google.com/
 * 
 * @ --------------------------------------------------------------------------
 *
 * @command GoldChange
 * @text Gold: Gain/Lose
 * @desc Allows you to give/take more gold than the event editor limit.
 *
 * @arg value:eval
 * @text Value
 * @desc How much gold should the player gain/lose?
 * Use negative values to remove gold.
 * @default 0
 *
 * @ --------------------------------------------------------------------------
 *
 * @command PictureEasingType
 * @text Picture: Easing Type
 * @desc Changes the easing type to a number of options.
 *
 * @arg pictureId:num
 * @text Picture ID
 * @type number
 * @min 1
 * @max 100
 * @desc Which picture do you wish to apply this easing to?
 * @default 1
 *
 * @arg easingType:str
 * @text Easing Type
 * @type combo
 * @option Linear
 * @option InSine
 * @option OutSine
 * @option InOutSine
 * @option InQuad
 * @option OutQuad
 * @option InOutQuad
 * @option InCubic
 * @option OutCubic
 * @option InOutCubic
 * @option InQuart
 * @option OutQuart
 * @option InOutQuart
 * @option InQuint
 * @option OutQuint
 * @option InOutQuint
 * @option InExpo
 * @option OutExpo
 * @option InOutExpo
 * @option InCirc
 * @option OutCirc
 * @option InOutCirc
 * @option InBack
 * @option OutBack
 * @option InOutBack
 * @option InElastic
 * @option OutElastic
 * @option InOutElastic
 * @option InBounce
 * @option OutBounce
 * @option InOutBounce
 * @desc Select which easing type you wish to apply.
 * @default Linear
 *
 * @arg LineBreak
 * @text ------------------------
 * @default --------------------------------
 *
 * @arg Instructions1
 * @text Instructions
 * @default Insert this Plugin Command after
 *
 * @arg Instructions2
 * @text -
 * @default a "Move Picture" event command.
 * 
 * @arg Instructions3
 * @text -
 * @default Turn off "Wait for Completion"
 *
 * @arg Instructions4
 * @text -
 * @default in the "Move Picture" event.
 *
 * @arg Instructions5
 * @text -
 * @default You may have to add in your own
 *
 * @arg Instructions6
 * @text -
 * @default "Wait" event command after.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command PictureEraseAll
 * @text Picture: Erase All
 * @desc Erases all pictures on the screen because it's extremely
 * tedious to do it one by one.
 *
 * @ --------------------------------------------------------------------------
 *
 * @command PictureEraseRange
 * @text Picture: Erase Range
 * @desc Erases all pictures within a range of numbers because it's
 * extremely tedious to do it one by one.
 *
 * @arg StartID:num
 * @text Starting ID
 * @type number
 * @min 1
 * @max 100
 * @desc The starting ID of the pictures to erase.
 * @default 1
 *
 * @arg EndingID:num
 * @text Ending ID
 * @type number
 * @min 1
 * @max 100
 * @desc The ending ID of the pictures to erase.
 * @default 100
 *
 * @ --------------------------------------------------------------------------
 *
 * @command ScreenShake
 * @text Screen Shake: Custom
 * @desc Creates a custom screen shake effect and also sets
 * the following uses of screen shake to this style.
 *
 * @arg Type:str
 * @text Shake Style
 * @type select
 * @option Original
 * @value original
 * @option Random
 * @value random
 * @option Horizontal
 * @value horizontal
 * @option Vertical
 * @value vertical
 * @desc Select shake style type.
 * @default random
 *
 * @arg Power:num
 * @text Power
 * @type number
 * @min 1
 * @max 9
 * @desc Power level for screen shake.
 * @default 5
 *
 * @arg Speed:num
 * @text Speed
 * @type number
 * @min 1
 * @max 9
 * @desc Speed level for screen shake.
 * @default 5
 *
 * @arg Duration:eval
 * @text Duration
 * @desc Duration of screenshake.
 * You can use code as well.
 * @default 60
 *
 * @arg Wait:eval
 * @text Wait for Completion
 * @parent Duration:eval
 * @type boolean
 * @on Wait
 * @off Don't Wait
 * @desc Wait until completion before moving onto the next event?
 * @default true
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SystemSetBattleSystem
 * @text System: Battle System Change
 * @desc Switch to a different battle system in-game.
 *
 * @arg option:str
 * @text Change To
 * @type select
 * @option Database Default (Use game database setting)
 * @value database
 * @option -
 * @value database
 * @option DTB: Default Turn Battle
 * @value dtb
 * @option TPB Active: Time Progress Battle (Active)
 * @value tpb active
 * @option TPB Wait: Time Progress Battle (Wait)
 * @value tpb wait
 * @option -
 * @value database
 * @option BTB: Brave Turn Battle (Req VisuMZ_2_BattleSystemBTB)
 * @value btb
 * @option CTB: Charge Turn Battle (Req VisuMZ_2_BattleSystemCTB)
 * @value ctb
 * @option FTB: Free Turn Battle (Req VisuMZ_2_BattleSystemFTB)
 * @value ftb
 * @option OTB: Order Turn Battle (Req VisuMZ_2_BattleSystemOTB)
 * @value otb
 * @option STB: Standard Turn Battle (Req VisuMZ_2_BattleSystemSTB)
 * @value stb
 * @desc Choose which battle system to switch to.
 * @default database
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SystemLoadImages
 * @text System: Load Images
 * @desc Allows you to (pre) load up images ahead of time.
 *
 * @arg animations:arraystr
 * @text img/animations/
 * @type file[]
 * @dir img/animations/
 * @desc Which files do you wish to load from this directory?
 * @default []
 *
 * @arg battlebacks1:arraystr
 * @text img/battlebacks1/
 * @type file[]
 * @dir img/battlebacks1/
 * @desc Which files do you wish to load from this directory?
 * @default []
 *
 * @arg battlebacks2:arraystr
 * @text img/battlebacks2/
 * @type file[]
 * @dir img/battlebacks2/
 * @desc Which files do you wish to load from this directory?
 * @default []
 *
 * @arg characters:arraystr
 * @text img/characters/
 * @type file[]
 * @dir img/characters/
 * @desc Which files do you wish to load from this directory?
 * @default []
 *
 * @arg enemies:arraystr
 * @text img/enemies/
 * @type file[]
 * @dir img/enemies/
 * @desc Which files do you wish to load from this directory?
 * @default []
 *
 * @arg faces:arraystr
 * @text img/faces/
 * @type file[]
 * @dir img/faces/
 * @desc Which files do you wish to load from this directory?
 * @default []
 *
 * @arg parallaxes:arraystr
 * @text img/parallaxes/
 * @type file[]
 * @dir img/parallaxes/
 * @desc Which files do you wish to load from this directory?
 * @default []
 *
 * @arg pictures:arraystr
 * @text img/pictures/
 * @type file[]
 * @dir img/pictures/
 * @desc Which files do you wish to load from this directory?
 * @default []
 *
 * @arg sv_actors:arraystr
 * @text img/sv_actors/
 * @type file[]
 * @dir img/sv_actors/
 * @desc Which files do you wish to load from this directory?
 * @default []
 *
 * @arg sv_enemies:arraystr
 * @text img/sv_enemies/
 * @type file[]
 * @dir img/sv_enemies/
 * @desc Which files do you wish to load from this directory?
 * @default []
 *
 * @arg system:arraystr
 * @text img/system/
 * @type file[]
 * @dir img/system/
 * @desc Which files do you wish to load from this directory?
 * @default []
 *
 * @arg tilesets:arraystr
 * @text img/tilesets/
 * @type file[]
 * @dir img/tilesets/
 * @desc Which files do you wish to load from this directory?
 * @default []
 *
 * @arg titles1:arraystr
 * @text img/titles1/
 * @type file[]
 * @dir img/titles1/
 * @desc Which files do you wish to load from this directory?
 * @default []
 *
 * @arg titles2:arraystr
 * @text img/titles2/
 * @type file[]
 * @dir img/titles2/
 * @desc Which files do you wish to load from this directory?
 * @default []
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SystemSetFontSize
 * @text System: Main Font Size
 * @desc Set the game's main font size.
 *
 * @arg option:num
 * @text Change To
 * @type number
 * @min 1
 * @desc Change the font size to this number.
 * @default 26
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SystemSetSideView
 * @text System: Side View Battle
 * @desc Switch between Front View or Side View for battle.
 *
 * @arg option:str
 * @text Change To
 * @type select
 * @option Front View
 * @value Front View
 * @option Side View
 * @value Side View
 * @option Toggle
 * @value Toggle
 * @desc Choose which view type to switch to.
 * @default Toggle
 *
 * @ --------------------------------------------------------------------------
 *
 * @command SystemSetWindowPadding
 * @text System: Window Padding
 * @desc Change the game's window padding amount.
 *
 * @arg option:num
 * @text Change To
 * @type number
 * @min 1
 * @desc Change the game's standard window padding to this value.
 * Default: 12
 * @default 12
 *
 * @ ==========================================================================
 * @ Plugin Parameters
 * @ ==========================================================================
 *
 * @param BreakHead
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param CoreEngine
 * @default Plugin Parameters
 * @param ATTENTION
 * @default READ THE HELP FILE
 *
 * @param BreakSettings
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param QoL:struct
 * @text Quality of Life Settings
 * @type struct<QoLSettings>
 * @desc Quality of Life settings for both developers and players.
 * @default {"PlayTest":"","NewGameBoot:eval":"false","ForceNoPlayTest:eval":"false","OpenConsole:eval":"true","F6key:eval":"true","F7key:eval":"true","NewGameCommonEvent:num":"0","DigitGrouping":"","DigitGroupingStandardText:eval":"true","DigitGroupingExText:eval":"true","DigitGroupingDamageSprites:eval":"true","DigitGroupingGaugeSprites:eval":"true","DigitGroupingLocale:str":"en-US","PlayerBenefit":"","EncounterRateMinimum:num":"10","EscapeAlways:eval":"true","ImprovedAccuracySystem:eval":"true","AccuracyBoost:eval":"true","LevelUpFullHp:eval":"true","LevelUpFullMp:eval":"true","Misc":"","AntiZoomPictures:eval":"true","AutoStretch:str":"stretch","FontShadows:eval":"false","FontSmoothing:eval":"true","KeyItemProtect:eval":"true","ModernControls:eval":"true","NoTileShadows:eval":"true","PixelateImageRendering:eval":"false","RequireFocus:eval":"true","SmartEventCollisionPriority:eval":"true"}
 * 
 * @param BattleSystem:str
 * @text Battle System
 * @type select
 * @option Database Default (Use game database setting)
 * @value database
 * @option -
 * @value database
 * @option DTB: Default Turn Battle
 * @value dtb
 * @option TPB Active: Time Progress Battle (Active)
 * @value tpb active
 * @option TPB wait: Time Progress Battle (Wait)
 * @value tpb wait
 * @option -
 * @value database
 * @option BTB: Brave Turn Battle (Req VisuMZ_2_BattleSystemBTB)
 * @value btb
 * @option CTB: Charge Turn Battle (Req VisuMZ_2_BattleSystemCTB)
 * @value ctb
 * @option FTB: Free Turn Battle (Req VisuMZ_2_BattleSystemFTB)
 * @value ftb
 * @option OTB: Order Turn Battle (Req VisuMZ_2_BattleSystemOTB)
 * @value otb
 * @option STB: Standard Turn Battle (Req VisuMZ_2_BattleSystemSTB)
 * @value stb
 * @desc Choose which battle system to use for your game.
 * @default database
 *
 * @param Color:struct
 * @text Color Settings
 * @type struct<Color>
 * @desc Change the colors used for in-game text.
 * @default {"BasicColors":"","ColorNormal:str":"0","ColorSystem:str":"16","ColorCrisis:str":"17","ColorDeath:str":"18","ColorGaugeBack:str":"19","ColorHPGauge1:str":"20","ColorHPGauge2:str":"21","ColorMPGauge1:str":"22","ColorMPGauge2:str":"23","ColorMPCost:str":"23","ColorPowerUp:str":"24","ColorPowerDown:str":"25","ColorCTGauge1:str":"26","ColorCTGauge2:str":"27","ColorTPGauge1:str":"28","ColorTPGauge2:str":"29","ColorTPCost:str":"29","ColorPending:str":"#2a847d","ColorExpGauge1:str":"30","ColorExpGauge2:str":"31","ColorMaxLvGauge1:str":"14","ColorMaxLvGauge2:str":"6","AlphaColors":"","OutlineColor:str":"rgba(0, 0, 0, 0.6)","DimColor1:str":"rgba(0, 0, 0, 0.6)","DimColor2:str":"rgba(0, 0, 0, 0)","ItemBackColor1:str":"rgba(32, 32, 32, 0.5)","ItemBackColor2:str":"rgba(0, 0, 0, 0.5)","ConditionalColors":"","ActorHPColor:func":"\"// Set the variables used in this function.\\nlet actor = arguments[0];\\n\\n// Check if the actor exists. If not, return normal.\\nif (!actor) {\\n    return this.normalColor();\\n\\n// If the actor is dead, return death color.\\n} else if (actor.isDead()) {\\n    return this.deathColor();\\n\\n// If the actor is dying, return crisis color.\\n} else if (actor.isDying()) {\\n    return this.crisisColor();\\n\\n// Otherwise, return the normal color.\\n} else {\\n    return this.normalColor();\\n}\"","ActorMPColor:func":"\"// Set the variables used in this function.\\nlet actor = arguments[0];\\n\\n// Check if the actor exists. If not, return normal.\\nif (!actor) {\\n    return this.normalColor();\\n\\n// If MP rate is below 25%, return crisis color.\\n} else if (actor.mpRate() < 0.25) {\\n    return this.crisisColor();\\n\\n// Otherwise, return the normal color.\\n} else {\\n    return this.normalColor();\\n}\"","ActorTPColor:func":"\"// Set the variables used in this function.\\nlet actor = arguments[0];\\n\\n// Check if the actor exists. If not, return normal.\\nif (!actor) {\\n    return this.normalColor();\\n\\n// If TP rate is below 25%, return crisis color.\\n} else if (actor.tpRate() < 0.25) {\\n    return this.crisisColor();\\n\\n// Otherwise, return the normal color.\\n} else {\\n    return this.normalColor();\\n}\"","ParamChange:func":"\"// Set the variables used in this function.\\nlet change = arguments[0];\\n\\n// If a positive change, use power up color.\\nif (change > 0) {\\n    return this.powerUpColor();\\n\\n// If a negative change, use power down color.\\n} else if (change < 0) {\\n    return this.powerDownColor();\\n\\n// Otherwise, return the normal color.\\n} else {\\n    return this.normalColor();\\n}\"","DamageColor:func":"\"// Set the variables used in this function.\\nlet colorType = arguments[0];\\n\\n// Check the value of the color type\\n// and return an appropriate color.\\nswitch (colorType) {\\n\\n    case 0: // HP damage\\n        return \\\"#ffffff\\\";\\n\\n    case 1: // HP recover\\n        return \\\"#b9ffb5\\\";\\n\\n    case 2: // MP damage\\n        return \\\"#bb88bb\\\";\\n\\n    case 3: // MP recover\\n        return \\\"#80b0ff\\\";\\n\\n    default:\\n        return \\\"#808080\\\";\\n}\""}
 *
 * @param Gold:struct
 * @text Gold Settings
 * @type struct<Gold>
 * @desc Change up how gold operates and is displayed in-game.
 * @default {"GoldMax:num":"999999999","GoldFontSize:num":"24","GoldIcon:num":"314","GoldOverlap:str":"A Lot","ItemStyle:eval":"true"}
 *
 * @param ImgLoad:struct
 * @text Image Loading
 * @type struct<ImgLoad>
 * @desc Game images that will be loaded upon booting up the game.
 * Use this responsibly!!!
 * @default {"animations:arraystr":"[]","battlebacks1:arraystr":"[]","battlebacks2:arraystr":"[]","characters:arraystr":"[]","enemies:arraystr":"[]","faces:arraystr":"[]","parallaxes:arraystr":"[]","pictures:arraystr":"[]","sv_actors:arraystr":"[]","sv_enemies:arraystr":"[]","system:arraystr":"[\"Balloon\",\"IconSet\"]","tilesets:arraystr":"[]","titles1:arraystr":"[]","titles2:arraystr":"[]"}
 *
 * @param KeyboardInput:struct
 * @text Keyboard Input
 * @type struct<KeyboardInput>
 * @desc Settings for the game that utilize keyboard input.
 * @default {"Controls":"","WASD:eval":"false","DashToggleR:eval":"false","NameInput":"","EnableNameInput:eval":"true","DefaultMode:str":"keyboard","QwertyLayout:eval":"true","NameInputMessage:eval":"\"Type in this character's name.\\nPress \\\\c[5]ENTER\\\\c[0] when you're done.\\n\\n-or-\\n\\nPress \\\\c[5]arrow keys\\\\c[0]/\\\\c[5]TAB\\\\c[0] to switch\\nto manual character entry.\\n\\nPress \\\\c[5]ESC\\\\c[0]/\\\\c[5]TAB\\\\c[0] to use to keyboard.\"","NumberInput":"","EnableNumberInput:eval":"true","ButtonAssist":"","Keyboard:str":"Keyboard","Manual:str":"Manual"}
 *
 * @param MenuBg:struct
 * @text Menu Background Settings
 * @type struct<MenuBg>
 * @desc Change how menu backgrounds look for each scene.
 * @default {"Scene_Menu:struct":"{\"SnapshotOpacity:num\":\"192\",\"BgFilename1:str\":\"\",\"BgFilename2:str\":\"\"}","Scene_Item:struct":"{\"SnapshotOpacity:num\":\"192\",\"BgFilename1:str\":\"\",\"BgFilename2:str\":\"\"}","Scene_Skill:struct":"{\"SnapshotOpacity:num\":\"192\",\"BgFilename1:str\":\"\",\"BgFilename2:str\":\"\"}","Scene_Equip:struct":"{\"SnapshotOpacity:num\":\"192\",\"BgFilename1:str\":\"\",\"BgFilename2:str\":\"\"}","Scene_Status:struct":"{\"SnapshotOpacity:num\":\"192\",\"BgFilename1:str\":\"\",\"BgFilename2:str\":\"\"}","Scene_Options:struct":"{\"SnapshotOpacity:num\":\"192\",\"BgFilename1:str\":\"\",\"BgFilename2:str\":\"\"}","Scene_Save:struct":"{\"SnapshotOpacity:num\":\"192\",\"BgFilename1:str\":\"\",\"BgFilename2:str\":\"\"}","Scene_Load:struct":"{\"SnapshotOpacity:num\":\"192\",\"BgFilename1:str\":\"\",\"BgFilename2:str\":\"\"}","Scene_GameEnd:struct":"{\"SnapshotOpacity:num\":\"128\",\"BgFilename1:str\":\"\",\"BgFilename2:str\":\"\"}","Scene_Shop:struct":"{\"SnapshotOpacity:num\":\"192\",\"BgFilename1:str\":\"\",\"BgFilename2:str\":\"\"}","Scene_Name:struct":"{\"SnapshotOpacity:num\":\"192\",\"BgFilename1:str\":\"\",\"BgFilename2:str\":\"\"}","Scene_Unlisted:struct":"{\"SnapshotOpacity:num\":\"192\",\"BgFilename1:str\":\"\",\"BgFilename2:str\":\"\"}"}
 *
 * @param ButtonAssist:struct
 * @text Menu Button Assist Window
 * @type struct<ButtonAssist>
 * @desc Settings pertaining to the Button Assist window found in in-game menus.
 * @default {"General":"","Enable:eval":"true","Location:str":"bottom","BgType:num":"0","Text":"","TextFmt:str":"%1:%2","MultiKeyFmt:str":"%1/%2","OkText:str":"Select","CancelText:str":"Back","SwitchActorText:str":"Switch Ally","Keys":"","KeyUnlisted:str":"\\}❪%1❫\\{","KeyUP:str":"^","KeyDOWN:str":"v","KeyLEFT:str":"<<","KeyRIGHT:str":">>","KeySHIFT:str":"\\}❪SHIFT❫\\{","KeyTAB:str":"\\}❪TAB❫\\{","KeyA:str":"A","KeyB:str":"B","KeyC:str":"C","KeyD:str":"D","KeyE:str":"E","KeyF:str":"F","KeyG:str":"G","KeyH:str":"H","KeyI:str":"I","KeyJ:str":"J","KeyK:str":"K","KeyL:str":"L","KeyM:str":"M","KeyN:str":"N","KeyO:str":"O","KeyP:str":"P","KeyQ:str":"Q","KeyR:str":"R","KeyS:str":"S","KeyT:str":"T","KeyU:str":"U","KeyV:str":"V","KeyW:str":"W","KeyX:str":"X","KeyY:str":"Y","KeyZ:str":"Z"}
 *
 * @param MenuLayout:struct
 * @text Menu Layout Settings
 * @type struct<MenuLayout>
 * @desc Change how menu layouts look for each scene.
 * @default {"Title:struct":"{\"TitleScreen\":\"\",\"DocumentTitleFmt:str\":\"%1: %2 - Version %3\",\"Subtitle:str\":\"Subtitle\",\"Version:str\":\"0.00\",\"drawGameTitle:func\":\"\\\"const x = 20;\\\\nconst y = Graphics.height / 4;\\\\nconst maxWidth = Graphics.width - x * 2;\\\\nconst text = $dataSystem.gameTitle;\\\\nconst bitmap = this._gameTitleSprite.bitmap;\\\\nbitmap.fontFace = $gameSystem.mainFontFace();\\\\nbitmap.outlineColor = \\\\\\\"black\\\\\\\";\\\\nbitmap.outlineWidth = 8;\\\\nbitmap.fontSize = 72;\\\\nbitmap.drawText(text, x, y, maxWidth, 48, \\\\\\\"center\\\\\\\");\\\"\",\"drawGameSubtitle:func\":\"\\\"const x = 20;\\\\nconst y = Graphics.height / 4 + 72;\\\\nconst maxWidth = Graphics.width - x * 2;\\\\nconst text = Scene_Title.subtitle;\\\\nconst bitmap = this._gameTitleSprite.bitmap;\\\\nbitmap.fontFace = $gameSystem.mainFontFace();\\\\nbitmap.outlineColor = \\\\\\\"black\\\\\\\";\\\\nbitmap.outlineWidth = 6;\\\\nbitmap.fontSize = 48;\\\\nbitmap.drawText(text, x, y, maxWidth, 48, \\\\\\\"center\\\\\\\");\\\"\",\"drawGameVersion:func\":\"\\\"const bitmap = this._gameTitleSprite.bitmap;\\\\nconst x = 0;\\\\nconst y = Graphics.height - 20;\\\\nconst width = Math.round(Graphics.width / 4);\\\\nconst height = 20;\\\\nconst c1 = ColorManager.dimColor1();\\\\nconst c2 = ColorManager.dimColor2();\\\\nconst text = 'Version ' + Scene_Title.version;\\\\nbitmap.gradientFillRect(x, y, width, height, c1, c2);\\\\nbitmap.fontFace = $gameSystem.mainFontFace();\\\\nbitmap.outlineColor = \\\\\\\"black\\\\\\\";\\\\nbitmap.outlineWidth = 3;\\\\nbitmap.fontSize = 16;\\\\nbitmap.drawText(text, x + 4, y, Graphics.width, height, \\\\\\\"left\\\\\\\");\\\"\",\"CommandRect:func\":\"\\\"const offsetX = $dataSystem.titleCommandWindow.offsetX;\\\\nconst offsetY = $dataSystem.titleCommandWindow.offsetY;\\\\nconst rows = this.commandWindowRows();\\\\nconst width = this.mainCommandWidth();\\\\nconst height = this.calcWindowHeight(rows, true);\\\\nconst x = (Graphics.boxWidth - width) / 2 + offsetX;\\\\nconst y = Graphics.boxHeight - height - 96 + offsetY;\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"ButtonFadeSpeed:num\":\"4\"}","MainMenu:struct":"{\"CommandWindow\":\"\",\"CommandBgType:num\":\"0\",\"CommandRect:func\":\"\\\"const width = this.mainCommandWidth();\\\\nconst height = this.mainAreaHeight() - this.goldWindowRect().height;\\\\nconst x = this.isRightInputMode() ? Graphics.boxWidth - width : 0;\\\\nconst y = this.mainAreaTop();\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"GoldWindow\":\"\",\"GoldBgType:num\":\"0\",\"GoldRect:func\":\"\\\"const rows = 1;\\\\nconst width = this.mainCommandWidth();\\\\nconst height = this.calcWindowHeight(rows, true);\\\\nconst x = this.isRightInputMode() ? Graphics.boxWidth - width : 0;\\\\nconst y = this.mainAreaBottom() - height;\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"StatusWindow\":\"\",\"StatusBgType:num\":\"0\",\"StatusRect:func\":\"\\\"const width = Graphics.boxWidth - this.mainCommandWidth();\\\\nconst height = this.mainAreaHeight();\\\\nconst x = this.isRightInputMode() ? 0 : Graphics.boxWidth - width;\\\\nconst y = this.mainAreaTop();\\\\nreturn new Rectangle(x, y, width, height);\\\"\"}","ItemMenu:struct":"{\"HelpWindow\":\"\",\"HelpBgType:num\":\"0\",\"HelpRect:func\":\"\\\"const x = 0;\\\\nconst y = this.helpAreaTop();\\\\nconst width = Graphics.boxWidth;\\\\nconst height = this.helpAreaHeight();\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"CategoryWindow\":\"\",\"CategoryBgType:num\":\"0\",\"CategoryRect:func\":\"\\\"const x = 0;\\\\nconst y = this.mainAreaTop();\\\\nconst rows = 1;\\\\nconst width = Graphics.boxWidth;\\\\nconst height = this.calcWindowHeight(rows, true);\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"ItemWindow\":\"\",\"ItemBgType:num\":\"0\",\"ItemRect:func\":\"\\\"const x = 0;\\\\nconst y = this._categoryWindow.y + this._categoryWindow.height;\\\\nconst width = Graphics.boxWidth;\\\\nconst height = this.mainAreaBottom() - y;\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"ActorWindow\":\"\",\"ActorBgType:num\":\"0\",\"ActorRect:func\":\"\\\"const x = 0;\\\\nconst y = this.mainAreaTop();\\\\nconst width = Graphics.boxWidth;\\\\nconst height = this.mainAreaHeight();\\\\nreturn new Rectangle(x, y, width, height);\\\"\"}","SkillMenu:struct":"{\"HelpWindow\":\"\",\"HelpBgType:num\":\"0\",\"HelpRect:func\":\"\\\"const x = 0;\\\\nconst y = this.helpAreaTop();\\\\nconst width = Graphics.boxWidth;\\\\nconst height = this.helpAreaHeight();\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"SkillTypeWindow\":\"\",\"SkillTypeBgType:num\":\"0\",\"SkillTypeRect:func\":\"\\\"const rows = 3;\\\\nconst width = this.mainCommandWidth();\\\\nconst height = this.calcWindowHeight(rows, true);\\\\nconst x = this.isRightInputMode() ? Graphics.boxWidth - width : 0;\\\\nconst y = this.mainAreaTop();\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"StatusWindow\":\"\",\"StatusBgType:num\":\"0\",\"StatusRect:func\":\"\\\"const width = Graphics.boxWidth - this.mainCommandWidth();\\\\nconst height = this._skillTypeWindow.height;\\\\nconst x = this.isRightInputMode() ? 0 : Graphics.boxWidth - width;\\\\nconst y = this.mainAreaTop();\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"ItemWindow\":\"\",\"ItemBgType:num\":\"0\",\"ItemRect:func\":\"\\\"const x = 0;\\\\nconst y = this._statusWindow.y + this._statusWindow.height;\\\\nconst width = Graphics.boxWidth;\\\\nconst height = this.mainAreaHeight() - this._statusWindow.height;\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"ActorWindow\":\"\",\"ActorBgType:num\":\"0\",\"ActorRect:func\":\"\\\"const x = 0;\\\\nconst y = this.mainAreaTop();\\\\nconst width = Graphics.boxWidth;\\\\nconst height = this.mainAreaHeight();\\\\nreturn new Rectangle(x, y, width, height);\\\"\"}","EquipMenu:struct":"{\"HelpWindow\":\"\",\"HelpBgType:num\":\"0\",\"HelpRect:func\":\"\\\"const x = 0;\\\\nconst y = this.helpAreaTop();\\\\nconst width = Graphics.boxWidth;\\\\nconst height = this.helpAreaHeight();\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"StatusWindow\":\"\",\"StatusBgType:num\":\"0\",\"StatusRect:func\":\"\\\"const x = 0;\\\\nconst y = this.mainAreaTop();\\\\nconst width = this.statusWidth();\\\\nconst height = this.mainAreaHeight();\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"CommandWindow\":\"\",\"CommandBgType:num\":\"0\",\"CommandRect:func\":\"\\\"const x = this.statusWidth();\\\\nconst y = this.mainAreaTop();\\\\nconst rows = 1;\\\\nconst width = Graphics.boxWidth - this.statusWidth();\\\\nconst height = this.calcWindowHeight(rows, true);\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"SlotWindow\":\"\",\"SlotBgType:num\":\"0\",\"SlotRect:func\":\"\\\"const commandWindowRect = this.commandWindowRect();\\\\nconst x = this.statusWidth();\\\\nconst y = commandWindowRect.y + commandWindowRect.height;\\\\nconst width = Graphics.boxWidth - this.statusWidth();\\\\nconst height = this.mainAreaHeight() - commandWindowRect.height;\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"ItemWindow\":\"\",\"ItemBgType:num\":\"0\",\"ItemRect:func\":\"\\\"return this.slotWindowRect();\\\"\"}","StatusMenu:struct":"{\"ProfileWindow\":\"\",\"ProfileBgType:num\":\"0\",\"ProfileRect:func\":\"\\\"const width = Graphics.boxWidth;\\\\nconst height = this.profileHeight();\\\\nconst x = 0;\\\\nconst y = this.mainAreaBottom() - height;\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"StatusWindow\":\"\",\"StatusBgType:num\":\"0\",\"StatusRect:func\":\"\\\"const x = 0;\\\\nconst y = this.mainAreaTop();\\\\nconst width = Graphics.boxWidth;\\\\nconst height = this.statusParamsWindowRect().y - y;\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"StatusParamsWindow\":\"\",\"StatusParamsBgType:num\":\"0\",\"StatusParamsRect:func\":\"\\\"const width = this.statusParamsWidth();\\\\nconst height = this.statusParamsHeight();\\\\nconst x = 0;\\\\nconst y = this.mainAreaBottom() - this.profileHeight() - height;\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"StatusEquipWindow\":\"\",\"StatusEquipBgType:num\":\"0\",\"StatusEquipRect:func\":\"\\\"const width = Graphics.boxWidth - this.statusParamsWidth();\\\\nconst height = this.statusParamsHeight();\\\\nconst x = this.statusParamsWidth();\\\\nconst y = this.mainAreaBottom() - this.profileHeight() - height;\\\\nreturn new Rectangle(x, y, width, height);\\\"\"}","OptionsMenu:struct":"{\"OptionsWindow\":\"\",\"OptionsBgType:num\":\"0\",\"OptionsRect:func\":\"\\\"const n = Math.min(this.maxCommands(), this.maxVisibleCommands());\\\\nconst width = 400;\\\\nconst height = this.calcWindowHeight(n, true);\\\\nconst x = (Graphics.boxWidth - width) / 2;\\\\nconst y = (Graphics.boxHeight - height) / 2;\\\\nreturn new Rectangle(x, y, width, height);\\\"\"}","SaveMenu:struct":"{\"HelpWindow\":\"\",\"HelpBgType:num\":\"0\",\"HelpRect:func\":\"\\\"const x = 0;\\\\nconst y = this.mainAreaTop();\\\\nconst rows = 1;\\\\nconst width = Graphics.boxWidth;\\\\nconst height = this.calcWindowHeight(rows, false);\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"ListWindow\":\"\",\"ListBgType:num\":\"0\",\"ListRect:func\":\"\\\"const x = 0;\\\\nconst y = this.mainAreaTop() + this._helpWindow.height;\\\\nconst width = Graphics.boxWidth;\\\\nconst height = this.mainAreaHeight() - this._helpWindow.height;\\\\nreturn new Rectangle(x, y, width, height);\\\"\"}","LoadMenu:struct":"{\"HelpWindow\":\"\",\"HelpBgType:num\":\"0\",\"HelpRect:func\":\"\\\"const x = 0;\\\\nconst y = this.mainAreaTop();\\\\nconst rows = 1;\\\\nconst width = Graphics.boxWidth;\\\\nconst height = this.calcWindowHeight(rows, false);\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"ListWindow\":\"\",\"ListBgType:num\":\"0\",\"ListRect:func\":\"\\\"const x = 0;\\\\nconst y = this.mainAreaTop() + this._helpWindow.height;\\\\nconst width = Graphics.boxWidth;\\\\nconst height = this.mainAreaHeight() - this._helpWindow.height;\\\\nreturn new Rectangle(x, y, width, height);\\\"\"}","GameEnd:struct":"{\"CommandList:arraystruct\":\"[\\\"{\\\\\\\"Symbol:str\\\\\\\":\\\\\\\"toTitle\\\\\\\",\\\\\\\"TextStr:str\\\\\\\":\\\\\\\"Untitled\\\\\\\",\\\\\\\"TextJS:func\\\\\\\":\\\\\\\"\\\\\\\\\\\\\\\"return TextManager.toTitle;\\\\\\\\\\\\\\\"\\\\\\\",\\\\\\\"ShowJS:func\\\\\\\":\\\\\\\"\\\\\\\\\\\\\\\"return true;\\\\\\\\\\\\\\\"\\\\\\\",\\\\\\\"EnableJS:func\\\\\\\":\\\\\\\"\\\\\\\\\\\\\\\"return true;\\\\\\\\\\\\\\\"\\\\\\\",\\\\\\\"ExtJS:func\\\\\\\":\\\\\\\"\\\\\\\\\\\\\\\"return null;\\\\\\\\\\\\\\\"\\\\\\\",\\\\\\\"CallHandlerJS:func\\\\\\\":\\\\\\\"\\\\\\\\\\\\\\\"SceneManager._scene.commandToTitle();\\\\\\\\\\\\\\\"\\\\\\\"}\\\",\\\"{\\\\\\\"Symbol:str\\\\\\\":\\\\\\\"cancel\\\\\\\",\\\\\\\"TextStr:str\\\\\\\":\\\\\\\"Untitled\\\\\\\",\\\\\\\"TextJS:func\\\\\\\":\\\\\\\"\\\\\\\\\\\\\\\"return TextManager.cancel;\\\\\\\\\\\\\\\"\\\\\\\",\\\\\\\"ShowJS:func\\\\\\\":\\\\\\\"\\\\\\\\\\\\\\\"return true;\\\\\\\\\\\\\\\"\\\\\\\",\\\\\\\"EnableJS:func\\\\\\\":\\\\\\\"\\\\\\\\\\\\\\\"return true;\\\\\\\\\\\\\\\"\\\\\\\",\\\\\\\"ExtJS:func\\\\\\\":\\\\\\\"\\\\\\\\\\\\\\\"return null;\\\\\\\\\\\\\\\"\\\\\\\",\\\\\\\"CallHandlerJS:func\\\\\\\":\\\\\\\"\\\\\\\\\\\\\\\"SceneManager._scene.popScene();\\\\\\\\\\\\\\\"\\\\\\\"}\\\"]\",\"CommandBgType:num\":\"0\",\"CommandRect:func\":\"\\\"const rows = 2;\\\\nconst width = this.mainCommandWidth();\\\\nconst height = this.calcWindowHeight(rows, true);\\\\nconst x = (Graphics.boxWidth - width) / 2;\\\\nconst y = (Graphics.boxHeight - height) / 2;\\\\nreturn new Rectangle(x, y, width, height);\\\"\"}","ShopMenu:struct":"{\"HelpWindow\":\"\",\"HelpBgType:num\":\"0\",\"HelpRect:func\":\"\\\"const wx = 0;\\\\nconst wy = this.helpAreaTop();\\\\nconst ww = Graphics.boxWidth;\\\\nconst wh = this.helpAreaHeight();\\\\nreturn new Rectangle(wx, wy, ww, wh);\\\"\",\"GoldWindow\":\"\",\"GoldBgType:num\":\"0\",\"GoldRect:func\":\"\\\"const rows = 1;\\\\nconst width = this.mainCommandWidth();\\\\nconst height = this.calcWindowHeight(rows, true);\\\\nconst x = Graphics.boxWidth - width;\\\\nconst y = this.mainAreaTop();\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"CommandWindow\":\"\",\"CommandBgType:num\":\"0\",\"CommandRect:func\":\"\\\"const x = 0;\\\\nconst y = this.mainAreaTop();\\\\nconst rows = 1;\\\\nconst width = this._goldWindow.x;\\\\nconst height = this.calcWindowHeight(rows, true);\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"DummyWindow\":\"\",\"DummyBgType:num\":\"0\",\"DummyRect:func\":\"\\\"const x = 0;\\\\nconst y = this._commandWindow.y + this._commandWindow.height;\\\\nconst width = Graphics.boxWidth;\\\\nconst height = this.mainAreaHeight() - this._commandWindow.height;\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"NumberWindow\":\"\",\"NumberBgType:num\":\"0\",\"NumberRect:func\":\"\\\"const x = 0;\\\\nconst y = this._dummyWindow.y;\\\\nconst width = Graphics.boxWidth - this.statusWidth();\\\\nconst height = this._dummyWindow.height;\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"StatusWindow\":\"\",\"StatusBgType:num\":\"0\",\"StatusRect:func\":\"\\\"const width = this.statusWidth();\\\\nconst height = this._dummyWindow.height;\\\\nconst x = Graphics.boxWidth - width;\\\\nconst y = this._dummyWindow.y;\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"BuyWindow\":\"\",\"BuyBgType:num\":\"0\",\"BuyRect:func\":\"\\\"const x = 0;\\\\nconst y = this._dummyWindow.y;\\\\nconst width = Graphics.boxWidth - this.statusWidth();\\\\nconst height = this._dummyWindow.height;\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"CategoryWindow\":\"\",\"CategoryBgType:num\":\"0\",\"CategoryRect:func\":\"\\\"const x = 0;\\\\nconst y = this._dummyWindow.y;\\\\nconst rows = 1;\\\\nconst width = Graphics.boxWidth;\\\\nconst height = this.calcWindowHeight(rows, true);\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"SellWindow\":\"\",\"SellBgType:num\":\"0\",\"SellRect:func\":\"\\\"const x = 0;\\\\nconst y = this._categoryWindow.y + this._categoryWindow.height;\\\\nconst width = Graphics.boxWidth;\\\\nconst height =\\\\n    this.mainAreaHeight() -\\\\n    this._commandWindow.height -\\\\n    this._categoryWindow.height;\\\\nreturn new Rectangle(x, y, width, height);\\\"\"}","NameMenu:struct":"{\"EditWindow\":\"\",\"EditBgType:num\":\"0\",\"EditRect:func\":\"\\\"const rows = 9;\\\\nconst inputWindowHeight = this.calcWindowHeight(rows, true);\\\\nconst padding = $gameSystem.windowPadding();\\\\nconst width = 600;\\\\nconst height = Math.min(ImageManager.faceHeight + padding * 2, this.mainAreaHeight() - inputWindowHeight);\\\\nconst x = (Graphics.boxWidth - width) / 2;\\\\nconst y = (this.mainAreaHeight() - (height + inputWindowHeight)) / 2 + this.mainAreaTop();\\\\nreturn new Rectangle(x, y, width, height);\\\"\",\"InputWindow\":\"\",\"InputBgType:num\":\"0\",\"InputRect:func\":\"\\\"const x = this._editWindow.x;\\\\nconst y = this._editWindow.y + this._editWindow.height;\\\\nconst rows = 9;\\\\nconst width = this._editWindow.width;\\\\nconst height = this.calcWindowHeight(rows, true);\\\\nreturn new Rectangle(x, y, width, height);\\\"\"}"}
 *
 * @param Param:struct
 * @text Parameter Settings
 * @type struct<Param>
 * @desc Change up the limits of parameters and how they're calculated.
 * @default {"DisplayedParams:arraystr":"[\"ATK\",\"DEF\",\"MAT\",\"MDF\",\"AGI\",\"LUK\"]","ExtDisplayedParams:arraystr":"[\"MaxHP\",\"MaxMP\",\"ATK\",\"DEF\",\"MAT\",\"MDF\",\"AGI\",\"LUK\"]","BasicParameters":"","CrisisRate:num":"0.25","BasicParameterFormula:func":"\"// Determine the variables used in this calculation.\\nlet paramId = arguments[0];\\nlet base = this.paramBase(paramId);\\nlet plus = this.paramPlus(paramId);\\nlet paramRate = this.paramRate(paramId);\\nlet buffRate = this.paramBuffRate(paramId);\\nlet flatBonus = this.paramFlatBonus(paramId);\\n\\n// Formula to determine total parameter value.\\nlet value = (base + plus) * paramRate * buffRate + flatBonus;\\n\\n// Determine the limits\\nconst maxValue = this.paramMax(paramId);\\nconst minValue = this.paramMin(paramId);\\n\\n// Final value\\nreturn Math.round(value.clamp(minValue, maxValue));\"","BasicParamCaps":"","BasicActorParamCaps":"","BasicActorParamMax0:str":"9999","BasicActorParamMax1:str":"9999","BasicActorParamMax2:str":"999","BasicActorParamMax3:str":"999","BasicActorParamMax4:str":"999","BasicActorParamMax5:str":"999","BasicActorParamMax6:str":"999","BasicActorParamMax7:str":"999","BasicEnemyParamCaps":"","BasicEnemyParamMax0:str":"999999","BasicEnemyParamMax1:str":"9999","BasicEnemyParamMax2:str":"999","BasicEnemyParamMax3:str":"999","BasicEnemyParamMax4:str":"999","BasicEnemyParamMax5:str":"999","BasicEnemyParamMax6:str":"999","BasicEnemyParamMax7:str":"999","XParameters":"","XParameterFormula:func":"\"// Determine the variables used in this calculation.\\nlet xparamId = arguments[0];\\nlet base = this.traitsSum(Game_BattlerBase.TRAIT_XPARAM, xparamId);\\nlet plus = this.xparamPlus(xparamId);\\nlet paramRate = this.xparamRate(xparamId);\\nlet flatBonus = this.xparamFlatBonus(xparamId);\\n\\n// Formula to determine total parameter value.\\nlet value = (base + plus) * paramRate + flatBonus;\\n\\n// Final value\\nreturn value;\"","XParamVocab":"","XParamVocab0:str":"Hit","XParamVocab1:str":"Evasion","XParamVocab2:str":"Critical Rate","XParamVocab3:str":"Critical Evade","XParamVocab4:str":"Magic Evade","XParamVocab5:str":"Magic Reflect","XParamVocab6:str":"Counter","XParamVocab7:str":"HP Regen","XParamVocab8:str":"MP Regen","XParamVocab9:str":"TP Regen","SParameters":"","SParameterFormula:func":"\"// Determine the variables used in this calculation.\\nlet sparamId = arguments[0];\\nlet base = this.traitsPi(Game_BattlerBase.TRAIT_SPARAM, sparamId);\\nlet plus = this.sparamPlus(sparamId);\\nlet paramRate = this.sparamRate(sparamId);\\nlet flatBonus = this.sparamFlatBonus(sparamId);\\n\\n// Formula to determine total parameter value.\\nlet value = (base + plus) * paramRate + flatBonus;\\n\\n// Final value\\nreturn value;\"","SParamVocab":"","SParamVocab0:str":"Aggro","SParamVocab1:str":"Guard","SParamVocab2:str":"Recovery","SParamVocab3:str":"Item Effect","SParamVocab4:str":"MP Cost","SParamVocab5:str":"TP Charge","SParamVocab6:str":"Physical DMG","SParamVocab7:str":"Magical DMG","SParamVocab8:str":"Floor DMG","SParamVocab9:str":"EXP Gain","Icons":"","DrawIcons:eval":"true","IconParam0:str":"84","IconParam1:str":"165","IconParam2:str":"76","IconParam3:str":"81","IconParam4:str":"101","IconParam5:str":"133","IconParam6:str":"140","IconParam7:str":"87","IconXParam0:str":"102","IconXParam1:str":"82","IconXParam2:str":"78","IconXParam3:str":"82","IconXParam4:str":"171","IconXParam5:str":"222","IconXParam6:str":"77","IconXParam7:str":"72","IconXParam8:str":"72","IconXParam9:str":"72","IconSParam0:str":"5","IconSParam1:str":"128","IconSParam2:str":"72","IconSParam3:str":"176","IconSParam4:str":"165","IconSParam5:str":"164","IconSParam6:str":"76","IconSParam7:str":"79","IconSParam8:str":"141","IconSParam9:str":"73"}
 *
 * @param CustomParam:arraystruct
 * @text Custom Parameters
 * @parent Param:struct
 * @type struct<CustomParam>[]
 * @desc Create custom parameters for your game!
 * These will appear in VisuStella MZ menus.
 * @default ["{\"ParamName:str\":\"Strength\",\"Abbreviation:str\":\"str\",\"Icon:num\":\"77\",\"Type:str\":\"integer\",\"ValueJS:json\":\"\\\"// Declare Constants\\\\nconst user = this;\\\\n\\\\n// Calculations\\\\nreturn (user.atk * 0.75) + (user.def * 0.25);\\\"\"}","{\"ParamName:str\":\"Dexterity\",\"Abbreviation:str\":\"dex\",\"Icon:num\":\"82\",\"Type:str\":\"integer\",\"ValueJS:json\":\"\\\"// Declare Constants\\\\nconst user = this;\\\\n\\\\n// Calculations\\\\nreturn (user.agi * 0.75) + (user.atk * 0.25);\\\"\"}","{\"ParamName:str\":\"Constitution\",\"Abbreviation:str\":\"con\",\"Icon:num\":\"81\",\"Type:str\":\"integer\",\"ValueJS:json\":\"\\\"// Declare Constants\\\\nconst user = this;\\\\n\\\\n// Calculations\\\\nreturn (user.def * 0.75) + (user.mdf * 0.25);\\\"\"}","{\"ParamName:str\":\"Intelligence\",\"Abbreviation:str\":\"int\",\"Icon:num\":\"79\",\"Type:str\":\"integer\",\"ValueJS:json\":\"\\\"// Declare Constants\\\\nconst user = this;\\\\n\\\\n// Calculations\\\\nreturn (user.mat * 0.75) + (user.mdf * 0.25);\\\"\"}","{\"ParamName:str\":\"Wisdom\",\"Abbreviation:str\":\"wis\",\"Icon:num\":\"72\",\"Type:str\":\"integer\",\"ValueJS:json\":\"\\\"// Declare Constants\\\\nconst user = this;\\\\n\\\\n// Calculations\\\\nreturn (user.mdf * 0.75) + (user.luk * 0.25);\\\"\"}","{\"ParamName:str\":\"Charisma\",\"Abbreviation:str\":\"cha\",\"Icon:num\":\"84\",\"Type:str\":\"integer\",\"ValueJS:json\":\"\\\"// Declare Constants\\\\nconst user = this;\\\\n\\\\n// Calculations\\\\nreturn (user.luk * 0.75) + (user.agi * 0.25);\\\"\"}"]
 *
 * @param ScreenShake:struct
 * @text Screen Shake Settings
 * @type struct<ScreenShake>
 * @desc Get more screen shake effects into your game!
 * @default {"DefaultStyle:str":"random","originalJS:func":"\"// Calculation\\nthis.x += Math.round($gameScreen.shake());\"","randomJS:func":"\"// Calculation\\n// Original Formula by Aries of Sheratan\\nconst power = $gameScreen._shakePower * 0.75;\\nconst speed = $gameScreen._shakeSpeed * 0.60;\\nconst duration = $gameScreen._shakeDuration;\\nthis.x += Math.round(Math.randomInt(power) - Math.randomInt(speed)) * (Math.min(duration, 30) * 0.5);\\nthis.y += Math.round(Math.randomInt(power) - Math.randomInt(speed)) * (Math.min(duration, 30) * 0.5);\"","horzJS:func":"\"// Calculation\\n// Original Formula by Aries of Sheratan\\nconst power = $gameScreen._shakePower * 0.75;\\nconst speed = $gameScreen._shakeSpeed * 0.60;\\nconst duration = $gameScreen._shakeDuration;\\nthis.x += Math.round(Math.randomInt(power) - Math.randomInt(speed)) * (Math.min(duration, 30) * 0.5);\"","vertJS:func":"\"// Calculation\\n// Original Formula by Aries of Sheratan\\nconst power = $gameScreen._shakePower * 0.75;\\nconst speed = $gameScreen._shakeSpeed * 0.60;\\nconst duration = $gameScreen._shakeDuration;\\nthis.y += Math.round(Math.randomInt(power) - Math.randomInt(speed)) * (Math.min(duration, 30) * 0.5);\""}
 *
 * @param TitleCommandList:arraystruct
 * @text Title Command List
 * @type struct<Command>[]
 * @desc Window commands used by the title screen.
 * Add new commands here.
 * @default ["{\"Symbol:str\":\"newGame\",\"TextStr:str\":\"Untitled\",\"TextJS:func\":\"\\\"return TextManager.newGame;\\\"\",\"ShowJS:func\":\"\\\"return true;\\\"\",\"EnableJS:func\":\"\\\"return true;\\\"\",\"ExtJS:func\":\"\\\"return null;\\\"\",\"CallHandlerJS:func\":\"\\\"SceneManager._scene.commandNewGame();\\\"\"}","{\"Symbol:str\":\"continue\",\"TextStr:str\":\"Untitled\",\"TextJS:func\":\"\\\"return TextManager.continue_;\\\"\",\"ShowJS:func\":\"\\\"return true;\\\"\",\"EnableJS:func\":\"\\\"return DataManager.isAnySavefileExists();\\\"\",\"ExtJS:func\":\"\\\"return null;\\\"\",\"CallHandlerJS:func\":\"\\\"SceneManager._scene.commandContinue();\\\"\"}","{\"Symbol:str\":\"options\",\"TextStr:str\":\"Untitled\",\"TextJS:func\":\"\\\"return TextManager.options;\\\"\",\"ShowJS:func\":\"\\\"return true;\\\"\",\"EnableJS:func\":\"\\\"return true;\\\"\",\"ExtJS:func\":\"\\\"return null;\\\"\",\"CallHandlerJS:func\":\"\\\"SceneManager._scene.commandOptions();\\\"\"}","{\"Symbol:str\":\"shutdown\",\"TextStr:str\":\"Untitled\",\"TextJS:func\":\"\\\"return TextManager.gameEnd;\\\"\",\"ShowJS:func\":\"\\\"return Utils.isNwjs();\\\"\",\"EnableJS:func\":\"\\\"return true;\\\"\",\"ExtJS:func\":\"\\\"return null;\\\"\",\"CallHandlerJS:func\":\"\\\"SceneManager.exit();\\\\n\\\\n// Note!\\\\n// Do NOT use this command with mobile devices or\\\\n// browser games. All it does is cause the game to\\\\n// display a blank, black canvas which the player\\\\n// is unable to do anything with. It does NOT force\\\\n// close the browser tab nor the app.\\\"\"}"]
 *
 * @param TitlePicButtons:arraystruct
 * @text Title Picture Buttons
 * @type struct<TitlePictureButton>[]
 * @desc Buttons that can be inserted into the title screen.
 * Add new title buttons here.
 * @default []
 *
 * @param UI:struct
 * @text UI Settings
 * @type struct<UI>
 * @desc Change up various in-game UI aspects.
 * @default {"UIArea":"","FadeSpeed:num":"24","BoxMargin:num":"4","CommandWidth:num":"240","BottomHelp:eval":"false","RightMenus:eval":"true","ShowButtons:eval":"true","cancelShowButton:eval":"true","menuShowButton:eval":"true","pagedownShowButton:eval":"true","numberShowButton:eval":"true","ButtonHeight:num":"52","BottomButtons:eval":"false","SideButtons:eval":"true","LargerResolution":"","RepositionActors:eval":"true","RepositionEnemies:eval":"true","MenuObjects":"","LvExpGauge:eval":"true","ParamArrow:str":"→","TextCodeSupport":"","TextCodeClassNames:eval":"true","TextCodeNicknames:eval":"true"}
 *
 * @param Window:struct
 * @text Window Settings
 * @type struct<Window>
 * @desc Adjust various in-game window settings.
 * @default {"WindowDefaults":"","EnableMasking:eval":"false","LineHeight:num":"36","SelectableItems":"","ShowItemBackground:eval":"true","ItemHeight:num":"8","DrawItemBackgroundJS:func":"\"const rect = arguments[0];\\nconst c1 = ColorManager.itemBackColor1();\\nconst c2 = ColorManager.itemBackColor2();\\nconst x = rect.x;\\nconst y = rect.y;\\nconst w = rect.width;\\nconst h = rect.height;\\nthis.contentsBack.gradientFillRect(x, y, w, h, c1, c2, true);\\nthis.contentsBack.strokeRect(x, y, w, h, c1);\"","ItemPadding:num":"8","BackOpacity:num":"192","TranslucentOpacity:num":"160","OpenSpeed:num":"32","ColSpacing:num":"8","RowSpacing:num":"4"}
 *
 * @param jsQuickFunc:arraystruct
 * @text JS: Quick Functions
 * @type struct<jsQuickFunc>[]
 * @desc Create quick JavaScript functions available from the
 * global namespace. Use with caution and moderation!!!
 * @default ["{\"FunctionName:str\":\"Example\",\"CodeJS:json\":\"\\\"// Insert this as a function anywhere you can input code\\\\n// such as Script Calls or Conditional Branch Scripts.\\\\n\\\\n// Process Code\\\\nreturn 'Example';\\\"\"}","{\"FunctionName:str\":\"Bad  Code  Name\",\"CodeJS:json\":\"\\\"// If a function name has spaces in them, the spaces will\\\\n// be removed. \\\\\\\"Bad  Code  Name\\\\\\\" becomes \\\\\\\"BadeCodeName\\\\\\\".\\\\n\\\\n// Process Code\\\\nOhNoItsBadCode()\\\\n\\\\n// If a function has bad code, a fail safe will catch the\\\\n// error and display it in the console.\\\"\"}","{\"FunctionName:str\":\"RandomNumber\",\"CodeJS:json\":\"\\\"// This generates a random number from 0 to itself.\\\\n// Example: RandomNumber(10)\\\\n\\\\n// Process Code\\\\nconst number = (arguments[0] || 0) + 1;\\\\nreturn Math.floor(number * Math.random());\\\"\"}","{\"FunctionName:str\":\"RandomBetween\",\"CodeJS:json\":\"\\\"// This generates a random number between two arguments.\\\\n// Example: RandomNumber(5, 10)\\\\n\\\\n// Process Code\\\\nlet min = Math.min(arguments[0] || 0, arguments[1] || 0);\\\\nlet max = Math.max(arguments[0] || 0, arguments[1] || 0);\\\\nreturn Math.floor(Math.random() * (max - min + 1) + min);\\\"\"}","{\"FunctionName:str\":\"RandomFrom\",\"CodeJS:json\":\"\\\"// Selects a number from the list of inserted numbers.\\\\n// Example: RandomFrom(5, 10, 15, 20)\\\\n\\\\n// Process Code\\\\nreturn arguments[Math.randomInt(arguments.length)];\\\"\"}"]
 *
 * @param BreakEnd1
 * @text --------------------------
 * @default ----------------------------------
 *
 * @param End Of
 * @default Plugin Parameters
 *
 * @param BreakEnd2
 * @text --------------------------
 * @default ----------------------------------
 *
 */
/* ----------------------------------------------------------------------------
 * Quality of Life Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~QoLSettings:
 *
 * @param PlayTest
 * @text Play Test
 *
 * @param NewGameBoot:eval
 * @text New Game on Boot
 * @parent PlayTest
 * @type boolean
 * @on Start New Game
 * @off Keep Title Screen
 * @desc Automatically start a new game on Play Test?
 * Only enabled during Play Test.
 * @default false
 *
 * @param ForceNoPlayTest:eval
 * @text No Play Test Mode
 * @parent PlayTest
 * @type boolean
 * @on Cancel Play Test
 * @off Keep Play Test
 * @desc Force the game to be out of Play Test mode when play testing.
 * @default false
 *
 * @param OpenConsole:eval
 * @text Open Console on Boot
 * @parent PlayTest
 * @type boolean
 * @on Open
 * @off Don't Open
 * @desc Open the Debug Console upon booting up your game?
 * Only enabled during Play Test.
 * @default true
 *
 * @param F6key:eval
 * @text F6: Toggle Sound
 * @parent PlayTest
 * @type boolean
 * @on Enable
 * @off Don't
 * @desc F6 Key Function: Turn on all sound to 100% or to 0%,
 * toggling between the two.
 * @default true
 *
 * @param F7key:eval
 * @text F7: Toggle Fast Mode
 * @parent PlayTest
 * @type boolean
 * @on Enable
 * @off Don't
 * @desc F7 Key Function: Toggle fast mode.
 * @default true
 *
 * @param NewGameCommonEvent:num
 * @text NewGame > CommonEvent
 * @parent PlayTest
 * @type common_event
 * @desc Runs a common event each time a new game during play test
 * session is started.
 * @default 0
 *
 * @param DigitGrouping
 * @text Digit Grouping
 *
 * @param DigitGroupingStandardText:eval
 * @text Standard Text
 * @parent DigitGrouping
 * @type boolean
 * @on Enable
 * @off Disable
 * @desc Make numbers like 1234567 appear like 1,234,567 for
 * standard text inside windows?
 * @default true
 *
 * @param DigitGroupingExText:eval
 * @text Ex Text
 * @parent DigitGrouping
 * @type boolean
 * @on Enable
 * @off Disable
 * @desc Make numbers like 1234567 appear like 1,234,567 for
 * ex text, written through drawTextEx (like messages)?
 * @default true
 *
 * @param DigitGroupingDamageSprites:eval
 * @text Damage Sprites
 * @parent DigitGrouping
 * @type boolean
 * @on Enable
 * @off Disable
 * @desc Make numbers like 1234567 appear like 1,234,567 for
 * in-battle damage sprites?
 * @default true
 *
 * @param DigitGroupingGaugeSprites:eval
 * @text Gauge Sprites
 * @parent DigitGrouping
 * @type boolean
 * @on Enable
 * @off Disable
 * @desc Make numbers like 1234567 appear like 1,234,567 for
 * visible gauge sprites such as HP, MP, and TP gauges?
 * @default true
 *
 * @param DigitGroupingLocale:str
 * @text Country/Locale
 * @parent DigitGrouping
 * @type combo
 * @option ar-SA
 * @option bn-BD
 * @option bn-IN
 * @option cs-CZ
 * @option da-DK
 * @option de-AT
 * @option de-CH
 * @option de-DE
 * @option el-GR
 * @option en-AU
 * @option en-CA
 * @option en-GB
 * @option en-IE
 * @option en-IN
 * @option en-NZ
 * @option en-US
 * @option en-ZA
 * @option es-AR
 * @option es-CL
 * @option es-CO
 * @option es-ES
 * @option es-MX
 * @option es-US
 * @option fi-FI
 * @option fr-BE
 * @option fr-CA
 * @option fr-CH
 * @option fr-FR
 * @option he-IL
 * @option hi-IN
 * @option hu-HU
 * @option id-ID
 * @option it-CH
 * @option it-IT
 * @option jp-JP
 * @option ko-KR
 * @option nl-BE
 * @option nl-NL
 * @option no-NO
 * @option pl-PL
 * @option pt-BR
 * @option pt-PT
 * @option ro-RO
 * @option ru-RU
 * @option sk-SK
 * @option sv-SE
 * @option ta-IN
 * @option ta-LK
 * @option th-TH
 * @option tr-TR
 * @option zh-CN
 * @option zh-HK
 * @option zh-TW
 * @desc Base the digit grouping on which country/locale?
 * @default en-US
 *
 * @param PlayerBenefit
 * @text Player Benefit
 *
 * @param EncounterRateMinimum:num
 * @text Encounter Rate Min
 * @parent PlayerBenefit
 * @min 0
 * @desc Minimum number of steps the player can take without any random encounters.
 * @default 10
 *
 * @param EscapeAlways:eval
 * @text Escape Always
 * @parent PlayerBenefit
 * @type boolean
 * @on Always
 * @off Default
 * @desc If the player wants to escape a battle, let them escape the battle with 100% chance.
 * @default true
 *
 * @param ImprovedAccuracySystem:eval
 * @text Accuracy Formula
 * @parent PlayerBenefit
 * @type boolean
 * @on Improve
 * @off Default
 * @desc Accuracy formula calculation change to
 * Skill Hit% * (User HIT - Target EVA) for better results.
 * @default true
 *
 * @param AccuracyBoost:eval
 * @text Accuracy Boost
 * @parent PlayerBenefit
 * @type boolean
 * @on Boost
 * @off Default
 * @desc Boost HIT and EVA rates in favor of the player.
 * @default true
 *
 * @param LevelUpFullHp:eval
 * @text Level Up -> Full HP
 * @parent PlayerBenefit
 * @type boolean
 * @on Heal
 * @off Default
 * @desc Recovers full HP when an actor levels up.
 * @default true
 *
 * @param LevelUpFullMp:eval
 * @text Level Up -> Full MP
 * @parent PlayerBenefit
 * @type boolean
 * @on Heal
 * @off Default
 * @desc Recovers full MP when an actor levels up.
 * @default true
 *
 * @param Misc
 * @text Misc
 *
 * @param AntiZoomPictures:eval
 * @text Anti-Zoom Pictures
 * @parent Misc
 * @type boolean
 * @on Anti-Zoom
 * @off Normal
 * @desc If on, prevents pictures from being affected by zoom.
 * @default true
 *
 * @param AutoStretch:str
 * @text Auto-Stretch
 * @parent Misc
 * @type select
 * @option Default
 * @value default
 * @option Stretch
 * @value stretch
 * @option Normal
 * @value normal
 * @desc Automatically stretch the game to fit the size of the client?
 * @default default
 *
 * @param FontShadows:eval
 * @text Font Shadows
 * @parent Misc
 * @type boolean
 * @on Shadows
 * @off Outlines
 * @desc If on, text uses shadows instead of outlines.
 * @default false
 *
 * @param FontSmoothing:eval
 * @text Font Smoothing
 * @parent Misc
 * @type boolean
 * @on Smooth
 * @off None
 * @desc If on, smoothes fonts shown in-game.
 * @default true
 *
 * @param KeyItemProtect:eval
 * @text Key Item Protection
 * @parent Misc
 * @type boolean
 * @on Unsellable
 * @off Sellable
 * @desc If on, prevents Key Items from being able to be sold and from being able to be consumed.
 * @default true
 *
 * @param ModernControls:eval
 * @text Modern Controls
 * @parent Misc
 * @type boolean
 * @on Enable
 * @off Default
 * @desc If on, allows usage of the Home/End buttons as well as other modern configs. Affects other VisuStella plugins.
 * @default true
 *
 * @param NewGameCommonEventAll:num
 * @text NewGame > CommonEvent
 * @parent Misc
 * @type common_event
 * @desc Runs a common event each time a new game during any session is started.
 * @default 0
 *
 * @param NoTileShadows:eval
 * @text No Tile Shadows
 * @parent Misc
 * @type boolean
 * @on Disable Tile Shadows
 * @off Default
 * @desc Removes tile shadows from being displayed in-game.
 * @default false
 *
 * @param PixelateImageRendering:eval
 * @text Pixel Image Rendering
 * @parent Misc
 * @type boolean
 * @on Pixelate
 * @off Smooth
 * @desc If on, pixelates the image rendering (for pixel games).
 * @default false
 *
 * @param RequireFocus:eval
 * @text Require Focus?
 * @parent Misc
 * @type boolean
 * @on Require
 * @off No Requirement
 * @desc Requires the game to be focused? If the game isn't
 * focused, it will pause if it's not the active window.
 * @default true
 *
 * @param SmartEventCollisionPriority:eval
 * @text Smart Event Collision
 * @parent Misc
 * @type boolean
 * @on Only Same Level
 * @off Default
 * @desc Makes events only able to collide with one another if they're 'Same as characters' priority.
 * @default true
 *
 */
/* ----------------------------------------------------------------------------
 * Color Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Color:
 *
 * @param BasicColors
 * @text Basic Colors
 *
 * @param ColorNormal:str
 * @text Normal
 * @parent BasicColors
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 0
 *
 * @param ColorSystem:str
 * @text System
 * @parent BasicColors
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 16
 *
 * @param ColorCrisis:str
 * @text Crisis
 * @parent BasicColors
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 17
 *
 * @param ColorDeath:str
 * @text Death
 * @parent BasicColors
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 18
 *
 * @param ColorGaugeBack:str
 * @text Gauge Back
 * @parent BasicColors
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 19
 *
 * @param ColorHPGauge1:str
 * @text HP Gauge 1
 * @parent BasicColors
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 20
 *
 * @param ColorHPGauge2:str
 * @text HP Gauge 2
 * @parent BasicColors
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 21
 *
 * @param ColorMPGauge1:str
 * @text MP Gauge 1
 * @parent BasicColors
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 22
 *
 * @param ColorMPGauge2:str
 * @text MP Gauge 2
 * @parent BasicColors
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 23
 *
 * @param ColorMPCost:str
 * @text MP Cost
 * @parent BasicColors
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 23
 *
 * @param ColorPowerUp:str
 * @text Power Up
 * @parent BasicColors
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 24
 *
 * @param ColorPowerDown:str
 * @text Power Down
 * @parent BasicColors
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 25
 *
 * @param ColorCTGauge1:str
 * @text CT Gauge 1
 * @parent BasicColors
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 26
 *
 * @param ColorCTGauge2:str
 * @text CT Gauge 2
 * @parent BasicColors
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 27
 *
 * @param ColorTPGauge1:str
 * @text TP Gauge 1
 * @parent BasicColors
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 28
 *
 * @param ColorTPGauge2:str
 * @text TP Gauge 2
 * @parent BasicColors
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 29
 *
 * @param ColorTPCost:str
 * @text TP Cost
 * @parent BasicColors
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 29
 *
 * @param ColorPending:str
 * @text Pending Color
 * @parent BasicColors
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default #2a847d
 *
 * @param ColorExpGauge1:str
 * @text EXP Gauge 1
 * @parent BasicColors
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 30
 *
 * @param ColorExpGauge2:str
 * @text EXP Gauge 2
 * @parent BasicColors
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 31
 *
 * @param ColorMaxLvGauge1:str
 * @text MaxLv Gauge 1
 * @parent BasicColors
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 14
 *
 * @param ColorMaxLvGauge2:str
 * @text MaxLv Gauge 2
 * @parent BasicColors
 * @desc Use #rrggbb for custom colors or regular numbers
 * for text colors from the Window Skin.
 * @default 6
 *
 * @param AlphaColors
 * @text Alpha Colors
 *
 * @param OutlineColor:str
 * @text Window Font Outline
 * @parent AlphaColors
 * @desc Colors with a bit of alpha settings.
 * Format rgba(0-255, 0-255, 0-255, 0-1)
 * @default rgba(0, 0, 0, 0.6)
 *
 * @param OutlineColorGauge:str
 * @text Gauge Number Outline
 * @parent AlphaColors
 * @desc Colors with a bit of alpha settings.
 * Format rgba(0-255, 0-255, 0-255, 0-1)
 * @default rgba(0, 0, 0, 1.0)
 *
 * @param DimColor1:str
 * @text Dim Color 1
 * @parent AlphaColors
 * @desc Colors with a bit of alpha settings.
 * Format rgba(0-255, 0-255, 0-255, 0-1)
 * @default rgba(0, 0, 0, 0.6)
 *
 * @param DimColor2:str
 * @text Dim Color 2
 * @parent AlphaColors
 * @desc Colors with a bit of alpha settings.
 * Format rgba(0-255, 0-255, 0-255, 0-1)
 * @default rgba(0, 0, 0, 0)
 *
 * @param ItemBackColor1:str
 * @text Item Back Color 1
 * @parent AlphaColors
 * @desc Colors with a bit of alpha settings.
 * Format rgba(0-255, 0-255, 0-255, 0-1)
 * @default rgba(32, 32, 32, 0.5)
 *
 * @param ItemBackColor2:str
 * @text Item Back Color 2
 * @parent AlphaColors
 * @desc Colors with a bit of alpha settings.
 * Format rgba(0-255, 0-255, 0-255, 0-1)
 * @default rgba(0, 0, 0, 0.5)
 *
 * @param ConditionalColors
 * @text Conditional Colors
 *
 * @param ActorHPColor:func
 * @text JS: Actor HP Color
 * @type note
 * @parent ConditionalColors
 * @desc Code used for determining what HP color to use for actors.
 * @default "// Set the variables used in this function.\nlet actor = arguments[0];\n\n// Check if the actor exists. If not, return normal.\nif (!actor) {\n    return this.normalColor();\n\n// If the actor is dead, return death color.\n} else if (actor.isDead()) {\n    return this.deathColor();\n\n// If the actor is dying, return crisis color.\n} else if (actor.isDying()) {\n    return this.crisisColor();\n\n// Otherwise, return the normal color.\n} else {\n    return this.normalColor();\n}"
 *
 * @param ActorMPColor:func
 * @text JS: Actor MP Color
 * @type note
 * @parent ConditionalColors
 * @desc Code used for determining what MP color to use for actors.
 * @default "// Set the variables used in this function.\nlet actor = arguments[0];\n\n// Check if the actor exists. If not, return normal.\nif (!actor) {\n    return this.normalColor();\n\n// If MP rate is below 25%, return crisis color.\n} else if (actor.mpRate() < 0.25) {\n    return this.crisisColor();\n\n// Otherwise, return the normal color.\n} else {\n    return this.normalColor();\n}"
 *
 * @param ActorTPColor:func
 * @text JS: Actor TP Color
 * @type note
 * @parent ConditionalColors
 * @desc Code used for determining what TP color to use for actors.
 * @default "// Set the variables used in this function.\nlet actor = arguments[0];\n\n// Check if the actor exists. If not, return normal.\nif (!actor) {\n    return this.normalColor();\n\n// If TP rate is below 25%, return crisis color.\n} else if (actor.tpRate() < 0.25) {\n    return this.crisisColor();\n\n// Otherwise, return the normal color.\n} else {\n    return this.normalColor();\n}"
 *
 * @param ParamChange:func
 * @text JS: Parameter Change
 * @type note
 * @parent ConditionalColors
 * @desc Code used for determining whatcolor to use for parameter changes.
 * @default "// Set the variables used in this function.\nlet change = arguments[0];\n\n// If a positive change, use power up color.\nif (change > 0) {\n    return this.powerUpColor();\n\n// If a negative change, use power down color.\n} else if (change < 0) {\n    return this.powerDownColor();\n\n// Otherwise, return the normal color.\n} else {\n    return this.normalColor();\n}"
 *
 * @param DamageColor:func
 * @text JS: Damage Colors
 * @type note
 * @parent ConditionalColors
 * @desc Code used for determining what color to use for damage types.
 * @default "// Set the variables used in this function.\nlet colorType = arguments[0];\n\n// Check the value of the color type\n// and return an appropriate color.\nswitch (colorType) {\n\n    case 0: // HP damage\n        return \"#ffffff\";\n\n    case 1: // HP recover\n        return \"#b9ffb5\";\n\n    case 2: // MP damage\n        return \"#bb88bb\";\n\n    case 3: // MP recover\n        return \"#80b0ff\";\n\n    default:\n        return \"#808080\";\n}"
 */
/* ----------------------------------------------------------------------------
 * Gold Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Gold:
 *
 * @param GoldMax:num
 * @text Gold Max
 * @type num
 * @min 1
 * @desc Maximum amount of Gold the party can hold.
 * Default 99999999
 * @default 99999999
 *
 * @param GoldFontSize:num
 * @text Gold Font Size
 * @type number
 * @min 1
 * @desc Font size used for displaying Gold inside Gold Windows.
 * Default: 26
 * @default 24
 *
 * @param GoldIcon:num
 * @text Gold Icon
 * @desc Icon used to represent Gold.
 * Use 0 for no icon.
 * @default 314
 *
 * @param GoldOverlap:str
 * @text Gold Overlap
 * @desc Text used too much Gold to fit in the window.
 * @default A Lot
 *
 * @param ItemStyle:eval
 * @text Item Style
 * @type boolean
 * @on Enable
 * @off Normal
 * @desc Draw gold in the item style?
 * ie: Icon, Label, Value
 * @default true
 *
 */
/* ----------------------------------------------------------------------------
 * Image Loading Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~ImgLoad:
 *
 * @param animations:arraystr
 * @text img/animations/
 * @type file[]
 * @dir img/animations/
 * @desc Which files do you wish to load from this directory upon
 * starting up the game?
 * @default []
 *
 * @param battlebacks1:arraystr
 * @text img/battlebacks1/
 * @type file[]
 * @dir img/battlebacks1/
 * @desc Which files do you wish to load from this directory upon
 * starting up the game?
 * @default []
 *
 * @param battlebacks2:arraystr
 * @text img/battlebacks2/
 * @type file[]
 * @dir img/battlebacks2/
 * @desc Which files do you wish to load from this directory upon
 * starting up the game?
 * @default []
 *
 * @param characters:arraystr
 * @text img/characters/
 * @type file[]
 * @dir img/characters/
 * @desc Which files do you wish to load from this directory upon
 * starting up the game?
 * @default []
 *
 * @param enemies:arraystr
 * @text img/enemies/
 * @type file[]
 * @dir img/enemies/
 * @desc Which files do you wish to load from this directory upon
 * starting up the game?
 * @default []
 *
 * @param faces:arraystr
 * @text img/faces/
 * @type file[]
 * @dir img/faces/
 * @desc Which files do you wish to load from this directory upon
 * starting up the game?
 * @default []
 *
 * @param parallaxes:arraystr
 * @text img/parallaxes/
 * @type file[]
 * @dir img/parallaxes/
 * @desc Which files do you wish to load from this directory upon
 * starting up the game?
 * @default []
 *
 * @param pictures:arraystr
 * @text img/pictures/
 * @type file[]
 * @dir img/pictures/
 * @desc Which files do you wish to load from this directory upon
 * starting up the game?
 * @default []
 *
 * @param sv_actors:arraystr
 * @text img/sv_actors/
 * @type file[]
 * @dir img/sv_actors/
 * @desc Which files do you wish to load from this directory upon
 * starting up the game?
 * @default []
 *
 * @param sv_enemies:arraystr
 * @text img/sv_enemies/
 * @type file[]
 * @dir img/sv_enemies/
 * @desc Which files do you wish to load from this directory upon
 * starting up the game?
 * @default []
 *
 * @param system:arraystr
 * @text img/system/
 * @type file[]
 * @dir img/system/
 * @desc Which files do you wish to load from this directory upon
 * starting up the game?
 * @default ["Balloon","IconSet"]
 *
 * @param tilesets:arraystr
 * @text img/tilesets/
 * @type file[]
 * @dir img/tilesets/
 * @desc Which files do you wish to load from this directory upon
 * starting up the game?
 * @default []
 *
 * @param titles1:arraystr
 * @text img/titles1/
 * @type file[]
 * @dir img/titles1/
 * @desc Which files do you wish to load from this directory upon
 * starting up the game?
 * @default []
 *
 * @param titles2:arraystr
 * @text img/titles2/
 * @type file[]
 * @dir img/titles2/
 * @desc Which files do you wish to load from this directory upon
 * starting up the game?
 * @default []
 *
 */
/* ----------------------------------------------------------------------------
 * Keyboard Input Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~KeyboardInput:
 *
 * @param Controls
 *
 * @param WASD:eval
 * @text WASD Movement
 * @parent Controls
 * @type boolean
 * @on Enable
 * @off Disable
 * @desc Enables or disables WASD movement for your game project.
 * Moves the W page down button to E.
 * @default false
 *
 * @param DashToggleR:eval
 * @text R Button: Dash Toggle
 * @parent Controls
 * @type boolean
 * @on Enable
 * @off Disable
 * @desc Enables or disables R button as an Always Dash option toggle.
 * @default false
 *
 * @param NameInput
 * @text Name Input
 *
 * @param EnableNameInput:eval
 * @text Enable?
 * @parent NameInput
 * @type boolean
 * @on Enable
 * @off Disable
 * @desc Enables keyboard input for name entry.
 * Only tested with English keyboards.
 * @default true
 * 
 * @param DefaultMode:str
 * @text Default Mode
 * @parent NameInput
 * @type select
 * @option Default - Uses Arrow Keys to select letters.
 * @value default
 * @option Keyboard - Uses Keyboard to type in letters.
 * @value keyboard
 * @desc Select default mode when entering the scene.
 * @default keyboard
 *
 * @param QwertyLayout:eval
 * @text QWERTY Layout
 * @parent NameInput
 * @type boolean
 * @on QWERTY Layout
 * @off ABCDEF Layout
 * @desc Uses the QWERTY layout for manual entry.
 * @default true
 *
 * @param NameInputMessage:eval
 * @text Keyboard Message
 * @parent NameInput
 * @type note
 * @desc The message displayed when allowing keyboard entry.
 * You may use text codes here.
 * @default "Type in this character's name.\nPress \\c[5]ENTER\\c[0] when you're done.\n\n-or-\n\nPress \\c[5]arrow keys\\c[0]/\\c[5]TAB\\c[0] to switch\nto manual character entry.\n\nPress \\c[5]ESC\\c[0]/\\c[5]TAB\\c[0] to use to keyboard."
 *
 * @param NumberInput
 * @text Number Input
 *
 * @param EnableNumberInput:eval
 * @text Enable?
 * @parent NumberInput
 * @type boolean
 * @on Enable
 * @off Disable
 * @desc Enables keyboard input for number entry.
 * Only tested with English keyboards.
 * @default true
 *
 * @param ButtonAssist
 * @text Button Assist
 * 
 * @param Keyboard:str
 * @text Switch To Keyboard
 * @parent ButtonAssist
 * @desc Text used to describe the keyboard switch.
 * @default Keyboard
 * 
 * @param Manual:str
 * @text Switch To Manual
 * @parent ButtonAssist
 * @desc Text used to describe the manual entry switch.
 * @default Manual
 *
 */
/* ----------------------------------------------------------------------------
 * Menu Background Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~MenuBg:
 *
 * @param Scene_Menu:struct
 * @text Scene_Menu
 * @type struct<BgSettings>
 * @desc The individual background settings for this scene.
 * @default {"SnapshotOpacity:num":"192","BgFilename1:str":"","BgFilename2:str":""}
 *
 * @param Scene_Item:struct
 * @text Scene_Item
 * @type struct<BgSettings>
 * @desc The individual background settings for this scene.
 * @default {"SnapshotOpacity:num":"192","BgFilename1:str":"","BgFilename2:str":""}
 *
 * @param Scene_Skill:struct
 * @text Scene_Skill
 * @type struct<BgSettings>
 * @desc The individual background settings for this scene.
 * @default {"SnapshotOpacity:num":"192","BgFilename1:str":"","BgFilename2:str":""}
 *
 * @param Scene_Equip:struct
 * @text Scene_Equip
 * @type struct<BgSettings>
 * @desc The individual background settings for this scene.
 * @default {"SnapshotOpacity:num":"192","BgFilename1:str":"","BgFilename2:str":""}
 *
 * @param Scene_Status:struct
 * @text Scene_Status
 * @type struct<BgSettings>
 * @desc The individual background settings for this scene.
 * @default {"SnapshotOpacity:num":"192","BgFilename1:str":"","BgFilename2:str":""}
 *
 * @param Scene_Options:struct
 * @text Scene_Options
 * @type struct<BgSettings>
 * @desc The individual background settings for this scene.
 * @default {"SnapshotOpacity:num":"192","BgFilename1:str":"","BgFilename2:str":""}
 *
 * @param Scene_Save:struct
 * @text Scene_Save
 * @type struct<BgSettings>
 * @desc The individual background settings for this scene.
 * @default {"SnapshotOpacity:num":"192","BgFilename1:str":"","BgFilename2:str":""}
 *
 * @param Scene_Load:struct
 * @text Scene_Load
 * @type struct<BgSettings>
 * @desc The individual background settings for this scene.
 * @default {"SnapshotOpacity:num":"192","BgFilename1:str":"","BgFilename2:str":""}
 *
 * @param Scene_GameEnd:struct
 * @text Scene_GameEnd
 * @type struct<BgSettings>
 * @desc The individual background settings for this scene.
 * @default {"SnapshotOpacity:num":"128","BgFilename1:str":"","BgFilename2:str":""}
 *
 * @param Scene_Shop:struct
 * @text Scene_Shop
 * @type struct<BgSettings>
 * @desc The individual background settings for this scene.
 * @default {"SnapshotOpacity:num":"192","BgFilename1:str":"","BgFilename2:str":""}
 *
 * @param Scene_Name:struct
 * @text Scene_Name
 * @type struct<BgSettings>
 * @desc The individual background settings for this scene.
 * @default {"SnapshotOpacity:num":"192","BgFilename1:str":"","BgFilename2:str":""}
 *
 * @param Scene_Unlisted:struct
 * @text Scene_Unlisted
 * @type struct<BgSettings>
 * @desc The individual background settings for any scenes that aren't listed here.
 * @default {"SnapshotOpacity:num":"192","BgFilename1:str":"","BgFilename2:str":""}
 *
 */
/* ----------------------------------------------------------------------------
 * Background Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~BgSettings:
 *
 * @param SnapshotOpacity:num
 * @text Snapshop Opacity
 * @type number
 * @min 0
 * @max 255
 * @desc Snapshot opacity for the scene.
 * @default 192
 *
 * @param BgFilename1:str
 * @text Background 1
 * @type file
 * @dir img/titles1/
 * @desc Filename used for the bottom background image.
 * Leave empty if you don't wish to use one.
 * @default 
 *
 * @param BgFilename2:str
 * @text Background 2
 * @type file
 * @dir img/titles2/
 * @desc Filename used for the upper background image.
 * Leave empty if you don't wish to use one.
 * @default 
 *
 */
/* ----------------------------------------------------------------------------
 * Menu Button Assist Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~ButtonAssist:
 *
 * @param General
 *
 * @param Enable:eval
 * @text Enable
 * @parent General
 * @type boolean
 * @on Use
 * @off Don't Use
 * @desc Enable the Menu Button Assist Window.
 * @default true
 *
 * @param Location:str
 * @text Location
 * @parent General
 * @type select
 * @option Top of Screen
 * @value top
 * @option Bottom of Screen
 * @value bottom
 * @desc Determine the location of the Button Assist Window.
 * Requires Plugin Parameters => UI => Side Buttons ON.
 * @default bottom
 *
 * @param BgType:num
 * @text Background Type
 * @parent General
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param Text
 *
 * @param TextFmt:str
 * @text Text Format
 * @parent Text
 * @desc Format on how the buttons are displayed.
 * Text codes allowed. %1 - Key, %2 - Text
 * @default %1:%2
 *
 * @param MultiKeyFmt:str
 * @text Multi-Key Format
 * @parent Text
 * @desc Format for actions with multiple keys.
 * Text codes allowed. %1 - Key 1, %2 - Key 2
 * @default %1/%2
 *
 * @param OkText:str
 * @text OK Text
 * @parent Text
 * @desc Default text used to display OK Key Action.
 * Text codes allowed.
 * @default Select
 *
 * @param CancelText:str
 * @text Cancel Text
 * @parent Text
 * @desc Default text used to display Cancel Key Action.
 * Text codes allowed.
 * @default Back
 *
 * @param SwitchActorText:str
 * @text Switch Actor Text
 * @parent Text
 * @desc Default text used to display Switch Actor Action.
 * Text codes allowed.
 * @default Switch Ally
 *
 * @param Keys
 *
 * @param KeyUnlisted:str
 * @text Key: Unlisted Format
 * @parent Keys
 * @desc If a key is not listed below, use this format.
 * Text codes allowed. %1 - Key
 * @default \}❪%1❫\{
 *
 * @param KeyUP:str
 * @text Key: Up
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default ^
 *
 * @param KeyDOWN:str
 * @text Key: Down
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default v
 *
 * @param KeyLEFT:str
 * @text Key: Left
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default <<
 *
 * @param KeyRIGHT:str
 * @text Key: Right
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default >>
 *
 * @param KeySHIFT:str
 * @text Key: Shift
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default \}❪SHIFT❫\{
 *
 * @param KeyTAB:str
 * @text Key: Tab
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default \}❪TAB❫\{
 *
 * @param KeyA:str
 * @text Key: A
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default A
 *
 * @param KeyB:str
 * @text Key: B
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default B
 *
 * @param KeyC:str
 * @text Key: C
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default C
 *
 * @param KeyD:str
 * @text Key: D
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default D
 *
 * @param KeyE:str
 * @text Key: E
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default E
 *
 * @param KeyF:str
 * @text Key: F
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default F
 *
 * @param KeyG:str
 * @text Key: G
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default G
 *
 * @param KeyH:str
 * @text Key: H
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default H
 *
 * @param KeyI:str
 * @text Key: I
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default I
 *
 * @param KeyJ:str
 * @text Key: J
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default J
 *
 * @param KeyK:str
 * @text Key: K
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default K
 *
 * @param KeyL:str
 * @text Key: L
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default L
 *
 * @param KeyM:str
 * @text Key: M
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default M
 *
 * @param KeyN:str
 * @text Key: N
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default N
 *
 * @param KeyO:str
 * @text Key: O
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default O
 *
 * @param KeyP:str
 * @text Key: P
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default P
 *
 * @param KeyQ:str
 * @text Key: Q
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default Q
 *
 * @param KeyR:str
 * @text Key: R
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default R
 *
 * @param KeyS:str
 * @text Key: S
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default S
 *
 * @param KeyT:str
 * @text Key: T
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default T
 *
 * @param KeyU:str
 * @text Key: U
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default U
 *
 * @param KeyV:str
 * @text Key: V
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default V
 *
 * @param KeyW:str
 * @text Key: W
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default W
 *
 * @param KeyX:str
 * @text Key: X
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default X
 *
 * @param KeyY:str
 * @text Key: Y
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default Y
 *
 * @param KeyZ:str
 * @text Key: Z
 * @parent Keys
 * @desc How this key is shown in-game.
 * Text codes allowed.
 * @default Z
 *
 */
/* ----------------------------------------------------------------------------
 * Menu Layout Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~MenuLayout:
 *
 * @param Title:struct
 * @text Scene_Title
 * @parent SceneSettings
 * @type struct<Title>
 * @desc Various options on adjusting the Title Scene.
 * @default {"TitleScreen":"","DocumentTitleFmt:str":"%1: %2 - Version %3","Subtitle:str":"Subtitle","Version:str":"0.00","drawGameTitle:func":"\"const x = 20;\\nconst y = Graphics.height / 4;\\nconst maxWidth = Graphics.width - x * 2;\\nconst text = $dataSystem.gameTitle;\\nconst bitmap = this._gameTitleSprite.bitmap;\\nbitmap.fontFace = $gameSystem.mainFontFace();\\nbitmap.outlineColor = \\\"black\\\";\\nbitmap.outlineWidth = 8;\\nbitmap.fontSize = 72;\\nbitmap.drawText(text, x, y, maxWidth, 48, \\\"center\\\");\"","drawGameSubtitle:func":"\"const x = 20;\\nconst y = Graphics.height / 4 + 72;\\nconst maxWidth = Graphics.width - x * 2;\\nconst text = Scene_Title.subtitle;\\nconst bitmap = this._gameTitleSprite.bitmap;\\nbitmap.fontFace = $gameSystem.mainFontFace();\\nbitmap.outlineColor = \\\"black\\\";\\nbitmap.outlineWidth = 6;\\nbitmap.fontSize = 48;\\nbitmap.drawText(text, x, y, maxWidth, 48, \\\"center\\\");\"","drawGameVersion:func":"\"const bitmap = this._gameTitleSprite.bitmap;\\nconst x = 0;\\nconst y = Graphics.height - 20;\\nconst width = Math.round(Graphics.width / 4);\\nconst height = 20;\\nconst c1 = ColorManager.dimColor1();\\nconst c2 = ColorManager.dimColor2();\\nconst text = 'Version ' + Scene_Title.version;\\nbitmap.gradientFillRect(x, y, width, height, c1, c2);\\nbitmap.fontFace = $gameSystem.mainFontFace();\\nbitmap.outlineColor = \\\"black\\\";\\nbitmap.outlineWidth = 3;\\nbitmap.fontSize = 16;\\nbitmap.drawText(text, x + 4, y, Graphics.width, height, \\\"left\\\");\"","CommandRect:func":"\"const offsetX = $dataSystem.titleCommandWindow.offsetX;\\nconst offsetY = $dataSystem.titleCommandWindow.offsetY;\\nconst rows = this.commandWindowRows();\\nconst width = this.mainCommandWidth();\\nconst height = this.calcWindowHeight(rows, true);\\nconst x = (Graphics.boxWidth - width) / 2 + offsetX;\\nconst y = Graphics.boxHeight - height - 96 + offsetY;\\nreturn new Rectangle(x, y, width, height);\"","ButtonFadeSpeed:num":"4"}
 *
 * @param MainMenu:struct
 * @text Scene_Menu
 * @parent SceneSettings
 * @type struct<MainMenu>
 * @desc Various options on adjusting the Main Menu Scene.
 * @default {"CommandWindow":"","CommandBgType:num":"0","CommandRect:func":"\"const width = this.mainCommandWidth();\\nconst height = this.mainAreaHeight() - this.goldWindowRect().height;\\nconst x = this.isRightInputMode() ? Graphics.boxWidth - width : 0;\\nconst y = this.mainAreaTop();\\nreturn new Rectangle(x, y, width, height);\"","GoldWindow":"","GoldBgType:num":"0","GoldRect:func":"\"const rows = 1;\\nconst width = this.mainCommandWidth();\\nconst height = this.calcWindowHeight(rows, true);\\nconst x = this.isRightInputMode() ? Graphics.boxWidth - width : 0;\\nconst y = this.mainAreaBottom() - height;\\nreturn new Rectangle(x, y, width, height);\"","StatusWindow":"","StatusBgType:num":"0","StatusRect:func":"\"const width = Graphics.boxWidth - this.mainCommandWidth();\\nconst height = this.mainAreaHeight();\\nconst x = this.isRightInputMode() ? 0 : Graphics.boxWidth - width;\\nconst y = this.mainAreaTop();\\nreturn new Rectangle(x, y, width, height);\""}
 *
 * @param ItemMenu:struct
 * @text Scene_Item
 * @parent SceneSettings
 * @type struct<ItemMenu>
 * @desc Various options on adjusting the Item Menu Scene.
 * @default {"HelpWindow":"","HelpBgType:num":"0","HelpRect:func":"\"const x = 0;\\nconst y = this.helpAreaTop();\\nconst width = Graphics.boxWidth;\\nconst height = this.helpAreaHeight();\\nreturn new Rectangle(x, y, width, height);\"","CategoryWindow":"","CategoryBgType:num":"0","CategoryRect:func":"\"const x = 0;\\nconst y = this.mainAreaTop();\\nconst rows = 1;\\nconst width = Graphics.boxWidth;\\nconst height = this.calcWindowHeight(rows, true);\\nreturn new Rectangle(x, y, width, height);\"","ItemWindow":"","ItemBgType:num":"0","ItemRect:func":"\"const x = 0;\\nconst y = this._categoryWindow.y + this._categoryWindow.height;\\nconst width = Graphics.boxWidth;\\nconst height = this.mainAreaBottom() - y;\\nreturn new Rectangle(x, y, width, height);\"","ActorWindow":"","ActorBgType:num":"0","ActorRect:func":"\"const x = 0;\\nconst y = this.mainAreaTop();\\nconst width = Graphics.boxWidth;\\nconst height = this.mainAreaHeight();\\nreturn new Rectangle(x, y, width, height);\""}
 *
 * @param SkillMenu:struct
 * @text Scene_Skill
 * @parent SceneSettings
 * @type struct<SkillMenu>
 * @desc Various options on adjusting the Skill Menu Scene.
 * @default {"HelpWindow":"","HelpBgType:num":"0","HelpRect:func":"\"const x = 0;\\nconst y = this.helpAreaTop();\\nconst width = Graphics.boxWidth;\\nconst height = this.helpAreaHeight();\\nreturn new Rectangle(x, y, width, height);\"","SkillTypeWindow":"","SkillTypeBgType:num":"0","SkillTypeRect:func":"\"const rows = 3;\\nconst width = this.mainCommandWidth();\\nconst height = this.calcWindowHeight(rows, true);\\nconst x = this.isRightInputMode() ? Graphics.boxWidth - width : 0;\\nconst y = this.mainAreaTop();\\nreturn new Rectangle(x, y, width, height);\"","StatusWindow":"","StatusBgType:num":"0","StatusRect:func":"\"const width = Graphics.boxWidth - this.mainCommandWidth();\\nconst height = this._skillTypeWindow.height;\\nconst x = this.isRightInputMode() ? 0 : Graphics.boxWidth - width;\\nconst y = this.mainAreaTop();\\nreturn new Rectangle(x, y, width, height);\"","ItemWindow":"","ItemBgType:num":"0","ItemRect:func":"\"const x = 0;\\nconst y = this._statusWindow.y + this._statusWindow.height;\\nconst width = Graphics.boxWidth;\\nconst height = this.mainAreaHeight() - this._statusWindow.height;\\nreturn new Rectangle(x, y, width, height);\"","ActorWindow":"","ActorBgType:num":"0","ActorRect:func":"\"const x = 0;\\nconst y = this.mainAreaTop();\\nconst width = Graphics.boxWidth;\\nconst height = this.mainAreaHeight();\\nreturn new Rectangle(x, y, width, height);\""}
 *
 * @param EquipMenu:struct
 * @text Scene_Equip
 * @parent SceneSettings
 * @type struct<EquipMenu>
 * @desc Various options on adjusting the Equip Menu Scene.
 * @default {"HelpWindow":"","HelpBgType:num":"0","HelpRect:func":"\"const x = 0;\\nconst y = this.helpAreaTop();\\nconst width = Graphics.boxWidth;\\nconst height = this.helpAreaHeight();\\nreturn new Rectangle(x, y, width, height);\"","StatusWindow":"","StatusBgType:num":"0","StatusRect:func":"\"const x = 0;\\nconst y = this.mainAreaTop();\\nconst width = this.statusWidth();\\nconst height = this.mainAreaHeight();\\nreturn new Rectangle(x, y, width, height);\"","CommandWindow":"","CommandBgType:num":"0","CommandRect:func":"\"const x = this.statusWidth();\\nconst y = this.mainAreaTop();\\nconst rows = 1;\\nconst width = Graphics.boxWidth - this.statusWidth();\\nconst height = this.calcWindowHeight(rows, true);\\nreturn new Rectangle(x, y, width, height);\"","SlotWindow":"","SlotBgType:num":"0","SlotRect:func":"\"const commandWindowRect = this.commandWindowRect();\\nconst x = this.statusWidth();\\nconst y = commandWindowRect.y + commandWindowRect.height;\\nconst width = Graphics.boxWidth - this.statusWidth();\\nconst height = this.mainAreaHeight() - commandWindowRect.height;\\nreturn new Rectangle(x, y, width, height);\"","ItemWindow":"","ItemBgType:num":"0","ItemRect:func":"\"return this.slotWindowRect();\""}
 *
 * @param StatusMenu:struct
 * @text Scene_Status
 * @parent SceneSettings
 * @type struct<StatusMenu>
 * @desc Various options on adjusting the Status Menu Scene.
 * @default {"ProfileWindow":"","ProfileBgType:num":"0","ProfileRect:func":"\"const width = Graphics.boxWidth;\\nconst height = this.profileHeight();\\nconst x = 0;\\nconst y = this.mainAreaBottom() - height;\\nreturn new Rectangle(x, y, width, height);\"","StatusWindow":"","StatusBgType:num":"0","StatusRect:func":"\"const x = 0;\\nconst y = this.mainAreaTop();\\nconst width = Graphics.boxWidth;\\nconst height = this.statusParamsWindowRect().y - y;\\nreturn new Rectangle(x, y, width, height);\"","StatusParamsWindow":"","StatusParamsBgType:num":"0","StatusParamsRect:func":"\"const width = this.statusParamsWidth();\\nconst height = this.statusParamsHeight();\\nconst x = 0;\\nconst y = this.mainAreaBottom() - this.profileHeight() - height;\\nreturn new Rectangle(x, y, width, height);\"","StatusEquipWindow":"","StatusEquipBgType:num":"0","StatusEquipRect:func":"\"const width = Graphics.boxWidth - this.statusParamsWidth();\\nconst height = this.statusParamsHeight();\\nconst x = this.statusParamsWidth();\\nconst y = this.mainAreaBottom() - this.profileHeight() - height;\\nreturn new Rectangle(x, y, width, height);\""}
 *
 * @param OptionsMenu:struct
 * @text Scene_Options
 * @parent SceneSettings
 * @type struct<OptionsMenu>
 * @desc Various options on adjusting the Options Menu Scene.
 * @default {"OptionsWindow":"","OptionsBgType:num":"0","OptionsRect:func":"\"const n = Math.min(this.maxCommands(), this.maxVisibleCommands());\\nconst width = 400;\\nconst height = this.calcWindowHeight(n, true);\\nconst x = (Graphics.boxWidth - width) / 2;\\nconst y = (Graphics.boxHeight - height) / 2;\\nreturn new Rectangle(x, y, width, height);\""}
 *
 * @param SaveMenu:struct
 * @text Scene_Save
 * @parent SceneSettings
 * @type struct<SaveMenu>
 * @desc Various options on adjusting the Save Menu Scene.
 * @default {"HelpWindow":"","HelpBgType:num":"0","HelpRect:func":"\"const x = 0;\\nconst y = this.mainAreaTop();\\nconst rows = 1;\\nconst width = Graphics.boxWidth;\\nconst height = this.calcWindowHeight(rows, false);\\nreturn new Rectangle(x, y, width, height);\"","ListWindow":"","ListBgType:num":"0","ListRect:func":"\"const x = 0;\\nconst y = this.mainAreaTop() + this._helpWindow.height;\\nconst width = Graphics.boxWidth;\\nconst height = this.mainAreaHeight() - this._helpWindow.height;\\nreturn new Rectangle(x, y, width, height);\""}
 *
 * @param LoadMenu:struct
 * @text Scene_Load
 * @parent SceneSettings
 * @type struct<LoadMenu>
 * @desc Various options on adjusting the Load Menu Scene.
 * @default {"HelpWindow":"","HelpBgType:num":"0","HelpRect:func":"\"const x = 0;\\nconst y = this.mainAreaTop();\\nconst rows = 1;\\nconst width = Graphics.boxWidth;\\nconst height = this.calcWindowHeight(rows, false);\\nreturn new Rectangle(x, y, width, height);\"","ListWindow":"","ListBgType:num":"0","ListRect:func":"\"const x = 0;\\nconst y = this.mainAreaTop() + this._helpWindow.height;\\nconst width = Graphics.boxWidth;\\nconst height = this.mainAreaHeight() - this._helpWindow.height;\\nreturn new Rectangle(x, y, width, height);\""}
 *
 * @param GameEnd:struct
 * @text Scene_GameEnd
 * @parent SceneSettings
 * @type struct<GameEnd>
 * @desc Various options on adjusting the Game End Scene.
 * @default {"CommandList:arraystruct":"[\"{\\\"Symbol:str\\\":\\\"toTitle\\\",\\\"TextStr:str\\\":\\\"Untitled\\\",\\\"TextJS:func\\\":\\\"\\\\\\\"return TextManager.toTitle;\\\\\\\"\\\",\\\"ShowJS:func\\\":\\\"\\\\\\\"return true;\\\\\\\"\\\",\\\"EnableJS:func\\\":\\\"\\\\\\\"return true;\\\\\\\"\\\",\\\"ExtJS:func\\\":\\\"\\\\\\\"return null;\\\\\\\"\\\",\\\"CallHandlerJS:func\\\":\\\"\\\\\\\"SceneManager._scene.commandToTitle();\\\\\\\"\\\"}\",\"{\\\"Symbol:str\\\":\\\"cancel\\\",\\\"TextStr:str\\\":\\\"Untitled\\\",\\\"TextJS:func\\\":\\\"\\\\\\\"return TextManager.cancel;\\\\\\\"\\\",\\\"ShowJS:func\\\":\\\"\\\\\\\"return true;\\\\\\\"\\\",\\\"EnableJS:func\\\":\\\"\\\\\\\"return true;\\\\\\\"\\\",\\\"ExtJS:func\\\":\\\"\\\\\\\"return null;\\\\\\\"\\\",\\\"CallHandlerJS:func\\\":\\\"\\\\\\\"SceneManager._scene.popScene();\\\\\\\"\\\"}\"]","CommandBgType:num":"0","CommandRect:func":"\"const rows = 2;\\nconst width = this.mainCommandWidth();\\nconst height = this.calcWindowHeight(rows, true);\\nconst x = (Graphics.boxWidth - width) / 2;\\nconst y = (Graphics.boxHeight - height) / 2;\\nreturn new Rectangle(x, y, width, height);\""}
 *
 * @param ShopMenu:struct
 * @text Scene_Shop
 * @parent SceneSettings
 * @type struct<ShopMenu>
 * @desc Various options on adjusting the Shop Menu Scene.
 * @default {"HelpWindow":"","HelpBgType:num":"0","HelpRect:func":"\"const wx = 0;\\nconst wy = this.helpAreaTop();\\nconst ww = Graphics.boxWidth;\\nconst wh = this.helpAreaHeight();\\nreturn new Rectangle(wx, wy, ww, wh);\"","GoldWindow":"","GoldBgType:num":"0","GoldRect:func":"\"const rows = 1;\\nconst width = this.mainCommandWidth();\\nconst height = this.calcWindowHeight(rows, true);\\nconst x = Graphics.boxWidth - width;\\nconst y = this.mainAreaTop();\\nreturn new Rectangle(x, y, width, height);\"","CommandWindow":"","CommandBgType:num":"0","CommandRect:func":"\"const x = 0;\\nconst y = this.mainAreaTop();\\nconst rows = 1;\\nconst width = this._goldWindow.x;\\nconst height = this.calcWindowHeight(rows, true);\\nreturn new Rectangle(x, y, width, height);\"","DummyWindow":"","DummyBgType:num":"0","DummyRect:func":"\"const x = 0;\\nconst y = this._commandWindow.y + this._commandWindow.height;\\nconst width = Graphics.boxWidth;\\nconst height = this.mainAreaHeight() - this._commandWindow.height;\\nreturn new Rectangle(x, y, width, height);\"","NumberWindow":"","NumberBgType:num":"0","NumberRect:func":"\"const x = 0;\\nconst y = this._dummyWindow.y;\\nconst width = Graphics.boxWidth - this.statusWidth();\\nconst height = this._dummyWindow.height;\\nreturn new Rectangle(x, y, width, height);\"","StatusWindow":"","StatusBgType:num":"0","StatusRect:func":"\"const width = this.statusWidth();\\nconst height = this._dummyWindow.height;\\nconst x = Graphics.boxWidth - width;\\nconst y = this._dummyWindow.y;\\nreturn new Rectangle(x, y, width, height);\"","BuyWindow":"","BuyBgType:num":"0","BuyRect:func":"\"const x = 0;\\nconst y = this._dummyWindow.y;\\nconst width = Graphics.boxWidth - this.statusWidth();\\nconst height = this._dummyWindow.height;\\nreturn new Rectangle(x, y, width, height);\"","CategoryWindow":"","CategoryBgType:num":"0","CategoryRect:func":"\"const x = 0;\\nconst y = this._dummyWindow.y;\\nconst rows = 1;\\nconst width = Graphics.boxWidth;\\nconst height = this.calcWindowHeight(rows, true);\\nreturn new Rectangle(x, y, width, height);\"","SellWindow":"","SellBgType:num":"0","SellRect:func":"\"const x = 0;\\nconst y = this._categoryWindow.y + this._categoryWindow.height;\\nconst width = Graphics.boxWidth;\\nconst height =\\n    this.mainAreaHeight() -\\n    this._commandWindow.height -\\n    this._categoryWindow.height;\\nreturn new Rectangle(x, y, width, height);\""}
 *
 * @param NameMenu:struct
 * @text Scene_Name
 * @parent SceneSettings
 * @type struct<NameMenu>
 * @desc Various options on adjusting the Actor Rename Scene.
 * @default {"EditWindow":"","EditBgType:num":"0","EditRect:func":"\"const rows = 9;\\nconst inputWindowHeight = this.calcWindowHeight(rows, true);\\nconst padding = $gameSystem.windowPadding();\\nconst width = 600;\\nconst height = Math.min(ImageManager.faceHeight + padding * 2, this.mainAreaHeight() - inputWindowHeight);\\nconst x = (Graphics.boxWidth - width) / 2;\\nconst y = (this.mainAreaHeight() - (height + inputWindowHeight)) / 2 + this.mainAreaTop();\\nreturn new Rectangle(x, y, width, height);\"","InputWindow":"","InputBgType:num":"0","InputRect:func":"\"const x = this._editWindow.x;\\nconst y = this._editWindow.y + this._editWindow.height;\\nconst rows = 9;\\nconst width = this._editWindow.width;\\nconst height = this.calcWindowHeight(rows, true);\\nreturn new Rectangle(x, y, width, height);\""}
 *
 */
/* ----------------------------------------------------------------------------
 * Main Menu Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~MainMenu:
 *
 * @param CommandWindow
 * @text Command Window
 *
 * @param CommandBgType:num
 * @text Background Type
 * @parent CommandWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param CommandRect:func
 * @text JS: X, Y, W, H
 * @parent CommandWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const width = this.mainCommandWidth();\nconst height = this.mainAreaHeight() - this.goldWindowRect().height;\nconst x = this.isRightInputMode() ? Graphics.boxWidth - width : 0;\nconst y = this.mainAreaTop();\nreturn new Rectangle(x, y, width, height);"
 *
 * @param GoldWindow
 * @text Gold Window
 *
 * @param GoldBgType:num
 * @text Background Type
 * @parent GoldWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param GoldRect:func
 * @text JS: X, Y, W, H
 * @parent GoldWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const rows = 1;\nconst width = this.mainCommandWidth();\nconst height = this.calcWindowHeight(rows, true);\nconst x = this.isRightInputMode() ? Graphics.boxWidth - width : 0;\nconst y = this.mainAreaBottom() - height;\nreturn new Rectangle(x, y, width, height);"
 *
 * @param StatusWindow
 * @text Status Window
 *
 * @param StatusBgType:num
 * @text Background Type
 * @parent StatusWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param StatusRect:func
 * @text JS: X, Y, W, H
 * @parent StatusWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const width = Graphics.boxWidth - this.mainCommandWidth();\nconst height = this.mainAreaHeight();\nconst x = this.isRightInputMode() ? 0 : Graphics.boxWidth - width;\nconst y = this.mainAreaTop();\nreturn new Rectangle(x, y, width, height);"
 *
 */
/* ----------------------------------------------------------------------------
 * Item Menu Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~ItemMenu:
 *
 * @param HelpWindow
 * @text Help Window
 *
 * @param HelpBgType:num
 * @text Background Type
 * @parent HelpWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param HelpRect:func
 * @text JS: X, Y, W, H
 * @parent HelpWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const x = 0;\nconst y = this.helpAreaTop();\nconst width = Graphics.boxWidth;\nconst height = this.helpAreaHeight();\nreturn new Rectangle(x, y, width, height);"
 *
 * @param CategoryWindow
 * @text Category Window
 *
 * @param CategoryBgType:num
 * @text Background Type
 * @parent CategoryWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param CategoryRect:func
 * @text JS: X, Y, W, H
 * @parent CategoryWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const x = 0;\nconst y = this.mainAreaTop();\nconst rows = 1;\nconst width = Graphics.boxWidth;\nconst height = this.calcWindowHeight(rows, true);\nreturn new Rectangle(x, y, width, height);"
 *
 * @param ItemWindow
 * @text Item Window
 *
 * @param ItemBgType:num
 * @text Background Type
 * @parent ItemWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param ItemRect:func
 * @text JS: X, Y, W, H
 * @parent ItemWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const x = 0;\nconst y = this._categoryWindow.y + this._categoryWindow.height;\nconst width = Graphics.boxWidth;\nconst height = this.mainAreaBottom() - y;\nreturn new Rectangle(x, y, width, height);"
 *
 * @param ActorWindow
 * @text Actor Window
 *
 * @param ActorBgType:num
 * @text Background Type
 * @parent ActorWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param ActorRect:func
 * @text JS: X, Y, W, H
 * @parent ActorWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const x = 0;\nconst y = this.mainAreaTop();\nconst width = Graphics.boxWidth;\nconst height = this.mainAreaHeight();\nreturn new Rectangle(x, y, width, height);"
 *
 */
/* ----------------------------------------------------------------------------
 * Skill Menu Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~SkillMenu:
 *
 * @param HelpWindow
 * @text Help Window
 *
 * @param HelpBgType:num
 * @text Background Type
 * @parent HelpWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param HelpRect:func
 * @text JS: X, Y, W, H
 * @parent HelpWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const x = 0;\nconst y = this.helpAreaTop();\nconst width = Graphics.boxWidth;\nconst height = this.helpAreaHeight();\nreturn new Rectangle(x, y, width, height);"
 *
 * @param SkillTypeWindow
 * @text Skill Type Window
 *
 * @param SkillTypeBgType:num
 * @text Background Type
 * @parent SkillTypeWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param SkillTypeRect:func
 * @text JS: X, Y, W, H
 * @parent SkillTypeWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const rows = 3;\nconst width = this.mainCommandWidth();\nconst height = this.calcWindowHeight(rows, true);\nconst x = this.isRightInputMode() ? Graphics.boxWidth - width : 0;\nconst y = this.mainAreaTop();\nreturn new Rectangle(x, y, width, height);"
 *
 * @param StatusWindow
 * @text Status Window
 *
 * @param StatusBgType:num
 * @text Background Type
 * @parent StatusWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param StatusRect:func
 * @text JS: X, Y, W, H
 * @parent StatusWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const width = Graphics.boxWidth - this.mainCommandWidth();\nconst height = this._skillTypeWindow.height;\nconst x = this.isRightInputMode() ? 0 : Graphics.boxWidth - width;\nconst y = this.mainAreaTop();\nreturn new Rectangle(x, y, width, height);"
 *
 * @param ItemWindow
 * @text Item Window
 *
 * @param ItemBgType:num
 * @text Background Type
 * @parent ItemWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param ItemRect:func
 * @text JS: X, Y, W, H
 * @parent ItemWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const x = 0;\nconst y = this._statusWindow.y + this._statusWindow.height;\nconst width = Graphics.boxWidth;\nconst height = this.mainAreaHeight() - this._statusWindow.height;\nreturn new Rectangle(x, y, width, height);"
 *
 * @param ActorWindow
 * @text Actor Window
 *
 * @param ActorBgType:num
 * @text Background Type
 * @parent ActorWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param ActorRect:func
 * @text JS: X, Y, W, H
 * @parent ActorWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const x = 0;\nconst y = this.mainAreaTop();\nconst width = Graphics.boxWidth;\nconst height = this.mainAreaHeight();\nreturn new Rectangle(x, y, width, height);"
 *
 */
/* ----------------------------------------------------------------------------
 * Equip Menu Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~EquipMenu:
 *
 * @param HelpWindow
 * @text Help Window
 *
 * @param HelpBgType:num
 * @text Background Type
 * @parent HelpWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param HelpRect:func
 * @text JS: X, Y, W, H
 * @parent HelpWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const x = 0;\nconst y = this.helpAreaTop();\nconst width = Graphics.boxWidth;\nconst height = this.helpAreaHeight();\nreturn new Rectangle(x, y, width, height);"
 *
 * @param StatusWindow
 * @text Status Window
 *
 * @param StatusBgType:num
 * @text Background Type
 * @parent StatusWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param StatusRect:func
 * @text JS: X, Y, W, H
 * @parent StatusWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const x = 0;\nconst y = this.mainAreaTop();\nconst width = this.statusWidth();\nconst height = this.mainAreaHeight();\nreturn new Rectangle(x, y, width, height);"
 *
 * @param CommandWindow
 * @text Command Window
 *
 * @param CommandBgType:num
 * @text Background Type
 * @parent CommandWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param CommandRect:func
 * @text JS: X, Y, W, H
 * @parent CommandWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const x = this.statusWidth();\nconst y = this.mainAreaTop();\nconst rows = 1;\nconst width = Graphics.boxWidth - this.statusWidth();\nconst height = this.calcWindowHeight(rows, true);\nreturn new Rectangle(x, y, width, height);"
 *
 * @param SlotWindow
 * @text Slot Window
 *
 * @param SlotBgType:num
 * @text Background Type
 * @parent SlotWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param SlotRect:func
 * @text JS: X, Y, W, H
 * @parent SlotWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const commandWindowRect = this.commandWindowRect();\nconst x = this.statusWidth();\nconst y = commandWindowRect.y + commandWindowRect.height;\nconst width = Graphics.boxWidth - this.statusWidth();\nconst height = this.mainAreaHeight() - commandWindowRect.height;\nreturn new Rectangle(x, y, width, height);"
 *
 * @param ItemWindow
 * @text Item Window
 *
 * @param ItemBgType:num
 * @text Background Type
 * @parent ItemWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param ItemRect:func
 * @text JS: X, Y, W, H
 * @parent ItemWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "return this.slotWindowRect();"
 *
 */
/* ----------------------------------------------------------------------------
 * Status Menu Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~StatusMenu:
 *
 * @param ProfileWindow
 * @text Profile Window
 *
 * @param ProfileBgType:num
 * @text Background Type
 * @parent ProfileWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param ProfileRect:func
 * @text JS: X, Y, W, H
 * @parent ProfileWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const width = Graphics.boxWidth;\nconst height = this.profileHeight();\nconst x = 0;\nconst y = this.mainAreaBottom() - height;\nreturn new Rectangle(x, y, width, height);"
 *
 * @param StatusWindow
 * @text Status Window
 *
 * @param StatusBgType:num
 * @text Background Type
 * @parent StatusWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param StatusRect:func
 * @text JS: X, Y, W, H
 * @parent StatusWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const x = 0;\nconst y = this.mainAreaTop();\nconst width = Graphics.boxWidth;\nconst height = this.statusParamsWindowRect().y - y;\nreturn new Rectangle(x, y, width, height);"
 *
 * @param StatusParamsWindow
 * @text Parameters Window
 *
 * @param StatusParamsBgType:num
 * @text Background Type
 * @parent StatusParamsWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param StatusParamsRect:func
 * @text JS: X, Y, W, H
 * @parent StatusParamsWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const width = this.statusParamsWidth();\nconst height = this.statusParamsHeight();\nconst x = 0;\nconst y = this.mainAreaBottom() - this.profileHeight() - height;\nreturn new Rectangle(x, y, width, height);"
 *
 * @param StatusEquipWindow
 * @text Equipment Window
 *
 * @param StatusEquipBgType:num
 * @text Background Type
 * @parent StatusEquipWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param StatusEquipRect:func
 * @text JS: X, Y, W, H
 * @parent StatusEquipWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const width = Graphics.boxWidth - this.statusParamsWidth();\nconst height = this.statusParamsHeight();\nconst x = this.statusParamsWidth();\nconst y = this.mainAreaBottom() - this.profileHeight() - height;\nreturn new Rectangle(x, y, width, height);"
 *
 */
/* ----------------------------------------------------------------------------
 * Options Menu Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~OptionsMenu:
 *
 * @param OptionsWindow
 * @text Options Window
 *
 * @param OptionsBgType:num
 * @text Background Type
 * @parent OptionsWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param OptionsRect:func
 * @text JS: X, Y, W, H
 * @parent OptionsWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const n = Math.min(this.maxCommands(), this.maxVisibleCommands());\nconst width = 400;\nconst height = this.calcWindowHeight(n, true);\nconst x = (Graphics.boxWidth - width) / 2;\nconst y = (Graphics.boxHeight - height) / 2;\nreturn new Rectangle(x, y, width, height);"
 *
 */
/* ----------------------------------------------------------------------------
 * Save Menu Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~SaveMenu:
 *
 * @param HelpWindow
 * @text Help Window
 *
 * @param HelpBgType:num
 * @text Background Type
 * @parent HelpWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param HelpRect:func
 * @text JS: X, Y, W, H
 * @parent HelpWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const x = 0;\nconst y = this.mainAreaTop();\nconst rows = 1;\nconst width = Graphics.boxWidth;\nconst height = this.calcWindowHeight(rows, false);\nreturn new Rectangle(x, y, width, height);"
 *
 * @param ListWindow
 * @text List Window
 *
 * @param ListBgType:num
 * @text Background Type
 * @parent ListWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param ListRect:func
 * @text JS: X, Y, W, H
 * @parent ListWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const x = 0;\nconst y = this.mainAreaTop() + this._helpWindow.height;\nconst width = Graphics.boxWidth;\nconst height = this.mainAreaHeight() - this._helpWindow.height;\nreturn new Rectangle(x, y, width, height);"
 *
 */
/* ----------------------------------------------------------------------------
 * Load Menu Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~LoadMenu:
 *
 * @param HelpWindow
 * @text Help Window
 *
 * @param HelpBgType:num
 * @text Background Type
 * @parent HelpWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param HelpRect:func
 * @text JS: X, Y, W, H
 * @parent HelpWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const x = 0;\nconst y = this.mainAreaTop();\nconst rows = 1;\nconst width = Graphics.boxWidth;\nconst height = this.calcWindowHeight(rows, false);\nreturn new Rectangle(x, y, width, height);"
 *
 * @param ListWindow
 * @text List Window
 *
 * @param ListBgType:num
 * @text Background Type
 * @parent ListWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param ListRect:func
 * @text JS: X, Y, W, H
 * @parent ListWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const x = 0;\nconst y = this.mainAreaTop() + this._helpWindow.height;\nconst width = Graphics.boxWidth;\nconst height = this.mainAreaHeight() - this._helpWindow.height;\nreturn new Rectangle(x, y, width, height);"
 *
 */
/* ----------------------------------------------------------------------------
 * Game End Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~GameEnd:
 *
 * @param CommandList:arraystruct
 * @text Command Window List
 * @type struct<Command>[]
 * @desc Window commands used by the Game End screen.
 * Add new commands here.
 * @default ["{\"Symbol:str\":\"toTitle\",\"TextStr:str\":\"Untitled\",\"TextJS:func\":\"\\\"return TextManager.toTitle;\\\"\",\"ShowJS:func\":\"\\\"return true;\\\"\",\"EnableJS:func\":\"\\\"return true;\\\"\",\"ExtJS:func\":\"\\\"return null;\\\"\",\"CallHandlerJS:func\":\"\\\"SceneManager._scene.commandToTitle();\\\"\"}","{\"Symbol:str\":\"cancel\",\"TextStr:str\":\"Untitled\",\"TextJS:func\":\"\\\"return TextManager.cancel;\\\"\",\"ShowJS:func\":\"\\\"return true;\\\"\",\"EnableJS:func\":\"\\\"return true;\\\"\",\"ExtJS:func\":\"\\\"return null;\\\"\",\"CallHandlerJS:func\":\"\\\"SceneManager._scene.popScene();\\\"\"}"]
 *
 * @param CommandBgType:num
 * @text Background Type
 * @parent CommandList:arraystruct
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param CommandRect:func
 * @text JS: X, Y, W, H
 * @parent CommandList:arraystruct
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const rows = 2;\nconst width = this.mainCommandWidth();\nconst height = this.calcWindowHeight(rows, true);\nconst x = (Graphics.boxWidth - width) / 2;\nconst y = (Graphics.boxHeight - height) / 2;\nreturn new Rectangle(x, y, width, height);"
 *
 */
/* ----------------------------------------------------------------------------
 * Shop Menu Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~ShopMenu:
 *
 * @param HelpWindow
 * @text Help Window
 *
 * @param HelpBgType:num
 * @text Background Type
 * @parent HelpWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param HelpRect:func
 * @text JS: X, Y, W, H
 * @parent HelpWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const wx = 0;\nconst wy = this.helpAreaTop();\nconst ww = Graphics.boxWidth;\nconst wh = this.helpAreaHeight();\nreturn new Rectangle(wx, wy, ww, wh);"
 *
 * @param GoldWindow
 * @text Gold Window
 *
 * @param GoldBgType:num
 * @text Background Type
 * @parent GoldWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param GoldRect:func
 * @text JS: X, Y, W, H
 * @parent GoldWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const rows = 1;\nconst width = this.mainCommandWidth();\nconst height = this.calcWindowHeight(rows, true);\nconst x = Graphics.boxWidth - width;\nconst y = this.mainAreaTop();\nreturn new Rectangle(x, y, width, height);"
 *
 * @param CommandWindow
 * @text Command Window
 *
 * @param CommandBgType:num
 * @text Background Type
 * @parent CommandWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param CommandRect:func
 * @text JS: X, Y, W, H
 * @parent CommandWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const x = 0;\nconst y = this.mainAreaTop();\nconst rows = 1;\nconst width = this._goldWindow.x;\nconst height = this.calcWindowHeight(rows, true);\nreturn new Rectangle(x, y, width, height);"
 *
 * @param DummyWindow
 * @text Dummy Window
 *
 * @param DummyBgType:num
 * @text Background Type
 * @parent DummyWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param DummyRect:func
 * @text JS: X, Y, W, H
 * @parent DummyWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const x = 0;\nconst y = this._commandWindow.y + this._commandWindow.height;\nconst width = Graphics.boxWidth;\nconst height = this.mainAreaHeight() - this._commandWindow.height;\nreturn new Rectangle(x, y, width, height);"
 *
 * @param NumberWindow
 * @text Number Window
 *
 * @param NumberBgType:num
 * @text Background Type
 * @parent NumberWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param NumberRect:func
 * @text JS: X, Y, W, H
 * @parent NumberWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const x = 0;\nconst y = this._dummyWindow.y;\nconst width = Graphics.boxWidth - this.statusWidth();\nconst height = this._dummyWindow.height;\nreturn new Rectangle(x, y, width, height);"
 *
 * @param StatusWindow
 * @text Status Window
 *
 * @param StatusBgType:num
 * @text Background Type
 * @parent StatusWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param StatusRect:func
 * @text JS: X, Y, W, H
 * @parent StatusWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const width = this.statusWidth();\nconst height = this._dummyWindow.height;\nconst x = Graphics.boxWidth - width;\nconst y = this._dummyWindow.y;\nreturn new Rectangle(x, y, width, height);"
 *
 * @param BuyWindow
 * @text Buy Window
 *
 * @param BuyBgType:num
 * @text Background Type
 * @parent BuyWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param BuyRect:func
 * @text JS: X, Y, W, H
 * @parent BuyWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const x = 0;\nconst y = this._dummyWindow.y;\nconst width = Graphics.boxWidth - this.statusWidth();\nconst height = this._dummyWindow.height;\nreturn new Rectangle(x, y, width, height);"
 *
 * @param CategoryWindow
 * @text Category Window
 *
 * @param CategoryBgType:num
 * @text Background Type
 * @parent CategoryWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param CategoryRect:func
 * @text JS: X, Y, W, H
 * @parent CategoryWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const x = 0;\nconst y = this._dummyWindow.y;\nconst rows = 1;\nconst width = Graphics.boxWidth;\nconst height = this.calcWindowHeight(rows, true);\nreturn new Rectangle(x, y, width, height);"
 *
 * @param SellWindow
 * @text Sell Window
 *
 * @param SellBgType:num
 * @text Background Type
 * @parent SellWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param SellRect:func
 * @text JS: X, Y, W, H
 * @parent SellWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const x = 0;\nconst y = this._categoryWindow.y + this._categoryWindow.height;\nconst width = Graphics.boxWidth;\nconst height =\n    this.mainAreaHeight() -\n    this._commandWindow.height -\n    this._categoryWindow.height;\nreturn new Rectangle(x, y, width, height);"
 *
 */
/* ----------------------------------------------------------------------------
 * Name Menu Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~NameMenu:
 *
 * @param EditWindow
 * @text Edit Window
 *
 * @param EditBgType:num
 * @text Background Type
 * @parent EditWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param EditRect:func
 * @text JS: X, Y, W, H
 * @parent EditWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const rows = 9;\nconst inputWindowHeight = this.calcWindowHeight(rows, true);\nconst padding = $gameSystem.windowPadding();\nconst width = 600;\nconst height = Math.min(ImageManager.faceHeight + padding * 2, this.mainAreaHeight() - inputWindowHeight);\nconst x = (Graphics.boxWidth - width) / 2;\nconst y = (this.mainAreaHeight() - (height + inputWindowHeight)) / 2 + this.mainAreaTop();\nreturn new Rectangle(x, y, width, height);"
 *
 * @param InputWindow
 * @text Input Window
 *
 * @param InputBgType:num
 * @text Background Type
 * @parent InputWindow
 * @type select
 * @option 0 - Window
 * @value 0
 * @option 1 - Dim
 * @value 1
 * @option 2 - Transparent
 * @value 2
 * @desc Select background type for this window.
 * @default 0
 *
 * @param InputRect:func
 * @text JS: X, Y, W, H
 * @parent InputWindow
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const x = this._editWindow.x;\nconst y = this._editWindow.y + this._editWindow.height;\nconst rows = 9;\nconst width = this._editWindow.width;\nconst height = this.calcWindowHeight(rows, true);\nreturn new Rectangle(x, y, width, height);"
 *
 */
/* ----------------------------------------------------------------------------
 * Title Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Title:
 *
 * @param TitleScreen
 * @text Title Screen
 *
 * @param DocumentTitleFmt:str
 * @text Document Title Format
 * @parent TitleScreen
 * @desc Format to display text in document title.
 * %1 - Main Title, %2 - Subtitle, %3 - Version
 * @default %1: %2 - Version %3
 *
 * @param Subtitle:str
 * @text Subtitle
 * @parent TitleScreen
 * @desc Subtitle to be displayed under the title name.
 * @default Subtitle
 *
 * @param Version:str
 * @text Version
 * @parent TitleScreen
 * @desc Version to be display in the title screen corner.
 * @default 0.00
 *
 * @param drawGameTitle:func
 * @text JS: Draw Title
 * @type note
 * @parent TitleScreen
 * @desc Code used to draw the game title.
 * @default "const x = 20;\nconst y = Graphics.height / 4;\nconst maxWidth = Graphics.width - x * 2;\nconst text = $dataSystem.gameTitle;\nconst bitmap = this._gameTitleSprite.bitmap;\nbitmap.fontFace = $gameSystem.mainFontFace();\nbitmap.outlineColor = \"black\";\nbitmap.outlineWidth = 8;\nbitmap.fontSize = 72;\nbitmap.drawText(text, x, y, maxWidth, 48, \"center\");"
 *
 * @param drawGameSubtitle:func
 * @text JS: Draw Subtitle
 * @type note
 * @parent TitleScreen
 * @desc Code used to draw the game subtitle.
 * @default "const x = 20;\nconst y = Graphics.height / 4 + 72;\nconst maxWidth = Graphics.width - x * 2;\nconst text = Scene_Title.subtitle;\nconst bitmap = this._gameTitleSprite.bitmap;\nbitmap.fontFace = $gameSystem.mainFontFace();\nbitmap.outlineColor = \"black\";\nbitmap.outlineWidth = 6;\nbitmap.fontSize = 48;\nbitmap.drawText(text, x, y, maxWidth, 48, \"center\");"
 *
 * @param drawGameVersion:func
 * @text JS: Draw Version
 * @type note
 * @parent TitleScreen
 * @desc Code used to draw the game version.
 * @default "const bitmap = this._gameTitleSprite.bitmap;\nconst x = 0;\nconst y = Graphics.height - 20;\nconst width = Math.round(Graphics.width / 4);\nconst height = 20;\nconst c1 = ColorManager.dimColor1();\nconst c2 = ColorManager.dimColor2();\nconst text = 'Version ' + Scene_Title.version;\nbitmap.gradientFillRect(x, y, width, height, c1, c2);\nbitmap.fontFace = $gameSystem.mainFontFace();\nbitmap.outlineColor = \"black\";\nbitmap.outlineWidth = 3;\nbitmap.fontSize = 16;\nbitmap.drawText(text, x + 4, y, Graphics.width, height, \"left\");"
 *
 * @param CommandRect:func
 * @text JS: X, Y, W, H
 * @parent TitleScreen
 * @type note
 * @desc Code used to determine the dimensions for this window.
 * @default "const offsetX = $dataSystem.titleCommandWindow.offsetX;\nconst offsetY = $dataSystem.titleCommandWindow.offsetY;\nconst rows = this.commandWindowRows();\nconst width = this.mainCommandWidth();\nconst height = this.calcWindowHeight(rows, true);\nconst x = (Graphics.boxWidth - width) / 2 + offsetX;\nconst y = Graphics.boxHeight - height - 96 + offsetY;\nreturn new Rectangle(x, y, width, height);"
 *
 * @param ButtonFadeSpeed:num
 * @text Button Fade Speed
 * @parent TitleScreen
 * @type number
 * @min 1
 * @max 255
 * @desc Speed at which the buttons fade in at (1-255).
 * @default 4
 *
 */
/* ----------------------------------------------------------------------------
 * Parameter Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Param:
 *
 * @param DisplayedParams:arraystr
 * @text Displayed Parameters
 * @type combo[]
 * @option MaxHP
 * @option MaxMP
 * @option ATK
 * @option DEF
 * @option MAT
 * @option MDF
 * @option AGI
 * @option LUK
 * @option HIT
 * @option EVA
 * @option CRI
 * @option CEV
 * @option MEV
 * @option MRF
 * @option CNT
 * @option HRG
 * @option MRG
 * @option TRG
 * @option TGR
 * @option GRD
 * @option REC
 * @option PHA
 * @option MCR
 * @option TCR
 * @option PDR
 * @option MDR
 * @option FDR
 * @option EXR
 * @desc A list of the parameters that will be displayed in-game.
 * @default ["ATK","DEF","MAT","MDF","AGI","LUK"]
 *
 * @param ExtDisplayedParams:arraystr
 * @text Extended Parameters
 * @parent DisplayedParams:arraystr
 * @type combo[]
 * @option MaxHP
 * @option MaxMP
 * @option ATK
 * @option DEF
 * @option MAT
 * @option MDF
 * @option AGI
 * @option LUK
 * @option HIT
 * @option EVA
 * @option CRI
 * @option CEV
 * @option MEV
 * @option MRF
 * @option CNT
 * @option HRG
 * @option MRG
 * @option TRG
 * @option TGR
 * @option GRD
 * @option REC
 * @option PHA
 * @option MCR
 * @option TCR
 * @option PDR
 * @option MDR
 * @option FDR
 * @option EXR
 * @desc The list shown in extended scenes (for other VisuStella plugins).
 * @default ["MaxHP","MaxMP","ATK","DEF","MAT","MDF","AGI","LUK"]
 *
 * @param BasicParameters
 * @text Basic Parameters
 *
 * @param CrisisRate:num
 * @text HP Crisis Rate
 * @parent BasicParameters
 * @desc HP Ratio at which a battler can be considered in crisis mode.
 * @default 0.25
 *
 * @param BasicParameterFormula:func
 * @text JS: Formula
 * @parent BasicParameters
 * @type note
 * @desc Formula used to determine the total value all 8 basic parameters: MaxHP, MaxMP, ATK, DEF, MAT, MDF, AGI, LUK.
 * @default "// Determine the variables used in this calculation.\nlet paramId = arguments[0];\nlet base = this.paramBase(paramId);\nlet plus = this.paramPlus(paramId);\nlet paramRate = this.paramRate(paramId);\nlet buffRate = this.paramBuffRate(paramId);\nlet flatBonus = this.paramFlatBonus(paramId);\n\n// Formula to determine total parameter value.\nlet value = (base + plus) * paramRate * buffRate + flatBonus;\n\n// Determine the limits\nconst maxValue = this.paramMax(paramId);\nconst minValue = this.paramMin(paramId);\n\n// Final value\nreturn Math.round(value.clamp(minValue, maxValue));"
 *
 * @param BasicParamCaps
 * @text Parameter Caps
 * @parent BasicParameters
 *
 * @param BasicActorParamCaps
 * @text Actors
 * @parent BasicParamCaps
 *
 * @param BasicActorParamMax0:str
 * @text MaxHP Cap
 * @parent BasicActorParamCaps
 * @desc Formula used to determine MaxHP cap.
 * Use 0 if you don't want a cap for this parameter.
 * @default 9999
 *
 * @param BasicActorParamMax1:str
 * @text MaxMP Cap
 * @parent BasicActorParamCaps
 * @desc Formula used to determine MaxMP cap.
 * Use 0 if you don't want a cap for this parameter.
 * @default 9999
 *
 * @param BasicActorParamMax2:str
 * @text ATK Cap
 * @parent BasicActorParamCaps
 * @desc Formula used to determine ATK cap.
 * Use 0 if you don't want a cap for this parameter.
 * @default 999
 *
 * @param BasicActorParamMax3:str
 * @text DEF Cap
 * @parent BasicActorParamCaps
 * @desc Formula used to determine DEF cap.
 * Use 0 if you don't want a cap for this parameter.
 * @default 999
 *
 * @param BasicActorParamMax4:str
 * @text MAT Cap
 * @parent BasicActorParamCaps
 * @desc Formula used to determine MAT cap.
 * Use 0 if you don't want a cap for this parameter.
 * @default 999
 *
 * @param BasicActorParamMax5:str
 * @text MDF Cap
 * @parent BasicActorParamCaps
 * @desc Formula used to determine MDF cap.
 * Use 0 if you don't want a cap for this parameter.
 * @default 999
 *
 * @param BasicActorParamMax6:str
 * @text AGI Cap
 * @parent BasicActorParamCaps
 * @desc Formula used to determine AGI cap.
 * Use 0 if you don't want a cap for this parameter.
 * @default 999
 *
 * @param BasicActorParamMax7:str
 * @text LUK Cap
 * @parent BasicActorParamCaps
 * @desc Formula used to determine LUK cap.
 * Use 0 if you don't want a cap for this parameter.
 * @default 999
 *
 * @param BasicEnemyParamCaps
 * @text Enemies
 * @parent BasicParamCaps
 *
 * @param BasicEnemyParamMax0:str
 * @text MaxHP Cap
 * @parent BasicEnemyParamCaps
 * @desc Formula used to determine MaxHP cap.
 * Use 0 if you don't want a cap for this parameter.
 * @default 999999
 *
 * @param BasicEnemyParamMax1:str
 * @text MaxMP Cap
 * @parent BasicEnemyParamCaps
 * @desc Formula used to determine MaxMP cap.
 * Use 0 if you don't want a cap for this parameter.
 * @default 9999
 *
 * @param BasicEnemyParamMax2:str
 * @text ATK Cap
 * @parent BasicEnemyParamCaps
 * @desc Formula used to determine ATK cap.
 * Use 0 if you don't want a cap for this parameter.
 * @default 999
 *
 * @param BasicEnemyParamMax3:str
 * @text DEF Cap
 * @parent BasicEnemyParamCaps
 * @desc Formula used to determine DEF cap.
 * Use 0 if you don't want a cap for this parameter.
 * @default 999
 *
 * @param BasicEnemyParamMax4:str
 * @text MAT Cap
 * @parent BasicEnemyParamCaps
 * @desc Formula used to determine MAT cap.
 * Use 0 if you don't want a cap for this parameter.
 * @default 999
 *
 * @param BasicEnemyParamMax5:str
 * @text MDF Cap
 * @parent BasicEnemyParamCaps
 * @desc Formula used to determine MDF cap.
 * Use 0 if you don't want a cap for this parameter.
 * @default 999
 *
 * @param BasicEnemyParamMax6:str
 * @text AGI Cap
 * @parent BasicEnemyParamCaps
 * @desc Formula used to determine AGI cap.
 * Use 0 if you don't want a cap for this parameter.
 * @default 999
 *
 * @param BasicEnemyParamMax7:str
 * @text LUK Cap
 * @parent BasicEnemyParamCaps
 * @desc Formula used to determine LUK cap.
 * Use 0 if you don't want a cap for this parameter.
 * @default 999
 *
 * @param XParameters
 * @text X Parameters
 *
 * @param XParameterFormula:func
 * @text JS: Formula
 * @parent XParameters
 * @type note
 * @desc Formula used to determine the total value all 10 X parameters: HIT, EVA, CRI, CEV, MEV, MRF, CNT, HRG, MRG, TRG.
 * @default "// Determine the variables used in this calculation.\nlet xparamId = arguments[0];\nlet base = this.traitsSum(Game_BattlerBase.TRAIT_XPARAM, xparamId);\nlet plus = this.xparamPlus(xparamId);\nlet paramRate = this.xparamRate(xparamId);\nlet flatBonus = this.xparamFlatBonus(xparamId);\n\n// Formula to determine total parameter value.\nlet value = (base + plus) * paramRate + flatBonus;\n\n// Final value\nreturn value;"
 *
 * @param XParamVocab
 * @text Vocabulary
 * @parent XParameters
 *
 * @param XParamVocab0:str
 * @text HIT
 * @parent XParamVocab
 * @desc The in-game vocabulary used for this X Parameter.
 * @default Hit
 *
 * @param XParamVocab1:str
 * @text EVA
 * @parent XParamVocab
 * @desc The in-game vocabulary used for this X Parameter.
 * @default Evasion
 *
 * @param XParamVocab2:str
 * @text CRI
 * @parent XParamVocab
 * @desc The in-game vocabulary used for this X Parameter.
 * @default Crit.Rate
 *
 * @param XParamVocab3:str
 * @text CEV
 * @parent XParamVocab
 * @desc The in-game vocabulary used for this X Parameter.
 * @default Crit.Evade
 *
 * @param XParamVocab4:str
 * @text MEV
 * @parent XParamVocab
 * @desc The in-game vocabulary used for this X Parameter.
 * @default Magic Evade
 *
 * @param XParamVocab5:str
 * @text MRF
 * @parent XParamVocab
 * @desc The in-game vocabulary used for this X Parameter.
 * @default Magic Reflect
 *
 * @param XParamVocab6:str
 * @text CNT
 * @parent XParamVocab
 * @desc The in-game vocabulary used for this X Parameter.
 * @default Counter
 *
 * @param XParamVocab7:str
 * @text HRG
 * @parent XParamVocab
 * @desc The in-game vocabulary used for this X Parameter.
 * @default HP Regen
 *
 * @param XParamVocab8:str
 * @text MRG
 * @parent XParamVocab
 * @desc The in-game vocabulary used for this X Parameter.
 * @default MP Regen
 *
 * @param XParamVocab9:str
 * @text TRG
 * @parent XParamVocab
 * @desc The in-game vocabulary used for this X Parameter.
 * @default TP Regen
 *
 * @param SParameters
 * @text S Parameters
 *
 * @param SParameterFormula:func
 * @text JS: Formula
 * @parent SParameters
 * @type note
 * @desc Formula used to determine the total value all 10 S parameters: TGR, GRD, REC, PHA, MCR, TCR, PDR, MDR, FDR, EXR.
 * @default "// Determine the variables used in this calculation.\nlet sparamId = arguments[0];\nlet base = this.traitsPi(Game_BattlerBase.TRAIT_SPARAM, sparamId);\nlet plus = this.sparamPlus(sparamId);\nlet paramRate = this.sparamRate(sparamId);\nlet flatBonus = this.sparamFlatBonus(sparamId);\n\n// Formula to determine total parameter value.\nlet value = (base + plus) * paramRate + flatBonus;\n\n// Final value\nreturn value;"
 *
 * @param SParamVocab
 * @text Vocabulary
 * @parent SParameters
 *
 * @param SParamVocab0:str
 * @text TGR
 * @parent SParamVocab
 * @desc The in-game vocabulary used for this S Parameter.
 * @default Aggro
 *
 * @param SParamVocab1:str
 * @text GRD
 * @parent SParamVocab
 * @desc The in-game vocabulary used for this S Parameter.
 * @default Guard
 *
 * @param SParamVocab2:str
 * @text REC
 * @parent SParamVocab
 * @desc The in-game vocabulary used for this S Parameter.
 * @default Recovery
 *
 * @param SParamVocab3:str
 * @text PHA
 * @parent SParamVocab
 * @desc The in-game vocabulary used for this S Parameter.
 * @default Item Effect
 *
 * @param SParamVocab4:str
 * @text MCR
 * @parent SParamVocab
 * @desc The in-game vocabulary used for this S Parameter.
 * @default MP Cost
 *
 * @param SParamVocab5:str
 * @text TCR
 * @parent SParamVocab
 * @desc The in-game vocabulary used for this S Parameter.
 * @default TP Charge
 *
 * @param SParamVocab6:str
 * @text PDR
 * @parent SParamVocab
 * @desc The in-game vocabulary used for this S Parameter.
 * @default Physical DMG
 *
 * @param SParamVocab7:str
 * @text MDR
 * @parent SParamVocab
 * @desc The in-game vocabulary used for this S Parameter.
 * @default Magical DMG
 *
 * @param SParamVocab8:str
 * @text FDR
 * @parent SParamVocab
 * @desc The in-game vocabulary used for this S Parameter.
 * @default Floor DMG
 *
 * @param SParamVocab9:str
 * @text EXR
 * @parent SParamVocab
 * @desc The in-game vocabulary used for this S Parameter.
 * @default EXP Gain
 *
 * @param Icons
 * @text Icons
 *
 * @param DrawIcons:eval
 * @text Draw Icons?
 * @parent Icons
 * @type boolean
 * @on Draw
 * @off Don't Draw
 * @desc Draw icons next to parameter names?
 * @default true
 *
 * @param IconParam0:str
 * @text MaxHP
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 84
 *
 * @param IconParam1:str
 * @text MaxMP
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 165
 *
 * @param IconParam2:str
 * @text ATK
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 76
 *
 * @param IconParam3:str
 * @text DEF
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 81
 *
 * @param IconParam4:str
 * @text MAT
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 101
 *
 * @param IconParam5:str
 * @text MDF
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 133
 *
 * @param IconParam6:str
 * @text AGI
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 140
 *
 * @param IconParam7:str
 * @text LUK
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 87
 *
 * @param IconXParam0:str
 * @text HIT
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 102
 *
 * @param IconXParam1:str
 * @text EVA
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 82
 *
 * @param IconXParam2:str
 * @text CRI
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 78
 *
 * @param IconXParam3:str
 * @text CEV
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 82
 *
 * @param IconXParam4:str
 * @text MEV
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 171
 *
 * @param IconXParam5:str
 * @text MRF
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 222
 *
 * @param IconXParam6:str
 * @text CNT
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 77
 *
 * @param IconXParam7:str
 * @text HRG
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 72
 *
 * @param IconXParam8:str
 * @text MRG
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 72
 *
 * @param IconXParam9:str
 * @text TRG
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 72
 *
 * @param IconSParam0:str
 * @text TGR
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 5
 *
 * @param IconSParam1:str
 * @text GRD
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 128
 *
 * @param IconSParam2:str
 * @text REC
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 72
 *
 * @param IconSParam3:str
 * @text PHA
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 176
 *
 * @param IconSParam4:str
 * @text MCR
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 165
 *
 * @param IconSParam5:str
 * @text TCR
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 164
 *
 * @param IconSParam6:str
 * @text PDR
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 76
 *
 * @param IconSParam7:str
 * @text MDR
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 79
 *
 * @param IconSParam8:str
 * @text FDR
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 141
 *
 * @param IconSParam9:str
 * @text EXR
 * @parent Icons
 * @desc Icon used for this parameter.
 * @default 73
 *
 */
/* ----------------------------------------------------------------------------
 * Commands Struct
 * ----------------------------------------------------------------------------
 */
/*~struct~Command:
 *
 * @param Symbol:str
 * @text Symbol
 * @desc The symbol used for this command.
 * @default Symbol
 *
 * @param TextStr:str
 * @text STR: Text
 * @desc Displayed text used for this title command.
 * If this has a value, ignore the JS: Text version.
 * @default Untitled
 *
 * @param TextJS:func
 * @text JS: Text
 * @type note
 * @desc JavaScript code used to determine string used for the displayed name.
 * @default "return 'Text';"
 *
 * @param ShowJS:func
 * @text JS: Show
 * @type note
 * @desc JavaScript code used to determine if the item is shown or not.
 * @default "return true;"
 *
 * @param EnableJS:func
 * @text JS: Enable
 * @type note
 * @desc JavaScript code used to determine if the item is enabled or not.
 * @default "return true;"
 *
 * @param ExtJS:func
 * @text JS: Ext
 * @type note
 * @desc JavaScript code used to determine any ext data that should be added.
 * @default "return null;"
 *
 * @param CallHandlerJS:func
 * @text JS: Run Code
 * @type note
 * @desc JavaScript code that runs once this command is selected.
 * @default ""
 *
 */
/* ----------------------------------------------------------------------------
 * Title Picture Buttons
 * ----------------------------------------------------------------------------
 */
/*~struct~TitlePictureButton:
 *
 * @param PictureFilename:str
 * @text Picture's Filename
 * @type file
 * @dir img/pictures/
 * @desc Filename used for the picture.
 * @default 
 *
 * @param ButtonURL:str
 * @text Button URL
 * @desc URL for the button to go to upon being clicked.
 * @default https://www.google.com/
 *
 * @param PositionJS:func
 * @text JS: Position
 * @type note
 * @desc JavaScript code that helps determine the button's Position.
 * @default "this.x = Graphics.width - this.bitmap.width - 20;\nthis.y = Graphics.height - this.bitmap.height - 20;"
 *
 * @param OnLoadJS:func
 * @text JS: On Load
 * @type note
 * @desc JavaScript code that runs once this button bitmap is loaded.
 * @default "this.opacity = 0;\nthis.visible = true;"
 *
 * @param CallHandlerJS:func
 * @text JS: Run Code
 * @type note
 * @desc JavaScript code that runs once this button is pressed.
 * @default "const url = this._data.ButtonURL;\nVisuMZ.openURL(url);"
 *
 */
/* ----------------------------------------------------------------------------
 * UI Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~UI:
 *
 * @param UIArea
 * @text UI Area
 *
 * @param FadeSpeed:num
 * @text Fade Speed
 * @parent UIArea
 * @desc Default fade speed for transitions.
 * @default 24
 *
 * @param BoxMargin:num
 * @text Box Margin
 * @parent UIArea
 * @type number
 * @min 0
 * @desc Set the margin in pixels for the screen borders.
 * Default: 4
 * @default 4
 *
 * @param CommandWidth:num
 * @text Command Window Width
 * @parent UIArea
 * @type number
 * @min 1
 * @desc Sets the width for standard Command Windows.
 * Default: 240
 * @default 240
 *
 * @param BottomHelp:eval
 * @text Bottom Help Window
 * @parent UIArea
 * @type boolean
 * @on Bottom
 * @off Top
 * @desc Put the Help Window at the bottom of the screen?
 * @default false
 *
 * @param RightMenus:eval
 * @text Right Aligned Menus
 * @parent UIArea
 * @type boolean
 * @on Right
 * @off Left
 * @desc Put most command windows to the right side of the screen.
 * @default true
 *
 * @param ShowButtons:eval
 * @text Show Buttons
 * @parent UIArea
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show clickable buttons in your game?
 * This will affect all buttons.
 * @default true
 *
 * @param cancelShowButton:eval
 * @text Show Cancel Button
 * @parent ShowButtons:eval
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show cancel button?
 * If 'Show Buttons' is false, this will be hidden.
 * @default true
 *
 * @param menuShowButton:eval
 * @text Show Menu Button
 * @parent ShowButtons:eval
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show main menu button from the map scene?
 * If 'Show Buttons' is false, this will be hidden.
 * @default true
 *
 * @param pagedownShowButton:eval
 * @text Show Page Up/Down
 * @parent ShowButtons:eval
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show page up/down buttons?
 * If 'Show Buttons' is false, this will be hidden.
 * @default true
 *
 * @param numberShowButton:eval
 * @text Show Number Buttons
 * @parent ShowButtons:eval
 * @type boolean
 * @on Show
 * @off Hide
 * @desc Show number adjustment buttons?
 * If 'Show Buttons' is false, this will be hidden.
 * @default true
 *
 * @param ButtonHeight:num
 * @text Button Area Height
 * @parent UIArea
 * @type number
 * @min 1
 * @desc Sets the height for the button area.
 * Default: 52
 * @default 52
 *
 * @param BottomButtons:eval
 * @text Bottom Buttons
 * @parent UIArea
 * @type boolean
 * @on Bottom
 * @off Top
 * @desc Put the buttons at the bottom of the screen?
 * @default false
 *
 * @param SideButtons:eval
 * @text Side Buttons
 * @parent UIArea
 * @type boolean
 * @on Side
 * @off Normal
 * @desc Push buttons to the side of the UI if there is room.
 * @default true
 *
 * @param LargerResolution
 * @text Larger Resolution
 *
 * @param RepositionActors:eval
 * @text Reposition Actors
 * @parent LargerResolution
 * @type boolean
 * @on Reposition
 * @off Keep As Is
 * @desc Update the position of actors in battle if the screen resolution has changed. Ignore if using Battle Core.
 * @default true
 *
 * @param RepositionEnemies:eval
 * @text Reposition Enemies
 * @parent LargerResolution
 * @type boolean
 * @on Reposition
 * @off Keep As Is
 * @desc Update the position of enemies in battle if the screen resolution has changed.
 * @default true
 *
 * @param MenuObjects
 * @text Menu Objects
 *
 * @param LvExpGauge:eval
 * @text Level -> EXP Gauge
 * @parent MenuObjects
 * @type boolean
 * @on Draw Gauge
 * @off Keep As Is
 * @desc Draw an EXP Gauge under the drawn level.
 * @default true
 *
 * @param ParamArrow:str
 * @text Parameter Arrow
 * @parent MenuObjects
 * @desc The arrow used to show changes in the parameter values.
 * @default →
 *
 * @param TextCodeSupport
 * @text Text Code Support
 *
 * @param TextCodeClassNames:eval
 * @text Class Names
 * @parent TextCodeSupport
 * @type boolean
 * @on Suport Text Codes
 * @off Normal Text
 * @desc Make class names support text codes?
 * @default true
 *
 * @param TextCodeNicknames:eval
 * @text Nicknames
 * @parent TextCodeSupport
 * @type boolean
 * @on Suport Text Codes
 * @off Normal Text
 * @desc Make nicknames support text codes?
 * @default true
 *
 */
/* ----------------------------------------------------------------------------
 * Window Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~Window:
 *
 * @param WindowDefaults
 * @text Defaults
 *
 * @param EnableMasking:eval
 * @text Enable Masking
 * @parent WindowDefaults
 * @type boolean
 * @on Masking On
 * @off Masking Off
 * @desc Enable window masking (windows hide other windows behind 
 * them)? WARNING: Turning it on can obscure data.
 * @default false
 *
 * @param LineHeight:num
 * @text Line Height
 * @parent WindowDefaults
 * @desc Default line height used for standard windows.
 * Default: 36
 * @default 36
 *
 * @param ItemPadding:num
 * @text Item Padding
 * @parent WindowDefaults
 * @desc Default line padding used for standard windows.
 * Default: 8
 * @default 8
 *
 * @param BackOpacity:num
 * @text Back Opacity
 * @parent WindowDefaults
 * @desc Default back opacity used for standard windows.
 * Default: 192
 * @default 192
 *
 * @param TranslucentOpacity:num
 * @text Translucent Opacity
 * @parent WindowDefaults
 * @desc Default translucent opacity used for standard windows.
 * Default: 160
 * @default 160
 *
 * @param OpenSpeed:num
 * @text Window Opening Speed
 * @parent WindowDefaults
 * @desc Default open speed used for standard windows.
 * Default: 32 (Use a number between 0-255)
 * @default 32
 * @default 24
 *
 * @param ColSpacing:num
 * @text Column Spacing
 * @parent WindowDefaults
 * @desc Default column spacing for selectable windows.
 * Default: 8
 * @default 8
 *
 * @param RowSpacing:num
 * @text Row Spacing
 * @parent WindowDefaults
 * @desc Default row spacing for selectable windows.
 * Default: 4
 * @default 4
 * 
 * @param SelectableItems
 * @text Selectable Items
 *
 * @param ShowItemBackground:eval
 * @text Show Background?
 * @parent SelectableItems
 * @type boolean
 * @on Show Backgrounds
 * @off No backgrounds.
 * @desc Selectable menu items have dark boxes behind them. Show them?
 * @default true
 *
 * @param ItemHeight:num
 * @text Item Height Padding
 * @parent SelectableItems
 * @desc Default padding for selectable items.
 * Default: 8
 * @default 8
 *
 * @param DrawItemBackgroundJS:func
 * @text JS: Draw Background
 * @parent SelectableItems
 * @type note
 * @desc Code used to draw the background rectangle behind clickable menu objects
 * @default "const rect = arguments[0];\nconst c1 = ColorManager.itemBackColor1();\nconst c2 = ColorManager.itemBackColor2();\nconst x = rect.x;\nconst y = rect.y;\nconst w = rect.width;\nconst h = rect.height;\nthis.contentsBack.gradientFillRect(x, y, w, h, c1, c2, true);\nthis.contentsBack.strokeRect(x, y, w, h, c1);"
 */
/* ----------------------------------------------------------------------------
 * JS Quick Function Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~jsQuickFunc:
 *
 * @param FunctionName:str
 * @text Function Name
 * @desc The function's name in the global namespace.
 * Will not overwrite functions/variables of the same name.
 * @default Untitled
 *
 * @param CodeJS:json
 * @text JS: Code
 * @type note
 * @desc Run this code when using the function.
 * @default "// Insert this as a function anywhere you can input code\n// such as Script Calls or Conditional Branch Scripts.\n\n// Process Code\n"
 *
 */
/* ----------------------------------------------------------------------------
 * Screen Shake Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~ScreenShake:
 *
 * @param DefaultStyle:str
 * @text Default Style
 * @type select
 * @option Original
 * @value original
 * @option Random
 * @value random
 * @option Horizontal
 * @value horizontal
 * @option Vertical
 * @value vertical
 * @desc The default style used for screen shakes.
 * @default random
 *
 * @param originalJS:func
 * @text JS: Original Style
 * @type note
 * @desc This code gives you control over screen shake for this
 * screen shake style.
 * @default "// Calculation\nthis.x += Math.round($gameScreen.shake());"
 *
 * @param randomJS:func
 * @text JS: Random Style
 * @type note
 * @desc This code gives you control over screen shake for this
 * screen shake style.
 * @default "// Calculation\n// Original Formula by Aries of Sheratan\nconst power = $gameScreen._shakePower * 0.75;\nconst speed = $gameScreen._shakeSpeed * 0.60;\nconst duration = $gameScreen._shakeDuration;\nthis.x += Math.round(Math.randomInt(power) - Math.randomInt(speed)) * (Math.min(duration, 30) * 0.5);\nthis.y += Math.round(Math.randomInt(power) - Math.randomInt(speed)) * (Math.min(duration, 30) * 0.5);"
 *
 * @param horzJS:func
 * @text JS: Horizontal Style
 * @type note
 * @desc This code gives you control over screen shake for this
 * screen shake style.
 * @default "// Calculation\n// Original Formula by Aries of Sheratan\nconst power = $gameScreen._shakePower * 0.75;\nconst speed = $gameScreen._shakeSpeed * 0.60;\nconst duration = $gameScreen._shakeDuration;\nthis.x += Math.round(Math.randomInt(power) - Math.randomInt(speed)) * (Math.min(duration, 30) * 0.5);"
 *
 * @param vertJS:func
 * @text JS: Vertical Style
 * @type note
 * @desc This code gives you control over screen shake for this
 * screen shake style.
 * @default "// Calculation\n// Original Formula by Aries of Sheratan\nconst power = $gameScreen._shakePower * 0.75;\nconst speed = $gameScreen._shakeSpeed * 0.60;\nconst duration = $gameScreen._shakeDuration;\nthis.y += Math.round(Math.randomInt(power) - Math.randomInt(speed)) * (Math.min(duration, 30) * 0.5);"
 *
 */
/* ----------------------------------------------------------------------------
 * Custom Parameter Settings
 * ----------------------------------------------------------------------------
 */
/*~struct~CustomParam:
 *
 * @param ParamName:str
 * @text Parameter Name
 * @desc What's the parameter's name?
 * Used for VisuStella MZ menus.
 * @default Untitled
 *
 * @param Abbreviation:str
 * @text Abbreviation
 * @parent ParamName:str
 * @desc What abbreviation do you want to use for the parameter?
 * Do not use special characters. Avoid numbers if possible.
 * @default unt
 *
 * @param Icon:num
 * @text Icon
 * @parent ParamName:str
 * @desc What icon do you want to use to represent this parameter?
 * Used for VisuStella MZ menus.
 * @default 160
 *
 * @param Type:str
 * @text Type
 * @parent ParamName:str
 * @type select
 * @option Integer (Whole Numbers Only)
 * @value integer
 * @option Float (Decimals are Allowed)
 * @value float
 * @desc What kind of number value will be returned with this parameter?
 * @default integer
 *
 * @param ValueJS:json
 * @text JS: Value
 * @type note
 * @desc Run this code when this parameter is to be returned.
 * @default "// Declare Constants\nconst user = this;\n\n// Calculations\nreturn 1;"
 *
 */
//=============================================================================

const _0x3315=['INOUTQUAD','ItemMenu','PictureEraseAll','Window_NameInput_refresh','paramRate1','SkillTypeRect','makeDocumentTitle','sparamFlat1','createBuffer','Actor','createWindowLayer','yScrollLinkedOffset','buttonAssistText2','setupCoreEasing','catchLoadError','_stored_deathColor','startShake','VOLUME_DOWN','setAnchor','StartID','mhp','markCoreEngineModified','CreateBattleSystemID','isMenuButtonAssistEnabled','ActorTPColor','INCUBIC','repeat','mainAreaTop','drawGameTitle','dimColor2','F17','SEPARATOR','determineSideButtonLayoutValid','processHandling','name','min','ColorMPCost','param','hide','EncounterRateMinimum','tilesets','OUTELASTIC','Padding','_shakeSpeed','flush','SParamVocab2','_windowLayer','top','ListRect','Game_Actor_levelUp','DummyRect','_changingClass','_dummyWindow','_CoreEngineSettings','helpWindowRect','Window_Base_drawText','EQUALS','targets','version','WARNING:\x20%1\x20has\x20already\x20been\x20declared\x0aand\x20cannot\x20be\x20used\x20as\x20a\x20Quick\x20JS\x20Function','bgm','NUMPAD8','LevelUpFullMp','fillText','MAXMP','isUseModernControls','item','ParseWeaponNotetags','floor','performEscape','_statusWindow','441429phQvnq','itemEva','commandWindowRows','Scene_GameEnd_createBackground','pagedownShowButton','IconXParam0','JUNJA','process_VisuMZ_CoreEngine_CustomParameters','drawActorClass','processTouch','drawCharacter','mainCommandWidth','paramchangeTextColor','MRF','mmp','_buttonAssistWindow','LATIN1','Scene_Boot_updateDocumentTitle','pictureButtons','CategoryRect','ActorBgType','picture','isCursorMovable','exp','Title','value','learnings','command355','areButtonsOutsideMainUI','updatePositionCoreEngineShakeRand','canUse','24RjwBaI','updatePositionCoreEngineShakeOriginal','setMoveEasingType','_inputWindow','nickname','move','advanced','terms','Scene_Map_createSpriteset','1bpeUbD','boxHeight','dashToggle','cursorPagedown','buttonAssistText5','setMute','expGaugeColor2','statusWindowRect','NumberBgType','textColor','_drawTextOutline','StatusEquipBgType','ParamChange','buttonAssistKey4','mainFontSize','_cache','StatusParamsRect','_stored_mpGaugeColor1','dimColor1','context','_coreEasingType','GoldRect','ItemBgType','ColorHPGauge1','ColorDeath','Window_NameInput_cursorPageup','drawGauge','WIN_OEM_FJ_ROYA','ConvertNumberToString','_stored_ctGaugeColor1','EXCLAMATION','tileWidth','NUMPAD5','maxLevel','targetX','isDying','itypeId','SceneManager_isGameActive','STENCIL_BUFFER_BIT','_encounterCount','_inputSpecialKeyCode','DigitGroupingLocale','MAT','initVisuMZCoreEngine','_stored_powerDownColor','_mainSprite','Scene_MenuBase_mainAreaTop','TILDE','ShopMenu','STB','destroy','Scene_Boot_startNormalGame','isFullDocumentTitle','_isButtonHidden','gaugeRate','Game_Troop_setup','createDigits','MODECHANGE','makeDeepCopy','onNameOk','goldWindowRect','subjectHitRate','ColorMPGauge2','Total','loadTitle2','resetBattleSystem','drawCurrentParam','stringKeyMap','ARRAYEVAL','processEscape','helpAreaBottom','OUTQUART','Scene_Item_create','PHA','isPressed','drawIcon','contentsBack','loadWindowskin','normalColor','isCancelled','targetSpritePosition','Basic','F21','_movementDuration','cancelShowButton','_screenY','PictureFilename','Enemy','adjustPictureAntiZoom','([\x5c+\x5c-]\x5cd+\x5c.?\x5cd+)>','KeyTAB','terminate','note','SnapshotOpacity','PixelateImageRendering','targetBackOpacity','INOUTBOUNCE','Window_Selectable_drawBackgroundRect','STR','PositionJS','getButtonAssistLocation','process_VisuMZ_CoreEngine_Settings','MRG','sqrt','setLastPluginCommandInterpreter','prototype','useDigitGroupingEx','_blank','random','_moveEasingType','snapForBackground','xparam','applyForcedGameTroopSettingsCoreEngine','drawTextEx','ProfileRect','CAPSLOCK','printError','BlurFilter','getBattleSystem','includes','defaultInputMode','KeyboardInput','IconXParam8','CANCEL','ColorExpGauge1','Game_Actor_changeClass','currentExp','createPageButtons','enemies','RepositionActors','IconXParam6','SmartEventCollisionPriority','MAX_GL_TEXTURES','Bitmap_drawText','ARRAYSTR','ONE','encounterStepsMinimum','ColorExpGauge2','_offsetX','drawBackgroundRect','DrawIcons','maxBattleMembers','stypeId','hideButtonFromView','gaugeLineHeight','titles2','_addShadow','_shouldPreventDefault','Game_Interpreter_PluginCommand','toUpperCase','ItemRect','animations','ParseItemNotetags','IconSParam5','ZOOM','mainAreaHeight','removeChild','SLASH','atbActive','_stored_normalColor','BaseTexture','actorWindowRect','initCoreEasing','COMMA','ParseTilesetNotetags','HelpRect','REPLACE','ShowJS','buttonAssistCancel','INELASTIC','_paramPlus','\x0a\x20\x20\x20\x20\x20\x20\x20\x20try\x20{\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20%2\x0a\x20\x20\x20\x20\x20\x20\x20\x20}\x20catch\x20(e)\x20{\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20if\x20($gameTemp.isPlaytest())\x20{\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20console.log(\x27JS\x20Quick\x20Function\x20\x22%1\x22\x20Error!\x27);\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20console.log(e);\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20}\x0a\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20return\x200;\x0a\x20\x20\x20\x20\x20\x20\x20\x20}\x0a\x20\x20\x20\x20','Param','SideView','Input_onKeyDown','currentClass','TGR','hpColor','Bitmap_strokeRect','_centerElementCoreEngine','updateMove','Window_Base_drawIcon','active','SParamVocab1','Scene_Unlisted','bitmap','addChildToBack','_cacheScaleX','Scene_Status_create','playCursor','makeEncounterCount','<JS\x20%1\x20%2:[\x20](.*)>','loadGameImagesCoreEngine','14551NyxfMP','openness','setCoreEngineScreenShakeStyle','xparamRate','PLUS','createJsQuickFunction','catchException','itemLineRect','open','Location','update','outlineColorGauge','buttonAssistText%1','AMPERSAND','start','Rate1','ShowDevTools','pictureId','TitleCommandList','Scene_Boot_loadSystemImages','DEF','successRate','buttonAssistSwitch','_editWindow','loadSystemImages','TRAIT_PARAM','ESC','Sprite_Gauge_gaugeRate','isAnimationForEach','maxCols','GroupDigits','_buttonType','catchNormalError','addLoadListener','XParamVocab2','targetY','Input_clear','maxItems','xparamRate1','pop','slotWindowRect','refresh','Graphics_centerElement','Game_Action_itemHit','_sideButtonLayout','BuyBgType','Game_Interpreter_command111','vertical','buttonAssistKey1','_animation','Game_Screen_initialize','isMapScrollLinked','isTriggered','onDatabaseLoaded','xparamPlus2','SceneManager_onKeyDown','round','_backgroundFilter','TextCodeClassNames','INQUART','_stored_ctGaugeColor2','hpGaugeColor1','WIN_OEM_ENLW','powerUpColor','removeAllFauxAnimations','VisuMZ_2_BattleSystemCTB','OUTQUAD','Subtitle','stencilOp','Game_Temp_initialize','addCommand','expRate','itemHeight','QoL','Speed','IconXParam4','ColorMPGauge1','moveMenuButtonSideButtonLayout','cursorLeft','isTouchedInsideFrame','PRESERVCONVERSION(%1)','NUMPAD0','Game_Action_updateLastTarget','_gamepadWait','maxLvGaugeColor1','boxWidth','sparamRateJS','startNormalGame','SCALE_MODES','dummyWindowRect','isOpenAndActive','initButtonHidden','setAction','buttonAssistWindowRect','buttonAssistKey5','randomJS','paramValueByName','DigitGroupingDamageSprites','maxGold','format','alwaysDash','VisuMZ_2_BattleSystemFTB','CodeJS','Scene_MenuBase_createBackground','updateMainMultiply','fromCharCode','_scene','length','sparam','EXSEL','process_VisuMZ_CoreEngine_jsQuickFunctions','Bitmap_measureTextWidth','xparamPlus1','sellWindowRect','pagedown','down','Scene_Name_create','windowPadding','clearStencil','parseForcedGameTroopSettingsCoreEngine','forceStencil','nw.gui','Game_Event_isCollidedWithEvents','sparamPlus','checkCacheKey','ACCEPT','valueOutlineColor','isMVAnimation','StatusEquipRect','setActorHomeRepositioned','map','StatusRect','createCancelButton','updatePlayTestF7','initDigitGrouping','params','processCursorMove','layoutSettings','_listWindow','charCode','ZERO','sparamPlus1','_onKeyPress','processMoveCommand','SLEEP','refreshDimmerBitmap','enemy','INOUTSINE','isRightInputMode','Window_Base_createTextState','right','EnableMasking','profileWindowRect','Window','processTimingData','CustomParamNames','_backSprite1','_coreEasing','iconHeight','cursorDown','isActor','drawActorSimpleStatus','framebuffer','itemHitImprovedAccuracy','BattleManager_processEscape','SELECT','isBusy','_fauxAnimationSprites','Scene_Base_createWindowLayer','IconXParam2','isSideButtonLayout','PGUP','openingSpeed','command357','Game_Picture_y','buttonAssistKey2','description','save','forceOutOfPlaytest','Flat','_stored_mpGaugeColor2','centerSprite','Scene_Map_updateMainMultiply','_pageupButton','_targetAnchor','_defaultStretchMode','_digitGroupingEx','itemBackColor2','Input_shouldPreventDefault','battleSystem','initMembers','Sprite_Animation_processSoundTimings','KEEP','WIN_OEM_RESET','Window_Selectable_cursorDown','_isPlaytest','_sellWindow','Gold','tab','IconParam6','isExpGaugeDrawn','createCustomBackgroundImages','reduce','ListBgType','transform','CTB','MINUS','EditRect','uiAreaWidth','GoldChange','GetParamIcon','_cancelButton','faces','startMove','isOptionValid','ItemBackColor1','NewGameCommonEventAll','enter','XParamVocab8','_inputString','WIN_OEM_FJ_MASSHOU','paramRate2','sin','show','ColorMaxLvGauge1','CustomParamType','GoldBgType','skillId','IconSParam0','ParseSkillNotetags','KeySHIFT','Bitmap_fillRect','scaleMode','renderNoMask','addChild','Window_Selectable_processCursorMove','setHome','useDigitGrouping','optionsWindowRect','PRINTSCREEN','ActorRect','isActiveTpb','createFauxAnimationSprite','updatePosition','Game_Actor_paramBase','Scene_MenuBase_createCancelButton','_commandList','KeyUnlisted','OutlineColorGauge','_list','evade','style','WIN_OEM_FJ_LOYA','_backgroundSprite','JSON','SParamVocab6','clearForcedGameTroopSettingsCoreEngine','_numberWindow','ENTER','Script\x20Call\x20Error','Game_BattlerBase_refresh','LevelUpFullHp','SHIFT','_statusParamsWindow','_hp','Game_Picture_x','MAXHP','Spriteset_Base_initialize','EquipMenu','performMiss','CRSEL','ImgLoad','Input_update','_hideButtons','ARRAYJSON','ColorTPCost','_menuButton','PictureEraseRange','CustomParamAbb','DisplayedParams','MDF','setCoreEngineUpdateWindowBg','Color','call','DrawItemBackgroundJS','processFauxAnimationRequests','system','initialLevel','maxLvGaugeColor2','F16','AGI','adjustSprite','_effectsContainer','drawCircle','moveCancelButtonSideButtonLayout','drawGameSubtitle','_actorWindow','748384bsKZsE','center','Game_BattlerBase_initMembers','clamp','Plus1','loadSystem','Type','buyWindowRect','makeCoreEngineCommandList','waiting','Flat2','isItem','mute','canEquip','worldTransform','anchor','_actor','SUBTRACT','create','_buyWindow','Sprite_Button_initialize','Input_pollGamepads','SceneManager_initialize','fontSize','NUM','HelpBgType','FDR','createSpriteset','Window_NameInput_cursorDown','ctrlKey','drawNewParam','powerDownColor','F20','_centerElement','buttonAssistText3','contents','hit','sparamRate1','popScene','getCombinedScrollingText','FUNC','fadeSpeed','smoothSelect','clear','pow','retrieveFauxAnimation','HELP','BoxMargin','createCommandWindow','Scene_MenuBase_createPageButtons','ParseAllNotetags','reserveCommonEvent','targetScaleY','setTargetAnchor','allowShiftScrolling','xparamFlat1','SEMICOLON','INSINE','characters','_onKeyDown','paramFlat','BTB','translucentOpacity','playMiss','FINAL','playEscape','subtitle','iconWidth','DefaultMode','destroyCoreEngineMarkedBitmaps','xparamRate2','SystemSetSideView','processAlwaysEscape','makeTargetSprites','BottomHelp','_baseTexture','Window_NameInput_cursorUp','_number','applyCoreEasing','Sprite_Actor_setActorHome','Game_System_initialize','_repositioned','ctGaugeColor1','OUTEXPO','_forcedBattleSys','buttonY','rightArrowWidth','SCROLL_LOCK','EREOF','OPEN_CURLY_BRACKET','resetFontSettings','vertJS','_index','pixelated','Window_NameInput_cursorLeft','TextJS','ParamArrow','ParseStateNotetags','updatePadding','_commandWindow','INCIRC','equips','constructor','isNormalPriority','_data','gameTitle','QwertyLayout','_backSprite2','Key%1','F18','tpCostColor','updateCoreEasing','getCustomBackgroundSettings','CancelText','Scene_Map_initialize','_dimmerSprite','outlineColorDmg','onButtonImageLoad','itemRect','exec','GoldOverlap','processKeyboardDigitChange','isSmartEventCollisionOn','createCustomParameter','drawActorLevel','calcCoreEasing','ONE_MINUS_SRC_ALPHA','%2%1%3','keypress','fillRect','toLocaleString','isBottomHelpMode','setupValueFont','_opening','sparamRate2','Wait','buttonAssistOk','drawActorExpGauge','ShowButtons','Window_NameInput_cursorRight','resize','GoldIcon','_targetOffsetX','IconParam0','REC','setSideView','Bitmap_gradientFillRect','_stored_gaugeBackColor','CLOSE_BRACKET','IconXParam1','currencyUnit','LINEAR','updateFauxAnimations','_context','OptionsBgType','Window_Gold_refresh','ParseClassNotetags','targetEvaRate','ASTERISK','F11','FontSmoothing','backspace','updateDashToggle','XParamVocab3','tpGaugeColor2','Linear','disable','changeTextColor','buttonAssistOffset1','QUESTION_MARK','isSideView','setEasingType','processCursorMoveModernControls','F13','OPEN_PAREN','left','parse','changeClass','bind','ColorCrisis','_itemWindow','Game_Interpreter_command122','isPlaytest','categoryWindowRect','evaded','batch','command122','BattleSystem','altKey','number','([\x5c+\x5c-]\x5cd+)([%％])>','textWidth','process_VisuMZ_CoreEngine_Notetags','areButtonsHidden','processKeyboardDelete','stencilFunc','createTitleButtons','outbounce','OUTBACK','gradientFillRect','Show\x20Scrolling\x20Text\x20Script\x20Error','gaugeHeight','META','ARRAYFUNC','abs','text','filter','buttonAssistKey3','applyEasing','buttonAssistKey%1','Bitmap_clearRect','DigitGroupingStandardText','default','endAnimation','_colorCache','button','buttonAssistWindowSideRect','EnableJS','Scene_Battle_createSpriteset','TimeProgress','command105','reserveNewGameCommonEvent','CIRCUMFLEX','_movementWholeDuration','_storedStack','itemHit','replace','updateOrigin','SParamVocab4','STRUCT','SkillMenu','darwin','Window_Base_drawFace','option','setMainFontSize','commandWindowRect','Sprite_Gauge_currentValue','CONVERT','CallHandlerJS','removeFauxAnimation','makeCommandList','GoldMax','Graphics_defaultStretchMode','consumeItem','apply','WIN_OEM_WSCTRL','isMaxLevel','OptionsMenu','getColor','InputBgType','repositionCancelButtonSideButtonLayout','Layer','CoreEngine','SellBgType','addWindow','WIN_OEM_PA2','eventsXyNt','_spriteset','Game_Interpreter_command355','Rate','XParameterFormula','_stored_hpGaugeColor2','colSpacing','43xrPHmz','HYPHEN_MINUS','DigitGroupingExText','movePageButtonSideButtonLayout','setActionState','VOLUME_UP','visible','processKeyboardHome','onClick','DataManager_setupNewGame','TCR','isSpecialCode','skipBranch','contains','nextLevelExp','isWindowMaskingEnabled','duration','_profileWindow','currentValue','WIN_OEM_CUSEL','Scene_Options_create','initBasic','CEV','makeInputButtonString','_customModified','Game_Picture_show','ImprovedAccuracySystem','F24','TextStr','enableDigitGroupingEx','Game_Picture_updateMove','_stored_crisisColor','startAutoNewGame','width','string','SystemSetFontSize','BACKSPACE','_coreEngineShakeStyle','TitlePicButtons','XParamVocab4','status','getBackgroundOpacity','_baseSprite','traitObjects','EXR','Window_StatusBase_drawActorSimpleStatus','_targetOffsetY','Control\x20Variables\x20Script\x20Error','OnLoadJS','processKeyboardBackspace','loadBitmap','mainAreaTopSideButtonLayout','FTB','Game_Party_consumeItem','getLevel','titles1','ShowItemBackground','IconSParam3','deathColor','RIGHT','playOk','levelUp','RowSpacing','buttonAreaHeight','blt','process_VisuMZ_CoreEngine_RegExp','processSoundTimings','contentsOpacity','createButtonAssistWindow','FontSize','_categoryWindow','INOUTEXPO','guardSkillId','isItemStyle','NUMPAD1','adjustBoxSize','XParamVocab6','img/%1/','INOUTCIRC','_slotWindow','Sprite_destroy','Scene_Battle_update','ActorHPColor','escape','SystemLoadAudio','updateLastTarget','Spriteset_Base_updatePosition','Spriteset_Base_update','subject','onKeyDownKeysF6F7','makeAutoBattleActions','createBackground','getColorDataFromPluginParameters','updateScene','GRD','ColorManager_loadWindowskin','ColorTPGauge2','_registerKeyInput','drawRightArrow','paramRateJS','CustomParamIcons','updateBackOpacity','home','RevertPreserveNumbers','setupCoreEngine','_anchor','KeyItemProtect','MDR','statusEquipWindowRect','getInputButtonString','repositionEnemiesByResolution','original','%1\x27s\x20version\x20does\x20not\x20match\x20plugin\x27s.\x20Please\x20update\x20it\x20in\x20the\x20Plugin\x20Manager.','SPACE','destroyed','isNumpadPressed','WIN_OEM_AUTO','Page','TextCodeNicknames','remove','CommandList','menuShowButton','buttonAssistOffset2','key%1','GREATER_THAN','DTB','xScrollLinkedOffset','isHandled','Renderer','%1\x20is\x20missing\x20a\x20required\x20plugin.\x0aPlease\x20install\x20%2\x20into\x20the\x20Plugin\x20Manager.','createFauxAnimationQueue','BuyRect','listWindowRect','itemBackColor1','Tilemap_addShadow','MCR','IconXParam3','ALT','opacity','textSizeEx','missed','OutlineColor','PreserveNumbers','showDevTools','_stored_maxLvGaugeColor2','match','blendFunc','keyboard','_stored_tpCostColor','isArrowPressed','updateOpacity','DOLLAR','updateDocumentTitle','Game_Interpreter_command105','IconXParam7','code','EVA','none','cancel','CRI','ParamMax','parameters','Game_Action_itemEva','TextManager_param','resetTextColor','_shakePower','buttonAssistOffset3','DigitGroupingGaugeSprites','xparamFlat2','bgsVolume','expGaugeColor1','#%1','Sprite_AnimationMV_processTimingData','processBack','F6key','sparamRate','inputWindowRect','RegExp','\x5c}❪SHIFT❫\x5c{','setSideButtonLayout','titleCommandWindow','_mapNameWindow','IconParam7','TAB','STENCIL_TEST','ALTGR','getCoreEngineScreenShakeStyle','Bitmap_resize','itemWindowRect','MAX_SAFE_INTEGER','buttonAssistOffset%1','Window_EquipItem_isEnabled','WIN_OEM_CLEAR','padding','gaugeBackColor','battlebacks2','processCursorHomeEndTrigger','WIN_OEM_PA3','Spriteset_Battle_createEnemies','OS_KEY','VisuMZ_1_OptionsCore','StatusBgType','PGDN','reservePlayTestNewGameCommonEvent','Spriteset_Base_destroy','exit','ScreenShake','AntiZoomPictures','wholeDuration','pendingColor','_optionsWindow','SlotBgType','processDigitChange','ColorPowerUp','cursorPageup','ParseEnemyNotetags','_drawTextShadow','helpAreaHeight','integer','setGuard','ParseArmorNotetags','retreat','Settings','currentLevelExp','push','Graphics_printError','backgroundBitmap','image-rendering','TPB\x20ACTIVE','KANA','test','drawText','inbounce','skills','setAttack','MenuBg','MEV','Plus','WindowLayer_render','members','BottomButtons','asin','DATABASE','ctrl','DimColor1','updatePositionCoreEngineShakeHorz','45347rnliRc','trim','_playTestFastMode','mpColor','_digitGrouping','sparamPlusJS','pageup','NewGameCommonEvent','Game_Picture_move','HASH','1ITnKzd','TextFmt','setHandler','skillTypes','ATK','animationId','_pollGamepads','INBACK','type','ModernControls','optSideView','Game_Picture_initBasic','Window_Selectable_itemRect','Window_NameInput_processHandling','drawGoldItemStyle','setupNewGame','Window_NumberInput_processDigitChange','Scene_Battle_createCancelButton','createMenuButton','NameInputMessage','EnableNameInput','shake','isOpen','IconParam4','PA1','children','levelUpRecovery','innerHeight','itemSuccessRate','WASD','drawGameVersion','%1%2','Window_Base_drawCharacter','StatusMenu','slice','_mp','_stored_tpGaugeColor2','keyCode','anchorCoreEasing','OPEN_BRACKET','height','ParseActorNotetags','ColorTPGauge1','gainGold','GoldFontSize','_shakeDuration','playBuzzer','drawAllParams','pictures','([\x5c+\x5c-]\x5cd+)>','child_process','paramFlatBonus','makeFontSmaller','Game_Character_processMoveCommand','sparamFlatBonus','initCoreEngine','FontShadows','paramX','initialBattleSystem','MainMenu','cursorUp','updateEffekseer','buttonAssistWindowButtonRect','tileHeight','203327NRefAR','isMagical','Window_Selectable_processTouch','usableSkills','DamageColor','_helpWindow','Bitmap_blt','paramMaxJS','mainAreaHeightSideButtonLayout','_offsetY','strokeRect','WIN_OEM_FINISH','initMembersCoreEngine','Input_setupEventHandlers','filters','catchUnknownError','SideButtons','HRG','Sprite_Picture_updateOrigin','isKeyItem','PictureEasingType','Icon','isNwjs','XParamVocab1','_pictureContainer','F23','(\x5cd+)>','isGameActive','setBackgroundOpacity','AutoStretch','F15','initialize','createChildSprite','split','OUTSINE','Window_NameInput_initialize','WIN_OEM_COPY','outlineColor','SParamVocab7','Symbol','targetObjects','Scene_MenuBase_helpAreaTop','helpAreaTop','drawCurrencyValue','MenuLayout','CommandRect','get','setupButtonImage','ItemBackColor2','_cacheScaleY','imageSmoothingEnabled','_lastPluginCommandInterpreter','Window_Base_update','DELETE','BACK_SLASH','StatusParamsBgType','InputRect','ParamName','touchUI','_pagedownButton','_playtestF7Looping','switchModes','end','_statusEquipWindow','Game_Map_setup','en-US','tpColor','paramFlatJS','Scene_MenuBase_mainAreaHeight','process_VisuMZ_CoreEngine_Functions','paramBaseAboveLevel99','scaleSprite','processTouchModernControls','_hideTileShadows','TPB\x20WAIT','ARRAYSTRUCT','CONTEXT_MENU','IconSParam8','index','Scene_Skill_create','Scene_Map_updateScene','log','inBattle','_stored_mpCostColor','innerWidth','ColorSystem','EndingID','targetOpacity','_clickHandler','NUMPAD9','xparamFlatJS','SParamVocab0','updateAnchor','ItemHeight','F10','ENTER_SPECIAL','ItemStyle','setActorHome','sv_enemies','CNT','updatePositionCoreEngineShakeVert','originalJS','ApplyEasing','DOUBLE_QUOTE','IconParam5','playTestF6','editWindowRect','setBackgroundType','_muteSound','_closing','Max','select','onEscapeSuccess','SParamVocab9','displayY','keyMapper','_mode','OpenSpeed','WIN_OEM_BACKTAB','F7key','EVAL','IconSParam4','scale','F22','Keyboard','drawParamText','PIPE','command111','WIN_ICO_00','_stored_tpGaugeColor1','paramName','initCoreEngineScreenShake','createFauxAnimation','INQUINT','SystemSetBattleSystem','registerCommand','PERCENT','ProfileBgType','eva','LUK','setup','IconXParam5','ActorMPColor','XParamVocab7','GameEnd','targetScaleX','NewGameBoot','isAlive','rowSpacing','mev','DocumentTitleFmt','ValueJS','randomInt','END','gold','ColorHPGauge2','drawItem','BasicParameterFormula','smallParamFontSize','ExtJS','ConvertParams','itemPadding','keyRepeatWait','HIT','mpGaugeColor2','IconSet','0.00','xparamFlatBonus','loadTitle1','29017EgzBEA','_timerSprite','paramBase','sparamFlat2','_skillTypeWindow','isMaskingEnabled','jsQuickFunc','_goldWindow','Plus2','4532BZdAuN','openURL','WIN_ICO_HELP','RequireFocus','level','xparamRateJS','title','XParamVocab9','paramWidth','makeActionList','render','_maxDigits','22BDKuoD','shift','LvExpGauge','mirror','runCombinedScrollingTextAsCode','Untitled','OpenURL','toString','WIN_ICO_CLEAR','Rate2','systemColor','_fauxAnimationQueue','erasePicture','ColSpacing','seVolume','EQUAL','Scene_Menu_create','rgba(0,\x200,\x200,\x201.0)','displayX','SParamVocab3','isEnabled','charAt','Scene_Equip_create','setWindowPadding','BgFilename1','backOpacity','INOUTQUINT','Window_Selectable_cursorUp','_forcedTroopView','mpCostColor','rgba(0,\x200,\x200,\x200.7)','createDimmerSprite','VisuMZ_2_BattleSystemBTB','Window_ShopSell_isEnabled','bgs','CommandBgType','ATTN','mpGaugeColor1','_realScale','createTextState','_destroyInternalTextures','max','INEXPO','result','VisuMZ_2_BattleSystemSTB','blockWidth','cursorRight','mainAreaBottom','onKeyDown','platform','updateTransform','BgType','IconParam2','loadPicture','checkSmartEventCollision','updateClose','_stored_pendingColor','Sprite_Button_updateOpacity','encounterStep','Flat1','CategoryBgType','drawIconBySize','WIN_OEM_JUMP','drawFace','text%1','TRG','evaluate','defineProperty','addEventListener','drawSegment','ButtonAssist','drawParamName','isRepeated','PDR','FadeSpeed','Window_NumberInput_start','isCollidedWithEvents','AccuracyBoost','bgmVolume','_stored_powerUpColor','isTpb','OUTBOUNCE','getInputMultiButtonStrings','BgFilename2','OUTCIRC','lineHeight','measureTextWidth','buttonAssistText1','Scene_Shop_create','_isWindow','isPhysical','Version','INQUAD','Window_NameInput_cursorPagedown','attackSkillId','INOUTELASTIC','TranslucentOpacity','Duration','updateKeyText','Scene_Boot_onDatabaseLoaded','INOUTBACK','Window_Base_initialize','ColorGaugeBack','PLAY','moveRelativeToResolutionChange','_screenX'];const _0x3168=function(_0x2140d0,_0x2d309c){_0x2140d0=_0x2140d0-0x1d4;let _0x331500=_0x3315[_0x2140d0];return _0x331500;};const _0x32f06e=_0x3168;(function(_0x364870,_0x3b0060){const _0xc32284=_0x3168;while(!![]){try{const _0xe6e568=parseInt(_0xc32284(0x328))+-parseInt(_0xc32284(0x6de))+-parseInt(_0xc32284(0x525))*-parseInt(_0xc32284(0x706))+-parseInt(_0xc32284(0x618))*-parseInt(_0xc32284(0x62d))+parseInt(_0xc32284(0x42f))*-parseInt(_0xc32284(0x1ff))+-parseInt(_0xc32284(0x621))*parseInt(_0xc32284(0x6fd))+parseInt(_0xc32284(0x52f))*parseInt(_0xc32284(0x56f));if(_0xe6e568===_0x3b0060)break;else _0x364870['push'](_0x364870['shift']());}catch(_0x76a777){_0x364870['push'](_0x364870['shift']());}}}(_0x3315,0x70316));var label=_0x32f06e(0x424),tier=tier||0x0,dependencies=[],pluginData=$plugins[_0x32f06e(0x3f6)](function(_0x3c185e){const _0x56a6ef=_0x32f06e;return _0x3c185e[_0x56a6ef(0x457)]&&_0x3c185e[_0x56a6ef(0x2af)][_0x56a6ef(0x77d)]('['+label+']');})[0x0];VisuMZ[label][_0x32f06e(0x50d)]=VisuMZ[label]['Settings']||{},VisuMZ[_0x32f06e(0x60f)]=function(_0xedab80,_0x2c2dc6){const _0x55bf99=_0x32f06e;for(const _0x549209 in _0x2c2dc6){if(_0x549209[_0x55bf99(0x4c0)](/(.*):(.*)/i)){const _0x220185=String(RegExp['$1']),_0x50c070=String(RegExp['$2'])['toUpperCase']()['trim']();let _0x48f947,_0x3b1f5a,_0x243740;switch(_0x50c070){case _0x55bf99(0x340):_0x48f947=_0x2c2dc6[_0x549209]!==''?Number(_0x2c2dc6[_0x549209]):0x0;break;case'ARRAYNUM':_0x3b1f5a=_0x2c2dc6[_0x549209]!==''?JSON[_0x55bf99(0x3d8)](_0x2c2dc6[_0x549209]):[],_0x48f947=_0x3b1f5a[_0x55bf99(0x281)](_0x21eef6=>Number(_0x21eef6));break;case _0x55bf99(0x5e7):_0x48f947=_0x2c2dc6[_0x549209]!==''?eval(_0x2c2dc6[_0x549209]):null;break;case _0x55bf99(0x74a):_0x3b1f5a=_0x2c2dc6[_0x549209]!==''?JSON[_0x55bf99(0x3d8)](_0x2c2dc6[_0x549209]):[],_0x48f947=_0x3b1f5a[_0x55bf99(0x281)](_0x5423d1=>eval(_0x5423d1));break;case _0x55bf99(0x2fd):_0x48f947=_0x2c2dc6[_0x549209]!==''?JSON[_0x55bf99(0x3d8)](_0x2c2dc6[_0x549209]):'';break;case _0x55bf99(0x311):_0x3b1f5a=_0x2c2dc6[_0x549209]!==''?JSON['parse'](_0x2c2dc6[_0x549209]):[],_0x48f947=_0x3b1f5a['map'](_0x437877=>JSON[_0x55bf99(0x3d8)](_0x437877));break;case _0x55bf99(0x350):_0x48f947=_0x2c2dc6[_0x549209]!==''?new Function(JSON[_0x55bf99(0x3d8)](_0x2c2dc6[_0x549209])):new Function('return\x200');break;case _0x55bf99(0x3f3):_0x3b1f5a=_0x2c2dc6[_0x549209]!==''?JSON[_0x55bf99(0x3d8)](_0x2c2dc6[_0x549209]):[],_0x48f947=_0x3b1f5a['map'](_0x9e2036=>new Function(JSON[_0x55bf99(0x3d8)](_0x9e2036)));break;case _0x55bf99(0x768):_0x48f947=_0x2c2dc6[_0x549209]!==''?String(_0x2c2dc6[_0x549209]):'';break;case _0x55bf99(0x78c):_0x3b1f5a=_0x2c2dc6[_0x549209]!==''?JSON[_0x55bf99(0x3d8)](_0x2c2dc6[_0x549209]):[],_0x48f947=_0x3b1f5a[_0x55bf99(0x281)](_0x4580c5=>String(_0x4580c5));break;case _0x55bf99(0x40d):_0x243740=_0x2c2dc6[_0x549209]!==''?JSON[_0x55bf99(0x3d8)](_0x2c2dc6[_0x549209]):{},_0xedab80[_0x220185]={},VisuMZ[_0x55bf99(0x60f)](_0xedab80[_0x220185],_0x243740);continue;case _0x55bf99(0x5ba):_0x3b1f5a=_0x2c2dc6[_0x549209]!==''?JSON[_0x55bf99(0x3d8)](_0x2c2dc6[_0x549209]):[],_0x48f947=_0x3b1f5a[_0x55bf99(0x281)](_0x12b60c=>VisuMZ[_0x55bf99(0x60f)]({},JSON['parse'](_0x12b60c)));break;default:continue;}_0xedab80[_0x220185]=_0x48f947;}}return _0xedab80;},(_0x4ff896=>{const _0x5ea8c6=_0x32f06e,_0x4c39a2=_0x4ff896[_0x5ea8c6(0x6b9)];for(const _0x4f0108 of dependencies){if(!Imported[_0x4f0108]){alert(_0x5ea8c6(0x4b0)[_0x5ea8c6(0x262)](_0x4c39a2,_0x4f0108)),SceneManager[_0x5ea8c6(0x4fc)]();break;}}const _0x11c10d=_0x4ff896['description'];if(_0x11c10d['match'](/\[Version[ ](.*?)\]/i)){const _0x671b89=Number(RegExp['$1']);_0x671b89!==VisuMZ[label][_0x5ea8c6(0x6d1)]&&(alert(_0x5ea8c6(0x49f)[_0x5ea8c6(0x262)](_0x4c39a2,_0x671b89)),SceneManager[_0x5ea8c6(0x4fc)]());}if(_0x11c10d[_0x5ea8c6(0x4c0)](/\[Tier[ ](\d+)\]/i)){const _0x3e8f09=Number(RegExp['$1']);_0x3e8f09<tier?(alert('%1\x20is\x20incorrectly\x20placed\x20on\x20the\x20plugin\x20list.\x0aIt\x20is\x20a\x20Tier\x20%2\x20plugin\x20placed\x20over\x20other\x20Tier\x20%3\x20plugins.\x0aPlease\x20reorder\x20the\x20plugin\x20list\x20from\x20smallest\x20to\x20largest\x20tier\x20numbers.'['format'](_0x4c39a2,_0x3e8f09,tier)),SceneManager[_0x5ea8c6(0x4fc)]()):tier=Math[_0x5ea8c6(0x656)](_0x3e8f09,tier);}VisuMZ[_0x5ea8c6(0x60f)](VisuMZ[label][_0x5ea8c6(0x50d)],_0x4ff896[_0x5ea8c6(0x4d0)]);})(pluginData),VisuMZ['CoreEngine']['Activated']={'PluginCommands':!![]},PluginManager['registerCommand'](pluginData[_0x32f06e(0x6b9)],_0x32f06e(0x633),_0x5e4401=>{VisuMZ['ConvertParams'](_0x5e4401,_0x5e4401);const _0x1f2c32=_0x5e4401['URL'];VisuMZ['openURL'](_0x1f2c32);}),PluginManager[_0x32f06e(0x5f6)](pluginData['name'],_0x32f06e(0x2d0),_0x2b0fb0=>{const _0x2d4f22=_0x32f06e;VisuMZ[_0x2d4f22(0x60f)](_0x2b0fb0,_0x2b0fb0);const _0x2436fc=_0x2b0fb0[_0x2d4f22(0x6f7)]||0x0;$gameParty[_0x2d4f22(0x55a)](_0x2436fc);}),PluginManager[_0x32f06e(0x5f6)](pluginData[_0x32f06e(0x6b9)],_0x32f06e(0x583),_0x127282=>{const _0x1e571a=_0x32f06e;VisuMZ[_0x1e571a(0x60f)](_0x127282,_0x127282);const _0x15d5dc=_0x127282[_0x1e571a(0x210)]||0x1,_0x5406a1=_0x127282['easingType']||_0x1e571a(0x3cd),_0x1e6411=$gameScreen[_0x1e571a(0x6f3)](_0x15d5dc);_0x1e6411&&_0x1e6411[_0x1e571a(0x3d3)](_0x5406a1);}),PluginManager[_0x32f06e(0x5f6)](pluginData[_0x32f06e(0x6b9)],_0x32f06e(0x699),_0x543b0d=>{const _0x5e9184=_0x32f06e;for(let _0x1ffa3e=0x1;_0x1ffa3e<=0x64;_0x1ffa3e++){$gameScreen[_0x5e9184(0x639)](_0x1ffa3e);}}),PluginManager[_0x32f06e(0x5f6)](pluginData[_0x32f06e(0x6b9)],_0x32f06e(0x314),_0x530294=>{const _0x22f438=_0x32f06e;VisuMZ['ConvertParams'](_0x530294,_0x530294);const _0x148eab=Math[_0x22f438(0x6ba)](_0x530294[_0x22f438(0x6aa)],_0x530294[_0x22f438(0x5c5)]),_0x5b3860=Math['max'](_0x530294[_0x22f438(0x6aa)],_0x530294[_0x22f438(0x5c5)]);for(let _0x585119=_0x148eab;_0x585119<=_0x5b3860;_0x585119++){$gameScreen[_0x22f438(0x639)](_0x585119);}}),PluginManager[_0x32f06e(0x5f6)](pluginData[_0x32f06e(0x6b9)],_0x32f06e(0x4fd),_0x4fc421=>{const _0x45a7e1=_0x32f06e;VisuMZ['ConvertParams'](_0x4fc421,_0x4fc421);const _0x55ffd2=_0x4fc421[_0x45a7e1(0x32e)]||'random',_0x8dcbda=_0x4fc421['Power'][_0x45a7e1(0x32b)](0x1,0x9),_0x49d988=_0x4fc421[_0x45a7e1(0x249)][_0x45a7e1(0x32b)](0x1,0x9),_0x582b4e=_0x4fc421[_0x45a7e1(0x68e)]||0x1,_0x4b8941=_0x4fc421[_0x45a7e1(0x3af)];$gameScreen[_0x45a7e1(0x201)](_0x55ffd2),$gameScreen[_0x45a7e1(0x6a7)](_0x8dcbda,_0x49d988,_0x582b4e);if(_0x4b8941){const _0x5a5e13=$gameTemp['getLastPluginCommandInterpreter']();if(_0x5a5e13)_0x5a5e13['wait'](_0x582b4e);}}),PluginManager[_0x32f06e(0x5f6)](pluginData['name'],_0x32f06e(0x452),_0x4446d6=>{const _0x32e629=_0x32f06e;VisuMZ['ConvertParams'](_0x4446d6,_0x4446d6);const _0x2fb90d=_0x4446d6[_0x32e629(0x411)]||0x1;$gameSystem['setMainFontSize'](_0x2fb90d);}),PluginManager[_0x32f06e(0x5f6)](pluginData[_0x32f06e(0x6b9)],_0x32f06e(0x36f),_0x5f1eab=>{const _0x55f7cf=_0x32f06e;if($gameParty[_0x55f7cf(0x5c1)]())return;VisuMZ[_0x55f7cf(0x60f)](_0x5f1eab,_0x5f1eab);const _0x3c7ee9=_0x5f1eab[_0x55f7cf(0x411)];if(_0x3c7ee9['match'](/Front/i))$gameSystem['setSideView'](![]);else _0x3c7ee9[_0x55f7cf(0x4c0)](/Side/i)?$gameSystem['setSideView'](!![]):$gameSystem[_0x55f7cf(0x3b9)](!$gameSystem['isSideView']());}),PluginManager[_0x32f06e(0x5f6)](pluginData[_0x32f06e(0x6b9)],_0x32f06e(0x483),_0x2682a9=>{const _0x295a93=_0x32f06e;if($gameParty[_0x295a93(0x5c1)]())return;VisuMZ[_0x295a93(0x60f)](_0x2682a9,_0x2682a9);const _0x1c94fe=[_0x295a93(0x6d3),_0x295a93(0x64f),'me','se'];for(const _0x7b5830 of _0x1c94fe){const _0x39eabd=_0x2682a9[_0x7b5830],_0x123399='%1/'[_0x295a93(0x262)](_0x7b5830);for(const _0x469667 of _0x39eabd){console[_0x295a93(0x5c0)](_0x123399,_0x469667),AudioManager[_0x295a93(0x69f)](_0x123399,_0x469667);}}}),PluginManager[_0x32f06e(0x5f6)](pluginData[_0x32f06e(0x6b9)],'SystemLoadImages',_0x477fbf=>{const _0x5baf19=_0x32f06e;if($gameParty[_0x5baf19(0x5c1)]())return;VisuMZ[_0x5baf19(0x60f)](_0x477fbf,_0x477fbf);const _0x5d120a=[_0x5baf19(0x1d5),'battlebacks1',_0x5baf19(0x4f2),_0x5baf19(0x362),_0x5baf19(0x786),_0x5baf19(0x2d3),'parallaxes','pictures','sv_actors',_0x5baf19(0x5d1),_0x5baf19(0x31d),_0x5baf19(0x6bf),_0x5baf19(0x466),'titles2'];for(const _0x38de88 of _0x5d120a){const _0x376984=_0x477fbf[_0x38de88],_0x13c730=_0x5baf19(0x47c)[_0x5baf19(0x262)](_0x38de88);for(const _0x4dce7a of _0x376984){ImageManager[_0x5baf19(0x461)](_0x13c730,_0x4dce7a);}}}),PluginManager[_0x32f06e(0x5f6)](pluginData[_0x32f06e(0x6b9)],_0x32f06e(0x5f5),_0x59da48=>{const _0x305ada=_0x32f06e;if($gameParty['inBattle']())return;VisuMZ['ConvertParams'](_0x59da48,_0x59da48);const _0x231a29=_0x59da48['option'][_0x305ada(0x79b)]()[_0x305ada(0x526)](),_0x342e22=VisuMZ[_0x305ada(0x424)][_0x305ada(0x6ad)](_0x231a29);$gameSystem['setBattleSystem'](_0x342e22);}),VisuMZ['CoreEngine'][_0x32f06e(0x6ad)]=function(_0x411481){const _0x216402=_0x32f06e;_0x411481=_0x411481||_0x216402(0x521),_0x411481=String(_0x411481)[_0x216402(0x79b)]()[_0x216402(0x526)]();switch(_0x411481){case _0x216402(0x4ac):return 0x0;case _0x216402(0x513):Imported['VisuMZ_1_OptionsCore']&&(ConfigManager[_0x216402(0x1dc)]=!![]);return 0x1;case _0x216402(0x5b9):Imported[_0x216402(0x4f7)]&&(ConfigManager[_0x216402(0x1dc)]=![]);return 0x2;case _0x216402(0x2cc):if(Imported[_0x216402(0x240)])return'CTB';break;case _0x216402(0x737):if(Imported[_0x216402(0x659)])return _0x216402(0x737);break;case'BTB':if(Imported[_0x216402(0x64d)])return _0x216402(0x365);break;case _0x216402(0x463):if(Imported[_0x216402(0x264)])return _0x216402(0x463);break;}return $dataSystem[_0x216402(0x2bc)];},PluginManager[_0x32f06e(0x5f6)](pluginData[_0x32f06e(0x6b9)],'SystemSetWindowPadding',_0x1ad51a=>{const _0x2b5868=_0x32f06e;VisuMZ['ConvertParams'](_0x1ad51a,_0x1ad51a);const _0x3a08ca=_0x1ad51a[_0x2b5868(0x411)]||0x1;$gameSystem[_0x2b5868(0x644)](_0x3a08ca);}),VisuMZ['CoreEngine'][_0x32f06e(0x690)]=Scene_Boot[_0x32f06e(0x76f)]['onDatabaseLoaded'],Scene_Boot['prototype'][_0x32f06e(0x234)]=function(){const _0xe482de=_0x32f06e;VisuMZ[_0xe482de(0x424)][_0xe482de(0x690)][_0xe482de(0x31a)](this),this[_0xe482de(0x470)](),this[_0xe482de(0x3e8)](),this[_0xe482de(0x76b)](),this[_0xe482de(0x5b4)](),this[_0xe482de(0x6e5)](),VisuMZ[_0xe482de(0x35a)]();},VisuMZ['CoreEngine'][_0x32f06e(0x4e0)]={},Scene_Boot[_0x32f06e(0x76f)][_0x32f06e(0x470)]=function(){const _0x1b7282=_0x32f06e,_0x317755=[_0x1b7282(0x309),'MAXMP',_0x1b7282(0x533),_0x1b7282(0x213),'MAT',_0x1b7282(0x317),_0x1b7282(0x321),_0x1b7282(0x5fa)],_0x2c49d9=[_0x1b7282(0x612),'EVA','CRI',_0x1b7282(0x445),'MEV','MRF',_0x1b7282(0x5d2),_0x1b7282(0x580),_0x1b7282(0x76c),_0x1b7282(0x66e)],_0x4a6244=[_0x1b7282(0x1ee),'GRD',_0x1b7282(0x3b8),_0x1b7282(0x74f),_0x1b7282(0x4b6),_0x1b7282(0x439),_0x1b7282(0x676),_0x1b7282(0x49a),_0x1b7282(0x342),_0x1b7282(0x45b)],_0x4ad0de=[_0x317755,_0x2c49d9,_0x4a6244],_0xb5d6f5=[_0x1b7282(0x51c),_0x1b7282(0x32c),_0x1b7282(0x620),_0x1b7282(0x5dd),_0x1b7282(0x42b),_0x1b7282(0x20e),_0x1b7282(0x636),'Flat',_0x1b7282(0x668),_0x1b7282(0x332)];for(const _0xdb8467 of _0x4ad0de){let _0x5c124d='';if(_0xdb8467===_0x317755)_0x5c124d=_0x1b7282(0x6bc);if(_0xdb8467===_0x2c49d9)_0x5c124d=_0x1b7282(0x775);if(_0xdb8467===_0x4a6244)_0x5c124d='sparam';for(const _0x5d61a8 of _0xb5d6f5){let _0x3aa519=_0x1b7282(0x54e)[_0x1b7282(0x262)](_0x5c124d,_0x5d61a8);VisuMZ['CoreEngine'][_0x1b7282(0x4e0)][_0x3aa519]=[],VisuMZ[_0x1b7282(0x424)][_0x1b7282(0x4e0)][_0x3aa519+'JS']=[];let _0x4a560f='<%1\x20%2:[\x20]';if([_0x1b7282(0x51c),_0x1b7282(0x2b2)][_0x1b7282(0x77d)](_0x5d61a8))_0x4a560f+=_0x1b7282(0x560);else{if(['Plus1',_0x1b7282(0x668)][_0x1b7282(0x77d)](_0x5d61a8))_0x4a560f+=_0x1b7282(0x3e6);else{if([_0x1b7282(0x620),_0x1b7282(0x332)][_0x1b7282(0x77d)](_0x5d61a8))_0x4a560f+=_0x1b7282(0x75f);else{if(_0x5d61a8==='Max')_0x4a560f+=_0x1b7282(0x589);else{if(_0x5d61a8===_0x1b7282(0x20e))_0x4a560f+='(\x5cd+)([%％])>';else _0x5d61a8===_0x1b7282(0x636)&&(_0x4a560f+='(\x5cd+\x5c.?\x5cd+)>');}}}}for(const _0x315750 of _0xdb8467){let _0x370b9d=_0x5d61a8[_0x1b7282(0x40a)](/[\d+]/g,'')[_0x1b7282(0x79b)]();const _0x24f1ef=_0x4a560f[_0x1b7282(0x262)](_0x315750,_0x370b9d);VisuMZ[_0x1b7282(0x424)][_0x1b7282(0x4e0)][_0x3aa519]['push'](new RegExp(_0x24f1ef,'i'));const _0xddb504=_0x1b7282(0x1fd)[_0x1b7282(0x262)](_0x315750,_0x370b9d);VisuMZ[_0x1b7282(0x424)]['RegExp'][_0x3aa519+'JS'][_0x1b7282(0x50f)](new RegExp(_0xddb504,'i'));}}}},Scene_Boot[_0x32f06e(0x76f)][_0x32f06e(0x3e8)]=function(){const _0x2bf1b3=_0x32f06e;if(VisuMZ[_0x2bf1b3(0x35a)])return;},Scene_Boot['prototype'][_0x32f06e(0x76b)]=function(){const _0x44cb04=_0x32f06e;VisuMZ[_0x44cb04(0x424)]['Settings'][_0x44cb04(0x248)]['OpenConsole']&&VisuMZ[_0x44cb04(0x20f)](!![]);VisuMZ['CoreEngine'][_0x44cb04(0x50d)]['QoL'][_0x44cb04(0x538)]&&(Input[_0x44cb04(0x5e2)][0x23]=_0x44cb04(0x5ad),Input[_0x44cb04(0x5e2)][0x24]=_0x44cb04(0x495));if(VisuMZ[_0x44cb04(0x424)]['Settings'][_0x44cb04(0x673)]){const _0x3bf814=VisuMZ[_0x44cb04(0x424)][_0x44cb04(0x50d)]['ButtonAssist'];_0x3bf814[_0x44cb04(0x2e5)]=_0x3bf814['KeySHIFT']||_0x44cb04(0x4e1),_0x3bf814[_0x44cb04(0x760)]=_0x3bf814[_0x44cb04(0x760)]||'\x5c}❪TAB❫\x5c{';}VisuMZ['CoreEngine'][_0x44cb04(0x50d)][_0x44cb04(0x77f)][_0x44cb04(0x54c)]&&(Input[_0x44cb04(0x5e2)][0x57]='up',Input['keyMapper'][0x41]=_0x44cb04(0x3d7),Input[_0x44cb04(0x5e2)][0x53]=_0x44cb04(0x272),Input[_0x44cb04(0x5e2)][0x44]=_0x44cb04(0x295),Input[_0x44cb04(0x5e2)][0x45]=_0x44cb04(0x271)),VisuMZ['CoreEngine'][_0x44cb04(0x50d)][_0x44cb04(0x77f)]['DashToggleR']&&(Input['keyMapper'][0x52]='dashToggle');},Scene_Boot[_0x32f06e(0x76f)][_0x32f06e(0x5b4)]=function(){const _0x170969=_0x32f06e;this[_0x170969(0x26d)]();},Scene_Boot[_0x32f06e(0x76f)][_0x32f06e(0x26d)]=function(){const _0x2ed2fa=_0x32f06e,_0x5463aa=VisuMZ[_0x2ed2fa(0x424)][_0x2ed2fa(0x50d)][_0x2ed2fa(0x61e)];for(const _0x301584 of _0x5463aa){const _0x2f7a47=_0x301584['FunctionName']['replace'](/[ ]/g,''),_0xfbdf27=_0x301584[_0x2ed2fa(0x265)];VisuMZ[_0x2ed2fa(0x424)][_0x2ed2fa(0x204)](_0x2f7a47,_0xfbdf27);}},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x204)]=function(_0x582a3e,_0x510087){const _0x3161fa=_0x32f06e;if(!!window[_0x582a3e]){if($gameTemp['isPlaytest']())console['log'](_0x3161fa(0x6d2)[_0x3161fa(0x262)](_0x582a3e));}const _0x223f60=_0x3161fa(0x1e9)[_0x3161fa(0x262)](_0x582a3e,_0x510087);window[_0x582a3e]=new Function(_0x223f60);},Scene_Boot[_0x32f06e(0x76f)]['process_VisuMZ_CoreEngine_CustomParameters']=function(){const _0x318154=_0x32f06e,_0x571f12=VisuMZ['CoreEngine'][_0x318154(0x50d)]['CustomParam'];if(!_0x571f12)return;for(const _0x1ecf67 of _0x571f12){if(!_0x1ecf67)continue;VisuMZ['CoreEngine'][_0x318154(0x3a3)](_0x1ecf67);}},VisuMZ[_0x32f06e(0x424)]['CustomParamNames']={},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x493)]={},VisuMZ[_0x32f06e(0x424)]['CustomParamType']={},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x315)]={},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x3a3)]=function(_0xdd7ead){const _0x1a875f=_0x32f06e,_0x4cbda6=_0xdd7ead['Abbreviation'],_0x1167cf=_0xdd7ead[_0x1a875f(0x5a8)],_0x4946be=_0xdd7ead[_0x1a875f(0x584)],_0x37aba0=_0xdd7ead['Type'],_0x38f3cf=new Function(_0xdd7ead[_0x1a875f(0x606)]);VisuMZ[_0x1a875f(0x424)][_0x1a875f(0x29a)][_0x4cbda6[_0x1a875f(0x79b)]()[_0x1a875f(0x526)]()]=_0x1167cf,VisuMZ[_0x1a875f(0x424)][_0x1a875f(0x493)][_0x4cbda6[_0x1a875f(0x79b)]()['trim']()]=_0x4946be,VisuMZ['CoreEngine'][_0x1a875f(0x2e0)][_0x4cbda6[_0x1a875f(0x79b)]()[_0x1a875f(0x526)]()]=_0x37aba0,VisuMZ[_0x1a875f(0x424)][_0x1a875f(0x315)][_0x4cbda6[_0x1a875f(0x79b)]()[_0x1a875f(0x526)]()]=_0x4cbda6,Object[_0x1a875f(0x670)](Game_BattlerBase['prototype'],_0x4cbda6,{'get'(){const _0x189fd6=_0x1a875f,_0x3561ff=_0x38f3cf[_0x189fd6(0x31a)](this);return _0x37aba0===_0x189fd6(0x509)?Math[_0x189fd6(0x237)](_0x3561ff):_0x3561ff;}});},VisuMZ[_0x32f06e(0x35a)]=function(){const _0x7f7961=_0x32f06e;for(const _0x418bac of $dataActors){if(_0x418bac)VisuMZ[_0x7f7961(0x558)](_0x418bac);}for(const _0x40b505 of $dataClasses){if(_0x40b505)VisuMZ[_0x7f7961(0x3c4)](_0x40b505);}for(const _0x37166a of $dataSkills){if(_0x37166a)VisuMZ[_0x7f7961(0x2e4)](_0x37166a);}for(const _0x542bdc of $dataItems){if(_0x542bdc)VisuMZ[_0x7f7961(0x1d6)](_0x542bdc);}for(const _0xb022b1 of $dataWeapons){if(_0xb022b1)VisuMZ[_0x7f7961(0x6da)](_0xb022b1);}for(const _0x398194 of $dataArmors){if(_0x398194)VisuMZ[_0x7f7961(0x50b)](_0x398194);}for(const _0x290bbc of $dataEnemies){if(_0x290bbc)VisuMZ[_0x7f7961(0x506)](_0x290bbc);}for(const _0x331280 of $dataStates){if(_0x331280)VisuMZ[_0x7f7961(0x389)](_0x331280);}for(const _0x5bbfac of $dataTilesets){if(_0x5bbfac)VisuMZ[_0x7f7961(0x1e2)](_0x5bbfac);}},VisuMZ[_0x32f06e(0x558)]=function(_0x1c780a){},VisuMZ[_0x32f06e(0x3c4)]=function(_0x1183dc){},VisuMZ[_0x32f06e(0x2e4)]=function(_0x8195a9){},VisuMZ[_0x32f06e(0x1d6)]=function(_0x26016e){},VisuMZ[_0x32f06e(0x6da)]=function(_0x206bf0){},VisuMZ[_0x32f06e(0x50b)]=function(_0x2dee27){},VisuMZ[_0x32f06e(0x506)]=function(_0x5c17b0){},VisuMZ[_0x32f06e(0x389)]=function(_0xde3c3f){},VisuMZ['ParseTilesetNotetags']=function(_0x4b93ac){},VisuMZ['CoreEngine']['ParseActorNotetags']=VisuMZ[_0x32f06e(0x558)],VisuMZ[_0x32f06e(0x558)]=function(_0x82f28e){const _0x585374=_0x32f06e;VisuMZ['CoreEngine']['ParseActorNotetags'][_0x585374(0x31a)](this,_0x82f28e);const _0x31e254=_0x82f28e[_0x585374(0x762)];if(_0x31e254[_0x585374(0x4c0)](/<MAX LEVEL:[ ](\d+)>/i)){_0x82f28e[_0x585374(0x727)]=Number(RegExp['$1']);if(_0x82f28e[_0x585374(0x727)]===0x0)_0x82f28e[_0x585374(0x727)]=Number[_0x585374(0x4ec)];}_0x31e254['match'](/<INITIAL LEVEL:[ ](\d+)>/i)&&(_0x82f28e[_0x585374(0x31e)]=Math[_0x585374(0x6ba)](Number(RegExp['$1']),_0x82f28e['maxLevel']));},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x3c4)]=VisuMZ[_0x32f06e(0x3c4)],VisuMZ[_0x32f06e(0x3c4)]=function(_0x282fcf){const _0x44f63a=_0x32f06e;VisuMZ[_0x44f63a(0x424)][_0x44f63a(0x3c4)][_0x44f63a(0x31a)](this,_0x282fcf);if(_0x282fcf[_0x44f63a(0x6f8)])for(const _0x74ed1d of _0x282fcf[_0x44f63a(0x6f8)]){_0x74ed1d[_0x44f63a(0x762)][_0x44f63a(0x4c0)](/<LEARN AT LEVEL:[ ](\d+)>/i)&&(_0x74ed1d[_0x44f63a(0x625)]=Math[_0x44f63a(0x656)](Number(RegExp['$1']),0x1));}},VisuMZ['CoreEngine'][_0x32f06e(0x506)]=VisuMZ['ParseEnemyNotetags'],VisuMZ[_0x32f06e(0x506)]=function(_0x4db789){const _0x2cdf39=_0x32f06e;VisuMZ[_0x2cdf39(0x424)]['ParseEnemyNotetags'][_0x2cdf39(0x31a)](this,_0x4db789),_0x4db789['level']=0x1;const _0x11d5de=_0x4db789['note'];if(_0x11d5de[_0x2cdf39(0x4c0)](/<LEVEL:[ ](\d+)>/i))_0x4db789[_0x2cdf39(0x625)]=Number(RegExp['$1']);if(_0x11d5de[_0x2cdf39(0x4c0)](/<MAXHP:[ ](\d+)>/i))_0x4db789[_0x2cdf39(0x286)][0x0]=Number(RegExp['$1']);if(_0x11d5de[_0x2cdf39(0x4c0)](/<MAXMP:[ ](\d+)>/i))_0x4db789[_0x2cdf39(0x286)][0x1]=Number(RegExp['$1']);if(_0x11d5de[_0x2cdf39(0x4c0)](/<ATK:[ ](\d+)>/i))_0x4db789[_0x2cdf39(0x286)][0x2]=Number(RegExp['$1']);if(_0x11d5de['match'](/<DEF:[ ](\d+)>/i))_0x4db789[_0x2cdf39(0x286)][0x3]=Number(RegExp['$1']);if(_0x11d5de[_0x2cdf39(0x4c0)](/<MAT:[ ](\d+)>/i))_0x4db789[_0x2cdf39(0x286)][0x4]=Number(RegExp['$1']);if(_0x11d5de['match'](/<MDF:[ ](\d+)>/i))_0x4db789[_0x2cdf39(0x286)][0x5]=Number(RegExp['$1']);if(_0x11d5de[_0x2cdf39(0x4c0)](/<AGI:[ ](\d+)>/i))_0x4db789[_0x2cdf39(0x286)][0x6]=Number(RegExp['$1']);if(_0x11d5de[_0x2cdf39(0x4c0)](/<LUK:[ ](\d+)>/i))_0x4db789[_0x2cdf39(0x286)][0x7]=Number(RegExp['$1']);if(_0x11d5de[_0x2cdf39(0x4c0)](/<EXP:[ ](\d+)>/i))_0x4db789[_0x2cdf39(0x6f5)]=Number(RegExp['$1']);if(_0x11d5de[_0x2cdf39(0x4c0)](/<GOLD:[ ](\d+)>/i))_0x4db789[_0x2cdf39(0x609)]=Number(RegExp['$1']);},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x41a)]=Graphics[_0x32f06e(0x2b8)],Graphics[_0x32f06e(0x2b8)]=function(){const _0x3f51dd=_0x32f06e;switch(VisuMZ[_0x3f51dd(0x424)][_0x3f51dd(0x50d)]['QoL'][_0x3f51dd(0x58c)]){case'stretch':return!![];case'normal':return![];default:return VisuMZ[_0x3f51dd(0x424)][_0x3f51dd(0x41a)]['call'](this);}},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x510)]=Graphics[_0x32f06e(0x77a)],Graphics['printError']=function(_0x256651,_0xe987c5,_0x2d943c=null){const _0x1295ef=_0x32f06e;VisuMZ['CoreEngine'][_0x1295ef(0x510)][_0x1295ef(0x31a)](this,_0x256651,_0xe987c5,_0x2d943c),VisuMZ[_0x1295ef(0x20f)](![]);},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x229)]=Graphics[_0x32f06e(0x349)],Graphics['_centerElement']=function(_0x20e250){const _0x18946d=_0x32f06e;VisuMZ['CoreEngine'][_0x18946d(0x229)]['call'](this,_0x20e250),this[_0x18946d(0x1f1)](_0x20e250);},Graphics[_0x32f06e(0x1f1)]=function(_0x401897){const _0x176ca8=_0x32f06e;VisuMZ[_0x176ca8(0x424)][_0x176ca8(0x50d)]['QoL'][_0x176ca8(0x3c8)]&&(_0x401897['style']['font-smooth']=_0x176ca8(0x4cc));VisuMZ[_0x176ca8(0x424)][_0x176ca8(0x50d)][_0x176ca8(0x248)][_0x176ca8(0x764)]&&(_0x401897[_0x176ca8(0x2fa)][_0x176ca8(0x512)]=_0x176ca8(0x385));const _0x45f300=Math[_0x176ca8(0x656)](0x0,Math['floor'](_0x401897[_0x176ca8(0x450)]*this[_0x176ca8(0x653)])),_0x372c0b=Math[_0x176ca8(0x656)](0x0,Math['floor'](_0x401897[_0x176ca8(0x557)]*this['_realScale']));_0x401897[_0x176ca8(0x2fa)]['width']=_0x45f300+'px',_0x401897['style'][_0x176ca8(0x557)]=_0x372c0b+'px';},Bitmap[_0x32f06e(0x76f)]['markCoreEngineModified']=function(){const _0x2ae0d7=_0x32f06e;this[_0x2ae0d7(0x447)]=!![];},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x47f)]=Sprite[_0x32f06e(0x76f)][_0x32f06e(0x738)],Sprite[_0x32f06e(0x76f)]['destroy']=function(){const _0x1c8be5=_0x32f06e;VisuMZ['CoreEngine'][_0x1c8be5(0x47f)]['call'](this),this[_0x1c8be5(0x36d)]();},Sprite[_0x32f06e(0x76f)][_0x32f06e(0x36d)]=function(){const _0xbc0f4d=_0x32f06e;if(!this['bitmap'])return;if(!this[_0xbc0f4d(0x1f7)][_0xbc0f4d(0x447)])return;this['bitmap'][_0xbc0f4d(0x373)]&&!this['_bitmap'][_0xbc0f4d(0x373)][_0xbc0f4d(0x4a1)]&&this[_0xbc0f4d(0x1f7)][_0xbc0f4d(0x738)]();},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x4ea)]=Bitmap[_0x32f06e(0x76f)][_0x32f06e(0x3b4)],Bitmap[_0x32f06e(0x76f)][_0x32f06e(0x3b4)]=function(_0x3e3bfa,_0x2164c5){const _0x40f791=_0x32f06e;VisuMZ[_0x40f791(0x424)][_0x40f791(0x4ea)]['call'](this,_0x3e3bfa,_0x2164c5),this[_0x40f791(0x6ac)]();},VisuMZ['CoreEngine']['Bitmap_blt']=Bitmap['prototype'][_0x32f06e(0x46f)],Bitmap['prototype']['blt']=function(_0x35dba3,_0x56fd39,_0x1a09ae,_0x59489a,_0x332ef4,_0x50c6fa,_0x23bf40,_0x5713ec,_0x437ac9){const _0x36afd9=_0x32f06e;VisuMZ['CoreEngine'][_0x36afd9(0x575)]['call'](this,_0x35dba3,_0x56fd39,_0x1a09ae,_0x59489a,_0x332ef4,_0x50c6fa,_0x23bf40,_0x5713ec,_0x437ac9),this[_0x36afd9(0x6ac)]();},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x3fa)]=Bitmap[_0x32f06e(0x76f)]['clearRect'],Bitmap[_0x32f06e(0x76f)]['clearRect']=function(_0x529a3b,_0x3f9c26,_0x51d2f2,_0x1311c7){const _0x2e26fd=_0x32f06e;VisuMZ['CoreEngine'][_0x2e26fd(0x3fa)][_0x2e26fd(0x31a)](this,_0x529a3b,_0x3f9c26,_0x51d2f2,_0x1311c7),this[_0x2e26fd(0x6ac)]();},VisuMZ['CoreEngine'][_0x32f06e(0x2e6)]=Bitmap[_0x32f06e(0x76f)][_0x32f06e(0x3a9)],Bitmap[_0x32f06e(0x76f)][_0x32f06e(0x3a9)]=function(_0x14562c,_0x2c976d,_0x3f2369,_0x5c620a,_0x531876){const _0x1e77a9=_0x32f06e;VisuMZ[_0x1e77a9(0x424)][_0x1e77a9(0x2e6)][_0x1e77a9(0x31a)](this,_0x14562c,_0x2c976d,_0x3f2369,_0x5c620a,_0x531876),this[_0x1e77a9(0x6ac)]();},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x1f0)]=Bitmap['prototype'][_0x32f06e(0x579)],Bitmap[_0x32f06e(0x76f)][_0x32f06e(0x579)]=function(_0x9fc071,_0x12d6fa,_0x5a71ff,_0x1a256b,_0x533c3c){const _0x147620=_0x32f06e;VisuMZ[_0x147620(0x424)][_0x147620(0x1f0)][_0x147620(0x31a)](this,_0x9fc071,_0x12d6fa,_0x5a71ff,_0x1a256b,_0x533c3c),this['markCoreEngineModified']();},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x3ba)]=Bitmap[_0x32f06e(0x76f)]['gradientFillRect'],Bitmap[_0x32f06e(0x76f)][_0x32f06e(0x3ef)]=function(_0x23fc91,_0xb08a61,_0x5359af,_0x282c57,_0x5665d3,_0x2a50b4,_0x1920b5){const _0x3aa5a6=_0x32f06e;VisuMZ[_0x3aa5a6(0x424)][_0x3aa5a6(0x3ba)][_0x3aa5a6(0x31a)](this,_0x23fc91,_0xb08a61,_0x5359af,_0x282c57,_0x5665d3,_0x2a50b4,_0x1920b5),this[_0x3aa5a6(0x6ac)]();},VisuMZ[_0x32f06e(0x424)]['Bitmap_drawCircle']=Bitmap[_0x32f06e(0x76f)][_0x32f06e(0x324)],Bitmap['prototype'][_0x32f06e(0x324)]=function(_0x31c6d7,_0x3cc140,_0x264a3f,_0x5d64d9){const _0x3800d4=_0x32f06e;_0x31c6d7=Math[_0x3800d4(0x237)](_0x31c6d7),_0x3cc140=Math[_0x3800d4(0x237)](_0x3cc140),_0x264a3f=Math[_0x3800d4(0x237)](_0x264a3f),VisuMZ[_0x3800d4(0x424)]['Bitmap_drawCircle'][_0x3800d4(0x31a)](this,_0x31c6d7,_0x3cc140,_0x264a3f,_0x5d64d9),this[_0x3800d4(0x6ac)]();},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x26e)]=Bitmap['prototype'][_0x32f06e(0x683)],Bitmap[_0x32f06e(0x76f)][_0x32f06e(0x683)]=function(_0x415602){const _0x23e192=_0x32f06e;return Math[_0x23e192(0x237)](VisuMZ['CoreEngine']['Bitmap_measureTextWidth'][_0x23e192(0x31a)](this,_0x415602));},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x78b)]=Bitmap[_0x32f06e(0x76f)][_0x32f06e(0x516)],Bitmap[_0x32f06e(0x76f)][_0x32f06e(0x516)]=function(_0x20f0ef,_0x2f6847,_0x563545,_0x5304a4,_0x7106f1,_0x1eef29){const _0x3d3536=_0x32f06e;_0x2f6847=Math[_0x3d3536(0x237)](_0x2f6847),_0x563545=Math[_0x3d3536(0x237)](_0x563545),_0x5304a4=Math[_0x3d3536(0x237)](_0x5304a4),_0x7106f1=Math['round'](_0x7106f1),VisuMZ[_0x3d3536(0x424)]['Bitmap_drawText']['call'](this,_0x20f0ef,_0x2f6847,_0x563545,_0x5304a4,_0x7106f1,_0x1eef29),this['markCoreEngineModified']();},VisuMZ[_0x32f06e(0x424)]['Bitmap_drawTextOutline']=Bitmap[_0x32f06e(0x76f)][_0x32f06e(0x710)],Bitmap[_0x32f06e(0x76f)]['_drawTextOutline']=function(_0x41fa9f,_0xccaa83,_0xd31f2,_0x5a1bd4){const _0xd0ce8c=_0x32f06e;VisuMZ[_0xd0ce8c(0x424)]['Settings'][_0xd0ce8c(0x248)][_0xd0ce8c(0x567)]?this['_drawTextShadow'](_0x41fa9f,_0xccaa83,_0xd31f2,_0x5a1bd4):VisuMZ[_0xd0ce8c(0x424)]['Bitmap_drawTextOutline'][_0xd0ce8c(0x31a)](this,_0x41fa9f,_0xccaa83,_0xd31f2,_0x5a1bd4);},Bitmap[_0x32f06e(0x76f)][_0x32f06e(0x507)]=function(_0x4444ac,_0x3a2531,_0x2518ab,_0x1719b1){const _0x5b0c87=_0x32f06e,_0x3be0b6=this[_0x5b0c87(0x719)];_0x3be0b6['fillStyle']=this[_0x5b0c87(0x594)],_0x3be0b6[_0x5b0c87(0x6d6)](_0x4444ac,_0x3a2531+0x2,_0x2518ab+0x2,_0x1719b1);},VisuMZ['CoreEngine']['Input_clear']=Input[_0x32f06e(0x353)],Input['clear']=function(){const _0xb5642=_0x32f06e;VisuMZ[_0xb5642(0x424)][_0xb5642(0x223)][_0xb5642(0x31a)](this),this[_0xb5642(0x2da)]=undefined,this[_0xb5642(0x72e)]=undefined,this[_0xb5642(0x252)]=Input[_0xb5642(0x611)];},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x30f)]=Input[_0x32f06e(0x209)],Input[_0x32f06e(0x209)]=function(){const _0x5d2ac9=_0x32f06e;VisuMZ[_0x5d2ac9(0x424)][_0x5d2ac9(0x30f)]['call'](this);if(this['_gamepadWait'])this['_gamepadWait']--;},VisuMZ[_0x32f06e(0x424)]['Input_pollGamepads']=Input[_0x32f06e(0x535)],Input[_0x32f06e(0x535)]=function(){const _0x3e41e7=_0x32f06e;if(this[_0x3e41e7(0x252)])return;VisuMZ['CoreEngine'][_0x3e41e7(0x33d)][_0x3e41e7(0x31a)](this);},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x57c)]=Input['_setupEventHandlers'],Input['_setupEventHandlers']=function(){const _0x41eaad=_0x32f06e;VisuMZ[_0x41eaad(0x424)]['Input_setupEventHandlers'][_0x41eaad(0x31a)](this),document[_0x41eaad(0x671)](_0x41eaad(0x3a8),this['_onKeyPress'][_0x41eaad(0x3da)](this));},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x1ec)]=Input['_onKeyDown'],Input[_0x32f06e(0x363)]=function(_0x2e39b2){const _0x196db9=_0x32f06e;this['_inputSpecialKeyCode']=_0x2e39b2[_0x196db9(0x554)],VisuMZ['CoreEngine'][_0x196db9(0x1ec)][_0x196db9(0x31a)](this,_0x2e39b2);},Input[_0x32f06e(0x28d)]=function(_0x185819){this['_registerKeyInput'](_0x185819);},Input[_0x32f06e(0x490)]=function(_0x355889){const _0x1f0b88=_0x32f06e;this['_inputSpecialKeyCode']=_0x355889[_0x1f0b88(0x554)];let _0x3463bc=String[_0x1f0b88(0x268)](_0x355889[_0x1f0b88(0x28a)]);this[_0x1f0b88(0x2da)]===undefined?this[_0x1f0b88(0x2da)]=_0x3463bc:this[_0x1f0b88(0x2da)]+=_0x3463bc;},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x2bb)]=Input[_0x32f06e(0x799)],Input[_0x32f06e(0x799)]=function(_0x4078f4){const _0x49eae3=_0x32f06e;if(_0x4078f4===0x8)return![];return VisuMZ[_0x49eae3(0x424)][_0x49eae3(0x2bb)][_0x49eae3(0x31a)](this,_0x4078f4);},Input[_0x32f06e(0x43a)]=function(_0x296395){const _0x4cf2aa=_0x32f06e;if(_0x296395[_0x4cf2aa(0x4c0)](/backspace/i))return this[_0x4cf2aa(0x72e)]===0x8;if(_0x296395[_0x4cf2aa(0x4c0)](/enter/i))return this[_0x4cf2aa(0x72e)]===0xd;if(_0x296395[_0x4cf2aa(0x4c0)](/escape/i))return this[_0x4cf2aa(0x72e)]===0x1b;},Input[_0x32f06e(0x4a2)]=function(){const _0x44073=_0x32f06e;return[0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39][_0x44073(0x43c)](this[_0x44073(0x72e)]);},Input[_0x32f06e(0x4c4)]=function(){const _0x5bba78=_0x32f06e;return[0x25,0x26,0x27,0x28][_0x5bba78(0x43c)](this[_0x5bba78(0x72e)]);},VisuMZ['CoreEngine'][_0x32f06e(0x4b5)]=Tilemap['prototype'][_0x32f06e(0x798)],Tilemap[_0x32f06e(0x76f)]['_addShadow']=function(_0x4ba083,_0x1772f1,_0x429bda,_0x17c7a8){const _0x44ea46=_0x32f06e;if($gameMap&&$gameMap['areTileShadowsHidden']())return;VisuMZ['CoreEngine'][_0x44ea46(0x4b5)][_0x44ea46(0x31a)](this,_0x4ba083,_0x1772f1,_0x429bda,_0x17c7a8);},Tilemap[_0x32f06e(0x4af)][_0x32f06e(0x76f)]['_createInternalTextures']=function(){const _0x403393=_0x32f06e;this[_0x403393(0x655)]();for(let _0x500ef3=0x0;_0x500ef3<Tilemap[_0x403393(0x423)][_0x403393(0x78a)];_0x500ef3++){const _0xfa2e94=new PIXI[(_0x403393(0x1de))]();_0xfa2e94['setSize'](0x800,0x800),VisuMZ[_0x403393(0x424)][_0x403393(0x50d)][_0x403393(0x248)][_0x403393(0x764)]&&(_0xfa2e94[_0x403393(0x2e7)]=PIXI[_0x403393(0x257)]['NEAREST']),this['_internalTextures']['push'](_0xfa2e94);}},WindowLayer[_0x32f06e(0x76f)][_0x32f06e(0x61d)]=function(){const _0x12b19c=_0x32f06e;return SceneManager&&SceneManager[_0x12b19c(0x269)]?SceneManager[_0x12b19c(0x269)][_0x12b19c(0x43e)]():!![];},VisuMZ['CoreEngine']['WindowLayer_render']=WindowLayer['prototype'][_0x32f06e(0x62b)],WindowLayer[_0x32f06e(0x76f)]['render']=function render(_0xedc46b){const _0x160a1b=_0x32f06e;this[_0x160a1b(0x61d)]()?VisuMZ[_0x160a1b(0x424)][_0x160a1b(0x51d)]['call'](this,_0xedc46b):this[_0x160a1b(0x2e8)](_0xedc46b);},WindowLayer[_0x32f06e(0x76f)][_0x32f06e(0x2e8)]=function render(_0x50f9d7){const _0x961e47=_0x32f06e;if(!this[_0x961e47(0x435)])return;const _0xe3894e=new PIXI['Graphics'](),_0x49e810=_0x50f9d7['gl'],_0x50edce=this[_0x961e47(0x548)]['clone']();_0x50f9d7[_0x961e47(0x2a1)][_0x961e47(0x277)](),_0xe3894e[_0x961e47(0x2cb)]=this[_0x961e47(0x2cb)],_0x50f9d7['batch'][_0x961e47(0x6c3)](),_0x49e810['enable'](_0x49e810['STENCIL_TEST']);while(_0x50edce[_0x961e47(0x26a)]>0x0){const _0x2404c9=_0x50edce[_0x961e47(0x62e)]();_0x2404c9['_isWindow']&&_0x2404c9[_0x961e47(0x435)]&&_0x2404c9[_0x961e47(0x200)]>0x0&&(_0x49e810['stencilFunc'](_0x49e810[_0x961e47(0x63c)],0x0,~0x0),_0x49e810[_0x961e47(0x243)](_0x49e810[_0x961e47(0x2bf)],_0x49e810['KEEP'],_0x49e810['KEEP']),_0x2404c9[_0x961e47(0x62b)](_0x50f9d7),_0x50f9d7[_0x961e47(0x3e1)]['flush'](),_0xe3894e[_0x961e47(0x353)](),_0x49e810[_0x961e47(0x3eb)](_0x49e810['ALWAYS'],0x1,~0x0),_0x49e810[_0x961e47(0x243)](_0x49e810[_0x961e47(0x1e4)],_0x49e810[_0x961e47(0x1e4)],_0x49e810[_0x961e47(0x1e4)]),_0x49e810[_0x961e47(0x4c1)](_0x49e810[_0x961e47(0x28b)],_0x49e810[_0x961e47(0x78d)]),_0xe3894e[_0x961e47(0x62b)](_0x50f9d7),_0x50f9d7[_0x961e47(0x3e1)]['flush'](),_0x49e810['blendFunc'](_0x49e810[_0x961e47(0x78d)],_0x49e810[_0x961e47(0x3a6)]));}_0x49e810[_0x961e47(0x3ce)](_0x49e810[_0x961e47(0x4e7)]),_0x49e810[_0x961e47(0x353)](_0x49e810[_0x961e47(0x72c)]),_0x49e810[_0x961e47(0x275)](0x0),_0x50f9d7['batch'][_0x961e47(0x6c3)]();for(const _0x38a27e of this[_0x961e47(0x548)]){!_0x38a27e[_0x961e47(0x686)]&&_0x38a27e[_0x961e47(0x435)]&&_0x38a27e[_0x961e47(0x62b)](_0x50f9d7);}_0x50f9d7[_0x961e47(0x3e1)][_0x961e47(0x6c3)]();},DataManager[_0x32f06e(0x582)]=function(_0x5ecc31){const _0xcfb950=_0x32f06e;return this['isItem'](_0x5ecc31)&&_0x5ecc31[_0xcfb950(0x72a)]===0x2;},VisuMZ['CoreEngine'][_0x32f06e(0x438)]=DataManager['setupNewGame'],DataManager[_0x32f06e(0x53e)]=function(){const _0x394c4b=_0x32f06e;VisuMZ[_0x394c4b(0x424)]['DataManager_setupNewGame'][_0x394c4b(0x31a)](this),this[_0x394c4b(0x4fa)](),this['reserveNewGameCommonEvent']();},DataManager[_0x32f06e(0x4fa)]=function(){const _0x395dc3=_0x32f06e;if($gameTemp['isPlaytest']()){const _0x3ddc28=VisuMZ[_0x395dc3(0x424)][_0x395dc3(0x50d)][_0x395dc3(0x248)][_0x395dc3(0x52c)];if(_0x3ddc28>0x0)$gameTemp['reserveCommonEvent'](_0x3ddc28);}},DataManager[_0x32f06e(0x405)]=function(){const _0x219233=_0x32f06e,_0x59e420=VisuMZ[_0x219233(0x424)]['Settings'][_0x219233(0x248)][_0x219233(0x2d7)]||0x0;if(_0x59e420>0x0)$gameTemp[_0x219233(0x35b)](_0x59e420);},TextManager[_0x32f06e(0x749)]=['','','',_0x32f06e(0x781),'','',_0x32f06e(0x356),'',_0x32f06e(0x453),_0x32f06e(0x4e6),'','','CLEAR',_0x32f06e(0x301),_0x32f06e(0x5ce),'',_0x32f06e(0x305),'CTRL',_0x32f06e(0x4b8),'PAUSE',_0x32f06e(0x779),_0x32f06e(0x514),'EISU',_0x32f06e(0x6e4),_0x32f06e(0x368),'HANJA','',_0x32f06e(0x219),_0x32f06e(0x415),'NONCONVERT',_0x32f06e(0x27c),_0x32f06e(0x73f),_0x32f06e(0x4a0),_0x32f06e(0x2aa),_0x32f06e(0x4f9),_0x32f06e(0x608),'HOME','LEFT','UP',_0x32f06e(0x46a),'DOWN',_0x32f06e(0x2a4),'PRINT','EXECUTE',_0x32f06e(0x2ee),'INSERT',_0x32f06e(0x5a4),'','0','1','2','3','4','5','6','7','8','9','COLON',_0x32f06e(0x360),'LESS_THAN','EQUALS',_0x32f06e(0x4ab),_0x32f06e(0x3d1),'AT','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',_0x32f06e(0x4f6),'',_0x32f06e(0x5bb),'',_0x32f06e(0x28f),_0x32f06e(0x250),_0x32f06e(0x479),'NUMPAD2','NUMPAD3','NUMPAD4',_0x32f06e(0x726),'NUMPAD6','NUMPAD7',_0x32f06e(0x6d4),_0x32f06e(0x5c8),'MULTIPLY','ADD',_0x32f06e(0x6b6),_0x32f06e(0x339),'DECIMAL','DIVIDE','F1','F2','F3','F4','F5','F6','F7','F8','F9',_0x32f06e(0x5cd),_0x32f06e(0x3c7),'F12',_0x32f06e(0x3d5),'F14',_0x32f06e(0x58d),_0x32f06e(0x320),_0x32f06e(0x6b5),_0x32f06e(0x395),'F19',_0x32f06e(0x348),_0x32f06e(0x758),_0x32f06e(0x5ea),_0x32f06e(0x588),_0x32f06e(0x44a),'','','','','','','','','NUM_LOCK',_0x32f06e(0x37f),'WIN_OEM_FJ_JISHO',_0x32f06e(0x2db),'WIN_OEM_FJ_TOUROKU',_0x32f06e(0x2fb),_0x32f06e(0x721),'','','','','','','','','',_0x32f06e(0x406),_0x32f06e(0x724),_0x32f06e(0x5d6),_0x32f06e(0x52e),_0x32f06e(0x4c6),_0x32f06e(0x5f7),_0x32f06e(0x20c),'UNDERSCORE',_0x32f06e(0x3d6),'CLOSE_PAREN',_0x32f06e(0x3c6),_0x32f06e(0x203),_0x32f06e(0x5ed),_0x32f06e(0x430),_0x32f06e(0x381),'CLOSE_CURLY_BRACKET',_0x32f06e(0x735),'','','','','VOLUME_MUTE',_0x32f06e(0x6a8),_0x32f06e(0x434),'','','SEMICOLON',_0x32f06e(0x6cf),_0x32f06e(0x1e1),_0x32f06e(0x2cd),'PERIOD',_0x32f06e(0x1db),'BACK_QUOTE','','','','','','','','','','','','','','','','','','','','','','','','','','',_0x32f06e(0x556),_0x32f06e(0x5a5),_0x32f06e(0x3bc),'QUOTE','',_0x32f06e(0x3f2),_0x32f06e(0x4e8),'',_0x32f06e(0x623),_0x32f06e(0x5ef),'',_0x32f06e(0x635),'','',_0x32f06e(0x2c0),_0x32f06e(0x66b),'WIN_OEM_PA1',_0x32f06e(0x427),_0x32f06e(0x4f4),_0x32f06e(0x41d),_0x32f06e(0x442),'WIN_OEM_ATTN',_0x32f06e(0x57a),_0x32f06e(0x593),_0x32f06e(0x4a3),_0x32f06e(0x23d),_0x32f06e(0x5e5),_0x32f06e(0x651),_0x32f06e(0x30d),_0x32f06e(0x26c),_0x32f06e(0x380),_0x32f06e(0x694),_0x32f06e(0x1d8),'',_0x32f06e(0x547),_0x32f06e(0x4ef),''],TextManager[_0x32f06e(0x3b0)]=VisuMZ['CoreEngine'][_0x32f06e(0x50d)]['ButtonAssist']['OkText'],TextManager['buttonAssistCancel']=VisuMZ['CoreEngine'][_0x32f06e(0x50d)][_0x32f06e(0x673)][_0x32f06e(0x399)],TextManager['buttonAssistSwitch']=VisuMZ['CoreEngine'][_0x32f06e(0x50d)][_0x32f06e(0x673)]['SwitchActorText'],VisuMZ['CoreEngine'][_0x32f06e(0x4d2)]=TextManager[_0x32f06e(0x6bc)],TextManager[_0x32f06e(0x6bc)]=function(_0x1809fc){const _0x75b5fe=_0x32f06e;return typeof _0x1809fc===_0x75b5fe(0x3e5)?VisuMZ[_0x75b5fe(0x424)][_0x75b5fe(0x4d2)][_0x75b5fe(0x31a)](this,_0x1809fc):this[_0x75b5fe(0x5f1)](_0x1809fc);},TextManager[_0x32f06e(0x5f1)]=function(_0x541057){const _0x238b4c=_0x32f06e;_0x541057=String(_0x541057||'')[_0x238b4c(0x79b)]();const _0x441746=VisuMZ[_0x238b4c(0x424)]['Settings'][_0x238b4c(0x1ea)];if(_0x541057==='MAXHP')return $dataSystem[_0x238b4c(0x704)][_0x238b4c(0x286)][0x0];if(_0x541057===_0x238b4c(0x6d7))return $dataSystem[_0x238b4c(0x704)]['params'][0x1];if(_0x541057==='ATK')return $dataSystem['terms'][_0x238b4c(0x286)][0x2];if(_0x541057===_0x238b4c(0x213))return $dataSystem[_0x238b4c(0x704)]['params'][0x3];if(_0x541057==='MAT')return $dataSystem[_0x238b4c(0x704)]['params'][0x4];if(_0x541057===_0x238b4c(0x317))return $dataSystem[_0x238b4c(0x704)][_0x238b4c(0x286)][0x5];if(_0x541057===_0x238b4c(0x321))return $dataSystem['terms'][_0x238b4c(0x286)][0x6];if(_0x541057===_0x238b4c(0x5fa))return $dataSystem['terms'][_0x238b4c(0x286)][0x7];if(_0x541057===_0x238b4c(0x612))return _0x441746['XParamVocab0'];if(_0x541057===_0x238b4c(0x4cb))return _0x441746[_0x238b4c(0x586)];if(_0x541057===_0x238b4c(0x4ce))return _0x441746[_0x238b4c(0x221)];if(_0x541057===_0x238b4c(0x445))return _0x441746[_0x238b4c(0x3cb)];if(_0x541057===_0x238b4c(0x51b))return _0x441746[_0x238b4c(0x456)];if(_0x541057===_0x238b4c(0x6eb))return _0x441746['XParamVocab5'];if(_0x541057===_0x238b4c(0x5d2))return _0x441746[_0x238b4c(0x47b)];if(_0x541057==='HRG')return _0x441746[_0x238b4c(0x5fe)];if(_0x541057===_0x238b4c(0x76c))return _0x441746[_0x238b4c(0x2d9)];if(_0x541057===_0x238b4c(0x66e))return _0x441746[_0x238b4c(0x628)];if(_0x541057===_0x238b4c(0x1ee))return _0x441746[_0x238b4c(0x5ca)];if(_0x541057===_0x238b4c(0x48d))return _0x441746[_0x238b4c(0x1f5)];if(_0x541057==='REC')return _0x441746[_0x238b4c(0x6c4)];if(_0x541057===_0x238b4c(0x74f))return _0x441746[_0x238b4c(0x640)];if(_0x541057===_0x238b4c(0x4b6))return _0x441746[_0x238b4c(0x40c)];if(_0x541057===_0x238b4c(0x439))return _0x441746['SParamVocab5'];if(_0x541057===_0x238b4c(0x676))return _0x441746[_0x238b4c(0x2fe)];if(_0x541057==='MDR')return _0x441746[_0x238b4c(0x595)];if(_0x541057===_0x238b4c(0x342))return _0x441746['SParamVocab8'];if(_0x541057==='EXR')return _0x441746[_0x238b4c(0x5e0)];if(VisuMZ[_0x238b4c(0x424)][_0x238b4c(0x29a)][_0x541057])return VisuMZ[_0x238b4c(0x424)][_0x238b4c(0x29a)][_0x541057];return'';},TextManager[_0x32f06e(0x49c)]=function(_0x469f14){const _0xa3fcd=_0x32f06e;if(_0x469f14===_0xa3fcd(0x4cd))_0x469f14=_0xa3fcd(0x482);let _0x220aee=[];for(let _0x134e36 in Input[_0xa3fcd(0x5e2)]){_0x134e36=Number(_0x134e36);if(_0x134e36>=0x60&&_0x134e36<=0x69)continue;if([0x12,0x20][_0xa3fcd(0x77d)](_0x134e36))continue;_0x469f14===Input[_0xa3fcd(0x5e2)][_0x134e36]&&_0x220aee[_0xa3fcd(0x50f)](_0x134e36);}for(let _0x218c85=0x0;_0x218c85<_0x220aee[_0xa3fcd(0x26a)];_0x218c85++){_0x220aee[_0x218c85]=TextManager[_0xa3fcd(0x749)][_0x220aee[_0x218c85]];}return this[_0xa3fcd(0x446)](_0x220aee);},TextManager['makeInputButtonString']=function(_0x4ffa63){const _0x30ddcc=_0x32f06e,_0xbf9a13=VisuMZ[_0x30ddcc(0x424)]['Settings'][_0x30ddcc(0x673)],_0x2a7bec=_0xbf9a13[_0x30ddcc(0x2f6)],_0x490364=_0x4ffa63[_0x30ddcc(0x226)](),_0x4e3c44=_0x30ddcc(0x394)['format'](_0x490364);return _0xbf9a13[_0x4e3c44]?_0xbf9a13[_0x4e3c44]:_0x2a7bec[_0x30ddcc(0x262)](_0x490364);},TextManager[_0x32f06e(0x67f)]=function(_0x7ee55c,_0x4ea6c5){const _0x245cb4=_0x32f06e,_0x5230ff=VisuMZ['CoreEngine']['Settings']['ButtonAssist'],_0x1aafd6=_0x5230ff['MultiKeyFmt'],_0xb07f12=this[_0x245cb4(0x49c)](_0x7ee55c),_0x307ec9=this['getInputButtonString'](_0x4ea6c5);return _0x1aafd6[_0x245cb4(0x262)](_0xb07f12,_0x307ec9);},VisuMZ['CoreEngine'][_0x32f06e(0x48e)]=ColorManager[_0x32f06e(0x753)],ColorManager[_0x32f06e(0x753)]=function(){const _0x4c30ae=_0x32f06e;VisuMZ[_0x4c30ae(0x424)][_0x4c30ae(0x48e)][_0x4c30ae(0x31a)](this),this[_0x4c30ae(0x3fe)]=this[_0x4c30ae(0x3fe)]||{};},ColorManager[_0x32f06e(0x48b)]=function(_0x35d51a,_0x5ebbf2){const _0x3f3950=_0x32f06e;return _0x5ebbf2=String(_0x5ebbf2),this[_0x3f3950(0x3fe)]=this[_0x3f3950(0x3fe)]||{},_0x5ebbf2[_0x3f3950(0x4c0)](/#(.*)/i)?this['_colorCache'][_0x35d51a]=_0x3f3950(0x4da)['format'](String(RegExp['$1'])):this[_0x3f3950(0x3fe)][_0x35d51a]=this[_0x3f3950(0x70f)](Number(_0x5ebbf2)),this[_0x3f3950(0x3fe)][_0x35d51a];},ColorManager[_0x32f06e(0x420)]=function(_0x1e093e){const _0x538946=_0x32f06e;return _0x1e093e=String(_0x1e093e),_0x1e093e['match'](/#(.*)/i)?'#%1'[_0x538946(0x262)](String(RegExp['$1'])):this[_0x538946(0x70f)](Number(_0x1e093e));},ColorManager['clearCachedKeys']=function(){const _0x144b72=_0x32f06e;this[_0x144b72(0x3fe)]={};},ColorManager[_0x32f06e(0x754)]=function(){const _0xd5bc11=_0x32f06e,_0x373173=_0xd5bc11(0x1dd);this[_0xd5bc11(0x3fe)]=this['_colorCache']||{};if(this['_colorCache'][_0x373173])return this['_colorCache'][_0x373173];const _0x211472=VisuMZ['CoreEngine'][_0xd5bc11(0x50d)][_0xd5bc11(0x319)]['ColorNormal'];return this[_0xd5bc11(0x48b)](_0x373173,_0x211472);},ColorManager[_0x32f06e(0x637)]=function(){const _0x2618f1=_0x32f06e,_0x4463f1='_stored_systemColor';this[_0x2618f1(0x3fe)]=this[_0x2618f1(0x3fe)]||{};if(this[_0x2618f1(0x3fe)][_0x4463f1])return this[_0x2618f1(0x3fe)][_0x4463f1];const _0x3cd670=VisuMZ[_0x2618f1(0x424)][_0x2618f1(0x50d)][_0x2618f1(0x319)][_0x2618f1(0x5c4)];return this[_0x2618f1(0x48b)](_0x4463f1,_0x3cd670);},ColorManager['crisisColor']=function(){const _0x273a22=_0x32f06e,_0x69cd5e=_0x273a22(0x44e);this[_0x273a22(0x3fe)]=this['_colorCache']||{};if(this[_0x273a22(0x3fe)][_0x69cd5e])return this[_0x273a22(0x3fe)][_0x69cd5e];const _0x2eb16d=VisuMZ[_0x273a22(0x424)][_0x273a22(0x50d)][_0x273a22(0x319)][_0x273a22(0x3db)];return this['getColorDataFromPluginParameters'](_0x69cd5e,_0x2eb16d);},ColorManager[_0x32f06e(0x469)]=function(){const _0x407645=_0x32f06e,_0x4e579a=_0x407645(0x6a6);this[_0x407645(0x3fe)]=this['_colorCache']||{};if(this[_0x407645(0x3fe)][_0x4e579a])return this[_0x407645(0x3fe)][_0x4e579a];const _0x23a6d0=VisuMZ[_0x407645(0x424)][_0x407645(0x50d)][_0x407645(0x319)][_0x407645(0x71e)];return this[_0x407645(0x48b)](_0x4e579a,_0x23a6d0);},ColorManager[_0x32f06e(0x4f1)]=function(){const _0x2eb603=_0x32f06e,_0x5280d0=_0x2eb603(0x3bb);this[_0x2eb603(0x3fe)]=this[_0x2eb603(0x3fe)]||{};if(this['_colorCache'][_0x5280d0])return this['_colorCache'][_0x5280d0];const _0x3aec50=VisuMZ[_0x2eb603(0x424)][_0x2eb603(0x50d)][_0x2eb603(0x319)][_0x2eb603(0x693)];return this[_0x2eb603(0x48b)](_0x5280d0,_0x3aec50);},ColorManager[_0x32f06e(0x23c)]=function(){const _0x1051af=_0x32f06e,_0x5d885d='_stored_hpGaugeColor1';this[_0x1051af(0x3fe)]=this[_0x1051af(0x3fe)]||{};if(this[_0x1051af(0x3fe)][_0x5d885d])return this[_0x1051af(0x3fe)][_0x5d885d];const _0x12399a=VisuMZ[_0x1051af(0x424)][_0x1051af(0x50d)][_0x1051af(0x319)][_0x1051af(0x71d)];return this[_0x1051af(0x48b)](_0x5d885d,_0x12399a);},ColorManager['hpGaugeColor2']=function(){const _0xa1687a=_0x32f06e,_0x2c05c8=_0xa1687a(0x42d);this['_colorCache']=this[_0xa1687a(0x3fe)]||{};if(this['_colorCache'][_0x2c05c8])return this[_0xa1687a(0x3fe)][_0x2c05c8];const _0x2e1010=VisuMZ[_0xa1687a(0x424)]['Settings']['Color'][_0xa1687a(0x60a)];return this[_0xa1687a(0x48b)](_0x2c05c8,_0x2e1010);},ColorManager[_0x32f06e(0x652)]=function(){const _0x136098=_0x32f06e,_0x411773=_0x136098(0x717);this['_colorCache']=this[_0x136098(0x3fe)]||{};if(this['_colorCache'][_0x411773])return this['_colorCache'][_0x411773];const _0x560327=VisuMZ[_0x136098(0x424)][_0x136098(0x50d)][_0x136098(0x319)][_0x136098(0x24b)];return this[_0x136098(0x48b)](_0x411773,_0x560327);},ColorManager[_0x32f06e(0x613)]=function(){const _0x36802c=_0x32f06e,_0x2f1640=_0x36802c(0x2b3);this[_0x36802c(0x3fe)]=this[_0x36802c(0x3fe)]||{};if(this[_0x36802c(0x3fe)][_0x2f1640])return this[_0x36802c(0x3fe)][_0x2f1640];const _0x3e1deb=VisuMZ['CoreEngine'][_0x36802c(0x50d)]['Color'][_0x36802c(0x744)];return this[_0x36802c(0x48b)](_0x2f1640,_0x3e1deb);},ColorManager[_0x32f06e(0x64a)]=function(){const _0x2578bd=_0x32f06e,_0x1fa442=_0x2578bd(0x5c2);this['_colorCache']=this[_0x2578bd(0x3fe)]||{};if(this[_0x2578bd(0x3fe)][_0x1fa442])return this[_0x2578bd(0x3fe)][_0x1fa442];const _0x145515=VisuMZ['CoreEngine'][_0x2578bd(0x50d)][_0x2578bd(0x319)][_0x2578bd(0x6bb)];return this['getColorDataFromPluginParameters'](_0x1fa442,_0x145515);},ColorManager[_0x32f06e(0x23e)]=function(){const _0x349ca9=_0x32f06e,_0x32063e=_0x349ca9(0x67c);this[_0x349ca9(0x3fe)]=this[_0x349ca9(0x3fe)]||{};if(this[_0x349ca9(0x3fe)][_0x32063e])return this[_0x349ca9(0x3fe)][_0x32063e];const _0x479890=VisuMZ[_0x349ca9(0x424)][_0x349ca9(0x50d)][_0x349ca9(0x319)][_0x349ca9(0x504)];return this[_0x349ca9(0x48b)](_0x32063e,_0x479890);},ColorManager[_0x32f06e(0x347)]=function(){const _0x220ce9=_0x32f06e,_0x71df39=_0x220ce9(0x732);this[_0x220ce9(0x3fe)]=this[_0x220ce9(0x3fe)]||{};if(this[_0x220ce9(0x3fe)][_0x71df39])return this[_0x220ce9(0x3fe)][_0x71df39];const _0x32bc95=VisuMZ[_0x220ce9(0x424)][_0x220ce9(0x50d)][_0x220ce9(0x319)]['ColorPowerDown'];return this[_0x220ce9(0x48b)](_0x71df39,_0x32bc95);},ColorManager[_0x32f06e(0x37a)]=function(){const _0x47f8e5=_0x32f06e,_0xb7c1ff=_0x47f8e5(0x723);this[_0x47f8e5(0x3fe)]=this['_colorCache']||{};if(this[_0x47f8e5(0x3fe)][_0xb7c1ff])return this[_0x47f8e5(0x3fe)][_0xb7c1ff];const _0x307459=VisuMZ[_0x47f8e5(0x424)][_0x47f8e5(0x50d)]['Color']['ColorCTGauge1'];return this[_0x47f8e5(0x48b)](_0xb7c1ff,_0x307459);},ColorManager['ctGaugeColor2']=function(){const _0x3420d8=_0x32f06e,_0x30e4bd=_0x3420d8(0x23b);this[_0x3420d8(0x3fe)]=this[_0x3420d8(0x3fe)]||{};if(this[_0x3420d8(0x3fe)][_0x30e4bd])return this['_colorCache'][_0x30e4bd];const _0x590031=VisuMZ[_0x3420d8(0x424)][_0x3420d8(0x50d)]['Color']['ColorCTGauge2'];return this['getColorDataFromPluginParameters'](_0x30e4bd,_0x590031);},ColorManager['tpGaugeColor1']=function(){const _0x499fb5=_0x32f06e,_0x2141aa=_0x499fb5(0x5f0);this['_colorCache']=this[_0x499fb5(0x3fe)]||{};if(this[_0x499fb5(0x3fe)][_0x2141aa])return this['_colorCache'][_0x2141aa];const _0xe2ab51=VisuMZ[_0x499fb5(0x424)][_0x499fb5(0x50d)]['Color'][_0x499fb5(0x559)];return this[_0x499fb5(0x48b)](_0x2141aa,_0xe2ab51);},ColorManager[_0x32f06e(0x3cc)]=function(){const _0x5599ce=_0x32f06e,_0xe09cc7=_0x5599ce(0x553);this[_0x5599ce(0x3fe)]=this[_0x5599ce(0x3fe)]||{};if(this[_0x5599ce(0x3fe)][_0xe09cc7])return this[_0x5599ce(0x3fe)][_0xe09cc7];const _0x1d671f=VisuMZ[_0x5599ce(0x424)][_0x5599ce(0x50d)][_0x5599ce(0x319)][_0x5599ce(0x48f)];return this[_0x5599ce(0x48b)](_0xe09cc7,_0x1d671f);},ColorManager[_0x32f06e(0x396)]=function(){const _0x3187a9=_0x32f06e,_0x1d8c7e=_0x3187a9(0x4c3);this[_0x3187a9(0x3fe)]=this['_colorCache']||{};if(this[_0x3187a9(0x3fe)][_0x1d8c7e])return this['_colorCache'][_0x1d8c7e];const _0x361bd4=VisuMZ['CoreEngine'][_0x3187a9(0x50d)][_0x3187a9(0x319)][_0x3187a9(0x312)];return this[_0x3187a9(0x48b)](_0x1d8c7e,_0x361bd4);},ColorManager[_0x32f06e(0x500)]=function(){const _0x46c755=_0x32f06e,_0x53c2dd=_0x46c755(0x665);this[_0x46c755(0x3fe)]=this[_0x46c755(0x3fe)]||{};if(this[_0x46c755(0x3fe)][_0x53c2dd])return this[_0x46c755(0x3fe)][_0x53c2dd];const _0x57512f=VisuMZ[_0x46c755(0x424)][_0x46c755(0x50d)][_0x46c755(0x319)][_0x46c755(0x312)];return this[_0x46c755(0x48b)](_0x53c2dd,_0x57512f);},ColorManager[_0x32f06e(0x4d9)]=function(){const _0x35936c=_0x32f06e,_0x370cd8='_stored_expGaugeColor1';this['_colorCache']=this['_colorCache']||{};if(this[_0x35936c(0x3fe)][_0x370cd8])return this['_colorCache'][_0x370cd8];const _0x5afacb=VisuMZ[_0x35936c(0x424)][_0x35936c(0x50d)][_0x35936c(0x319)][_0x35936c(0x782)];return this['getColorDataFromPluginParameters'](_0x370cd8,_0x5afacb);},ColorManager[_0x32f06e(0x70c)]=function(){const _0x12680e=_0x32f06e,_0x35debc='_stored_expGaugeColor2';this[_0x12680e(0x3fe)]=this['_colorCache']||{};if(this[_0x12680e(0x3fe)][_0x35debc])return this[_0x12680e(0x3fe)][_0x35debc];const _0x57fa65=VisuMZ[_0x12680e(0x424)][_0x12680e(0x50d)][_0x12680e(0x319)][_0x12680e(0x78f)];return this[_0x12680e(0x48b)](_0x35debc,_0x57fa65);},ColorManager['maxLvGaugeColor1']=function(){const _0x1ead1e=_0x32f06e,_0x448912='_stored_maxLvGaugeColor1';this[_0x1ead1e(0x3fe)]=this[_0x1ead1e(0x3fe)]||{};if(this[_0x1ead1e(0x3fe)][_0x448912])return this[_0x1ead1e(0x3fe)][_0x448912];const _0x24ab7d=VisuMZ[_0x1ead1e(0x424)][_0x1ead1e(0x50d)][_0x1ead1e(0x319)][_0x1ead1e(0x2df)];return this[_0x1ead1e(0x48b)](_0x448912,_0x24ab7d);},ColorManager[_0x32f06e(0x31f)]=function(){const _0x3c216c=_0x32f06e,_0x58f1ec=_0x3c216c(0x4bf);this['_colorCache']=this['_colorCache']||{};if(this[_0x3c216c(0x3fe)][_0x58f1ec])return this['_colorCache'][_0x58f1ec];const _0x281d95=VisuMZ['CoreEngine'][_0x3c216c(0x50d)]['Color']['ColorMaxLvGauge2'];return this[_0x3c216c(0x48b)](_0x58f1ec,_0x281d95);},ColorManager[_0x32f06e(0x1ef)]=function(_0x4846ea){const _0x1f814c=_0x32f06e;return VisuMZ['CoreEngine']['Settings'][_0x1f814c(0x319)][_0x1f814c(0x481)]['call'](this,_0x4846ea);},ColorManager[_0x32f06e(0x528)]=function(_0x4e3ced){const _0x554373=_0x32f06e;return VisuMZ['CoreEngine']['Settings']['Color'][_0x554373(0x5fd)]['call'](this,_0x4e3ced);},ColorManager[_0x32f06e(0x5b1)]=function(_0x3fc6f5){const _0x33882f=_0x32f06e;return VisuMZ['CoreEngine'][_0x33882f(0x50d)][_0x33882f(0x319)][_0x33882f(0x6af)][_0x33882f(0x31a)](this,_0x3fc6f5);},ColorManager[_0x32f06e(0x6ea)]=function(_0x2ac876){const _0x57b355=_0x32f06e;return VisuMZ[_0x57b355(0x424)][_0x57b355(0x50d)]['Color'][_0x57b355(0x712)][_0x57b355(0x31a)](this,_0x2ac876);},ColorManager['damageColor']=function(_0x4af5a2){const _0x10d9d1=_0x32f06e;return VisuMZ['CoreEngine'][_0x10d9d1(0x50d)][_0x10d9d1(0x319)][_0x10d9d1(0x573)][_0x10d9d1(0x31a)](this,_0x4af5a2);},ColorManager[_0x32f06e(0x594)]=function(){const _0x3a47f4=_0x32f06e;return VisuMZ[_0x3a47f4(0x424)][_0x3a47f4(0x50d)]['Color'][_0x3a47f4(0x4bc)];},ColorManager[_0x32f06e(0x39c)]=function(){const _0x1f536d=_0x32f06e;return VisuMZ[_0x1f536d(0x424)][_0x1f536d(0x50d)][_0x1f536d(0x319)]['OutlineColorDmg']||_0x1f536d(0x64b);},ColorManager[_0x32f06e(0x20a)]=function(){const _0x42ffee=_0x32f06e;return VisuMZ['CoreEngine'][_0x42ffee(0x50d)][_0x42ffee(0x319)][_0x42ffee(0x2f7)]||_0x42ffee(0x63e);},ColorManager[_0x32f06e(0x718)]=function(){const _0xe2e8f5=_0x32f06e;return VisuMZ[_0xe2e8f5(0x424)]['Settings'][_0xe2e8f5(0x319)][_0xe2e8f5(0x523)];},ColorManager[_0x32f06e(0x6b4)]=function(){const _0xcf919d=_0x32f06e;return VisuMZ['CoreEngine']['Settings'][_0xcf919d(0x319)]['DimColor2'];},ColorManager[_0x32f06e(0x4b4)]=function(){const _0x5103d0=_0x32f06e;return VisuMZ[_0x5103d0(0x424)]['Settings']['Color'][_0x5103d0(0x2d6)];},ColorManager[_0x32f06e(0x2ba)]=function(){const _0x1cd8fc=_0x32f06e;return VisuMZ[_0x1cd8fc(0x424)][_0x1cd8fc(0x50d)][_0x1cd8fc(0x319)][_0x1cd8fc(0x59f)];},SceneManager[_0x32f06e(0x408)]=[],VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x33e)]=SceneManager['initialize'],SceneManager['initialize']=function(){const _0x5c6967=_0x32f06e;VisuMZ[_0x5c6967(0x424)][_0x5c6967(0x33e)][_0x5c6967(0x31a)](this),this[_0x5c6967(0x731)]();},VisuMZ['CoreEngine'][_0x32f06e(0x236)]=SceneManager[_0x32f06e(0x65d)],SceneManager[_0x32f06e(0x65d)]=function(_0x3cfe58){const _0x500012=_0x32f06e;if($gameTemp)this[_0x500012(0x488)](_0x3cfe58);VisuMZ[_0x500012(0x424)][_0x500012(0x236)][_0x500012(0x31a)](this,_0x3cfe58);},SceneManager[_0x32f06e(0x488)]=function(_0x759c2){const _0x8f4006=_0x32f06e;if(!_0x759c2[_0x8f4006(0x345)]&&!_0x759c2[_0x8f4006(0x3e4)])switch(_0x759c2['keyCode']){case 0x75:this[_0x8f4006(0x5d8)]();break;case 0x76:if(Input[_0x8f4006(0x750)](_0x8f4006(0x62e))||Input[_0x8f4006(0x750)](_0x8f4006(0x522)))return;this['playTestF7']();break;}},SceneManager['playTestF6']=function(){const _0x12391a=_0x32f06e;if($gameTemp[_0x12391a(0x3de)]()&&VisuMZ['CoreEngine'][_0x12391a(0x50d)]['QoL'][_0x12391a(0x4dd)]){ConfigManager['seVolume']!==0x0?(ConfigManager[_0x12391a(0x67b)]=0x0,ConfigManager[_0x12391a(0x4d8)]=0x0,ConfigManager['meVolume']=0x0,ConfigManager[_0x12391a(0x63b)]=0x0):(ConfigManager[_0x12391a(0x67b)]=0x64,ConfigManager['bgsVolume']=0x64,ConfigManager['meVolume']=0x64,ConfigManager[_0x12391a(0x63b)]=0x64);ConfigManager[_0x12391a(0x2b0)]();if(this[_0x12391a(0x269)][_0x12391a(0x38e)]===Scene_Options){if(this[_0x12391a(0x269)][_0x12391a(0x501)])this[_0x12391a(0x269)][_0x12391a(0x501)][_0x12391a(0x228)]();if(this[_0x12391a(0x269)][_0x12391a(0x289)])this[_0x12391a(0x269)][_0x12391a(0x289)]['refresh']();}}},SceneManager['playTestF7']=function(){const _0x3c14b6=_0x32f06e;$gameTemp[_0x3c14b6(0x3de)]()&&VisuMZ[_0x3c14b6(0x424)][_0x3c14b6(0x50d)][_0x3c14b6(0x248)][_0x3c14b6(0x5e6)]&&($gameTemp[_0x3c14b6(0x527)]=!$gameTemp['_playTestFastMode']);},SceneManager[_0x32f06e(0x731)]=function(){const _0xfe8983=_0x32f06e;this[_0xfe8983(0x22b)]=![],this['_hideButtons']=!VisuMZ['CoreEngine'][_0xfe8983(0x50d)]['UI'][_0xfe8983(0x3b2)];},SceneManager[_0x32f06e(0x4e2)]=function(_0x4b67cf){const _0x14f1a5=_0x32f06e;VisuMZ[_0x14f1a5(0x424)][_0x14f1a5(0x50d)]['UI'][_0x14f1a5(0x57f)]&&(this['_sideButtonLayout']=_0x4b67cf);},SceneManager[_0x32f06e(0x2a9)]=function(){const _0x1e2fca=_0x32f06e;return this[_0x1e2fca(0x22b)];},SceneManager[_0x32f06e(0x3e9)]=function(){const _0x55b924=_0x32f06e;return this[_0x55b924(0x310)];},SceneManager['areButtonsOutsideMainUI']=function(){const _0x4f38d3=_0x32f06e;return this['areButtonsHidden']()||this[_0x4f38d3(0x2a9)]();},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x72b)]=SceneManager['isGameActive'],SceneManager[_0x32f06e(0x58a)]=function(){const _0x45908a=_0x32f06e;return VisuMZ[_0x45908a(0x424)][_0x45908a(0x50d)][_0x45908a(0x248)][_0x45908a(0x624)]?VisuMZ[_0x45908a(0x424)]['SceneManager_isGameActive'][_0x45908a(0x31a)](this):!![];},SceneManager[_0x32f06e(0x205)]=function(_0x13e015){const _0x207ac0=_0x32f06e;if(_0x13e015 instanceof Error)this[_0x207ac0(0x21f)](_0x13e015);else _0x13e015 instanceof Array&&_0x13e015[0x0]==='LoadError'?this[_0x207ac0(0x6a5)](_0x13e015):this[_0x207ac0(0x57e)](_0x13e015);this['stop']();},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x2a3)]=BattleManager[_0x32f06e(0x74b)],BattleManager[_0x32f06e(0x74b)]=function(){const _0x2b86c3=_0x32f06e;if(VisuMZ[_0x2b86c3(0x424)][_0x2b86c3(0x50d)][_0x2b86c3(0x248)]['EscapeAlways'])this[_0x2b86c3(0x370)]();else return VisuMZ['CoreEngine']['BattleManager_processEscape'][_0x2b86c3(0x31a)](this);},BattleManager[_0x32f06e(0x370)]=function(){const _0x190bfe=_0x32f06e;return $gameParty[_0x190bfe(0x6dc)](),SoundManager[_0x190bfe(0x369)](),this[_0x190bfe(0x5df)](),!![];},BattleManager[_0x32f06e(0x67d)]=function(){const _0x24c75c=_0x32f06e;return $gameSystem[_0x24c75c(0x77c)]()>=0x1;},BattleManager[_0x32f06e(0x2f0)]=function(){return $gameSystem['getBattleSystem']()===0x1;},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x244)]=Game_Temp[_0x32f06e(0x76f)]['initialize'],Game_Temp[_0x32f06e(0x76f)][_0x32f06e(0x58e)]=function(){const _0x2e537f=_0x32f06e;VisuMZ[_0x2e537f(0x424)]['Game_Temp_initialize'][_0x2e537f(0x31a)](this),this[_0x2e537f(0x2b1)](),this[_0x2e537f(0x4b1)]();},Game_Temp[_0x32f06e(0x76f)]['forceOutOfPlaytest']=function(){const _0x32b2e9=_0x32f06e;VisuMZ[_0x32b2e9(0x424)][_0x32b2e9(0x50d)][_0x32b2e9(0x248)]['ForceNoPlayTest']&&(this[_0x32b2e9(0x2c2)]=![]);},Game_Temp[_0x32f06e(0x76f)][_0x32f06e(0x4b1)]=function(){const _0x22bc02=_0x32f06e;this[_0x22bc02(0x638)]=[];},Game_Temp[_0x32f06e(0x76f)]['requestFauxAnimation']=function(_0xdb8711,_0x23ee1d,_0x39ecbf,_0xdb4ca0){const _0x9645ca=_0x32f06e;if(!this['showFauxAnimations']())return;_0x39ecbf=_0x39ecbf||![],_0xdb4ca0=_0xdb4ca0||![];if($dataAnimations[_0x23ee1d]){const _0x43e56f={'targets':_0xdb8711,'animationId':_0x23ee1d,'mirror':_0x39ecbf,'mute':_0xdb4ca0};this[_0x9645ca(0x638)]['push'](_0x43e56f);for(const _0xe409ce of _0xdb8711){_0xe409ce['startAnimation']&&_0xe409ce['startAnimation']();}}},Game_Temp[_0x32f06e(0x76f)]['showFauxAnimations']=function(){return!![];},Game_Temp[_0x32f06e(0x76f)]['retrieveFauxAnimation']=function(){const _0x4c85d3=_0x32f06e;return this['_fauxAnimationQueue'][_0x4c85d3(0x62e)]();},Game_Temp[_0x32f06e(0x76f)][_0x32f06e(0x76e)]=function(_0x2a31cf){const _0x5beb3e=_0x32f06e;this[_0x5beb3e(0x5a2)]=_0x2a31cf;},Game_Temp[_0x32f06e(0x76f)]['getLastPluginCommandInterpreter']=function(){return this['_lastPluginCommandInterpreter'];},Game_Temp[_0x32f06e(0x76f)][_0x32f06e(0x2ff)]=function(){const _0x49da1d=_0x32f06e;this['_forcedTroopView']=undefined,this[_0x49da1d(0x37c)]=undefined;},Game_Temp['prototype'][_0x32f06e(0x776)]=function(_0x5a6d2d){const _0x420dba=_0x32f06e;$gameMap&&$dataMap&&$dataMap[_0x420dba(0x762)]&&this[_0x420dba(0x276)]($dataMap['note']);const _0x901e64=$dataTroops[_0x5a6d2d];_0x901e64&&this[_0x420dba(0x276)](_0x901e64['name']);},Game_Temp[_0x32f06e(0x76f)][_0x32f06e(0x276)]=function(_0x2b8e9e){const _0x174968=_0x32f06e;if(!_0x2b8e9e)return;if(_0x2b8e9e[_0x174968(0x4c0)](/<(?:FRONTVIEW|FRONT VIEW|FV)>/i))this['_forcedTroopView']='FV';else{if(_0x2b8e9e[_0x174968(0x4c0)](/<(?:SIDEVIEW|SIDE VIEW|SV)>/i))this[_0x174968(0x649)]='SV';else{if(_0x2b8e9e[_0x174968(0x4c0)](/<(?:BATTLEVIEW|BATTLE VIEW):[ ](.*)>/i)){const _0x35a748=String(RegExp['$1']);if(_0x35a748['match'](/(?:FRONTVIEW|FRONT VIEW|FV)/i))this['_forcedTroopView']='FV';else _0x35a748[_0x174968(0x4c0)](/(?:SIDEVIEW|SIDE VIEW|SV)/i)&&(this[_0x174968(0x649)]='SV');}}}if(_0x2b8e9e[_0x174968(0x4c0)](/<(?:DTB)>/i))this[_0x174968(0x37c)]=0x0;else{if(_0x2b8e9e['match'](/<(?:TPB|ATB)[ ]ACTIVE>/i))this[_0x174968(0x37c)]=0x1;else{if(_0x2b8e9e[_0x174968(0x4c0)](/<(?:TPB|ATB)[ ]WAIT>/i))this[_0x174968(0x37c)]=0x2;else{if(_0x2b8e9e['match'](/<(?:CTB)>/i))Imported[_0x174968(0x240)]&&(this['_forcedBattleSys']=_0x174968(0x2cc));else{if(_0x2b8e9e[_0x174968(0x4c0)](/<(?:STB)>/i))Imported['VisuMZ_2_BattleSystemSTB']&&(this[_0x174968(0x37c)]=_0x174968(0x737));else{if(_0x2b8e9e[_0x174968(0x4c0)](/<(?:BTB)>/i))Imported[_0x174968(0x64d)]&&(this[_0x174968(0x37c)]=_0x174968(0x365));else{if(_0x2b8e9e[_0x174968(0x4c0)](/<(?:FTB)>/i))Imported[_0x174968(0x264)]&&(this[_0x174968(0x37c)]='FTB');else{if(_0x2b8e9e[_0x174968(0x4c0)](/<(?:BATTLEVIEW|BATTLE VIEW):[ ](.*)>/i)){const _0x43e019=String(RegExp['$1']);if(_0x43e019[_0x174968(0x4c0)](/DTB/i))this[_0x174968(0x37c)]=0x0;else{if(_0x43e019['match'](/(?:TPB|ATB)[ ]ACTIVE/i))this[_0x174968(0x37c)]=0x1;else{if(_0x43e019[_0x174968(0x4c0)](/(?:TPB|ATB)[ ]WAIT/i))this[_0x174968(0x37c)]=0x2;else{if(_0x43e019['match'](/CTB/i))Imported[_0x174968(0x240)]&&(this[_0x174968(0x37c)]='CTB');else{if(_0x43e019[_0x174968(0x4c0)](/STB/i))Imported[_0x174968(0x659)]&&(this['_forcedBattleSys']=_0x174968(0x737));else{if(_0x43e019[_0x174968(0x4c0)](/BTB/i))Imported['VisuMZ_2_BattleSystemBTB']&&(this[_0x174968(0x37c)]=_0x174968(0x365));else _0x43e019[_0x174968(0x4c0)](/FTB/i)&&(Imported[_0x174968(0x264)]&&(this[_0x174968(0x37c)]=_0x174968(0x463)));}}}}}}}}}}}}}},VisuMZ[_0x32f06e(0x424)]['Game_System_initialize']=Game_System['prototype'][_0x32f06e(0x58e)],Game_System['prototype']['initialize']=function(){const _0x447068=_0x32f06e;VisuMZ[_0x447068(0x424)][_0x447068(0x378)][_0x447068(0x31a)](this),this[_0x447068(0x566)]();},Game_System[_0x32f06e(0x76f)][_0x32f06e(0x566)]=function(){const _0xf9a70b=_0x32f06e;this[_0xf9a70b(0x6cc)]={'SideView':$dataSystem[_0xf9a70b(0x539)],'BattleSystem':this[_0xf9a70b(0x569)](),'FontSize':$dataSystem[_0xf9a70b(0x703)]['fontSize'],'Padding':0xc};},Game_System[_0x32f06e(0x76f)][_0x32f06e(0x3d2)]=function(){const _0x474e0a=_0x32f06e;if($gameTemp[_0x474e0a(0x649)]==='SV')return!![];else{if($gameTemp[_0x474e0a(0x649)]==='FV')return![];}if(this[_0x474e0a(0x6cc)]===undefined)this[_0x474e0a(0x566)]();if(this[_0x474e0a(0x6cc)][_0x474e0a(0x1eb)]===undefined)this[_0x474e0a(0x566)]();return this[_0x474e0a(0x6cc)][_0x474e0a(0x1eb)];},Game_System['prototype']['setSideView']=function(_0x1644ed){const _0x3c2089=_0x32f06e;if(this['_CoreEngineSettings']===undefined)this['initCoreEngine']();if(this['_CoreEngineSettings']['SideView']===undefined)this[_0x3c2089(0x566)]();this['_CoreEngineSettings']['SideView']=_0x1644ed;},Game_System[_0x32f06e(0x76f)]['resetBattleSystem']=function(){const _0xdc0031=_0x32f06e;if(this['_CoreEngineSettings']===undefined)this[_0xdc0031(0x566)]();this[_0xdc0031(0x6cc)]['BattleSystem']=this[_0xdc0031(0x569)]();},Game_System[_0x32f06e(0x76f)]['initialBattleSystem']=function(){const _0x1f6ac6=_0x32f06e,_0x361094=(VisuMZ[_0x1f6ac6(0x424)][_0x1f6ac6(0x50d)][_0x1f6ac6(0x3e3)]||_0x1f6ac6(0x521))[_0x1f6ac6(0x79b)]()[_0x1f6ac6(0x526)]();return VisuMZ[_0x1f6ac6(0x424)][_0x1f6ac6(0x6ad)](_0x361094);},Game_System[_0x32f06e(0x76f)][_0x32f06e(0x77c)]=function(){const _0x243d74=_0x32f06e;if($gameTemp[_0x243d74(0x37c)]!==undefined)return $gameTemp[_0x243d74(0x37c)];if(this['_CoreEngineSettings']===undefined)this['initCoreEngine']();if(this[_0x243d74(0x6cc)][_0x243d74(0x3e3)]===undefined)this[_0x243d74(0x747)]();return this[_0x243d74(0x6cc)][_0x243d74(0x3e3)];},Game_System[_0x32f06e(0x76f)]['setBattleSystem']=function(_0x26f76d){const _0x5131b2=_0x32f06e;if(this[_0x5131b2(0x6cc)]===undefined)this[_0x5131b2(0x566)]();if(this[_0x5131b2(0x6cc)][_0x5131b2(0x3e3)]===undefined)this[_0x5131b2(0x747)]();this['_CoreEngineSettings'][_0x5131b2(0x3e3)]=_0x26f76d;},Game_System[_0x32f06e(0x76f)][_0x32f06e(0x714)]=function(){const _0x55fc0f=_0x32f06e;if(this[_0x55fc0f(0x6cc)]===undefined)this[_0x55fc0f(0x566)]();if(this[_0x55fc0f(0x6cc)][_0x55fc0f(0x474)]===undefined)this[_0x55fc0f(0x566)]();return this[_0x55fc0f(0x6cc)][_0x55fc0f(0x474)];},Game_System[_0x32f06e(0x76f)][_0x32f06e(0x412)]=function(_0xab663a){const _0x21e0a7=_0x32f06e;if(this['_CoreEngineSettings']===undefined)this['initCoreEngine']();if(this[_0x21e0a7(0x6cc)]['TimeProgress']===undefined)this[_0x21e0a7(0x566)]();this[_0x21e0a7(0x6cc)][_0x21e0a7(0x474)]=_0xab663a;},Game_System['prototype'][_0x32f06e(0x274)]=function(){const _0x341b47=_0x32f06e;if(this[_0x341b47(0x6cc)]===undefined)this[_0x341b47(0x566)]();if(this[_0x341b47(0x6cc)][_0x341b47(0x6c1)]===undefined)this[_0x341b47(0x566)]();return this['_CoreEngineSettings']['Padding'];},Game_System[_0x32f06e(0x76f)][_0x32f06e(0x644)]=function(_0x2093bd){const _0x2e6851=_0x32f06e;if(this[_0x2e6851(0x6cc)]===undefined)this[_0x2e6851(0x566)]();if(this[_0x2e6851(0x6cc)][_0x2e6851(0x403)]===undefined)this[_0x2e6851(0x566)]();this[_0x2e6851(0x6cc)][_0x2e6851(0x6c1)]=_0x2093bd;},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x231)]=Game_Screen[_0x32f06e(0x76f)]['initialize'],Game_Screen[_0x32f06e(0x76f)][_0x32f06e(0x58e)]=function(){const _0x5aa7d5=_0x32f06e;VisuMZ[_0x5aa7d5(0x424)]['Game_Screen_initialize'][_0x5aa7d5(0x31a)](this),this[_0x5aa7d5(0x5f2)]();},Game_Screen[_0x32f06e(0x76f)][_0x32f06e(0x5f2)]=function(){const _0x304072=_0x32f06e,_0x48df7a=VisuMZ['CoreEngine'][_0x304072(0x50d)][_0x304072(0x4fd)];this['_coreEngineShakeStyle']=_0x48df7a?.['DefaultStyle']||_0x304072(0x772);},Game_Screen[_0x32f06e(0x76f)][_0x32f06e(0x4e9)]=function(){const _0x4574fd=_0x32f06e;if(this[_0x4574fd(0x454)]===undefined)this['initCoreEngineScreenShake']();return this[_0x4574fd(0x454)];},Game_Screen['prototype']['setCoreEngineScreenShakeStyle']=function(_0x5452f1){const _0x51d7ae=_0x32f06e;if(this[_0x51d7ae(0x454)]===undefined)this[_0x51d7ae(0x5f2)]();this[_0x51d7ae(0x454)]=_0x5452f1['toLowerCase']()[_0x51d7ae(0x526)]();},Game_Picture[_0x32f06e(0x76f)]['isMapScrollLinked']=function(){const _0x166385=_0x32f06e;if($gameParty[_0x166385(0x5c1)]())return![];return this[_0x166385(0x6b9)]()&&this[_0x166385(0x6b9)]()[_0x166385(0x642)](0x0)==='!';},VisuMZ['CoreEngine'][_0x32f06e(0x308)]=Game_Picture[_0x32f06e(0x76f)]['x'],Game_Picture[_0x32f06e(0x76f)]['x']=function(){const _0x18b4dc=_0x32f06e;return this[_0x18b4dc(0x232)]()?this[_0x18b4dc(0x4ad)]():VisuMZ[_0x18b4dc(0x424)][_0x18b4dc(0x308)][_0x18b4dc(0x31a)](this);},Game_Picture[_0x32f06e(0x76f)][_0x32f06e(0x4ad)]=function(){const _0x351576=_0x32f06e,_0x5b46c2=$gameMap[_0x351576(0x63f)]()*$gameMap[_0x351576(0x725)]();return this['_x']-_0x5b46c2;},VisuMZ[_0x32f06e(0x424)]['Game_Picture_y']=Game_Picture[_0x32f06e(0x76f)]['y'],Game_Picture[_0x32f06e(0x76f)]['y']=function(){const _0x2729d7=_0x32f06e;return this[_0x2729d7(0x232)]()?this['yScrollLinkedOffset']():VisuMZ[_0x2729d7(0x424)][_0x2729d7(0x2ad)][_0x2729d7(0x31a)](this);},Game_Picture['prototype'][_0x32f06e(0x6a2)]=function(){const _0x35d2c1=_0x32f06e,_0x33ceb4=$gameMap[_0x35d2c1(0x5e1)]()*$gameMap[_0x35d2c1(0x56e)]();return this['_y']-_0x33ceb4;},Game_Picture['prototype'][_0x32f06e(0x3d3)]=function(_0x26aaf8){this['_coreEasingType']=_0x26aaf8;},VisuMZ['CoreEngine']['Game_Picture_calcEasing']=Game_Picture[_0x32f06e(0x76f)]['calcEasing'],Game_Picture['prototype']['calcEasing']=function(_0x2e2235){const _0x26b280=_0x32f06e;return this[_0x26b280(0x71a)]=this['_coreEasingType']||0x0,[0x0,0x1,0x2,0x3]['includes'](this[_0x26b280(0x71a)])?VisuMZ[_0x26b280(0x424)]['Game_Picture_calcEasing'][_0x26b280(0x31a)](this,_0x2e2235):VisuMZ[_0x26b280(0x5d5)](_0x2e2235,this['_coreEasingType']);},VisuMZ['CoreEngine'][_0x32f06e(0x22a)]=Game_Action[_0x32f06e(0x76f)][_0x32f06e(0x409)],Game_Action[_0x32f06e(0x76f)][_0x32f06e(0x409)]=function(_0x4b6f07){const _0x157599=_0x32f06e;return VisuMZ[_0x157599(0x424)][_0x157599(0x50d)][_0x157599(0x248)]['ImprovedAccuracySystem']?this[_0x157599(0x2a2)](_0x4b6f07):VisuMZ['CoreEngine']['Game_Action_itemHit'][_0x157599(0x31a)](this,_0x4b6f07);},Game_Action[_0x32f06e(0x76f)][_0x32f06e(0x2a2)]=function(_0x39418d){const _0x523ab2=_0x32f06e,_0x1ffed2=this[_0x523ab2(0x54b)](_0x39418d),_0x3aa77f=this[_0x523ab2(0x743)](_0x39418d),_0x8922b9=this['targetEvaRate'](_0x39418d);return _0x1ffed2*(_0x3aa77f-_0x8922b9);},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x4d1)]=Game_Action[_0x32f06e(0x76f)][_0x32f06e(0x6df)],Game_Action[_0x32f06e(0x76f)]['itemEva']=function(_0x2addaf){const _0x82bdf3=_0x32f06e;return VisuMZ[_0x82bdf3(0x424)]['Settings'][_0x82bdf3(0x248)]['ImprovedAccuracySystem']?0x0:VisuMZ[_0x82bdf3(0x424)][_0x82bdf3(0x4d1)]['call'](this,_0x2addaf);},Game_Action[_0x32f06e(0x76f)]['itemSuccessRate']=function(_0xb3175d){const _0x56ea91=_0x32f06e;return this[_0x56ea91(0x6d9)]()[_0x56ea91(0x214)]*0.01;},Game_Action['prototype'][_0x32f06e(0x743)]=function(_0x201346){const _0xc8d143=_0x32f06e;if(VisuMZ['CoreEngine'][_0xc8d143(0x50d)][_0xc8d143(0x248)]['AccuracyBoost']&&this[_0xc8d143(0x333)]())return 0x1;return this['isPhysical']()?VisuMZ[_0xc8d143(0x424)][_0xc8d143(0x50d)][_0xc8d143(0x248)][_0xc8d143(0x67a)]&&this['subject']()[_0xc8d143(0x29f)]()?this[_0xc8d143(0x487)]()[_0xc8d143(0x34c)]+0.05:this['subject']()[_0xc8d143(0x34c)]:0x1;},Game_Action[_0x32f06e(0x76f)][_0x32f06e(0x3c5)]=function(_0x1bd806){const _0x2955be=_0x32f06e;if(this[_0x2955be(0x487)]()[_0x2955be(0x29f)]()===_0x1bd806[_0x2955be(0x29f)]())return 0x0;if(this[_0x2955be(0x687)]())return VisuMZ[_0x2955be(0x424)][_0x2955be(0x50d)][_0x2955be(0x248)][_0x2955be(0x67a)]&&_0x1bd806['isEnemy']()?_0x1bd806['eva']-0.05:_0x1bd806[_0x2955be(0x5f9)];else return this[_0x2955be(0x570)]()?_0x1bd806[_0x2955be(0x604)]:0x0;},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x251)]=Game_Action[_0x32f06e(0x76f)][_0x32f06e(0x484)],Game_Action[_0x32f06e(0x76f)][_0x32f06e(0x484)]=function(_0x534ab1){const _0x42a918=_0x32f06e;VisuMZ[_0x42a918(0x424)][_0x42a918(0x251)]['call'](this,_0x534ab1);if(VisuMZ[_0x42a918(0x424)]['Settings']['QoL'][_0x42a918(0x449)])return;const _0x161ca6=_0x534ab1[_0x42a918(0x658)]();_0x161ca6[_0x42a918(0x4bb)]&&(0x1-this[_0x42a918(0x6df)](_0x534ab1)>this[_0x42a918(0x409)](_0x534ab1)&&(_0x161ca6[_0x42a918(0x4bb)]=![],_0x161ca6[_0x42a918(0x3e0)]=!![]));},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x32a)]=Game_BattlerBase[_0x32f06e(0x76f)][_0x32f06e(0x2bd)],Game_BattlerBase[_0x32f06e(0x76f)][_0x32f06e(0x2bd)]=function(){const _0xad9717=_0x32f06e;this['_cache']={},VisuMZ[_0xad9717(0x424)][_0xad9717(0x32a)]['call'](this);},VisuMZ[_0x32f06e(0x424)]['Game_BattlerBase_refresh']=Game_BattlerBase[_0x32f06e(0x76f)][_0x32f06e(0x228)],Game_BattlerBase['prototype']['refresh']=function(){const _0x1ddbcb=_0x32f06e;this[_0x1ddbcb(0x715)]={},VisuMZ['CoreEngine'][_0x1ddbcb(0x303)][_0x1ddbcb(0x31a)](this);},Game_BattlerBase[_0x32f06e(0x76f)][_0x32f06e(0x27b)]=function(_0x2f99cb){const _0x41357d=_0x32f06e;return this[_0x41357d(0x715)]=this[_0x41357d(0x715)]||{},this[_0x41357d(0x715)][_0x2f99cb]!==undefined;},Game_BattlerBase['prototype']['paramPlus']=function(_0x20a2ef){const _0x37826a=_0x32f06e,_0x22a6c4=(_0x2bdf20,_0x1ebb5d)=>{const _0x134f08=_0x3168;if(!_0x1ebb5d)return _0x2bdf20;if(_0x1ebb5d['note'][_0x134f08(0x4c0)](VisuMZ['CoreEngine'][_0x134f08(0x4e0)]['paramPlus'][_0x20a2ef])){var _0x5ba346=Number(RegExp['$1']);_0x2bdf20+=_0x5ba346;}if(_0x1ebb5d[_0x134f08(0x762)][_0x134f08(0x4c0)](VisuMZ[_0x134f08(0x424)][_0x134f08(0x4e0)]['paramPlusJS'][_0x20a2ef])){var _0x56b581=String(RegExp['$1']);try{_0x2bdf20+=eval(_0x56b581);}catch(_0x191eae){if($gameTemp[_0x134f08(0x3de)]())console['log'](_0x191eae);}}return _0x2bdf20;};return this[_0x37826a(0x45a)]()[_0x37826a(0x2c9)](_0x22a6c4,this[_0x37826a(0x1e8)][_0x20a2ef]);},Game_BattlerBase[_0x32f06e(0x76f)]['paramMax']=function(_0x55ffba){const _0x1430f9=_0x32f06e;var _0x5b38b=_0x1430f9(0x757)+(this[_0x1430f9(0x29f)]()?_0x1430f9(0x6a0):_0x1430f9(0x75d))+_0x1430f9(0x4cf)+_0x55ffba;if(this[_0x1430f9(0x27b)](_0x5b38b))return this[_0x1430f9(0x715)][_0x5b38b];this[_0x1430f9(0x715)][_0x5b38b]=eval(VisuMZ[_0x1430f9(0x424)]['Settings'][_0x1430f9(0x1ea)][_0x5b38b]);const _0x60356a=(_0x39fb09,_0x345a4d)=>{const _0x4997b0=_0x1430f9;if(!_0x345a4d)return _0x39fb09;if(_0x345a4d[_0x4997b0(0x762)][_0x4997b0(0x4c0)](VisuMZ[_0x4997b0(0x424)][_0x4997b0(0x4e0)]['paramMax'][_0x55ffba])){var _0x30be42=Number(RegExp['$1']);if(_0x30be42===0x0)_0x30be42=Number[_0x4997b0(0x4ec)];_0x39fb09=Math[_0x4997b0(0x656)](_0x39fb09,_0x30be42);}if(_0x345a4d['note']['match'](VisuMZ['CoreEngine'][_0x4997b0(0x4e0)][_0x4997b0(0x576)][_0x55ffba])){var _0x3c1990=String(RegExp['$1']);try{_0x39fb09=Math[_0x4997b0(0x656)](_0x39fb09,Number(eval(_0x3c1990)));}catch(_0x4ddafe){if($gameTemp[_0x4997b0(0x3de)]())console['log'](_0x4ddafe);}}return _0x39fb09;};if(this[_0x1430f9(0x715)][_0x5b38b]===0x0)this[_0x1430f9(0x715)][_0x5b38b]=Number[_0x1430f9(0x4ec)];return this[_0x1430f9(0x715)][_0x5b38b]=this[_0x1430f9(0x45a)]()[_0x1430f9(0x2c9)](_0x60356a,this[_0x1430f9(0x715)][_0x5b38b]),this[_0x1430f9(0x715)][_0x5b38b];},Game_BattlerBase[_0x32f06e(0x76f)]['paramRate']=function(_0x82fb21){const _0x163ea7=_0x32f06e,_0x17552a=this['traitsPi'](Game_BattlerBase[_0x163ea7(0x218)],_0x82fb21),_0x13009f=(_0x5d0657,_0x4b4acf)=>{const _0x3f8f44=_0x163ea7;if(!_0x4b4acf)return _0x5d0657;if(_0x4b4acf['note'][_0x3f8f44(0x4c0)](VisuMZ['CoreEngine'][_0x3f8f44(0x4e0)][_0x3f8f44(0x69b)][_0x82fb21])){var _0x3c5012=Number(RegExp['$1'])/0x64;_0x5d0657*=_0x3c5012;}if(_0x4b4acf[_0x3f8f44(0x762)]['match'](VisuMZ[_0x3f8f44(0x424)]['RegExp'][_0x3f8f44(0x2dc)][_0x82fb21])){var _0x3c5012=Number(RegExp['$1']);_0x5d0657*=_0x3c5012;}if(_0x4b4acf['note'][_0x3f8f44(0x4c0)](VisuMZ['CoreEngine'][_0x3f8f44(0x4e0)][_0x3f8f44(0x492)][_0x82fb21])){var _0x481411=String(RegExp['$1']);try{_0x5d0657*=eval(_0x481411);}catch(_0x2c039d){if($gameTemp[_0x3f8f44(0x3de)]())console[_0x3f8f44(0x5c0)](_0x2c039d);}}return _0x5d0657;};return this['traitObjects']()['reduce'](_0x13009f,_0x17552a);},Game_BattlerBase[_0x32f06e(0x76f)][_0x32f06e(0x562)]=function(_0x3ac238){const _0x46ad5f=(_0x5b122a,_0x399f90)=>{const _0xd849d1=_0x3168;if(!_0x399f90)return _0x5b122a;if(_0x399f90['note']['match'](VisuMZ[_0xd849d1(0x424)][_0xd849d1(0x4e0)][_0xd849d1(0x364)][_0x3ac238])){var _0x52fb6c=Number(RegExp['$1']);_0x5b122a+=_0x52fb6c;}if(_0x399f90['note'][_0xd849d1(0x4c0)](VisuMZ['CoreEngine'][_0xd849d1(0x4e0)][_0xd849d1(0x5b2)][_0x3ac238])){var _0x3a8879=String(RegExp['$1']);try{_0x5b122a+=eval(_0x3a8879);}catch(_0x751239){if($gameTemp[_0xd849d1(0x3de)]())console[_0xd849d1(0x5c0)](_0x751239);}}return _0x5b122a;};return this['traitObjects']()['reduce'](_0x46ad5f,0x0);},Game_BattlerBase[_0x32f06e(0x76f)][_0x32f06e(0x6bc)]=function(_0x59796d){const _0x3f6c10=_0x32f06e;let _0x521961=_0x3f6c10(0x6bc)+_0x59796d+_0x3f6c10(0x745);if(this[_0x3f6c10(0x27b)](_0x521961))return this[_0x3f6c10(0x715)][_0x521961];return this['_cache'][_0x521961]=Math[_0x3f6c10(0x237)](VisuMZ[_0x3f6c10(0x424)][_0x3f6c10(0x50d)][_0x3f6c10(0x1ea)][_0x3f6c10(0x60c)]['call'](this,_0x59796d)),this[_0x3f6c10(0x715)][_0x521961];},Game_BattlerBase['prototype']['xparamPlus']=function(_0x32ffb3){const _0x2c4dfa=_0x32f06e,_0x5cfd05=(_0xc52161,_0x431dd0)=>{const _0x52953f=_0x3168;if(!_0x431dd0)return _0xc52161;if(_0x431dd0['note'][_0x52953f(0x4c0)](VisuMZ[_0x52953f(0x424)][_0x52953f(0x4e0)][_0x52953f(0x26f)][_0x32ffb3])){var _0xb68a6=Number(RegExp['$1'])/0x64;_0xc52161+=_0xb68a6;}if(_0x431dd0[_0x52953f(0x762)][_0x52953f(0x4c0)](VisuMZ['CoreEngine'][_0x52953f(0x4e0)][_0x52953f(0x235)][_0x32ffb3])){var _0xb68a6=Number(RegExp['$1']);_0xc52161+=_0xb68a6;}if(_0x431dd0[_0x52953f(0x762)][_0x52953f(0x4c0)](VisuMZ[_0x52953f(0x424)]['RegExp']['xparamPlusJS'][_0x32ffb3])){var _0x20525e=String(RegExp['$1']);try{_0xc52161+=eval(_0x20525e);}catch(_0x4f1ff8){if($gameTemp[_0x52953f(0x3de)]())console[_0x52953f(0x5c0)](_0x4f1ff8);}}return _0xc52161;};return this[_0x2c4dfa(0x45a)]()[_0x2c4dfa(0x2c9)](_0x5cfd05,0x0);},Game_BattlerBase[_0x32f06e(0x76f)][_0x32f06e(0x202)]=function(_0x191a17){const _0x406872=_0x32f06e,_0x5b8451=(_0x2b2acb,_0x334ee5)=>{const _0x459058=_0x3168;if(!_0x334ee5)return _0x2b2acb;if(_0x334ee5[_0x459058(0x762)][_0x459058(0x4c0)](VisuMZ[_0x459058(0x424)][_0x459058(0x4e0)][_0x459058(0x225)][_0x191a17])){var _0x869a54=Number(RegExp['$1'])/0x64;_0x2b2acb*=_0x869a54;}if(_0x334ee5[_0x459058(0x762)][_0x459058(0x4c0)](VisuMZ[_0x459058(0x424)][_0x459058(0x4e0)][_0x459058(0x36e)][_0x191a17])){var _0x869a54=Number(RegExp['$1']);_0x2b2acb*=_0x869a54;}if(_0x334ee5[_0x459058(0x762)]['match'](VisuMZ[_0x459058(0x424)]['RegExp'][_0x459058(0x626)][_0x191a17])){var _0x32e44f=String(RegExp['$1']);try{_0x2b2acb*=eval(_0x32e44f);}catch(_0x2f23dc){if($gameTemp[_0x459058(0x3de)]())console['log'](_0x2f23dc);}}return _0x2b2acb;};return this['traitObjects']()[_0x406872(0x2c9)](_0x5b8451,0x1);},Game_BattlerBase['prototype'][_0x32f06e(0x616)]=function(_0xe43999){const _0x45202b=_0x32f06e,_0xda6ec0=(_0x28b3a8,_0x4be870)=>{const _0x5ac735=_0x3168;if(!_0x4be870)return _0x28b3a8;if(_0x4be870['note']['match'](VisuMZ[_0x5ac735(0x424)]['RegExp'][_0x5ac735(0x35f)][_0xe43999])){var _0x313422=Number(RegExp['$1'])/0x64;_0x28b3a8+=_0x313422;}if(_0x4be870[_0x5ac735(0x762)][_0x5ac735(0x4c0)](VisuMZ[_0x5ac735(0x424)]['RegExp'][_0x5ac735(0x4d7)][_0xe43999])){var _0x313422=Number(RegExp['$1']);_0x28b3a8+=_0x313422;}if(_0x4be870[_0x5ac735(0x762)][_0x5ac735(0x4c0)](VisuMZ[_0x5ac735(0x424)][_0x5ac735(0x4e0)][_0x5ac735(0x5c9)][_0xe43999])){var _0x63575f=String(RegExp['$1']);try{_0x28b3a8+=eval(_0x63575f);}catch(_0x52af38){if($gameTemp[_0x5ac735(0x3de)]())console[_0x5ac735(0x5c0)](_0x52af38);}}return _0x28b3a8;};return this[_0x45202b(0x45a)]()['reduce'](_0xda6ec0,0x0);},Game_BattlerBase[_0x32f06e(0x76f)][_0x32f06e(0x775)]=function(_0x50dd2a){const _0x433a27=_0x32f06e;let _0x200401='xparam'+_0x50dd2a+'Total';if(this[_0x433a27(0x27b)](_0x200401))return this[_0x433a27(0x715)][_0x200401];return this[_0x433a27(0x715)][_0x200401]=VisuMZ[_0x433a27(0x424)]['Settings'][_0x433a27(0x1ea)][_0x433a27(0x42c)][_0x433a27(0x31a)](this,_0x50dd2a),this[_0x433a27(0x715)][_0x200401];},Game_BattlerBase['prototype'][_0x32f06e(0x27a)]=function(_0x5c9a0b){const _0x52ee82=_0x32f06e,_0xebfc4b=(_0x324a55,_0x310b9a)=>{const _0x164de=_0x3168;if(!_0x310b9a)return _0x324a55;if(_0x310b9a[_0x164de(0x762)][_0x164de(0x4c0)](VisuMZ[_0x164de(0x424)][_0x164de(0x4e0)][_0x164de(0x28c)][_0x5c9a0b])){var _0x152a61=Number(RegExp['$1'])/0x64;_0x324a55+=_0x152a61;}if(_0x310b9a[_0x164de(0x762)][_0x164de(0x4c0)](VisuMZ['CoreEngine']['RegExp']['sparamPlus2'][_0x5c9a0b])){var _0x152a61=Number(RegExp['$1']);_0x324a55+=_0x152a61;}if(_0x310b9a[_0x164de(0x762)][_0x164de(0x4c0)](VisuMZ[_0x164de(0x424)][_0x164de(0x4e0)][_0x164de(0x52a)][_0x5c9a0b])){var _0x29a1ec=String(RegExp['$1']);try{_0x324a55+=eval(_0x29a1ec);}catch(_0x8d40e5){if($gameTemp[_0x164de(0x3de)]())console[_0x164de(0x5c0)](_0x8d40e5);}}return _0x324a55;};return this[_0x52ee82(0x45a)]()['reduce'](_0xebfc4b,0x0);},Game_BattlerBase[_0x32f06e(0x76f)][_0x32f06e(0x4de)]=function(_0x1d9486){const _0x4d538f=_0x32f06e,_0x31eca1=(_0x30009c,_0x2d052d)=>{const _0x5dd94b=_0x3168;if(!_0x2d052d)return _0x30009c;if(_0x2d052d['note'][_0x5dd94b(0x4c0)](VisuMZ[_0x5dd94b(0x424)]['RegExp'][_0x5dd94b(0x34d)][_0x1d9486])){var _0x37139d=Number(RegExp['$1'])/0x64;_0x30009c*=_0x37139d;}if(_0x2d052d[_0x5dd94b(0x762)][_0x5dd94b(0x4c0)](VisuMZ['CoreEngine'][_0x5dd94b(0x4e0)][_0x5dd94b(0x3ae)][_0x1d9486])){var _0x37139d=Number(RegExp['$1']);_0x30009c*=_0x37139d;}if(_0x2d052d['note'][_0x5dd94b(0x4c0)](VisuMZ[_0x5dd94b(0x424)][_0x5dd94b(0x4e0)][_0x5dd94b(0x255)][_0x1d9486])){var _0x19f33c=String(RegExp['$1']);try{_0x30009c*=eval(_0x19f33c);}catch(_0xe3b1a1){if($gameTemp[_0x5dd94b(0x3de)]())console['log'](_0xe3b1a1);}}return _0x30009c;};return this[_0x4d538f(0x45a)]()[_0x4d538f(0x2c9)](_0x31eca1,0x1);},Game_BattlerBase['prototype'][_0x32f06e(0x565)]=function(_0x557b3d){const _0x1236f1=_0x32f06e,_0xe094eb=(_0x3bcc4e,_0x1c594e)=>{const _0x52a9ad=_0x3168;if(!_0x1c594e)return _0x3bcc4e;if(_0x1c594e[_0x52a9ad(0x762)][_0x52a9ad(0x4c0)](VisuMZ['CoreEngine']['RegExp'][_0x52a9ad(0x69e)][_0x557b3d])){var _0x185dbd=Number(RegExp['$1'])/0x64;_0x3bcc4e+=_0x185dbd;}if(_0x1c594e[_0x52a9ad(0x762)][_0x52a9ad(0x4c0)](VisuMZ[_0x52a9ad(0x424)][_0x52a9ad(0x4e0)][_0x52a9ad(0x61b)][_0x557b3d])){var _0x185dbd=Number(RegExp['$1']);_0x3bcc4e+=_0x185dbd;}if(_0x1c594e[_0x52a9ad(0x762)][_0x52a9ad(0x4c0)](VisuMZ[_0x52a9ad(0x424)][_0x52a9ad(0x4e0)]['sparamFlatJS'][_0x557b3d])){var _0x533870=String(RegExp['$1']);try{_0x3bcc4e+=eval(_0x533870);}catch(_0xb69733){if($gameTemp[_0x52a9ad(0x3de)]())console[_0x52a9ad(0x5c0)](_0xb69733);}}return _0x3bcc4e;};return this['traitObjects']()[_0x1236f1(0x2c9)](_0xe094eb,0x0);},Game_BattlerBase[_0x32f06e(0x76f)][_0x32f06e(0x26b)]=function(_0xfc6a5d){const _0x433ad1=_0x32f06e;let _0x36b792=_0x433ad1(0x26b)+_0xfc6a5d+'Total';if(this[_0x433ad1(0x27b)](_0x36b792))return this[_0x433ad1(0x715)][_0x36b792];return this[_0x433ad1(0x715)][_0x36b792]=VisuMZ[_0x433ad1(0x424)]['Settings'][_0x433ad1(0x1ea)]['SParameterFormula']['call'](this,_0xfc6a5d),this[_0x433ad1(0x715)][_0x36b792];},Game_BattlerBase[_0x32f06e(0x76f)][_0x32f06e(0x25f)]=function(_0x6a2c4b,_0x56a8ab){const _0x3a2a23=_0x32f06e;if(typeof paramId==='number')return this['param'](_0x6a2c4b);_0x6a2c4b=String(_0x6a2c4b||'')[_0x3a2a23(0x79b)]();if(_0x6a2c4b==='MAXHP')return this[_0x3a2a23(0x6bc)](0x0);if(_0x6a2c4b===_0x3a2a23(0x6d7))return this[_0x3a2a23(0x6bc)](0x1);if(_0x6a2c4b===_0x3a2a23(0x533))return this[_0x3a2a23(0x6bc)](0x2);if(_0x6a2c4b===_0x3a2a23(0x213))return this[_0x3a2a23(0x6bc)](0x3);if(_0x6a2c4b===_0x3a2a23(0x730))return this['param'](0x4);if(_0x6a2c4b===_0x3a2a23(0x317))return this[_0x3a2a23(0x6bc)](0x5);if(_0x6a2c4b===_0x3a2a23(0x321))return this[_0x3a2a23(0x6bc)](0x6);if(_0x6a2c4b===_0x3a2a23(0x5fa))return this['param'](0x7);if(_0x6a2c4b===_0x3a2a23(0x612))return _0x56a8ab?String(Math[_0x3a2a23(0x237)](this[_0x3a2a23(0x775)](0x0)*0x64))+'%':this['xparam'](0x0);if(_0x6a2c4b===_0x3a2a23(0x4cb))return _0x56a8ab?String(Math['round'](this[_0x3a2a23(0x775)](0x1)*0x64))+'%':this[_0x3a2a23(0x775)](0x1);if(_0x6a2c4b===_0x3a2a23(0x4ce))return _0x56a8ab?String(Math[_0x3a2a23(0x237)](this[_0x3a2a23(0x775)](0x2)*0x64))+'%':this[_0x3a2a23(0x775)](0x2);if(_0x6a2c4b===_0x3a2a23(0x445))return _0x56a8ab?String(Math['round'](this[_0x3a2a23(0x775)](0x3)*0x64))+'%':this['xparam'](0x3);if(_0x6a2c4b==='MEV')return _0x56a8ab?String(Math[_0x3a2a23(0x237)](this['xparam'](0x4)*0x64))+'%':this['xparam'](0x4);if(_0x6a2c4b===_0x3a2a23(0x6eb))return _0x56a8ab?String(Math[_0x3a2a23(0x237)](this[_0x3a2a23(0x775)](0x5)*0x64))+'%':this['xparam'](0x5);if(_0x6a2c4b===_0x3a2a23(0x5d2))return _0x56a8ab?String(Math[_0x3a2a23(0x237)](this[_0x3a2a23(0x775)](0x6)*0x64))+'%':this[_0x3a2a23(0x775)](0x6);if(_0x6a2c4b==='HRG')return _0x56a8ab?String(Math[_0x3a2a23(0x237)](this[_0x3a2a23(0x775)](0x7)*0x64))+'%':this[_0x3a2a23(0x775)](0x7);if(_0x6a2c4b===_0x3a2a23(0x76c))return _0x56a8ab?String(Math[_0x3a2a23(0x237)](this['xparam'](0x8)*0x64))+'%':this[_0x3a2a23(0x775)](0x8);if(_0x6a2c4b===_0x3a2a23(0x66e))return _0x56a8ab?String(Math['round'](this['xparam'](0x9)*0x64))+'%':this['xparam'](0x9);if(_0x6a2c4b==='TGR')return _0x56a8ab?String(Math[_0x3a2a23(0x237)](this[_0x3a2a23(0x26b)](0x0)*0x64))+'%':this[_0x3a2a23(0x26b)](0x0);if(_0x6a2c4b===_0x3a2a23(0x48d))return _0x56a8ab?String(Math[_0x3a2a23(0x237)](this[_0x3a2a23(0x26b)](0x1)*0x64))+'%':this[_0x3a2a23(0x26b)](0x1);if(_0x6a2c4b===_0x3a2a23(0x3b8))return _0x56a8ab?String(Math['round'](this[_0x3a2a23(0x26b)](0x2)*0x64))+'%':this[_0x3a2a23(0x26b)](0x2);if(_0x6a2c4b==='PHA')return _0x56a8ab?String(Math[_0x3a2a23(0x237)](this[_0x3a2a23(0x26b)](0x3)*0x64))+'%':this[_0x3a2a23(0x26b)](0x3);if(_0x6a2c4b===_0x3a2a23(0x4b6))return _0x56a8ab?String(Math[_0x3a2a23(0x237)](this[_0x3a2a23(0x26b)](0x4)*0x64))+'%':this[_0x3a2a23(0x26b)](0x4);if(_0x6a2c4b===_0x3a2a23(0x439))return _0x56a8ab?String(Math[_0x3a2a23(0x237)](this[_0x3a2a23(0x26b)](0x5)*0x64))+'%':this[_0x3a2a23(0x26b)](0x5);if(_0x6a2c4b===_0x3a2a23(0x676))return _0x56a8ab?String(Math[_0x3a2a23(0x237)](this[_0x3a2a23(0x26b)](0x6)*0x64))+'%':this[_0x3a2a23(0x26b)](0x6);if(_0x6a2c4b==='MDR')return _0x56a8ab?String(Math['round'](this[_0x3a2a23(0x26b)](0x7)*0x64))+'%':this[_0x3a2a23(0x26b)](0x7);if(_0x6a2c4b===_0x3a2a23(0x342))return _0x56a8ab?String(Math[_0x3a2a23(0x237)](this['sparam'](0x8)*0x64))+'%':this[_0x3a2a23(0x26b)](0x8);if(_0x6a2c4b===_0x3a2a23(0x45b))return _0x56a8ab?String(Math['round'](this['sparam'](0x9)*0x64))+'%':this[_0x3a2a23(0x26b)](0x9);if(VisuMZ['CoreEngine'][_0x3a2a23(0x315)][_0x6a2c4b]){const _0x35a334=VisuMZ[_0x3a2a23(0x424)][_0x3a2a23(0x315)][_0x6a2c4b],_0x12f253=this[_0x35a334];return VisuMZ[_0x3a2a23(0x424)][_0x3a2a23(0x2e0)][_0x6a2c4b]===_0x3a2a23(0x509)?_0x12f253:_0x56a8ab?String(Math[_0x3a2a23(0x237)](_0x12f253*0x64))+'%':_0x12f253;}return'';},Game_BattlerBase[_0x32f06e(0x76f)][_0x32f06e(0x729)]=function(){const _0x1b457a=_0x32f06e;return this[_0x1b457a(0x602)]()&&this[_0x1b457a(0x307)]<this[_0x1b457a(0x6ab)]*VisuMZ[_0x1b457a(0x424)][_0x1b457a(0x50d)][_0x1b457a(0x1ea)]['CrisisRate'];},Game_Battler['prototype'][_0x32f06e(0x30c)]=function(){const _0x438212=_0x32f06e;SoundManager[_0x438212(0x367)](),this['requestMotion'](_0x438212(0x2f9));},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x2f3)]=Game_Actor[_0x32f06e(0x76f)]['paramBase'],Game_Actor['prototype'][_0x32f06e(0x61a)]=function(_0x4559a4){const _0x1c9b14=_0x32f06e;if(this['level']>0x63)return this[_0x1c9b14(0x5b5)](_0x4559a4);return VisuMZ[_0x1c9b14(0x424)][_0x1c9b14(0x2f3)][_0x1c9b14(0x31a)](this,_0x4559a4);},Game_Actor[_0x32f06e(0x76f)][_0x32f06e(0x5b5)]=function(_0x39b7a4){const _0x23aaf2=_0x32f06e,_0x456f7b=this[_0x23aaf2(0x1ed)]()[_0x23aaf2(0x286)][_0x39b7a4][0x63],_0x177c3c=this['currentClass']()[_0x23aaf2(0x286)][_0x39b7a4][0x62];return _0x456f7b+(_0x456f7b-_0x177c3c)*(this[_0x23aaf2(0x625)]-0x63);},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x783)]=Game_Actor[_0x32f06e(0x76f)][_0x32f06e(0x3d9)],Game_Actor[_0x32f06e(0x76f)][_0x32f06e(0x3d9)]=function(_0x16bc48,_0x4023f5){const _0x4d6ae9=_0x32f06e;$gameTemp[_0x4d6ae9(0x6ca)]=!![],VisuMZ[_0x4d6ae9(0x424)][_0x4d6ae9(0x783)][_0x4d6ae9(0x31a)](this,_0x16bc48,_0x4023f5),$gameTemp['_changingClass']=undefined;},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x6c8)]=Game_Actor[_0x32f06e(0x76f)][_0x32f06e(0x46c)],Game_Actor['prototype']['levelUp']=function(){const _0x58ea9a=_0x32f06e;VisuMZ[_0x58ea9a(0x424)][_0x58ea9a(0x6c8)]['call'](this);if(!$gameTemp[_0x58ea9a(0x6ca)])this[_0x58ea9a(0x549)]();},Game_Actor[_0x32f06e(0x76f)][_0x32f06e(0x549)]=function(){const _0x5d80f8=_0x32f06e;this[_0x5d80f8(0x715)]={};if(VisuMZ[_0x5d80f8(0x424)][_0x5d80f8(0x50d)][_0x5d80f8(0x248)][_0x5d80f8(0x304)])this[_0x5d80f8(0x307)]=this[_0x5d80f8(0x6ab)];if(VisuMZ['CoreEngine'][_0x5d80f8(0x50d)]['QoL'][_0x5d80f8(0x6d5)])this[_0x5d80f8(0x552)]=this[_0x5d80f8(0x6ec)];},Game_Actor[_0x32f06e(0x76f)][_0x32f06e(0x246)]=function(){const _0x4250ba=_0x32f06e;if(this[_0x4250ba(0x41e)]())return 0x1;const _0xb69f38=this[_0x4250ba(0x43d)]()-this[_0x4250ba(0x50e)](),_0x22c221=this[_0x4250ba(0x784)]()-this[_0x4250ba(0x50e)]();return(_0x22c221/_0xb69f38)[_0x4250ba(0x32b)](0x0,0x1);},Game_Actor['prototype'][_0x32f06e(0x45a)]=function(){const _0x2acf35=_0x32f06e,_0x1e6197=Game_Battler['prototype']['traitObjects']['call'](this);for(const _0x3fa50c of this[_0x2acf35(0x38d)]()){_0x3fa50c&&_0x1e6197['push'](_0x3fa50c);}return _0x1e6197[_0x2acf35(0x50f)](this[_0x2acf35(0x1ed)](),this['actor']()),_0x1e6197;},Object[_0x32f06e(0x670)](Game_Enemy[_0x32f06e(0x76f)],_0x32f06e(0x625),{'get':function(){const _0x34c7e8=_0x32f06e;return this[_0x34c7e8(0x465)]();},'configurable':!![]}),Game_Enemy['prototype'][_0x32f06e(0x465)]=function(){const _0x31d326=_0x32f06e;return this[_0x31d326(0x291)]()[_0x31d326(0x625)];},Game_Enemy['prototype'][_0x32f06e(0x695)]=function(){const _0x66fd18=_0x32f06e;!this[_0x66fd18(0x379)]&&(this['_screenY']+=Math[_0x66fd18(0x237)]((Graphics[_0x66fd18(0x557)]-0x270)/0x2),this[_0x66fd18(0x75b)]-=Math[_0x66fd18(0x6db)]((Graphics[_0x66fd18(0x557)]-Graphics[_0x66fd18(0x707)])/0x2),$gameSystem[_0x66fd18(0x3d2)]()?this[_0x66fd18(0x696)]-=Math[_0x66fd18(0x6db)]((Graphics[_0x66fd18(0x450)]-Graphics['boxWidth'])/0x2):this[_0x66fd18(0x696)]+=Math['round']((Graphics['boxWidth']-0x330)/0x2)),this[_0x66fd18(0x379)]=!![];},Game_Party[_0x32f06e(0x76f)][_0x32f06e(0x261)]=function(){const _0x4d8b6b=_0x32f06e;return VisuMZ[_0x4d8b6b(0x424)][_0x4d8b6b(0x50d)][_0x4d8b6b(0x2c4)][_0x4d8b6b(0x419)];},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x464)]=Game_Party[_0x32f06e(0x76f)][_0x32f06e(0x41b)],Game_Party[_0x32f06e(0x76f)][_0x32f06e(0x41b)]=function(_0x1ff0f4){const _0x1b08b3=_0x32f06e;if(VisuMZ[_0x1b08b3(0x424)][_0x1b08b3(0x50d)][_0x1b08b3(0x248)]['KeyItemProtect']&&DataManager[_0x1b08b3(0x582)](_0x1ff0f4))return;VisuMZ[_0x1b08b3(0x424)]['Game_Party_consumeItem'][_0x1b08b3(0x31a)](this,_0x1ff0f4);},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x73d)]=Game_Troop[_0x32f06e(0x76f)][_0x32f06e(0x5fb)],Game_Troop[_0x32f06e(0x76f)][_0x32f06e(0x5fb)]=function(_0x327219){const _0x148025=_0x32f06e;$gameTemp[_0x148025(0x2ff)](),$gameTemp[_0x148025(0x776)](_0x327219),VisuMZ[_0x148025(0x424)][_0x148025(0x73d)]['call'](this,_0x327219);},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x5af)]=Game_Map[_0x32f06e(0x76f)][_0x32f06e(0x5fb)],Game_Map[_0x32f06e(0x76f)][_0x32f06e(0x5fb)]=function(_0x125de1){const _0xe41442=_0x32f06e;VisuMZ[_0xe41442(0x424)][_0xe41442(0x5af)][_0xe41442(0x31a)](this,_0x125de1),this[_0xe41442(0x497)](_0x125de1);},Game_Map[_0x32f06e(0x76f)]['setupCoreEngine']=function(){const _0x3242eb=_0x32f06e;this['_hideTileShadows']=VisuMZ[_0x3242eb(0x424)][_0x3242eb(0x50d)][_0x3242eb(0x248)]['NoTileShadows']||![];if($dataMap&&$dataMap[_0x3242eb(0x762)]){if($dataMap[_0x3242eb(0x762)][_0x3242eb(0x4c0)](/<SHOW TILE SHADOWS>/i))this[_0x3242eb(0x5b8)]=![];if($dataMap[_0x3242eb(0x762)]['match'](/<HIDE TILE SHADOWS>/i))this[_0x3242eb(0x5b8)]=!![];}},Game_Map[_0x32f06e(0x76f)]['areTileShadowsHidden']=function(){const _0x3b9bd3=_0x32f06e;if(this[_0x3b9bd3(0x5b8)]===undefined)this[_0x3b9bd3(0x497)]();return this[_0x3b9bd3(0x5b8)];},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x564)]=Game_Character[_0x32f06e(0x76f)][_0x32f06e(0x28e)],Game_Character[_0x32f06e(0x76f)][_0x32f06e(0x28e)]=function(_0x37943f){const _0x547931=_0x32f06e;try{VisuMZ['CoreEngine'][_0x547931(0x564)]['call'](this,_0x37943f);}catch(_0x1a1af6){if($gameTemp[_0x547931(0x3de)]())console['log'](_0x1a1af6);}},Game_Player[_0x32f06e(0x76f)][_0x32f06e(0x1fc)]=function(){const _0x468d71=_0x32f06e,_0x168099=$gameMap[_0x468d71(0x667)]();this[_0x468d71(0x72d)]=Math['randomInt'](_0x168099)+Math[_0x468d71(0x607)](_0x168099)+this['encounterStepsMinimum']();},Game_Player[_0x32f06e(0x76f)][_0x32f06e(0x78e)]=function(){const _0x4b4ce3=_0x32f06e;return $dataMap&&$dataMap['note']&&$dataMap[_0x4b4ce3(0x762)]['match'](/<MINIMUM ENCOUNTER STEPS:[ ](\d+)>/i)?Number(RegExp['$1']):VisuMZ['CoreEngine']['Settings']['QoL'][_0x4b4ce3(0x6be)];},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x279)]=Game_Event[_0x32f06e(0x76f)][_0x32f06e(0x679)],Game_Event[_0x32f06e(0x76f)][_0x32f06e(0x679)]=function(_0x2d8c56,_0x370f38){const _0x4b6372=_0x32f06e;return this['isSmartEventCollisionOn']()?this['checkSmartEventCollision'](_0x2d8c56,_0x370f38):VisuMZ[_0x4b6372(0x424)][_0x4b6372(0x279)][_0x4b6372(0x31a)](this,_0x2d8c56,_0x370f38);},Game_Event['prototype'][_0x32f06e(0x3a2)]=function(){const _0x1a6fea=_0x32f06e;return VisuMZ[_0x1a6fea(0x424)]['Settings']['QoL'][_0x1a6fea(0x789)];},Game_Event[_0x32f06e(0x76f)][_0x32f06e(0x663)]=function(_0x58e4e7,_0x477abf){const _0x1552db=_0x32f06e;if(!this[_0x1552db(0x38f)]())return![];else{const _0x5e623b=$gameMap[_0x1552db(0x428)](_0x58e4e7,_0x477abf)[_0x1552db(0x3f6)](_0x533710=>_0x533710[_0x1552db(0x38f)]());return _0x5e623b['length']>0x0;}},VisuMZ['CoreEngine'][_0x32f06e(0x4c8)]=Game_Interpreter[_0x32f06e(0x76f)][_0x32f06e(0x404)],Game_Interpreter[_0x32f06e(0x76f)][_0x32f06e(0x404)]=function(_0x5adb27){const _0x2473e1=_0x32f06e,_0x37f6c7=this[_0x2473e1(0x34f)]();return _0x37f6c7[_0x2473e1(0x4c0)](/\/\/[ ]SCRIPT[ ]CALL/i)?this[_0x2473e1(0x631)](_0x37f6c7):VisuMZ[_0x2473e1(0x424)][_0x2473e1(0x4c8)][_0x2473e1(0x31a)](this,_0x5adb27);},Game_Interpreter[_0x32f06e(0x76f)][_0x32f06e(0x34f)]=function(){const _0x35d26e=_0x32f06e;let _0x1a08cd='',_0x136538=this[_0x35d26e(0x384)]+0x1;while(this['_list'][_0x136538]&&this[_0x35d26e(0x2f8)][_0x136538][_0x35d26e(0x4ca)]===0x195){_0x1a08cd+=this[_0x35d26e(0x2f8)][_0x136538]['parameters'][0x0]+'\x0a',_0x136538++;}return _0x1a08cd;},Game_Interpreter[_0x32f06e(0x76f)][_0x32f06e(0x631)]=function(_0x247259){const _0x30594b=_0x32f06e;try{eval(_0x247259);}catch(_0x15488a){$gameTemp[_0x30594b(0x3de)]()&&(console[_0x30594b(0x5c0)](_0x30594b(0x3f0)),console[_0x30594b(0x5c0)](_0x15488a));}return!![];},VisuMZ['CoreEngine'][_0x32f06e(0x22d)]=Game_Interpreter[_0x32f06e(0x76f)][_0x32f06e(0x5ee)],Game_Interpreter[_0x32f06e(0x76f)][_0x32f06e(0x5ee)]=function(_0x1ed5da){const _0x57dd0b=_0x32f06e;try{VisuMZ[_0x57dd0b(0x424)][_0x57dd0b(0x22d)][_0x57dd0b(0x31a)](this,_0x1ed5da);}catch(_0x4fb90a){$gameTemp[_0x57dd0b(0x3de)]()&&(console[_0x57dd0b(0x5c0)]('Conditional\x20Branch\x20Script\x20Error'),console[_0x57dd0b(0x5c0)](_0x4fb90a)),this[_0x57dd0b(0x43b)]();}return!![];},VisuMZ['CoreEngine'][_0x32f06e(0x3dd)]=Game_Interpreter[_0x32f06e(0x76f)][_0x32f06e(0x3e2)],Game_Interpreter[_0x32f06e(0x76f)][_0x32f06e(0x3e2)]=function(_0x2d384c){const _0x862766=_0x32f06e;try{VisuMZ[_0x862766(0x424)][_0x862766(0x3dd)]['call'](this,_0x2d384c);}catch(_0x273afa){$gameTemp[_0x862766(0x3de)]()&&(console[_0x862766(0x5c0)](_0x862766(0x45e)),console[_0x862766(0x5c0)](_0x273afa));}return!![];},VisuMZ['CoreEngine'][_0x32f06e(0x42a)]=Game_Interpreter['prototype']['command355'],Game_Interpreter[_0x32f06e(0x76f)][_0x32f06e(0x6f9)]=function(){const _0x1b236c=_0x32f06e;try{VisuMZ['CoreEngine'][_0x1b236c(0x42a)][_0x1b236c(0x31a)](this);}catch(_0x4c42dc){$gameTemp[_0x1b236c(0x3de)]()&&(console['log'](_0x1b236c(0x302)),console[_0x1b236c(0x5c0)](_0x4c42dc));}return!![];},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x79a)]=Game_Interpreter[_0x32f06e(0x76f)][_0x32f06e(0x2ac)],Game_Interpreter['prototype'][_0x32f06e(0x2ac)]=function(_0x48c5df){const _0x4d672d=_0x32f06e;return $gameTemp[_0x4d672d(0x76e)](this),VisuMZ[_0x4d672d(0x424)][_0x4d672d(0x79a)][_0x4d672d(0x31a)](this,_0x48c5df);},Scene_Base['prototype'][_0x32f06e(0x351)]=function(){const _0x2b472a=_0x32f06e;return VisuMZ[_0x2b472a(0x424)][_0x2b472a(0x50d)]['UI'][_0x2b472a(0x677)];},Scene_Base['prototype'][_0x32f06e(0x3ab)]=function(){const _0x1fac72=_0x32f06e;return VisuMZ[_0x1fac72(0x424)][_0x1fac72(0x50d)]['UI'][_0x1fac72(0x372)];},Scene_Base[_0x32f06e(0x76f)]['isBottomButtonMode']=function(){const _0x1fc8a6=_0x32f06e;return VisuMZ['CoreEngine'][_0x1fc8a6(0x50d)]['UI'][_0x1fc8a6(0x51f)];},Scene_Base[_0x32f06e(0x76f)][_0x32f06e(0x293)]=function(){const _0x18aea6=_0x32f06e;return VisuMZ[_0x18aea6(0x424)][_0x18aea6(0x50d)]['UI']['RightMenus'];},Scene_Base[_0x32f06e(0x76f)][_0x32f06e(0x6e9)]=function(){const _0xafe46f=_0x32f06e;return VisuMZ[_0xafe46f(0x424)]['Settings']['UI']['CommandWidth'];},Scene_Base[_0x32f06e(0x76f)][_0x32f06e(0x46e)]=function(){const _0x59d9de=_0x32f06e;return VisuMZ[_0x59d9de(0x424)]['Settings']['UI']['ButtonHeight'];},Scene_Base[_0x32f06e(0x76f)][_0x32f06e(0x43e)]=function(){const _0x5846b8=_0x32f06e;return VisuMZ['CoreEngine'][_0x5846b8(0x50d)][_0x5846b8(0x298)][_0x5846b8(0x296)];},VisuMZ['CoreEngine'][_0x32f06e(0x2a7)]=Scene_Base[_0x32f06e(0x76f)][_0x32f06e(0x6a1)],Scene_Base[_0x32f06e(0x76f)][_0x32f06e(0x6a1)]=function(){const _0x15852d=_0x32f06e;VisuMZ[_0x15852d(0x424)][_0x15852d(0x2a7)][_0x15852d(0x31a)](this),this[_0x15852d(0x473)](),this[_0x15852d(0x6c5)]['x']=Math[_0x15852d(0x237)](this[_0x15852d(0x6c5)]['x']),this[_0x15852d(0x6c5)]['y']=Math[_0x15852d(0x237)](this[_0x15852d(0x6c5)]['y']);},Scene_Base[_0x32f06e(0x76f)][_0x32f06e(0x473)]=function(){},Scene_Base[_0x32f06e(0x76f)][_0x32f06e(0x22f)]=function(){const _0x16e154=_0x32f06e;return TextManager['getInputMultiButtonStrings'](_0x16e154(0x52b),'pagedown');},Scene_Base[_0x32f06e(0x76f)][_0x32f06e(0x2ae)]=function(){const _0x247752=_0x32f06e;return TextManager['getInputButtonString'](_0x247752(0x2c5));},Scene_Base[_0x32f06e(0x76f)][_0x32f06e(0x3f7)]=function(){return TextManager['getInputButtonString']('shift');},Scene_Base['prototype'][_0x32f06e(0x713)]=function(){const _0x4dbaf5=_0x32f06e;return TextManager[_0x4dbaf5(0x49c)]('ok');},Scene_Base[_0x32f06e(0x76f)][_0x32f06e(0x25d)]=function(){const _0x496421=_0x32f06e;return TextManager[_0x496421(0x49c)](_0x496421(0x4cd));},Scene_Base[_0x32f06e(0x76f)][_0x32f06e(0x684)]=function(){const _0x3eee76=_0x32f06e;return this[_0x3eee76(0x2b6)]&&this[_0x3eee76(0x2b6)][_0x3eee76(0x435)]?TextManager[_0x3eee76(0x215)]:'';},Scene_Base[_0x32f06e(0x76f)][_0x32f06e(0x6a3)]=function(){return'';},Scene_Base[_0x32f06e(0x76f)][_0x32f06e(0x34a)]=function(){return'';},Scene_Base[_0x32f06e(0x76f)]['buttonAssistText4']=function(){const _0x29490c=_0x32f06e;return TextManager[_0x29490c(0x3b0)];},Scene_Base['prototype'][_0x32f06e(0x70a)]=function(){const _0x424254=_0x32f06e;return TextManager[_0x424254(0x1e6)];},Scene_Base[_0x32f06e(0x76f)][_0x32f06e(0x3d0)]=function(){return 0x0;},Scene_Base[_0x32f06e(0x76f)][_0x32f06e(0x4a9)]=function(){return 0x0;},Scene_Base[_0x32f06e(0x76f)][_0x32f06e(0x4d5)]=function(){return 0x0;},Scene_Base['prototype']['buttonAssistOffset4']=function(){return 0x0;},Scene_Base[_0x32f06e(0x76f)]['buttonAssistOffset5']=function(){return 0x0;},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x212)]=Scene_Boot[_0x32f06e(0x76f)][_0x32f06e(0x217)],Scene_Boot[_0x32f06e(0x76f)][_0x32f06e(0x217)]=function(){const _0x249837=_0x32f06e;VisuMZ[_0x249837(0x424)][_0x249837(0x212)][_0x249837(0x31a)](this),this[_0x249837(0x1fe)]();},Scene_Boot[_0x32f06e(0x76f)][_0x32f06e(0x1fe)]=function(){const _0x42d649=_0x32f06e,_0x501a16=[_0x42d649(0x1d5),'battlebacks1',_0x42d649(0x4f2),_0x42d649(0x362),'enemies',_0x42d649(0x2d3),'parallaxes',_0x42d649(0x55f),'sv_actors','sv_enemies',_0x42d649(0x31d),'tilesets',_0x42d649(0x466),_0x42d649(0x797)];for(const _0xca5fd9 of _0x501a16){const _0x5a0075=VisuMZ[_0x42d649(0x424)][_0x42d649(0x50d)][_0x42d649(0x30e)][_0xca5fd9],_0x4198c3=_0x42d649(0x47c)['format'](_0xca5fd9);for(const _0x5ae458 of _0x5a0075){ImageManager[_0x42d649(0x461)](_0x4198c3,_0x5ae458);}}},VisuMZ['CoreEngine'][_0x32f06e(0x739)]=Scene_Boot[_0x32f06e(0x76f)][_0x32f06e(0x256)],Scene_Boot[_0x32f06e(0x76f)][_0x32f06e(0x256)]=function(){const _0x149272=_0x32f06e;Utils[_0x149272(0x2d5)](_0x149272(0x515))&&VisuMZ[_0x149272(0x424)]['Settings']['QoL'][_0x149272(0x601)]?this[_0x149272(0x44f)]():VisuMZ['CoreEngine'][_0x149272(0x739)][_0x149272(0x31a)](this);},Scene_Boot[_0x32f06e(0x76f)][_0x32f06e(0x44f)]=function(){DataManager['setupNewGame'](),SceneManager['goto'](Scene_Map);},Scene_Boot[_0x32f06e(0x76f)][_0x32f06e(0x47a)]=function(){const _0x3cbba2=_0x32f06e,_0x50af95=$dataSystem['advanced'][_0x3cbba2(0x2cf)],_0x3c4798=$dataSystem['advanced']['uiAreaHeight'],_0x1b34d8=VisuMZ['CoreEngine']['Settings']['UI'][_0x3cbba2(0x357)];Graphics[_0x3cbba2(0x254)]=_0x50af95-_0x1b34d8*0x2,Graphics[_0x3cbba2(0x707)]=_0x3c4798-_0x1b34d8*0x2,this[_0x3cbba2(0x6b7)]();},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x6ef)]=Scene_Boot[_0x32f06e(0x76f)]['updateDocumentTitle'],Scene_Boot['prototype'][_0x32f06e(0x4c7)]=function(){const _0x4807cc=_0x32f06e;this[_0x4807cc(0x73a)]()?this[_0x4807cc(0x69d)]():VisuMZ[_0x4807cc(0x424)][_0x4807cc(0x6ef)][_0x4807cc(0x31a)](this);},Scene_Boot[_0x32f06e(0x76f)][_0x32f06e(0x73a)]=function(){const _0x31af85=_0x32f06e;if(Scene_Title[_0x31af85(0x36a)]==='')return![];if(Scene_Title[_0x31af85(0x36a)]===_0x31af85(0x242))return![];if(Scene_Title[_0x31af85(0x6d1)]==='')return![];if(Scene_Title[_0x31af85(0x6d1)]===_0x31af85(0x615))return![];return!![];},Scene_Boot[_0x32f06e(0x76f)]['makeDocumentTitle']=function(){const _0x16d7ca=_0x32f06e,_0x35a8a3=$dataSystem[_0x16d7ca(0x391)],_0x450e53=Scene_Title[_0x16d7ca(0x36a)]||'',_0x4fb498=Scene_Title[_0x16d7ca(0x6d1)]||'',_0x49782d=VisuMZ[_0x16d7ca(0x424)][_0x16d7ca(0x50d)][_0x16d7ca(0x59b)][_0x16d7ca(0x6f6)][_0x16d7ca(0x605)],_0x3de75c=_0x49782d['format'](_0x35a8a3,_0x450e53,_0x4fb498);document[_0x16d7ca(0x627)]=_0x3de75c;},Scene_Boot[_0x32f06e(0x76f)][_0x32f06e(0x6b7)]=function(){const _0x5ee0e3=_0x32f06e;if(VisuMZ[_0x5ee0e3(0x424)][_0x5ee0e3(0x50d)]['UI'][_0x5ee0e3(0x57f)]){const _0x13674b=Graphics['width']-Graphics['boxWidth']-VisuMZ[_0x5ee0e3(0x424)][_0x5ee0e3(0x50d)]['UI'][_0x5ee0e3(0x357)]*0x2,_0x30c166=Sprite_Button[_0x5ee0e3(0x76f)][_0x5ee0e3(0x65a)][_0x5ee0e3(0x31a)](this)*0x4;if(_0x13674b>=_0x30c166)SceneManager[_0x5ee0e3(0x4e2)](!![]);}},Scene_Title[_0x32f06e(0x36a)]=VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x50d)][_0x32f06e(0x59b)][_0x32f06e(0x6f6)][_0x32f06e(0x242)],Scene_Title['version']=VisuMZ[_0x32f06e(0x424)]['Settings'][_0x32f06e(0x59b)][_0x32f06e(0x6f6)][_0x32f06e(0x688)],Scene_Title[_0x32f06e(0x6f0)]=VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x50d)][_0x32f06e(0x455)],VisuMZ[_0x32f06e(0x424)]['Scene_Title_drawGameTitle']=Scene_Title[_0x32f06e(0x76f)][_0x32f06e(0x6b3)],Scene_Title[_0x32f06e(0x76f)]['drawGameTitle']=function(){const _0xf3fe49=_0x32f06e;VisuMZ[_0xf3fe49(0x424)]['Settings'][_0xf3fe49(0x59b)][_0xf3fe49(0x6f6)]['drawGameTitle'][_0xf3fe49(0x31a)](this);if(Scene_Title[_0xf3fe49(0x36a)]!==''&&Scene_Title[_0xf3fe49(0x36a)]!=='Subtitle')this['drawGameSubtitle']();if(Scene_Title['version']!==''&&Scene_Title[_0xf3fe49(0x6d1)]!==_0xf3fe49(0x615))this['drawGameVersion']();},Scene_Title[_0x32f06e(0x76f)]['drawGameSubtitle']=function(){const _0x5e8fed=_0x32f06e;VisuMZ[_0x5e8fed(0x424)]['Settings'][_0x5e8fed(0x59b)][_0x5e8fed(0x6f6)][_0x5e8fed(0x326)][_0x5e8fed(0x31a)](this);},Scene_Title['prototype'][_0x32f06e(0x54d)]=function(){const _0xabbd84=_0x32f06e;VisuMZ['CoreEngine'][_0xabbd84(0x50d)][_0xabbd84(0x59b)]['Title'][_0xabbd84(0x54d)][_0xabbd84(0x31a)](this);},Scene_Title[_0x32f06e(0x76f)]['createCommandWindow']=function(){const _0x638a13=_0x32f06e;this[_0x638a13(0x3ec)]();const _0x45bcc0=$dataSystem[_0x638a13(0x4e3)]['background'],_0x39e093=this['commandWindowRect']();this[_0x638a13(0x38b)]=new Window_TitleCommand(_0x39e093),this[_0x638a13(0x38b)][_0x638a13(0x5da)](_0x45bcc0);const _0x8ab6d0=this[_0x638a13(0x413)]();this[_0x638a13(0x38b)][_0x638a13(0x702)](_0x8ab6d0['x'],_0x8ab6d0['y'],_0x8ab6d0[_0x638a13(0x450)],_0x8ab6d0[_0x638a13(0x557)]),this[_0x638a13(0x426)](this[_0x638a13(0x38b)]);},Scene_Title[_0x32f06e(0x76f)][_0x32f06e(0x6e0)]=function(){const _0x18753a=_0x32f06e;return this['_commandWindow']?this[_0x18753a(0x38b)]['maxItems']():VisuMZ[_0x18753a(0x424)][_0x18753a(0x50d)][_0x18753a(0x211)][_0x18753a(0x26a)];},Scene_Title['prototype']['commandWindowRect']=function(){const _0x5e8780=_0x32f06e;return VisuMZ[_0x5e8780(0x424)]['Settings'][_0x5e8780(0x59b)][_0x5e8780(0x6f6)]['CommandRect'][_0x5e8780(0x31a)](this);},Scene_Title[_0x32f06e(0x76f)][_0x32f06e(0x3ec)]=function(){const _0x114916=_0x32f06e;for(const _0x59d5b1 of Scene_Title[_0x114916(0x6f0)]){const _0x2b0705=new Sprite_TitlePictureButton(_0x59d5b1);this[_0x114916(0x2e9)](_0x2b0705);}},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x39a)]=Scene_Map[_0x32f06e(0x76f)][_0x32f06e(0x58e)],Scene_Map[_0x32f06e(0x76f)][_0x32f06e(0x58e)]=function(){const _0x4c30dc=_0x32f06e;VisuMZ[_0x4c30dc(0x424)]['Scene_Map_initialize'][_0x4c30dc(0x31a)](this),$gameTemp[_0x4c30dc(0x2ff)]();},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x2b5)]=Scene_Map[_0x32f06e(0x76f)][_0x32f06e(0x267)],Scene_Map[_0x32f06e(0x76f)][_0x32f06e(0x267)]=function(){const _0x4ed434=_0x32f06e;VisuMZ['CoreEngine'][_0x4ed434(0x2b5)][_0x4ed434(0x31a)](this),$gameTemp[_0x4ed434(0x527)]&&!$gameMessage[_0x4ed434(0x2a5)]()&&(this['updateMain'](),SceneManager[_0x4ed434(0x56c)]());},Scene_Map['prototype'][_0x32f06e(0x761)]=function(){const _0x5d3477=_0x32f06e;Scene_Message[_0x5d3477(0x76f)][_0x5d3477(0x761)][_0x5d3477(0x31a)](this),!SceneManager['isNextScene'](Scene_Battle)&&(this[_0x5d3477(0x429)][_0x5d3477(0x209)](),this[_0x5d3477(0x4e4)][_0x5d3477(0x6bd)](),this[_0x5d3477(0x6c5)][_0x5d3477(0x435)]=![],SceneManager[_0x5d3477(0x774)]()),$gameScreen['clearZoom']();},VisuMZ['CoreEngine']['Scene_Map_createMenuButton']=Scene_Map[_0x32f06e(0x76f)][_0x32f06e(0x541)],Scene_Map[_0x32f06e(0x76f)][_0x32f06e(0x541)]=function(){const _0x4d3c78=_0x32f06e;VisuMZ['CoreEngine']['Scene_Map_createMenuButton'][_0x4d3c78(0x31a)](this),SceneManager[_0x4d3c78(0x2a9)]()&&this[_0x4d3c78(0x24c)]();},Scene_Map[_0x32f06e(0x76f)][_0x32f06e(0x24c)]=function(){const _0x39de10=_0x32f06e;this[_0x39de10(0x313)]['x']=Graphics[_0x39de10(0x254)]+0x4;},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x5bf)]=Scene_Map['prototype'][_0x32f06e(0x48c)],Scene_Map[_0x32f06e(0x76f)]['updateScene']=function(){const _0x29adbf=_0x32f06e;VisuMZ['CoreEngine'][_0x29adbf(0x5bf)][_0x29adbf(0x31a)](this),this['updateDashToggle']();},Scene_Map[_0x32f06e(0x76f)][_0x32f06e(0x3ca)]=function(){const _0x1dad34=_0x32f06e;Input[_0x1dad34(0x233)](_0x1dad34(0x708))&&(ConfigManager['alwaysDash']=!ConfigManager[_0x1dad34(0x263)],ConfigManager[_0x1dad34(0x2b0)]());},VisuMZ['CoreEngine'][_0x32f06e(0x598)]=Scene_MenuBase['prototype'][_0x32f06e(0x599)],Scene_MenuBase[_0x32f06e(0x76f)]['helpAreaTop']=function(){const _0x30c492=_0x32f06e;let _0x343849=0x0;return SceneManager[_0x30c492(0x6fa)]()?_0x343849=this['helpAreaTopSideButtonLayout']():_0x343849=VisuMZ[_0x30c492(0x424)][_0x30c492(0x598)][_0x30c492(0x31a)](this),this[_0x30c492(0x6ae)]()&&this[_0x30c492(0x76a)]()==='top'&&(_0x343849+=Window_ButtonAssist[_0x30c492(0x76f)][_0x30c492(0x682)]()),_0x343849;},Scene_MenuBase[_0x32f06e(0x76f)]['helpAreaTopSideButtonLayout']=function(){const _0x4e9690=_0x32f06e;return this['isBottomHelpMode']()?this[_0x4e9690(0x65c)]():0x0;},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x734)]=Scene_MenuBase[_0x32f06e(0x76f)][_0x32f06e(0x6b2)],Scene_MenuBase[_0x32f06e(0x76f)][_0x32f06e(0x6b2)]=function(){const _0x401899=_0x32f06e;return SceneManager[_0x401899(0x6fa)]()?this[_0x401899(0x462)]():VisuMZ[_0x401899(0x424)][_0x401899(0x734)]['call'](this);},Scene_MenuBase[_0x32f06e(0x76f)]['mainAreaTopSideButtonLayout']=function(){const _0x134326=_0x32f06e;return!this[_0x134326(0x3ab)]()?this[_0x134326(0x74c)]():0x0;},VisuMZ['CoreEngine']['Scene_MenuBase_mainAreaHeight']=Scene_MenuBase[_0x32f06e(0x76f)][_0x32f06e(0x1d9)],Scene_MenuBase[_0x32f06e(0x76f)]['mainAreaHeight']=function(){const _0x563742=_0x32f06e;let _0x56a9e2=0x0;return SceneManager[_0x563742(0x6fa)]()?_0x56a9e2=this[_0x563742(0x577)]():_0x56a9e2=VisuMZ[_0x563742(0x424)][_0x563742(0x5b3)]['call'](this),this[_0x563742(0x6ae)]()&&this[_0x563742(0x76a)]()!=='button'&&(_0x56a9e2-=Window_ButtonAssist[_0x563742(0x76f)][_0x563742(0x682)]()),_0x56a9e2;},Scene_MenuBase[_0x32f06e(0x76f)][_0x32f06e(0x577)]=function(){const _0x33e37b=_0x32f06e;return Graphics[_0x33e37b(0x707)]-this[_0x33e37b(0x508)]();},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x266)]=Scene_MenuBase[_0x32f06e(0x76f)][_0x32f06e(0x48a)],Scene_MenuBase['prototype']['createBackground']=function(){const _0x493fe0=_0x32f06e;this['_backgroundFilter']=new PIXI[(_0x493fe0(0x57d))][(_0x493fe0(0x77b))](clamp=!![]),this[_0x493fe0(0x2fc)]=new Sprite(),this['_backgroundSprite']['bitmap']=SceneManager[_0x493fe0(0x511)](),this['_backgroundSprite']['filters']=[this[_0x493fe0(0x238)]],this[_0x493fe0(0x2e9)](this[_0x493fe0(0x2fc)]),this[_0x493fe0(0x58b)](0xc0),this[_0x493fe0(0x58b)](this[_0x493fe0(0x458)]()),this['createCustomBackgroundImages']();},Scene_MenuBase[_0x32f06e(0x76f)][_0x32f06e(0x458)]=function(){const _0x34902c=_0x32f06e,_0x3c2f0e=String(this[_0x34902c(0x38e)]['name']),_0x3b0892=this[_0x34902c(0x398)](_0x3c2f0e);return _0x3b0892?_0x3b0892[_0x34902c(0x763)]:0xc0;},Scene_MenuBase[_0x32f06e(0x76f)][_0x32f06e(0x2c8)]=function(){const _0x5080bf=_0x32f06e,_0x5af970=String(this[_0x5080bf(0x38e)][_0x5080bf(0x6b9)]),_0x520246=this[_0x5080bf(0x398)](_0x5af970);_0x520246&&(_0x520246[_0x5080bf(0x645)]!==''||_0x520246[_0x5080bf(0x680)]!=='')&&(this[_0x5080bf(0x29b)]=new Sprite(ImageManager[_0x5080bf(0x617)](_0x520246['BgFilename1'])),this['_backSprite2']=new Sprite(ImageManager[_0x5080bf(0x746)](_0x520246[_0x5080bf(0x680)])),this['addChild'](this[_0x5080bf(0x29b)]),this['addChild'](this[_0x5080bf(0x393)]),this[_0x5080bf(0x29b)][_0x5080bf(0x1f7)][_0x5080bf(0x220)](this['adjustSprite']['bind'](this,this[_0x5080bf(0x29b)])),this[_0x5080bf(0x393)][_0x5080bf(0x1f7)][_0x5080bf(0x220)](this['adjustSprite'][_0x5080bf(0x3da)](this,this[_0x5080bf(0x393)])));},Scene_MenuBase[_0x32f06e(0x76f)][_0x32f06e(0x398)]=function(_0x5e6abd){const _0x1503f6=_0x32f06e;return VisuMZ[_0x1503f6(0x424)]['Settings'][_0x1503f6(0x51a)][_0x5e6abd]||VisuMZ[_0x1503f6(0x424)][_0x1503f6(0x50d)][_0x1503f6(0x51a)][_0x1503f6(0x1f6)];},Scene_MenuBase[_0x32f06e(0x76f)][_0x32f06e(0x322)]=function(_0x2fec54){const _0x2f346e=_0x32f06e;this[_0x2f346e(0x5b6)](_0x2fec54),this[_0x2f346e(0x2b4)](_0x2fec54);},VisuMZ['CoreEngine'][_0x32f06e(0x2f4)]=Scene_MenuBase[_0x32f06e(0x76f)][_0x32f06e(0x283)],Scene_MenuBase['prototype'][_0x32f06e(0x283)]=function(){const _0x4c0d76=_0x32f06e;VisuMZ[_0x4c0d76(0x424)]['Scene_MenuBase_createCancelButton'][_0x4c0d76(0x31a)](this),SceneManager[_0x4c0d76(0x2a9)]()&&this[_0x4c0d76(0x325)]();},Scene_MenuBase[_0x32f06e(0x76f)][_0x32f06e(0x325)]=function(){const _0x10a794=_0x32f06e;this[_0x10a794(0x2d2)]['x']=Graphics[_0x10a794(0x254)]+0x4;},VisuMZ[_0x32f06e(0x424)]['Scene_MenuBase_createPageButtons']=Scene_MenuBase[_0x32f06e(0x76f)][_0x32f06e(0x785)],Scene_MenuBase[_0x32f06e(0x76f)]['createPageButtons']=function(){const _0x3ef476=_0x32f06e;VisuMZ['CoreEngine'][_0x3ef476(0x359)]['call'](this),SceneManager[_0x3ef476(0x2a9)]()&&this['movePageButtonSideButtonLayout']();},Scene_MenuBase['prototype'][_0x32f06e(0x432)]=function(){const _0x267f36=_0x32f06e;this[_0x267f36(0x2b6)]['x']=-0x1*(this[_0x267f36(0x2b6)][_0x267f36(0x450)]+this[_0x267f36(0x5aa)][_0x267f36(0x450)]+0x8),this[_0x267f36(0x5aa)]['x']=-0x1*(this['_pagedownButton'][_0x267f36(0x450)]+0x4);},Scene_MenuBase[_0x32f06e(0x76f)][_0x32f06e(0x6ae)]=function(){const _0x1d1cb1=_0x32f06e;return VisuMZ['CoreEngine'][_0x1d1cb1(0x50d)][_0x1d1cb1(0x673)]['Enable'];},Scene_MenuBase['prototype']['getButtonAssistLocation']=function(){const _0x3fa6bc=_0x32f06e;return SceneManager[_0x3fa6bc(0x2a9)]()||SceneManager['areButtonsHidden']()?VisuMZ[_0x3fa6bc(0x424)][_0x3fa6bc(0x50d)][_0x3fa6bc(0x673)][_0x3fa6bc(0x208)]:'button';},Scene_MenuBase['prototype']['createButtonAssistWindow']=function(){const _0x43232d=_0x32f06e;if(!this[_0x43232d(0x6ae)]())return;const _0x3364ad=this[_0x43232d(0x25c)]();this[_0x43232d(0x6ed)]=new Window_ButtonAssist(_0x3364ad),this['addWindow'](this['_buttonAssistWindow']);},Scene_MenuBase[_0x32f06e(0x76f)][_0x32f06e(0x25c)]=function(){const _0x56c505=_0x32f06e;return this[_0x56c505(0x76a)]()===_0x56c505(0x3ff)?this['buttonAssistWindowButtonRect']():this[_0x56c505(0x400)]();},Scene_MenuBase[_0x32f06e(0x76f)][_0x32f06e(0x56d)]=function(){const _0x30e65e=_0x32f06e,_0x559169=ConfigManager[_0x30e65e(0x5a9)]?(Sprite_Button[_0x30e65e(0x76f)][_0x30e65e(0x65a)]()+0x6)*0x2:0x0,_0x12e974=this[_0x30e65e(0x37d)](),_0x467600=Graphics[_0x30e65e(0x254)]-_0x559169*0x2,_0xb22d83=this[_0x30e65e(0x46e)]();return new Rectangle(_0x559169,_0x12e974,_0x467600,_0xb22d83);},Scene_MenuBase[_0x32f06e(0x76f)][_0x32f06e(0x400)]=function(){const _0x3f99ec=_0x32f06e,_0x3a87bb=Graphics[_0x3f99ec(0x254)],_0x29db3e=Window_ButtonAssist[_0x3f99ec(0x76f)]['lineHeight'](),_0x101b5f=0x0;let _0xb74384=0x0;return this['getButtonAssistLocation']()===_0x3f99ec(0x6c6)?_0xb74384=0x0:_0xb74384=Graphics[_0x3f99ec(0x707)]-_0x29db3e,new Rectangle(_0x101b5f,_0xb74384,_0x3a87bb,_0x29db3e);},Scene_Menu[_0x32f06e(0x288)]=VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x50d)][_0x32f06e(0x59b)][_0x32f06e(0x56a)],VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x63d)]=Scene_Menu[_0x32f06e(0x76f)][_0x32f06e(0x33a)],Scene_Menu[_0x32f06e(0x76f)][_0x32f06e(0x33a)]=function(){const _0x27673b=_0x32f06e;VisuMZ[_0x27673b(0x424)][_0x27673b(0x63d)][_0x27673b(0x31a)](this),this['setCoreEngineUpdateWindowBg']();},Scene_Menu[_0x32f06e(0x76f)]['setCoreEngineUpdateWindowBg']=function(){const _0x5e7307=_0x32f06e;this['_commandWindow']&&this[_0x5e7307(0x38b)][_0x5e7307(0x5da)](Scene_Menu[_0x5e7307(0x288)]['CommandBgType']),this[_0x5e7307(0x61f)]&&this[_0x5e7307(0x61f)]['setBackgroundType'](Scene_Menu[_0x5e7307(0x288)][_0x5e7307(0x2e1)]),this[_0x5e7307(0x6dd)]&&this[_0x5e7307(0x6dd)][_0x5e7307(0x5da)](Scene_Menu[_0x5e7307(0x288)][_0x5e7307(0x4f8)]);},Scene_Menu[_0x32f06e(0x76f)][_0x32f06e(0x413)]=function(){const _0x277a22=_0x32f06e;return Scene_Menu['layoutSettings'][_0x277a22(0x59c)][_0x277a22(0x31a)](this);},Scene_Menu['prototype'][_0x32f06e(0x742)]=function(){const _0x317066=_0x32f06e;return Scene_Menu[_0x317066(0x288)][_0x317066(0x71b)]['call'](this);},Scene_Menu[_0x32f06e(0x76f)][_0x32f06e(0x70d)]=function(){const _0x27a3d7=_0x32f06e;return Scene_Menu[_0x27a3d7(0x288)][_0x27a3d7(0x282)][_0x27a3d7(0x31a)](this);},Scene_Item[_0x32f06e(0x288)]=VisuMZ['CoreEngine']['Settings'][_0x32f06e(0x59b)][_0x32f06e(0x698)],VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x74e)]=Scene_Item[_0x32f06e(0x76f)][_0x32f06e(0x33a)],Scene_Item['prototype'][_0x32f06e(0x33a)]=function(){const _0x24fc65=_0x32f06e;VisuMZ['CoreEngine'][_0x24fc65(0x74e)][_0x24fc65(0x31a)](this),this['setCoreEngineUpdateWindowBg']();},Scene_Item['prototype']['setCoreEngineUpdateWindowBg']=function(){const _0x5500f1=_0x32f06e;this[_0x5500f1(0x574)]&&this[_0x5500f1(0x574)]['setBackgroundType'](Scene_Item[_0x5500f1(0x288)]['HelpBgType']),this[_0x5500f1(0x475)]&&this['_categoryWindow'][_0x5500f1(0x5da)](Scene_Item['layoutSettings'][_0x5500f1(0x669)]),this['_itemWindow']&&this[_0x5500f1(0x3dc)][_0x5500f1(0x5da)](Scene_Item['layoutSettings'][_0x5500f1(0x71c)]),this[_0x5500f1(0x327)]&&this[_0x5500f1(0x327)][_0x5500f1(0x5da)](Scene_Item['layoutSettings'][_0x5500f1(0x6f2)]);},Scene_Item[_0x32f06e(0x76f)]['helpWindowRect']=function(){const _0x18bd1a=_0x32f06e;return Scene_Item[_0x18bd1a(0x288)][_0x18bd1a(0x1e3)]['call'](this);},Scene_Item[_0x32f06e(0x76f)][_0x32f06e(0x3df)]=function(){const _0x1f5729=_0x32f06e;return Scene_Item[_0x1f5729(0x288)][_0x1f5729(0x6f1)][_0x1f5729(0x31a)](this);},Scene_Item['prototype']['itemWindowRect']=function(){const _0x3d93e3=_0x32f06e;return Scene_Item[_0x3d93e3(0x288)][_0x3d93e3(0x1d4)][_0x3d93e3(0x31a)](this);},Scene_Item['prototype'][_0x32f06e(0x1df)]=function(){const _0x4f89ea=_0x32f06e;return Scene_Item['layoutSettings'][_0x4f89ea(0x2ef)][_0x4f89ea(0x31a)](this);},Scene_Skill[_0x32f06e(0x288)]=VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x50d)]['MenuLayout'][_0x32f06e(0x40e)],VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x5be)]=Scene_Skill[_0x32f06e(0x76f)][_0x32f06e(0x33a)],Scene_Skill[_0x32f06e(0x76f)]['create']=function(){const _0x5e0a8c=_0x32f06e;VisuMZ[_0x5e0a8c(0x424)]['Scene_Skill_create']['call'](this),this[_0x5e0a8c(0x318)]();},Scene_Skill[_0x32f06e(0x76f)][_0x32f06e(0x318)]=function(){const _0x58e536=_0x32f06e;this['_helpWindow']&&this[_0x58e536(0x574)][_0x58e536(0x5da)](Scene_Skill['layoutSettings'][_0x58e536(0x341)]),this[_0x58e536(0x61c)]&&this[_0x58e536(0x61c)][_0x58e536(0x5da)](Scene_Skill[_0x58e536(0x288)]['SkillTypeBgType']),this[_0x58e536(0x6dd)]&&this[_0x58e536(0x6dd)][_0x58e536(0x5da)](Scene_Skill['layoutSettings'][_0x58e536(0x4f8)]),this[_0x58e536(0x3dc)]&&this[_0x58e536(0x3dc)][_0x58e536(0x5da)](Scene_Skill[_0x58e536(0x288)][_0x58e536(0x71c)]),this[_0x58e536(0x327)]&&this[_0x58e536(0x327)]['setBackgroundType'](Scene_Skill[_0x58e536(0x288)][_0x58e536(0x6f2)]);},Scene_Skill['prototype']['helpWindowRect']=function(){const _0x58d65e=_0x32f06e;return Scene_Skill['layoutSettings'][_0x58d65e(0x1e3)][_0x58d65e(0x31a)](this);},Scene_Skill[_0x32f06e(0x76f)]['skillTypeWindowRect']=function(){const _0x554119=_0x32f06e;return Scene_Skill[_0x554119(0x288)][_0x554119(0x69c)][_0x554119(0x31a)](this);},Scene_Skill[_0x32f06e(0x76f)]['statusWindowRect']=function(){const _0x4c87e8=_0x32f06e;return Scene_Skill[_0x4c87e8(0x288)][_0x4c87e8(0x282)][_0x4c87e8(0x31a)](this);},Scene_Skill[_0x32f06e(0x76f)][_0x32f06e(0x4eb)]=function(){const _0x149411=_0x32f06e;return Scene_Skill[_0x149411(0x288)][_0x149411(0x1d4)][_0x149411(0x31a)](this);},Scene_Skill[_0x32f06e(0x76f)][_0x32f06e(0x1df)]=function(){const _0x4906be=_0x32f06e;return Scene_Skill[_0x4906be(0x288)][_0x4906be(0x2ef)][_0x4906be(0x31a)](this);},Scene_Equip[_0x32f06e(0x288)]=VisuMZ[_0x32f06e(0x424)]['Settings'][_0x32f06e(0x59b)][_0x32f06e(0x30b)],VisuMZ['CoreEngine'][_0x32f06e(0x643)]=Scene_Equip[_0x32f06e(0x76f)][_0x32f06e(0x33a)],Scene_Equip['prototype']['create']=function(){const _0x3aa135=_0x32f06e;VisuMZ[_0x3aa135(0x424)][_0x3aa135(0x643)]['call'](this),this['setCoreEngineUpdateWindowBg']();},Scene_Equip['prototype'][_0x32f06e(0x318)]=function(){const _0x4d5da4=_0x32f06e;this[_0x4d5da4(0x574)]&&this[_0x4d5da4(0x574)][_0x4d5da4(0x5da)](Scene_Equip[_0x4d5da4(0x288)][_0x4d5da4(0x341)]),this['_statusWindow']&&this[_0x4d5da4(0x6dd)][_0x4d5da4(0x5da)](Scene_Equip[_0x4d5da4(0x288)][_0x4d5da4(0x4f8)]),this['_commandWindow']&&this['_commandWindow'][_0x4d5da4(0x5da)](Scene_Equip[_0x4d5da4(0x288)][_0x4d5da4(0x650)]),this[_0x4d5da4(0x47e)]&&this[_0x4d5da4(0x47e)][_0x4d5da4(0x5da)](Scene_Equip[_0x4d5da4(0x288)][_0x4d5da4(0x502)]),this['_itemWindow']&&this[_0x4d5da4(0x3dc)]['setBackgroundType'](Scene_Equip[_0x4d5da4(0x288)][_0x4d5da4(0x71c)]);},Scene_Equip[_0x32f06e(0x76f)][_0x32f06e(0x6cd)]=function(){const _0x27a2b0=_0x32f06e;return Scene_Equip[_0x27a2b0(0x288)][_0x27a2b0(0x1e3)][_0x27a2b0(0x31a)](this);},Scene_Equip[_0x32f06e(0x76f)][_0x32f06e(0x70d)]=function(){const _0x5f4ded=_0x32f06e;return Scene_Equip[_0x5f4ded(0x288)]['StatusRect'][_0x5f4ded(0x31a)](this);},Scene_Equip[_0x32f06e(0x76f)][_0x32f06e(0x413)]=function(){const _0x47ed36=_0x32f06e;return Scene_Equip[_0x47ed36(0x288)][_0x47ed36(0x59c)][_0x47ed36(0x31a)](this);},Scene_Equip[_0x32f06e(0x76f)][_0x32f06e(0x227)]=function(){const _0x2d8e21=_0x32f06e;return Scene_Equip[_0x2d8e21(0x288)]['SlotRect'][_0x2d8e21(0x31a)](this);},Scene_Equip[_0x32f06e(0x76f)][_0x32f06e(0x4eb)]=function(){const _0x569ae7=_0x32f06e;return Scene_Equip[_0x569ae7(0x288)][_0x569ae7(0x1d4)][_0x569ae7(0x31a)](this);},Scene_Status[_0x32f06e(0x288)]=VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x50d)][_0x32f06e(0x59b)][_0x32f06e(0x550)],VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x1fa)]=Scene_Status[_0x32f06e(0x76f)][_0x32f06e(0x33a)],Scene_Status[_0x32f06e(0x76f)][_0x32f06e(0x33a)]=function(){const _0x38f9ea=_0x32f06e;VisuMZ[_0x38f9ea(0x424)][_0x38f9ea(0x1fa)][_0x38f9ea(0x31a)](this),this[_0x38f9ea(0x318)]();},Scene_Status[_0x32f06e(0x76f)][_0x32f06e(0x318)]=function(){const _0x299ebc=_0x32f06e;this[_0x299ebc(0x440)]&&this[_0x299ebc(0x440)][_0x299ebc(0x5da)](Scene_Status['layoutSettings'][_0x299ebc(0x5f8)]),this[_0x299ebc(0x6dd)]&&this['_statusWindow'][_0x299ebc(0x5da)](Scene_Status[_0x299ebc(0x288)][_0x299ebc(0x4f8)]),this[_0x299ebc(0x306)]&&this[_0x299ebc(0x306)][_0x299ebc(0x5da)](Scene_Status[_0x299ebc(0x288)][_0x299ebc(0x5a6)]),this[_0x299ebc(0x5ae)]&&this['_statusEquipWindow'][_0x299ebc(0x5da)](Scene_Status[_0x299ebc(0x288)][_0x299ebc(0x711)]);},Scene_Status[_0x32f06e(0x76f)][_0x32f06e(0x297)]=function(){const _0x1a485f=_0x32f06e;return Scene_Status[_0x1a485f(0x288)][_0x1a485f(0x778)][_0x1a485f(0x31a)](this);},Scene_Status[_0x32f06e(0x76f)][_0x32f06e(0x70d)]=function(){const _0x8d1379=_0x32f06e;return Scene_Status[_0x8d1379(0x288)]['StatusRect']['call'](this);},Scene_Status[_0x32f06e(0x76f)]['statusParamsWindowRect']=function(){const _0x3411b1=_0x32f06e;return Scene_Status[_0x3411b1(0x288)][_0x3411b1(0x716)][_0x3411b1(0x31a)](this);},Scene_Status[_0x32f06e(0x76f)][_0x32f06e(0x49b)]=function(){const _0x508c8c=_0x32f06e;return Scene_Status['layoutSettings'][_0x508c8c(0x27f)]['call'](this);},Scene_Options[_0x32f06e(0x288)]=VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x50d)][_0x32f06e(0x59b)][_0x32f06e(0x41f)],VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x443)]=Scene_Options[_0x32f06e(0x76f)]['create'],Scene_Options[_0x32f06e(0x76f)][_0x32f06e(0x33a)]=function(){const _0x4f9cd=_0x32f06e;VisuMZ[_0x4f9cd(0x424)]['Scene_Options_create'][_0x4f9cd(0x31a)](this),this[_0x4f9cd(0x318)]();},Scene_Options[_0x32f06e(0x76f)]['setCoreEngineUpdateWindowBg']=function(){const _0x1d224c=_0x32f06e;this[_0x1d224c(0x501)]&&this['_optionsWindow'][_0x1d224c(0x5da)](Scene_Options[_0x1d224c(0x288)][_0x1d224c(0x3c2)]);},Scene_Options['prototype'][_0x32f06e(0x2ed)]=function(){const _0x442d8f=_0x32f06e;return Scene_Options[_0x442d8f(0x288)]['OptionsRect'][_0x442d8f(0x31a)](this);},Scene_Save[_0x32f06e(0x288)]=VisuMZ[_0x32f06e(0x424)]['Settings'][_0x32f06e(0x59b)]['SaveMenu'],Scene_Save[_0x32f06e(0x76f)][_0x32f06e(0x33a)]=function(){const _0x3c93d1=_0x32f06e;Scene_File[_0x3c93d1(0x76f)][_0x3c93d1(0x33a)][_0x3c93d1(0x31a)](this),this[_0x3c93d1(0x318)]();},Scene_Save[_0x32f06e(0x76f)][_0x32f06e(0x318)]=function(){const _0x302761=_0x32f06e;this[_0x302761(0x574)]&&this['_helpWindow'][_0x302761(0x5da)](Scene_Save[_0x302761(0x288)]['HelpBgType']),this[_0x302761(0x289)]&&this[_0x302761(0x289)][_0x302761(0x5da)](Scene_Save[_0x302761(0x288)][_0x302761(0x2ca)]);},Scene_Save[_0x32f06e(0x76f)]['helpWindowRect']=function(){const _0x15b487=_0x32f06e;return Scene_Save[_0x15b487(0x288)][_0x15b487(0x1e3)]['call'](this);},Scene_Save[_0x32f06e(0x76f)][_0x32f06e(0x4b3)]=function(){const _0x83f13c=_0x32f06e;return Scene_Save[_0x83f13c(0x288)][_0x83f13c(0x6c7)]['call'](this);},Scene_Load[_0x32f06e(0x288)]=VisuMZ[_0x32f06e(0x424)]['Settings'][_0x32f06e(0x59b)]['LoadMenu'],Scene_Load[_0x32f06e(0x76f)][_0x32f06e(0x33a)]=function(){const _0x538663=_0x32f06e;Scene_File['prototype'][_0x538663(0x33a)][_0x538663(0x31a)](this),this[_0x538663(0x318)]();},Scene_Load[_0x32f06e(0x76f)][_0x32f06e(0x318)]=function(){const _0x41e133=_0x32f06e;this[_0x41e133(0x574)]&&this[_0x41e133(0x574)]['setBackgroundType'](Scene_Load['layoutSettings'][_0x41e133(0x341)]),this[_0x41e133(0x289)]&&this[_0x41e133(0x289)]['setBackgroundType'](Scene_Load[_0x41e133(0x288)][_0x41e133(0x2ca)]);},Scene_Load[_0x32f06e(0x76f)][_0x32f06e(0x6cd)]=function(){const _0x354aec=_0x32f06e;return Scene_Load[_0x354aec(0x288)]['HelpRect'][_0x354aec(0x31a)](this);},Scene_Load[_0x32f06e(0x76f)]['listWindowRect']=function(){const _0x32642c=_0x32f06e;return Scene_Load[_0x32642c(0x288)][_0x32642c(0x6c7)]['call'](this);},Scene_GameEnd[_0x32f06e(0x288)]=VisuMZ[_0x32f06e(0x424)]['Settings'][_0x32f06e(0x59b)][_0x32f06e(0x5ff)],VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x6e1)]=Scene_GameEnd[_0x32f06e(0x76f)]['createBackground'],Scene_GameEnd[_0x32f06e(0x76f)]['createBackground']=function(){const _0x5892d1=_0x32f06e;Scene_MenuBase[_0x5892d1(0x76f)][_0x5892d1(0x48a)][_0x5892d1(0x31a)](this);},Scene_GameEnd[_0x32f06e(0x76f)][_0x32f06e(0x358)]=function(){const _0x1b4f00=_0x32f06e,_0x17f007=this[_0x1b4f00(0x413)]();this[_0x1b4f00(0x38b)]=new Window_GameEnd(_0x17f007),this['_commandWindow']['setHandler']('cancel',this[_0x1b4f00(0x34e)][_0x1b4f00(0x3da)](this)),this['addWindow'](this[_0x1b4f00(0x38b)]),this[_0x1b4f00(0x38b)][_0x1b4f00(0x5da)](Scene_GameEnd[_0x1b4f00(0x288)][_0x1b4f00(0x650)]);},Scene_GameEnd['prototype'][_0x32f06e(0x413)]=function(){const _0x31dbdb=_0x32f06e;return Scene_GameEnd[_0x31dbdb(0x288)][_0x31dbdb(0x59c)]['call'](this);},Scene_Shop[_0x32f06e(0x288)]=VisuMZ['CoreEngine'][_0x32f06e(0x50d)]['MenuLayout'][_0x32f06e(0x736)],VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x685)]=Scene_Shop['prototype']['create'],Scene_Shop['prototype']['create']=function(){const _0x57aa4c=_0x32f06e;VisuMZ[_0x57aa4c(0x424)][_0x57aa4c(0x685)][_0x57aa4c(0x31a)](this),this[_0x57aa4c(0x318)]();},Scene_Shop[_0x32f06e(0x76f)][_0x32f06e(0x318)]=function(){const _0x5f1d68=_0x32f06e;this['_helpWindow']&&this[_0x5f1d68(0x574)][_0x5f1d68(0x5da)](Scene_Shop[_0x5f1d68(0x288)][_0x5f1d68(0x341)]),this[_0x5f1d68(0x61f)]&&this[_0x5f1d68(0x61f)][_0x5f1d68(0x5da)](Scene_Shop['layoutSettings'][_0x5f1d68(0x2e1)]),this[_0x5f1d68(0x38b)]&&this[_0x5f1d68(0x38b)]['setBackgroundType'](Scene_Shop['layoutSettings'][_0x5f1d68(0x650)]),this[_0x5f1d68(0x6cb)]&&this[_0x5f1d68(0x6cb)]['setBackgroundType'](Scene_Shop[_0x5f1d68(0x288)]['DummyBgType']),this[_0x5f1d68(0x300)]&&this[_0x5f1d68(0x300)]['setBackgroundType'](Scene_Shop['layoutSettings'][_0x5f1d68(0x70e)]),this['_statusWindow']&&this[_0x5f1d68(0x6dd)][_0x5f1d68(0x5da)](Scene_Shop[_0x5f1d68(0x288)][_0x5f1d68(0x4f8)]),this['_buyWindow']&&this[_0x5f1d68(0x33b)][_0x5f1d68(0x5da)](Scene_Shop['layoutSettings'][_0x5f1d68(0x22c)]),this[_0x5f1d68(0x475)]&&this[_0x5f1d68(0x475)][_0x5f1d68(0x5da)](Scene_Shop[_0x5f1d68(0x288)][_0x5f1d68(0x669)]),this[_0x5f1d68(0x2c3)]&&this[_0x5f1d68(0x2c3)][_0x5f1d68(0x5da)](Scene_Shop[_0x5f1d68(0x288)][_0x5f1d68(0x425)]);},Scene_Shop[_0x32f06e(0x76f)][_0x32f06e(0x6cd)]=function(){const _0xe91a51=_0x32f06e;return Scene_Shop[_0xe91a51(0x288)]['HelpRect']['call'](this);},Scene_Shop['prototype'][_0x32f06e(0x742)]=function(){const _0x5e71c2=_0x32f06e;return Scene_Shop[_0x5e71c2(0x288)]['GoldRect'][_0x5e71c2(0x31a)](this);},Scene_Shop[_0x32f06e(0x76f)][_0x32f06e(0x413)]=function(){const _0x1a21ff=_0x32f06e;return Scene_Shop[_0x1a21ff(0x288)][_0x1a21ff(0x59c)]['call'](this);},Scene_Shop[_0x32f06e(0x76f)][_0x32f06e(0x258)]=function(){const _0xb42017=_0x32f06e;return Scene_Shop['layoutSettings'][_0xb42017(0x6c9)][_0xb42017(0x31a)](this);},Scene_Shop[_0x32f06e(0x76f)]['numberWindowRect']=function(){const _0x5ed1cf=_0x32f06e;return Scene_Shop[_0x5ed1cf(0x288)]['NumberRect'][_0x5ed1cf(0x31a)](this);},Scene_Shop[_0x32f06e(0x76f)]['statusWindowRect']=function(){const _0x31a3d9=_0x32f06e;return Scene_Shop[_0x31a3d9(0x288)][_0x31a3d9(0x282)]['call'](this);},Scene_Shop[_0x32f06e(0x76f)][_0x32f06e(0x32f)]=function(){const _0x24d2f6=_0x32f06e;return Scene_Shop[_0x24d2f6(0x288)][_0x24d2f6(0x4b2)]['call'](this);},Scene_Shop[_0x32f06e(0x76f)][_0x32f06e(0x3df)]=function(){const _0x53f4a5=_0x32f06e;return Scene_Shop[_0x53f4a5(0x288)][_0x53f4a5(0x6f1)][_0x53f4a5(0x31a)](this);},Scene_Shop[_0x32f06e(0x76f)][_0x32f06e(0x270)]=function(){const _0x2da5cf=_0x32f06e;return Scene_Shop[_0x2da5cf(0x288)]['SellRect'][_0x2da5cf(0x31a)](this);},Scene_Name[_0x32f06e(0x288)]=VisuMZ['CoreEngine'][_0x32f06e(0x50d)]['MenuLayout']['NameMenu'],VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x273)]=Scene_Name[_0x32f06e(0x76f)][_0x32f06e(0x33a)],Scene_Name['prototype'][_0x32f06e(0x33a)]=function(){const _0x3fcfbc=_0x32f06e;VisuMZ[_0x3fcfbc(0x424)][_0x3fcfbc(0x273)][_0x3fcfbc(0x31a)](this),this['setCoreEngineUpdateWindowBg']();},Scene_Name[_0x32f06e(0x76f)][_0x32f06e(0x318)]=function(){const _0x59d459=_0x32f06e;this['_editWindow']&&this[_0x59d459(0x216)][_0x59d459(0x5da)](Scene_Name[_0x59d459(0x288)]['EditBgType']),this['_inputWindow']&&this[_0x59d459(0x700)]['setBackgroundType'](Scene_Name[_0x59d459(0x288)][_0x59d459(0x421)]);},Scene_Name['prototype'][_0x32f06e(0x508)]=function(){return 0x0;},Scene_Name[_0x32f06e(0x76f)][_0x32f06e(0x5d9)]=function(){const _0x3326a0=_0x32f06e;return Scene_Name[_0x3326a0(0x288)][_0x3326a0(0x2ce)][_0x3326a0(0x31a)](this);},Scene_Name['prototype'][_0x32f06e(0x4df)]=function(){const _0x1733cf=_0x32f06e;return Scene_Name[_0x1733cf(0x288)][_0x1733cf(0x5a7)][_0x1733cf(0x31a)](this);},Scene_Name['prototype'][_0x32f06e(0x543)]=function(){const _0x3a3c59=_0x32f06e;if(!this[_0x3a3c59(0x700)])return![];return VisuMZ['CoreEngine'][_0x3a3c59(0x50d)][_0x3a3c59(0x77f)][_0x3a3c59(0x543)];},Scene_Name[_0x32f06e(0x76f)][_0x32f06e(0x22f)]=function(){const _0x8b452a=_0x32f06e;return this['EnableNameInput']()?TextManager['getInputButtonString']('tab'):Scene_MenuBase[_0x8b452a(0x76f)][_0x8b452a(0x22f)][_0x8b452a(0x31a)](this);},Scene_Name[_0x32f06e(0x76f)][_0x32f06e(0x684)]=function(){const _0x357823=_0x32f06e;if(this['EnableNameInput']()){const _0x34d368=VisuMZ[_0x357823(0x424)][_0x357823(0x50d)][_0x357823(0x77f)];return this['_inputWindow'][_0x357823(0x5e3)]==='keyboard'?_0x34d368[_0x357823(0x5eb)]||_0x357823(0x5eb):_0x34d368['Manual']||'Manual';}else return Scene_MenuBase[_0x357823(0x76f)][_0x357823(0x684)][_0x357823(0x31a)](this);},VisuMZ['CoreEngine'][_0x32f06e(0x480)]=Scene_Battle[_0x32f06e(0x76f)]['update'],Scene_Battle[_0x32f06e(0x76f)][_0x32f06e(0x209)]=function(){const _0x32d3dd=_0x32f06e;VisuMZ[_0x32d3dd(0x424)][_0x32d3dd(0x480)][_0x32d3dd(0x31a)](this);if($gameTemp['_playTestFastMode'])this[_0x32d3dd(0x284)]();},Scene_Battle['prototype']['updatePlayTestF7']=function(){const _0x54909e=_0x32f06e;!BattleManager['isInputting']()&&!this['_playtestF7Looping']&&!$gameMessage['isBusy']()&&(this['_playtestF7Looping']=!![],this[_0x54909e(0x209)](),SceneManager['updateEffekseer'](),this[_0x54909e(0x5ab)]=![]);},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x540)]=Scene_Battle[_0x32f06e(0x76f)][_0x32f06e(0x283)],Scene_Battle[_0x32f06e(0x76f)][_0x32f06e(0x283)]=function(){const _0x2bd80e=_0x32f06e;VisuMZ['CoreEngine'][_0x2bd80e(0x540)]['call'](this),SceneManager[_0x2bd80e(0x2a9)]()&&this['repositionCancelButtonSideButtonLayout']();},Scene_Battle['prototype'][_0x32f06e(0x422)]=function(){const _0x3062b7=_0x32f06e;this[_0x3062b7(0x2d2)]['x']=Graphics['boxWidth']+0x4,this['isBottomButtonMode']()?this[_0x3062b7(0x2d2)]['y']=Graphics[_0x3062b7(0x707)]-this[_0x3062b7(0x46e)]():this[_0x3062b7(0x2d2)]['y']=0x0;},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x33c)]=Sprite_Button[_0x32f06e(0x76f)][_0x32f06e(0x58e)],Sprite_Button[_0x32f06e(0x76f)]['initialize']=function(_0x401d8a){const _0x4abdf6=_0x32f06e;VisuMZ[_0x4abdf6(0x424)]['Sprite_Button_initialize'][_0x4abdf6(0x31a)](this,_0x401d8a),this[_0x4abdf6(0x25a)]();},Sprite_Button[_0x32f06e(0x76f)][_0x32f06e(0x25a)]=function(){const _0x19a7ab=_0x32f06e,_0x5696d5=VisuMZ['CoreEngine'][_0x19a7ab(0x50d)]['UI'];this[_0x19a7ab(0x73b)]=![];switch(this[_0x19a7ab(0x21e)]){case _0x19a7ab(0x4cd):this[_0x19a7ab(0x73b)]=!_0x5696d5[_0x19a7ab(0x75a)];break;case _0x19a7ab(0x52b):case _0x19a7ab(0x271):this['_isButtonHidden']=!_0x5696d5[_0x19a7ab(0x6e2)];break;case'down':case'up':case'down2':case'up2':case'ok':this['_isButtonHidden']=!_0x5696d5['numberShowButton'];break;case'menu':this[_0x19a7ab(0x73b)]=!_0x5696d5[_0x19a7ab(0x4a8)];break;}},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x666)]=Sprite_Button[_0x32f06e(0x76f)][_0x32f06e(0x4c5)],Sprite_Button[_0x32f06e(0x76f)][_0x32f06e(0x4c5)]=function(){const _0xebb3e4=_0x32f06e;SceneManager[_0xebb3e4(0x3e9)]()||this['_isButtonHidden']?this[_0xebb3e4(0x795)]():VisuMZ['CoreEngine'][_0xebb3e4(0x666)][_0xebb3e4(0x31a)](this);},Sprite_Button[_0x32f06e(0x76f)][_0x32f06e(0x795)]=function(){const _0x3105c5=_0x32f06e;this['visible']=![],this[_0x3105c5(0x4b9)]=0x0,this['x']=Graphics[_0x3105c5(0x450)]*0xa,this['y']=Graphics[_0x3105c5(0x557)]*0xa;},VisuMZ[_0x32f06e(0x424)]['Sprite_Battler_startMove']=Sprite_Battler['prototype'][_0x32f06e(0x2d4)],Sprite_Battler['prototype'][_0x32f06e(0x2d4)]=function(_0xd29b33,_0x5bda0f,_0x23c8e7){const _0x48a8a5=_0x32f06e;(this['_targetOffsetX']!==_0xd29b33||this['_targetOffsetY']!==_0x5bda0f)&&(this[_0x48a8a5(0x6ff)](_0x48a8a5(0x3cd)),this[_0x48a8a5(0x407)]=_0x23c8e7),VisuMZ['CoreEngine']['Sprite_Battler_startMove'][_0x48a8a5(0x31a)](this,_0xd29b33,_0x5bda0f,_0x23c8e7);},Sprite_Battler[_0x32f06e(0x76f)][_0x32f06e(0x6ff)]=function(_0x510dcc){this['_moveEasingType']=_0x510dcc;},Sprite_Battler[_0x32f06e(0x76f)][_0x32f06e(0x1f2)]=function(){const _0x4cc944=_0x32f06e;if(this[_0x4cc944(0x759)]<=0x0)return;const _0x318982=this['_movementDuration'],_0xdd92b=this[_0x4cc944(0x407)],_0x577b1b=this[_0x4cc944(0x773)];this[_0x4cc944(0x790)]=this[_0x4cc944(0x3f8)](this[_0x4cc944(0x790)],this[_0x4cc944(0x3b6)],_0x318982,_0xdd92b,_0x577b1b),this[_0x4cc944(0x578)]=this[_0x4cc944(0x3f8)](this[_0x4cc944(0x578)],this[_0x4cc944(0x45d)],_0x318982,_0xdd92b,_0x577b1b),this[_0x4cc944(0x759)]--;if(this[_0x4cc944(0x759)]<=0x0)this['onMoveEnd']();},Sprite_Battler[_0x32f06e(0x76f)][_0x32f06e(0x3f8)]=function(_0x2b1c60,_0x50800b,_0x1d18a9,_0x56e2d0,_0xca651a){const _0xd9184a=_0x32f06e,_0xff6f77=VisuMZ[_0xd9184a(0x5d5)]((_0x56e2d0-_0x1d18a9)/_0x56e2d0,_0xca651a||'Linear'),_0xa1a776=VisuMZ[_0xd9184a(0x5d5)]((_0x56e2d0-_0x1d18a9+0x1)/_0x56e2d0,_0xca651a||_0xd9184a(0x3cd)),_0x171ed7=(_0x2b1c60-_0x50800b*_0xff6f77)/(0x1-_0xff6f77);return _0x171ed7+(_0x50800b-_0x171ed7)*_0xa1a776;},VisuMZ['CoreEngine'][_0x32f06e(0x377)]=Sprite_Actor[_0x32f06e(0x76f)][_0x32f06e(0x5d0)],Sprite_Actor[_0x32f06e(0x76f)]['setActorHome']=function(_0x38e2b9){const _0x327efa=_0x32f06e;VisuMZ[_0x327efa(0x424)][_0x327efa(0x50d)]['UI'][_0x327efa(0x787)]?this['setActorHomeRepositioned'](_0x38e2b9):VisuMZ[_0x327efa(0x424)]['Sprite_Actor_setActorHome'][_0x327efa(0x31a)](this,_0x38e2b9);},Sprite_Actor[_0x32f06e(0x76f)][_0x32f06e(0x280)]=function(_0x3e64e6){const _0x3e0dbb=_0x32f06e;let _0x34e319=Math['round'](Graphics[_0x3e0dbb(0x450)]/0x2+0xc0);_0x34e319-=Math[_0x3e0dbb(0x6db)]((Graphics[_0x3e0dbb(0x450)]-Graphics[_0x3e0dbb(0x254)])/0x2),_0x34e319+=_0x3e64e6*0x20;let _0x43bbe1=Graphics['height']-0xc8-$gameParty[_0x3e0dbb(0x793)]()*0x30;_0x43bbe1-=Math[_0x3e0dbb(0x6db)]((Graphics[_0x3e0dbb(0x557)]-Graphics['boxHeight'])/0x2),_0x43bbe1+=_0x3e64e6*0x30,this[_0x3e0dbb(0x2eb)](_0x34e319,_0x43bbe1);},Sprite_Actor[_0x32f06e(0x76f)][_0x32f06e(0x50c)]=function(){const _0x58ebe8=_0x32f06e;this[_0x58ebe8(0x2d4)](0x4b0,0x0,0x78);},Sprite_Animation[_0x32f06e(0x76f)][_0x32f06e(0x70b)]=function(_0x3c472d){this['_muteSound']=_0x3c472d;},VisuMZ[_0x32f06e(0x424)]['Sprite_Animation_processSoundTimings']=Sprite_Animation['prototype'][_0x32f06e(0x471)],Sprite_Animation['prototype'][_0x32f06e(0x471)]=function(){const _0x4d2dd7=_0x32f06e;if(this[_0x4d2dd7(0x5db)])return;VisuMZ[_0x4d2dd7(0x424)][_0x4d2dd7(0x2be)]['call'](this);},Sprite_Animation[_0x32f06e(0x76f)][_0x32f06e(0x756)]=function(_0x4e3661){const _0x5b6353=_0x32f06e;if(_0x4e3661[_0x5b6353(0x733)]){}const _0x36653c=this[_0x5b6353(0x230)][_0x5b6353(0x6b9)];let _0x284a34=_0x4e3661[_0x5b6353(0x557)]*_0x4e3661['scale']['y'],_0x3adc13=0x0,_0x2ba1aa=-_0x284a34/0x2;if(_0x36653c['match'](/<(?:HEAD|HEADER|TOP)>/i))_0x2ba1aa=-_0x284a34;if(_0x36653c[_0x5b6353(0x4c0)](/<(?:FOOT|FOOTER|BOTTOM)>/i))_0x2ba1aa=0x0;if(_0x36653c[_0x5b6353(0x4c0)](/<(?:LEFT)>/i))_0x3adc13=-_0x4e3661[_0x5b6353(0x450)]/0x2;if(_0x36653c['match'](/<(?:RIGHT)>/i))_0x2ba1aa=_0x4e3661[_0x5b6353(0x450)]/0x2;if(_0x36653c[_0x5b6353(0x4c0)](/<ANCHOR X:[ ](\d+\.?\d*)>/i))_0x3adc13=Number(RegExp['$1'])*_0x4e3661[_0x5b6353(0x450)];_0x36653c[_0x5b6353(0x4c0)](/<ANCHOR Y:[ ](\d+\.?\d*)>/i)&&(_0x2ba1aa=(0x1-Number(RegExp['$1']))*-_0x284a34);_0x36653c[_0x5b6353(0x4c0)](/<ANCHOR:[ ](\d+\.?\d*),[ ](\d+\.?\d*)>/i)&&(_0x3adc13=Number(RegExp['$1'])*_0x4e3661[_0x5b6353(0x450)],_0x2ba1aa=(0x1-Number(RegExp['$2']))*-_0x284a34);if(_0x36653c['match'](/<OFFSET X:[ ]([\+\-]\d+)>/i))_0x3adc13+=Number(RegExp['$1']);if(_0x36653c[_0x5b6353(0x4c0)](/<OFFSET Y:[ ]([\+\-]\d+)>/i))_0x2ba1aa+=Number(RegExp['$1']);_0x36653c[_0x5b6353(0x4c0)](/<OFFSET:[ ]([\+\-]\d+),[ ]([\+\-]\d+)>/i)&&(_0x3adc13+=Number(RegExp['$1']),_0x2ba1aa+=Number(RegExp['$2']));const _0x33ddf2=new Point(_0x3adc13,_0x2ba1aa);return _0x4e3661[_0x5b6353(0x65f)](),_0x4e3661[_0x5b6353(0x336)][_0x5b6353(0x41c)](_0x33ddf2);},Sprite_AnimationMV['prototype'][_0x32f06e(0x70b)]=function(_0x4886b4){const _0x550156=_0x32f06e;this[_0x550156(0x5db)]=_0x4886b4;},VisuMZ[_0x32f06e(0x424)]['Sprite_AnimationMV_processTimingData']=Sprite_AnimationMV[_0x32f06e(0x76f)][_0x32f06e(0x299)],Sprite_AnimationMV[_0x32f06e(0x76f)][_0x32f06e(0x299)]=function(_0x24b1ed){const _0x28b3d8=_0x32f06e;this['_muteSound']&&(_0x24b1ed=JsonEx['makeDeepCopy'](_0x24b1ed),_0x24b1ed['se']&&(_0x24b1ed['se']['volume']=0x0)),VisuMZ[_0x28b3d8(0x424)][_0x28b3d8(0x4db)][_0x28b3d8(0x31a)](this,_0x24b1ed);},Sprite_Damage[_0x32f06e(0x76f)][_0x32f06e(0x73e)]=function(_0x143d35){const _0x32107d=_0x32f06e;let _0x7833=Math[_0x32107d(0x3f4)](_0x143d35)[_0x32107d(0x634)]();this[_0x32107d(0x2ec)]()&&(_0x7833=VisuMZ[_0x32107d(0x21d)](_0x7833));const _0x28065c=this[_0x32107d(0x33f)](),_0x58fe92=Math[_0x32107d(0x6db)](_0x28065c*0.75);for(let _0x5a6cae=0x0;_0x5a6cae<_0x7833['length'];_0x5a6cae++){const _0x10c1ca=this[_0x32107d(0x58f)](_0x58fe92,_0x28065c);_0x10c1ca[_0x32107d(0x1f7)][_0x32107d(0x516)](_0x7833[_0x5a6cae],0x0,0x0,_0x58fe92,_0x28065c,_0x32107d(0x329)),_0x10c1ca['x']=(_0x5a6cae-(_0x7833[_0x32107d(0x26a)]-0x1)/0x2)*_0x58fe92,_0x10c1ca['dy']=-_0x5a6cae;}},Sprite_Damage[_0x32f06e(0x76f)][_0x32f06e(0x2ec)]=function(){const _0x2388c7=_0x32f06e;return VisuMZ[_0x2388c7(0x424)][_0x2388c7(0x50d)][_0x2388c7(0x248)][_0x2388c7(0x260)];},Sprite_Damage[_0x32f06e(0x76f)][_0x32f06e(0x27d)]=function(){return ColorManager['outlineColorDmg']();},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x21a)]=Sprite_Gauge[_0x32f06e(0x76f)][_0x32f06e(0x73c)],Sprite_Gauge[_0x32f06e(0x76f)]['gaugeRate']=function(){const _0xb6e466=_0x32f06e;return VisuMZ['CoreEngine']['Sprite_Gauge_gaugeRate'][_0xb6e466(0x31a)](this)[_0xb6e466(0x32b)](0x0,0x1);},VisuMZ['CoreEngine'][_0x32f06e(0x414)]=Sprite_Gauge[_0x32f06e(0x76f)][_0x32f06e(0x441)],Sprite_Gauge['prototype'][_0x32f06e(0x441)]=function(){const _0x2b4738=_0x32f06e;let _0xb2aff4=VisuMZ['CoreEngine'][_0x2b4738(0x414)][_0x2b4738(0x31a)](this);return _0xb2aff4;},Sprite_Gauge[_0x32f06e(0x76f)]['drawValue']=function(){const _0x40ade1=_0x32f06e;let _0x3bb264=this[_0x40ade1(0x441)]();this[_0x40ade1(0x2ec)]()&&(_0x3bb264=VisuMZ['GroupDigits'](_0x3bb264));const _0x271d60=this['bitmapWidth']()-0x1,_0x4760a7=this['bitmapHeight']();this[_0x40ade1(0x3ac)](),this['bitmap']['drawText'](_0x3bb264,0x0,0x0,_0x271d60,_0x4760a7,'right');},Sprite_Gauge['prototype']['valueOutlineWidth']=function(){return 0x3;},Sprite_Gauge[_0x32f06e(0x76f)]['useDigitGrouping']=function(){const _0x2d0e8d=_0x32f06e;return VisuMZ[_0x2d0e8d(0x424)][_0x2d0e8d(0x50d)][_0x2d0e8d(0x248)][_0x2d0e8d(0x4d6)];},Sprite_Gauge['prototype'][_0x32f06e(0x27d)]=function(){const _0x4a968e=_0x32f06e;return ColorManager[_0x4a968e(0x20a)]();};function Sprite_TitlePictureButton(){this['initialize'](...arguments);}Sprite_TitlePictureButton[_0x32f06e(0x76f)]=Object[_0x32f06e(0x33a)](Sprite_Clickable['prototype']),Sprite_TitlePictureButton['prototype'][_0x32f06e(0x38e)]=Sprite_TitlePictureButton,Sprite_TitlePictureButton[_0x32f06e(0x76f)][_0x32f06e(0x58e)]=function(_0x584af7){const _0x2a97d2=_0x32f06e;Sprite_Clickable[_0x2a97d2(0x76f)][_0x2a97d2(0x58e)][_0x2a97d2(0x31a)](this),this['_data']=_0x584af7,this['_clickHandler']=null,this['setup']();},Sprite_TitlePictureButton[_0x32f06e(0x76f)][_0x32f06e(0x5fb)]=function(){const _0xde0176=_0x32f06e;this['x']=Graphics['width'],this['y']=Graphics[_0xde0176(0x557)],this[_0xde0176(0x435)]=![],this[_0xde0176(0x59e)]();},Sprite_TitlePictureButton['prototype'][_0x32f06e(0x59e)]=function(){const _0x444de5=_0x32f06e;this[_0x444de5(0x1f7)]=ImageManager[_0x444de5(0x662)](this[_0x444de5(0x390)][_0x444de5(0x75c)]),this[_0x444de5(0x1f7)][_0x444de5(0x220)](this[_0x444de5(0x39d)][_0x444de5(0x3da)](this));},Sprite_TitlePictureButton[_0x32f06e(0x76f)][_0x32f06e(0x39d)]=function(){const _0x31b813=_0x32f06e;this['_data'][_0x31b813(0x45f)]['call'](this),this[_0x31b813(0x390)][_0x31b813(0x769)][_0x31b813(0x31a)](this),this['setClickHandler'](this[_0x31b813(0x390)][_0x31b813(0x416)]['bind'](this));},Sprite_TitlePictureButton['prototype'][_0x32f06e(0x209)]=function(){const _0x24ae9f=_0x32f06e;Sprite_Clickable[_0x24ae9f(0x76f)][_0x24ae9f(0x209)][_0x24ae9f(0x31a)](this),this[_0x24ae9f(0x4c5)](),this[_0x24ae9f(0x6e7)]();},Sprite_TitlePictureButton[_0x32f06e(0x76f)][_0x32f06e(0x351)]=function(){const _0x3e58b3=_0x32f06e;return VisuMZ[_0x3e58b3(0x424)][_0x3e58b3(0x50d)][_0x3e58b3(0x59b)][_0x3e58b3(0x6f6)]['ButtonFadeSpeed'];},Sprite_TitlePictureButton['prototype'][_0x32f06e(0x4c5)]=function(){const _0x282772=_0x32f06e;this['_pressed']?this[_0x282772(0x4b9)]=0xff:(this[_0x282772(0x4b9)]+=this[_0x282772(0x435)]?this['fadeSpeed']():-0x1*this[_0x282772(0x351)](),this[_0x282772(0x4b9)]=Math['min'](0xc0,this[_0x282772(0x4b9)]));},Sprite_TitlePictureButton[_0x32f06e(0x76f)]['setClickHandler']=function(_0x581329){const _0x518edc=_0x32f06e;this[_0x518edc(0x5c7)]=_0x581329;},Sprite_TitlePictureButton['prototype'][_0x32f06e(0x437)]=function(){const _0x235b10=_0x32f06e;this[_0x235b10(0x5c7)]&&this[_0x235b10(0x5c7)]();},VisuMZ['CoreEngine'][_0x32f06e(0x30a)]=Spriteset_Base[_0x32f06e(0x76f)][_0x32f06e(0x58e)],Spriteset_Base[_0x32f06e(0x76f)][_0x32f06e(0x58e)]=function(){const _0x36ee6a=_0x32f06e;VisuMZ['CoreEngine'][_0x36ee6a(0x30a)][_0x36ee6a(0x31a)](this),this[_0x36ee6a(0x57b)]();},Spriteset_Base[_0x32f06e(0x76f)][_0x32f06e(0x57b)]=function(){const _0x2e56f1=_0x32f06e;this[_0x2e56f1(0x2a6)]=[],this[_0x2e56f1(0x1f9)]=this[_0x2e56f1(0x5e9)]['x'],this[_0x2e56f1(0x5a0)]=this[_0x2e56f1(0x5e9)]['y'];},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x4fb)]=Spriteset_Base[_0x32f06e(0x76f)][_0x32f06e(0x738)],Spriteset_Base[_0x32f06e(0x76f)][_0x32f06e(0x738)]=function(_0x3cc6d4){const _0x385e61=_0x32f06e;this[_0x385e61(0x23f)](),VisuMZ[_0x385e61(0x424)]['Spriteset_Base_destroy']['call'](this,_0x3cc6d4);},VisuMZ['CoreEngine']['Spriteset_Base_update']=Spriteset_Base[_0x32f06e(0x76f)][_0x32f06e(0x209)],Spriteset_Base['prototype'][_0x32f06e(0x209)]=function(){const _0xe1c350=_0x32f06e;VisuMZ['CoreEngine'][_0xe1c350(0x486)][_0xe1c350(0x31a)](this),this['updatePictureAntiZoom'](),this['updateFauxAnimations']();},Spriteset_Base[_0x32f06e(0x76f)]['updatePictureAntiZoom']=function(){const _0x4e03e2=_0x32f06e;if(!VisuMZ['CoreEngine']['Settings'][_0x4e03e2(0x248)][_0x4e03e2(0x4fe)])return;if(this[_0x4e03e2(0x1f9)]===this['scale']['x']&&this[_0x4e03e2(0x5a0)]===this[_0x4e03e2(0x5e9)]['y'])return;this[_0x4e03e2(0x75e)](),this[_0x4e03e2(0x1f9)]=this['scale']['x'],this[_0x4e03e2(0x5a0)]=this[_0x4e03e2(0x5e9)]['y'];},Spriteset_Base['prototype'][_0x32f06e(0x75e)]=function(){const _0x2fa2b5=_0x32f06e;this['scale']['x']!==0x0&&(this[_0x2fa2b5(0x587)][_0x2fa2b5(0x5e9)]['x']=0x1/this['scale']['x'],this[_0x2fa2b5(0x587)]['x']=-(this['x']/this[_0x2fa2b5(0x5e9)]['x'])),this[_0x2fa2b5(0x5e9)]['y']!==0x0&&(this[_0x2fa2b5(0x587)][_0x2fa2b5(0x5e9)]['y']=0x1/this[_0x2fa2b5(0x5e9)]['y'],this['_pictureContainer']['y']=-(this['y']/this[_0x2fa2b5(0x5e9)]['y']));},Spriteset_Base[_0x32f06e(0x76f)][_0x32f06e(0x3c0)]=function(){const _0x3ae360=_0x32f06e;for(const _0x51fdcb of this[_0x3ae360(0x2a6)]){!_0x51fdcb['isPlaying']()&&this[_0x3ae360(0x417)](_0x51fdcb);}this[_0x3ae360(0x31c)]();},Spriteset_Base[_0x32f06e(0x76f)][_0x32f06e(0x31c)]=function(){const _0x23e45b=_0x32f06e;for(;;){const _0x23b01c=$gameTemp[_0x23e45b(0x355)]();if(_0x23b01c)this[_0x23e45b(0x5f3)](_0x23b01c);else break;}},Spriteset_Base[_0x32f06e(0x76f)][_0x32f06e(0x5f3)]=function(_0x36621c){const _0x35c7b8=_0x32f06e,_0x32b44c=$dataAnimations[_0x36621c[_0x35c7b8(0x534)]],_0x4db217=_0x36621c[_0x35c7b8(0x6d0)],_0x5bb95e=_0x36621c[_0x35c7b8(0x630)],_0x5e6ffa=_0x36621c[_0x35c7b8(0x334)];let _0x364d7b=this['animationBaseDelay']();const _0x3ff154=this['animationNextDelay']();if(this[_0x35c7b8(0x21b)](_0x32b44c))for(const _0x157744 of _0x4db217){this[_0x35c7b8(0x2f1)]([_0x157744],_0x32b44c,_0x5bb95e,_0x364d7b,_0x5e6ffa),_0x364d7b+=_0x3ff154;}else this[_0x35c7b8(0x2f1)](_0x4db217,_0x32b44c,_0x5bb95e,_0x364d7b,_0x5e6ffa);},Spriteset_Base[_0x32f06e(0x76f)][_0x32f06e(0x2f1)]=function(_0x3815d2,_0x54ee59,_0xfd2893,_0x445ec9,_0x579d3c){const _0x521255=_0x32f06e,_0x1e10fa=this[_0x521255(0x27e)](_0x54ee59),_0x17b983=new(_0x1e10fa?Sprite_AnimationMV:Sprite_Animation)(),_0x545855=this[_0x521255(0x371)](_0x3815d2);this['animationShouldMirror'](_0x3815d2[0x0])&&(_0xfd2893=!_0xfd2893),_0x17b983[_0x521255(0x597)]=_0x3815d2,_0x17b983[_0x521255(0x5fb)](_0x545855,_0x54ee59,_0xfd2893,_0x445ec9),_0x17b983[_0x521255(0x70b)](_0x579d3c),this[_0x521255(0x323)]['addChild'](_0x17b983),this['_fauxAnimationSprites'][_0x521255(0x50f)](_0x17b983);},Spriteset_Base['prototype'][_0x32f06e(0x417)]=function(_0x45ecf6){const _0x59998a=_0x32f06e;this[_0x59998a(0x2a6)][_0x59998a(0x4a6)](_0x45ecf6),this[_0x59998a(0x323)][_0x59998a(0x1da)](_0x45ecf6);for(const _0x16753f of _0x45ecf6[_0x59998a(0x597)]){_0x16753f[_0x59998a(0x3fd)]&&_0x16753f['endAnimation']();}_0x45ecf6['destroy']();},Spriteset_Base['prototype'][_0x32f06e(0x23f)]=function(){const _0x4c09af=_0x32f06e;for(const _0x46ca54 of this[_0x4c09af(0x2a6)]){this['removeFauxAnimation'](_0x46ca54);}},Spriteset_Base[_0x32f06e(0x76f)]['isFauxAnimationPlaying']=function(){const _0x17b1b3=_0x32f06e;return this[_0x17b1b3(0x2a6)][_0x17b1b3(0x26a)]>0x0;},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x485)]=Spriteset_Base[_0x32f06e(0x76f)][_0x32f06e(0x2f2)],Spriteset_Base[_0x32f06e(0x76f)][_0x32f06e(0x2f2)]=function(){const _0x561e4b=_0x32f06e;VisuMZ[_0x561e4b(0x424)][_0x561e4b(0x485)][_0x561e4b(0x31a)](this),this['updatePositionCoreEngine']();},Spriteset_Base[_0x32f06e(0x76f)]['updatePositionCoreEngine']=function(){const _0x4c2d15=_0x32f06e;if(!$gameScreen)return;if($gameScreen[_0x4c2d15(0x55c)]<=0x0)return;this['x']-=Math[_0x4c2d15(0x237)]($gameScreen[_0x4c2d15(0x544)]());const _0x133366=$gameScreen[_0x4c2d15(0x4e9)]();switch($gameScreen['getCoreEngineScreenShakeStyle']()){case _0x4c2d15(0x49e):this[_0x4c2d15(0x6fe)]();break;case'horizontal':this['updatePositionCoreEngineShakeHorz']();break;case _0x4c2d15(0x22e):this[_0x4c2d15(0x5d3)]();break;default:this['updatePositionCoreEngineShakeRand']();break;}},Spriteset_Base[_0x32f06e(0x76f)][_0x32f06e(0x6fe)]=function(){const _0x340e23=_0x32f06e,_0x2792ec=VisuMZ[_0x340e23(0x424)][_0x340e23(0x50d)][_0x340e23(0x4fd)];if(_0x2792ec&&_0x2792ec['originalJS'])return _0x2792ec[_0x340e23(0x5d4)][_0x340e23(0x31a)](this);this['x']+=Math[_0x340e23(0x237)]($gameScreen['shake']());},Spriteset_Base['prototype'][_0x32f06e(0x6fb)]=function(){const _0x5792e9=_0x32f06e,_0x3b6621=VisuMZ['CoreEngine'][_0x5792e9(0x50d)][_0x5792e9(0x4fd)];if(_0x3b6621&&_0x3b6621[_0x5792e9(0x25e)])return _0x3b6621[_0x5792e9(0x25e)][_0x5792e9(0x31a)](this);const _0x53a191=$gameScreen[_0x5792e9(0x4d4)]*0.75,_0x157b44=$gameScreen['_shakeSpeed']*0.6,_0x2f6405=$gameScreen['_shakeDuration'];this['x']+=Math[_0x5792e9(0x237)](Math[_0x5792e9(0x607)](_0x53a191)-Math['randomInt'](_0x157b44))*(Math[_0x5792e9(0x6ba)](_0x2f6405,0x1e)*0.5),this['y']+=Math[_0x5792e9(0x237)](Math[_0x5792e9(0x607)](_0x53a191)-Math[_0x5792e9(0x607)](_0x157b44))*(Math[_0x5792e9(0x6ba)](_0x2f6405,0x1e)*0.5);},Spriteset_Base[_0x32f06e(0x76f)][_0x32f06e(0x524)]=function(){const _0x9facc3=_0x32f06e,_0x5d5abe=VisuMZ[_0x9facc3(0x424)][_0x9facc3(0x50d)][_0x9facc3(0x4fd)];if(_0x5d5abe&&_0x5d5abe['horzJS'])return _0x5d5abe['horzJS'][_0x9facc3(0x31a)](this);const _0x3ba153=$gameScreen[_0x9facc3(0x4d4)]*0.75,_0x334b29=$gameScreen[_0x9facc3(0x6c2)]*0.6,_0x45f88f=$gameScreen[_0x9facc3(0x55c)];this['x']+=Math[_0x9facc3(0x237)](Math[_0x9facc3(0x607)](_0x3ba153)-Math[_0x9facc3(0x607)](_0x334b29))*(Math[_0x9facc3(0x6ba)](_0x45f88f,0x1e)*0.5);},Spriteset_Base[_0x32f06e(0x76f)][_0x32f06e(0x5d3)]=function(){const _0x9f115=_0x32f06e,_0x2b4ddd=VisuMZ[_0x9f115(0x424)]['Settings'][_0x9f115(0x4fd)];if(_0x2b4ddd&&_0x2b4ddd['vertJS'])return _0x2b4ddd[_0x9f115(0x383)]['call'](this);const _0x1933f2=$gameScreen[_0x9f115(0x4d4)]*0.75,_0x522671=$gameScreen['_shakeSpeed']*0.6,_0x5b9204=$gameScreen[_0x9f115(0x55c)];this['y']+=Math[_0x9f115(0x237)](Math[_0x9f115(0x607)](_0x1933f2)-Math[_0x9f115(0x607)](_0x522671))*(Math[_0x9f115(0x6ba)](_0x5b9204,0x1e)*0.5);},Spriteset_Battle[_0x32f06e(0x76f)][_0x32f06e(0x48a)]=function(){const _0x15dbce=_0x32f06e;this[_0x15dbce(0x238)]=new PIXI['filters']['BlurFilter'](clamp=!![]),this[_0x15dbce(0x2fc)]=new Sprite(),this[_0x15dbce(0x2fc)]['bitmap']=SceneManager[_0x15dbce(0x511)](),this['_backgroundSprite'][_0x15dbce(0x57d)]=[this[_0x15dbce(0x238)]],this[_0x15dbce(0x459)]['addChild'](this['_backgroundSprite']);},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x4f5)]=Spriteset_Battle['prototype']['createEnemies'],Spriteset_Battle['prototype']['createEnemies']=function(){const _0x205021=_0x32f06e;VisuMZ[_0x205021(0x424)][_0x205021(0x50d)]['UI']['RepositionEnemies']&&this['repositionEnemiesByResolution'](),VisuMZ[_0x205021(0x424)]['Spriteset_Battle_createEnemies'][_0x205021(0x31a)](this);},Spriteset_Battle[_0x32f06e(0x76f)][_0x32f06e(0x49d)]=function(){const _0x3c6ba0=_0x32f06e;for(member of $gameTroop[_0x3c6ba0(0x51e)]()){member[_0x3c6ba0(0x695)]();}},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x692)]=Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x58e)],Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x58e)]=function(_0x837330){const _0x717ef0=_0x32f06e;_0x837330['x']=Math[_0x717ef0(0x237)](_0x837330['x']),_0x837330['y']=Math['round'](_0x837330['y']),_0x837330[_0x717ef0(0x450)]=Math['round'](_0x837330[_0x717ef0(0x450)]),_0x837330['height']=Math['round'](_0x837330[_0x717ef0(0x557)]),this['initDigitGrouping'](),VisuMZ[_0x717ef0(0x424)]['Window_Base_initialize'][_0x717ef0(0x31a)](this,_0x837330),this[_0x717ef0(0x1e0)]();},Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x285)]=function(){const _0xe166dd=_0x32f06e;this[_0xe166dd(0x529)]=VisuMZ[_0xe166dd(0x424)]['Settings'][_0xe166dd(0x248)][_0xe166dd(0x3fb)],this[_0xe166dd(0x2b9)]=VisuMZ[_0xe166dd(0x424)][_0xe166dd(0x50d)][_0xe166dd(0x248)][_0xe166dd(0x431)];},Window_Base['prototype'][_0x32f06e(0x682)]=function(){const _0x1716e3=_0x32f06e;return VisuMZ[_0x1716e3(0x424)][_0x1716e3(0x50d)][_0x1716e3(0x298)]['LineHeight'];},Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x610)]=function(){const _0x3b1243=_0x32f06e;return VisuMZ[_0x3b1243(0x424)][_0x3b1243(0x50d)][_0x3b1243(0x298)]['ItemPadding'];},Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x494)]=function(){const _0x263ff3=_0x32f06e;this[_0x263ff3(0x646)]=VisuMZ['CoreEngine']['Settings']['Window']['BackOpacity'];},Window_Base['prototype'][_0x32f06e(0x366)]=function(){const _0x2871ca=_0x32f06e;return VisuMZ[_0x2871ca(0x424)][_0x2871ca(0x50d)][_0x2871ca(0x298)][_0x2871ca(0x68d)];},Window_Base['prototype']['openingSpeed']=function(){const _0x235353=_0x32f06e;return VisuMZ['CoreEngine'][_0x235353(0x50d)][_0x235353(0x298)][_0x235353(0x5e4)];},VisuMZ['CoreEngine'][_0x32f06e(0x5a3)]=Window_Base[_0x32f06e(0x76f)]['update'],Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x209)]=function(){const _0x51fdb5=_0x32f06e;VisuMZ[_0x51fdb5(0x424)]['Window_Base_update'][_0x51fdb5(0x31a)](this),this['updateCoreEasing']();},Window_Base[_0x32f06e(0x76f)]['updateOpen']=function(){const _0x3ef0aa=_0x32f06e;this[_0x3ef0aa(0x3ad)]&&(this['openness']+=this[_0x3ef0aa(0x2ab)](),this[_0x3ef0aa(0x545)]()&&(this[_0x3ef0aa(0x3ad)]=![]));},Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x664)]=function(){const _0xcb8a95=_0x32f06e;this[_0xcb8a95(0x5dc)]&&(this[_0xcb8a95(0x200)]-=this['openingSpeed'](),this['isClosed']()&&(this[_0xcb8a95(0x5dc)]=![]));},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x6ce)]=Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x516)],Window_Base['prototype'][_0x32f06e(0x516)]=function(_0x206c8e,_0x423bae,_0x32a5e,_0x5c24f1,_0x38ed66){const _0x415ca3=_0x32f06e;if(this['useDigitGrouping']())_0x206c8e=VisuMZ[_0x415ca3(0x21d)](_0x206c8e);VisuMZ['CoreEngine'][_0x415ca3(0x6ce)][_0x415ca3(0x31a)](this,_0x206c8e,_0x423bae,_0x32a5e,_0x5c24f1,_0x38ed66);},Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x2ec)]=function(){const _0x33c786=_0x32f06e;return this[_0x33c786(0x529)];},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x294)]=Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x654)],Window_Base[_0x32f06e(0x76f)]['createTextState']=function(_0x34aa4e,_0x1b8434,_0x469304,_0x2412ab){const _0x571c1b=_0x32f06e;var _0x247462=VisuMZ[_0x571c1b(0x424)]['Window_Base_createTextState'][_0x571c1b(0x31a)](this,_0x34aa4e,_0x1b8434,_0x469304,_0x2412ab);if(this[_0x571c1b(0x770)]())_0x247462[_0x571c1b(0x3f5)]=VisuMZ[_0x571c1b(0x21d)](_0x247462[_0x571c1b(0x3f5)]);return _0x247462;},Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x770)]=function(){const _0x4e2535=_0x32f06e;return this[_0x4e2535(0x2b9)];},Window_Base[_0x32f06e(0x76f)]['enableDigitGrouping']=function(_0x3874e8){const _0x3ff714=_0x32f06e;this[_0x3ff714(0x529)]=_0x3874e8;},Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x44c)]=function(_0xb55c54){const _0x20e45e=_0x32f06e;this[_0x20e45e(0x2b9)]=_0xb55c54;},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x1f3)]=Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x751)],Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x751)]=function(_0x57bcc3,_0x1f5b46,_0x58a3fa){const _0x4d320b=_0x32f06e;_0x1f5b46=Math['round'](_0x1f5b46),_0x58a3fa=Math[_0x4d320b(0x237)](_0x58a3fa),VisuMZ[_0x4d320b(0x424)]['Window_Base_drawIcon']['call'](this,_0x57bcc3,_0x1f5b46,_0x58a3fa);},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x410)]=Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x66c)],Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x66c)]=function(_0x1a6f4d,_0x41476c,_0x447932,_0x42a09c,_0x56e5e8,_0x60c652){const _0x4b019c=_0x32f06e;_0x56e5e8=_0x56e5e8||ImageManager['faceWidth'],_0x60c652=_0x60c652||ImageManager['faceHeight'],_0x447932=Math[_0x4b019c(0x237)](_0x447932),_0x42a09c=Math[_0x4b019c(0x237)](_0x42a09c),_0x56e5e8=Math[_0x4b019c(0x237)](_0x56e5e8),_0x60c652=Math['round'](_0x60c652),VisuMZ[_0x4b019c(0x424)][_0x4b019c(0x410)][_0x4b019c(0x31a)](this,_0x1a6f4d,_0x41476c,_0x447932,_0x42a09c,_0x56e5e8,_0x60c652);},VisuMZ['CoreEngine']['Window_Base_drawCharacter']=Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x6e8)],Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x6e8)]=function(_0x297bad,_0x58ab47,_0x482fc2,_0x2ce32f){const _0x1d9596=_0x32f06e;_0x482fc2=Math[_0x1d9596(0x237)](_0x482fc2),_0x2ce32f=Math['round'](_0x2ce32f),VisuMZ[_0x1d9596(0x424)][_0x1d9596(0x54f)][_0x1d9596(0x31a)](this,_0x297bad,_0x58ab47,_0x482fc2,_0x2ce32f);},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x53b)]=Window_Selectable['prototype'][_0x32f06e(0x39e)],Window_Selectable[_0x32f06e(0x76f)]['itemRect']=function(_0x2a0c82){const _0x5d3421=_0x32f06e;let _0x3aed34=VisuMZ[_0x5d3421(0x424)][_0x5d3421(0x53b)][_0x5d3421(0x31a)](this,_0x2a0c82);return _0x3aed34['x']=Math[_0x5d3421(0x237)](_0x3aed34['x']),_0x3aed34['y']=Math['round'](_0x3aed34['y']),_0x3aed34['width']=Math[_0x5d3421(0x237)](_0x3aed34['width']),_0x3aed34['height']=Math[_0x5d3421(0x237)](_0x3aed34[_0x5d3421(0x557)]),_0x3aed34;},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x45c)]=Window_StatusBase[_0x32f06e(0x76f)]['drawActorSimpleStatus'],Window_StatusBase['prototype'][_0x32f06e(0x2a0)]=function(_0x2f7fb9,_0x1f0cda,_0x2a205b){const _0x1b0daf=_0x32f06e;_0x1f0cda=Math[_0x1b0daf(0x237)](_0x1f0cda),_0x2a205b=Math[_0x1b0daf(0x237)](_0x2a205b),VisuMZ['CoreEngine'][_0x1b0daf(0x45c)][_0x1b0daf(0x31a)](this,_0x2f7fb9,_0x1f0cda,_0x2a205b);},Window_Base['prototype'][_0x32f06e(0x1e0)]=function(){const _0x26b552=_0x32f06e;this['_coreEasing']={'duration':0x0,'wholeDuration':0x0,'type':_0x26b552(0x3bf),'targetX':this['x'],'targetY':this['y'],'targetScaleX':this['scale']['x'],'targetScaleY':this['scale']['y'],'targetOpacity':this[_0x26b552(0x4b9)],'targetBackOpacity':this['backOpacity'],'targetContentsOpacity':this[_0x26b552(0x472)]};},Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x397)]=function(){const _0x11404a=_0x32f06e;if(!this['_coreEasing'])return;if(this['_coreEasing'][_0x11404a(0x43f)]<=0x0)return;this['x']=this[_0x11404a(0x376)](this['x'],this[_0x11404a(0x29c)][_0x11404a(0x728)]),this['y']=this[_0x11404a(0x376)](this['y'],this[_0x11404a(0x29c)][_0x11404a(0x222)]),this['scale']['x']=this[_0x11404a(0x376)](this[_0x11404a(0x5e9)]['x'],this[_0x11404a(0x29c)]['targetScaleX']),this['scale']['y']=this[_0x11404a(0x376)](this[_0x11404a(0x5e9)]['y'],this[_0x11404a(0x29c)][_0x11404a(0x35c)]),this['opacity']=this[_0x11404a(0x376)](this['opacity'],this['_coreEasing'][_0x11404a(0x5c6)]),this[_0x11404a(0x646)]=this[_0x11404a(0x376)](this['backOpacity'],this[_0x11404a(0x29c)][_0x11404a(0x765)]),this[_0x11404a(0x472)]=this[_0x11404a(0x376)](this[_0x11404a(0x472)],this[_0x11404a(0x29c)]['targetContentsOpacity']),this[_0x11404a(0x29c)]['duration']--;},Window_Base['prototype']['applyCoreEasing']=function(_0x4eb7a3,_0x136649){const _0x1ca85a=_0x32f06e;if(!this[_0x1ca85a(0x29c)])return _0x136649;const _0x245691=this[_0x1ca85a(0x29c)]['duration'],_0x3c68c6=this[_0x1ca85a(0x29c)][_0x1ca85a(0x4ff)],_0x1d2fc8=this[_0x1ca85a(0x3a5)]((_0x3c68c6-_0x245691)/_0x3c68c6),_0x19ee22=this['calcCoreEasing']((_0x3c68c6-_0x245691+0x1)/_0x3c68c6),_0x543140=(_0x4eb7a3-_0x136649*_0x1d2fc8)/(0x1-_0x1d2fc8);return _0x543140+(_0x136649-_0x543140)*_0x19ee22;},Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x3a5)]=function(_0x32cb26){const _0x45a26b=_0x32f06e;if(!this[_0x45a26b(0x29c)])return _0x32cb26;return VisuMZ[_0x45a26b(0x5d5)](_0x32cb26,this['_coreEasing'][_0x45a26b(0x537)]||_0x45a26b(0x3bf));},Window_Base['prototype'][_0x32f06e(0x555)]=function(_0x44a023,_0xffa0f9){const _0x47a1de=_0x32f06e;if(!this[_0x47a1de(0x29c)])return;this['x']=this[_0x47a1de(0x29c)][_0x47a1de(0x728)],this['y']=this[_0x47a1de(0x29c)][_0x47a1de(0x222)],this[_0x47a1de(0x5e9)]['x']=this[_0x47a1de(0x29c)][_0x47a1de(0x600)],this['scale']['y']=this['_coreEasing']['targetScaleY'],this[_0x47a1de(0x4b9)]=this[_0x47a1de(0x29c)][_0x47a1de(0x5c6)],this['backOpacity']=this[_0x47a1de(0x29c)]['targetBackOpacity'],this[_0x47a1de(0x472)]=this[_0x47a1de(0x29c)]['targetContentsOpacity'],this['setupCoreEasing'](_0x44a023,_0xffa0f9,this['x'],this['y'],this[_0x47a1de(0x5e9)]['x'],this['scale']['y'],this[_0x47a1de(0x4b9)],this[_0x47a1de(0x646)],this[_0x47a1de(0x472)]);},Window_Base['prototype'][_0x32f06e(0x6a4)]=function(_0x309fe5,_0x14521d,_0x2fdf29,_0xd3f080,_0x1e250f,_0x34f5eb,_0x3f5b95,_0x7a9a08,_0x9e5224){const _0x12ace6=_0x32f06e;this[_0x12ace6(0x29c)]={'duration':_0x309fe5,'wholeDuration':_0x309fe5,'type':_0x14521d,'targetX':_0x2fdf29,'targetY':_0xd3f080,'targetScaleX':_0x1e250f,'targetScaleY':_0x34f5eb,'targetOpacity':_0x3f5b95,'targetBackOpacity':_0x7a9a08,'targetContentsOpacity':_0x9e5224};},Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x59a)]=function(_0x31c5fb,_0x3b5c28,_0x26d5e5,_0x38b40a,_0x182aca){const _0x46395f=_0x32f06e;this[_0x46395f(0x382)](),this[_0x46395f(0x34b)]['fontSize']=VisuMZ['CoreEngine']['Settings']['Gold'][_0x46395f(0x55b)];const _0x5248b9=VisuMZ['CoreEngine'][_0x46395f(0x50d)][_0x46395f(0x2c4)][_0x46395f(0x3b5)];if(_0x5248b9>0x0&&_0x3b5c28===TextManager[_0x46395f(0x3be)]){const _0x409ecc=_0x38b40a+(this['lineHeight']()-ImageManager[_0x46395f(0x29d)])/0x2;this['drawIcon'](_0x5248b9,_0x26d5e5+(_0x182aca-ImageManager['iconWidth']),_0x409ecc),_0x182aca-=ImageManager[_0x46395f(0x36b)]+0x4;}else this['changeTextColor'](ColorManager[_0x46395f(0x637)]()),this['drawText'](_0x3b5c28,_0x26d5e5,_0x38b40a,_0x182aca,_0x46395f(0x295)),_0x182aca-=this[_0x46395f(0x3e7)](_0x3b5c28)+0x6;this[_0x46395f(0x4d3)]();const _0x1029fb=this[_0x46395f(0x3e7)](this[_0x46395f(0x529)]?VisuMZ[_0x46395f(0x21d)](_0x31c5fb):_0x31c5fb);_0x1029fb>_0x182aca?this[_0x46395f(0x516)](VisuMZ[_0x46395f(0x424)][_0x46395f(0x50d)]['Gold'][_0x46395f(0x3a0)],_0x26d5e5,_0x38b40a,_0x182aca,_0x46395f(0x295)):this[_0x46395f(0x516)](_0x31c5fb,_0x26d5e5,_0x38b40a,_0x182aca,_0x46395f(0x295)),this[_0x46395f(0x382)]();},Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x66a)]=function(_0x46b6d9,_0x4b607a,_0x1535ff,_0x20c0c0,_0x293b28){const _0x3af765=_0x32f06e,_0x2be3b5=ImageManager[_0x3af765(0x32d)](_0x3af765(0x614)),_0x5675e8=ImageManager[_0x3af765(0x36b)],_0x47c4e3=ImageManager[_0x3af765(0x29d)],_0x2b54cd=_0x46b6d9%0x10*_0x5675e8,_0x2413d8=Math[_0x3af765(0x6db)](_0x46b6d9/0x10)*_0x47c4e3,_0x3b5056=_0x20c0c0,_0x3c7067=_0x20c0c0;this['contents'][_0x3af765(0x3c1)][_0x3af765(0x5a1)]=_0x293b28,this[_0x3af765(0x34b)][_0x3af765(0x46f)](_0x2be3b5,_0x2b54cd,_0x2413d8,_0x5675e8,_0x47c4e3,_0x4b607a,_0x1535ff,_0x3b5056,_0x3c7067),this['contents'][_0x3af765(0x3c1)]['imageSmoothingEnabled']=!![];},Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x720)]=function(_0x10799e,_0xd90bd1,_0x49216d,_0x38199c,_0x56cdab,_0x44678f){const _0x3001bd=_0x32f06e,_0x3bf4c2=Math[_0x3001bd(0x6db)]((_0x49216d-0x2)*_0x38199c),_0x1f8c3c=Sprite_Gauge[_0x3001bd(0x76f)][_0x3001bd(0x3f1)][_0x3001bd(0x31a)](this),_0x4e91ae=_0xd90bd1+this[_0x3001bd(0x682)]()-_0x1f8c3c-0x2;this[_0x3001bd(0x34b)][_0x3001bd(0x3a9)](_0x10799e,_0x4e91ae,_0x49216d,_0x1f8c3c,ColorManager['gaugeBackColor']()),this['contents']['gradientFillRect'](_0x10799e+0x1,_0x4e91ae+0x1,_0x3bf4c2,_0x1f8c3c-0x2,_0x56cdab,_0x44678f);},Window_Selectable[_0x32f06e(0x76f)][_0x32f06e(0x29e)]=function(_0x3f3009){const _0x3f3f63=_0x32f06e;let _0x5d789d=this['index']();const _0x181165=this[_0x3f3f63(0x224)](),_0x5a1202=this[_0x3f3f63(0x21c)]();if(this[_0x3f3f63(0x6d8)]()&&(_0x5d789d<_0x181165||_0x3f3009&&_0x5a1202===0x1)){_0x5d789d+=_0x5a1202;if(_0x5d789d>=_0x181165)_0x5d789d=_0x181165-0x1;this[_0x3f3f63(0x352)](_0x5d789d);}else!this[_0x3f3f63(0x6d8)]()&&((_0x5d789d<_0x181165-_0x5a1202||_0x3f3009&&_0x5a1202===0x1)&&this[_0x3f3f63(0x352)]((_0x5d789d+_0x5a1202)%_0x181165));},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x2c1)]=Window_Selectable['prototype'][_0x32f06e(0x29e)],Window_Selectable[_0x32f06e(0x76f)][_0x32f06e(0x29e)]=function(_0x33f44a){const _0x337fb7=_0x32f06e;this['isUseModernControls']()&&_0x33f44a&&this[_0x337fb7(0x21c)]()===0x1&&this[_0x337fb7(0x5bd)]()===this[_0x337fb7(0x224)]()-0x1?this[_0x337fb7(0x352)](0x0):VisuMZ[_0x337fb7(0x424)][_0x337fb7(0x2c1)]['call'](this,_0x33f44a);},Window_Selectable[_0x32f06e(0x76f)]['cursorUp']=function(_0x1634b0){const _0x334a9a=_0x32f06e;let _0x1654c5=Math[_0x334a9a(0x656)](0x0,this[_0x334a9a(0x5bd)]());const _0xa5e45e=this[_0x334a9a(0x224)](),_0x58d0ff=this[_0x334a9a(0x21c)]();if(this[_0x334a9a(0x6d8)]()&&_0x1654c5>0x0||_0x1634b0&&_0x58d0ff===0x1){_0x1654c5-=_0x58d0ff;if(_0x1654c5<=0x0)_0x1654c5=0x0;this[_0x334a9a(0x352)](_0x1654c5);}else!this[_0x334a9a(0x6d8)]()&&((_0x1654c5>=_0x58d0ff||_0x1634b0&&_0x58d0ff===0x1)&&this[_0x334a9a(0x352)]((_0x1654c5-_0x58d0ff+_0xa5e45e)%_0xa5e45e));},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x648)]=Window_Selectable[_0x32f06e(0x76f)][_0x32f06e(0x56b)],Window_Selectable['prototype']['cursorUp']=function(_0x1f5d3f){const _0x17308e=_0x32f06e;this['isUseModernControls']()&&_0x1f5d3f&&this[_0x17308e(0x21c)]()===0x1&&this[_0x17308e(0x5bd)]()===0x0?this[_0x17308e(0x352)](this['maxItems']()-0x1):VisuMZ[_0x17308e(0x424)]['Window_Selectable_cursorUp'][_0x17308e(0x31a)](this,_0x1f5d3f);},Window_Selectable[_0x32f06e(0x76f)][_0x32f06e(0x6d8)]=function(){const _0x5cddb8=_0x32f06e;return VisuMZ[_0x5cddb8(0x424)][_0x5cddb8(0x50d)][_0x5cddb8(0x248)][_0x5cddb8(0x538)];},VisuMZ['CoreEngine'][_0x32f06e(0x2ea)]=Window_Selectable['prototype'][_0x32f06e(0x287)],Window_Selectable[_0x32f06e(0x76f)][_0x32f06e(0x287)]=function(){const _0x451c88=_0x32f06e;this['isUseModernControls']()?(this['processCursorMoveModernControls'](),this[_0x451c88(0x4f3)]()):VisuMZ['CoreEngine'][_0x451c88(0x2ea)][_0x451c88(0x31a)](this);},Window_Selectable[_0x32f06e(0x76f)][_0x32f06e(0x35e)]=function(){return!![];},Window_Selectable[_0x32f06e(0x76f)][_0x32f06e(0x3d4)]=function(){const _0x23b1a0=_0x32f06e;if(this['isCursorMovable']()){const _0x4d0582=this[_0x23b1a0(0x5bd)]();Input[_0x23b1a0(0x675)](_0x23b1a0(0x272))&&(Input[_0x23b1a0(0x750)]('shift')&&this[_0x23b1a0(0x35e)]()?this[_0x23b1a0(0x709)]():this[_0x23b1a0(0x29e)](Input[_0x23b1a0(0x233)](_0x23b1a0(0x272)))),Input[_0x23b1a0(0x675)]('up')&&(Input['isPressed'](_0x23b1a0(0x62e))&&this[_0x23b1a0(0x35e)]()?this['cursorPageup']():this[_0x23b1a0(0x56b)](Input[_0x23b1a0(0x233)]('up'))),Input[_0x23b1a0(0x675)](_0x23b1a0(0x295))&&this[_0x23b1a0(0x65b)](Input[_0x23b1a0(0x233)](_0x23b1a0(0x295))),Input[_0x23b1a0(0x675)](_0x23b1a0(0x3d7))&&this[_0x23b1a0(0x24d)](Input['isTriggered']('left')),!this['isHandled'](_0x23b1a0(0x271))&&Input[_0x23b1a0(0x675)](_0x23b1a0(0x271))&&this[_0x23b1a0(0x709)](),!this[_0x23b1a0(0x4ae)]('pageup')&&Input['isRepeated']('pageup')&&this[_0x23b1a0(0x505)](),this['index']()!==_0x4d0582&&this['playCursorSound']();}},Window_Selectable['prototype'][_0x32f06e(0x4f3)]=function(){const _0x6765e=_0x32f06e;if(this[_0x6765e(0x6f4)]()){const _0x1b0296=this[_0x6765e(0x5bd)]();Input[_0x6765e(0x233)](_0x6765e(0x495))&&this[_0x6765e(0x352)](Math[_0x6765e(0x6ba)](this[_0x6765e(0x5bd)](),0x0)),Input[_0x6765e(0x233)](_0x6765e(0x5ad))&&this[_0x6765e(0x352)](Math[_0x6765e(0x656)](this['index'](),this[_0x6765e(0x224)]()-0x1)),this[_0x6765e(0x5bd)]()!==_0x1b0296&&this['playCursorSound']();}},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x571)]=Window_Selectable[_0x32f06e(0x76f)]['processTouch'],Window_Selectable[_0x32f06e(0x76f)][_0x32f06e(0x6e7)]=function(){const _0x27f2a8=_0x32f06e;this['isUseModernControls']()?this[_0x27f2a8(0x5b7)]():VisuMZ[_0x27f2a8(0x424)][_0x27f2a8(0x571)]['call'](this);},Window_Selectable['prototype'][_0x32f06e(0x5b7)]=function(){const _0x37439d=_0x32f06e;VisuMZ[_0x37439d(0x424)][_0x37439d(0x571)][_0x37439d(0x31a)](this);},Window_Selectable['prototype'][_0x32f06e(0x42e)]=function(){const _0x223545=_0x32f06e;return VisuMZ[_0x223545(0x424)][_0x223545(0x50d)][_0x223545(0x298)][_0x223545(0x63a)];},Window_Selectable[_0x32f06e(0x76f)][_0x32f06e(0x603)]=function(){const _0x30a6a0=_0x32f06e;return VisuMZ['CoreEngine'][_0x30a6a0(0x50d)][_0x30a6a0(0x298)][_0x30a6a0(0x46d)];},Window_Selectable['prototype'][_0x32f06e(0x247)]=function(){const _0x2e2443=_0x32f06e;return Window_Scrollable['prototype'][_0x2e2443(0x247)][_0x2e2443(0x31a)](this)+VisuMZ['CoreEngine'][_0x2e2443(0x50d)][_0x2e2443(0x298)][_0x2e2443(0x5cc)];;},VisuMZ['CoreEngine'][_0x32f06e(0x767)]=Window_Selectable[_0x32f06e(0x76f)][_0x32f06e(0x791)],Window_Selectable[_0x32f06e(0x76f)][_0x32f06e(0x791)]=function(_0x2bc572){const _0x90aad2=_0x32f06e,_0x57de0c=VisuMZ['CoreEngine'][_0x90aad2(0x50d)]['Window'];if(_0x57de0c[_0x90aad2(0x467)]===![])return;_0x57de0c['DrawItemBackgroundJS']?_0x57de0c[_0x90aad2(0x31b)][_0x90aad2(0x31a)](this,_0x2bc572):VisuMZ[_0x90aad2(0x424)]['Window_Selectable_drawBackgroundRect']['call'](this,_0x2bc572);},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x3c3)]=Window_Gold[_0x32f06e(0x76f)][_0x32f06e(0x228)],Window_Gold[_0x32f06e(0x76f)][_0x32f06e(0x228)]=function(){const _0x5ab598=_0x32f06e;this[_0x5ab598(0x478)]()?this[_0x5ab598(0x53d)]():VisuMZ['CoreEngine'][_0x5ab598(0x3c3)][_0x5ab598(0x31a)](this);},Window_Gold[_0x32f06e(0x76f)][_0x32f06e(0x478)]=function(){const _0x2248b6=_0x32f06e;if(TextManager['currencyUnit']!==this[_0x2248b6(0x3be)]())return![];return VisuMZ['CoreEngine'][_0x2248b6(0x50d)][_0x2248b6(0x2c4)][_0x2248b6(0x5cf)];},Window_Gold['prototype']['drawGoldItemStyle']=function(){const _0x4578e1=_0x32f06e;this[_0x4578e1(0x382)](),this[_0x4578e1(0x34b)][_0x4578e1(0x353)](),this['contents'][_0x4578e1(0x33f)]=VisuMZ[_0x4578e1(0x424)]['Settings'][_0x4578e1(0x2c4)][_0x4578e1(0x55b)];const _0x43c526=VisuMZ['CoreEngine'][_0x4578e1(0x50d)][_0x4578e1(0x2c4)]['GoldIcon'],_0x1e50aa=this[_0x4578e1(0x206)](0x0);if(_0x43c526>0x0){const _0x349ec9=_0x1e50aa['y']+(this[_0x4578e1(0x682)]()-ImageManager[_0x4578e1(0x29d)])/0x2;this[_0x4578e1(0x751)](_0x43c526,_0x1e50aa['x'],_0x349ec9);const _0x58e822=ImageManager['iconWidth']+0x4;_0x1e50aa['x']+=_0x58e822,_0x1e50aa[_0x4578e1(0x450)]-=_0x58e822;}this['changeTextColor'](ColorManager[_0x4578e1(0x637)]()),this['drawText'](this[_0x4578e1(0x3be)](),_0x1e50aa['x'],_0x1e50aa['y'],_0x1e50aa[_0x4578e1(0x450)],_0x4578e1(0x3d7));const _0x19a65b=this[_0x4578e1(0x3e7)](this[_0x4578e1(0x3be)]())+0x6;;_0x1e50aa['x']+=_0x19a65b,_0x1e50aa[_0x4578e1(0x450)]-=_0x19a65b,this[_0x4578e1(0x4d3)]();const _0x5191a5=this[_0x4578e1(0x6f7)](),_0x14ed0a=this['textWidth'](this['_digitGrouping']?VisuMZ[_0x4578e1(0x21d)](this[_0x4578e1(0x6f7)]()):this[_0x4578e1(0x6f7)]());_0x14ed0a>_0x1e50aa[_0x4578e1(0x450)]?this['drawText'](VisuMZ[_0x4578e1(0x424)]['Settings']['Gold'][_0x4578e1(0x3a0)],_0x1e50aa['x'],_0x1e50aa['y'],_0x1e50aa[_0x4578e1(0x450)],_0x4578e1(0x295)):this[_0x4578e1(0x516)](this[_0x4578e1(0x6f7)](),_0x1e50aa['x'],_0x1e50aa['y'],_0x1e50aa[_0x4578e1(0x450)],'right'),this[_0x4578e1(0x382)]();},Window_StatusBase[_0x32f06e(0x76f)][_0x32f06e(0x5ec)]=function(_0x3a900d,_0x2f9ffe,_0x4d8cf0,_0xc85fee,_0x461a86){const _0xc3a1d0=_0x32f06e;_0xc85fee=String(_0xc85fee||'')['toUpperCase']();if(VisuMZ[_0xc3a1d0(0x424)]['Settings'][_0xc3a1d0(0x1ea)][_0xc3a1d0(0x792)]){const _0x21cf92=VisuMZ['GetParamIcon'](_0xc85fee);_0x461a86?(this[_0xc3a1d0(0x66a)](_0x21cf92,_0x3a900d,_0x2f9ffe,this['gaugeLineHeight']()),_0x4d8cf0-=this[_0xc3a1d0(0x796)]()+0x2,_0x3a900d+=this['gaugeLineHeight']()+0x2):(this[_0xc3a1d0(0x751)](_0x21cf92,_0x3a900d+0x2,_0x2f9ffe+0x2),_0x4d8cf0-=ImageManager[_0xc3a1d0(0x36b)]+0x4,_0x3a900d+=ImageManager['iconWidth']+0x4);}const _0x3afb2b=TextManager[_0xc3a1d0(0x6bc)](_0xc85fee);this[_0xc3a1d0(0x382)](),this[_0xc3a1d0(0x3cf)](ColorManager['systemColor']()),_0x461a86?(this[_0xc3a1d0(0x34b)]['fontSize']=this[_0xc3a1d0(0x60d)](),this[_0xc3a1d0(0x34b)]['drawText'](_0x3afb2b,_0x3a900d,_0x2f9ffe,_0x4d8cf0,this['gaugeLineHeight'](),_0xc3a1d0(0x3d7))):this[_0xc3a1d0(0x516)](_0x3afb2b,_0x3a900d,_0x2f9ffe,_0x4d8cf0),this[_0xc3a1d0(0x382)]();},Window_StatusBase['prototype'][_0x32f06e(0x60d)]=function(){const _0x3728d2=_0x32f06e;return $gameSystem[_0x3728d2(0x714)]()-0x8;},Window_StatusBase[_0x32f06e(0x76f)][_0x32f06e(0x6e6)]=function(_0x5cba37,_0xddd0da,_0x59baf7,_0x179716){const _0x41b56b=_0x32f06e;_0x179716=_0x179716||0xa8,this[_0x41b56b(0x4d3)]();if(VisuMZ[_0x41b56b(0x424)][_0x41b56b(0x50d)]['UI'][_0x41b56b(0x239)])this[_0x41b56b(0x777)](_0x5cba37[_0x41b56b(0x1ed)]()[_0x41b56b(0x6b9)],_0xddd0da,_0x59baf7,_0x179716);else{const _0x3710d7=_0x5cba37[_0x41b56b(0x1ed)]()['name'][_0x41b56b(0x40a)](/\\I\[(\d+)\]/gi,'');this[_0x41b56b(0x516)](_0x3710d7,_0xddd0da,_0x59baf7,_0x179716);}},Window_StatusBase[_0x32f06e(0x76f)]['drawActorNickname']=function(_0x339683,_0x51aec5,_0x117bf8,_0x528467){const _0x213700=_0x32f06e;_0x528467=_0x528467||0x10e,this[_0x213700(0x4d3)]();if(VisuMZ[_0x213700(0x424)]['Settings']['UI'][_0x213700(0x4a5)])this[_0x213700(0x777)](_0x339683[_0x213700(0x701)](),_0x51aec5,_0x117bf8,_0x528467);else{const _0x4a6a71=_0x339683[_0x213700(0x701)]()[_0x213700(0x40a)](/\\I\[(\d+)\]/gi,'');this[_0x213700(0x516)](_0x339683[_0x213700(0x701)](),_0x51aec5,_0x117bf8,_0x528467);}},VisuMZ[_0x32f06e(0x424)]['Window_StatusBase_drawActorLevel']=Window_StatusBase[_0x32f06e(0x76f)][_0x32f06e(0x3a4)],Window_StatusBase[_0x32f06e(0x76f)][_0x32f06e(0x3a4)]=function(_0x365299,_0x39cce2,_0x5f5af3){const _0x9c0f8e=_0x32f06e;if(this[_0x9c0f8e(0x2c7)]())this[_0x9c0f8e(0x3b1)](_0x365299,_0x39cce2,_0x5f5af3);VisuMZ[_0x9c0f8e(0x424)]['Window_StatusBase_drawActorLevel'][_0x9c0f8e(0x31a)](this,_0x365299,_0x39cce2,_0x5f5af3);},Window_StatusBase[_0x32f06e(0x76f)]['isExpGaugeDrawn']=function(){const _0x3a57bc=_0x32f06e;return VisuMZ[_0x3a57bc(0x424)]['Settings']['UI'][_0x3a57bc(0x62f)];},Window_StatusBase[_0x32f06e(0x76f)][_0x32f06e(0x3b1)]=function(_0x570be5,_0x713cf9,_0x2dade9){const _0x5095c5=_0x32f06e;if(!_0x570be5)return;if(!_0x570be5[_0x5095c5(0x29f)]())return;const _0x4967a1=0x80,_0x11af17=_0x570be5['expRate']();let _0x9db920=ColorManager[_0x5095c5(0x4d9)](),_0x1fffd4=ColorManager[_0x5095c5(0x70c)]();_0x11af17>=0x1&&(_0x9db920=ColorManager[_0x5095c5(0x253)](),_0x1fffd4=ColorManager[_0x5095c5(0x31f)]()),this['drawGauge'](_0x713cf9,_0x2dade9,_0x4967a1,_0x11af17,_0x9db920,_0x1fffd4);},Window_EquipStatus[_0x32f06e(0x76f)][_0x32f06e(0x55e)]=function(){const _0x2da353=_0x32f06e;let _0x42bf4f=0x0;for(const _0x1e01e8 of VisuMZ[_0x2da353(0x424)]['Settings'][_0x2da353(0x1ea)]['DisplayedParams']){const _0x4881e2=this[_0x2da353(0x610)](),_0x43d1c7=this['paramY'](_0x42bf4f);this[_0x2da353(0x60b)](_0x4881e2,_0x43d1c7,_0x1e01e8),_0x42bf4f++;}},Window_EquipStatus['prototype'][_0x32f06e(0x674)]=function(_0x1880d4,_0x5ee906,_0x24226b){const _0x59c6cb=_0x32f06e,_0x24b33b=this[_0x59c6cb(0x568)]()-this['itemPadding']()*0x2;this[_0x59c6cb(0x5ec)](_0x1880d4,_0x5ee906,_0x24b33b,_0x24226b,![]);},Window_EquipStatus[_0x32f06e(0x76f)][_0x32f06e(0x748)]=function(_0x43e6c3,_0x379068,_0x16b7ee){const _0x17d5fa=_0x32f06e,_0x1ba9f2=this[_0x17d5fa(0x629)]();this['resetTextColor'](),this[_0x17d5fa(0x516)](this['_actor']['paramValueByName'](_0x16b7ee,!![]),_0x43e6c3,_0x379068,_0x1ba9f2,'right');},Window_EquipStatus[_0x32f06e(0x76f)][_0x32f06e(0x491)]=function(_0xfb9d94,_0x4b9146){const _0x17726b=_0x32f06e,_0x1da45e=this[_0x17726b(0x37e)]();this[_0x17726b(0x3cf)](ColorManager[_0x17726b(0x637)]());const _0x411433=VisuMZ['CoreEngine'][_0x17726b(0x50d)]['UI'][_0x17726b(0x388)];this[_0x17726b(0x516)](_0x411433,_0xfb9d94,_0x4b9146,_0x1da45e,_0x17726b(0x329));},Window_EquipStatus[_0x32f06e(0x76f)][_0x32f06e(0x346)]=function(_0x59cc63,_0x800c58,_0x508a95){const _0x114258=_0x32f06e,_0x38907c=this[_0x114258(0x629)](),_0x33a34f=this['_tempActor'][_0x114258(0x25f)](_0x508a95),_0x2948e1=_0x33a34f-this[_0x114258(0x338)][_0x114258(0x25f)](_0x508a95);this[_0x114258(0x3cf)](ColorManager['paramchangeTextColor'](_0x2948e1)),this[_0x114258(0x516)](VisuMZ[_0x114258(0x722)](_0x33a34f,0x0,_0x508a95),_0x59cc63,_0x800c58,_0x38907c,_0x114258(0x295));},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x4ee)]=Window_EquipItem[_0x32f06e(0x76f)][_0x32f06e(0x641)],Window_EquipItem[_0x32f06e(0x76f)]['isEnabled']=function(_0x2bafe6){const _0x6c7bfe=_0x32f06e;return _0x2bafe6&&this[_0x6c7bfe(0x338)]?this[_0x6c7bfe(0x338)][_0x6c7bfe(0x335)](_0x2bafe6):VisuMZ[_0x6c7bfe(0x424)]['Window_EquipItem_isEnabled'][_0x6c7bfe(0x31a)](this,_0x2bafe6);},Window_StatusParams[_0x32f06e(0x76f)][_0x32f06e(0x224)]=function(){const _0x4a9127=_0x32f06e;return VisuMZ[_0x4a9127(0x424)][_0x4a9127(0x50d)]['Param'][_0x4a9127(0x316)][_0x4a9127(0x26a)];},Window_StatusParams[_0x32f06e(0x76f)][_0x32f06e(0x60b)]=function(_0x3d7d4a){const _0x56c5df=_0x32f06e,_0x12c604=this[_0x56c5df(0x206)](_0x3d7d4a),_0x588ab6=VisuMZ[_0x56c5df(0x424)][_0x56c5df(0x50d)][_0x56c5df(0x1ea)][_0x56c5df(0x316)][_0x3d7d4a],_0x42d1e2=TextManager[_0x56c5df(0x6bc)](_0x588ab6),_0x1173b3=this[_0x56c5df(0x338)]['paramValueByName'](_0x588ab6,!![]);this[_0x56c5df(0x5ec)](_0x12c604['x'],_0x12c604['y'],0xa0,_0x588ab6,![]),this['resetTextColor'](),this[_0x56c5df(0x516)](_0x1173b3,_0x12c604['x']+0xa0,_0x12c604['y'],0x3c,'right');};if(VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x50d)][_0x32f06e(0x77f)]['EnableNameInput']){VisuMZ['CoreEngine'][_0x32f06e(0x50d)][_0x32f06e(0x77f)][_0x32f06e(0x392)]&&(Window_NameInput[_0x32f06e(0x6ee)]=['Q','W','E','R','T','Y','U','I','O','P','A','S','D','F','G','H','J','K','L','\x27','`','Z','X','C','V','B','N','M',',','.','q','w','e','r','t','y','u','i','o','p','a','s','d','f','g','h','j','k','l',':','~','z','x','c','v','b','n','m','\x22',';','1','2','3','4','5','6','7','8','9','0','!','@','#','$','%','^','&','*','(',')','<','>','[',']','-','_','/','\x20',_0x32f06e(0x4a4),'OK']);;VisuMZ['CoreEngine']['Window_NameInput_initialize']=Window_NameInput[_0x32f06e(0x76f)][_0x32f06e(0x58e)],Window_NameInput[_0x32f06e(0x76f)][_0x32f06e(0x58e)]=function(_0x275301){const _0x57045a=_0x32f06e;this[_0x57045a(0x5e3)]=this[_0x57045a(0x77e)](),VisuMZ['CoreEngine'][_0x57045a(0x592)]['call'](this,_0x275301),Input[_0x57045a(0x353)](),this['deselect']();},Window_NameInput['prototype'][_0x32f06e(0x77e)]=function(){const _0x44ba22=_0x32f06e;return VisuMZ[_0x44ba22(0x424)]['Settings'][_0x44ba22(0x77f)][_0x44ba22(0x36c)]||'keyboard';},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x53c)]=Window_NameInput['prototype'][_0x32f06e(0x6b8)],Window_NameInput[_0x32f06e(0x76f)]['processHandling']=function(){const _0x444fdb=_0x32f06e;if(!this[_0x444fdb(0x545)]())return;if(!this[_0x444fdb(0x1f4)])return;if(Input[_0x444fdb(0x43a)](_0x444fdb(0x3c9)))Input[_0x444fdb(0x353)](),this[_0x444fdb(0x4dc)]();else{if(Input['isTriggered'](_0x444fdb(0x2c5)))Input[_0x444fdb(0x353)](),this[_0x444fdb(0x5e3)]===_0x444fdb(0x4c2)?this[_0x444fdb(0x5ac)](_0x444fdb(0x3fc)):this['switchModes'](_0x444fdb(0x4c2));else{if(this[_0x444fdb(0x5e3)]===_0x444fdb(0x4c2))this['processKeyboardHandling']();else Input[_0x444fdb(0x43a)]('escape')?(Input[_0x444fdb(0x353)](),this[_0x444fdb(0x5ac)]('keyboard')):VisuMZ[_0x444fdb(0x424)][_0x444fdb(0x53c)]['call'](this);}}},VisuMZ[_0x32f06e(0x424)]['Window_NameInput_processTouch']=Window_NameInput['prototype'][_0x32f06e(0x6e7)],Window_NameInput[_0x32f06e(0x76f)][_0x32f06e(0x6e7)]=function(){const _0x3b598d=_0x32f06e;if(!this[_0x3b598d(0x259)]())return;if(this['_mode']===_0x3b598d(0x4c2)){if(TouchInput[_0x3b598d(0x233)]()&&this[_0x3b598d(0x24e)]())this[_0x3b598d(0x5ac)]('default');else TouchInput[_0x3b598d(0x755)]()&&this[_0x3b598d(0x5ac)](_0x3b598d(0x3fc));}else VisuMZ[_0x3b598d(0x424)]['Window_NameInput_processTouch'][_0x3b598d(0x31a)](this);},Window_NameInput[_0x32f06e(0x76f)]['processKeyboardHandling']=function(){const _0x1ec026=_0x32f06e;if(Input[_0x1ec026(0x43a)](_0x1ec026(0x2d8)))Input[_0x1ec026(0x353)](),this[_0x1ec026(0x741)]();else{if(Input['_inputString']!==undefined){let _0x40fb24=Input['_inputString'],_0x57975b=_0x40fb24[_0x1ec026(0x26a)];for(let _0x38dab1=0x0;_0x38dab1<_0x57975b;++_0x38dab1){this[_0x1ec026(0x216)]['add'](_0x40fb24[_0x38dab1])?SoundManager[_0x1ec026(0x46b)]():SoundManager[_0x1ec026(0x55d)]();}Input['clear']();}}},Window_NameInput[_0x32f06e(0x76f)][_0x32f06e(0x5ac)]=function(_0x1533c7){const _0xfed000=_0x32f06e;let _0x31861e=this[_0xfed000(0x5e3)];this[_0xfed000(0x5e3)]=_0x1533c7,_0x31861e!==this[_0xfed000(0x5e3)]&&(this[_0xfed000(0x228)](),SoundManager[_0xfed000(0x46b)](),this['_mode']===_0xfed000(0x3fc)?this[_0xfed000(0x5de)](0x0):this[_0xfed000(0x5de)](-0x1));},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x344)]=Window_NameInput[_0x32f06e(0x76f)][_0x32f06e(0x29e)],Window_NameInput['prototype'][_0x32f06e(0x29e)]=function(_0x540c40){const _0x5f57a3=_0x32f06e;if(this['_mode']==='keyboard'&&!Input[_0x5f57a3(0x4c4)]())return;if(Input[_0x5f57a3(0x4a2)]())return;VisuMZ[_0x5f57a3(0x424)]['Window_NameInput_cursorDown'][_0x5f57a3(0x31a)](this,_0x540c40),this[_0x5f57a3(0x5ac)](_0x5f57a3(0x3fc));},VisuMZ[_0x32f06e(0x424)]['Window_NameInput_cursorUp']=Window_NameInput[_0x32f06e(0x76f)][_0x32f06e(0x56b)],Window_NameInput[_0x32f06e(0x76f)][_0x32f06e(0x56b)]=function(_0xaae32d){const _0xf11dfd=_0x32f06e;if(this[_0xf11dfd(0x5e3)]===_0xf11dfd(0x4c2)&&!Input[_0xf11dfd(0x4c4)]())return;if(Input['isNumpadPressed']())return;VisuMZ['CoreEngine'][_0xf11dfd(0x374)][_0xf11dfd(0x31a)](this,_0xaae32d),this['switchModes']('default');},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x3b3)]=Window_NameInput[_0x32f06e(0x76f)][_0x32f06e(0x65b)],Window_NameInput[_0x32f06e(0x76f)]['cursorRight']=function(_0x346bf3){const _0x46be36=_0x32f06e;if(this[_0x46be36(0x5e3)]===_0x46be36(0x4c2)&&!Input['isArrowPressed']())return;if(Input['isNumpadPressed']())return;VisuMZ['CoreEngine']['Window_NameInput_cursorRight'][_0x46be36(0x31a)](this,_0x346bf3),this[_0x46be36(0x5ac)]('default');},VisuMZ['CoreEngine'][_0x32f06e(0x386)]=Window_NameInput[_0x32f06e(0x76f)][_0x32f06e(0x24d)],Window_NameInput[_0x32f06e(0x76f)]['cursorLeft']=function(_0x28ce12){const _0x3b0350=_0x32f06e;if(this['_mode']===_0x3b0350(0x4c2)&&!Input[_0x3b0350(0x4c4)]())return;if(Input[_0x3b0350(0x4a2)]())return;VisuMZ[_0x3b0350(0x424)][_0x3b0350(0x386)][_0x3b0350(0x31a)](this,_0x28ce12),this['switchModes'](_0x3b0350(0x3fc));},VisuMZ['CoreEngine'][_0x32f06e(0x68a)]=Window_NameInput['prototype'][_0x32f06e(0x709)],Window_NameInput['prototype'][_0x32f06e(0x709)]=function(){const _0x346f19=_0x32f06e;if(this[_0x346f19(0x5e3)]===_0x346f19(0x4c2))return;if(Input[_0x346f19(0x4a2)]())return;VisuMZ['CoreEngine']['Window_NameInput_cursorPagedown'][_0x346f19(0x31a)](this),this['switchModes'](_0x346f19(0x3fc));},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x71f)]=Window_NameInput['prototype'][_0x32f06e(0x505)],Window_NameInput[_0x32f06e(0x76f)][_0x32f06e(0x505)]=function(){const _0x32915b=_0x32f06e;if(this[_0x32915b(0x5e3)]===_0x32915b(0x4c2))return;if(Input[_0x32915b(0x4a2)]())return;VisuMZ[_0x32915b(0x424)][_0x32915b(0x71f)][_0x32915b(0x31a)](this),this[_0x32915b(0x5ac)](_0x32915b(0x3fc));},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x69a)]=Window_NameInput[_0x32f06e(0x76f)][_0x32f06e(0x228)],Window_NameInput[_0x32f06e(0x76f)]['refresh']=function(){const _0x3a0f3c=_0x32f06e;if(this['_mode']===_0x3a0f3c(0x4c2)){this[_0x3a0f3c(0x34b)][_0x3a0f3c(0x353)](),this[_0x3a0f3c(0x752)]['clear'](),this[_0x3a0f3c(0x4d3)]();let _0x24fb0a=VisuMZ[_0x3a0f3c(0x424)]['Settings']['KeyboardInput'][_0x3a0f3c(0x542)][_0x3a0f3c(0x590)]('\x0a'),_0x17ea60=_0x24fb0a['length'],_0x2b3297=(this[_0x3a0f3c(0x54a)]-_0x17ea60*this['lineHeight']())/0x2;for(let _0x16976f=0x0;_0x16976f<_0x17ea60;++_0x16976f){let _0x20975f=_0x24fb0a[_0x16976f],_0x1ade1f=this[_0x3a0f3c(0x4ba)](_0x20975f)[_0x3a0f3c(0x450)],_0x1b71a6=Math['floor']((this[_0x3a0f3c(0x34b)][_0x3a0f3c(0x450)]-_0x1ade1f)/0x2);this[_0x3a0f3c(0x777)](_0x20975f,_0x1b71a6,_0x2b3297),_0x2b3297+=this[_0x3a0f3c(0x682)]();}}else VisuMZ[_0x3a0f3c(0x424)][_0x3a0f3c(0x69a)]['call'](this);};};VisuMZ[_0x32f06e(0x424)]['Window_ShopSell_isEnabled']=Window_ShopSell['prototype'][_0x32f06e(0x641)],Window_ShopSell[_0x32f06e(0x76f)][_0x32f06e(0x641)]=function(_0x41bafe){const _0x2c7bfe=_0x32f06e;return VisuMZ[_0x2c7bfe(0x424)][_0x2c7bfe(0x50d)][_0x2c7bfe(0x248)][_0x2c7bfe(0x499)]&&DataManager['isKeyItem'](_0x41bafe)?![]:VisuMZ['CoreEngine'][_0x2c7bfe(0x64e)]['call'](this,_0x41bafe);},Window_NumberInput[_0x32f06e(0x76f)][_0x32f06e(0x6d8)]=function(){return![];};VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x50d)][_0x32f06e(0x77f)]['EnableNumberInput']&&(VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x678)]=Window_NumberInput[_0x32f06e(0x76f)][_0x32f06e(0x20d)],Window_NumberInput[_0x32f06e(0x76f)][_0x32f06e(0x20d)]=function(){const _0x11afd2=_0x32f06e;VisuMZ[_0x11afd2(0x424)][_0x11afd2(0x678)]['call'](this),this[_0x11afd2(0x5de)](this[_0x11afd2(0x62c)]-0x1);},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x53f)]=Window_NumberInput['prototype'][_0x32f06e(0x503)],Window_NumberInput['prototype'][_0x32f06e(0x503)]=function(){const _0x1fb23f=_0x32f06e;if(!this['isOpenAndActive']())return;if(Input[_0x1fb23f(0x4a2)]())this['processKeyboardDigitChange']();else{if(Input['isSpecialCode'](_0x1fb23f(0x3c9)))this['processKeyboardBackspace']();else{if(Input['_inputSpecialKeyCode']===0x2e)this[_0x1fb23f(0x3ea)]();else{if(Input[_0x1fb23f(0x72e)]===0x24)this[_0x1fb23f(0x436)]();else Input[_0x1fb23f(0x72e)]===0x23?this['processKeyboardEnd']():(VisuMZ[_0x1fb23f(0x424)][_0x1fb23f(0x53f)]['call'](this),Input['clear']());}}}},Window_NumberInput[_0x32f06e(0x76f)][_0x32f06e(0x287)]=function(){const _0x20b6a3=_0x32f06e;if(!this[_0x20b6a3(0x6f4)]())return;Input[_0x20b6a3(0x4a2)]()?this[_0x20b6a3(0x3a1)]():Window_Selectable[_0x20b6a3(0x76f)][_0x20b6a3(0x287)]['call'](this);},Window_NumberInput[_0x32f06e(0x76f)][_0x32f06e(0x4f3)]=function(){},Window_NumberInput['prototype'][_0x32f06e(0x3a1)]=function(){const _0x31ed73=_0x32f06e;if(String(this['_number'])[_0x31ed73(0x26a)]>=this[_0x31ed73(0x62c)])return;this['_number']=Number(String(this['_number'])+Input[_0x31ed73(0x2da)]);const _0xbcd564='9'[_0x31ed73(0x6b1)](this[_0x31ed73(0x62c)]);this[_0x31ed73(0x375)]=this[_0x31ed73(0x375)]['clamp'](0x0,_0xbcd564),Input[_0x31ed73(0x353)](),this[_0x31ed73(0x228)](),SoundManager[_0x31ed73(0x1fb)](),this['select'](this[_0x31ed73(0x62c)]-0x1);},Window_NumberInput[_0x32f06e(0x76f)][_0x32f06e(0x460)]=function(){const _0x4293d0=_0x32f06e;this['_number']=Number(String(this[_0x4293d0(0x375)])[_0x4293d0(0x551)](0x0,-0x1)),this['_number']=Math['max'](0x0,this['_number']),Input['clear'](),this['refresh'](),SoundManager[_0x4293d0(0x1fb)](),this[_0x4293d0(0x5de)](this[_0x4293d0(0x62c)]-0x1);},Window_NumberInput['prototype'][_0x32f06e(0x3ea)]=function(){const _0x58ebe9=_0x32f06e;this[_0x58ebe9(0x375)]=Number(String(this[_0x58ebe9(0x375)])['substring'](0x1)),this[_0x58ebe9(0x375)]=Math['max'](0x0,this['_number']),Input['clear'](),this['refresh'](),SoundManager[_0x58ebe9(0x1fb)](),this[_0x58ebe9(0x5de)](this[_0x58ebe9(0x62c)]-0x1);});;Window_TitleCommand[_0x32f06e(0x2f5)]=VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x50d)][_0x32f06e(0x211)],Window_TitleCommand['prototype'][_0x32f06e(0x418)]=function(){const _0x2a0307=_0x32f06e;this[_0x2a0307(0x330)]();},Window_TitleCommand[_0x32f06e(0x76f)]['makeCoreEngineCommandList']=function(){const _0x4ada85=_0x32f06e;for(const _0x280930 of Window_TitleCommand[_0x4ada85(0x2f5)]){if(_0x280930[_0x4ada85(0x1e5)][_0x4ada85(0x31a)](this)){const _0x4ed68e=_0x280930[_0x4ada85(0x596)];let _0x5674f8=_0x280930[_0x4ada85(0x44b)];if(['','Untitled']['includes'](_0x5674f8))_0x5674f8=_0x280930['TextJS']['call'](this);const _0x4d4399=_0x280930[_0x4ada85(0x401)]['call'](this),_0x37ee50=_0x280930[_0x4ada85(0x60e)][_0x4ada85(0x31a)](this);this[_0x4ada85(0x245)](_0x5674f8,_0x4ed68e,_0x4d4399,_0x37ee50),this[_0x4ada85(0x531)](_0x4ed68e,_0x280930[_0x4ada85(0x416)][_0x4ada85(0x3da)](this,_0x37ee50));}}},Window_GameEnd['_commandList']=VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x50d)][_0x32f06e(0x59b)][_0x32f06e(0x5ff)][_0x32f06e(0x4a7)],Window_GameEnd[_0x32f06e(0x76f)][_0x32f06e(0x418)]=function(){const _0xc0aff5=_0x32f06e;this[_0xc0aff5(0x330)]();},Window_GameEnd[_0x32f06e(0x76f)][_0x32f06e(0x330)]=function(){const _0x2fccbf=_0x32f06e;for(const _0x2f191e of Window_GameEnd[_0x2fccbf(0x2f5)]){if(_0x2f191e['ShowJS'][_0x2fccbf(0x31a)](this)){const _0x216457=_0x2f191e[_0x2fccbf(0x596)];let _0x58d301=_0x2f191e[_0x2fccbf(0x44b)];if(['',_0x2fccbf(0x632)]['includes'](_0x58d301))_0x58d301=_0x2f191e[_0x2fccbf(0x387)][_0x2fccbf(0x31a)](this);const _0x74c331=_0x2f191e[_0x2fccbf(0x401)]['call'](this),_0x2a55ab=_0x2f191e[_0x2fccbf(0x60e)][_0x2fccbf(0x31a)](this);this['addCommand'](_0x58d301,_0x216457,_0x74c331,_0x2a55ab),this['setHandler'](_0x216457,_0x2f191e['CallHandlerJS'][_0x2fccbf(0x3da)](this,_0x2a55ab));}}};function Window_ButtonAssist(){const _0xe56956=_0x32f06e;this[_0xe56956(0x58e)](...arguments);}Window_ButtonAssist[_0x32f06e(0x76f)]=Object[_0x32f06e(0x33a)](Window_Base['prototype']),Window_ButtonAssist[_0x32f06e(0x76f)]['constructor']=Window_ButtonAssist,Window_ButtonAssist[_0x32f06e(0x76f)]['initialize']=function(_0x29aa7b){const _0x3ed680=_0x32f06e;this[_0x3ed680(0x390)]={},Window_Base['prototype'][_0x3ed680(0x58e)]['call'](this,_0x29aa7b),this['setBackgroundType'](VisuMZ[_0x3ed680(0x424)]['Settings']['ButtonAssist'][_0x3ed680(0x660)]||0x0),this[_0x3ed680(0x228)]();},Window_ButtonAssist[_0x32f06e(0x76f)]['makeFontBigger']=function(){const _0x5d0a05=_0x32f06e;this['contents'][_0x5d0a05(0x33f)]<=0x60&&(this['contents']['fontSize']+=0x6);},Window_ButtonAssist[_0x32f06e(0x76f)][_0x32f06e(0x563)]=function(){const _0x20986c=_0x32f06e;this[_0x20986c(0x34b)][_0x20986c(0x33f)]>=0x18&&(this['contents'][_0x20986c(0x33f)]-=0x6);},Window_ButtonAssist['prototype'][_0x32f06e(0x209)]=function(){const _0x5ee415=_0x32f06e;Window_Base[_0x5ee415(0x76f)][_0x5ee415(0x209)][_0x5ee415(0x31a)](this),this[_0x5ee415(0x68f)]();},Window_ButtonAssist[_0x32f06e(0x76f)][_0x32f06e(0x38a)]=function(){const _0x1385e4=_0x32f06e;this[_0x1385e4(0x4f0)]=SceneManager[_0x1385e4(0x269)]['getButtonAssistLocation']()!==_0x1385e4(0x3ff)?0x0:0x8;},Window_ButtonAssist[_0x32f06e(0x76f)]['updateKeyText']=function(){const _0x1e9af8=_0x32f06e,_0x7eb021=SceneManager[_0x1e9af8(0x269)];for(let _0x212b4e=0x1;_0x212b4e<=0x5;_0x212b4e++){if(this[_0x1e9af8(0x390)]['key%1'[_0x1e9af8(0x262)](_0x212b4e)]!==_0x7eb021[_0x1e9af8(0x3f9)['format'](_0x212b4e)]())return this[_0x1e9af8(0x228)]();if(this['_data'][_0x1e9af8(0x66d)['format'](_0x212b4e)]!==_0x7eb021[_0x1e9af8(0x20b)[_0x1e9af8(0x262)](_0x212b4e)]())return this['refresh']();}},Window_ButtonAssist['prototype'][_0x32f06e(0x228)]=function(){const _0x31df21=_0x32f06e;this[_0x31df21(0x34b)][_0x31df21(0x353)]();for(let _0x50be89=0x1;_0x50be89<=0x5;_0x50be89++){this['drawSegment'](_0x50be89);}},Window_ButtonAssist[_0x32f06e(0x76f)][_0x32f06e(0x672)]=function(_0x1387bd){const _0x1173d9=_0x32f06e,_0x3b345a=this[_0x1173d9(0x5c3)]/0x5,_0x214cfa=SceneManager[_0x1173d9(0x269)],_0x5548a8=_0x214cfa['buttonAssistKey%1'[_0x1173d9(0x262)](_0x1387bd)](),_0x549fe4=_0x214cfa['buttonAssistText%1'[_0x1173d9(0x262)](_0x1387bd)]();this[_0x1173d9(0x390)][_0x1173d9(0x4aa)['format'](_0x1387bd)]=_0x5548a8,this['_data'][_0x1173d9(0x66d)[_0x1173d9(0x262)](_0x1387bd)]=_0x549fe4;if(_0x5548a8==='')return;if(_0x549fe4==='')return;const _0x3af8ce=_0x214cfa[_0x1173d9(0x4ed)[_0x1173d9(0x262)](_0x1387bd)](),_0x13fec5=this[_0x1173d9(0x610)](),_0x3e7bdf=_0x3b345a*(_0x1387bd-0x1)+_0x13fec5+_0x3af8ce,_0x34b422=VisuMZ[_0x1173d9(0x424)][_0x1173d9(0x50d)][_0x1173d9(0x673)][_0x1173d9(0x530)];this[_0x1173d9(0x777)](_0x34b422['format'](_0x5548a8,_0x549fe4),_0x3e7bdf,0x0,_0x3b345a-_0x13fec5*0x2);},VisuMZ[_0x32f06e(0x20f)]=function(_0x5e0a55){const _0xe80a79=_0x32f06e;if(Utils[_0xe80a79(0x2d5)](_0xe80a79(0x515))){var _0x39e181=require(_0xe80a79(0x278))[_0xe80a79(0x298)][_0xe80a79(0x59d)]();SceneManager[_0xe80a79(0x4be)]();if(_0x5e0a55)setTimeout(_0x39e181['focus'][_0xe80a79(0x3da)](_0x39e181),0x190);}},VisuMZ[_0x32f06e(0x5d5)]=function(_0x2188e8,_0x1b1b11){const _0x53cb88=_0x32f06e;_0x1b1b11=_0x1b1b11[_0x53cb88(0x79b)]();var _0x226710=1.70158,_0x95e238=0.7;switch(_0x1b1b11){case _0x53cb88(0x3bf):return _0x2188e8;case _0x53cb88(0x361):return-0x1*Math['cos'](_0x2188e8*(Math['PI']/0x2))+0x1;case _0x53cb88(0x591):return Math['sin'](_0x2188e8*(Math['PI']/0x2));case _0x53cb88(0x292):return-0.5*(Math['cos'](Math['PI']*_0x2188e8)-0x1);case _0x53cb88(0x689):return _0x2188e8*_0x2188e8;case _0x53cb88(0x241):return _0x2188e8*(0x2-_0x2188e8);case _0x53cb88(0x697):return _0x2188e8<0.5?0x2*_0x2188e8*_0x2188e8:-0x1+(0x4-0x2*_0x2188e8)*_0x2188e8;case _0x53cb88(0x6b0):return _0x2188e8*_0x2188e8*_0x2188e8;case'OUTCUBIC':var _0x18d097=_0x2188e8-0x1;return _0x18d097*_0x18d097*_0x18d097+0x1;case'INOUTCUBIC':return _0x2188e8<0.5?0x4*_0x2188e8*_0x2188e8*_0x2188e8:(_0x2188e8-0x1)*(0x2*_0x2188e8-0x2)*(0x2*_0x2188e8-0x2)+0x1;case _0x53cb88(0x23a):return _0x2188e8*_0x2188e8*_0x2188e8*_0x2188e8;case _0x53cb88(0x74d):var _0x18d097=_0x2188e8-0x1;return 0x1-_0x18d097*_0x18d097*_0x18d097*_0x18d097;case'INOUTQUART':var _0x18d097=_0x2188e8-0x1;return _0x2188e8<0.5?0x8*_0x2188e8*_0x2188e8*_0x2188e8*_0x2188e8:0x1-0x8*_0x18d097*_0x18d097*_0x18d097*_0x18d097;case _0x53cb88(0x5f4):return _0x2188e8*_0x2188e8*_0x2188e8*_0x2188e8*_0x2188e8;case'OUTQUINT':var _0x18d097=_0x2188e8-0x1;return 0x1+_0x18d097*_0x18d097*_0x18d097*_0x18d097*_0x18d097;case _0x53cb88(0x647):var _0x18d097=_0x2188e8-0x1;return _0x2188e8<0.5?0x10*_0x2188e8*_0x2188e8*_0x2188e8*_0x2188e8*_0x2188e8:0x1+0x10*_0x18d097*_0x18d097*_0x18d097*_0x18d097*_0x18d097;case _0x53cb88(0x657):if(_0x2188e8===0x0)return 0x0;return Math[_0x53cb88(0x354)](0x2,0xa*(_0x2188e8-0x1));case _0x53cb88(0x37b):if(_0x2188e8===0x1)return 0x1;return-Math[_0x53cb88(0x354)](0x2,-0xa*_0x2188e8)+0x1;case _0x53cb88(0x476):if(_0x2188e8===0x0||_0x2188e8===0x1)return _0x2188e8;var _0x466684=_0x2188e8*0x2,_0x4cefa1=_0x466684-0x1;if(_0x466684<0x1)return 0.5*Math[_0x53cb88(0x354)](0x2,0xa*_0x4cefa1);return 0.5*(-Math[_0x53cb88(0x354)](0x2,-0xa*_0x4cefa1)+0x2);case _0x53cb88(0x38c):var _0x466684=_0x2188e8/0x1;return-0x1*(Math[_0x53cb88(0x76d)](0x1-_0x466684*_0x2188e8)-0x1);case _0x53cb88(0x681):var _0x18d097=_0x2188e8-0x1;return Math[_0x53cb88(0x76d)](0x1-_0x18d097*_0x18d097);case _0x53cb88(0x47d):var _0x466684=_0x2188e8*0x2,_0x4cefa1=_0x466684-0x2;if(_0x466684<0x1)return-0.5*(Math[_0x53cb88(0x76d)](0x1-_0x466684*_0x466684)-0x1);return 0.5*(Math[_0x53cb88(0x76d)](0x1-_0x4cefa1*_0x4cefa1)+0x1);case _0x53cb88(0x536):return _0x2188e8*_0x2188e8*((_0x226710+0x1)*_0x2188e8-_0x226710);case _0x53cb88(0x3ee):var _0x466684=_0x2188e8/0x1-0x1;return _0x466684*_0x466684*((_0x226710+0x1)*_0x466684+_0x226710)+0x1;break;case _0x53cb88(0x691):var _0x466684=_0x2188e8*0x2,_0x1bf1c8=_0x466684-0x2,_0x1ccedc=_0x226710*1.525;if(_0x466684<0x1)return 0.5*_0x466684*_0x466684*((_0x1ccedc+0x1)*_0x466684-_0x1ccedc);return 0.5*(_0x1bf1c8*_0x1bf1c8*((_0x1ccedc+0x1)*_0x1bf1c8+_0x1ccedc)+0x2);case _0x53cb88(0x1e7):if(_0x2188e8===0x0||_0x2188e8===0x1)return _0x2188e8;var _0x466684=_0x2188e8/0x1,_0x4cefa1=_0x466684-0x1,_0x469fde=0x1-_0x95e238,_0x1ccedc=_0x469fde/(0x2*Math['PI'])*Math['asin'](0x1);return-(Math[_0x53cb88(0x354)](0x2,0xa*_0x4cefa1)*Math[_0x53cb88(0x2dd)]((_0x4cefa1-_0x1ccedc)*(0x2*Math['PI'])/_0x469fde));case _0x53cb88(0x6c0):var _0x469fde=0x1-_0x95e238,_0x466684=_0x2188e8*0x2;if(_0x2188e8===0x0||_0x2188e8===0x1)return _0x2188e8;var _0x1ccedc=_0x469fde/(0x2*Math['PI'])*Math[_0x53cb88(0x520)](0x1);return Math['pow'](0x2,-0xa*_0x466684)*Math['sin']((_0x466684-_0x1ccedc)*(0x2*Math['PI'])/_0x469fde)+0x1;case _0x53cb88(0x68c):var _0x469fde=0x1-_0x95e238;if(_0x2188e8===0x0||_0x2188e8===0x1)return _0x2188e8;var _0x466684=_0x2188e8*0x2,_0x4cefa1=_0x466684-0x1,_0x1ccedc=_0x469fde/(0x2*Math['PI'])*Math[_0x53cb88(0x520)](0x1);if(_0x466684<0x1)return-0.5*(Math['pow'](0x2,0xa*_0x4cefa1)*Math['sin']((_0x4cefa1-_0x1ccedc)*(0x2*Math['PI'])/_0x469fde));return Math['pow'](0x2,-0xa*_0x4cefa1)*Math[_0x53cb88(0x2dd)]((_0x4cefa1-_0x1ccedc)*(0x2*Math['PI'])/_0x469fde)*0.5+0x1;case _0x53cb88(0x67e):var _0x466684=_0x2188e8/0x1;if(_0x466684<0x1/2.75)return 7.5625*_0x466684*_0x466684;else{if(_0x466684<0x2/2.75){var _0x1bf1c8=_0x466684-1.5/2.75;return 7.5625*_0x1bf1c8*_0x1bf1c8+0.75;}else{if(_0x466684<2.5/2.75){var _0x1bf1c8=_0x466684-2.25/2.75;return 7.5625*_0x1bf1c8*_0x1bf1c8+0.9375;}else{var _0x1bf1c8=_0x466684-2.625/2.75;return 7.5625*_0x1bf1c8*_0x1bf1c8+0.984375;}}}case'INBOUNCE':var _0x3b916=0x1-VisuMZ[_0x53cb88(0x5d5)](0x1-_0x2188e8,_0x53cb88(0x3ed));return _0x3b916;case _0x53cb88(0x766):if(_0x2188e8<0.5)var _0x3b916=VisuMZ[_0x53cb88(0x5d5)](_0x2188e8*0x2,_0x53cb88(0x517))*0.5;else var _0x3b916=VisuMZ['ApplyEasing'](_0x2188e8*0x2-0x1,'outbounce')*0.5+0.5;return _0x3b916;default:return _0x2188e8;}},VisuMZ[_0x32f06e(0x2d1)]=function(_0x286218){const _0x45ee03=_0x32f06e;_0x286218=String(_0x286218)[_0x45ee03(0x79b)]();const _0xe5aa62=VisuMZ[_0x45ee03(0x424)]['Settings'][_0x45ee03(0x1ea)];if(_0x286218==='MAXHP')return _0xe5aa62[_0x45ee03(0x3b7)];if(_0x286218===_0x45ee03(0x6d7))return _0xe5aa62['IconParam1'];if(_0x286218===_0x45ee03(0x533))return _0xe5aa62[_0x45ee03(0x661)];if(_0x286218===_0x45ee03(0x213))return _0xe5aa62['IconParam3'];if(_0x286218===_0x45ee03(0x730))return _0xe5aa62[_0x45ee03(0x546)];if(_0x286218===_0x45ee03(0x317))return _0xe5aa62[_0x45ee03(0x5d7)];if(_0x286218===_0x45ee03(0x321))return _0xe5aa62[_0x45ee03(0x2c6)];if(_0x286218==='LUK')return _0xe5aa62[_0x45ee03(0x4e5)];if(_0x286218===_0x45ee03(0x612))return _0xe5aa62[_0x45ee03(0x6e3)];if(_0x286218===_0x45ee03(0x4cb))return _0xe5aa62[_0x45ee03(0x3bd)];if(_0x286218===_0x45ee03(0x4ce))return _0xe5aa62[_0x45ee03(0x2a8)];if(_0x286218===_0x45ee03(0x445))return _0xe5aa62[_0x45ee03(0x4b7)];if(_0x286218===_0x45ee03(0x51b))return _0xe5aa62[_0x45ee03(0x24a)];if(_0x286218===_0x45ee03(0x6eb))return _0xe5aa62[_0x45ee03(0x5fc)];if(_0x286218==='CNT')return _0xe5aa62[_0x45ee03(0x788)];if(_0x286218===_0x45ee03(0x580))return _0xe5aa62[_0x45ee03(0x4c9)];if(_0x286218===_0x45ee03(0x76c))return _0xe5aa62[_0x45ee03(0x780)];if(_0x286218==='TRG')return _0xe5aa62['IconXParam9'];if(_0x286218===_0x45ee03(0x1ee))return _0xe5aa62[_0x45ee03(0x2e3)];if(_0x286218===_0x45ee03(0x48d))return _0xe5aa62['IconSParam1'];if(_0x286218==='REC')return _0xe5aa62['IconSParam2'];if(_0x286218===_0x45ee03(0x74f))return _0xe5aa62[_0x45ee03(0x468)];if(_0x286218==='MCR')return _0xe5aa62[_0x45ee03(0x5e8)];if(_0x286218===_0x45ee03(0x439))return _0xe5aa62[_0x45ee03(0x1d7)];if(_0x286218===_0x45ee03(0x676))return _0xe5aa62['IconSParam6'];if(_0x286218===_0x45ee03(0x49a))return _0xe5aa62['IconSParam7'];if(_0x286218===_0x45ee03(0x342))return _0xe5aa62[_0x45ee03(0x5bc)];if(_0x286218===_0x45ee03(0x45b))return _0xe5aa62['IconSParam9'];if(VisuMZ[_0x45ee03(0x424)]['CustomParamIcons'][_0x286218])return VisuMZ[_0x45ee03(0x424)][_0x45ee03(0x493)][_0x286218]||0x0;return 0x0;},VisuMZ[_0x32f06e(0x722)]=function(_0x35b564,_0x5f0959,_0x10208b){const _0x5e7c5f=_0x32f06e;if(_0x10208b===undefined&&_0x35b564%0x1===0x0)return _0x35b564;if(_0x10208b!==undefined&&[_0x5e7c5f(0x309),_0x5e7c5f(0x6d7),_0x5e7c5f(0x533),'DEF',_0x5e7c5f(0x730),_0x5e7c5f(0x317),_0x5e7c5f(0x321),'LUK'][_0x5e7c5f(0x77d)](String(_0x10208b)[_0x5e7c5f(0x79b)]()['trim']()))return _0x35b564;return _0x5f0959=_0x5f0959||0x0,String((_0x35b564*0x64)['toFixed'](_0x5f0959))+'%';},VisuMZ[_0x32f06e(0x21d)]=function(_0x131cf4){const _0xaf56f1=_0x32f06e;_0x131cf4=String(_0x131cf4);if(!_0x131cf4)return _0x131cf4;if(typeof _0x131cf4!==_0xaf56f1(0x451))return _0x131cf4;const _0x4df1a5=VisuMZ[_0xaf56f1(0x424)][_0xaf56f1(0x50d)][_0xaf56f1(0x248)][_0xaf56f1(0x72f)]||_0xaf56f1(0x5b0),_0x5d9388={'maximumFractionDigits':0x6};_0x131cf4=_0x131cf4['replace'](/\[(.*?)\]/g,(_0x216a3d,_0x21c67f)=>{const _0x59dd9f=_0xaf56f1;return VisuMZ[_0x59dd9f(0x4bd)](_0x21c67f,'[',']');}),_0x131cf4=_0x131cf4[_0xaf56f1(0x40a)](/<(.*?)>/g,(_0x1582e3,_0x5ee8e3)=>{const _0x5876fd=_0xaf56f1;return VisuMZ[_0x5876fd(0x4bd)](_0x5ee8e3,'<','>');}),_0x131cf4=_0x131cf4['replace'](/\{\{(.*?)\}\}/g,(_0x4966f9,_0x587134)=>{const _0x29168b=_0xaf56f1;return VisuMZ[_0x29168b(0x4bd)](_0x587134,'','');}),_0x131cf4=_0x131cf4[_0xaf56f1(0x40a)](/(\d+\.?\d*)/g,(_0x485e3d,_0x4b097a)=>{const _0x191368=_0xaf56f1;let _0x593774=_0x4b097a;if(_0x593774[0x0]==='0')return _0x593774;if(_0x593774[_0x593774['length']-0x1]==='.')return Number(_0x593774)['toLocaleString'](_0x4df1a5,_0x5d9388)+'.';else return _0x593774[_0x593774['length']-0x1]===','?Number(_0x593774)[_0x191368(0x3aa)](_0x4df1a5,_0x5d9388)+',':Number(_0x593774)['toLocaleString'](_0x4df1a5,_0x5d9388);});let _0x467e89=0x3;while(_0x467e89--){_0x131cf4=VisuMZ[_0xaf56f1(0x496)](_0x131cf4);}return _0x131cf4;},VisuMZ['PreserveNumbers']=function(_0x546301,_0x149771,_0x1d2cc2){const _0x1258dc=_0x32f06e;return _0x546301=_0x546301[_0x1258dc(0x40a)](/(\d)/gi,(_0x4a50d2,_0x32a4d1)=>_0x1258dc(0x24f)['format'](Number(_0x32a4d1))),_0x1258dc(0x3a7)[_0x1258dc(0x262)](_0x546301,_0x149771,_0x1d2cc2);},VisuMZ[_0x32f06e(0x496)]=function(_0x2823ef){return _0x2823ef=_0x2823ef['replace'](/PRESERVCONVERSION\((\d+)\)/gi,(_0x37cb7b,_0x24982c)=>Number(parseInt(_0x24982c))),_0x2823ef;},VisuMZ[_0x32f06e(0x622)]=function(_0x20511c){const _0x459887=_0x32f06e;SoundManager['playOk']();if(!Utils[_0x459887(0x585)]()){const _0x1f62fd=window[_0x459887(0x207)](_0x20511c,_0x459887(0x771));}else{const _0x2bcc54=process[_0x459887(0x65e)]==_0x459887(0x40f)?_0x459887(0x207):process[_0x459887(0x65e)]=='win32'?'start':'xdg-open';require(_0x459887(0x561))[_0x459887(0x39f)](_0x2bcc54+'\x20'+_0x20511c);}},Game_Picture[_0x32f06e(0x76f)][_0x32f06e(0x337)]=function(){const _0xb508f2=_0x32f06e;return this[_0xb508f2(0x498)];},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x53a)]=Game_Picture[_0x32f06e(0x76f)][_0x32f06e(0x444)],Game_Picture[_0x32f06e(0x76f)][_0x32f06e(0x444)]=function(){const _0x152ecc=_0x32f06e;VisuMZ['CoreEngine'][_0x152ecc(0x53a)][_0x152ecc(0x31a)](this),this[_0x152ecc(0x498)]={'x':0x0,'y':0x0},this[_0x152ecc(0x2b7)]={'x':0x0,'y':0x0};},VisuMZ[_0x32f06e(0x424)]['Game_Picture_updateMove']=Game_Picture['prototype'][_0x32f06e(0x1f2)],Game_Picture[_0x32f06e(0x76f)][_0x32f06e(0x1f2)]=function(){const _0x524ead=_0x32f06e;this[_0x524ead(0x5cb)](),VisuMZ[_0x524ead(0x424)][_0x524ead(0x44d)][_0x524ead(0x31a)](this);},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x448)]=Game_Picture['prototype'][_0x32f06e(0x2de)],Game_Picture[_0x32f06e(0x76f)][_0x32f06e(0x2de)]=function(_0x1e81b7,_0x2a14e3,_0x39c0f8,_0x53e7c1,_0x5e7ff8,_0x7ff131,_0x106aa3,_0x4b486e){const _0x7cd8b9=_0x32f06e;VisuMZ['CoreEngine'][_0x7cd8b9(0x448)][_0x7cd8b9(0x31a)](this,_0x1e81b7,_0x2a14e3,_0x39c0f8,_0x53e7c1,_0x5e7ff8,_0x7ff131,_0x106aa3,_0x4b486e),this[_0x7cd8b9(0x6a9)]([{'x':0x0,'y':0x0},{'x':0.5,'y':0.5}][_0x2a14e3]||{'x':0x0,'y':0x0});},VisuMZ['CoreEngine'][_0x32f06e(0x52d)]=Game_Picture[_0x32f06e(0x76f)][_0x32f06e(0x702)],Game_Picture['prototype'][_0x32f06e(0x702)]=function(_0x375d56,_0x21f29a,_0x41761a,_0x4a77d2,_0x1e8b25,_0x108625,_0x4869c0,_0x4d517a,_0x233508){const _0x17b47f=_0x32f06e;VisuMZ[_0x17b47f(0x424)]['Game_Picture_move'][_0x17b47f(0x31a)](this,_0x375d56,_0x21f29a,_0x41761a,_0x4a77d2,_0x1e8b25,_0x108625,_0x4869c0,_0x4d517a,_0x233508),this[_0x17b47f(0x35d)]([{'x':0x0,'y':0x0},{'x':0.5,'y':0.5}][_0x375d56]||{'x':0x0,'y':0x0});},Game_Picture[_0x32f06e(0x76f)][_0x32f06e(0x5cb)]=function(){const _0x3e80da=_0x32f06e;this['_duration']>0x0&&(this[_0x3e80da(0x498)]['x']=this['applyEasing'](this[_0x3e80da(0x498)]['x'],this[_0x3e80da(0x2b7)]['x']),this[_0x3e80da(0x498)]['y']=this['applyEasing'](this[_0x3e80da(0x498)]['y'],this[_0x3e80da(0x2b7)]['y']));},Game_Picture[_0x32f06e(0x76f)][_0x32f06e(0x6a9)]=function(_0x5921c5){const _0x294fd7=_0x32f06e;this[_0x294fd7(0x498)]=_0x5921c5,this[_0x294fd7(0x2b7)]=JsonEx[_0x294fd7(0x740)](this['_anchor']);},Game_Picture[_0x32f06e(0x76f)]['setTargetAnchor']=function(_0x5183d8){const _0x404126=_0x32f06e;this[_0x404126(0x2b7)]=_0x5183d8;},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x581)]=Sprite_Picture[_0x32f06e(0x76f)][_0x32f06e(0x40b)],Sprite_Picture[_0x32f06e(0x76f)][_0x32f06e(0x40b)]=function(){const _0x5926aa=_0x32f06e,_0x5130cb=this[_0x5926aa(0x6f3)]();!_0x5130cb[_0x5926aa(0x337)]()?VisuMZ[_0x5926aa(0x424)][_0x5926aa(0x581)][_0x5926aa(0x31a)](this):(this[_0x5926aa(0x337)]['x']=_0x5130cb[_0x5926aa(0x337)]()['x'],this[_0x5926aa(0x337)]['y']=_0x5130cb['anchor']()['y']);},Game_Action[_0x32f06e(0x76f)]['setEnemyAction']=function(_0x5dc5d1){const _0x185e56=_0x32f06e;if(_0x5dc5d1){const _0x46c43c=_0x5dc5d1[_0x185e56(0x2e2)];if(_0x46c43c===0x1&&this[_0x185e56(0x487)]()[_0x185e56(0x68b)]()!==0x1)this[_0x185e56(0x519)]();else _0x46c43c===0x2&&this[_0x185e56(0x487)]()[_0x185e56(0x477)]()!==0x2?this[_0x185e56(0x50a)]():this['setSkill'](_0x46c43c);}else this[_0x185e56(0x353)]();},Game_Actor[_0x32f06e(0x76f)][_0x32f06e(0x572)]=function(){const _0x48d13d=_0x32f06e;return this[_0x48d13d(0x518)]()[_0x48d13d(0x3f6)](_0x542904=>this[_0x48d13d(0x6fc)](_0x542904)&&this[_0x48d13d(0x532)]()[_0x48d13d(0x77d)](_0x542904[_0x48d13d(0x794)]));},Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x64c)]=function(){const _0x211eb7=_0x32f06e;this['_dimmerSprite']=new Sprite(),this[_0x211eb7(0x39b)][_0x211eb7(0x1f7)]=new Bitmap(0x0,0x0),this[_0x211eb7(0x39b)]['x']=0x0,this[_0x211eb7(0x1f8)](this[_0x211eb7(0x39b)]);},Window_Base[_0x32f06e(0x76f)][_0x32f06e(0x290)]=function(){const _0x543cde=_0x32f06e;if(this[_0x543cde(0x39b)]){const _0x293140=this[_0x543cde(0x39b)][_0x543cde(0x1f7)],_0x5cd013=this['width'],_0x45bea2=this['height'],_0x56e546=this[_0x543cde(0x4f0)],_0x55251c=ColorManager['dimColor1'](),_0x5de295=ColorManager['dimColor2']();_0x293140[_0x543cde(0x3b4)](_0x5cd013,_0x45bea2),_0x293140[_0x543cde(0x3ef)](0x0,0x0,_0x5cd013,_0x56e546,_0x5de295,_0x55251c,!![]),_0x293140['fillRect'](0x0,_0x56e546,_0x5cd013,_0x45bea2-_0x56e546*0x2,_0x55251c),_0x293140[_0x543cde(0x3ef)](0x0,_0x45bea2-_0x56e546,_0x5cd013,_0x56e546,_0x55251c,_0x5de295,!![]),this[_0x543cde(0x39b)]['setFrame'](0x0,0x0,_0x5cd013,_0x45bea2);}},Game_Actor['prototype'][_0x32f06e(0x489)]=function(){const _0x390f4a=_0x32f06e;for(let _0x341da2=0x0;_0x341da2<this['numActions']();_0x341da2++){const _0x18973a=this[_0x390f4a(0x62a)]();let _0x3252dc=Number['MIN_SAFE_INTEGER'];this[_0x390f4a(0x25b)](_0x341da2,_0x18973a[0x0]);for(const _0x4d0b15 of _0x18973a){const _0xa4f8f3=_0x4d0b15[_0x390f4a(0x66f)]();_0xa4f8f3>_0x3252dc&&(_0x3252dc=_0xa4f8f3,this[_0x390f4a(0x25b)](_0x341da2,_0x4d0b15));}}this[_0x390f4a(0x433)](_0x390f4a(0x331));},Window_BattleItem[_0x32f06e(0x76f)][_0x32f06e(0x641)]=function(_0x129cff){const _0x359781=_0x32f06e;return BattleManager['actor']()?BattleManager['actor']()[_0x359781(0x6fc)](_0x129cff):Window_ItemList[_0x359781(0x76f)][_0x359781(0x641)][_0x359781(0x31a)](this,_0x129cff);},VisuMZ['CoreEngine'][_0x32f06e(0x705)]=Scene_Map[_0x32f06e(0x76f)]['createSpriteset'],Scene_Map['prototype'][_0x32f06e(0x343)]=function(){const _0x5d519d=_0x32f06e;VisuMZ['CoreEngine'][_0x5d519d(0x705)][_0x5d519d(0x31a)](this);const _0x10222b=this[_0x5d519d(0x429)][_0x5d519d(0x619)];if(_0x10222b)this['addChild'](_0x10222b);},VisuMZ[_0x32f06e(0x424)][_0x32f06e(0x402)]=Scene_Battle[_0x32f06e(0x76f)]['createSpriteset'],Scene_Battle['prototype'][_0x32f06e(0x343)]=function(){const _0x286233=_0x32f06e;VisuMZ['CoreEngine']['Scene_Battle_createSpriteset'][_0x286233(0x31a)](this);const _0x3c688e=this[_0x286233(0x429)][_0x286233(0x619)];if(_0x3c688e)this[_0x286233(0x2e9)](_0x3c688e);};