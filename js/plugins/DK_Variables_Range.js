/*
Title: Variables range
Author: DKPlugins
Site: https://dk-plugins.ru
E-mail: kuznetsovdenis96@gmail.com
Version: 2.0.0
Release: 02.09.2020
First release: 31.08.2019
*/

/*ru
Название: Диапазон переменных
Автор: DKPlugins
Сайт: https://dk-plugins.ru
E-mail: kuznetsovdenis96@gmail.com
Версия: 2.0.0
Релиз: 02.09.2020
Первый релиз: 31.08.2019
*/

/*:
 * @plugindesc v.2.0.0 Variables range. Allows you to control the value of a variable in a specified range
 * @author DKPlugins
 * @url https://dk-plugins.ru
 * @target MZ
 * @help

 ### Info about plugin ###
 Title: DKTools_Variables_Range
 Author: DKPlugins
 Site: https://dk-plugins.ru
 Version: 2.0.0
 Release: 02.09.2020
 First release: 31.08.2019

 ###=========================================================================
 ## Support
 ###=========================================================================
 Donate: https://dk-plugins.ru/donate
 Become a patron: https://www.patreon.com/dkplugins

 ###=========================================================================
 ## Compatibility
 ###=========================================================================
 RPG Maker MV: 1.6+
 RPG Maker MZ: 1.0+

 ###=========================================================================
 ## License and terms of use
 ###=========================================================================
 Recent information about the terms of use: https://dk-plugins.ru/terms-of-use

 You can:
 -Free use the plugin for your commercial and non commercial projects.
 -Translate the plugin to other languages (inform if you do this)
 -Change code of plugin, but you must specify a link to the original plugin

 You can't:
 -Delete or change any information about plugin (Title, authorship, contact information, version and release)

 * @param Variables
 * @desc List of variables
 * @type struct<Variable>[]
 * @default []

 */

/*:ru
 * @plugindesc v.2.0.0 Диапазон переменных. Позволяет контролировать значение переменной в заданном диапазоне
 * @author DKPlugins
 * @url https://dk-plugins.ru
 * @target MZ
 * @help

 ### Информация о плагине ###
 Название: DKTools_Variables_Range
 Автор: DKPlugins
 Сайт: https://dk-plugins.ru
 Версия: 2.0.0
 Релиз: 02.09.2020
 Первый релиз: 31.08.2019

 ###=========================================================================
 ## Поддержка
 ###=========================================================================
 Поддержать: https://dk-plugins.ru/donate
 Стать патроном: https://www.patreon.com/dkplugins

 ###=========================================================================
 ## Совместимость
 ###=========================================================================
 RPG Maker MV: 1.6+
 RPG Maker MZ: 1.0+

 ###=========================================================================
 ## Лицензия и правила использования плагина
 ###=========================================================================
 Актуальная информация о правилах использования: https://dk-plugins.ru/terms-of-use

 Вы можете:
 -Бесплатно использовать данный плагин в некоммерческих и коммерческих проектах
 -Переводить плагин на другие языки (сообщите мне, если Вы перевели плагин на другой язык)
 -Изменять код плагина, но Вы обязаны указать ссылку на оригинальный плагин

 Вы не можете:
 -Убирать или изменять любую информацию о плагине (Название, авторство, контактная информация, версия и дата релиза)

 * @param Variables
 * @text Переменные
 * @desc Список переменных
 * @type struct<Variable>[]
 * @default []

 */

/*~struct~Variable:

 * @param Id
 * @desc Variable
 * @type variable
 * @default 0

 * @param Min Value
 * @text Minimum value
 * @desc Minimum value
 * @default 0

 * @param Max Value
 * @text Maximum value
 * @desc Maximum value
 * @default 0

*/

/*~struct~Variable:ru

 * @param Id
 * @text Переменная
 * @desc Переменная
 * @type variable
 * @default 0

 * @param Min Value
 * @text Минимальное значение
 * @desc Минимальное значение
 * @default 0

 * @param Max Value
 * @text Максимальное значение
 * @desc Максимальное значение
 * @default 0

*/

'use strict';

var Imported = Imported || {};
Imported['DK_Variables_Range'] = '2.0.0';

(function() {

    const parameters = PluginManager.parameters('DK_Variables_Range');
    const variables = (JSON.parse(parameters['Variables']) || []).map((json) => {
        const obj = JSON.parse(json);

        return {
            'Id': Number(obj['Id']),
            'Min Value': Number(obj['Min Value']),
            'Max Value': Number(obj['Max Value'])
        };
    });

    //===========================================================================
    // Game_Variables
    //===========================================================================

    const VariablesRange_Game_Variables_setValue = Game_Variables.prototype.setValue;
    Game_Variables.prototype.setValue = function(id, value) {
        const preset = variables.find(v => v['Id'] === id);

        if (preset) {
            if (value < preset['Min Value']) {
                value = preset['Min Value'];
            } else if (value > preset['Max Value']) {
                value = preset['Max Value'];
            }
        }

        VariablesRange_Game_Variables_setValue.call(this, id, value);
    };

})();


